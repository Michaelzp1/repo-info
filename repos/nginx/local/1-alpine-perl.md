# `nginx:1.21.3-alpine-perl`

## Docker Metadata

- Image ID: `sha256:213a542c9862a490e0140cb5caf9b9916013d516b6ce3aa35a7f120b95c4cd32`
- Created: `2021-09-09T18:27:07.73390303Z`
- Virtual Size: ~ 57.58 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/docker-entrypoint.sh"]`
- Command: `["nginx","-g","daemon off;"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `NGINX_VERSION=1.21.3`
  - `NJS_VERSION=0.6.2`
  - `PKG_RELEASE=1`
- Labels:
  - `maintainer=NGINX Docker Maintainers <docker-maint@nginx.com>`

## `apk` (`.apk`-based packages)

### `apk` package: `alpine-baselayout`

```console
alpine-baselayout-3.2.0-r16 description:
Alpine base dir structure and init scripts

alpine-baselayout-3.2.0-r16 webpage:
https://git.alpinelinux.org/cgit/aports/tree/main/alpine-baselayout

alpine-baselayout-3.2.0-r16 installed size:
404 KiB

alpine-baselayout-3.2.0-r16 license:
GPL-2.0-only

```

### `apk` package: `alpine-keys`

```console
alpine-keys-2.3-r1 description:
Public keys for Alpine Linux packages

alpine-keys-2.3-r1 webpage:
https://alpinelinux.org

alpine-keys-2.3-r1 installed size:
116 KiB

alpine-keys-2.3-r1 license:
MIT

```

### `apk` package: `apk-tools`

```console
apk-tools-2.12.7-r0 description:
Alpine Package Keeper - package manager for alpine

apk-tools-2.12.7-r0 webpage:
https://gitlab.alpinelinux.org/alpine/apk-tools

apk-tools-2.12.7-r0 installed size:
304 KiB

apk-tools-2.12.7-r0 license:
GPL-2.0-only

```

### `apk` package: `brotli-libs`

```console
brotli-libs-1.0.9-r5 description:
Generic lossless compressor (libraries)

brotli-libs-1.0.9-r5 webpage:
https://github.com/google/brotli

brotli-libs-1.0.9-r5 installed size:
720 KiB

brotli-libs-1.0.9-r5 license:
MIT

```

### `apk` package: `busybox`

```console
busybox-1.33.1-r3 description:
Size optimized toolbox of many common UNIX utilities

busybox-1.33.1-r3 webpage:
https://busybox.net/

busybox-1.33.1-r3 installed size:
928 KiB

busybox-1.33.1-r3 license:
GPL-2.0-only

```

### `apk` package: `ca-certificates`

```console
ca-certificates-20191127-r5 description:
Common CA certificates PEM files from Mozilla

ca-certificates-20191127-r5 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-20191127-r5 installed size:
672 KiB

ca-certificates-20191127-r5 license:
MPL-2.0 AND MIT

```

### `apk` package: `ca-certificates-bundle`

```console
ca-certificates-bundle-20191127-r5 description:
Pre generated bundle of Mozilla certificates

ca-certificates-bundle-20191127-r5 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-bundle-20191127-r5 installed size:
228 KiB

ca-certificates-bundle-20191127-r5 license:
MPL-2.0 AND MIT

```

### `apk` package: `curl`

```console
curl-7.78.0-r0 description:
URL retrival utility and library

curl-7.78.0-r0 webpage:
https://curl.se/

curl-7.78.0-r0 installed size:
244 KiB

curl-7.78.0-r0 license:
MIT

```

### `apk` package: `freetype`

```console
freetype-2.10.4-r1 description:
TrueType font rendering library

freetype-2.10.4-r1 webpage:
https://www.freetype.org/

freetype-2.10.4-r1 installed size:
728 KiB

freetype-2.10.4-r1 license:
FTL GPL-2.0-or-later

```

### `apk` package: `geoip`

```console
geoip-1.6.12-r1 description:
Lookup countries by IP addresses

geoip-1.6.12-r1 webpage:
http://www.maxmind.com/app/ip-location

geoip-1.6.12-r1 installed size:
284 KiB

geoip-1.6.12-r1 license:
GPL

```

### `apk` package: `libbz2`

```console
libbz2-1.0.8-r1 description:
Shared library for bz2

libbz2-1.0.8-r1 webpage:
http://sources.redhat.com/bzip2

libbz2-1.0.8-r1 installed size:
72 KiB

libbz2-1.0.8-r1 license:
bzip2-1.0.6

```

### `apk` package: `libc-utils`

```console
libc-utils-0.7.2-r3 description:
Meta package to pull in correct libc

libc-utils-0.7.2-r3 webpage:
https://alpinelinux.org

libc-utils-0.7.2-r3 installed size:
4096 B

libc-utils-0.7.2-r3 license:
BSD-2-Clause AND BSD-3-Clause

```

### `apk` package: `libcrypto1.1`

```console
libcrypto1.1-1.1.1l-r0 description:
Crypto library from openssl

libcrypto1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libcrypto1.1-1.1.1l-r0 installed size:
2704 KiB

libcrypto1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libcurl`

```console
libcurl-7.78.0-r0 description:
The multiprotocol file transfer library

libcurl-7.78.0-r0 webpage:
https://curl.se/

libcurl-7.78.0-r0 installed size:
500 KiB

libcurl-7.78.0-r0 license:
MIT

```

### `apk` package: `libedit`

```console
libedit-20210216.3.1-r0 description:
BSD line editing library

libedit-20210216.3.1-r0 webpage:
https://www.thrysoee.dk/editline

libedit-20210216.3.1-r0 installed size:
196 KiB

libedit-20210216.3.1-r0 license:
BSD-3-Clause

```

### `apk` package: `libgcrypt`

```console
libgcrypt-1.9.4-r0 description:
General purpose crypto library based on the code used in GnuPG

libgcrypt-1.9.4-r0 webpage:
https://www.gnupg.org/

libgcrypt-1.9.4-r0 installed size:
1220 KiB

libgcrypt-1.9.4-r0 license:
LGPL-2.1-or-later

```

### `apk` package: `libgd`

```console
libgd-2.3.2-r1 description:
Library for the dynamic creation of images by programmers (libraries)

libgd-2.3.2-r1 webpage:
https://libgd.github.io/

libgd-2.3.2-r1 installed size:
372 KiB

libgd-2.3.2-r1 license:
custom

```

### `apk` package: `libgpg-error`

```console
libgpg-error-1.42-r0 description:
Support library for libgcrypt

libgpg-error-1.42-r0 webpage:
https://www.gnupg.org/

libgpg-error-1.42-r0 installed size:
212 KiB

libgpg-error-1.42-r0 license:
GPL-2.0-or-later LGPL-2.1-or-later

```

### `apk` package: `libintl`

```console
libintl-0.21-r0 description:
GNU gettext runtime library

libintl-0.21-r0 webpage:
https://www.gnu.org/software/gettext/gettext.html

libintl-0.21-r0 installed size:
56 KiB

libintl-0.21-r0 license:
LGPL-2.1-or-later

```

### `apk` package: `libjpeg-turbo`

```console
libjpeg-turbo-2.1.0-r0 description:
Accelerated baseline JPEG compression and decompression library

libjpeg-turbo-2.1.0-r0 webpage:
https://libjpeg-turbo.org/

libjpeg-turbo-2.1.0-r0 installed size:
1076 KiB

libjpeg-turbo-2.1.0-r0 license:
BSD-3-Clause IJG Zlib

```

### `apk` package: `libpng`

```console
libpng-1.6.37-r1 description:
Portable Network Graphics library

libpng-1.6.37-r1 webpage:
http://www.libpng.org

libpng-1.6.37-r1 installed size:
204 KiB

libpng-1.6.37-r1 license:
Libpng

```

### `apk` package: `libretls`

```console
libretls-3.3.3p1-r2 description:
port of libtls from libressl to openssl

libretls-3.3.3p1-r2 webpage:
https://git.causal.agency/libretls/

libretls-3.3.3p1-r2 installed size:
84 KiB

libretls-3.3.3p1-r2 license:
ISC AND (BSD-3-Clause OR MIT)

```

### `apk` package: `libssl1.1`

```console
libssl1.1-1.1.1l-r0 description:
SSL shared libraries

libssl1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libssl1.1-1.1.1l-r0 installed size:
528 KiB

libssl1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libwebp`

```console
libwebp-1.2.0-r2 description:
Libraries for working with WebP images

libwebp-1.2.0-r2 webpage:
https://developers.google.com/speed/webp

libwebp-1.2.0-r2 installed size:
580 KiB

libwebp-1.2.0-r2 license:
BSD-3-Clause

```

### `apk` package: `libxml2`

```console
libxml2-2.9.12-r1 description:
XML parsing library, version 2

libxml2-2.9.12-r1 webpage:
http://www.xmlsoft.org/

libxml2-2.9.12-r1 installed size:
1200 KiB

libxml2-2.9.12-r1 license:
MIT

```

### `apk` package: `libxslt`

```console
libxslt-1.1.34-r1 description:
XML stylesheet transformation library

libxslt-1.1.34-r1 webpage:
http://xmlsoft.org/XSLT/

libxslt-1.1.34-r1 installed size:
360 KiB

libxslt-1.1.34-r1 license:
custom

```

### `apk` package: `musl`

```console
musl-1.2.2-r3 description:
the musl c library (libc) implementation

musl-1.2.2-r3 webpage:
https://musl.libc.org/

musl-1.2.2-r3 installed size:
608 KiB

musl-1.2.2-r3 license:
MIT

```

### `apk` package: `musl-utils`

```console
musl-utils-1.2.2-r3 description:
the musl c library (libc) implementation

musl-utils-1.2.2-r3 webpage:
https://musl.libc.org/

musl-utils-1.2.2-r3 installed size:
144 KiB

musl-utils-1.2.2-r3 license:
MIT BSD GPL2+

```

### `apk` package: `ncurses-libs`

```console
ncurses-libs-6.2_p20210612-r0 description:
Ncurses libraries

ncurses-libs-6.2_p20210612-r0 webpage:
https://invisible-island.net/ncurses/

ncurses-libs-6.2_p20210612-r0 installed size:
500 KiB

ncurses-libs-6.2_p20210612-r0 license:
MIT

```

### `apk` package: `ncurses-terminfo-base`

```console
ncurses-terminfo-base-6.2_p20210612-r0 description:
Descriptions of common terminals

ncurses-terminfo-base-6.2_p20210612-r0 webpage:
https://invisible-island.net/ncurses/

ncurses-terminfo-base-6.2_p20210612-r0 installed size:
216 KiB

ncurses-terminfo-base-6.2_p20210612-r0 license:
MIT

```

### `apk` package: `nghttp2-libs`

```console
nghttp2-libs-1.43.0-r0 description:
Experimental HTTP/2 client, server and proxy (libraries)

nghttp2-libs-1.43.0-r0 webpage:
https://nghttp2.org

nghttp2-libs-1.43.0-r0 installed size:
168 KiB

nghttp2-libs-1.43.0-r0 license:
MIT

```

### `apk` package: `nginx`

```console
nginx-1.21.3-r1 description:
High performance web server

nginx-1.21.3-r1 webpage:
https://nginx.org/

nginx-1.21.3-r1 installed size:
2584 KiB

nginx-1.21.3-r1 license:
2-clause BSD-like license

```

### `apk` package: `nginx-module-geoip`

```console
nginx-module-geoip-1.21.3-r1 description:
nginx GeoIP dynamic modules

nginx-module-geoip-1.21.3-r1 webpage:
https://nginx.org/

nginx-module-geoip-1.21.3-r1 installed size:
116 KiB

nginx-module-geoip-1.21.3-r1 license:
2-clause BSD-like license

```

### `apk` package: `nginx-module-image-filter`

```console
nginx-module-image-filter-1.21.3-r1 description:
nginx image filter dynamic module

nginx-module-image-filter-1.21.3-r1 webpage:
https://nginx.org/

nginx-module-image-filter-1.21.3-r1 installed size:
88 KiB

nginx-module-image-filter-1.21.3-r1 license:
2-clause BSD-like license

```

### `apk` package: `nginx-module-njs`

```console
nginx-module-njs-1.21.3.0.6.2-r1 description:
nginx njs dynamic modules

nginx-module-njs-1.21.3.0.6.2-r1 webpage:
https://nginx.org/

nginx-module-njs-1.21.3.0.6.2-r1 installed size:
3624 KiB

nginx-module-njs-1.21.3.0.6.2-r1 license:
2-clause BSD-like license

```

### `apk` package: `nginx-module-perl`

```console
nginx-module-perl-1.21.3-r1 description:
nginx Perl dynamic module

nginx-module-perl-1.21.3-r1 webpage:
https://nginx.org/

nginx-module-perl-1.21.3-r1 installed size:
156 KiB

nginx-module-perl-1.21.3-r1 license:
2-clause BSD-like license

```

### `apk` package: `nginx-module-xslt`

```console
nginx-module-xslt-1.21.3-r1 description:
nginx xslt dynamic module

nginx-module-xslt-1.21.3-r1 webpage:
https://nginx.org/

nginx-module-xslt-1.21.3-r1 installed size:
84 KiB

nginx-module-xslt-1.21.3-r1 license:
2-clause BSD-like license

```

### `apk` package: `pcre`

```console
pcre-8.44-r0 description:
Perl-compatible regular expression library

pcre-8.44-r0 webpage:
http://pcre.sourceforge.net

pcre-8.44-r0 installed size:
392 KiB

pcre-8.44-r0 license:
BSD-3-Clause

```

### `apk` package: `perl`

```console
perl-5.32.1-r0 description:
Larry Wall's Practical Extraction and Report Language

perl-5.32.1-r0 webpage:
https://www.perl.org/

perl-5.32.1-r0 installed size:
37 MiB

perl-5.32.1-r0 license:
Artistic-Perl OR GPL-1.0-or-later

```

### `apk` package: `scanelf`

```console
scanelf-1.3.2-r0 description:
Scan ELF binaries for stuff

scanelf-1.3.2-r0 webpage:
https://wiki.gentoo.org/wiki/Hardened/PaX_Utilities

scanelf-1.3.2-r0 installed size:
92 KiB

scanelf-1.3.2-r0 license:
GPL-2.0-only

```

### `apk` package: `ssl_client`

```console
ssl_client-1.33.1-r3 description:
EXternal ssl_client for busybox wget

ssl_client-1.33.1-r3 webpage:
https://busybox.net/

ssl_client-1.33.1-r3 installed size:
28 KiB

ssl_client-1.33.1-r3 license:
GPL-2.0-only

```

### `apk` package: `tzdata`

```console
tzdata-2021a-r0 description:
Timezone data

tzdata-2021a-r0 webpage:
https://www.iana.org/time-zones

tzdata-2021a-r0 installed size:
3436 KiB

tzdata-2021a-r0 license:
Public-Domain

```

### `apk` package: `xz-libs`

```console
xz-libs-5.2.5-r0 description:
Library and CLI tools for XZ and LZMA compressed files (libraries)

xz-libs-5.2.5-r0 webpage:
https://tukaani.org/xz

xz-libs-5.2.5-r0 installed size:
148 KiB

xz-libs-5.2.5-r0 license:
GPL-2.0-or-later AND Public-Domain AND LGPL-2.1-or-later

```

### `apk` package: `zlib`

```console
zlib-1.2.11-r3 description:
A compression/decompression Library

zlib-1.2.11-r3 webpage:
https://zlib.net/

zlib-1.2.11-r3 installed size:
108 KiB

zlib-1.2.11-r3 license:
Zlib

```
