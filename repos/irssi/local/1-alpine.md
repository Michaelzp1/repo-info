# `irssi:1.2.3-alpine`

## Docker Metadata

- Image ID: `sha256:e872f1d1a64037f0c87f699ce6328fb406d308379574ce606a800c87c1e102b8`
- Created: `2021-09-01T03:03:23.266191878Z`
- Virtual Size: ~ 58.54 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["irssi"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `HOME=/home/user`
  - `LANG=C.UTF-8`
  - `IRSSI_VERSION=1.2.3`

## `apk` (`.apk`-based packages)

### `apk` package: `alpine-baselayout`

```console
alpine-baselayout-3.2.0-r8 description:
Alpine base dir structure and init scripts

alpine-baselayout-3.2.0-r8 webpage:
https://git.alpinelinux.org/cgit/aports/tree/main/alpine-baselayout

alpine-baselayout-3.2.0-r8 installed size:
400 KiB

alpine-baselayout-3.2.0-r8 license:
GPL-2.0-only

```

### `apk` package: `alpine-keys`

```console
alpine-keys-2.2-r0 description:
Public keys for Alpine Linux packages

alpine-keys-2.2-r0 webpage:
https://alpinelinux.org

alpine-keys-2.2-r0 installed size:
104 KiB

alpine-keys-2.2-r0 license:
MIT

```

### `apk` package: `apk-tools`

```console
apk-tools-2.12.7-r0 description:
Alpine Package Keeper - package manager for alpine

apk-tools-2.12.7-r0 webpage:
https://gitlab.alpinelinux.org/alpine/apk-tools

apk-tools-2.12.7-r0 installed size:
304 KiB

apk-tools-2.12.7-r0 license:
GPL-2.0-only

```

### `apk` package: `busybox`

```console
busybox-1.32.1-r6 description:
Size optimized toolbox of many common UNIX utilities

busybox-1.32.1-r6 webpage:
https://busybox.net/

busybox-1.32.1-r6 installed size:
924 KiB

busybox-1.32.1-r6 license:
GPL-2.0-only

```

### `apk` package: `ca-certificates`

```console
ca-certificates-20191127-r5 description:
Common CA certificates PEM files from Mozilla

ca-certificates-20191127-r5 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-20191127-r5 installed size:
672 KiB

ca-certificates-20191127-r5 license:
MPL-2.0 AND MIT

```

### `apk` package: `ca-certificates-bundle`

```console
ca-certificates-bundle-20191127-r5 description:
Pre generated bundle of Mozilla certificates

ca-certificates-bundle-20191127-r5 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-bundle-20191127-r5 installed size:
228 KiB

ca-certificates-bundle-20191127-r5 license:
MPL-2.0 AND MIT

```

### `apk` package: `glib`

```console
glib-2.66.8-r0 description:
Common C routines used by Gtk+ and other libs

glib-2.66.8-r0 webpage:
https://developer.gnome.org/glib/

glib-2.66.8-r0 installed size:
3324 KiB

glib-2.66.8-r0 license:
LGPL-2.1-or-later

```

### `apk` package: `libblkid`

```console
libblkid-2.36.1-r1 description:
Block device identification library from util-linux

libblkid-2.36.1-r1 webpage:
https://git.kernel.org/cgit/utils/util-linux/util-linux.git

libblkid-2.36.1-r1 installed size:
292 KiB

libblkid-2.36.1-r1 license:
GPL-3.0-or-later AND GPL-2.0-or-later AND GPL-2.0-only AND

```

### `apk` package: `libbz2`

```console
libbz2-1.0.8-r1 description:
Shared library for bz2

libbz2-1.0.8-r1 webpage:
http://sources.redhat.com/bzip2

libbz2-1.0.8-r1 installed size:
72 KiB

libbz2-1.0.8-r1 license:
bzip2-1.0.6

```

### `apk` package: `libc-utils`

```console
libc-utils-0.7.2-r3 description:
Meta package to pull in correct libc

libc-utils-0.7.2-r3 webpage:
https://alpinelinux.org

libc-utils-0.7.2-r3 installed size:
4096 B

libc-utils-0.7.2-r3 license:
BSD-2-Clause AND BSD-3-Clause

```

### `apk` package: `libcrypto1.1`

```console
libcrypto1.1-1.1.1l-r0 description:
Crypto library from openssl

libcrypto1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libcrypto1.1-1.1.1l-r0 installed size:
2704 KiB

libcrypto1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libffi`

```console
libffi-3.3-r2 description:
A portable, high level programming interface to various calling conventions.

libffi-3.3-r2 webpage:
https://sourceware.org/libffi

libffi-3.3-r2 installed size:
52 KiB

libffi-3.3-r2 license:
MIT

```

### `apk` package: `libintl`

```console
libintl-0.20.2-r2 description:
GNU gettext runtime library

libintl-0.20.2-r2 webpage:
https://www.gnu.org/software/gettext/gettext.html

libintl-0.20.2-r2 installed size:
56 KiB

libintl-0.20.2-r2 license:
LGPL-2.1-or-later

```

### `apk` package: `libmount`

```console
libmount-2.36.1-r1 description:
Block device identification library from util-linux

libmount-2.36.1-r1 webpage:
https://git.kernel.org/cgit/utils/util-linux/util-linux.git

libmount-2.36.1-r1 installed size:
328 KiB

libmount-2.36.1-r1 license:
GPL-3.0-or-later AND GPL-2.0-or-later AND GPL-2.0-only AND

```

### `apk` package: `libssl1.1`

```console
libssl1.1-1.1.1l-r0 description:
SSL shared libraries

libssl1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libssl1.1-1.1.1l-r0 installed size:
528 KiB

libssl1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libtls-standalone`

```console
libtls-standalone-2.9.1-r1 description:
libtls extricated from libressl sources

libtls-standalone-2.9.1-r1 webpage:
https://www.libressl.org/

libtls-standalone-2.9.1-r1 installed size:
108 KiB

libtls-standalone-2.9.1-r1 license:
ISC

```

### `apk` package: `musl`

```console
musl-1.2.2-r1 description:
the musl c library (libc) implementation

musl-1.2.2-r1 webpage:
https://musl.libc.org/

musl-1.2.2-r1 installed size:
608 KiB

musl-1.2.2-r1 license:
MIT

```

### `apk` package: `musl-utils`

```console
musl-utils-1.2.2-r1 description:
the musl c library (libc) implementation

musl-utils-1.2.2-r1 webpage:
https://musl.libc.org/

musl-utils-1.2.2-r1 installed size:
140 KiB

musl-utils-1.2.2-r1 license:
MIT BSD GPL2+

```

### `apk` package: `ncurses-libs`

```console
ncurses-libs-6.2_p20210109-r0 description:
Ncurses libraries

ncurses-libs-6.2_p20210109-r0 webpage:
https://invisible-island.net/ncurses/

ncurses-libs-6.2_p20210109-r0 installed size:
496 KiB

ncurses-libs-6.2_p20210109-r0 license:
MIT

```

### `apk` package: `ncurses-terminfo-base`

```console
ncurses-terminfo-base-6.2_p20210109-r0 description:
Descriptions of common terminals

ncurses-terminfo-base-6.2_p20210109-r0 webpage:
https://invisible-island.net/ncurses/

ncurses-terminfo-base-6.2_p20210109-r0 installed size:
216 KiB

ncurses-terminfo-base-6.2_p20210109-r0 license:
MIT

```

### `apk` package: `pcre`

```console
pcre-8.44-r0 description:
Perl-compatible regular expression library

pcre-8.44-r0 webpage:
http://pcre.sourceforge.net

pcre-8.44-r0 installed size:
392 KiB

pcre-8.44-r0 license:
BSD-3-Clause

```

### `apk` package: `perl`

```console
perl-5.32.0-r0 description:
Larry Wall's Practical Extraction and Report Language

perl-5.32.0-r0 webpage:
https://www.perl.org/

perl-5.32.0-r0 installed size:
37 MiB

perl-5.32.0-r0 license:
Artistic-Perl OR GPL-1.0-or-later

```

### `apk` package: `perl-capture-tiny`

```console
perl-capture-tiny-0.48-r2 description:
Capture STDOUT and STDERR from Perl, XS or external programs

perl-capture-tiny-0.48-r2 webpage:
https://metacpan.org/release/Capture-Tiny/

perl-capture-tiny-0.48-r2 installed size:
84 KiB

perl-capture-tiny-0.48-r2 license:
Apache-2.0

```

### `apk` package: `perl-devel-symdump`

```console
perl-devel-symdump-2.18-r2 description:
dump symbol names or the symbol table

perl-devel-symdump-2.18-r2 webpage:
https://metacpan.org/release/Devel-Symdump/

perl-devel-symdump-2.18-r2 installed size:
76 KiB

perl-devel-symdump-2.18-r2 license:
GPL PerlArtistic

```

### `apk` package: `perl-encode-locale`

```console
perl-encode-locale-1.05-r3 description:
Perl module - Determine the locale encoding

perl-encode-locale-1.05-r3 webpage:
http://search.cpan.org/~gaas

perl-encode-locale-1.05-r3 installed size:
72 KiB

perl-encode-locale-1.05-r3 license:
GPL PerlArtistic

```

### `apk` package: `perl-file-listing`

```console
perl-file-listing-6.14-r0 description:
Parse directory listing

perl-file-listing-6.14-r0 webpage:
https://metacpan.org/release/File-Listing/

perl-file-listing-6.14-r0 installed size:
68 KiB

perl-file-listing-6.14-r0 license:
GPL-1.0-or-later OR Artistic-1.0-Perl

```

### `apk` package: `perl-html-parser`

```console
perl-html-parser-3.75-r1 description:
HTML parser class

perl-html-parser-3.75-r1 webpage:
https://metacpan.org/release/HTML-Parser/

perl-html-parser-3.75-r1 installed size:
184 KiB

perl-html-parser-3.75-r1 license:
Artistic-1.0-Perl or GPL-1.0-or-later

```

### `apk` package: `perl-html-tagset`

```console
perl-html-tagset-3.20-r3 description:
data tables useful in parsing HTML

perl-html-tagset-3.20-r3 webpage:
https://metacpan.org/release/HTML-Tagset/

perl-html-tagset-3.20-r3 installed size:
68 KiB

perl-html-tagset-3.20-r3 license:
GPL PerlArtistic

```

### `apk` package: `perl-http-cookies`

```console
perl-http-cookies-6.10-r0 description:
HTTP cookie jars

perl-http-cookies-6.10-r0 webpage:
https://metacpan.org/release/HTTP-Cookies

perl-http-cookies-6.10-r0 installed size:
96 KiB

perl-http-cookies-6.10-r0 license:
GPL-1.0-or-later OR Artistic-1.0-Perl

```

### `apk` package: `perl-http-daemon`

```console
perl-http-daemon-6.12-r1 description:
HTTP::Daemon perl module

perl-http-daemon-6.12-r1 webpage:
https://metacpan.org/release/HTTP-Daemon

perl-http-daemon-6.12-r1 installed size:
84 KiB

perl-http-daemon-6.12-r1 license:
GPL-1.0-or-later OR Artistic-1.0-Perl

```

### `apk` package: `perl-http-date`

```console
perl-http-date-6.05-r1 description:
Perl module date conversion routines

perl-http-date-6.05-r1 webpage:
https://metacpan.org/pod/HTTP::Date

perl-http-date-6.05-r1 installed size:
64 KiB

perl-http-date-6.05-r1 license:
GPL-1.0-or-later OR Artistic-1.0-Perl

```

### `apk` package: `perl-http-message`

```console
perl-http-message-6.27-r0 description:
HTTP style message

perl-http-message-6.27-r0 webpage:
https://metacpan.org/release/HTTP-Message

perl-http-message-6.27-r0 installed size:
212 KiB

perl-http-message-6.27-r0 license:
GPL-1.0-or-later OR Artistic-1.0-Perl

```

### `apk` package: `perl-http-negotiate`

```console
perl-http-negotiate-6.01-r3 description:
HTTP::Negotiate perl module

perl-http-negotiate-6.01-r3 webpage:
https://metacpan.org/release/HTTP-Negotiate/

perl-http-negotiate-6.01-r3 installed size:
68 KiB

perl-http-negotiate-6.01-r3 license:
GPL-2.0 or Artistic

```

### `apk` package: `perl-io-html`

```console
perl-io-html-1.004-r0 description:
Open an HTML file with automatic charset detection

perl-io-html-1.004-r0 webpage:
https://metacpan.org/release/IO-HTML/

perl-io-html-1.004-r0 installed size:
72 KiB

perl-io-html-1.004-r0 license:
GPL-1.0-or-later OR Artistic-1.0-Perl

```

### `apk` package: `perl-libwww`

```console
perl-libwww-6.52-r0 description:
Perl module - World Wide Web library

perl-libwww-6.52-r0 webpage:
https://metacpan.org/author/MSCHILLI

perl-libwww-6.52-r0 installed size:
396 KiB

perl-libwww-6.52-r0 license:
GPL-1.0-or-later OR Artistic-1.0-Perl

```

### `apk` package: `perl-lwp-mediatypes`

```console
perl-lwp-mediatypes-6.04-r1 description:
Perl module - guess media type for a file or a URL

perl-lwp-mediatypes-6.04-r1 webpage:
http://search.cpan.org/~gaas

perl-lwp-mediatypes-6.04-r1 installed size:
112 KiB

perl-lwp-mediatypes-6.04-r1 license:
GPL PerlArtistic

```

### `apk` package: `perl-net-http`

```console
perl-net-http-6.20-r0 description:
Low-level HTTP connection (client)

perl-net-http-6.20-r0 webpage:
https://metacpan.org/release/Net-HTTP/

perl-net-http-6.20-r0 installed size:
96 KiB

perl-net-http-6.20-r0 license:
GPL-1.0-or-later OR Artistic-1.0-Perl

```

### `apk` package: `perl-pod-coverage`

```console
perl-pod-coverage-0.23-r2 description:
Perl - Checks if the documentation of a module is comprehensive

perl-pod-coverage-0.23-r2 webpage:
http://search.cpan.org/~rclamp/Pod-Coverage-0.23

perl-pod-coverage-0.23-r2 installed size:
128 KiB

perl-pod-coverage-0.23-r2 license:
Artistic-1.0-Perl or GPL-1.0-or-later

```

### `apk` package: `perl-pod-parser`

```console
perl-pod-parser-1.63-r0 description:
Modules for parsing/translating POD format documents

perl-pod-parser-1.63-r0 webpage:
https://metacpan.org/release/Pod-Parser/

perl-pod-parser-1.63-r0 installed size:
248 KiB

perl-pod-parser-1.63-r0 license:
GPL-1.0-or-later OR Artistic-1.0-Perl

```

### `apk` package: `perl-test-pod`

```console
perl-test-pod-1.52-r2 description:
check for POD errors in files

perl-test-pod-1.52-r2 webpage:
https://metacpan.org/release/Test-Pod/

perl-test-pod-1.52-r2 installed size:
60 KiB

perl-test-pod-1.52-r2 license:
GPL PerlArtistic

```

### `apk` package: `perl-try-tiny`

```console
perl-try-tiny-0.30-r2 description:
minimal try/catch with proper preservation of package prepare_subpackages prepare_language_packs prepare_package create_apks

perl-try-tiny-0.30-r2 webpage:
https://search.cpan.org/dist/Try-Tiny/

perl-try-tiny-0.30-r2 installed size:
76 KiB

perl-try-tiny-0.30-r2 license:
MIT

```

### `apk` package: `perl-uri`

```console
perl-uri-5.05-r0 description:
Uniform Resource Identifiers (absolute and relative)

perl-uri-5.05-r0 webpage:
https://metacpan.org/release/URI/

perl-uri-5.05-r0 installed size:
332 KiB

perl-uri-5.05-r0 license:
GPL-1.0-or-later OR Artistic-1.0-Perl

```

### `apk` package: `perl-www-robotrules`

```console
perl-www-robotrules-6.02-r3 description:
WWW::RobotRules perl module

perl-www-robotrules-6.02-r3 webpage:
https://metacpan.org/release/WWW-RobotRules/

perl-www-robotrules-6.02-r3 installed size:
72 KiB

perl-www-robotrules-6.02-r3 license:
GPL-2.0 or Artistic

```

### `apk` package: `scanelf`

```console
scanelf-1.2.8-r0 description:
Scan ELF binaries for stuff

scanelf-1.2.8-r0 webpage:
https://wiki.gentoo.org/wiki/Hardened/PaX_Utilities

scanelf-1.2.8-r0 installed size:
92 KiB

scanelf-1.2.8-r0 license:
GPL-2.0-only

```

### `apk` package: `ssl_client`

```console
ssl_client-1.32.1-r6 description:
EXternal ssl_client for busybox wget

ssl_client-1.32.1-r6 webpage:
https://busybox.net/

ssl_client-1.32.1-r6 installed size:
28 KiB

ssl_client-1.32.1-r6 license:
GPL-2.0-only

```

### `apk` package: `zlib`

```console
zlib-1.2.11-r3 description:
A compression/decompression Library

zlib-1.2.11-r3 webpage:
https://zlib.net/

zlib-1.2.11-r3 installed size:
108 KiB

zlib-1.2.11-r3 license:
Zlib

```
