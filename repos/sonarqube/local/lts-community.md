# `sonarqube:8.9.3-community`

## Docker Metadata

- Image ID: `sha256:87d5e5ae3cf47e53b4f999032677607da555759033e87fe523d01bf56383c82a`
- Created: `2021-10-26T19:28:24.659581504Z`
- Virtual Size: ~ 508.74 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["bin/run.sh"]`
- Command: `["bin/sonar.sh"]`
- Environment:
  - `PATH=/opt/java/openjdk/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `JAVA_VERSION=jdk-11.0.11+9`
  - `LANG=en_US.UTF-8`
  - `LANGUAGE=en_US:en`
  - `LC_ALL=en_US.UTF-8`
  - `JAVA_HOME=/opt/java/openjdk`
  - `SONARQUBE_HOME=/opt/sonarqube`
  - `SONAR_VERSION=8.9.3.48735`
  - `SQ_DATA_DIR=/opt/sonarqube/data`
  - `SQ_EXTENSIONS_DIR=/opt/sonarqube/extensions`
  - `SQ_LOGS_DIR=/opt/sonarqube/logs`
  - `SQ_TEMP_DIR=/opt/sonarqube/temp`

## `apk` (`.apk`-based packages)

### `apk` package: `alpine-baselayout`

```console
alpine-baselayout-3.2.0-r7 description:
Alpine base dir structure and init scripts

alpine-baselayout-3.2.0-r7 webpage:
https://git.alpinelinux.org/cgit/aports/tree/main/alpine-baselayout

alpine-baselayout-3.2.0-r7 installed size:
400 KiB

alpine-baselayout-3.2.0-r7 license:
GPL-2.0-only

```

### `apk` package: `alpine-keys`

```console
alpine-keys-2.2-r0 description:
Public keys for Alpine Linux packages

alpine-keys-2.2-r0 webpage:
https://alpinelinux.org

alpine-keys-2.2-r0 installed size:
104 KiB

alpine-keys-2.2-r0 license:
MIT

```

### `apk` package: `apk-tools`

```console
apk-tools-2.10.8-r0 description:
Alpine Package Keeper - package manager for alpine

apk-tools-2.10.8-r0 webpage:
https://gitlab.alpinelinux.org/alpine/apk-tools

apk-tools-2.10.8-r0 installed size:
260 KiB

apk-tools-2.10.8-r0 license:
GPL-2.0-only

```

### `apk` package: `bash`

```console
bash-5.0.17-r0 description:
The GNU Bourne Again shell

bash-5.0.17-r0 webpage:
https://www.gnu.org/software/bash/bash.html

bash-5.0.17-r0 installed size:
1172 KiB

bash-5.0.17-r0 license:
GPL-3.0-or-later

```

### `apk` package: `brotli-libs`

```console
brotli-libs-1.0.9-r1 description:
Generic lossless compressor (libraries)

brotli-libs-1.0.9-r1 webpage:
https://github.com/google/brotli

brotli-libs-1.0.9-r1 installed size:
716 KiB

brotli-libs-1.0.9-r1 license:
MIT

```

### `apk` package: `busybox`

```console
busybox-1.31.1-r20 description:
Size optimized toolbox of many common UNIX utilities

busybox-1.31.1-r20 webpage:
https://busybox.net/

busybox-1.31.1-r20 installed size:
940 KiB

busybox-1.31.1-r20 license:
GPL-2.0-only

```

### `apk` package: `ca-certificates-bundle`

```console
ca-certificates-bundle-20191127-r4 description:
Pre generated bundle of Mozilla certificates

ca-certificates-bundle-20191127-r4 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-bundle-20191127-r4 installed size:
228 KiB

ca-certificates-bundle-20191127-r4 license:
MPL-2.0 GPL-2.0-or-later

```

### `apk` package: `encodings`

```console
encodings-1.0.5-r0 description:
X.org font encoding files

encodings-1.0.5-r0 webpage:
http://xorg.freedesktop.org/

encodings-1.0.5-r0 installed size:
788 KiB

encodings-1.0.5-r0 license:
custom

```

### `apk` package: `expat`

```console
expat-2.2.9-r1 description:
An XML Parser library written in C

expat-2.2.9-r1 webpage:
http://www.libexpat.org/

expat-2.2.9-r1 installed size:
184 KiB

expat-2.2.9-r1 license:
MIT

```

### `apk` package: `fontconfig`

```console
fontconfig-2.13.1-r2 description:
Library for configuring and customizing font access

fontconfig-2.13.1-r2 webpage:
https://www.freedesktop.org/wiki/Software/fontconfig

fontconfig-2.13.1-r2 installed size:
632 KiB

fontconfig-2.13.1-r2 license:
MIT

```

### `apk` package: `freetype`

```console
freetype-2.10.4-r0 description:
TrueType font rendering library

freetype-2.10.4-r0 webpage:
https://www.freetype.org/

freetype-2.10.4-r0 installed size:
732 KiB

freetype-2.10.4-r0 license:
FTL GPL-2.0-or-later

```

### `apk` package: `glibc`

```console
glibc-2.33-r0 description:
GNU C Library compatibility layer

glibc-2.33-r0 webpage:
https://github.com/sgerrand/alpine-pkg-glibc

glibc-2.33-r0 installed size:
9224 KiB

glibc-2.33-r0 license:
LGPL

```

### `apk` package: `glibc-bin`

```console
glibc-bin-2.33-r0 description:
GNU C Library compatibility layer

glibc-bin-2.33-r0 webpage:
https://github.com/sgerrand/alpine-pkg-glibc

glibc-bin-2.33-r0 installed size:
2572 KiB

glibc-bin-2.33-r0 license:
LGPL

```

### `apk` package: `libbz2`

```console
libbz2-1.0.8-r1 description:
Shared library for bz2

libbz2-1.0.8-r1 webpage:
http://sources.redhat.com/bzip2

libbz2-1.0.8-r1 installed size:
72 KiB

libbz2-1.0.8-r1 license:
bzip2-1.0.6

```

### `apk` package: `libc-utils`

```console
libc-utils-0.7.2-r3 description:
Meta package to pull in correct libc

libc-utils-0.7.2-r3 webpage:
https://alpinelinux.org

libc-utils-0.7.2-r3 installed size:
4096 B

libc-utils-0.7.2-r3 license:
BSD-2-Clause AND BSD-3-Clause

```

### `apk` package: `libcrypto1.1`

```console
libcrypto1.1-1.1.1l-r0 description:
Crypto library from openssl

libcrypto1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libcrypto1.1-1.1.1l-r0 installed size:
2700 KiB

libcrypto1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libfontenc`

```console
libfontenc-1.1.4-r0 description:
X11 font encoding library

libfontenc-1.1.4-r0 webpage:
http://xorg.freedesktop.org/

libfontenc-1.1.4-r0 installed size:
48 KiB

libfontenc-1.1.4-r0 license:
MIT

```

### `apk` package: `libgcc`

```console
libgcc-9.3.0-r2 description:
GNU C compiler runtime libraries

libgcc-9.3.0-r2 webpage:
https://gcc.gnu.org

libgcc-9.3.0-r2 installed size:
88 KiB

libgcc-9.3.0-r2 license:
GPL-2.0-or-later LGPL-2.1-or-later

```

### `apk` package: `libpng`

```console
libpng-1.6.37-r1 description:
Portable Network Graphics library

libpng-1.6.37-r1 webpage:
http://www.libpng.org

libpng-1.6.37-r1 installed size:
200 KiB

libpng-1.6.37-r1 license:
Libpng

```

### `apk` package: `libssl1.1`

```console
libssl1.1-1.1.1l-r0 description:
SSL shared libraries

libssl1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libssl1.1-1.1.1l-r0 installed size:
528 KiB

libssl1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libtls-standalone`

```console
libtls-standalone-2.9.1-r1 description:
libtls extricated from libressl sources

libtls-standalone-2.9.1-r1 webpage:
https://www.libressl.org/

libtls-standalone-2.9.1-r1 installed size:
108 KiB

libtls-standalone-2.9.1-r1 license:
ISC

```

### `apk` package: `libuuid`

```console
libuuid-2.35.2-r0 description:
DCE compatible Universally Unique Identifier library

libuuid-2.35.2-r0 webpage:
https://git.kernel.org/cgit/utils/util-linux/util-linux.git

libuuid-2.35.2-r0 installed size:
40 KiB

libuuid-2.35.2-r0 license:
GPL-2.0 GPL-2.0-or-later LGPL-2.0-or-later BSD Public-Domain

```

### `apk` package: `mkfontscale`

```console
mkfontscale-1.2.1-r1 description:
Scalable font index generator for X

mkfontscale-1.2.1-r1 webpage:
http://xorg.freedesktop.org

mkfontscale-1.2.1-r1 installed size:
56 KiB

mkfontscale-1.2.1-r1 license:
MIT

```

### `apk` package: `musl`

```console
musl-1.1.24-r10 description:
the musl c library (libc) implementation

musl-1.1.24-r10 webpage:
https://musl.libc.org/

musl-1.1.24-r10 installed size:
600 KiB

musl-1.1.24-r10 license:
MIT

```

### `apk` package: `musl-utils`

```console
musl-utils-1.1.24-r10 description:
the musl c library (libc) implementation

musl-utils-1.1.24-r10 webpage:
https://musl.libc.org/

musl-utils-1.1.24-r10 installed size:
148 KiB

musl-utils-1.1.24-r10 license:
MIT BSD GPL2+

```

### `apk` package: `ncurses-libs`

```console
ncurses-libs-6.2_p20200523-r0 description:
Ncurses libraries

ncurses-libs-6.2_p20200523-r0 webpage:
https://invisible-island.net/ncurses/

ncurses-libs-6.2_p20200523-r0 installed size:
496 KiB

ncurses-libs-6.2_p20200523-r0 license:
MIT

```

### `apk` package: `ncurses-terminfo-base`

```console
ncurses-terminfo-base-6.2_p20200523-r0 description:
Descriptions of common terminals

ncurses-terminfo-base-6.2_p20200523-r0 webpage:
https://invisible-island.net/ncurses/

ncurses-terminfo-base-6.2_p20200523-r0 installed size:
212 KiB

ncurses-terminfo-base-6.2_p20200523-r0 license:
MIT

```

### `apk` package: `readline`

```console
readline-8.0.4-r0 description:
GNU readline library

readline-8.0.4-r0 webpage:
https://tiswww.cwru.edu/php/chet/readline/rltop.html

readline-8.0.4-r0 installed size:
292 KiB

readline-8.0.4-r0 license:
GPL-2.0-or-later

```

### `apk` package: `scanelf`

```console
scanelf-1.2.6-r0 description:
Scan ELF binaries for stuff

scanelf-1.2.6-r0 webpage:
https://wiki.gentoo.org/wiki/Hardened/PaX_Utilities

scanelf-1.2.6-r0 installed size:
92 KiB

scanelf-1.2.6-r0 license:
GPL-2.0-only

```

### `apk` package: `ssl_client`

```console
ssl_client-1.31.1-r20 description:
EXternal ssl_client for busybox wget

ssl_client-1.31.1-r20 webpage:
https://busybox.net/

ssl_client-1.31.1-r20 installed size:
28 KiB

ssl_client-1.31.1-r20 license:
GPL-2.0-only

```

### `apk` package: `su-exec`

```console
su-exec-0.2-r1 description:
switch user and group id, setgroups and exec

su-exec-0.2-r1 webpage:
https://github.com/ncopa/su-exec

su-exec-0.2-r1 installed size:
24 KiB

su-exec-0.2-r1 license:
MIT

```

### `apk` package: `ttf-dejavu`

```console
ttf-dejavu-2.37-r1 description:
Font family based on the Bitstream Vera Fonts with a wider range of characters

ttf-dejavu-2.37-r1 webpage:
https://dejavu-fonts.github.io/

ttf-dejavu-2.37-r1 installed size:
17 MiB

ttf-dejavu-2.37-r1 license:
custom

```

### `apk` package: `zlib`

```console
zlib-1.2.11-r3 description:
A compression/decompression Library

zlib-1.2.11-r3 webpage:
https://zlib.net/

zlib-1.2.11-r3 installed size:
108 KiB

zlib-1.2.11-r3 license:
Zlib

```
