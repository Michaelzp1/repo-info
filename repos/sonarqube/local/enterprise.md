# `sonarqube:9.1.0-enterprise`

## Docker Metadata

- Image ID: `sha256:bc4639271c691442c2838f974018d1a0d862ed4cc94084bb47ff731431713e65`
- Created: `2021-10-26T19:33:12.261088337Z`
- Virtual Size: ~ 669.23 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/opt/sonarqube/bin/run.sh"]`
- Command: `["/opt/sonarqube/bin/sonar.sh"]`
- Environment:
  - `PATH=/opt/java/openjdk/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `LANG=en_US.UTF-8`
  - `LANGUAGE=en_US:en`
  - `LC_ALL=en_US.UTF-8`
  - `JAVA_HOME=/usr/lib/jvm/java-11-openjdk`
  - `SONARQUBE_HOME=/opt/sonarqube`
  - `SONAR_VERSION=9.1.0.47736`
  - `SQ_DATA_DIR=/opt/sonarqube/data`
  - `SQ_EXTENSIONS_DIR=/opt/sonarqube/extensions`
  - `SQ_LOGS_DIR=/opt/sonarqube/logs`
  - `SQ_TEMP_DIR=/opt/sonarqube/temp`

## `apk` (`.apk`-based packages)

### `apk` package: `alpine-baselayout`

```console
alpine-baselayout-3.2.0-r16 description:
Alpine base dir structure and init scripts

alpine-baselayout-3.2.0-r16 webpage:
https://git.alpinelinux.org/cgit/aports/tree/main/alpine-baselayout

alpine-baselayout-3.2.0-r16 installed size:
404 KiB

alpine-baselayout-3.2.0-r16 license:
GPL-2.0-only

```

### `apk` package: `alpine-keys`

```console
alpine-keys-2.3-r1 description:
Public keys for Alpine Linux packages

alpine-keys-2.3-r1 webpage:
https://alpinelinux.org

alpine-keys-2.3-r1 installed size:
116 KiB

alpine-keys-2.3-r1 license:
MIT

```

### `apk` package: `alsa-lib`

```console
alsa-lib-1.2.5-r2 description:
Advanced Linux Sound Architecture (ALSA) library

alsa-lib-1.2.5-r2 webpage:
http://www.alsa-project.org

alsa-lib-1.2.5-r2 installed size:
1464 KiB

alsa-lib-1.2.5-r2 license:
LGPL-2.1-or-later

```

### `apk` package: `apk-tools`

```console
apk-tools-2.12.7-r0 description:
Alpine Package Keeper - package manager for alpine

apk-tools-2.12.7-r0 webpage:
https://gitlab.alpinelinux.org/alpine/apk-tools

apk-tools-2.12.7-r0 installed size:
304 KiB

apk-tools-2.12.7-r0 license:
GPL-2.0-only

```

### `apk` package: `bash`

```console
bash-5.1.4-r0 description:
The GNU Bourne Again shell

bash-5.1.4-r0 webpage:
https://www.gnu.org/software/bash/bash.html

bash-5.1.4-r0 installed size:
1296 KiB

bash-5.1.4-r0 license:
GPL-3.0-or-later

```

### `apk` package: `brotli-libs`

```console
brotli-libs-1.0.9-r5 description:
Generic lossless compressor (libraries)

brotli-libs-1.0.9-r5 webpage:
https://github.com/google/brotli

brotli-libs-1.0.9-r5 installed size:
720 KiB

brotli-libs-1.0.9-r5 license:
MIT

```

### `apk` package: `busybox`

```console
busybox-1.33.1-r3 description:
Size optimized toolbox of many common UNIX utilities

busybox-1.33.1-r3 webpage:
https://busybox.net/

busybox-1.33.1-r3 installed size:
928 KiB

busybox-1.33.1-r3 license:
GPL-2.0-only

```

### `apk` package: `ca-certificates`

```console
ca-certificates-20191127-r5 description:
Common CA certificates PEM files from Mozilla

ca-certificates-20191127-r5 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-20191127-r5 installed size:
672 KiB

ca-certificates-20191127-r5 license:
MPL-2.0 AND MIT

```

### `apk` package: `ca-certificates-bundle`

```console
ca-certificates-bundle-20191127-r5 description:
Pre generated bundle of Mozilla certificates

ca-certificates-bundle-20191127-r5 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-bundle-20191127-r5 installed size:
228 KiB

ca-certificates-bundle-20191127-r5 license:
MPL-2.0 AND MIT

```

### `apk` package: `encodings`

```console
encodings-1.0.5-r0 description:
X.org font encoding files

encodings-1.0.5-r0 webpage:
http://xorg.freedesktop.org/

encodings-1.0.5-r0 installed size:
788 KiB

encodings-1.0.5-r0 license:
custom

```

### `apk` package: `expat`

```console
expat-2.4.1-r0 description:
XML Parser library written in C

expat-2.4.1-r0 webpage:
http://www.libexpat.org/

expat-2.4.1-r0 installed size:
188 KiB

expat-2.4.1-r0 license:
MIT

```

### `apk` package: `fontconfig`

```console
fontconfig-2.13.1-r4 description:
Library for configuring and customizing font access

fontconfig-2.13.1-r4 webpage:
https://www.freedesktop.org/wiki/Software/fontconfig

fontconfig-2.13.1-r4 installed size:
632 KiB

fontconfig-2.13.1-r4 license:
MIT

```

### `apk` package: `freetype`

```console
freetype-2.10.4-r1 description:
TrueType font rendering library

freetype-2.10.4-r1 webpage:
https://www.freetype.org/

freetype-2.10.4-r1 installed size:
728 KiB

freetype-2.10.4-r1 license:
FTL GPL-2.0-or-later

```

### `apk` package: `giflib`

```console
giflib-5.2.1-r0 description:
A library for reading and writing GIF images

giflib-5.2.1-r0 webpage:
https://sourceforge.net/projects/giflib/

giflib-5.2.1-r0 installed size:
52 KiB

giflib-5.2.1-r0 license:
MIT

```

### `apk` package: `java-cacerts`

```console
java-cacerts-1.0-r1 description:
Script to update java cacerts store

java-cacerts-1.0-r1 webpage:
https://git.alpinelinux.org/aports/tree/community/java-cacerts

java-cacerts-1.0-r1 installed size:
32 KiB

java-cacerts-1.0-r1 license:
MIT

```

### `apk` package: `java-common`

```console
java-common-0.4-r0 description:
Java common (updates java links)

java-common-0.4-r0 webpage:
https://git.alpinelinux.org/aports/

java-common-0.4-r0 installed size:
12 KiB

java-common-0.4-r0 license:
GPL-2.0-or-later

```

### `apk` package: `lcms2`

```console
lcms2-2.12-r1 description:
Color Management Engine

lcms2-2.12-r1 webpage:
http://www.littlecms.com

lcms2-2.12-r1 installed size:
344 KiB

lcms2-2.12-r1 license:
MIT GPL-3.0-only

```

### `apk` package: `libbsd`

```console
libbsd-0.11.3-r0 description:
commonly-used BSD functions not implemented by all libcs

libbsd-0.11.3-r0 webpage:
https://libbsd.freedesktop.org/

libbsd-0.11.3-r0 installed size:
80 KiB

libbsd-0.11.3-r0 license:
BSD

```

### `apk` package: `libbz2`

```console
libbz2-1.0.8-r1 description:
Shared library for bz2

libbz2-1.0.8-r1 webpage:
http://sources.redhat.com/bzip2

libbz2-1.0.8-r1 installed size:
72 KiB

libbz2-1.0.8-r1 license:
bzip2-1.0.6

```

### `apk` package: `libc-utils`

```console
libc-utils-0.7.2-r3 description:
Meta package to pull in correct libc

libc-utils-0.7.2-r3 webpage:
https://alpinelinux.org

libc-utils-0.7.2-r3 installed size:
4096 B

libc-utils-0.7.2-r3 license:
BSD-2-Clause AND BSD-3-Clause

```

### `apk` package: `libcrypto1.1`

```console
libcrypto1.1-1.1.1l-r0 description:
Crypto library from openssl

libcrypto1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libcrypto1.1-1.1.1l-r0 installed size:
2704 KiB

libcrypto1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libffi`

```console
libffi-3.3-r2 description:
A portable, high level programming interface to various calling conventions.

libffi-3.3-r2 webpage:
https://sourceware.org/libffi

libffi-3.3-r2 installed size:
52 KiB

libffi-3.3-r2 license:
MIT

```

### `apk` package: `libfontenc`

```console
libfontenc-1.1.4-r0 description:
X11 font encoding library

libfontenc-1.1.4-r0 webpage:
http://xorg.freedesktop.org/

libfontenc-1.1.4-r0 installed size:
48 KiB

libfontenc-1.1.4-r0 license:
MIT

```

### `apk` package: `libjpeg-turbo`

```console
libjpeg-turbo-2.1.0-r0 description:
Accelerated baseline JPEG compression and decompression library

libjpeg-turbo-2.1.0-r0 webpage:
https://libjpeg-turbo.org/

libjpeg-turbo-2.1.0-r0 installed size:
1076 KiB

libjpeg-turbo-2.1.0-r0 license:
BSD-3-Clause IJG Zlib

```

### `apk` package: `libmd`

```console
libmd-1.0.3-r0 description:
Message Digest functions from BSD systems

libmd-1.0.3-r0 webpage:
https://www.hadrons.org/software/libmd/

libmd-1.0.3-r0 installed size:
56 KiB

libmd-1.0.3-r0 license:
Public Domain

```

### `apk` package: `libpng`

```console
libpng-1.6.37-r1 description:
Portable Network Graphics library

libpng-1.6.37-r1 webpage:
http://www.libpng.org

libpng-1.6.37-r1 installed size:
204 KiB

libpng-1.6.37-r1 license:
Libpng

```

### `apk` package: `libretls`

```console
libretls-3.3.3p1-r2 description:
port of libtls from libressl to openssl

libretls-3.3.3p1-r2 webpage:
https://git.causal.agency/libretls/

libretls-3.3.3p1-r2 installed size:
84 KiB

libretls-3.3.3p1-r2 license:
ISC AND (BSD-3-Clause OR MIT)

```

### `apk` package: `libssl1.1`

```console
libssl1.1-1.1.1l-r0 description:
SSL shared libraries

libssl1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libssl1.1-1.1.1l-r0 installed size:
528 KiB

libssl1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libtasn1`

```console
libtasn1-4.17.0-r0 description:
The ASN.1 library used in GNUTLS

libtasn1-4.17.0-r0 webpage:
https://www.gnu.org/software/gnutls/

libtasn1-4.17.0-r0 installed size:
88 KiB

libtasn1-4.17.0-r0 license:
LGPL-2.1-or-later

```

### `apk` package: `libuuid`

```console
libuuid-2.37-r0 description:
DCE compatible Universally Unique Identifier library

libuuid-2.37-r0 webpage:
https://git.kernel.org/cgit/utils/util-linux/util-linux.git

libuuid-2.37-r0 installed size:
40 KiB

libuuid-2.37-r0 license:
GPL-3.0-or-later AND GPL-2.0-or-later AND GPL-2.0-only AND

```

### `apk` package: `libx11`

```console
libx11-1.7.2-r0 description:
X11 client-side library

libx11-1.7.2-r0 webpage:
http://xorg.freedesktop.org/

libx11-1.7.2-r0 installed size:
3240 KiB

libx11-1.7.2-r0 license:
custom:XFREE86

```

### `apk` package: `libxau`

```console
libxau-1.0.9-r0 description:
X11 authorisation library

libxau-1.0.9-r0 webpage:
http://xorg.freedesktop.org/

libxau-1.0.9-r0 installed size:
28 KiB

libxau-1.0.9-r0 license:
MIT

```

### `apk` package: `libxcb`

```console
libxcb-1.14-r2 description:
X11 client-side library

libxcb-1.14-r2 webpage:
https://xcb.freedesktop.org

libxcb-1.14-r2 installed size:
996 KiB

libxcb-1.14-r2 license:
MIT

```

### `apk` package: `libxdmcp`

```console
libxdmcp-1.1.3-r0 description:
X11 Display Manager Control Protocol library

libxdmcp-1.1.3-r0 webpage:
http://xorg.freedesktop.org/

libxdmcp-1.1.3-r0 installed size:
40 KiB

libxdmcp-1.1.3-r0 license:
MIT

```

### `apk` package: `libxext`

```console
libxext-1.3.4-r0 description:
X11 miscellaneous extensions library

libxext-1.3.4-r0 webpage:
http://xorg.freedesktop.org/

libxext-1.3.4-r0 installed size:
84 KiB

libxext-1.3.4-r0 license:
MIT

```

### `apk` package: `libxi`

```console
libxi-1.7.10-r0 description:
X11 Input extension library

libxi-1.7.10-r0 webpage:
https://www.x.org

libxi-1.7.10-r0 installed size:
76 KiB

libxi-1.7.10-r0 license:
MIT AND X11

```

### `apk` package: `libxrender`

```console
libxrender-0.9.10-r3 description:
X Rendering Extension client library

libxrender-0.9.10-r3 webpage:
http://xorg.freedesktop.org/

libxrender-0.9.10-r3 installed size:
56 KiB

libxrender-0.9.10-r3 license:
custom

```

### `apk` package: `libxtst`

```console
libxtst-1.2.3-r3 description:
X11 Testing -- Resource extension library

libxtst-1.2.3-r3 webpage:
https://wiki.freedesktop.org/xorg/

libxtst-1.2.3-r3 installed size:
40 KiB

libxtst-1.2.3-r3 license:
custom

```

### `apk` package: `mkfontscale`

```console
mkfontscale-1.2.1-r1 description:
Scalable font index generator for X

mkfontscale-1.2.1-r1 webpage:
http://xorg.freedesktop.org

mkfontscale-1.2.1-r1 installed size:
56 KiB

mkfontscale-1.2.1-r1 license:
MIT

```

### `apk` package: `musl`

```console
musl-1.2.2-r3 description:
the musl c library (libc) implementation

musl-1.2.2-r3 webpage:
https://musl.libc.org/

musl-1.2.2-r3 installed size:
608 KiB

musl-1.2.2-r3 license:
MIT

```

### `apk` package: `musl-utils`

```console
musl-utils-1.2.2-r3 description:
the musl c library (libc) implementation

musl-utils-1.2.2-r3 webpage:
https://musl.libc.org/

musl-utils-1.2.2-r3 installed size:
144 KiB

musl-utils-1.2.2-r3 license:
MIT BSD GPL2+

```

### `apk` package: `ncurses-libs`

```console
ncurses-libs-6.2_p20210612-r0 description:
Ncurses libraries

ncurses-libs-6.2_p20210612-r0 webpage:
https://invisible-island.net/ncurses/

ncurses-libs-6.2_p20210612-r0 installed size:
500 KiB

ncurses-libs-6.2_p20210612-r0 license:
MIT

```

### `apk` package: `ncurses-terminfo-base`

```console
ncurses-terminfo-base-6.2_p20210612-r0 description:
Descriptions of common terminals

ncurses-terminfo-base-6.2_p20210612-r0 webpage:
https://invisible-island.net/ncurses/

ncurses-terminfo-base-6.2_p20210612-r0 installed size:
216 KiB

ncurses-terminfo-base-6.2_p20210612-r0 license:
MIT

```

### `apk` package: `openjdk11-jre`

```console
openjdk11-jre-11.0.11_p9-r0 description:
Oracle OpenJDK 11 (JRE)

openjdk11-jre-11.0.11_p9-r0 webpage:
https://github.com/openjdk/jdk11u

openjdk11-jre-11.0.11_p9-r0 installed size:
696 KiB

openjdk11-jre-11.0.11_p9-r0 license:
GPL-2.0-with-classpath-exception

```

### `apk` package: `openjdk11-jre-headless`

```console
openjdk11-jre-headless-11.0.11_p9-r0 description:
Oracle OpenJDK 11 (JRE headless)

openjdk11-jre-headless-11.0.11_p9-r0 webpage:
https://github.com/openjdk/jdk11u

openjdk11-jre-headless-11.0.11_p9-r0 installed size:
159 MiB

openjdk11-jre-headless-11.0.11_p9-r0 license:
GPL-2.0-with-classpath-exception

```

### `apk` package: `p11-kit`

```console
p11-kit-0.23.22-r0 description:
Library for loading and sharing PKCS#11 modules

p11-kit-0.23.22-r0 webpage:
https://p11-glue.freedesktop.org/

p11-kit-0.23.22-r0 installed size:
1200 KiB

p11-kit-0.23.22-r0 license:
BSD-3-Clause

```

### `apk` package: `p11-kit-trust`

```console
p11-kit-trust-0.23.22-r0 description:
System trust module from p11-kit

p11-kit-trust-0.23.22-r0 webpage:
https://p11-glue.freedesktop.org/

p11-kit-trust-0.23.22-r0 installed size:
452 KiB

p11-kit-trust-0.23.22-r0 license:
BSD-3-Clause

```

### `apk` package: `readline`

```console
readline-8.1.0-r0 description:
GNU readline library

readline-8.1.0-r0 webpage:
https://tiswww.cwru.edu/php/chet/readline/rltop.html

readline-8.1.0-r0 installed size:
308 KiB

readline-8.1.0-r0 license:
GPL-2.0-or-later

```

### `apk` package: `scanelf`

```console
scanelf-1.3.2-r0 description:
Scan ELF binaries for stuff

scanelf-1.3.2-r0 webpage:
https://wiki.gentoo.org/wiki/Hardened/PaX_Utilities

scanelf-1.3.2-r0 installed size:
92 KiB

scanelf-1.3.2-r0 license:
GPL-2.0-only

```

### `apk` package: `ssl_client`

```console
ssl_client-1.33.1-r3 description:
EXternal ssl_client for busybox wget

ssl_client-1.33.1-r3 webpage:
https://busybox.net/

ssl_client-1.33.1-r3 installed size:
28 KiB

ssl_client-1.33.1-r3 license:
GPL-2.0-only

```

### `apk` package: `su-exec`

```console
su-exec-0.2-r1 description:
switch user and group id, setgroups and exec

su-exec-0.2-r1 webpage:
https://github.com/ncopa/su-exec

su-exec-0.2-r1 installed size:
24 KiB

su-exec-0.2-r1 license:
MIT

```

### `apk` package: `ttf-dejavu`

```console
ttf-dejavu-2.37-r1 description:
Font family based on the Bitstream Vera Fonts with a wider range of characters

ttf-dejavu-2.37-r1 webpage:
https://dejavu-fonts.github.io/

ttf-dejavu-2.37-r1 installed size:
17 MiB

ttf-dejavu-2.37-r1 license:
custom

```

### `apk` package: `zlib`

```console
zlib-1.2.11-r3 description:
A compression/decompression Library

zlib-1.2.11-r3 webpage:
https://zlib.net/

zlib-1.2.11-r3 installed size:
108 KiB

zlib-1.2.11-r3 license:
Zlib

```
