## `kibana:6.8.20`

```console
$ docker pull kibana@sha256:f58cb1ff78beaf2eada9e41b102da33749622e258f25aff45dcfa7949b07c227
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `kibana:6.8.20` - linux; amd64

```console
$ docker pull kibana@sha256:efb725e1b0ad8da7b48a67bfd8549678e7893ff1caf273e37d0e1371cfa84a21
```

-	Docker Version: 20.10.6
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **316.7 MB (316655874 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:300c82ff960e06cb5853bcb0269c9030a8051be0cdfcc68f02b3e09b9a77b694`
-	Default Command: `["\/usr\/local\/bin\/kibana-docker"]`

```dockerfile
# Wed, 15 Sep 2021 18:20:23 GMT
ADD file:b3ebbe8bd304723d43b7b44a6d990cd657b63d93d6a2a9293983a30bfc1dfa53 in / 
# Wed, 15 Sep 2021 18:20:23 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20201113 org.opencontainers.image.title=CentOS Base Image org.opencontainers.image.vendor=CentOS org.opencontainers.image.licenses=GPL-2.0-only org.opencontainers.image.created=2020-11-13 00:00:00+00:00
# Wed, 15 Sep 2021 18:20:23 GMT
CMD ["/bin/bash"]
# Thu, 07 Oct 2021 22:47:17 GMT
EXPOSE 5601
# Thu, 07 Oct 2021 22:48:06 GMT
RUN yum update -y && yum install -y fontconfig freetype && yum clean all
# Thu, 07 Oct 2021 22:48:28 GMT
COPY --chown=1000:0dir:78a3ad5d7dc33a6bbc5cabc75f58cd2ce0c75658fd5c7606a1f4505262e790c4 in /usr/share/kibana 
# Thu, 07 Oct 2021 22:48:30 GMT
WORKDIR /usr/share/kibana
# Thu, 07 Oct 2021 22:48:30 GMT
RUN ln -s /usr/share/kibana /opt/kibana
# Thu, 07 Oct 2021 22:48:31 GMT
ENV ELASTIC_CONTAINER=true
# Thu, 07 Oct 2021 22:48:31 GMT
ENV PATH=/usr/share/kibana/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
# Thu, 07 Oct 2021 22:48:31 GMT
COPY --chown=1000:0file:60b6181f28c99b362092ea6139b6d4112ddd0bef32d52563c33b26bdc2b51318 in /usr/share/kibana/config/kibana.yml 
# Thu, 07 Oct 2021 22:48:32 GMT
COPY --chown=1000:0file:7d472d1939e23e2f10e7c5fd1e9166eefd646214aa0d8a1c09d92af71c9cbd87 in /usr/local/bin/ 
# Thu, 07 Oct 2021 22:48:33 GMT
RUN chmod g+ws /usr/share/kibana && find /usr/share/kibana -gid 0 -and -not -perm /g+w -exec chmod g+w {} \;
# Thu, 07 Oct 2021 22:48:34 GMT
RUN groupadd --gid 1000 kibana && useradd --uid 1000 --gid 1000 --home-dir /usr/share/kibana --no-create-home kibana
# Thu, 07 Oct 2021 22:48:34 GMT
USER kibana
# Thu, 07 Oct 2021 22:48:35 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.vendor=Elastic org.label-schema.name=kibana org.label-schema.version=6.8.20 org.label-schema.url=https://www.elastic.co/products/kibana org.label-schema.vcs-url=https://github.com/elastic/kibana org.label-schema.license=Elastic License license=Elastic License
# Thu, 07 Oct 2021 22:48:35 GMT
CMD ["/usr/local/bin/kibana-docker"]
```

-	Layers:
	-	`sha256:2d473b07cdd5f0912cd6f1a703352c82b512407db6b05b43f2553732b55df3bc`  
		Last Modified: Sat, 14 Nov 2020 00:21:39 GMT  
		Size: 76.1 MB (76097157 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:3aec4036184cc00b3cc4c97e162986f16b1910ce99431b9e68c40332c98502d7`  
		Last Modified: Thu, 14 Oct 2021 14:13:42 GMT  
		Size: 51.9 MB (51917619 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:9f1c5a7463159abed47adedb8ae9255a1405e7fd60d795e859fcf6c6d7ffc804`  
		Last Modified: Thu, 14 Oct 2021 14:13:57 GMT  
		Size: 188.6 MB (188636343 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:deab2ff41ef31782472d1bb4718857585f1e9479992cefab3a41bc419b3fcfc1`  
		Last Modified: Thu, 14 Oct 2021 14:13:36 GMT  
		Size: 132.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:f532163f4308788c1b07dab457b0baa85351a46944ac949ab7236d2427bcd6b1`  
		Last Modified: Thu, 14 Oct 2021 14:13:34 GMT  
		Size: 395.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:3b3a625200cff796397a0aa47a896d9ea9aca3490b81dc19f8e3bed68e6cd5ab`  
		Last Modified: Thu, 14 Oct 2021 14:13:33 GMT  
		Size: 2.3 KB (2261 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:e764a856cf3626ab0ed889a4cdb77c44fe52b8b91408fe9ad213b0a835eea41d`  
		Last Modified: Thu, 14 Oct 2021 14:13:32 GMT  
		Size: 150.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:4d9bdecf088ee636baa53e1489be8ea869bd6b9cac117d10a3f6d1762810cf34`  
		Last Modified: Thu, 14 Oct 2021 14:13:31 GMT  
		Size: 1.8 KB (1817 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
