# `ibmjava:8-jre-alpine`

## Docker Metadata

- Image ID: `sha256:4db284aa5e3038cce48dc6e001182edbd761dd755ac284093be2e58c75cec910`
- Created: `2021-09-17T19:22:41.929385795Z`
- Virtual Size: ~ 213.02 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["/bin/sh"]`
- Environment:
  - `PATH=/opt/ibm/java/jre/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `JAVA_VERSION=8.0.6.36`
  - `JAVA_HOME=/opt/ibm/java/jre`
  - `IBM_JAVA_OPTIONS=-XX:+UseContainerSupport`

## `apk` (`.apk`-based packages)

### `apk` package: `alpine-baselayout`

```console
alpine-baselayout-3.2.0-r7 description:
Alpine base dir structure and init scripts

alpine-baselayout-3.2.0-r7 webpage:
https://git.alpinelinux.org/cgit/aports/tree/main/alpine-baselayout

alpine-baselayout-3.2.0-r7 installed size:
400 KiB

alpine-baselayout-3.2.0-r7 license:
GPL-2.0-only

```

### `apk` package: `alpine-keys`

```console
alpine-keys-2.2-r0 description:
Public keys for Alpine Linux packages

alpine-keys-2.2-r0 webpage:
https://alpinelinux.org

alpine-keys-2.2-r0 installed size:
104 KiB

alpine-keys-2.2-r0 license:
MIT

```

### `apk` package: `apk-tools`

```console
apk-tools-2.10.8-r0 description:
Alpine Package Keeper - package manager for alpine

apk-tools-2.10.8-r0 webpage:
https://gitlab.alpinelinux.org/alpine/apk-tools

apk-tools-2.10.8-r0 installed size:
260 KiB

apk-tools-2.10.8-r0 license:
GPL-2.0-only

```

### `apk` package: `busybox`

```console
busybox-1.31.1-r20 description:
Size optimized toolbox of many common UNIX utilities

busybox-1.31.1-r20 webpage:
https://busybox.net/

busybox-1.31.1-r20 installed size:
940 KiB

busybox-1.31.1-r20 license:
GPL-2.0-only

```

### `apk` package: `ca-certificates`

```console
ca-certificates-20191127-r4 description:
Common CA certificates PEM files from Mozilla

ca-certificates-20191127-r4 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-20191127-r4 installed size:
672 KiB

ca-certificates-20191127-r4 license:
MPL-2.0 GPL-2.0-or-later

```

### `apk` package: `ca-certificates-bundle`

```console
ca-certificates-bundle-20191127-r4 description:
Pre generated bundle of Mozilla certificates

ca-certificates-bundle-20191127-r4 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-bundle-20191127-r4 installed size:
228 KiB

ca-certificates-bundle-20191127-r4 license:
MPL-2.0 GPL-2.0-or-later

```

### `apk` package: `glibc`

```console
glibc-2.30-r0 description:
GNU C Library compatibility layer

glibc-2.30-r0 webpage:
https://github.com/sgerrand/alpine-pkg-glibc

glibc-2.30-r0 installed size:
9028 KiB

glibc-2.30-r0 license:
LGPL

```

### `apk` package: `libc-utils`

```console
libc-utils-0.7.2-r3 description:
Meta package to pull in correct libc

libc-utils-0.7.2-r3 webpage:
https://alpinelinux.org

libc-utils-0.7.2-r3 installed size:
4096 B

libc-utils-0.7.2-r3 license:
BSD-2-Clause AND BSD-3-Clause

```

### `apk` package: `libcrypto1.1`

```console
libcrypto1.1-1.1.1l-r0 description:
Crypto library from openssl

libcrypto1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libcrypto1.1-1.1.1l-r0 installed size:
2700 KiB

libcrypto1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libssl1.1`

```console
libssl1.1-1.1.1l-r0 description:
SSL shared libraries

libssl1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libssl1.1-1.1.1l-r0 installed size:
528 KiB

libssl1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libtls-standalone`

```console
libtls-standalone-2.9.1-r1 description:
libtls extricated from libressl sources

libtls-standalone-2.9.1-r1 webpage:
https://www.libressl.org/

libtls-standalone-2.9.1-r1 installed size:
108 KiB

libtls-standalone-2.9.1-r1 license:
ISC

```

### `apk` package: `musl`

```console
musl-1.1.24-r10 description:
the musl c library (libc) implementation

musl-1.1.24-r10 webpage:
https://musl.libc.org/

musl-1.1.24-r10 installed size:
600 KiB

musl-1.1.24-r10 license:
MIT

```

### `apk` package: `musl-utils`

```console
musl-utils-1.1.24-r10 description:
the musl c library (libc) implementation

musl-utils-1.1.24-r10 webpage:
https://musl.libc.org/

musl-utils-1.1.24-r10 installed size:
148 KiB

musl-utils-1.1.24-r10 license:
MIT BSD GPL2+

```

### `apk` package: `openssl`

```console
openssl-1.1.1l-r0 description:
Toolkit for Transport Layer Security (TLS)

openssl-1.1.1l-r0 webpage:
https://www.openssl.org/

openssl-1.1.1l-r0 installed size:
660 KiB

openssl-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `scanelf`

```console
scanelf-1.2.6-r0 description:
Scan ELF binaries for stuff

scanelf-1.2.6-r0 webpage:
https://wiki.gentoo.org/wiki/Hardened/PaX_Utilities

scanelf-1.2.6-r0 installed size:
92 KiB

scanelf-1.2.6-r0 license:
GPL-2.0-only

```

### `apk` package: `ssl_client`

```console
ssl_client-1.31.1-r20 description:
EXternal ssl_client for busybox wget

ssl_client-1.31.1-r20 webpage:
https://busybox.net/

ssl_client-1.31.1-r20 installed size:
28 KiB

ssl_client-1.31.1-r20 license:
GPL-2.0-only

```

### `apk` package: `zlib`

```console
zlib-1.2.11-r3 description:
A compression/decompression Library

zlib-1.2.11-r3 webpage:
https://zlib.net/

zlib-1.2.11-r3 installed size:
108 KiB

zlib-1.2.11-r3 license:
Zlib

```
