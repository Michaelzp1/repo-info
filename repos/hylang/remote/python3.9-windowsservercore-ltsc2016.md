## `hylang:python3.9-windowsservercore-ltsc2016`

```console
$ docker pull hylang@sha256:cd99ba11d4097e0256dd64f29946d7d49bf75ab4cb001f886da5de3346e3a432
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	windows version 10.0.14393.4704; amd64

### `hylang:python3.9-windowsservercore-ltsc2016` - windows version 10.0.14393.4704; amd64

```console
$ docker pull hylang@sha256:464b50deebcb5495facd7da3566f60af977cdbe182ae4f87071c2135ebae5077
```

-	Docker Version: 20.10.8
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **6.3 GB (6334848413 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3248e1884d41e481fca4bfd8705b2d26afee1637407c94c8fcf496f220804054`
-	Default Command: `["hy"]`
-	`SHELL`: `["powershell","-Command","$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]`

```dockerfile
# Sat, 19 Nov 2016 17:05:00 GMT
RUN Apply image 1607-RTM-amd64
# Wed, 06 Oct 2021 21:16:00 GMT
RUN Install update ltsc2016-amd64
# Wed, 13 Oct 2021 12:40:35 GMT
SHELL [powershell -Command $ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';]
# Wed, 13 Oct 2021 17:41:04 GMT
ENV PYTHONIOENCODING=UTF-8
# Wed, 13 Oct 2021 17:52:13 GMT
ENV PYTHON_VERSION=3.9.7
# Wed, 13 Oct 2021 17:52:14 GMT
ENV PYTHON_RELEASE=3.9.7
# Wed, 13 Oct 2021 17:54:37 GMT
RUN $url = ('https://www.python.org/ftp/python/{0}/python-{1}-amd64.exe' -f $env:PYTHON_RELEASE, $env:PYTHON_VERSION); 	Write-Host ('Downloading {0} ...' -f $url); 	[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; 	Invoke-WebRequest -Uri $url -OutFile 'python.exe'; 		Write-Host 'Installing ...'; 	$exitCode = (Start-Process python.exe -Wait -NoNewWindow -PassThru 		-ArgumentList @( 			'/quiet', 			'InstallAllUsers=1', 			'TargetDir=C:\Python', 			'PrependPath=1', 			'Shortcuts=0', 			'Include_doc=0', 			'Include_pip=0', 			'Include_test=0' 		) 	).ExitCode; 	if ($exitCode -ne 0) { 		Write-Host ('Running python installer failed with exit code: {0}' -f $exitCode); 		Get-ChildItem $env:TEMP | Sort-Object -Descending -Property LastWriteTime | Select-Object -First 1 | Get-Content; 		exit $exitCode; 	} 		$env:PATH = [Environment]::GetEnvironmentVariable('PATH', [EnvironmentVariableTarget]::Machine); 		Write-Host 'Verifying install ...'; 	Write-Host '  python --version'; python --version; 		Write-Host 'Removing ...'; 	Remove-Item python.exe -Force; 	Remove-Item $env:TEMP/Python*.log -Force; 		Write-Host 'Complete.'
# Wed, 13 Oct 2021 17:54:38 GMT
ENV PYTHON_PIP_VERSION=21.2.4
# Wed, 13 Oct 2021 17:54:39 GMT
ENV PYTHON_SETUPTOOLS_VERSION=57.5.0
# Tue, 26 Oct 2021 21:27:05 GMT
ENV PYTHON_GET_PIP_URL=https://github.com/pypa/get-pip/raw/3cb8888cc2869620f57d5d2da64da38f516078c7/public/get-pip.py
# Tue, 26 Oct 2021 21:27:06 GMT
ENV PYTHON_GET_PIP_SHA256=c518250e91a70d7b20cceb15272209a4ded2a0c263ae5776f129e0d9b5674309
# Tue, 26 Oct 2021 21:28:48 GMT
RUN Write-Host ('Downloading get-pip.py ({0}) ...' -f $env:PYTHON_GET_PIP_URL); 	[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; 	Invoke-WebRequest -Uri $env:PYTHON_GET_PIP_URL -OutFile 'get-pip.py'; 	Write-Host ('Verifying sha256 ({0}) ...' -f $env:PYTHON_GET_PIP_SHA256); 	if ((Get-FileHash 'get-pip.py' -Algorithm sha256).Hash -ne $env:PYTHON_GET_PIP_SHA256) { 		Write-Host 'FAILED!'; 		exit 1; 	}; 		Write-Host ('Installing pip=={0} ...' -f $env:PYTHON_PIP_VERSION); 	python get-pip.py 		--disable-pip-version-check 		--no-cache-dir 		('pip=={0}' -f $env:PYTHON_PIP_VERSION) 		('setuptools=={0}' -f $env:PYTHON_SETUPTOOLS_VERSION) 	; 	Remove-Item get-pip.py -Force; 		Write-Host 'Verifying pip install ...'; 	pip --version; 		Write-Host 'Complete.'
# Tue, 26 Oct 2021 21:28:49 GMT
CMD ["python"]
# Tue, 26 Oct 2021 21:50:35 GMT
ENV HY_VERSION=1.0a3
# Tue, 26 Oct 2021 21:52:01 GMT
RUN pip install --no-cache-dir ('hy == {0}' -f $env:HY_VERSION)
# Tue, 26 Oct 2021 21:52:02 GMT
CMD ["hy"]
```

-	Layers:
	-	`sha256:3889bb8d808bbae6fa5a33e07093e65c31371bcf9e4c38c21be6b9af52ad1548`  
		Size: 4.1 GB (4069985900 bytes)  
		MIME: application/vnd.docker.image.rootfs.foreign.diff.tar.gzip
	-	`sha256:0c776a8e8e3c02d360995b7fa26a3fd7c0928965795fac57b69ff07418ab07bf`  
		Size: 2.2 GB (2202780626 bytes)  
		MIME: application/vnd.docker.image.rootfs.foreign.diff.tar.gzip
	-	`sha256:6974b1bd85ba3f9ce16d86231eced43f720fed9c13411d37584dfe7193bcde60`  
		Last Modified: Wed, 13 Oct 2021 13:27:57 GMT  
		Size: 1.3 KB (1322 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:b3055c2d7cf2c2be88d289dc3ae3bf805ce85e1f7024af719a8184af8b0ffe70`  
		Last Modified: Wed, 13 Oct 2021 18:00:35 GMT  
		Size: 1.4 KB (1431 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:bbb86e34dc320f944838666c681cd4577a112935731659b4f3c34a923488c222`  
		Last Modified: Wed, 13 Oct 2021 18:04:09 GMT  
		Size: 1.4 KB (1422 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:6e361524d90e123690a8c1f722f99991dfbf189a8b0469be23e1e1e7372277dd`  
		Last Modified: Wed, 13 Oct 2021 18:04:09 GMT  
		Size: 1.4 KB (1429 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:9567b087a7367ea7ea4af09c3ababe63b384d39f5495a8e196bfd1c8d0f3526e`  
		Last Modified: Wed, 13 Oct 2021 18:05:09 GMT  
		Size: 54.4 MB (54444510 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:825ceff89f00fb9ff0ce8e67e8db9bd18bc779fedec6bfcc1e6f18df0a942e83`  
		Last Modified: Wed, 13 Oct 2021 18:04:09 GMT  
		Size: 1.4 KB (1429 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:b58dfe4ef52ae207e6682afa01f8d698e290c37a1aa40cc356f91ed8cc7be5e5`  
		Last Modified: Wed, 13 Oct 2021 18:04:06 GMT  
		Size: 1.4 KB (1434 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:dab365a5a87ce90a88d18d9685a5229e0c057c20ff677c0905a5942d245c1b60`  
		Last Modified: Tue, 26 Oct 2021 21:32:00 GMT  
		Size: 1.4 KB (1405 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:d9ae05ee975ee03d931756c58ff5768af5a1e96ee5198edd725be9c677dbaee3`  
		Last Modified: Tue, 26 Oct 2021 21:32:00 GMT  
		Size: 1.4 KB (1435 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:1b8f417b07eaa9177a101cd28b84cb3220f6923216d02a2d00c2bbde782da665`  
		Last Modified: Tue, 26 Oct 2021 21:32:06 GMT  
		Size: 6.3 MB (6306259 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:c570d218a916568b6ce9e2c2b288ca9325c604a7ba5f642aa824c64235ec6c51`  
		Last Modified: Tue, 26 Oct 2021 21:31:59 GMT  
		Size: 1.4 KB (1430 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:3c0bfc73a030db5fb9cb69f1148035096fdd33f45ae557616ac516eb23c0f36c`  
		Last Modified: Tue, 26 Oct 2021 21:52:59 GMT  
		Size: 1.4 KB (1418 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:0a4940085031dabcc77854621117e6f5d1b07b0cdb6c9b93e2b61c74235b1949`  
		Last Modified: Tue, 26 Oct 2021 21:53:01 GMT  
		Size: 1.3 MB (1315542 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:31d32e961ae420cc86ef6c375706ea7b268a29a5e06e2492875b3cc42720ddfd`  
		Last Modified: Tue, 26 Oct 2021 21:52:59 GMT  
		Size: 1.4 KB (1421 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
