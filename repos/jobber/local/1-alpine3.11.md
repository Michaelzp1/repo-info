# `jobber:1.4.4-alpine3.11`

## Docker Metadata

- Image ID: `sha256:f3b2ec9cd879ec4ae942b88182cbeb7fa419a9511a19a5bd85e09d7f8db12399`
- Created: `2021-09-01T03:01:45.738361728Z`
- Virtual Size: ~ 32.10 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["/usr/libexec/jobberrunner","-u","/var/jobber/1000/cmd.sock","/home/jobberuser/.jobber"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `USERID=1000`
  - `JOBBER_VERSION=1.4.4`
  - `JOBBER_SHA256=ec09b2efafff69c91fba2124bf28607209e1c9b50ed834ff444a9d40798aa8d3`

## `apk` (`.apk`-based packages)

### `apk` package: `alpine-baselayout`

```console
alpine-baselayout-3.2.0-r3 description:
Alpine base dir structure and init scripts

alpine-baselayout-3.2.0-r3 webpage:
https://git.alpinelinux.org/cgit/aports/tree/main/alpine-baselayout

alpine-baselayout-3.2.0-r3 installed size:
404 KiB

alpine-baselayout-3.2.0-r3 license:
GPL-2.0-only

```

### `apk` package: `alpine-keys`

```console
alpine-keys-2.1-r2 description:
Public keys for Alpine Linux packages

alpine-keys-2.1-r2 webpage:
https://alpinelinux.org

alpine-keys-2.1-r2 installed size:
96 KiB

alpine-keys-2.1-r2 license:
MIT

```

### `apk` package: `apk-tools`

```console
apk-tools-2.10.8-r0 description:
Alpine Package Keeper - package manager for alpine

apk-tools-2.10.8-r0 webpage:
https://gitlab.alpinelinux.org/alpine/apk-tools

apk-tools-2.10.8-r0 installed size:
260 KiB

apk-tools-2.10.8-r0 license:
GPL2

```

### `apk` package: `busybox`

```console
busybox-1.31.1-r10 description:
Size optimized toolbox of many common UNIX utilities

busybox-1.31.1-r10 webpage:
https://busybox.net/

busybox-1.31.1-r10 installed size:
940 KiB

busybox-1.31.1-r10 license:
GPL-2.0-only

```

### `apk` package: `ca-certificates-cacert`

```console
ca-certificates-cacert-20191127-r2 description:
Mozilla bundled certificates

ca-certificates-cacert-20191127-r2 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-cacert-20191127-r2 installed size:
240 KiB

ca-certificates-cacert-20191127-r2 license:
MPL-2.0 GPL-2.0-or-later

```

### `apk` package: `jobber`

```console
jobber-1.4.4-r0 description:
A replacement for cron, with sophisticated status-reporting and error-handling.

jobber-1.4.4-r0 webpage:
https://dshearer.github.io/jobber/

jobber-1.4.4-r0 installed size:
25 MiB

jobber-1.4.4-r0 license:
MIT

```

### `apk` package: `libc-utils`

```console
libc-utils-0.7.2-r0 description:
Meta package to pull in correct libc

libc-utils-0.7.2-r0 webpage:
http://alpinelinux.org

libc-utils-0.7.2-r0 installed size:
4096 B

libc-utils-0.7.2-r0 license:
BSD

```

### `apk` package: `libcrypto1.1`

```console
libcrypto1.1-1.1.1l-r0 description:
Crypto library from openssl

libcrypto1.1-1.1.1l-r0 webpage:
https://www.openssl.org

libcrypto1.1-1.1.1l-r0 installed size:
2700 KiB

libcrypto1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libssl1.1`

```console
libssl1.1-1.1.1l-r0 description:
SSL shared libraries

libssl1.1-1.1.1l-r0 webpage:
https://www.openssl.org

libssl1.1-1.1.1l-r0 installed size:
528 KiB

libssl1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libtls-standalone`

```console
libtls-standalone-2.9.1-r0 description:
libtls extricated from libressl sources

libtls-standalone-2.9.1-r0 webpage:
https://www.libressl.org/

libtls-standalone-2.9.1-r0 installed size:
108 KiB

libtls-standalone-2.9.1-r0 license:
ISC

```

### `apk` package: `musl`

```console
musl-1.1.24-r3 description:
the musl c library (libc) implementation

musl-1.1.24-r3 webpage:
https://musl.libc.org/

musl-1.1.24-r3 installed size:
600 KiB

musl-1.1.24-r3 license:
MIT

```

### `apk` package: `musl-utils`

```console
musl-utils-1.1.24-r3 description:
the musl c library (libc) implementation

musl-utils-1.1.24-r3 webpage:
https://musl.libc.org/

musl-utils-1.1.24-r3 installed size:
148 KiB

musl-utils-1.1.24-r3 license:
MIT BSD GPL2+

```

### `apk` package: `scanelf`

```console
scanelf-1.2.4-r0 description:
Scan ELF binaries for stuff

scanelf-1.2.4-r0 webpage:
https://wiki.gentoo.org/wiki/Hardened/PaX_Utilities

scanelf-1.2.4-r0 installed size:
92 KiB

scanelf-1.2.4-r0 license:
GPL-2.0-only

```

### `apk` package: `ssl_client`

```console
ssl_client-1.31.1-r10 description:
EXternal ssl_client for busybox wget

ssl_client-1.31.1-r10 webpage:
https://busybox.net/

ssl_client-1.31.1-r10 installed size:
28 KiB

ssl_client-1.31.1-r10 license:
GPL-2.0-only

```

### `apk` package: `zlib`

```console
zlib-1.2.11-r3 description:
A compression/decompression Library

zlib-1.2.11-r3 webpage:
https://zlib.net/

zlib-1.2.11-r3 installed size:
108 KiB

zlib-1.2.11-r3 license:
Zlib

```
