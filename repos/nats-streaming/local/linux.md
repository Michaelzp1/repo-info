# `nats-streaming:0.23.0-scratch`

## Docker Metadata

- Image ID: `sha256:8d6770bd180dcf6d53ae5677624eca3e0ab915e0534d7a8ebfb0e23dabf23c09`
- Created: `2021-10-15T23:32:47.547965508Z`
- Virtual Size: ~ 22.57 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/nats-streaming-server"]`
- Command: `["-m","8222"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
