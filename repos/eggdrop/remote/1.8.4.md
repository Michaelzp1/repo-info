## `eggdrop:1.8.4`

```console
$ docker pull eggdrop@sha256:20e9e88de9ad7aaa3c7d6beb98dbd5b2c2ed6b4863847bee489b020ec035b4c8
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `eggdrop:1.8.4` - linux; amd64

```console
$ docker pull eggdrop@sha256:3515bc8d05ff6214c16fa920bd27f66468844e616e114a44a9c5968cc810adff
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **8.8 MB (8801912 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:158d1a7aca7e5a2c0c5e721c885f80379ad74b38e0f936d81f764950730c8b58`
-	Entrypoint: `["\/home\/eggdrop\/eggdrop\/entrypoint.sh"]`
-	Default Command: `["eggdrop.conf"]`

```dockerfile
# Tue, 31 Aug 2021 23:18:31 GMT
ADD file:9d14b11183983923090d9e6d15cc51ee210466296e913bfefbfd580b3de59c95 in / 
# Tue, 31 Aug 2021 23:18:31 GMT
CMD ["/bin/sh"]
# Wed, 01 Sep 2021 00:45:26 GMT
MAINTAINER Geo Van O <geo@eggheads.org>
# Wed, 01 Sep 2021 00:45:27 GMT
RUN adduser -S eggdrop
# Wed, 01 Sep 2021 00:45:29 GMT
RUN apk add --no-cache 'su-exec>=0.2'
# Wed, 01 Sep 2021 00:47:24 GMT
RUN apk add --no-cache tcl bash openssl
# Wed, 01 Sep 2021 00:48:53 GMT
RUN apk add --no-cache --virtual egg-deps tcl-dev wget ca-certificates make tar gpgme build-base openssl-dev   && wget ftp://ftp.eggheads.org/pub/eggdrop/source/1.8/eggdrop-1.8.4.tar.gz   && wget ftp://ftp.eggheads.org/pub/eggdrop/source/1.8/eggdrop-1.8.4.tar.gz.asc   && gpg --keyserver ha.pool.sks-keyservers.net --recv-key E01C240484DE7DBE190FE141E7667DE1D1A39AFF   && gpg --batch --verify eggdrop-1.8.4.tar.gz.asc eggdrop-1.8.4.tar.gz   && command -v gpgconf > /dev/null   && gpgconf --kill all   && rm eggdrop-1.8.4.tar.gz.asc   && tar -zxvf eggdrop-1.8.4.tar.gz   && rm eggdrop-1.8.4.tar.gz   && ( cd eggdrop-1.8.4     && ./configure     && make config     && make     && make install DEST=/home/eggdrop/eggdrop )   && rm -rf eggdrop-1.8.4   && mkdir /home/eggdrop/eggdrop/data   && chown -R eggdrop /home/eggdrop/eggdrop   && apk del egg-deps
# Wed, 01 Sep 2021 00:48:54 GMT
ENV NICK=
# Wed, 01 Sep 2021 00:48:54 GMT
ENV SERVER=
# Wed, 01 Sep 2021 00:48:54 GMT
ENV LISTEN=3333
# Wed, 01 Sep 2021 00:48:55 GMT
ENV OWNER=
# Wed, 01 Sep 2021 00:48:55 GMT
ENV USERFILE=eggdrop.user
# Wed, 01 Sep 2021 00:48:55 GMT
ENV CHANFILE=eggdrop.chan
# Wed, 01 Sep 2021 00:48:56 GMT
WORKDIR /home/eggdrop/eggdrop
# Wed, 01 Sep 2021 00:48:56 GMT
EXPOSE 3333
# Wed, 01 Sep 2021 00:48:57 GMT
COPY file:f8d85155d39ecdefdd2ce710ca8c1211edaffb7c3fbbde0877da166dd3aaa579 in /home/eggdrop/eggdrop 
# Wed, 01 Sep 2021 00:48:57 GMT
COPY file:b76e92fb28997fa3fd71a3b880ff3b73567ca05021b617d51ebdcefd8c31b457 in /home/eggdrop/eggdrop/scripts/ 
# Wed, 01 Sep 2021 00:48:58 GMT
ENTRYPOINT ["/home/eggdrop/eggdrop/entrypoint.sh"]
# Wed, 01 Sep 2021 00:48:58 GMT
CMD ["eggdrop.conf"]
```

-	Layers:
	-	`sha256:6a428f9f83b0a29f1fdd2ccccca19a9bab805a925b8eddf432a5a3d3da04afbc`  
		Last Modified: Tue, 31 Aug 2021 23:19:15 GMT  
		Size: 2.8 MB (2817307 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:5f20f9fdd96b050b7674a3c80fab08aa90a03ade7cdb1605902daa948fda52a2`  
		Last Modified: Wed, 01 Sep 2021 00:50:59 GMT  
		Size: 1.3 KB (1264 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:20bd70c5c8eea46f41315a10333d6c4180d7330668d5d9a03a677e69f00dc72e`  
		Last Modified: Wed, 01 Sep 2021 00:50:56 GMT  
		Size: 9.6 KB (9603 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:a2574265c46e1bcf9b75a096f4c4ba73e414b625819aed53a53f373e11e016dc`  
		Last Modified: Wed, 01 Sep 2021 00:51:07 GMT  
		Size: 2.7 MB (2685358 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:c786a5b4be16b348ab37d1166a4e4c7d64fb1c801787034e017600ff03553208`  
		Last Modified: Wed, 01 Sep 2021 00:51:07 GMT  
		Size: 3.3 MB (3285797 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:b6f54e33975937b1852cb665d7c706121b1e18b1182d603de1f5dc8c9ed981e7`  
		Last Modified: Wed, 01 Sep 2021 00:51:06 GMT  
		Size: 1.9 KB (1880 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:9cf6541232161c3bb1d26a43c96019ab8d7689d531648c648bb4d7125d21ae39`  
		Last Modified: Wed, 01 Sep 2021 00:51:06 GMT  
		Size: 703.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
