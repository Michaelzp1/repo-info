# `arangodb:3.8`

## Docker Metadata

- Image ID: `sha256:803819368973bc9c6ddc09a77f8054a2a00e030d56cd3857866dc88ab2663773`
- Created: `2021-11-01T18:19:54.287072417Z`
- Virtual Size: ~ 428.15 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/entrypoint.sh"]`
- Command: `["arangod"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `ARANGO_VERSION=3.8.2`
  - `ARANGO_URL=https://download.arangodb.com/arangodb38/DEBIAN/amd64`
  - `ARANGO_PACKAGE=arangodb3_3.8.2-1_amd64.deb`
  - `ARANGO_PACKAGE_URL=https://download.arangodb.com/arangodb38/DEBIAN/amd64/arangodb3_3.8.2-1_amd64.deb`
  - `ARANGO_SIGNATURE_URL=https://download.arangodb.com/arangodb38/DEBIAN/amd64/arangodb3_3.8.2-1_amd64.deb.asc`
  - `GLIBCXX_FORCE_NEW=1`

## `apk` (`.apk`-based packages)

### `apk` package: `alpine-baselayout`

```console
alpine-baselayout-3.2.0-r7 description:
Alpine base dir structure and init scripts

alpine-baselayout-3.2.0-r7 webpage:
https://git.alpinelinux.org/cgit/aports/tree/main/alpine-baselayout

alpine-baselayout-3.2.0-r7 installed size:
400 KiB

alpine-baselayout-3.2.0-r7 license:
GPL-2.0-only

```

### `apk` package: `alpine-keys`

```console
alpine-keys-2.2-r0 description:
Public keys for Alpine Linux packages

alpine-keys-2.2-r0 webpage:
https://alpinelinux.org

alpine-keys-2.2-r0 installed size:
104 KiB

alpine-keys-2.2-r0 license:
MIT

```

### `apk` package: `apk-tools`

```console
apk-tools-2.10.8-r0 description:
Alpine Package Keeper - package manager for alpine

apk-tools-2.10.8-r0 webpage:
https://gitlab.alpinelinux.org/alpine/apk-tools

apk-tools-2.10.8-r0 installed size:
260 KiB

apk-tools-2.10.8-r0 license:
GPL-2.0-only

```

### `apk` package: `binutils`

```console
binutils-2.34-r2 description:
Tools necessary to build programs

binutils-2.34-r2 webpage:
https://www.gnu.org/software/binutils/

binutils-2.34-r2 installed size:
9528 KiB

binutils-2.34-r2 license:
GPL-2.0 GPL-3.0-or-later LGPL-2.0 BSD

```

### `apk` package: `brotli-libs`

```console
brotli-libs-1.0.9-r1 description:
Generic lossless compressor (libraries)

brotli-libs-1.0.9-r1 webpage:
https://github.com/google/brotli

brotli-libs-1.0.9-r1 installed size:
716 KiB

brotli-libs-1.0.9-r1 license:
MIT

```

### `apk` package: `busybox`

```console
busybox-1.31.1-r20 description:
Size optimized toolbox of many common UNIX utilities

busybox-1.31.1-r20 webpage:
https://busybox.net/

busybox-1.31.1-r20 installed size:
940 KiB

busybox-1.31.1-r20 license:
GPL-2.0-only

```

### `apk` package: `c-ares`

```console
c-ares-1.17.2-r0 description:
An asynchronously DNS/names resolver library

c-ares-1.17.2-r0 webpage:
https://c-ares.haxx.se/

c-ares-1.17.2-r0 installed size:
92 KiB

c-ares-1.17.2-r0 license:
MIT

```

### `apk` package: `ca-certificates`

```console
ca-certificates-20191127-r4 description:
Common CA certificates PEM files from Mozilla

ca-certificates-20191127-r4 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-20191127-r4 installed size:
672 KiB

ca-certificates-20191127-r4 license:
MPL-2.0 GPL-2.0-or-later

```

### `apk` package: `ca-certificates-bundle`

```console
ca-certificates-bundle-20191127-r4 description:
Pre generated bundle of Mozilla certificates

ca-certificates-bundle-20191127-r4 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-bundle-20191127-r4 installed size:
228 KiB

ca-certificates-bundle-20191127-r4 license:
MPL-2.0 GPL-2.0-or-later

```

### `apk` package: `libc-utils`

```console
libc-utils-0.7.2-r3 description:
Meta package to pull in correct libc

libc-utils-0.7.2-r3 webpage:
https://alpinelinux.org

libc-utils-0.7.2-r3 installed size:
4096 B

libc-utils-0.7.2-r3 license:
BSD-2-Clause AND BSD-3-Clause

```

### `apk` package: `libcrypto1.1`

```console
libcrypto1.1-1.1.1l-r0 description:
Crypto library from openssl

libcrypto1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libcrypto1.1-1.1.1l-r0 installed size:
2700 KiB

libcrypto1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libgcc`

```console
libgcc-9.3.0-r2 description:
GNU C compiler runtime libraries

libgcc-9.3.0-r2 webpage:
https://gcc.gnu.org

libgcc-9.3.0-r2 installed size:
88 KiB

libgcc-9.3.0-r2 license:
GPL-2.0-or-later LGPL-2.1-or-later

```

### `apk` package: `libssl1.1`

```console
libssl1.1-1.1.1l-r0 description:
SSL shared libraries

libssl1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libssl1.1-1.1.1l-r0 installed size:
528 KiB

libssl1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libstdc++`

```console
libstdc++-9.3.0-r2 description:
GNU C++ standard runtime library

libstdc++-9.3.0-r2 webpage:
https://gcc.gnu.org

libstdc++-9.3.0-r2 installed size:
1632 KiB

libstdc++-9.3.0-r2 license:
GPL-2.0-or-later LGPL-2.1-or-later

```

### `apk` package: `libtls-standalone`

```console
libtls-standalone-2.9.1-r1 description:
libtls extricated from libressl sources

libtls-standalone-2.9.1-r1 webpage:
https://www.libressl.org/

libtls-standalone-2.9.1-r1 installed size:
108 KiB

libtls-standalone-2.9.1-r1 license:
ISC

```

### `apk` package: `musl`

```console
musl-1.1.24-r10 description:
the musl c library (libc) implementation

musl-1.1.24-r10 webpage:
https://musl.libc.org/

musl-1.1.24-r10 installed size:
600 KiB

musl-1.1.24-r10 license:
MIT

```

### `apk` package: `musl-utils`

```console
musl-utils-1.1.24-r10 description:
the musl c library (libc) implementation

musl-utils-1.1.24-r10 webpage:
https://musl.libc.org/

musl-utils-1.1.24-r10 installed size:
148 KiB

musl-utils-1.1.24-r10 license:
MIT BSD GPL2+

```

### `apk` package: `nghttp2-libs`

```console
nghttp2-libs-1.41.0-r0 description:
Experimental HTTP/2 client, server and proxy (libraries)

nghttp2-libs-1.41.0-r0 webpage:
https://nghttp2.org

nghttp2-libs-1.41.0-r0 installed size:
156 KiB

nghttp2-libs-1.41.0-r0 license:
MIT

```

### `apk` package: `nodejs`

```console
nodejs-12.22.6-r0 description:
JavaScript runtime built on V8 engine - LTS version

nodejs-12.22.6-r0 webpage:
https://nodejs.org/

nodejs-12.22.6-r0 installed size:
28 MiB

nodejs-12.22.6-r0 license:
MIT

```

### `apk` package: `numactl`

```console
numactl-2.0.13-r0 description:
Simple NUMA policy support

numactl-2.0.13-r0 webpage:
https://github.com/numactl/numactl

numactl-2.0.13-r0 installed size:
60 KiB

numactl-2.0.13-r0 license:
LGPL-2.1-only

```

### `apk` package: `numactl-tools`

```console
numactl-tools-2.0.13-r0 description:
NUMA policy control tools

numactl-tools-2.0.13-r0 webpage:
https://github.com/numactl/numactl

numactl-tools-2.0.13-r0 installed size:
164 KiB

numactl-tools-2.0.13-r0 license:
GPL-2.0-only

```

### `apk` package: `pwgen`

```console
pwgen-2.08-r1 description:
A Password Generator

pwgen-2.08-r1 webpage:
https://github.com/tytso/pwgen

pwgen-2.08-r1 installed size:
44 KiB

pwgen-2.08-r1 license:
GPL-2.0-only

```

### `apk` package: `scanelf`

```console
scanelf-1.2.6-r0 description:
Scan ELF binaries for stuff

scanelf-1.2.6-r0 webpage:
https://wiki.gentoo.org/wiki/Hardened/PaX_Utilities

scanelf-1.2.6-r0 installed size:
92 KiB

scanelf-1.2.6-r0 license:
GPL-2.0-only

```

### `apk` package: `ssl_client`

```console
ssl_client-1.31.1-r20 description:
EXternal ssl_client for busybox wget

ssl_client-1.31.1-r20 webpage:
https://busybox.net/

ssl_client-1.31.1-r20 installed size:
28 KiB

ssl_client-1.31.1-r20 license:
GPL-2.0-only

```

### `apk` package: `zlib`

```console
zlib-1.2.11-r3 description:
A compression/decompression Library

zlib-1.2.11-r3 webpage:
https://zlib.net/

zlib-1.2.11-r3 installed size:
108 KiB

zlib-1.2.11-r3 license:
Zlib

```
