## `sl:7`

```console
$ docker pull sl@sha256:ec1a422bbe07b5de95ce84f01cdd425fbe96a56cf28025d87611b1ba716d9d8d
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `sl:7` - linux; amd64

```console
$ docker pull sl@sha256:d126700a45b6ec063767b29724446249183fd33314f390770c7d47b8bfae81e5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **70.4 MB (70413744 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:01e86928dc7dc6cee76df7f7e8949ab300aee24b7a803890b77d480ed934f1b3`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 03 Nov 2021 19:43:26 GMT
ADD file:9e989684e6936cae1d477e917812617cd48fd0c7a58cf6d8d1c2d58a7ad93b10 in / 
# Wed, 03 Nov 2021 19:43:26 GMT
LABEL name=SL7 Base Image vendor=Scientific Linux build-date=20211103
# Wed, 03 Nov 2021 19:43:26 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:0a331b6fa8322fe6fd7280931539552260e7422d8320aa5d96600084bd9734d7`  
		Last Modified: Wed, 03 Nov 2021 19:43:46 GMT  
		Size: 70.4 MB (70413744 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
