## `fedora:33`

```console
$ docker pull fedora@sha256:3bbf787afce2a275a8a0d688eaaaf908c74b8d4e09334563d2c3307c54fec8c5
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; ppc64le
	-	linux; s390x

### `fedora:33` - linux; amd64

```console
$ docker pull fedora@sha256:9ff4c42c4178edb8aa5c07ad3e51c567f920e33f31e0a427702dfdc3af7ac921
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **61.9 MB (61925742 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a5465556eeb262e1517f15d0dd45a68a90c5fabf4fb71a6946f2214a0d940122`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 01 Apr 2021 17:59:37 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Thu, 01 Apr 2021 18:00:06 GMT
ENV DISTTAG=f33container FGC=f33 FBR=f33
# Fri, 01 Oct 2021 23:02:03 GMT
ADD file:d4f751e58150d6e40170313e4a4d2b679614887567bd7711a43fb591d7a0626b in / 
# Fri, 01 Oct 2021 23:02:04 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:1309f2859882d544fe157d16e1a6dcb2b5fce9c042f3258549dba724a5d2ca9c`  
		Last Modified: Fri, 01 Oct 2021 23:03:22 GMT  
		Size: 61.9 MB (61925742 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:33` - linux; arm variant v7

```console
$ docker pull fedora@sha256:5d21b4f28472d8ecfc1f1dca05adf98f42e3933442e3c411377064854bb3d48d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **58.4 MB (58430017 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:472a4e536aa561b790cd764df808d0ae7fc807f40a1e5f21fb9f1ef98ff54759`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 23 Jul 2021 19:03:21 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Fri, 23 Jul 2021 19:03:56 GMT
ENV DISTTAG=f33container FGC=f33 FBR=f33
# Sat, 02 Oct 2021 18:48:18 GMT
ADD file:7fdfa0765b715e4bdee41b6f6725aa71ce94f91e0367c647cbdb7385a9abb18e in / 
# Sat, 02 Oct 2021 18:48:19 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:1484d766ab3920d272ef03c20c7ed04615ad4793a03964a52521108c81ede8ac`  
		Last Modified: Sat, 02 Oct 2021 18:51:37 GMT  
		Size: 58.4 MB (58430017 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:33` - linux; arm64 variant v8

```console
$ docker pull fedora@sha256:5a928b91a63932222d7b0d36dfe50de40d9abe5443b176e7518f93074493f77e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **61.7 MB (61721408 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:861e59d6505f6fecea8d13a1423a205a44f8654d99b113731cb2e93c94960ce2`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Tue, 02 Nov 2021 21:12:33 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Tue, 02 Nov 2021 21:12:34 GMT
ENV DISTTAG=f33container FGC=f33 FBR=f33
# Tue, 02 Nov 2021 21:12:39 GMT
ADD file:00b7e028586c400aca1a550d7e6d4cc31d2a960a637185c455a02bbbfa63e4d8 in / 
# Tue, 02 Nov 2021 21:12:40 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:be573086c244344a87f98bcbc05239ec44dbfd9495510c5e7e93fcae47e7dadd`  
		Last Modified: Fri, 01 Oct 2021 23:29:50 GMT  
		Size: 61.7 MB (61721408 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:33` - linux; ppc64le

```console
$ docker pull fedora@sha256:6c543a6d0c2b4dd4f6f8b69e753f68ad9a93b00de24c51bf1a23407cee51eeb6
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **67.5 MB (67518901 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:71fdf22ead60b408eb173cdffe8fac5d629fae2a8d8350409dfab40df2b7074b`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 23 Jul 2021 23:32:50 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Fri, 23 Jul 2021 23:34:07 GMT
ENV DISTTAG=f33container FGC=f33 FBR=f33
# Mon, 04 Oct 2021 19:39:01 GMT
ADD file:c2c874297e66359cbcadd727ca2a43f52b54c5a737126aa1effcd8e314d495b1 in / 
# Mon, 04 Oct 2021 19:39:13 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:44f60760a0145aef3fc54686500ce857bcb25877aaac22a36c00c3d8adaa350a`  
		Last Modified: Mon, 04 Oct 2021 19:41:54 GMT  
		Size: 67.5 MB (67518901 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:33` - linux; s390x

```console
$ docker pull fedora@sha256:3776a2e7e350a7b56ea9c5bd63c71180a33c5abaaf4ce23958d49ad84452f17f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **63.5 MB (63492747 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:84b49f84bfe6fe5663b5179604778c8053750e5cafd36ef7c5b4c4b71d110dd8`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 23 Jul 2021 02:30:24 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Fri, 23 Jul 2021 02:30:42 GMT
ENV DISTTAG=f33container FGC=f33 FBR=f33
# Fri, 01 Oct 2021 22:41:52 GMT
ADD file:897c9d1e3342834f7aba99f2657886ae331d647bccb776e97205dbfdca171b38 in / 
# Fri, 01 Oct 2021 22:42:00 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:1e98204d8c4e9e0388320408f91921c254172c9c71425ad22ab0696974d7964f`  
		Last Modified: Fri, 01 Oct 2021 22:43:15 GMT  
		Size: 63.5 MB (63492747 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
