# `fedora:35`

## Docker Metadata

- Image ID: `sha256:b080de8a4da3da9ec2fdbeeb6d70d94079b27df87483039fc45abd2152109c43`
- Created: `2021-11-02T21:29:22.547065293Z`
- Virtual Size: ~ 153.24 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["/bin/bash"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `DISTTAG=f35container`
  - `FGC=f35`
  - `FBR=f35`
- Labels:
  - `maintainer=Clement Verna <cverna@fedoraproject.org>`
