# `fedora:rawhide`

## Docker Metadata

- Image ID: `sha256:1cf0d0c364a631bc7dd12c8a1f59d01e4ccca6ffece5bd504e1567d057de92e6`
- Created: `2021-11-02T21:29:33.95972478Z`
- Virtual Size: ~ 160.28 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["/bin/bash"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `DISTTAG=fRawhidecontainer`
  - `FGC=fRawhide`
  - `FBR=fRawhide`
- Labels:
  - `maintainer=Clement Verna <cverna@fedoraproject.org>`
