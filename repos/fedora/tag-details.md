<!-- THIS FILE IS GENERATED VIA './update-remote.sh' -->

# Tags of `fedora`

-	[`fedora:33`](#fedora33)
-	[`fedora:34`](#fedora34)
-	[`fedora:35`](#fedora35)
-	[`fedora:36`](#fedora36)
-	[`fedora:latest`](#fedoralatest)
-	[`fedora:rawhide`](#fedorarawhide)

## `fedora:33`

```console
$ docker pull fedora@sha256:3bbf787afce2a275a8a0d688eaaaf908c74b8d4e09334563d2c3307c54fec8c5
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; ppc64le
	-	linux; s390x

### `fedora:33` - linux; amd64

```console
$ docker pull fedora@sha256:9ff4c42c4178edb8aa5c07ad3e51c567f920e33f31e0a427702dfdc3af7ac921
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **61.9 MB (61925742 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a5465556eeb262e1517f15d0dd45a68a90c5fabf4fb71a6946f2214a0d940122`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 01 Apr 2021 17:59:37 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Thu, 01 Apr 2021 18:00:06 GMT
ENV DISTTAG=f33container FGC=f33 FBR=f33
# Fri, 01 Oct 2021 23:02:03 GMT
ADD file:d4f751e58150d6e40170313e4a4d2b679614887567bd7711a43fb591d7a0626b in / 
# Fri, 01 Oct 2021 23:02:04 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:1309f2859882d544fe157d16e1a6dcb2b5fce9c042f3258549dba724a5d2ca9c`  
		Last Modified: Fri, 01 Oct 2021 23:03:22 GMT  
		Size: 61.9 MB (61925742 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:33` - linux; arm variant v7

```console
$ docker pull fedora@sha256:5d21b4f28472d8ecfc1f1dca05adf98f42e3933442e3c411377064854bb3d48d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **58.4 MB (58430017 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:472a4e536aa561b790cd764df808d0ae7fc807f40a1e5f21fb9f1ef98ff54759`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 23 Jul 2021 19:03:21 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Fri, 23 Jul 2021 19:03:56 GMT
ENV DISTTAG=f33container FGC=f33 FBR=f33
# Sat, 02 Oct 2021 18:48:18 GMT
ADD file:7fdfa0765b715e4bdee41b6f6725aa71ce94f91e0367c647cbdb7385a9abb18e in / 
# Sat, 02 Oct 2021 18:48:19 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:1484d766ab3920d272ef03c20c7ed04615ad4793a03964a52521108c81ede8ac`  
		Last Modified: Sat, 02 Oct 2021 18:51:37 GMT  
		Size: 58.4 MB (58430017 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:33` - linux; arm64 variant v8

```console
$ docker pull fedora@sha256:5a928b91a63932222d7b0d36dfe50de40d9abe5443b176e7518f93074493f77e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **61.7 MB (61721408 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:861e59d6505f6fecea8d13a1423a205a44f8654d99b113731cb2e93c94960ce2`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Tue, 02 Nov 2021 21:12:33 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Tue, 02 Nov 2021 21:12:34 GMT
ENV DISTTAG=f33container FGC=f33 FBR=f33
# Tue, 02 Nov 2021 21:12:39 GMT
ADD file:00b7e028586c400aca1a550d7e6d4cc31d2a960a637185c455a02bbbfa63e4d8 in / 
# Tue, 02 Nov 2021 21:12:40 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:be573086c244344a87f98bcbc05239ec44dbfd9495510c5e7e93fcae47e7dadd`  
		Last Modified: Fri, 01 Oct 2021 23:29:50 GMT  
		Size: 61.7 MB (61721408 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:33` - linux; ppc64le

```console
$ docker pull fedora@sha256:6c543a6d0c2b4dd4f6f8b69e753f68ad9a93b00de24c51bf1a23407cee51eeb6
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **67.5 MB (67518901 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:71fdf22ead60b408eb173cdffe8fac5d629fae2a8d8350409dfab40df2b7074b`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 23 Jul 2021 23:32:50 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Fri, 23 Jul 2021 23:34:07 GMT
ENV DISTTAG=f33container FGC=f33 FBR=f33
# Mon, 04 Oct 2021 19:39:01 GMT
ADD file:c2c874297e66359cbcadd727ca2a43f52b54c5a737126aa1effcd8e314d495b1 in / 
# Mon, 04 Oct 2021 19:39:13 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:44f60760a0145aef3fc54686500ce857bcb25877aaac22a36c00c3d8adaa350a`  
		Last Modified: Mon, 04 Oct 2021 19:41:54 GMT  
		Size: 67.5 MB (67518901 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:33` - linux; s390x

```console
$ docker pull fedora@sha256:3776a2e7e350a7b56ea9c5bd63c71180a33c5abaaf4ce23958d49ad84452f17f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **63.5 MB (63492747 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:84b49f84bfe6fe5663b5179604778c8053750e5cafd36ef7c5b4c4b71d110dd8`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 23 Jul 2021 02:30:24 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Fri, 23 Jul 2021 02:30:42 GMT
ENV DISTTAG=f33container FGC=f33 FBR=f33
# Fri, 01 Oct 2021 22:41:52 GMT
ADD file:897c9d1e3342834f7aba99f2657886ae331d647bccb776e97205dbfdca171b38 in / 
# Fri, 01 Oct 2021 22:42:00 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:1e98204d8c4e9e0388320408f91921c254172c9c71425ad22ab0696974d7964f`  
		Last Modified: Fri, 01 Oct 2021 22:43:15 GMT  
		Size: 63.5 MB (63492747 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `fedora:34`

```console
$ docker pull fedora@sha256:46b1d53ec8e6420626eb7745b262eea428f559e2f4a62601b784a9647d662c2e
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; ppc64le
	-	linux; s390x

### `fedora:34` - linux; amd64

```console
$ docker pull fedora@sha256:298551249a2b7f8aff696cde077030ace4f2c79b9eb0dc95e8584a4ac2343409
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **64.8 MB (64810132 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8598024f1676f04e46611f1458cedf28e4ee03d6c1e92b538369284cb30c72ef`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 01 Apr 2021 17:59:37 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Thu, 01 Apr 2021 18:00:19 GMT
ENV DISTTAG=f34container FGC=f34 FBR=f34
# Tue, 02 Nov 2021 21:29:08 GMT
ADD file:f99942c1d5b60568fe8fe2de3009b4c088f081829aed07d407b2c35f10bb0793 in / 
# Tue, 02 Nov 2021 21:29:09 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:6a71e0ba93050177c71848f88d73990f7cca62b2c9269189c2528838f2036192`  
		Last Modified: Tue, 02 Nov 2021 21:29:55 GMT  
		Size: 64.8 MB (64810132 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:34` - linux; arm variant v7

```console
$ docker pull fedora@sha256:1b216142a07bdc70e202ed5e1829a1693dcf57c7cd9df1ea3da55b409c2b0999
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **61.2 MB (61214402 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:59b31180ef1ee7e70789643df1add2f0b917239e031d345c8d840ed767cd5b40`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 23 Jul 2021 19:03:21 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Fri, 23 Jul 2021 19:04:30 GMT
ENV DISTTAG=f34container FGC=f34 FBR=f34
# Tue, 02 Nov 2021 22:55:37 GMT
ADD file:5a722788581957a01260cc5521e0f9850a4786162df3f665d57895154261045b in / 
# Tue, 02 Nov 2021 22:55:38 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:3ce7588d0323053e87efc2e2336c4696eb5ff2abc9b01d179145e1370550daa3`  
		Last Modified: Tue, 02 Nov 2021 22:57:59 GMT  
		Size: 61.2 MB (61214402 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:34` - linux; arm64 variant v8

```console
$ docker pull fedora@sha256:a259d66ab98b9c4f904aa8fac015ebd5beb1aaab4603c8520df2aec18806dca3
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **64.5 MB (64509259 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:00ae57b4c403420ab935c5555ff6c3a7007505e583d83a33f624b1d1d7e79b7b`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Tue, 02 Nov 2021 21:12:33 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Tue, 02 Nov 2021 21:12:46 GMT
ENV DISTTAG=f34container FGC=f34 FBR=f34
# Tue, 02 Nov 2021 21:12:51 GMT
ADD file:eac4b442d0ba1bf66a5569ca0020a9e6a6307a4bc3e35b667abeb38b6c70944f in / 
# Tue, 02 Nov 2021 21:12:52 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:fb777518fefc5a8e284f93de0f1bf730b35026e31cf4a86f0f7ddc20dd2a8d38`  
		Last Modified: Tue, 02 Nov 2021 21:14:00 GMT  
		Size: 64.5 MB (64509259 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:34` - linux; ppc64le

```console
$ docker pull fedora@sha256:b6d2107cd7df04936b788abc3a4693a6225e8bfb5aedab32443e2a74abfa0669
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **70.6 MB (70584229 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a5893964cfb16778672eb374fa585d7a1b7eb8515e7f0eaaf07d52a33cb7caa2`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 23 Jul 2021 23:32:50 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Fri, 23 Jul 2021 23:34:39 GMT
ENV DISTTAG=f34container FGC=f34 FBR=f34
# Tue, 02 Nov 2021 22:46:04 GMT
ADD file:0a5f1899223a98871c9143c71515c26f6da825712b4efc2d1977116d8b2b9c13 in / 
# Tue, 02 Nov 2021 22:46:10 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:7b1b2f3e394e2a3d59c2bbc0619c9e12a5224d57e92e0ad3b2f824ba7a8db95c`  
		Last Modified: Tue, 02 Nov 2021 22:47:31 GMT  
		Size: 70.6 MB (70584229 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:34` - linux; s390x

```console
$ docker pull fedora@sha256:87e5516ce5af0cb1dc648f605eb226b95e249db7f1ce206373b7e5b6be4a8cff
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **62.4 MB (62361216 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8394c40c7247d6d74dd56906c10fbb9b63ac69f2457efc9e17cc1584b2242849`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 23 Jul 2021 02:30:24 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Fri, 23 Jul 2021 02:30:59 GMT
ENV DISTTAG=f34container FGC=f34 FBR=f34
# Tue, 02 Nov 2021 21:28:03 GMT
ADD file:3f5bd4755b72d34e60e8a0bbc4536ba04e2f5cb40f7b9ca233bcbb5c3c41f92a in / 
# Tue, 02 Nov 2021 21:28:14 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:0116786a6420db43c23228c4abb8301f3767840c833d1d47b2783e368e60a430`  
		Last Modified: Tue, 02 Nov 2021 21:29:53 GMT  
		Size: 62.4 MB (62361216 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `fedora:35`

```console
$ docker pull fedora@sha256:72c6c48a902baff1ab9948558556ef59e3429c65697287791be3c709738955b3
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; ppc64le
	-	linux; s390x

### `fedora:35` - linux; amd64

```console
$ docker pull fedora@sha256:db8380e69336e37a94bacad856abfde03cd7060f53530be1c7de32526b00dfd0
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.8 MB (54779887 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b080de8a4da3da9ec2fdbeeb6d70d94079b27df87483039fc45abd2152109c43`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 01 Apr 2021 17:59:37 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Fri, 01 Oct 2021 23:02:34 GMT
ENV DISTTAG=f35container FGC=f35 FBR=f35
# Tue, 02 Nov 2021 21:29:21 GMT
ADD file:be45b9d0e66aaff43304ea2300127b0027e748b263fa26d203cd7012eddf7d19 in / 
# Tue, 02 Nov 2021 21:29:22 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:fc811dadee2400b171b0e1eed1d973c4aa9459c6f81c77ce11c014a6104ae005`  
		Last Modified: Tue, 02 Nov 2021 21:30:16 GMT  
		Size: 54.8 MB (54779887 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:35` - linux; arm variant v7

```console
$ docker pull fedora@sha256:e82204d97e6f84155f20a6644864f2a30a8ac0193d00eb31e5427b1ff11bc043
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **51.5 MB (51458022 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:56216e9f4d1dc523252090c1fa10428885af2b1533ad1fb0c008ed42411e6f28`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 23 Jul 2021 19:03:21 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Sat, 02 Oct 2021 18:49:21 GMT
ENV DISTTAG=f35container FGC=f35 FBR=f35
# Tue, 02 Nov 2021 22:56:07 GMT
ADD file:b6bb687b83a6557c55635cc922854e306b9d106bdf59fe4af2e48e7f085fc7bd in / 
# Tue, 02 Nov 2021 22:56:08 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:c4392c845f7cef31de93d83b4b2d94d7a31e4a65d445f2b2b1c18a44022aa194`  
		Last Modified: Tue, 02 Nov 2021 22:58:41 GMT  
		Size: 51.5 MB (51458022 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:35` - linux; arm64 variant v8

```console
$ docker pull fedora@sha256:eb6934c99216962a8860abc8cc38a5dbe26a3a823ca6a646886d02bbb7c90252
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.8 MB (53779450 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:21f7c65055e723f9afeff807f84236a391f9451c2957bb960e79704fbb70b0e2`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Tue, 02 Nov 2021 21:12:33 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Tue, 02 Nov 2021 21:12:58 GMT
ENV DISTTAG=f35container FGC=f35 FBR=f35
# Tue, 02 Nov 2021 21:13:02 GMT
ADD file:f692703c83a7a1b73f3b8481470a706e384ae7e385652a5ffbe604b338f1255a in / 
# Tue, 02 Nov 2021 21:13:03 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:55bbeef7e282fa3909a3d777b6c2bd2c965627de4d537994838627d5508ea328`  
		Last Modified: Tue, 02 Nov 2021 21:14:16 GMT  
		Size: 53.8 MB (53779450 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:35` - linux; ppc64le

```console
$ docker pull fedora@sha256:f8ea92b303c6138d78f9bf54fdf6dd2495b9d0dc4073118a38038522f809cc7b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **59.6 MB (59575956 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3ed8a45706d361309316c3078e65e37cab04801078e93a964902635e34be002d`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 23 Jul 2021 23:32:50 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Mon, 04 Oct 2021 19:40:14 GMT
ENV DISTTAG=f35container FGC=f35 FBR=f35
# Tue, 02 Nov 2021 22:46:26 GMT
ADD file:674acdcc535fa5adfec9f763c10e6d300ef041ac207d96ef9da09d1289837415 in / 
# Tue, 02 Nov 2021 22:46:33 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:aa1933de26af74dd559b4acc8c51450452147d8c8208f0badd081fce540c9332`  
		Last Modified: Tue, 02 Nov 2021 22:47:51 GMT  
		Size: 59.6 MB (59575956 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:35` - linux; s390x

```console
$ docker pull fedora@sha256:6d0c2a9d93b142fc01b21e21af644be95b7e6bb5cc0ccbb0382de787e99a26b4
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **52.7 MB (52727110 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a4b4a9b57271af591022ae0dd8f97e3d68a2d61c89f28a5c9a0d9395c3332033`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 23 Jul 2021 02:30:24 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Fri, 01 Oct 2021 22:42:23 GMT
ENV DISTTAG=f35container FGC=f35 FBR=f35
# Tue, 02 Nov 2021 21:28:36 GMT
ADD file:21c28fd1d13df6b1e0bb57f883f8351c467ea20e05e8d36d1cc046173040c3ce in / 
# Tue, 02 Nov 2021 21:28:45 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:4ff0313afc065432177e40ce9e640fb0ded68d6be98fdd233a8cec88e2b42b69`  
		Last Modified: Tue, 02 Nov 2021 21:30:06 GMT  
		Size: 52.7 MB (52727110 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `fedora:36`

```console
$ docker pull fedora@sha256:8f826d2b4322fe33d89a49ce21889daf05c5f67e0b0f06c1057d8b021b2021c8
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; ppc64le
	-	linux; s390x

### `fedora:36` - linux; amd64

```console
$ docker pull fedora@sha256:f7bb300bcb576e73dde5a246d2b19526d43960d1f7eb47c73417a240b7ed35bc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **57.7 MB (57706756 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1cf0d0c364a631bc7dd12c8a1f59d01e4ccca6ffece5bd504e1567d057de92e6`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 01 Apr 2021 17:59:37 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Thu, 01 Apr 2021 18:00:32 GMT
ENV DISTTAG=fRawhidecontainer FGC=fRawhide FBR=fRawhide
# Tue, 02 Nov 2021 21:29:33 GMT
ADD file:acf26dfcac681503f9e26a37bb5b068a4759dc0ab17b5f1050593f335314fe36 in / 
# Tue, 02 Nov 2021 21:29:33 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:a029976c022b471b42be8f08d0335b0464dcd02cac8f67209c40ac6e341ab301`  
		Last Modified: Tue, 02 Nov 2021 21:30:34 GMT  
		Size: 57.7 MB (57706756 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:36` - linux; arm variant v7

```console
$ docker pull fedora@sha256:b3fd24fd2c83692d44c3a1875277f975b9dc8e77729129679903fd53d7992e9d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.9 MB (53895689 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:62cded2320283ab500b22a46a9fbb927abcdd4a563b00439f58a515d6453ccd5`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 23 Jul 2021 19:03:21 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Fri, 23 Jul 2021 19:05:03 GMT
ENV DISTTAG=fRawhidecontainer FGC=fRawhide FBR=fRawhide
# Tue, 02 Nov 2021 22:56:37 GMT
ADD file:f63043a615d0735e7e2c2bb327ee859b139ee786498cf77946621b0d266f224a in / 
# Tue, 02 Nov 2021 22:56:39 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:9f1cd5a19a036e3a24f897dba2456ee68bf2f113bd402d4a32eab45e2583deb9`  
		Last Modified: Tue, 02 Nov 2021 22:59:28 GMT  
		Size: 53.9 MB (53895689 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:36` - linux; arm64 variant v8

```console
$ docker pull fedora@sha256:e68bc50be57ad40bdea883ab3258ef54d31db8f040f566fb94d7c33b2dbbdbee
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **56.5 MB (56500761 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:7f231a68ffe80ee9627d48fd6d094a60ecce03b963e8e8e913b1a8232820a0c1`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Tue, 02 Nov 2021 21:12:33 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Tue, 02 Nov 2021 21:13:09 GMT
ENV DISTTAG=fRawhidecontainer FGC=fRawhide FBR=fRawhide
# Tue, 02 Nov 2021 21:13:13 GMT
ADD file:604e2ef3b8dc32d11da1cea3535b660bc595a1edb5f2c1086044bad452f95173 in / 
# Tue, 02 Nov 2021 21:13:15 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:761f7e0d8b44b3ccf888adc5f0c06915c28cd89cfd49738aa57bd74c6b80fca4`  
		Last Modified: Tue, 02 Nov 2021 21:14:36 GMT  
		Size: 56.5 MB (56500761 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:36` - linux; ppc64le

```console
$ docker pull fedora@sha256:c8a8978ab0cd7179f7939b0b90c819c790177d04af9da87c0be8a3f1eb8b6455
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **62.9 MB (62936147 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:0ad394517791196c73aa8e8409673c0a0edb9fd169c031e46d66832f01fec1b8`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 23 Jul 2021 23:32:50 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Mon, 04 Oct 2021 19:40:57 GMT
ENV DISTTAG=fRawhidecontainer FGC=fRawhide FBR=fRawhide
# Tue, 02 Nov 2021 22:46:52 GMT
ADD file:4588be93eea957e400eab1c1cdc36dc31eb37f882edb87f2c6e83c4fd5025293 in / 
# Tue, 02 Nov 2021 22:46:58 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:99ad4baf4202d7be14137713425709d4866cb140fb8757e5ba4c49fc1ea36642`  
		Last Modified: Tue, 02 Nov 2021 22:48:15 GMT  
		Size: 62.9 MB (62936147 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:36` - linux; s390x

```console
$ docker pull fedora@sha256:afafc77b4632c6750bfffaa099fc8e753b2a38e0496922956e13020cd937203e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.2 MB (55158170 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f4825a88388dd83c71d7ebe46b8bbcd412d59c160072145869ca74f23541b3a2`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 23 Jul 2021 02:30:24 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Fri, 01 Oct 2021 22:42:38 GMT
ENV DISTTAG=fRawhidecontainer FGC=fRawhide FBR=fRawhide
# Tue, 02 Nov 2021 21:29:08 GMT
ADD file:8a77a81970b6a702c1b54fad57fad996bfa087b741913c5e6497f56f8e4189cb in / 
# Tue, 02 Nov 2021 21:29:18 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:e90ab5cf9c8733234d970ca9a967d31286943996a9eb293f507785641cc7e149`  
		Last Modified: Tue, 02 Nov 2021 21:30:21 GMT  
		Size: 55.2 MB (55158170 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `fedora:latest`

```console
$ docker pull fedora@sha256:72c6c48a902baff1ab9948558556ef59e3429c65697287791be3c709738955b3
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; ppc64le
	-	linux; s390x

### `fedora:latest` - linux; amd64

```console
$ docker pull fedora@sha256:db8380e69336e37a94bacad856abfde03cd7060f53530be1c7de32526b00dfd0
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.8 MB (54779887 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b080de8a4da3da9ec2fdbeeb6d70d94079b27df87483039fc45abd2152109c43`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 01 Apr 2021 17:59:37 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Fri, 01 Oct 2021 23:02:34 GMT
ENV DISTTAG=f35container FGC=f35 FBR=f35
# Tue, 02 Nov 2021 21:29:21 GMT
ADD file:be45b9d0e66aaff43304ea2300127b0027e748b263fa26d203cd7012eddf7d19 in / 
# Tue, 02 Nov 2021 21:29:22 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:fc811dadee2400b171b0e1eed1d973c4aa9459c6f81c77ce11c014a6104ae005`  
		Last Modified: Tue, 02 Nov 2021 21:30:16 GMT  
		Size: 54.8 MB (54779887 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:latest` - linux; arm variant v7

```console
$ docker pull fedora@sha256:e82204d97e6f84155f20a6644864f2a30a8ac0193d00eb31e5427b1ff11bc043
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **51.5 MB (51458022 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:56216e9f4d1dc523252090c1fa10428885af2b1533ad1fb0c008ed42411e6f28`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 23 Jul 2021 19:03:21 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Sat, 02 Oct 2021 18:49:21 GMT
ENV DISTTAG=f35container FGC=f35 FBR=f35
# Tue, 02 Nov 2021 22:56:07 GMT
ADD file:b6bb687b83a6557c55635cc922854e306b9d106bdf59fe4af2e48e7f085fc7bd in / 
# Tue, 02 Nov 2021 22:56:08 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:c4392c845f7cef31de93d83b4b2d94d7a31e4a65d445f2b2b1c18a44022aa194`  
		Last Modified: Tue, 02 Nov 2021 22:58:41 GMT  
		Size: 51.5 MB (51458022 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:latest` - linux; arm64 variant v8

```console
$ docker pull fedora@sha256:eb6934c99216962a8860abc8cc38a5dbe26a3a823ca6a646886d02bbb7c90252
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.8 MB (53779450 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:21f7c65055e723f9afeff807f84236a391f9451c2957bb960e79704fbb70b0e2`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Tue, 02 Nov 2021 21:12:33 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Tue, 02 Nov 2021 21:12:58 GMT
ENV DISTTAG=f35container FGC=f35 FBR=f35
# Tue, 02 Nov 2021 21:13:02 GMT
ADD file:f692703c83a7a1b73f3b8481470a706e384ae7e385652a5ffbe604b338f1255a in / 
# Tue, 02 Nov 2021 21:13:03 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:55bbeef7e282fa3909a3d777b6c2bd2c965627de4d537994838627d5508ea328`  
		Last Modified: Tue, 02 Nov 2021 21:14:16 GMT  
		Size: 53.8 MB (53779450 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:latest` - linux; ppc64le

```console
$ docker pull fedora@sha256:f8ea92b303c6138d78f9bf54fdf6dd2495b9d0dc4073118a38038522f809cc7b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **59.6 MB (59575956 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3ed8a45706d361309316c3078e65e37cab04801078e93a964902635e34be002d`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 23 Jul 2021 23:32:50 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Mon, 04 Oct 2021 19:40:14 GMT
ENV DISTTAG=f35container FGC=f35 FBR=f35
# Tue, 02 Nov 2021 22:46:26 GMT
ADD file:674acdcc535fa5adfec9f763c10e6d300ef041ac207d96ef9da09d1289837415 in / 
# Tue, 02 Nov 2021 22:46:33 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:aa1933de26af74dd559b4acc8c51450452147d8c8208f0badd081fce540c9332`  
		Last Modified: Tue, 02 Nov 2021 22:47:51 GMT  
		Size: 59.6 MB (59575956 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:latest` - linux; s390x

```console
$ docker pull fedora@sha256:6d0c2a9d93b142fc01b21e21af644be95b7e6bb5cc0ccbb0382de787e99a26b4
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **52.7 MB (52727110 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a4b4a9b57271af591022ae0dd8f97e3d68a2d61c89f28a5c9a0d9395c3332033`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 23 Jul 2021 02:30:24 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Fri, 01 Oct 2021 22:42:23 GMT
ENV DISTTAG=f35container FGC=f35 FBR=f35
# Tue, 02 Nov 2021 21:28:36 GMT
ADD file:21c28fd1d13df6b1e0bb57f883f8351c467ea20e05e8d36d1cc046173040c3ce in / 
# Tue, 02 Nov 2021 21:28:45 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:4ff0313afc065432177e40ce9e640fb0ded68d6be98fdd233a8cec88e2b42b69`  
		Last Modified: Tue, 02 Nov 2021 21:30:06 GMT  
		Size: 52.7 MB (52727110 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `fedora:rawhide`

```console
$ docker pull fedora@sha256:8f826d2b4322fe33d89a49ce21889daf05c5f67e0b0f06c1057d8b021b2021c8
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; ppc64le
	-	linux; s390x

### `fedora:rawhide` - linux; amd64

```console
$ docker pull fedora@sha256:f7bb300bcb576e73dde5a246d2b19526d43960d1f7eb47c73417a240b7ed35bc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **57.7 MB (57706756 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1cf0d0c364a631bc7dd12c8a1f59d01e4ccca6ffece5bd504e1567d057de92e6`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 01 Apr 2021 17:59:37 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Thu, 01 Apr 2021 18:00:32 GMT
ENV DISTTAG=fRawhidecontainer FGC=fRawhide FBR=fRawhide
# Tue, 02 Nov 2021 21:29:33 GMT
ADD file:acf26dfcac681503f9e26a37bb5b068a4759dc0ab17b5f1050593f335314fe36 in / 
# Tue, 02 Nov 2021 21:29:33 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:a029976c022b471b42be8f08d0335b0464dcd02cac8f67209c40ac6e341ab301`  
		Last Modified: Tue, 02 Nov 2021 21:30:34 GMT  
		Size: 57.7 MB (57706756 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:rawhide` - linux; arm variant v7

```console
$ docker pull fedora@sha256:b3fd24fd2c83692d44c3a1875277f975b9dc8e77729129679903fd53d7992e9d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.9 MB (53895689 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:62cded2320283ab500b22a46a9fbb927abcdd4a563b00439f58a515d6453ccd5`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 23 Jul 2021 19:03:21 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Fri, 23 Jul 2021 19:05:03 GMT
ENV DISTTAG=fRawhidecontainer FGC=fRawhide FBR=fRawhide
# Tue, 02 Nov 2021 22:56:37 GMT
ADD file:f63043a615d0735e7e2c2bb327ee859b139ee786498cf77946621b0d266f224a in / 
# Tue, 02 Nov 2021 22:56:39 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:9f1cd5a19a036e3a24f897dba2456ee68bf2f113bd402d4a32eab45e2583deb9`  
		Last Modified: Tue, 02 Nov 2021 22:59:28 GMT  
		Size: 53.9 MB (53895689 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:rawhide` - linux; arm64 variant v8

```console
$ docker pull fedora@sha256:e68bc50be57ad40bdea883ab3258ef54d31db8f040f566fb94d7c33b2dbbdbee
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **56.5 MB (56500761 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:7f231a68ffe80ee9627d48fd6d094a60ecce03b963e8e8e913b1a8232820a0c1`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Tue, 02 Nov 2021 21:12:33 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Tue, 02 Nov 2021 21:13:09 GMT
ENV DISTTAG=fRawhidecontainer FGC=fRawhide FBR=fRawhide
# Tue, 02 Nov 2021 21:13:13 GMT
ADD file:604e2ef3b8dc32d11da1cea3535b660bc595a1edb5f2c1086044bad452f95173 in / 
# Tue, 02 Nov 2021 21:13:15 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:761f7e0d8b44b3ccf888adc5f0c06915c28cd89cfd49738aa57bd74c6b80fca4`  
		Last Modified: Tue, 02 Nov 2021 21:14:36 GMT  
		Size: 56.5 MB (56500761 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:rawhide` - linux; ppc64le

```console
$ docker pull fedora@sha256:c8a8978ab0cd7179f7939b0b90c819c790177d04af9da87c0be8a3f1eb8b6455
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **62.9 MB (62936147 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:0ad394517791196c73aa8e8409673c0a0edb9fd169c031e46d66832f01fec1b8`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 23 Jul 2021 23:32:50 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Mon, 04 Oct 2021 19:40:57 GMT
ENV DISTTAG=fRawhidecontainer FGC=fRawhide FBR=fRawhide
# Tue, 02 Nov 2021 22:46:52 GMT
ADD file:4588be93eea957e400eab1c1cdc36dc31eb37f882edb87f2c6e83c4fd5025293 in / 
# Tue, 02 Nov 2021 22:46:58 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:99ad4baf4202d7be14137713425709d4866cb140fb8757e5ba4c49fc1ea36642`  
		Last Modified: Tue, 02 Nov 2021 22:48:15 GMT  
		Size: 62.9 MB (62936147 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `fedora:rawhide` - linux; s390x

```console
$ docker pull fedora@sha256:afafc77b4632c6750bfffaa099fc8e753b2a38e0496922956e13020cd937203e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.2 MB (55158170 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f4825a88388dd83c71d7ebe46b8bbcd412d59c160072145869ca74f23541b3a2`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 23 Jul 2021 02:30:24 GMT
LABEL maintainer=Clement Verna <cverna@fedoraproject.org>
# Fri, 01 Oct 2021 22:42:38 GMT
ENV DISTTAG=fRawhidecontainer FGC=fRawhide FBR=fRawhide
# Tue, 02 Nov 2021 21:29:08 GMT
ADD file:8a77a81970b6a702c1b54fad57fad996bfa087b741913c5e6497f56f8e4189cb in / 
# Tue, 02 Nov 2021 21:29:18 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:e90ab5cf9c8733234d970ca9a967d31286943996a9eb293f507785641cc7e149`  
		Last Modified: Tue, 02 Nov 2021 21:30:21 GMT  
		Size: 55.2 MB (55158170 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
