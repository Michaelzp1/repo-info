# `clearlinux:latest`

## Docker Metadata

- Image ID: `sha256:0e0b9cc27514fb521cead6daea43c532b29a51bc47c50b9ecd31b6b785f7ed1f`
- Created: `2021-11-01T22:19:30.094407645Z`
- Virtual Size: ~ 188.53 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["/bin/bash"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
