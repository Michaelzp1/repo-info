## `clearlinux:base`

```console
$ docker pull clearlinux@sha256:b3ac3344ba57864402d49c999a38007e10be3cb7041ce4a63a01c53a42d4688e
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `clearlinux:base` - linux; amd64

```console
$ docker pull clearlinux@sha256:919b2c5d2a3ce7f5cd15ceb41a20939d228195e81b0a1981f976ab65dcf0b1f4
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **73.5 MB (73516931 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:0e0b9cc27514fb521cead6daea43c532b29a51bc47c50b9ecd31b6b785f7ed1f`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Tue, 09 Mar 2021 00:22:54 GMT
MAINTAINER William Douglas <william.douglas@intel.com>
# Mon, 01 Nov 2021 22:19:29 GMT
ADD file:efa20c20fe141c9dc5a245f0323efb3a72251651a977ba58fce8e1e2042e7888 in / 
# Mon, 01 Nov 2021 22:19:30 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:b4356d7b06a47e4b2c49d80190a7093a30ee22fdd17519a8c0318cdabc6bc748`  
		Last Modified: Mon, 01 Nov 2021 22:19:49 GMT  
		Size: 73.5 MB (73516931 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
