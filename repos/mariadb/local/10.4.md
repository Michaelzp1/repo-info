# `mariadb:10.4.21-focal`

## Docker Metadata

- Image ID: `sha256:f5f1527bdc85420e34baab9fa2467bc865342f43d0d11eb5b92c5b43063818a1`
- Created: `2021-10-16T03:10:10.301434655Z`
- Virtual Size: ~ 400.34 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["docker-entrypoint.sh"]`
- Command: `["mysqld"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `GOSU_VERSION=1.13`
  - `MARIADB_MAJOR=10.4`
  - `MARIADB_VERSION=1:10.4.21+maria~focal`

## `dpkg` (`.deb`-based packages)

### `dpkg` source package: `acl=2.2.53-6`

Binary Packages:

- `libacl1:amd64=2.2.53-6`
