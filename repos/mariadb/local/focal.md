# `mariadb:10.6.4-focal`

## Docker Metadata

- Image ID: `sha256:12e05d5da3c5223e9877e8eb90d68560ff66cedcb955131061d60d093a908f0c`
- Created: `2021-10-16T03:09:02.974904981Z`
- Virtual Size: ~ 408.87 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["docker-entrypoint.sh"]`
- Command: `["mysqld"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `GOSU_VERSION=1.13`
  - `MARIADB_MAJOR=10.6`
  - `MARIADB_VERSION=1:10.6.4+maria~focal`

## `dpkg` (`.deb`-based packages)

### `dpkg` source package: `acl=2.2.53-6`

Binary Packages:

- `libacl1:amd64=2.2.53-6`
