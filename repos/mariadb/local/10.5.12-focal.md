# `mariadb:10.5.12-focal`

## Docker Metadata

- Image ID: `sha256:9a2e250ac49189e065785b9744b87ec79472c9fa20766aafd25af62a40953316`
- Created: `2021-10-16T03:09:39.684506211Z`
- Virtual Size: ~ 407.34 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["docker-entrypoint.sh"]`
- Command: `["mysqld"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `GOSU_VERSION=1.13`
  - `MARIADB_MAJOR=10.5`
  - `MARIADB_VERSION=1:10.5.12+maria~focal`

## `dpkg` (`.deb`-based packages)

### `dpkg` source package: `acl=2.2.53-6`

Binary Packages:

- `libacl1:amd64=2.2.53-6`
