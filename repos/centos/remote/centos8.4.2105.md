## `centos:centos8.4.2105`

```console
$ docker pull centos@sha256:a27fd8080b517143cbbbab9dfb7c8571c40d67d534bbdee55bd6c473f432b177
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 3
	-	linux; amd64
	-	linux; arm64 variant v8
	-	linux; ppc64le

### `centos:centos8.4.2105` - linux; amd64

```console
$ docker pull centos@sha256:a1801b843b1bfaf77c501e7a6d3f709401a1e0c83863037fa3aab063a7fdb9dc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **83.5 MB (83518086 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5d0da3dc976460b72c77d94c8a1ad043720b0416bfc16c52c45d4847e53fadb6`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:20:04 GMT
ADD file:805cb5e15fb6e0bb0326ca33fd2942e068863ce2a8491bb71522c652f31fb466 in / 
# Wed, 15 Sep 2021 18:20:04 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20210915
# Wed, 15 Sep 2021 18:20:05 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:a1d0c75327776413fa0db9ed3adcdbadedc95a662eb1d360dad82bb913f8a1d1`  
		Last Modified: Wed, 15 Sep 2021 18:21:25 GMT  
		Size: 83.5 MB (83518086 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:centos8.4.2105` - linux; arm64 variant v8

```console
$ docker pull centos@sha256:65a4aad1156d8a0679537cb78519a17eb7142e05a968b26a5361153006224fdc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **83.9 MB (83941353 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e6a0117ec169eda93dc5ca978c6ac87580e36765a66097a6bfb6639a3bd4038a`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 17:39:41 GMT
ADD file:420712a90b0934202b326dc06b73638ab8e4603d12be2c23d67d834eb6cfc052 in / 
# Wed, 15 Sep 2021 17:39:42 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20210915
# Wed, 15 Sep 2021 17:39:42 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:52f9ef134af7dd14738733e567402af86136287d9468978d044780a6435a1193`  
		Last Modified: Wed, 15 Sep 2021 17:40:42 GMT  
		Size: 83.9 MB (83941353 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:centos8.4.2105` - linux; ppc64le

```console
$ docker pull centos@sha256:8bcb67d4c816e16f79bdea8079c5e30e88f404f4aa8fa124245864830d7852d2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **94.0 MB (93995843 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cc5795ce20d19510cc342b342b04876802e6d0766f5c61e68295e341c0fc12d4`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:28:42 GMT
ADD file:bbae4486bc410c279ebd54b2fc78bcf65f121306acf1eaccacbd9f83f5d6d337 in / 
# Wed, 15 Sep 2021 18:28:50 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20210915
# Wed, 15 Sep 2021 18:28:53 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:ea373b0f63e4f4e7a9529c8258e71e0e897f5ed4eb2af13a8e7e87ceefaa8b5d`  
		Last Modified: Wed, 15 Sep 2021 18:30:35 GMT  
		Size: 94.0 MB (93995843 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
