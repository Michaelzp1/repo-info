## `centos:6.10`

```console
$ docker pull centos@sha256:abc8bec96842a07c03274d3f1549bd37ec5dbfb0e36dcdfc31d70b1ad3281431
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `centos:6.10` - linux; amd64

```console
$ docker pull centos@sha256:ea2905089c8cd9c420ca8b877c3e08ddd732be0fb1d350ae5b87a49e9119a4ff
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **69.8 MB (69800401 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f1af727749c4a4d0355feaad60065e240a473f1a8a7b716bdff0f2be03c28935`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:20:53 GMT
ADD file:2e9397e387a495d85cde898dd51c1741710b271efc40dd6d1151548b02aeb2ea in / 
# Wed, 15 Sep 2021 18:20:53 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20180804
# Wed, 15 Sep 2021 18:20:54 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:06a11a3d840dd2b7c5c6b6b169545ff77a2c1dd63b0f8fa9991709e3ffaee0ba`  
		Last Modified: Sun, 24 Feb 2019 17:11:23 GMT  
		Size: 69.8 MB (69800401 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
