# `centos:latest`

## Docker Metadata

- Image ID: `sha256:5d0da3dc976460b72c77d94c8a1ad043720b0416bfc16c52c45d4847e53fadb6`
- Created: `2021-09-15T18:20:05.184694267Z`
- Virtual Size: ~ 231.22 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["/bin/bash"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
- Labels:
  - `org.label-schema.build-date=20210915`
  - `org.label-schema.license=GPLv2`
  - `org.label-schema.name=CentOS Base Image`
  - `org.label-schema.schema-version=1.0`
  - `org.label-schema.vendor=CentOS`

## `rpm` (`.rpm`-based packages)

### `rpm` package: `acl-2.2.53-1.el8.x86_64`

Licenses (from `rpm --query`): GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `audit-libs-3.0-0.17.20191104git1c2f876.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `basesystem-11-5.el8.noarch`

Licenses (from `rpm --query`): Public Domain

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `bash-4.4.19-14.el8.x86_64`

Licenses (from `rpm --query`): GPLv3+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `bind-export-libs-9.11.26-3.el8.x86_64`

Licenses (from `rpm --query`): MPLv2.0

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `binutils-2.30-93.el8.x86_64`

Licenses (from `rpm --query`): GPLv3+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `bzip2-libs-1.0.6-26.el8.x86_64`

Licenses (from `rpm --query`): BSD

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `ca-certificates-2020.2.41-80.0.el8_2.noarch`

Licenses (from `rpm --query`): Public Domain

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `centos-gpg-keys-8-2.el8.noarch`

Licenses (from `rpm --query`): GPLv2

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `centos-linux-release-8.4-1.2105.el8.noarch`

Licenses (from `rpm --query`): GPLv2

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `centos-linux-repos-8-2.el8.noarch`

Licenses (from `rpm --query`): GPLv2

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `chkconfig-1.13-2.el8.x86_64`

Licenses (from `rpm --query`): GPLv2

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `coreutils-single-8.30-8.el8.x86_64`

Licenses (from `rpm --query`): GPLv3+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `cpio-2.12-10.el8.x86_64`

Licenses (from `rpm --query`): GPLv3+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `cracklib-2.9.6-15.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `cracklib-dicts-2.9.6-15.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `crypto-policies-20210209-1.gitbfb6bed.el8_3.noarch`

Licenses (from `rpm --query`): LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `cryptsetup-libs-2.3.3-4.el8.x86_64`

Licenses (from `rpm --query`): GPLv2+ and LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `curl-7.61.1-18.el8.x86_64`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `cyrus-sasl-lib-2.1.27-5.el8.x86_64`

Licenses (from `rpm --query`): BSD with advertising

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `dbus-1.12.8-12.el8.x86_64`

Licenses (from `rpm --query`): (GPLv2+ or AFL) and GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `dbus-common-1.12.8-12.el8.noarch`

Licenses (from `rpm --query`): (GPLv2+ or AFL) and GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `dbus-daemon-1.12.8-12.el8.x86_64`

Licenses (from `rpm --query`): (GPLv2+ or AFL) and GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `dbus-libs-1.12.8-12.el8.x86_64`

Licenses (from `rpm --query`): (GPLv2+ or AFL) and GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `dbus-tools-1.12.8-12.el8.x86_64`

Licenses (from `rpm --query`): (GPLv2+ or AFL) and GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `device-mapper-1.02.175-5.el8.x86_64`

Licenses (from `rpm --query`): GPLv2

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `device-mapper-libs-1.02.175-5.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `dhcp-client-4.3.6-44.0.1.el8.x86_64`

Licenses (from `rpm --query`): ISC

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `dhcp-common-4.3.6-44.0.1.el8.noarch`

Licenses (from `rpm --query`): ISC

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `dhcp-libs-4.3.6-44.0.1.el8.x86_64`

Licenses (from `rpm --query`): ISC

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `dnf-4.4.2-11.el8.noarch`

Licenses (from `rpm --query`): GPLv2+ and GPLv2 and GPL

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `dnf-data-4.4.2-11.el8.noarch`

Licenses (from `rpm --query`): GPLv2+ and GPLv2 and GPL

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `dracut-049-135.git20210121.el8.x86_64`

Licenses (from `rpm --query`): GPLv2+ and LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `dracut-network-049-135.git20210121.el8.x86_64`

Licenses (from `rpm --query`): GPLv2+ and LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `dracut-squash-049-135.git20210121.el8.x86_64`

Licenses (from `rpm --query`): GPLv2+ and LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `elfutils-default-yama-scope-0.182-3.el8.noarch`

Licenses (from `rpm --query`): GPLv2+ or LGPLv3+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `elfutils-libelf-0.182-3.el8.x86_64`

Licenses (from `rpm --query`): GPLv2+ or LGPLv3+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `elfutils-libs-0.182-3.el8.x86_64`

Licenses (from `rpm --query`): GPLv2+ or LGPLv3+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `ethtool-5.8-5.el8.x86_64`

Licenses (from `rpm --query`): GPLv2

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `expat-2.2.5-4.el8.x86_64`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `file-libs-5.33-16.el8_3.1.x86_64`

Licenses (from `rpm --query`): BSD

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `filesystem-3.8-3.el8.x86_64`

Licenses (from `rpm --query`): Public Domain

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `findutils-4.6.0-20.el8.x86_64`

Licenses (from `rpm --query`): GPLv3+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `gawk-4.2.1-2.el8.x86_64`

Licenses (from `rpm --query`): GPLv3+ and GPLv2+ and LGPLv2+ and BSD

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `gdbm-1.18-1.el8.x86_64`

Licenses (from `rpm --query`): GPLv3+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `gdbm-libs-1.18-1.el8.x86_64`

Licenses (from `rpm --query`): GPLv3+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `glib2-2.56.4-9.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `glibc-2.28-151.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `glibc-common-2.28-151.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `glibc-minimal-langpack-2.28-151.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `gmp-6.1.2-10.el8.x86_64`

Licenses (from `rpm --query`): LGPLv3+ or GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `gnupg2-2.2.20-2.el8.x86_64`

Licenses (from `rpm --query`): GPLv3+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `gnutls-3.6.14-7.el8_3.x86_64`

Licenses (from `rpm --query`): GPLv3+ and LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `gpgme-1.13.1-7.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+ and MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `grep-3.1-6.el8.x86_64`

Licenses (from `rpm --query`): GPLv3+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `gzip-1.9-12.el8.x86_64`

Licenses (from `rpm --query`): GPLv3+ and GFDL

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `hostname-3.20-6.el8.x86_64`

Licenses (from `rpm --query`): GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `hwdata-0.314-8.8.el8.noarch`

Licenses (from `rpm --query`): GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `ima-evm-utils-1.3.2-12.el8.x86_64`

Licenses (from `rpm --query`): GPLv2

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `info-6.5-6.el8.x86_64`

Licenses (from `rpm --query`): GPLv3+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `ipcalc-0.2.4-4.el8.x86_64`

Licenses (from `rpm --query`): GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `iproute-5.9.0-4.el8.x86_64`

Licenses (from `rpm --query`): GPLv2+ and Public Domain

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `iptables-libs-1.8.4-17.el8.x86_64`

Licenses (from `rpm --query`): GPLv2 and Artistic 2.0 and ISC

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `iputils-20180629-7.el8.x86_64`

Licenses (from `rpm --query`): BSD and GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `json-c-0.13.1-0.4.el8.x86_64`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `kexec-tools-2.0.20-46.el8.x86_64`

Licenses (from `rpm --query`): GPLv2

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `keyutils-libs-1.5.10-6.el8.x86_64`

Licenses (from `rpm --query`): GPLv2+ and LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `kmod-25-17.el8.x86_64`

Licenses (from `rpm --query`): GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `kmod-libs-25-17.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `krb5-libs-1.18.2-8.el8.x86_64`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `langpacks-en-1.0-12.el8.noarch`

Licenses (from `rpm --query`): GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `less-530-1.el8.x86_64`

Licenses (from `rpm --query`): GPLv3+ or BSD

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libacl-2.2.53-1.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libarchive-3.3.3-1.el8.x86_64`

Licenses (from `rpm --query`): BSD

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libassuan-2.5.1-3.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+ and GPLv3+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libattr-2.4.48-3.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libblkid-2.32.1-27.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libcap-2.26-4.el8.x86_64`

Licenses (from `rpm --query`): GPLv2

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libcap-ng-0.7.9-5.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libcom_err-1.45.6-1.el8.x86_64`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libcomps-0.1.11-5.el8.x86_64`

Licenses (from `rpm --query`): GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libcurl-minimal-7.61.1-18.el8.x86_64`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libdb-5.3.28-40.el8.x86_64`

Licenses (from `rpm --query`): BSD and LGPLv2 and Sleepycat

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libdb-utils-5.3.28-40.el8.x86_64`

Licenses (from `rpm --query`): BSD and LGPLv2 and Sleepycat

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libdnf-0.55.0-7.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libfdisk-2.32.1-27.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libffi-3.1-22.el8.x86_64`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libgcc-8.4.1-1.el8.x86_64`

Licenses (from `rpm --query`): GPLv3+ and GPLv3+ with exceptions and GPLv2+ with exceptions and LGPLv2+ and BSD

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libgcrypt-1.8.5-4.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libgpg-error-1.31-1.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libibverbs-32.0-4.el8.x86_64`

Licenses (from `rpm --query`): GPLv2 or BSD

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libidn2-2.2.0-1.el8.x86_64`

Licenses (from `rpm --query`): (GPLv2+ or LGPLv3+) and GPLv3+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libkcapi-1.2.0-2.el8.x86_64`

Licenses (from `rpm --query`): BSD or GPLv2

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libkcapi-hmaccalc-1.2.0-2.el8.x86_64`

Licenses (from `rpm --query`): BSD or GPLv2

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libksba-1.3.5-7.el8.x86_64`

Licenses (from `rpm --query`): (LGPLv3+ or GPLv2+) and GPLv3+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libmetalink-0.1.3-7.el8.x86_64`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libmnl-1.0.4-6.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libmodulemd-2.9.4-2.el8.x86_64`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libmount-2.32.1-27.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libnghttp2-1.33.0-3.el8_2.1.x86_64`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libnl3-3.5.0-1.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libnsl2-1.2.0-2.20180605git4a062cf.el8.x86_64`

Licenses (from `rpm --query`): BSD and LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libpcap-1.9.1-5.el8.x86_64`

Licenses (from `rpm --query`): BSD with advertising

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libpwquality-1.4.4-3.el8.x86_64`

Licenses (from `rpm --query`): BSD or GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `librepo-1.12.0-3.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libreport-filesystem-2.9.5-15.el8.x86_64`

Licenses (from `rpm --query`): GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libseccomp-2.5.1-1.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libselinux-2.9-5.el8.x86_64`

Licenses (from `rpm --query`): Public Domain

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libsemanage-2.9-6.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libsepol-2.9-2.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libsigsegv-2.11-5.el8.x86_64`

Licenses (from `rpm --query`): GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libsmartcols-2.32.1-27.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libsolv-0.7.16-2.el8.x86_64`

Licenses (from `rpm --query`): BSD

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libstdc++-8.4.1-1.el8.x86_64`

Licenses (from `rpm --query`): GPLv3+ and GPLv3+ with exceptions and GPLv2+ with exceptions and LGPLv2+ and BSD

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libtasn1-4.13-3.el8.x86_64`

Licenses (from `rpm --query`): GPLv3+ and LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libtirpc-1.1.4-4.el8.x86_64`

Licenses (from `rpm --query`): SISSL and BSD

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libunistring-0.9.9-3.el8.x86_64`

Licenses (from `rpm --query`): GPLv2+ or LGPLv3+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libusbx-1.0.23-4.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libutempter-1.1.6-14.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libuuid-2.32.1-27.el8.x86_64`

Licenses (from `rpm --query`): BSD

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libverto-0.3.0-5.el8.x86_64`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libxcrypt-4.1.1-4.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+ and BSD and Public Domain

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libxml2-2.9.7-9.el8.x86_64`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libyaml-0.1.7-5.el8.x86_64`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libzstd-1.4.4-1.el8.x86_64`

Licenses (from `rpm --query`): BSD and GPLv2

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `lua-libs-5.3.4-11.el8.x86_64`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `lz4-libs-1.8.3-2.el8.x86_64`

Licenses (from `rpm --query`): GPLv2+ and BSD

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `lzo-2.08-14.el8.x86_64`

Licenses (from `rpm --query`): GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `mpfr-3.1.6-1.el8.x86_64`

Licenses (from `rpm --query`): LGPLv3+ and GPLv3+ and GFDL

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `ncurses-base-6.1-7.20180224.el8.noarch`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `ncurses-libs-6.1-7.20180224.el8.x86_64`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `nettle-3.4.1-2.el8.x86_64`

Licenses (from `rpm --query`): LGPLv3+ or GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `npth-1.5-4.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `openldap-2.4.46-16.el8.x86_64`

Licenses (from `rpm --query`): OpenLDAP

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `openssl-libs-1.1.1g-15.el8_3.x86_64`

Licenses (from `rpm --query`): OpenSSL and ASL 2.0

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `p11-kit-0.23.22-1.el8.x86_64`

Licenses (from `rpm --query`): BSD

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `p11-kit-trust-0.23.22-1.el8.x86_64`

Licenses (from `rpm --query`): BSD

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `pam-1.3.1-14.el8.x86_64`

Licenses (from `rpm --query`): BSD and GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `pciutils-3.7.0-1.el8.x86_64`

Licenses (from `rpm --query`): GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `pciutils-libs-3.7.0-1.el8.x86_64`

Licenses (from `rpm --query`): GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `pcre-8.42-4.el8.x86_64`

Licenses (from `rpm --query`): BSD

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `pcre2-10.32-2.el8.x86_64`

Licenses (from `rpm --query`): BSD

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `platform-python-3.6.8-37.el8.x86_64`

Licenses (from `rpm --query`): Python

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `platform-python-setuptools-39.2.0-6.el8.noarch`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `popt-1.18-1.el8.x86_64`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `procps-ng-3.3.15-6.el8.x86_64`

Licenses (from `rpm --query`): GPL+ and GPLv2 and GPLv2+ and GPLv3+ and LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `python3-dnf-4.4.2-11.el8.noarch`

Licenses (from `rpm --query`): GPLv2+ and GPLv2 and GPL

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `python3-gpg-1.13.1-7.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+ and MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `python3-hawkey-0.55.0-7.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `python3-libcomps-0.1.11-5.el8.x86_64`

Licenses (from `rpm --query`): GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `python3-libdnf-0.55.0-7.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `python3-libs-3.6.8-37.el8.x86_64`

Licenses (from `rpm --query`): Python

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `python3-pip-wheel-9.0.3-19.el8.noarch`

Licenses (from `rpm --query`): MIT and Python and ASL 2.0 and BSD and ISC and LGPLv2 and MPLv2.0 and (ASL 2.0 or BSD)

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `python3-rpm-4.14.3-13.el8.x86_64`

Licenses (from `rpm --query`): GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `python3-setuptools-wheel-39.2.0-6.el8.noarch`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `rdma-core-32.0-4.el8.x86_64`

Licenses (from `rpm --query`): GPLv2 or BSD

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `readline-7.0-10.el8.x86_64`

Licenses (from `rpm --query`): GPLv3+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `rootfiles-8.1-22.el8.noarch`

Licenses (from `rpm --query`): Public Domain

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `rpm-4.14.3-13.el8.x86_64`

Licenses (from `rpm --query`): GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `rpm-build-libs-4.14.3-13.el8.x86_64`

Licenses (from `rpm --query`): GPLv2+ and LGPLv2+ with exceptions

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `rpm-libs-4.14.3-13.el8.x86_64`

Licenses (from `rpm --query`): GPLv2+ and LGPLv2+ with exceptions

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `sed-4.5-2.el8.x86_64`

Licenses (from `rpm --query`): GPLv3+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `setup-2.12.2-6.el8.noarch`

Licenses (from `rpm --query`): Public Domain

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `shadow-utils-4.6-12.el8.x86_64`

Licenses (from `rpm --query`): BSD and GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `snappy-1.1.8-3.el8.x86_64`

Licenses (from `rpm --query`): BSD

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `sqlite-libs-3.26.0-13.el8.x86_64`

Licenses (from `rpm --query`): Public Domain

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `squashfs-tools-4.3-20.el8.x86_64`

Licenses (from `rpm --query`): GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `systemd-239-45.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+ and MIT and GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `systemd-libs-239-45.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+ and MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `systemd-pam-239-45.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+ and MIT and GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `systemd-udev-239-45.el8.x86_64`

Licenses (from `rpm --query`): LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `tar-1.30-5.el8.x86_64`

Licenses (from `rpm --query`): GPLv3+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `tpm2-tss-2.3.2-3.el8.x86_64`

Licenses (from `rpm --query`): BSD

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `tzdata-2021a-1.el8.noarch`

Licenses (from `rpm --query`): Public Domain

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `util-linux-2.32.1-27.el8.x86_64`

Licenses (from `rpm --query`): GPLv2 and GPLv2+ and LGPLv2+ and BSD with advertising and Public Domain

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `vim-minimal-8.0.1763-15.el8.x86_64`

Licenses (from `rpm --query`): Vim and MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `xz-5.2.4-3.el8.x86_64`

Licenses (from `rpm --query`): GPLv2+ and Public Domain

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `xz-libs-5.2.4-3.el8.x86_64`

Licenses (from `rpm --query`): Public Domain

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `yum-4.4.2-11.el8.noarch`

Licenses (from `rpm --query`): GPLv2+ and GPLv2 and GPL

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `zlib-1.2.11-17.el8.x86_64`

Licenses (from `rpm --query`): zlib and Boost

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!
