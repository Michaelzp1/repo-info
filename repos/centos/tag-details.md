<!-- THIS FILE IS GENERATED VIA './update-remote.sh' -->

# Tags of `centos`

-	[`centos:6`](#centos6)
-	[`centos:6.10`](#centos610)
-	[`centos:7`](#centos7)
-	[`centos:7.9.2009`](#centos792009)
-	[`centos:8`](#centos8)
-	[`centos:8.4.2105`](#centos842105)
-	[`centos:centos6`](#centoscentos6)
-	[`centos:centos6.10`](#centoscentos610)
-	[`centos:centos7`](#centoscentos7)
-	[`centos:centos7.9.2009`](#centoscentos792009)
-	[`centos:centos8`](#centoscentos8)
-	[`centos:centos8.4.2105`](#centoscentos842105)
-	[`centos:latest`](#centoslatest)

## `centos:6`

```console
$ docker pull centos@sha256:a93df2e96e07f56ea48f215425c6f1673ab922927894595bb5c0ee4c5a955133
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 2
	-	linux; amd64
	-	linux; 386

### `centos:6` - linux; amd64

```console
$ docker pull centos@sha256:3688aa867eb84332460e172b9250c9c198fdfd8d987605fd53f246f498c60bcf
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **69.8 MB (69835815 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5bf9684f472089d6d5cb636041d3d6dc748dbde39f1aefc374bbd367bd2aabbf`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:20:32 GMT
MAINTAINER https://github.com/CentOS/sig-cloud-instance-images
# Wed, 15 Sep 2021 18:20:38 GMT
ADD file:0065316a41144e95bcb133567cc86816b8368a823cc067d741e06ded59849fd8 in / 
# Wed, 15 Sep 2021 18:20:38 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20181006
# Wed, 15 Sep 2021 18:20:39 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:ff50d722b38227ec8f2bbf0cdbce428b66745077c173d8117d91376128fa532e`  
		Last Modified: Wed, 30 Jan 2019 15:06:57 GMT  
		Size: 69.8 MB (69835815 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:6` - linux; 386

```console
$ docker pull centos@sha256:498c1320b2574fc5bbf897768ffd2c510edf0af3bc4c561aa1836f64845c1c27
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **70.1 MB (70074539 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:259a2295d192482447eb64d2bb5d1f6b02268de8dd8d9aa0403ae9a34b0d74a3`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 17:39:00 GMT
MAINTAINER https://github.com/CentOS/sig-cloud-instance-images
# Wed, 15 Sep 2021 17:39:09 GMT
ADD file:a37e74347ae6032b793c87e5c46a27c0d8c24ca0ee4700f3eb1851c834b3ce19 in / 
# Wed, 15 Sep 2021 17:39:09 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20181006
# Wed, 15 Sep 2021 17:39:10 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:3c3945419b531b5672104afd31d137de0c2226d41118e983353be7d4fadb3a34`  
		Last Modified: Fri, 15 Mar 2019 10:39:49 GMT  
		Size: 70.1 MB (70074539 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `centos:6.10`

```console
$ docker pull centos@sha256:abc8bec96842a07c03274d3f1549bd37ec5dbfb0e36dcdfc31d70b1ad3281431
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `centos:6.10` - linux; amd64

```console
$ docker pull centos@sha256:ea2905089c8cd9c420ca8b877c3e08ddd732be0fb1d350ae5b87a49e9119a4ff
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **69.8 MB (69800401 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f1af727749c4a4d0355feaad60065e240a473f1a8a7b716bdff0f2be03c28935`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:20:53 GMT
ADD file:2e9397e387a495d85cde898dd51c1741710b271efc40dd6d1151548b02aeb2ea in / 
# Wed, 15 Sep 2021 18:20:53 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20180804
# Wed, 15 Sep 2021 18:20:54 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:06a11a3d840dd2b7c5c6b6b169545ff77a2c1dd63b0f8fa9991709e3ffaee0ba`  
		Last Modified: Sun, 24 Feb 2019 17:11:23 GMT  
		Size: 69.8 MB (69800401 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `centos:7`

```console
$ docker pull centos@sha256:9d4bcbbb213dfd745b58be38b13b996ebb5ac315fe75711bd618426a630e0987
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; ppc64le

### `centos:7` - linux; amd64

```console
$ docker pull centos@sha256:dead07b4d8ed7e29e98de0f4504d87e8880d4347859d839686a31da35a3b532f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **76.1 MB (76097157 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:eeb6ee3f44bd0b5103bb561b4c16bcb82328cfe5809ab675bb17ab3a16c517c9`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:20:23 GMT
ADD file:b3ebbe8bd304723d43b7b44a6d990cd657b63d93d6a2a9293983a30bfc1dfa53 in / 
# Wed, 15 Sep 2021 18:20:23 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20201113 org.opencontainers.image.title=CentOS Base Image org.opencontainers.image.vendor=CentOS org.opencontainers.image.licenses=GPL-2.0-only org.opencontainers.image.created=2020-11-13 00:00:00+00:00
# Wed, 15 Sep 2021 18:20:23 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:2d473b07cdd5f0912cd6f1a703352c82b512407db6b05b43f2553732b55df3bc`  
		Last Modified: Sat, 14 Nov 2020 00:21:39 GMT  
		Size: 76.1 MB (76097157 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:7` - linux; arm variant v7

```console
$ docker pull centos@sha256:1cef7b00a46a2a90c62eef9a9707d01d19c2f5e253b7ccc574ccaa775063d7e0
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **70.0 MB (70029389 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:df85be4d57c4a6d35c3a4f6a10c7501eeefb08cab83e91ccb073c0cb979936b2`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:02:06 GMT
ADD file:e9a3f27674e8ef05c9fb52ea3995976938e8063024e52e52c8f72f0514f1f10c in / 
# Wed, 15 Sep 2021 18:02:08 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20181205
# Wed, 15 Sep 2021 18:02:08 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:193bcbf05ff9ae85ac1a58cacd9c07f8f4297dc648808c347cceb3797ae603af`  
		Last Modified: Mon, 11 Feb 2019 09:47:52 GMT  
		Size: 70.0 MB (70029389 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:7` - linux; arm64 variant v8

```console
$ docker pull centos@sha256:864a7acea4a5e8fa7a4d83720fbcbadbe38b183f46f3600e04a3f8c1d961ed87
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **108.4 MB (108374945 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:dfc30428e1631ce2adb2a7978c667f88dc033a3c2991943f5e2307d857ed0370`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 17:39:58 GMT
ADD file:5b1e63a3cb041177b802b501dedcd71a86f1773ea0f69f048f2eb3901097711d in / 
# Wed, 15 Sep 2021 17:39:58 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20201113 org.opencontainers.image.title=CentOS Base Image org.opencontainers.image.vendor=CentOS org.opencontainers.image.licenses=GPL-2.0-only org.opencontainers.image.created=2020-11-13 00:00:00+00:00
# Wed, 15 Sep 2021 17:39:59 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:6717b8ec66cd6add0272c6391165585613c31314a43ff77d9751b53010e531ec`  
		Last Modified: Sat, 14 Nov 2020 00:41:36 GMT  
		Size: 108.4 MB (108374945 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:7` - linux; 386

```console
$ docker pull centos@sha256:8faead07bd1d5fdd17414a1759eae004da5daa9575a846f4730f44dec0f47843
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **76.7 MB (76701822 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d0be79ecaa51a8dd0d144ac7084e8407d06185aa81c214fb12fdd0ad7b5d3ad1`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 17:38:48 GMT
ADD file:d7dc747583a35caf4429d6d3a67aa63d7293ea091ad41c0d029217717c27ed0f in / 
# Wed, 15 Sep 2021 17:38:49 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20201113 org.opencontainers.image.title=CentOS Base Image org.opencontainers.image.vendor=CentOS org.opencontainers.image.licenses=GPL-2.0-only org.opencontainers.image.created=2020-11-13 00:00:00+00:00
# Wed, 15 Sep 2021 17:38:49 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:ce510b3e78588b800eed5e3c325c3cab32ee9d0804706a14dd193cd1b4661e42`  
		Last Modified: Sat, 14 Nov 2020 00:39:37 GMT  
		Size: 76.7 MB (76701822 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:7` - linux; ppc64le

```console
$ docker pull centos@sha256:b98bfc4a7094a98ed52d49f9558bb15a4cee9854f182d30085d459d4a55d2e03
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **80.5 MB (80516460 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:fc1c5750d2313b5ced6caf90794a7e6189a1267496926b06d57cdeaec5455c0d`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:29:27 GMT
ADD file:7f21ae7d20a8e347d8b678bcf26be83abb1ee27d3b567c9cddd993e45ce8ac34 in / 
# Wed, 15 Sep 2021 18:29:31 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20201113 org.opencontainers.image.title=CentOS Base Image org.opencontainers.image.vendor=CentOS org.opencontainers.image.licenses=GPL-2.0-only org.opencontainers.image.created=2020-11-13 00:00:00+00:00
# Wed, 15 Sep 2021 18:29:40 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:3fe478aaff9b8f3ba958253e7339e9016ec07c075b805ebfc8cd372ddd01ee64`  
		Last Modified: Tue, 17 Nov 2020 04:06:20 GMT  
		Size: 80.5 MB (80516460 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `centos:7.9.2009`

```console
$ docker pull centos@sha256:9d4bcbbb213dfd745b58be38b13b996ebb5ac315fe75711bd618426a630e0987
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; ppc64le

### `centos:7.9.2009` - linux; amd64

```console
$ docker pull centos@sha256:dead07b4d8ed7e29e98de0f4504d87e8880d4347859d839686a31da35a3b532f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **76.1 MB (76097157 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:eeb6ee3f44bd0b5103bb561b4c16bcb82328cfe5809ab675bb17ab3a16c517c9`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:20:23 GMT
ADD file:b3ebbe8bd304723d43b7b44a6d990cd657b63d93d6a2a9293983a30bfc1dfa53 in / 
# Wed, 15 Sep 2021 18:20:23 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20201113 org.opencontainers.image.title=CentOS Base Image org.opencontainers.image.vendor=CentOS org.opencontainers.image.licenses=GPL-2.0-only org.opencontainers.image.created=2020-11-13 00:00:00+00:00
# Wed, 15 Sep 2021 18:20:23 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:2d473b07cdd5f0912cd6f1a703352c82b512407db6b05b43f2553732b55df3bc`  
		Last Modified: Sat, 14 Nov 2020 00:21:39 GMT  
		Size: 76.1 MB (76097157 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:7.9.2009` - linux; arm variant v7

```console
$ docker pull centos@sha256:1cef7b00a46a2a90c62eef9a9707d01d19c2f5e253b7ccc574ccaa775063d7e0
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **70.0 MB (70029389 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:df85be4d57c4a6d35c3a4f6a10c7501eeefb08cab83e91ccb073c0cb979936b2`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:02:06 GMT
ADD file:e9a3f27674e8ef05c9fb52ea3995976938e8063024e52e52c8f72f0514f1f10c in / 
# Wed, 15 Sep 2021 18:02:08 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20181205
# Wed, 15 Sep 2021 18:02:08 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:193bcbf05ff9ae85ac1a58cacd9c07f8f4297dc648808c347cceb3797ae603af`  
		Last Modified: Mon, 11 Feb 2019 09:47:52 GMT  
		Size: 70.0 MB (70029389 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:7.9.2009` - linux; arm64 variant v8

```console
$ docker pull centos@sha256:864a7acea4a5e8fa7a4d83720fbcbadbe38b183f46f3600e04a3f8c1d961ed87
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **108.4 MB (108374945 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:dfc30428e1631ce2adb2a7978c667f88dc033a3c2991943f5e2307d857ed0370`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 17:39:58 GMT
ADD file:5b1e63a3cb041177b802b501dedcd71a86f1773ea0f69f048f2eb3901097711d in / 
# Wed, 15 Sep 2021 17:39:58 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20201113 org.opencontainers.image.title=CentOS Base Image org.opencontainers.image.vendor=CentOS org.opencontainers.image.licenses=GPL-2.0-only org.opencontainers.image.created=2020-11-13 00:00:00+00:00
# Wed, 15 Sep 2021 17:39:59 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:6717b8ec66cd6add0272c6391165585613c31314a43ff77d9751b53010e531ec`  
		Last Modified: Sat, 14 Nov 2020 00:41:36 GMT  
		Size: 108.4 MB (108374945 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:7.9.2009` - linux; 386

```console
$ docker pull centos@sha256:8faead07bd1d5fdd17414a1759eae004da5daa9575a846f4730f44dec0f47843
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **76.7 MB (76701822 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d0be79ecaa51a8dd0d144ac7084e8407d06185aa81c214fb12fdd0ad7b5d3ad1`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 17:38:48 GMT
ADD file:d7dc747583a35caf4429d6d3a67aa63d7293ea091ad41c0d029217717c27ed0f in / 
# Wed, 15 Sep 2021 17:38:49 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20201113 org.opencontainers.image.title=CentOS Base Image org.opencontainers.image.vendor=CentOS org.opencontainers.image.licenses=GPL-2.0-only org.opencontainers.image.created=2020-11-13 00:00:00+00:00
# Wed, 15 Sep 2021 17:38:49 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:ce510b3e78588b800eed5e3c325c3cab32ee9d0804706a14dd193cd1b4661e42`  
		Last Modified: Sat, 14 Nov 2020 00:39:37 GMT  
		Size: 76.7 MB (76701822 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:7.9.2009` - linux; ppc64le

```console
$ docker pull centos@sha256:b98bfc4a7094a98ed52d49f9558bb15a4cee9854f182d30085d459d4a55d2e03
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **80.5 MB (80516460 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:fc1c5750d2313b5ced6caf90794a7e6189a1267496926b06d57cdeaec5455c0d`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:29:27 GMT
ADD file:7f21ae7d20a8e347d8b678bcf26be83abb1ee27d3b567c9cddd993e45ce8ac34 in / 
# Wed, 15 Sep 2021 18:29:31 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20201113 org.opencontainers.image.title=CentOS Base Image org.opencontainers.image.vendor=CentOS org.opencontainers.image.licenses=GPL-2.0-only org.opencontainers.image.created=2020-11-13 00:00:00+00:00
# Wed, 15 Sep 2021 18:29:40 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:3fe478aaff9b8f3ba958253e7339e9016ec07c075b805ebfc8cd372ddd01ee64`  
		Last Modified: Tue, 17 Nov 2020 04:06:20 GMT  
		Size: 80.5 MB (80516460 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `centos:8`

```console
$ docker pull centos@sha256:a27fd8080b517143cbbbab9dfb7c8571c40d67d534bbdee55bd6c473f432b177
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 3
	-	linux; amd64
	-	linux; arm64 variant v8
	-	linux; ppc64le

### `centos:8` - linux; amd64

```console
$ docker pull centos@sha256:a1801b843b1bfaf77c501e7a6d3f709401a1e0c83863037fa3aab063a7fdb9dc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **83.5 MB (83518086 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5d0da3dc976460b72c77d94c8a1ad043720b0416bfc16c52c45d4847e53fadb6`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:20:04 GMT
ADD file:805cb5e15fb6e0bb0326ca33fd2942e068863ce2a8491bb71522c652f31fb466 in / 
# Wed, 15 Sep 2021 18:20:04 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20210915
# Wed, 15 Sep 2021 18:20:05 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:a1d0c75327776413fa0db9ed3adcdbadedc95a662eb1d360dad82bb913f8a1d1`  
		Last Modified: Wed, 15 Sep 2021 18:21:25 GMT  
		Size: 83.5 MB (83518086 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:8` - linux; arm64 variant v8

```console
$ docker pull centos@sha256:65a4aad1156d8a0679537cb78519a17eb7142e05a968b26a5361153006224fdc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **83.9 MB (83941353 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e6a0117ec169eda93dc5ca978c6ac87580e36765a66097a6bfb6639a3bd4038a`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 17:39:41 GMT
ADD file:420712a90b0934202b326dc06b73638ab8e4603d12be2c23d67d834eb6cfc052 in / 
# Wed, 15 Sep 2021 17:39:42 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20210915
# Wed, 15 Sep 2021 17:39:42 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:52f9ef134af7dd14738733e567402af86136287d9468978d044780a6435a1193`  
		Last Modified: Wed, 15 Sep 2021 17:40:42 GMT  
		Size: 83.9 MB (83941353 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:8` - linux; ppc64le

```console
$ docker pull centos@sha256:8bcb67d4c816e16f79bdea8079c5e30e88f404f4aa8fa124245864830d7852d2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **94.0 MB (93995843 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cc5795ce20d19510cc342b342b04876802e6d0766f5c61e68295e341c0fc12d4`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:28:42 GMT
ADD file:bbae4486bc410c279ebd54b2fc78bcf65f121306acf1eaccacbd9f83f5d6d337 in / 
# Wed, 15 Sep 2021 18:28:50 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20210915
# Wed, 15 Sep 2021 18:28:53 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:ea373b0f63e4f4e7a9529c8258e71e0e897f5ed4eb2af13a8e7e87ceefaa8b5d`  
		Last Modified: Wed, 15 Sep 2021 18:30:35 GMT  
		Size: 94.0 MB (93995843 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `centos:8.4.2105`

```console
$ docker pull centos@sha256:a27fd8080b517143cbbbab9dfb7c8571c40d67d534bbdee55bd6c473f432b177
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 3
	-	linux; amd64
	-	linux; arm64 variant v8
	-	linux; ppc64le

### `centos:8.4.2105` - linux; amd64

```console
$ docker pull centos@sha256:a1801b843b1bfaf77c501e7a6d3f709401a1e0c83863037fa3aab063a7fdb9dc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **83.5 MB (83518086 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5d0da3dc976460b72c77d94c8a1ad043720b0416bfc16c52c45d4847e53fadb6`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:20:04 GMT
ADD file:805cb5e15fb6e0bb0326ca33fd2942e068863ce2a8491bb71522c652f31fb466 in / 
# Wed, 15 Sep 2021 18:20:04 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20210915
# Wed, 15 Sep 2021 18:20:05 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:a1d0c75327776413fa0db9ed3adcdbadedc95a662eb1d360dad82bb913f8a1d1`  
		Last Modified: Wed, 15 Sep 2021 18:21:25 GMT  
		Size: 83.5 MB (83518086 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:8.4.2105` - linux; arm64 variant v8

```console
$ docker pull centos@sha256:65a4aad1156d8a0679537cb78519a17eb7142e05a968b26a5361153006224fdc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **83.9 MB (83941353 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e6a0117ec169eda93dc5ca978c6ac87580e36765a66097a6bfb6639a3bd4038a`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 17:39:41 GMT
ADD file:420712a90b0934202b326dc06b73638ab8e4603d12be2c23d67d834eb6cfc052 in / 
# Wed, 15 Sep 2021 17:39:42 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20210915
# Wed, 15 Sep 2021 17:39:42 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:52f9ef134af7dd14738733e567402af86136287d9468978d044780a6435a1193`  
		Last Modified: Wed, 15 Sep 2021 17:40:42 GMT  
		Size: 83.9 MB (83941353 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:8.4.2105` - linux; ppc64le

```console
$ docker pull centos@sha256:8bcb67d4c816e16f79bdea8079c5e30e88f404f4aa8fa124245864830d7852d2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **94.0 MB (93995843 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cc5795ce20d19510cc342b342b04876802e6d0766f5c61e68295e341c0fc12d4`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:28:42 GMT
ADD file:bbae4486bc410c279ebd54b2fc78bcf65f121306acf1eaccacbd9f83f5d6d337 in / 
# Wed, 15 Sep 2021 18:28:50 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20210915
# Wed, 15 Sep 2021 18:28:53 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:ea373b0f63e4f4e7a9529c8258e71e0e897f5ed4eb2af13a8e7e87ceefaa8b5d`  
		Last Modified: Wed, 15 Sep 2021 18:30:35 GMT  
		Size: 94.0 MB (93995843 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `centos:centos6`

```console
$ docker pull centos@sha256:a93df2e96e07f56ea48f215425c6f1673ab922927894595bb5c0ee4c5a955133
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 2
	-	linux; amd64
	-	linux; 386

### `centos:centos6` - linux; amd64

```console
$ docker pull centos@sha256:3688aa867eb84332460e172b9250c9c198fdfd8d987605fd53f246f498c60bcf
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **69.8 MB (69835815 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5bf9684f472089d6d5cb636041d3d6dc748dbde39f1aefc374bbd367bd2aabbf`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:20:32 GMT
MAINTAINER https://github.com/CentOS/sig-cloud-instance-images
# Wed, 15 Sep 2021 18:20:38 GMT
ADD file:0065316a41144e95bcb133567cc86816b8368a823cc067d741e06ded59849fd8 in / 
# Wed, 15 Sep 2021 18:20:38 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20181006
# Wed, 15 Sep 2021 18:20:39 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:ff50d722b38227ec8f2bbf0cdbce428b66745077c173d8117d91376128fa532e`  
		Last Modified: Wed, 30 Jan 2019 15:06:57 GMT  
		Size: 69.8 MB (69835815 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:centos6` - linux; 386

```console
$ docker pull centos@sha256:498c1320b2574fc5bbf897768ffd2c510edf0af3bc4c561aa1836f64845c1c27
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **70.1 MB (70074539 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:259a2295d192482447eb64d2bb5d1f6b02268de8dd8d9aa0403ae9a34b0d74a3`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 17:39:00 GMT
MAINTAINER https://github.com/CentOS/sig-cloud-instance-images
# Wed, 15 Sep 2021 17:39:09 GMT
ADD file:a37e74347ae6032b793c87e5c46a27c0d8c24ca0ee4700f3eb1851c834b3ce19 in / 
# Wed, 15 Sep 2021 17:39:09 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20181006
# Wed, 15 Sep 2021 17:39:10 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:3c3945419b531b5672104afd31d137de0c2226d41118e983353be7d4fadb3a34`  
		Last Modified: Fri, 15 Mar 2019 10:39:49 GMT  
		Size: 70.1 MB (70074539 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `centos:centos6.10`

```console
$ docker pull centos@sha256:abc8bec96842a07c03274d3f1549bd37ec5dbfb0e36dcdfc31d70b1ad3281431
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `centos:centos6.10` - linux; amd64

```console
$ docker pull centos@sha256:ea2905089c8cd9c420ca8b877c3e08ddd732be0fb1d350ae5b87a49e9119a4ff
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **69.8 MB (69800401 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f1af727749c4a4d0355feaad60065e240a473f1a8a7b716bdff0f2be03c28935`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:20:53 GMT
ADD file:2e9397e387a495d85cde898dd51c1741710b271efc40dd6d1151548b02aeb2ea in / 
# Wed, 15 Sep 2021 18:20:53 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20180804
# Wed, 15 Sep 2021 18:20:54 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:06a11a3d840dd2b7c5c6b6b169545ff77a2c1dd63b0f8fa9991709e3ffaee0ba`  
		Last Modified: Sun, 24 Feb 2019 17:11:23 GMT  
		Size: 69.8 MB (69800401 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `centos:centos7`

```console
$ docker pull centos@sha256:9d4bcbbb213dfd745b58be38b13b996ebb5ac315fe75711bd618426a630e0987
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; ppc64le

### `centos:centos7` - linux; amd64

```console
$ docker pull centos@sha256:dead07b4d8ed7e29e98de0f4504d87e8880d4347859d839686a31da35a3b532f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **76.1 MB (76097157 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:eeb6ee3f44bd0b5103bb561b4c16bcb82328cfe5809ab675bb17ab3a16c517c9`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:20:23 GMT
ADD file:b3ebbe8bd304723d43b7b44a6d990cd657b63d93d6a2a9293983a30bfc1dfa53 in / 
# Wed, 15 Sep 2021 18:20:23 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20201113 org.opencontainers.image.title=CentOS Base Image org.opencontainers.image.vendor=CentOS org.opencontainers.image.licenses=GPL-2.0-only org.opencontainers.image.created=2020-11-13 00:00:00+00:00
# Wed, 15 Sep 2021 18:20:23 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:2d473b07cdd5f0912cd6f1a703352c82b512407db6b05b43f2553732b55df3bc`  
		Last Modified: Sat, 14 Nov 2020 00:21:39 GMT  
		Size: 76.1 MB (76097157 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:centos7` - linux; arm variant v7

```console
$ docker pull centos@sha256:1cef7b00a46a2a90c62eef9a9707d01d19c2f5e253b7ccc574ccaa775063d7e0
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **70.0 MB (70029389 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:df85be4d57c4a6d35c3a4f6a10c7501eeefb08cab83e91ccb073c0cb979936b2`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:02:06 GMT
ADD file:e9a3f27674e8ef05c9fb52ea3995976938e8063024e52e52c8f72f0514f1f10c in / 
# Wed, 15 Sep 2021 18:02:08 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20181205
# Wed, 15 Sep 2021 18:02:08 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:193bcbf05ff9ae85ac1a58cacd9c07f8f4297dc648808c347cceb3797ae603af`  
		Last Modified: Mon, 11 Feb 2019 09:47:52 GMT  
		Size: 70.0 MB (70029389 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:centos7` - linux; arm64 variant v8

```console
$ docker pull centos@sha256:864a7acea4a5e8fa7a4d83720fbcbadbe38b183f46f3600e04a3f8c1d961ed87
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **108.4 MB (108374945 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:dfc30428e1631ce2adb2a7978c667f88dc033a3c2991943f5e2307d857ed0370`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 17:39:58 GMT
ADD file:5b1e63a3cb041177b802b501dedcd71a86f1773ea0f69f048f2eb3901097711d in / 
# Wed, 15 Sep 2021 17:39:58 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20201113 org.opencontainers.image.title=CentOS Base Image org.opencontainers.image.vendor=CentOS org.opencontainers.image.licenses=GPL-2.0-only org.opencontainers.image.created=2020-11-13 00:00:00+00:00
# Wed, 15 Sep 2021 17:39:59 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:6717b8ec66cd6add0272c6391165585613c31314a43ff77d9751b53010e531ec`  
		Last Modified: Sat, 14 Nov 2020 00:41:36 GMT  
		Size: 108.4 MB (108374945 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:centos7` - linux; 386

```console
$ docker pull centos@sha256:8faead07bd1d5fdd17414a1759eae004da5daa9575a846f4730f44dec0f47843
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **76.7 MB (76701822 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d0be79ecaa51a8dd0d144ac7084e8407d06185aa81c214fb12fdd0ad7b5d3ad1`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 17:38:48 GMT
ADD file:d7dc747583a35caf4429d6d3a67aa63d7293ea091ad41c0d029217717c27ed0f in / 
# Wed, 15 Sep 2021 17:38:49 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20201113 org.opencontainers.image.title=CentOS Base Image org.opencontainers.image.vendor=CentOS org.opencontainers.image.licenses=GPL-2.0-only org.opencontainers.image.created=2020-11-13 00:00:00+00:00
# Wed, 15 Sep 2021 17:38:49 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:ce510b3e78588b800eed5e3c325c3cab32ee9d0804706a14dd193cd1b4661e42`  
		Last Modified: Sat, 14 Nov 2020 00:39:37 GMT  
		Size: 76.7 MB (76701822 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:centos7` - linux; ppc64le

```console
$ docker pull centos@sha256:b98bfc4a7094a98ed52d49f9558bb15a4cee9854f182d30085d459d4a55d2e03
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **80.5 MB (80516460 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:fc1c5750d2313b5ced6caf90794a7e6189a1267496926b06d57cdeaec5455c0d`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:29:27 GMT
ADD file:7f21ae7d20a8e347d8b678bcf26be83abb1ee27d3b567c9cddd993e45ce8ac34 in / 
# Wed, 15 Sep 2021 18:29:31 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20201113 org.opencontainers.image.title=CentOS Base Image org.opencontainers.image.vendor=CentOS org.opencontainers.image.licenses=GPL-2.0-only org.opencontainers.image.created=2020-11-13 00:00:00+00:00
# Wed, 15 Sep 2021 18:29:40 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:3fe478aaff9b8f3ba958253e7339e9016ec07c075b805ebfc8cd372ddd01ee64`  
		Last Modified: Tue, 17 Nov 2020 04:06:20 GMT  
		Size: 80.5 MB (80516460 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `centos:centos7.9.2009`

```console
$ docker pull centos@sha256:9d4bcbbb213dfd745b58be38b13b996ebb5ac315fe75711bd618426a630e0987
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; ppc64le

### `centos:centos7.9.2009` - linux; amd64

```console
$ docker pull centos@sha256:dead07b4d8ed7e29e98de0f4504d87e8880d4347859d839686a31da35a3b532f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **76.1 MB (76097157 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:eeb6ee3f44bd0b5103bb561b4c16bcb82328cfe5809ab675bb17ab3a16c517c9`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:20:23 GMT
ADD file:b3ebbe8bd304723d43b7b44a6d990cd657b63d93d6a2a9293983a30bfc1dfa53 in / 
# Wed, 15 Sep 2021 18:20:23 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20201113 org.opencontainers.image.title=CentOS Base Image org.opencontainers.image.vendor=CentOS org.opencontainers.image.licenses=GPL-2.0-only org.opencontainers.image.created=2020-11-13 00:00:00+00:00
# Wed, 15 Sep 2021 18:20:23 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:2d473b07cdd5f0912cd6f1a703352c82b512407db6b05b43f2553732b55df3bc`  
		Last Modified: Sat, 14 Nov 2020 00:21:39 GMT  
		Size: 76.1 MB (76097157 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:centos7.9.2009` - linux; arm variant v7

```console
$ docker pull centos@sha256:1cef7b00a46a2a90c62eef9a9707d01d19c2f5e253b7ccc574ccaa775063d7e0
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **70.0 MB (70029389 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:df85be4d57c4a6d35c3a4f6a10c7501eeefb08cab83e91ccb073c0cb979936b2`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:02:06 GMT
ADD file:e9a3f27674e8ef05c9fb52ea3995976938e8063024e52e52c8f72f0514f1f10c in / 
# Wed, 15 Sep 2021 18:02:08 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20181205
# Wed, 15 Sep 2021 18:02:08 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:193bcbf05ff9ae85ac1a58cacd9c07f8f4297dc648808c347cceb3797ae603af`  
		Last Modified: Mon, 11 Feb 2019 09:47:52 GMT  
		Size: 70.0 MB (70029389 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:centos7.9.2009` - linux; arm64 variant v8

```console
$ docker pull centos@sha256:864a7acea4a5e8fa7a4d83720fbcbadbe38b183f46f3600e04a3f8c1d961ed87
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **108.4 MB (108374945 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:dfc30428e1631ce2adb2a7978c667f88dc033a3c2991943f5e2307d857ed0370`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 17:39:58 GMT
ADD file:5b1e63a3cb041177b802b501dedcd71a86f1773ea0f69f048f2eb3901097711d in / 
# Wed, 15 Sep 2021 17:39:58 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20201113 org.opencontainers.image.title=CentOS Base Image org.opencontainers.image.vendor=CentOS org.opencontainers.image.licenses=GPL-2.0-only org.opencontainers.image.created=2020-11-13 00:00:00+00:00
# Wed, 15 Sep 2021 17:39:59 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:6717b8ec66cd6add0272c6391165585613c31314a43ff77d9751b53010e531ec`  
		Last Modified: Sat, 14 Nov 2020 00:41:36 GMT  
		Size: 108.4 MB (108374945 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:centos7.9.2009` - linux; 386

```console
$ docker pull centos@sha256:8faead07bd1d5fdd17414a1759eae004da5daa9575a846f4730f44dec0f47843
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **76.7 MB (76701822 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d0be79ecaa51a8dd0d144ac7084e8407d06185aa81c214fb12fdd0ad7b5d3ad1`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 17:38:48 GMT
ADD file:d7dc747583a35caf4429d6d3a67aa63d7293ea091ad41c0d029217717c27ed0f in / 
# Wed, 15 Sep 2021 17:38:49 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20201113 org.opencontainers.image.title=CentOS Base Image org.opencontainers.image.vendor=CentOS org.opencontainers.image.licenses=GPL-2.0-only org.opencontainers.image.created=2020-11-13 00:00:00+00:00
# Wed, 15 Sep 2021 17:38:49 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:ce510b3e78588b800eed5e3c325c3cab32ee9d0804706a14dd193cd1b4661e42`  
		Last Modified: Sat, 14 Nov 2020 00:39:37 GMT  
		Size: 76.7 MB (76701822 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:centos7.9.2009` - linux; ppc64le

```console
$ docker pull centos@sha256:b98bfc4a7094a98ed52d49f9558bb15a4cee9854f182d30085d459d4a55d2e03
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **80.5 MB (80516460 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:fc1c5750d2313b5ced6caf90794a7e6189a1267496926b06d57cdeaec5455c0d`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:29:27 GMT
ADD file:7f21ae7d20a8e347d8b678bcf26be83abb1ee27d3b567c9cddd993e45ce8ac34 in / 
# Wed, 15 Sep 2021 18:29:31 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20201113 org.opencontainers.image.title=CentOS Base Image org.opencontainers.image.vendor=CentOS org.opencontainers.image.licenses=GPL-2.0-only org.opencontainers.image.created=2020-11-13 00:00:00+00:00
# Wed, 15 Sep 2021 18:29:40 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:3fe478aaff9b8f3ba958253e7339e9016ec07c075b805ebfc8cd372ddd01ee64`  
		Last Modified: Tue, 17 Nov 2020 04:06:20 GMT  
		Size: 80.5 MB (80516460 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `centos:centos8`

```console
$ docker pull centos@sha256:a27fd8080b517143cbbbab9dfb7c8571c40d67d534bbdee55bd6c473f432b177
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 3
	-	linux; amd64
	-	linux; arm64 variant v8
	-	linux; ppc64le

### `centos:centos8` - linux; amd64

```console
$ docker pull centos@sha256:a1801b843b1bfaf77c501e7a6d3f709401a1e0c83863037fa3aab063a7fdb9dc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **83.5 MB (83518086 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5d0da3dc976460b72c77d94c8a1ad043720b0416bfc16c52c45d4847e53fadb6`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:20:04 GMT
ADD file:805cb5e15fb6e0bb0326ca33fd2942e068863ce2a8491bb71522c652f31fb466 in / 
# Wed, 15 Sep 2021 18:20:04 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20210915
# Wed, 15 Sep 2021 18:20:05 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:a1d0c75327776413fa0db9ed3adcdbadedc95a662eb1d360dad82bb913f8a1d1`  
		Last Modified: Wed, 15 Sep 2021 18:21:25 GMT  
		Size: 83.5 MB (83518086 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:centos8` - linux; arm64 variant v8

```console
$ docker pull centos@sha256:65a4aad1156d8a0679537cb78519a17eb7142e05a968b26a5361153006224fdc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **83.9 MB (83941353 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e6a0117ec169eda93dc5ca978c6ac87580e36765a66097a6bfb6639a3bd4038a`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 17:39:41 GMT
ADD file:420712a90b0934202b326dc06b73638ab8e4603d12be2c23d67d834eb6cfc052 in / 
# Wed, 15 Sep 2021 17:39:42 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20210915
# Wed, 15 Sep 2021 17:39:42 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:52f9ef134af7dd14738733e567402af86136287d9468978d044780a6435a1193`  
		Last Modified: Wed, 15 Sep 2021 17:40:42 GMT  
		Size: 83.9 MB (83941353 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:centos8` - linux; ppc64le

```console
$ docker pull centos@sha256:8bcb67d4c816e16f79bdea8079c5e30e88f404f4aa8fa124245864830d7852d2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **94.0 MB (93995843 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cc5795ce20d19510cc342b342b04876802e6d0766f5c61e68295e341c0fc12d4`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:28:42 GMT
ADD file:bbae4486bc410c279ebd54b2fc78bcf65f121306acf1eaccacbd9f83f5d6d337 in / 
# Wed, 15 Sep 2021 18:28:50 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20210915
# Wed, 15 Sep 2021 18:28:53 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:ea373b0f63e4f4e7a9529c8258e71e0e897f5ed4eb2af13a8e7e87ceefaa8b5d`  
		Last Modified: Wed, 15 Sep 2021 18:30:35 GMT  
		Size: 94.0 MB (93995843 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `centos:centos8.4.2105`

```console
$ docker pull centos@sha256:a27fd8080b517143cbbbab9dfb7c8571c40d67d534bbdee55bd6c473f432b177
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 3
	-	linux; amd64
	-	linux; arm64 variant v8
	-	linux; ppc64le

### `centos:centos8.4.2105` - linux; amd64

```console
$ docker pull centos@sha256:a1801b843b1bfaf77c501e7a6d3f709401a1e0c83863037fa3aab063a7fdb9dc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **83.5 MB (83518086 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5d0da3dc976460b72c77d94c8a1ad043720b0416bfc16c52c45d4847e53fadb6`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:20:04 GMT
ADD file:805cb5e15fb6e0bb0326ca33fd2942e068863ce2a8491bb71522c652f31fb466 in / 
# Wed, 15 Sep 2021 18:20:04 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20210915
# Wed, 15 Sep 2021 18:20:05 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:a1d0c75327776413fa0db9ed3adcdbadedc95a662eb1d360dad82bb913f8a1d1`  
		Last Modified: Wed, 15 Sep 2021 18:21:25 GMT  
		Size: 83.5 MB (83518086 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:centos8.4.2105` - linux; arm64 variant v8

```console
$ docker pull centos@sha256:65a4aad1156d8a0679537cb78519a17eb7142e05a968b26a5361153006224fdc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **83.9 MB (83941353 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e6a0117ec169eda93dc5ca978c6ac87580e36765a66097a6bfb6639a3bd4038a`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 17:39:41 GMT
ADD file:420712a90b0934202b326dc06b73638ab8e4603d12be2c23d67d834eb6cfc052 in / 
# Wed, 15 Sep 2021 17:39:42 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20210915
# Wed, 15 Sep 2021 17:39:42 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:52f9ef134af7dd14738733e567402af86136287d9468978d044780a6435a1193`  
		Last Modified: Wed, 15 Sep 2021 17:40:42 GMT  
		Size: 83.9 MB (83941353 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:centos8.4.2105` - linux; ppc64le

```console
$ docker pull centos@sha256:8bcb67d4c816e16f79bdea8079c5e30e88f404f4aa8fa124245864830d7852d2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **94.0 MB (93995843 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cc5795ce20d19510cc342b342b04876802e6d0766f5c61e68295e341c0fc12d4`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:28:42 GMT
ADD file:bbae4486bc410c279ebd54b2fc78bcf65f121306acf1eaccacbd9f83f5d6d337 in / 
# Wed, 15 Sep 2021 18:28:50 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20210915
# Wed, 15 Sep 2021 18:28:53 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:ea373b0f63e4f4e7a9529c8258e71e0e897f5ed4eb2af13a8e7e87ceefaa8b5d`  
		Last Modified: Wed, 15 Sep 2021 18:30:35 GMT  
		Size: 94.0 MB (93995843 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `centos:latest`

```console
$ docker pull centos@sha256:a27fd8080b517143cbbbab9dfb7c8571c40d67d534bbdee55bd6c473f432b177
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 3
	-	linux; amd64
	-	linux; arm64 variant v8
	-	linux; ppc64le

### `centos:latest` - linux; amd64

```console
$ docker pull centos@sha256:a1801b843b1bfaf77c501e7a6d3f709401a1e0c83863037fa3aab063a7fdb9dc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **83.5 MB (83518086 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5d0da3dc976460b72c77d94c8a1ad043720b0416bfc16c52c45d4847e53fadb6`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:20:04 GMT
ADD file:805cb5e15fb6e0bb0326ca33fd2942e068863ce2a8491bb71522c652f31fb466 in / 
# Wed, 15 Sep 2021 18:20:04 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20210915
# Wed, 15 Sep 2021 18:20:05 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:a1d0c75327776413fa0db9ed3adcdbadedc95a662eb1d360dad82bb913f8a1d1`  
		Last Modified: Wed, 15 Sep 2021 18:21:25 GMT  
		Size: 83.5 MB (83518086 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:latest` - linux; arm64 variant v8

```console
$ docker pull centos@sha256:65a4aad1156d8a0679537cb78519a17eb7142e05a968b26a5361153006224fdc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **83.9 MB (83941353 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e6a0117ec169eda93dc5ca978c6ac87580e36765a66097a6bfb6639a3bd4038a`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 17:39:41 GMT
ADD file:420712a90b0934202b326dc06b73638ab8e4603d12be2c23d67d834eb6cfc052 in / 
# Wed, 15 Sep 2021 17:39:42 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20210915
# Wed, 15 Sep 2021 17:39:42 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:52f9ef134af7dd14738733e567402af86136287d9468978d044780a6435a1193`  
		Last Modified: Wed, 15 Sep 2021 17:40:42 GMT  
		Size: 83.9 MB (83941353 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `centos:latest` - linux; ppc64le

```console
$ docker pull centos@sha256:8bcb67d4c816e16f79bdea8079c5e30e88f404f4aa8fa124245864830d7852d2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **94.0 MB (93995843 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cc5795ce20d19510cc342b342b04876802e6d0766f5c61e68295e341c0fc12d4`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:28:42 GMT
ADD file:bbae4486bc410c279ebd54b2fc78bcf65f121306acf1eaccacbd9f83f5d6d337 in / 
# Wed, 15 Sep 2021 18:28:50 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20210915
# Wed, 15 Sep 2021 18:28:53 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:ea373b0f63e4f4e7a9529c8258e71e0e897f5ed4eb2af13a8e7e87ceefaa8b5d`  
		Last Modified: Wed, 15 Sep 2021 18:30:35 GMT  
		Size: 94.0 MB (93995843 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
