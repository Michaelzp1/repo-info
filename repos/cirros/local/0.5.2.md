# `cirros:0.5.2`

## Docker Metadata

- Image ID: `sha256:f9cae1daf5f682cb6403a766b3e6afd73a102296910f27ea1ec392b54dc0c188`
- Created: `2021-03-08T21:36:45.370448306Z`
- Virtual Size: ~ 12.56 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["/sbin/init"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
