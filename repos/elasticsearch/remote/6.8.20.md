## `elasticsearch:6.8.20`

```console
$ docker pull elasticsearch@sha256:70889ef6ff6ab6df8ade85428e47dabc7fa114e33e540333e4b0829af46468f7
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `elasticsearch:6.8.20` - linux; amd64

```console
$ docker pull elasticsearch@sha256:703d0ee684ec16cf8f923ebafd02e0b0c81d16a6f654f670a3efdbd7e4cba4a3
```

-	Docker Version: 20.10.6
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **483.0 MB (483040639 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:86ac76565a15d27de702318cc19e3195d22701b79477c8766c79cf312b0394bf`
-	Entrypoint: `["\/usr\/local\/bin\/docker-entrypoint.sh"]`
-	Default Command: `["eswrapper"]`

```dockerfile
# Wed, 15 Sep 2021 18:20:23 GMT
ADD file:b3ebbe8bd304723d43b7b44a6d990cd657b63d93d6a2a9293983a30bfc1dfa53 in / 
# Wed, 15 Sep 2021 18:20:23 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20201113 org.opencontainers.image.title=CentOS Base Image org.opencontainers.image.vendor=CentOS org.opencontainers.image.licenses=GPL-2.0-only org.opencontainers.image.created=2020-11-13 00:00:00+00:00
# Wed, 15 Sep 2021 18:20:23 GMT
CMD ["/bin/bash"]
# Thu, 07 Oct 2021 22:04:43 GMT
ENV ELASTIC_CONTAINER=true
# Thu, 07 Oct 2021 22:04:43 GMT
ENV JAVA_HOME=/opt/jdk-15.0.1+9
# Thu, 07 Oct 2021 22:04:48 GMT
COPY dir:8d79ae5f21bb18379c0d92b3d252f4730fec22a4509252ec794212b8f72bd7af in /opt/jdk-15.0.1+9 
# Thu, 07 Oct 2021 22:05:37 GMT
RUN for iter in {1..10}; do yum update  --setopt=tsflags=nodocs -y &&     yum install -y  --setopt=tsflags=nodocs nc unzip wget which &&     yum clean all && exit_code=0 && break || exit_code=\$? && echo "yum error: retry $iter in 10s" && sleep 10; done;     (exit $exit_code)
# Thu, 07 Oct 2021 22:05:39 GMT
RUN groupadd -g 1000 elasticsearch &&     adduser -u 1000 -g 1000 -G 0 -d /usr/share/elasticsearch elasticsearch &&     chmod 0775 /usr/share/elasticsearch &&     chgrp 0 /usr/share/elasticsearch
# Thu, 07 Oct 2021 22:05:39 GMT
WORKDIR /usr/share/elasticsearch
# Thu, 07 Oct 2021 22:05:44 GMT
COPY --chown=1000:0dir:ffb6642bc44428e21004f343f678b841a4fd48fdceaf5f9a17c6aead48dfad5a in /usr/share/elasticsearch 
# Thu, 07 Oct 2021 22:05:44 GMT
ENV PATH=/usr/share/elasticsearch/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
# Thu, 07 Oct 2021 22:05:44 GMT
COPY --chown=1000:0file:08193f849fc25f29db1438eff6d5c9fe1d63237aeb07a3e0009e8ba554f97c31 in /usr/local/bin/docker-entrypoint.sh 
# Thu, 07 Oct 2021 22:05:45 GMT
RUN chgrp 0 /usr/local/bin/docker-entrypoint.sh &&     chmod g=u /etc/passwd &&     chmod 0775 /usr/local/bin/docker-entrypoint.sh
# Thu, 07 Oct 2021 22:05:45 GMT
EXPOSE 9200 9300
# Thu, 07 Oct 2021 22:05:46 GMT
LABEL org.label-schema.build-date=2021-10-07T22:00:24.085009Z org.label-schema.license=Elastic-License org.label-schema.name=Elasticsearch org.label-schema.schema-version=1.0 org.label-schema.url=https://www.elastic.co/products/elasticsearch org.label-schema.usage=https://www.elastic.co/guide/en/elasticsearch/reference/index.html org.label-schema.vcs-ref=c859302dcfa51ee10915348ce467453130ed8b97 org.label-schema.vcs-url=https://github.com/elastic/elasticsearch org.label-schema.vendor=Elastic org.label-schema.version=6.8.20 org.opencontainers.image.created=2021-10-07T22:00:24.085009Z org.opencontainers.image.documentation=https://www.elastic.co/guide/en/elasticsearch/reference/index.html org.opencontainers.image.licenses=Elastic-License org.opencontainers.image.revision=c859302dcfa51ee10915348ce467453130ed8b97 org.opencontainers.image.source=https://github.com/elastic/elasticsearch org.opencontainers.image.title=Elasticsearch org.opencontainers.image.url=https://www.elastic.co/products/elasticsearch org.opencontainers.image.vendor=Elastic org.opencontainers.image.version=6.8.20
# Thu, 07 Oct 2021 22:05:46 GMT
ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]
# Thu, 07 Oct 2021 22:05:46 GMT
CMD ["eswrapper"]
```

-	Layers:
	-	`sha256:2d473b07cdd5f0912cd6f1a703352c82b512407db6b05b43f2553732b55df3bc`  
		Last Modified: Sat, 14 Nov 2020 00:21:39 GMT  
		Size: 76.1 MB (76097157 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:f24a4cc54c7c368727fe395785960f69a0dbe1fe8fca5bdbbe0c8d45a8dc51d8`  
		Last Modified: Thu, 14 Oct 2021 14:13:31 GMT  
		Size: 207.7 MB (207657728 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:1b6df3adfb8f398060c16b9e9ee78862e7deae89bf64ba501e709c71e595b402`  
		Last Modified: Thu, 14 Oct 2021 14:13:23 GMT  
		Size: 49.1 MB (49138569 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:a75da569075e87317d63fa7e45b18ced12bb07fda73169ddfeb1bc913062980c`  
		Last Modified: Thu, 14 Oct 2021 14:13:15 GMT  
		Size: 2.3 KB (2312 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:787abc13b0c35727891900ef2d218b89343dcb84e9dd8e4d85083ea690f83d76`  
		Last Modified: Thu, 14 Oct 2021 14:13:25 GMT  
		Size: 150.1 MB (150140409 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:9392e421ee4302715968c11bfaf914be2f375d258027cf632e6987e9303e5888`  
		Last Modified: Thu, 14 Oct 2021 14:13:14 GMT  
		Size: 2.1 KB (2064 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:97ce69fc8e63b9df5f043722782f8e8158015660c7ab54a1b174ce81653acea5`  
		Last Modified: Thu, 14 Oct 2021 14:13:14 GMT  
		Size: 2.4 KB (2400 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
