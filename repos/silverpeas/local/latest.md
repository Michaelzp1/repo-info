# `silverpeas:6.2.1`

## Docker Metadata

- Image ID: `sha256:92894ed5fd296d1a7e5a379a3be910f468d6bc50dc6fe5b09b1e89ee7a6a1405`
- Created: `2021-10-16T03:23:30.266689378Z`
- Virtual Size: ~ 3.19 Gb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["/opt/run.sh"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `TERM=xterm`
  - `LANG=en_US.UTF-8`
  - `LANGUAGE=en_US.UTF-8`
  - `LC_ALL=en_US.UTF-8`
  - `JAVA_HOME=/docker-java-home`
  - `SILVERPEAS_HOME=/opt/silverpeas`
  - `JBOSS_HOME=/opt/wildfly`
  - `SILVERPEAS_VERSION=6.2.1`
  - `WILDFLY_VERSION=20.0.1`
- Labels:
  - `build=1`
  - `description=Image to install and to run Silverpeas 6.2.1`
  - `name=Silverpeas 6.2.1`
  - `vendor=Silverpeas`
  - `version=6.2.1`

## `dpkg` (`.deb`-based packages)

### `dpkg` source package: `acl=2.2.53-6`

Binary Packages:

- `libacl1:amd64=2.2.53-6`

Licenses: (parsed from: `/usr/share/doc/libacl1/copyright`)

- `GPL-2`
- `GPL-2+`
- `LGPL-2+`
- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris acl=2.2.53-6
'http://archive.ubuntu.com/ubuntu/pool/main/a/acl/acl_2.2.53-6.dsc' acl_2.2.53-6.dsc 2336 SHA256:02dad794aa09133e557552d75568324ed3e84fb56e93626e67993cf54a97df34
'http://archive.ubuntu.com/ubuntu/pool/main/a/acl/acl_2.2.53.orig.tar.gz' acl_2.2.53.orig.tar.gz 524300 SHA256:06be9865c6f418d851ff4494e12406568353b891ffe1f596b34693c387af26c7
'http://archive.ubuntu.com/ubuntu/pool/main/a/acl/acl_2.2.53.orig.tar.gz.asc' acl_2.2.53.orig.tar.gz.asc 833 SHA256:06849bece0b56a6a7269173abe101cff223bb9346d74027a3cd5ff80914abf4b
'http://archive.ubuntu.com/ubuntu/pool/main/a/acl/acl_2.2.53-6.debian.tar.xz' acl_2.2.53-6.debian.tar.xz 25108 SHA256:c80e6150d9b213e52f5e65ff78d4ee95a71b5a258c1f8b980365d20ed1753a5c
```

### `dpkg` source package: `adduser=3.118ubuntu2`

Binary Packages:

- `adduser=3.118ubuntu2`

Licenses: (parsed from: `/usr/share/doc/adduser/copyright`)

- `GPL-2`

Source:

```console
$ apt-get source -qq --print-uris adduser=3.118ubuntu2
'http://archive.ubuntu.com/ubuntu/pool/main/a/adduser/adduser_3.118ubuntu2.dsc' adduser_3.118ubuntu2.dsc 1131 SHA256:785f99d8c75c972cd42d3fab3afa07f97299bb1d70013fe5d295f045224774bb
'http://archive.ubuntu.com/ubuntu/pool/main/a/adduser/adduser_3.118ubuntu2.tar.xz' adduser_3.118ubuntu2.tar.xz 222364 SHA256:9429124c39c381b541005da6f0ae29831bd6533dd65c923e06ca2a7c310db382
```

### `dpkg` source package: `adwaita-icon-theme=3.36.1-2ubuntu0.20.04.2`

Binary Packages:

- `adwaita-icon-theme=3.36.1-2ubuntu0.20.04.2`

Licenses: (parsed from: `/usr/share/doc/adwaita-icon-theme/copyright`)

- `CC-BY-3.0-US`
- `CC-BY-SA-2.0-IT`
- `CC-BY-SA-2.0-IT,`
- `CC-BY-SA-3.0`
- `CC-BY-SA-3.0-US`
- `CC-BY-SA-3.0-Unported`
- `GFDL-1.2`
- `GFDL-1.2+`
- `GPL`
- `GPL-unspecified`
- `LGPL-3`

Source:

```console
$ apt-get source -qq --print-uris adwaita-icon-theme=3.36.1-2ubuntu0.20.04.2
'http://archive.ubuntu.com/ubuntu/pool/main/a/adwaita-icon-theme/adwaita-icon-theme_3.36.1-2ubuntu0.20.04.2.dsc' adwaita-icon-theme_3.36.1-2ubuntu0.20.04.2.dsc 1937 SHA512:ab41a7dd8e8b727b904b3abb5e3eabb24ed2da109f0a0bad0eccb5237525bedc52718a53a95d2c2142e06d50c63747846a9ea0f2c57e5de99c0922e48d62a45c
'http://archive.ubuntu.com/ubuntu/pool/main/a/adwaita-icon-theme/adwaita-icon-theme_3.36.1.orig.tar.xz' adwaita-icon-theme_3.36.1.orig.tar.xz 17247020 SHA512:5df33c8592ebd17e05fb36590423177fa3e07345565302a5b7ef58fb7d7a9b2b3a1789907a81f5ca0abc7446f7e83af059ca9d6432e8182d5274394c79a24fb9
'http://archive.ubuntu.com/ubuntu/pool/main/a/adwaita-icon-theme/adwaita-icon-theme_3.36.1-2ubuntu0.20.04.2.debian.tar.xz' adwaita-icon-theme_3.36.1-2ubuntu0.20.04.2.debian.tar.xz 30356 SHA512:dbd6542afe35e09d3f91405d07b464fee3aa1cd9c8ba2498e16d17365002409c07c120b452e873e60c51a914273828617824864f1abffad17526f379872e4dfc
```

### `dpkg` source package: `alsa-lib=1.2.2-2.1ubuntu2.4`

Binary Packages:

- `libasound2:amd64=1.2.2-2.1ubuntu2.4`
- `libasound2-data=1.2.2-2.1ubuntu2.4`

Licenses: (parsed from: `/usr/share/doc/libasound2/copyright`, `/usr/share/doc/libasound2-data/copyright`)

- `LGPL-2.1`
- `LPGL-2.1+`

Source:

```console
$ apt-get source -qq --print-uris alsa-lib=1.2.2-2.1ubuntu2.4
'http://archive.ubuntu.com/ubuntu/pool/main/a/alsa-lib/alsa-lib_1.2.2-2.1ubuntu2.4.dsc' alsa-lib_1.2.2-2.1ubuntu2.4.dsc 2029 SHA512:56ba169275fb8f0d6c02b3130796a4271104cbf013ffac0d8e9567c1ad3651bb44706b14c0f04c5e82d37a19c2646994799983199987d27524bd0afcf200620e
'http://archive.ubuntu.com/ubuntu/pool/main/a/alsa-lib/alsa-lib_1.2.2.orig.tar.bz2' alsa-lib_1.2.2.orig.tar.bz2 1030747 SHA512:d21adb3ff998918c7d1820f9ce2aaf4202dd45ccb87cb092d49da8b2402b6ddaad06325be0fd59f17393a5d9958e3743bfccb4b14bdb947a42e7d791d73c7033
'http://archive.ubuntu.com/ubuntu/pool/main/a/alsa-lib/alsa-lib_1.2.2-2.1ubuntu2.4.debian.tar.xz' alsa-lib_1.2.2-2.1ubuntu2.4.debian.tar.xz 59000 SHA512:9c359539a19f18169ba37e8731fb29707b0a1eaefdc17bf66e6b5243d5f61032693245787d09fa86dc4bd3e639e41f76caf0a7a1d383ee0c40bf36c1101eeaea
```

### `dpkg` source package: `alsa-topology-conf=1.2.2-1`

Binary Packages:

- `alsa-topology-conf=1.2.2-1`

Licenses: (parsed from: `/usr/share/doc/alsa-topology-conf/copyright`)

- `BSD-3-clause`

Source:

```console
$ apt-get source -qq --print-uris alsa-topology-conf=1.2.2-1
'http://archive.ubuntu.com/ubuntu/pool/main/a/alsa-topology-conf/alsa-topology-conf_1.2.2-1.dsc' alsa-topology-conf_1.2.2-1.dsc 2080 SHA256:944e4d481742a09e6c120e532f69c400c88860815297efa8b461befe531b121a
'http://archive.ubuntu.com/ubuntu/pool/main/a/alsa-topology-conf/alsa-topology-conf_1.2.2.orig.tar.bz2' alsa-topology-conf_1.2.2.orig.tar.bz2 6346 SHA256:b472d6b567c78173bd69543d9cffc9e379c80eb763c3afc8d5b24d5610d19425
'http://archive.ubuntu.com/ubuntu/pool/main/a/alsa-topology-conf/alsa-topology-conf_1.2.2-1.debian.tar.xz' alsa-topology-conf_1.2.2-1.debian.tar.xz 2212 SHA256:66e874f2d845434f0f37619bcac2ff51f954219779539f3cceb5b5ef1570a66f
```

### `dpkg` source package: `alsa-ucm-conf=1.2.2-1ubuntu0.10`

Binary Packages:

- `alsa-ucm-conf=1.2.2-1ubuntu0.10`

Licenses: (parsed from: `/usr/share/doc/alsa-ucm-conf/copyright`)

- `BSD-3-clause`

**WARNING:** unable to find source (`apt-get source` failed or returned no results)!  
This is *usually* due to a new package version being released and the old version being removed.


### `dpkg` source package: `aom=1.0.0.errata1-3build1`

Binary Packages:

- `libaom0:amd64=1.0.0.errata1-3build1`

Licenses: (parsed from: `/usr/share/doc/libaom0/copyright`)

- `BSD-2-Clause`
- `BSD-2-clause`
- `BSD-3-clause`
- `Expat`
- `ISC`
- `public-domain-md5`

Source:

```console
$ apt-get source -qq --print-uris aom=1.0.0.errata1-3build1
'http://archive.ubuntu.com/ubuntu/pool/universe/a/aom/aom_1.0.0.errata1-3build1.dsc' aom_1.0.0.errata1-3build1.dsc 2273 SHA256:2258c82054c8377a1610024c3d3a044fda21a43611ac3695f906d1fcf1be2319
'http://archive.ubuntu.com/ubuntu/pool/universe/a/aom/aom_1.0.0.errata1.orig.tar.xz' aom_1.0.0.errata1.orig.tar.xz 1898808 SHA256:1dd501c622d871acf31fb942bd3b73a00883fc10f7c498fec97b22c858ca415f
'http://archive.ubuntu.com/ubuntu/pool/universe/a/aom/aom_1.0.0.errata1-3build1.debian.tar.xz' aom_1.0.0.errata1-3build1.debian.tar.xz 21208 SHA256:6a397c5e85f1fe1f5139d72c52da99d6645313420985d26734fdbee21cdc9d35
```

### `dpkg` source package: `apache-pom=18-1`

Binary Packages:

- `libapache-pom-java=18-1`

Licenses: (parsed from: `/usr/share/doc/libapache-pom-java/copyright`)

- `Apache-2.0`

Source:

```console
$ apt-get source -qq --print-uris apache-pom=18-1
'http://archive.ubuntu.com/ubuntu/pool/universe/a/apache-pom/apache-pom_18-1.dsc' apache-pom_18-1.dsc 2045 SHA256:85ae428afdba43d01fe7d7f942322ceb725781fe3e42f2f4915b5e55e165a633
'http://archive.ubuntu.com/ubuntu/pool/universe/a/apache-pom/apache-pom_18.orig.tar.gz' apache-pom_18.orig.tar.gz 8107 SHA256:5a7e2a1ea9767998929722bbd6c2f34f5a2b3c1cbd14e5210c5465c937acbc36
'http://archive.ubuntu.com/ubuntu/pool/universe/a/apache-pom/apache-pom_18-1.debian.tar.xz' apache-pom_18-1.debian.tar.xz 2804 SHA256:93ea901e12006f057982e7b79dcc5cb205c1fbd886350a3115c8d9c2d8fec593
```

### `dpkg` source package: `apparmor=2.13.3-7ubuntu5.1`

Binary Packages:

- `apparmor=2.13.3-7ubuntu5.1`
- `libapparmor1:amd64=2.13.3-7ubuntu5.1`

Licenses: (parsed from: `/usr/share/doc/apparmor/copyright`, `/usr/share/doc/libapparmor1/copyright`)

- `BSD-3-clause`
- `GPL-2`
- `GPL-2+`
- `LGPL-2.1`
- `LGPL-2.1+`

Source:

```console
$ apt-get source -qq --print-uris apparmor=2.13.3-7ubuntu5.1
'http://archive.ubuntu.com/ubuntu/pool/main/a/apparmor/apparmor_2.13.3-7ubuntu5.1.dsc' apparmor_2.13.3-7ubuntu5.1.dsc 3322 SHA512:90869fc59de29022722beb99cc0274ea16fa93e45d1e80a59ff3b6dd51ce3a3a9de3e1f110ad0c1a7f67a1a8c85f76589989375dcdb2cc1e85d81d8f6aa11fc1
'http://archive.ubuntu.com/ubuntu/pool/main/a/apparmor/apparmor_2.13.3.orig.tar.gz' apparmor_2.13.3.orig.tar.gz 7384974 SHA512:137b2bf026ec655b662e9c264d7d48d878db474a3f1cc5a38bfd7df2f85b682bddb77b091ab5595178231a0a262c9ae9cdd61409461cd889bdee156906ef1141
'http://archive.ubuntu.com/ubuntu/pool/main/a/apparmor/apparmor_2.13.3-7ubuntu5.1.debian.tar.xz' apparmor_2.13.3-7ubuntu5.1.debian.tar.xz 107436 SHA512:f107a0427aaca7dc706613952cbc159ca31613af71d930c7f53d91928ee9937d7c8030d4677109214dc121af322028382ecdec698d71c94c12423f178450224e
```

### `dpkg` source package: `apt=2.0.6`

Binary Packages:

- `apt=2.0.6`
- `apt-utils=2.0.6`
- `libapt-pkg6.0:amd64=2.0.6`

Licenses: (parsed from: `/usr/share/doc/apt/copyright`, `/usr/share/doc/apt-utils/copyright`, `/usr/share/doc/libapt-pkg6.0/copyright`)

- `GPL-2`
- `GPLv2+`

Source:

```console
$ apt-get source -qq --print-uris apt=2.0.6
'http://archive.ubuntu.com/ubuntu/pool/main/a/apt/apt_2.0.6.dsc' apt_2.0.6.dsc 2835 SHA512:71a27905e35c3c53ed7e7cb255c203ff24b49a53e81d802425eca2ec6b3b587b54d54046f8970e79b51d2d23034b15509dda517d54cfd3699ab0f22111763a23
'http://archive.ubuntu.com/ubuntu/pool/main/a/apt/apt_2.0.6.tar.xz' apt_2.0.6.tar.xz 2176328 SHA512:a62ca7245039791384137cc5a417a876346e570e1a6c3d9856242003ee393e91c013fe7df2154792ba0b222a95cfda31847a654c444061587353d9020d69a167
```

### `dpkg` source package: `argon2=0~20171227-0.2`

Binary Packages:

- `libargon2-1:amd64=0~20171227-0.2`

Licenses: (parsed from: `/usr/share/doc/libargon2-1/copyright`)

- `Apache-2.0`
- `CC0`

Source:

```console
$ apt-get source -qq --print-uris argon2=0~20171227-0.2
'http://archive.ubuntu.com/ubuntu/pool/main/a/argon2/argon2_0~20171227-0.2.dsc' argon2_0~20171227-0.2.dsc 2108 SHA256:357d1e93318d7dd3bee401ee9cd92bd0f3ecaab3990013580a12306efda4ebf7
'http://archive.ubuntu.com/ubuntu/pool/main/a/argon2/argon2_0~20171227.orig.tar.gz' argon2_0~20171227.orig.tar.gz 1503745 SHA256:eaea0172c1f4ee4550d1b6c9ce01aab8d1ab66b4207776aa67991eb5872fdcd8
'http://archive.ubuntu.com/ubuntu/pool/main/a/argon2/argon2_0~20171227-0.2.debian.tar.xz' argon2_0~20171227-0.2.debian.tar.xz 6932 SHA256:49e630c0027ebbe0b53e3e692ce99da750e9bdfeddcebf303e595b4af5a2142f
```

### `dpkg` source package: `at-spi2-atk=2.34.2-0ubuntu2~20.04.1`

Binary Packages:

- `libatk-bridge2.0-0:amd64=2.34.2-0ubuntu2~20.04.1`

Licenses: (parsed from: `/usr/share/doc/libatk-bridge2.0-0/copyright`)

- `GPL-2`
- `LGPL-2`
- `LGPL-2+`

Source:

```console
$ apt-get source -qq --print-uris at-spi2-atk=2.34.2-0ubuntu2~20.04.1
'http://archive.ubuntu.com/ubuntu/pool/main/a/at-spi2-atk/at-spi2-atk_2.34.2-0ubuntu2~20.04.1.dsc' at-spi2-atk_2.34.2-0ubuntu2~20.04.1.dsc 2704 SHA512:cc98a20c9161f3cbd3e3650fffc2e6267f73eb6b4b39489ae97ed20add1a46288d3752e01e87d3dd9e2338a6892585ca6cee6917cf21cf3dbeee329687bfee5b
'http://archive.ubuntu.com/ubuntu/pool/main/a/at-spi2-atk/at-spi2-atk_2.34.2.orig.tar.xz' at-spi2-atk_2.34.2.orig.tar.xz 96608 SHA512:59e7ad5c944748ca00af8b0a9df03c9ffbc6afae6e65c25a2566a9e2a30e66724c4492076be1730c2894c636f82c795c533669572584d8d5675f68b349ad16c4
'http://archive.ubuntu.com/ubuntu/pool/main/a/at-spi2-atk/at-spi2-atk_2.34.2-0ubuntu2~20.04.1.debian.tar.xz' at-spi2-atk_2.34.2-0ubuntu2~20.04.1.debian.tar.xz 10508 SHA512:de0c529ab7f33f6542ff02e17343874f3b3e4254133fd561adf007317913d2ca697d9ac5f2ff1d13cc02d4d1ebf80ff5ea3ab98ebc230a55710e302be3759235
```

### `dpkg` source package: `at-spi2-core=2.36.0-2`

Binary Packages:

- `at-spi2-core=2.36.0-2`
- `libatspi2.0-0:amd64=2.36.0-2`

Licenses: (parsed from: `/usr/share/doc/at-spi2-core/copyright`, `/usr/share/doc/libatspi2.0-0/copyright`)

- `AFL-2.1`
- `GPL-2`
- `GPL-2+`
- `LGPL-2`
- `LGPL-2+`
- `public-domain`

Source:

```console
$ apt-get source -qq --print-uris at-spi2-core=2.36.0-2
'http://archive.ubuntu.com/ubuntu/pool/main/a/at-spi2-core/at-spi2-core_2.36.0-2.dsc' at-spi2-core_2.36.0-2.dsc 2675 SHA256:a1eba0f65d0de5c43f9964413fa1e71725c5a75ff18c119dfa8f1ca1d768e9a9
'http://archive.ubuntu.com/ubuntu/pool/main/a/at-spi2-core/at-spi2-core_2.36.0.orig.tar.xz' at-spi2-core_2.36.0.orig.tar.xz 186824 SHA256:88da57de0a7e3c60bc341a974a80fdba091612db3547c410d6deab039ca5c05a
'http://archive.ubuntu.com/ubuntu/pool/main/a/at-spi2-core/at-spi2-core_2.36.0-2.debian.tar.xz' at-spi2-core_2.36.0-2.debian.tar.xz 11036 SHA256:a220220faef2e1363e8a7de035891ab9c81ac19b208eb0e13fc7a19555c582a4
```

### `dpkg` source package: `atk1.0=2.35.1-1ubuntu2`

Binary Packages:

- `libatk1.0-0:amd64=2.35.1-1ubuntu2`
- `libatk1.0-data=2.35.1-1ubuntu2`

Licenses: (parsed from: `/usr/share/doc/libatk1.0-0/copyright`, `/usr/share/doc/libatk1.0-data/copyright`)

- `LGPL-2`

Source:

```console
$ apt-get source -qq --print-uris atk1.0=2.35.1-1ubuntu2
'http://archive.ubuntu.com/ubuntu/pool/main/a/atk1.0/atk1.0_2.35.1-1ubuntu2.dsc' atk1.0_2.35.1-1ubuntu2.dsc 2892 SHA256:0fc79edbda262b81f27a21f4f410e45d66db6d293f21f69acaadbce49f7cf680
'http://archive.ubuntu.com/ubuntu/pool/main/a/atk1.0/atk1.0_2.35.1.orig.tar.xz' atk1.0_2.35.1.orig.tar.xz 294224 SHA256:be9360fa3f845e91f001c20e73b3a0315b38983411b1dc008195f779ac543884
'http://archive.ubuntu.com/ubuntu/pool/main/a/atk1.0/atk1.0_2.35.1-1ubuntu2.debian.tar.xz' atk1.0_2.35.1-1ubuntu2.debian.tar.xz 13096 SHA256:25821c5a7b675170c9014474c6703deca57074fead11077a5fe2149aefd32a53
```

### `dpkg` source package: `attr=1:2.4.48-5`

Binary Packages:

- `libattr1:amd64=1:2.4.48-5`

Licenses: (parsed from: `/usr/share/doc/libattr1/copyright`)

- `GPL-2`
- `GPL-2+`
- `LGPL-2+`
- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris attr=1:2.4.48-5
'http://archive.ubuntu.com/ubuntu/pool/main/a/attr/attr_2.4.48-5.dsc' attr_2.4.48-5.dsc 2433 SHA256:0b20a285b758083e2e202ffdd2930cef1bf84fee498791fc3e26b69a3bced01d
'http://archive.ubuntu.com/ubuntu/pool/main/a/attr/attr_2.4.48.orig.tar.gz' attr_2.4.48.orig.tar.gz 467840 SHA256:5ead72b358ec709ed00bbf7a9eaef1654baad937c001c044fe8b74c57f5324e7
'http://archive.ubuntu.com/ubuntu/pool/main/a/attr/attr_2.4.48.orig.tar.gz.asc' attr_2.4.48.orig.tar.gz.asc 833 SHA256:5d23c2c83cc13d170f1c209f48d0efa1fc46d16487b790e9996c5206dcfe0395
'http://archive.ubuntu.com/ubuntu/pool/main/a/attr/attr_2.4.48-5.debian.tar.xz' attr_2.4.48-5.debian.tar.xz 25560 SHA256:02238253d324250dabdc0434f7de045d85df93458995a465ac434f2a3978a312
```

### `dpkg` source package: `audit=1:2.8.5-2ubuntu6`

Binary Packages:

- `libaudit-common=1:2.8.5-2ubuntu6`
- `libaudit1:amd64=1:2.8.5-2ubuntu6`

Licenses: (parsed from: `/usr/share/doc/libaudit-common/copyright`, `/usr/share/doc/libaudit1/copyright`)

- `GPL-1`
- `GPL-2`
- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris audit=1:2.8.5-2ubuntu6
'http://archive.ubuntu.com/ubuntu/pool/main/a/audit/audit_2.8.5-2ubuntu6.dsc' audit_2.8.5-2ubuntu6.dsc 2764 SHA256:b149fad8217d68a80299c1ef72539ee7d756146d692b7e51eade7341e60ac528
'http://archive.ubuntu.com/ubuntu/pool/main/a/audit/audit_2.8.5.orig.tar.gz' audit_2.8.5.orig.tar.gz 1140694 SHA256:0e5d4103646e00f8d1981e1cd2faea7a2ae28e854c31a803e907a383c5e2ecb7
'http://archive.ubuntu.com/ubuntu/pool/main/a/audit/audit_2.8.5-2ubuntu6.debian.tar.xz' audit_2.8.5-2ubuntu6.debian.tar.xz 18712 SHA256:d85ecf206bfe256a86e6d39602cd2744beda264a28e413f31c4da227e6542ea7
```

### `dpkg` source package: `avahi=0.7-4ubuntu7.1`

Binary Packages:

- `libavahi-client3:amd64=0.7-4ubuntu7.1`
- `libavahi-common-data:amd64=0.7-4ubuntu7.1`
- `libavahi-common3:amd64=0.7-4ubuntu7.1`

Licenses: (parsed from: `/usr/share/doc/libavahi-client3/copyright`, `/usr/share/doc/libavahi-common-data/copyright`, `/usr/share/doc/libavahi-common3/copyright`)

- `GPL`
- `GPL-2`
- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris avahi=0.7-4ubuntu7.1
'http://archive.ubuntu.com/ubuntu/pool/main/a/avahi/avahi_0.7-4ubuntu7.1.dsc' avahi_0.7-4ubuntu7.1.dsc 4171 SHA512:073a4f11bf28de71ff5d013d3ad6c7e9b86282b1f0067d97360c3d36776c3e21720ba08a636fcfe172692fded74a1a66a13f667839727f202cfc298f24cc3054
'http://archive.ubuntu.com/ubuntu/pool/main/a/avahi/avahi_0.7.orig.tar.gz' avahi_0.7.orig.tar.gz 1333400 SHA512:bae5a1e9204aca90b90e7fd223d19e809e3514d03ba5fa2da1e55bf1d72d3d3b98567f357900c36393613dc17dc98e15ff3ebf0f226f2f6b9766e592452a6ce7
'http://archive.ubuntu.com/ubuntu/pool/main/a/avahi/avahi_0.7-4ubuntu7.1.debian.tar.xz' avahi_0.7-4ubuntu7.1.debian.tar.xz 36940 SHA512:b6bff1f8e7ea7f9fc7ef228036091d9529e9a8f8960cf2f1ac8b21f8f2cd77e327538aed9477ba823ede215ef8a8767db2339d55c70e6b267e3bbdbc4e14a78e
```

### `dpkg` source package: `base-files=11ubuntu5.4`

Binary Packages:

- `base-files=11ubuntu5.4`

Licenses: (parsed from: `/usr/share/doc/base-files/copyright`)

- `GPL`

Source:

```console
$ apt-get source -qq --print-uris base-files=11ubuntu5.4
'http://archive.ubuntu.com/ubuntu/pool/main/b/base-files/base-files_11ubuntu5.4.dsc' base-files_11ubuntu5.4.dsc 1331 SHA512:e312d34034ef8d471488894159d6eda5f21f8d9255ace701ddbe051c37546f0f4465ad655eaca80375c6f565a95c2e540a54693400c62f982bb67e995d212430
'http://archive.ubuntu.com/ubuntu/pool/main/b/base-files/base-files_11ubuntu5.4.tar.xz' base-files_11ubuntu5.4.tar.xz 80576 SHA512:11e6e0b05c5c3ae9bfcc8568deb1a9ac765b4dd6ce741ae988ad783e4d58d861de2344219422931f5fc775b1bedf456bb98d2e841e00dae5e08686564d841690
```

### `dpkg` source package: `base-passwd=3.5.47`

Binary Packages:

- `base-passwd=3.5.47`

Licenses: (parsed from: `/usr/share/doc/base-passwd/copyright`)

- `GPL-2`
- `PD`

Source:

```console
$ apt-get source -qq --print-uris base-passwd=3.5.47
'http://archive.ubuntu.com/ubuntu/pool/main/b/base-passwd/base-passwd_3.5.47.dsc' base-passwd_3.5.47.dsc 1757 SHA256:5a77a4cce51d1eb72e9d96d4083c641435c05888922c7bd3fa6b4395bf9afad3
'http://archive.ubuntu.com/ubuntu/pool/main/b/base-passwd/base-passwd_3.5.47.tar.xz' base-passwd_3.5.47.tar.xz 53024 SHA256:9810ae0216e96bf9fc7ca6163d47ef8ec7d1677f533451af5911d8202a490a23
```

### `dpkg` source package: `bash=5.0-6ubuntu1.1`

Binary Packages:

- `bash=5.0-6ubuntu1.1`

Licenses: (parsed from: `/usr/share/doc/bash/copyright`)

- `GPL-3`

Source:

```console
$ apt-get source -qq --print-uris bash=5.0-6ubuntu1.1
'http://archive.ubuntu.com/ubuntu/pool/main/b/bash/bash_5.0-6ubuntu1.1.dsc' bash_5.0-6ubuntu1.1.dsc 2418 SHA512:283e6fa78ac1a13e6e3a70efa6f67817871b12c1fe099475dfe6b87fd9f2926406446fabb72a66d8546ad320976761ffe68867a1e90bd6a5f97c07e851df9933
'http://archive.ubuntu.com/ubuntu/pool/main/b/bash/bash_5.0.orig.tar.xz' bash_5.0.orig.tar.xz 5554808 SHA512:f3a719997a8515bae7e84701afafc9b2cdd23c95d29533adb678000b08eba968450b93d5576c3cffbeccbdcd95b713db830e8efeda689258dcfe6f15f0c5dec4
'http://archive.ubuntu.com/ubuntu/pool/main/b/bash/bash_5.0-6ubuntu1.1.debian.tar.xz' bash_5.0-6ubuntu1.1.debian.tar.xz 74356 SHA512:450eacea5316075107da9951356021091dfc96889057769590ae8505fa851a99d2f48ce300281b8a448c87e9573cfa2f68a04369ee97955be204e73fa2fd6385
```

### `dpkg` source package: `boost1.71=1.71.0-6ubuntu6`

Binary Packages:

- `libboost-date-time1.71.0:amd64=1.71.0-6ubuntu6`
- `libboost-filesystem1.71.0:amd64=1.71.0-6ubuntu6`
- `libboost-iostreams1.71.0:amd64=1.71.0-6ubuntu6`
- `libboost-locale1.71.0:amd64=1.71.0-6ubuntu6`
- `libboost-thread1.71.0:amd64=1.71.0-6ubuntu6`

Licenses: (parsed from: `/usr/share/doc/libboost-date-time1.71.0/copyright`, `/usr/share/doc/libboost-filesystem1.71.0/copyright`, `/usr/share/doc/libboost-iostreams1.71.0/copyright`, `/usr/share/doc/libboost-locale1.71.0/copyright`, `/usr/share/doc/libboost-thread1.71.0/copyright`)

- `Apache-2.0`
- `BSD2`
- `BSD3_DEShaw`
- `BSD3_Google`
- `BSL-1.0`
- `Caramel`
- `CrystalClear`
- `HP`
- `Jam`
- `Kempf`
- `MIT`
- `NIST`
- `OldBoost1`
- `OldBoost2`
- `OldBoost3`
- `Python`
- `SGI`
- `Spencer`
- `Zlib`

Source:

```console
$ apt-get source -qq --print-uris boost1.71=1.71.0-6ubuntu6
'http://archive.ubuntu.com/ubuntu/pool/main/b/boost1.71/boost1.71_1.71.0-6ubuntu6.dsc' boost1.71_1.71.0-6ubuntu6.dsc 8517 SHA256:e05b5d8de7633e2bf353e9c75d999eafd93bdf47c993023d38f4e5ef5311557b
'http://archive.ubuntu.com/ubuntu/pool/main/b/boost1.71/boost1.71_1.71.0.orig.tar.xz' boost1.71_1.71.0.orig.tar.xz 56601144 SHA256:e30fb3f666df75fc2ba23403ccbd8bcb0ee5595dc099412b4abde7a9fdde3918
'http://archive.ubuntu.com/ubuntu/pool/main/b/boost1.71/boost1.71_1.71.0-6ubuntu6.debian.tar.xz' boost1.71_1.71.0-6ubuntu6.debian.tar.xz 362348 SHA256:56031ade12bf8ca7c196f11f4afd5d2cc30ab840d2a1f1cec5e7ad87b68addeb
```

### `dpkg` source package: `brotli=1.0.7-6ubuntu0.1`

Binary Packages:

- `libbrotli1:amd64=1.0.7-6ubuntu0.1`

Licenses: (parsed from: `/usr/share/doc/libbrotli1/copyright`)

- `MIT`

Source:

```console
$ apt-get source -qq --print-uris brotli=1.0.7-6ubuntu0.1
'http://archive.ubuntu.com/ubuntu/pool/main/b/brotli/brotli_1.0.7-6ubuntu0.1.dsc' brotli_1.0.7-6ubuntu0.1.dsc 2385 SHA512:139a93e110c6cf50531bdfee5ec4a8751ca81d1e02d2f38b21c1a9a478064286ddeb6bfdf20af488f7e2f53219cf460a00e68b77ef1b860fbf0df67f300d303b
'http://archive.ubuntu.com/ubuntu/pool/main/b/brotli/brotli_1.0.7.orig.tar.gz' brotli_1.0.7.orig.tar.gz 23827908 SHA512:a82362aa36d2f2094bca0b2808d9de0d57291fb3a4c29d7c0ca0a37e73087ec5ac4df299c8c363e61106fccf2fe7f58b5cf76eb97729e2696058ef43b1d3930a
'http://archive.ubuntu.com/ubuntu/pool/main/b/brotli/brotli_1.0.7-6ubuntu0.1.debian.tar.xz' brotli_1.0.7-6ubuntu0.1.debian.tar.xz 13672 SHA512:eb24ee68d0a699bb8f382c7f80c313e0bb26bea6b22f74bf01af236eafe345cf602f7544da4a74eb8c8f70defcd6b867018df97a96e5e894535cf731400edaa8
```

### `dpkg` source package: `bsh=2.0b4-20`

Binary Packages:

- `libbsh-java=2.0b4-20`

Licenses: (parsed from: `/usr/share/doc/libbsh-java/copyright`)

- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris bsh=2.0b4-20
'http://archive.ubuntu.com/ubuntu/pool/universe/b/bsh/bsh_2.0b4-20.dsc' bsh_2.0b4-20.dsc 2153 SHA256:4e0280e91b744734e566ab802956af3f8fcccb4681d6b1349831d81569d6b81f
'http://archive.ubuntu.com/ubuntu/pool/universe/b/bsh/bsh_2.0b4.orig.tar.gz' bsh_2.0b4.orig.tar.gz 826645 SHA256:776a64db4967af4fdfa13e3801eaf4249afbb7ffa1ced13f525fdf44e6e340f7
'http://archive.ubuntu.com/ubuntu/pool/universe/b/bsh/bsh_2.0b4-20.debian.tar.xz' bsh_2.0b4-20.debian.tar.xz 9776 SHA256:ce84129dd92e1fb13ea688930c2042b1fa16f8a1a4c1dd5632b13d227df9befe
```

### `dpkg` source package: `bzip2=1.0.8-2`

Binary Packages:

- `bzip2=1.0.8-2`
- `libbz2-1.0:amd64=1.0.8-2`

Licenses: (parsed from: `/usr/share/doc/bzip2/copyright`, `/usr/share/doc/libbz2-1.0/copyright`)

- `BSD-variant`
- `GPL-2`

Source:

```console
$ apt-get source -qq --print-uris bzip2=1.0.8-2
'http://archive.ubuntu.com/ubuntu/pool/main/b/bzip2/bzip2_1.0.8-2.dsc' bzip2_1.0.8-2.dsc 2180 SHA256:646cdcbb786a89a647cfafb280ef467143c06c445c4bf6fe69ec4a7882943064
'http://archive.ubuntu.com/ubuntu/pool/main/b/bzip2/bzip2_1.0.8.orig.tar.gz' bzip2_1.0.8.orig.tar.gz 810029 SHA256:ab5a03176ee106d3f0fa90e381da478ddae405918153cca248e682cd0c4a2269
'http://archive.ubuntu.com/ubuntu/pool/main/b/bzip2/bzip2_1.0.8-2.debian.tar.bz2' bzip2_1.0.8-2.debian.tar.bz2 26032 SHA256:237c8619bc9bc16f357b1077064a3e58aa1a230dadb4b9bb3bd8dc8f454afc0b
```

### `dpkg` source package: `ca-certificates-java=20190405ubuntu1`

Binary Packages:

- `ca-certificates-java=20190405ubuntu1`

Licenses: (parsed from: `/usr/share/doc/ca-certificates-java/copyright`)

- `GPL`

Source:

```console
$ apt-get source -qq --print-uris ca-certificates-java=20190405ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/c/ca-certificates-java/ca-certificates-java_20190405ubuntu1.dsc' ca-certificates-java_20190405ubuntu1.dsc 1922 SHA256:d9ec82784392c7b7be22d75e2aa4e90803c40af025f636ed22eb794758c341c7
'http://archive.ubuntu.com/ubuntu/pool/main/c/ca-certificates-java/ca-certificates-java_20190405ubuntu1.tar.xz' ca-certificates-java_20190405ubuntu1.tar.xz 17412 SHA256:f121928556e2eaa80d7569b219e7af7e3265f02fc5a147a2ecb5cabcb55f659d
```

### `dpkg` source package: `ca-certificates=20210119~20.04.2`

Binary Packages:

- `ca-certificates=20210119~20.04.2`

Licenses: (parsed from: `/usr/share/doc/ca-certificates/copyright`)

- `GPL-2`
- `GPL-2+`
- `MPL-2.0`

Source:

```console
$ apt-get source -qq --print-uris ca-certificates=20210119~20.04.2
'http://archive.ubuntu.com/ubuntu/pool/main/c/ca-certificates/ca-certificates_20210119~20.04.2.dsc' ca-certificates_20210119~20.04.2.dsc 1917 SHA512:f7cebf760482ea3514303154a40f0a143ab1d97b0fe4d2e360c325115e2b5148e387be271060238e3e4133733316bb02cb161f42c2606dbf72d2493cce0667f9
'http://archive.ubuntu.com/ubuntu/pool/main/c/ca-certificates/ca-certificates_20210119~20.04.2.tar.xz' ca-certificates_20210119~20.04.2.tar.xz 232956 SHA512:03740922fcc051a5252129de3e973293de1e5f5833c08b58264362fed2d5d67b53c05f71f426ff56805a8fb8a6ffe35ef4e1556006a3e09471e8424d02ff099e
```

### `dpkg` source package: `cairo=1.16.0-4ubuntu1`

Binary Packages:

- `libcairo-gobject2:amd64=1.16.0-4ubuntu1`
- `libcairo2:amd64=1.16.0-4ubuntu1`

Licenses: (parsed from: `/usr/share/doc/libcairo-gobject2/copyright`, `/usr/share/doc/libcairo2/copyright`)

- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris cairo=1.16.0-4ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/c/cairo/cairo_1.16.0-4ubuntu1.dsc' cairo_1.16.0-4ubuntu1.dsc 2950 SHA256:f53596e412c2e1799d5e7e1c414d7db2cade33ba85fd912d39f60525b5a2e23b
'http://archive.ubuntu.com/ubuntu/pool/main/c/cairo/cairo_1.16.0.orig.tar.xz' cairo_1.16.0.orig.tar.xz 41997432 SHA256:5e7b29b3f113ef870d1e3ecf8adf21f923396401604bda16d44be45e66052331
'http://archive.ubuntu.com/ubuntu/pool/main/c/cairo/cairo_1.16.0-4ubuntu1.debian.tar.xz' cairo_1.16.0-4ubuntu1.debian.tar.xz 30416 SHA256:3725774f0a3f244a8b910e5a5e46bc731ee87372c6effb6c5af2d0db65c64426
```

### `dpkg` source package: `cdebconf=0.251ubuntu1`

Binary Packages:

- `libdebconfclient0:amd64=0.251ubuntu1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris cdebconf=0.251ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/c/cdebconf/cdebconf_0.251ubuntu1.dsc' cdebconf_0.251ubuntu1.dsc 2858 SHA256:0753b98ec773e743de19d393f444a8b88915ad75340cc58007eb7c309031121d
'http://archive.ubuntu.com/ubuntu/pool/main/c/cdebconf/cdebconf_0.251ubuntu1.tar.xz' cdebconf_0.251ubuntu1.tar.xz 276744 SHA256:d07848e52aecb70e82d8bafd082ecee3cccd7a8229b59527e07cc49023aa22d0
```

### `dpkg` source package: `cdparanoia=3.10.2+debian-13`

Binary Packages:

- `libcdparanoia0:amd64=3.10.2+debian-13`

Licenses: (parsed from: `/usr/share/doc/libcdparanoia0/copyright`)

- `GPL-2`
- `GPL-2+`
- `LGPL-2.1`
- `LGPL-2.1+`

Source:

```console
$ apt-get source -qq --print-uris cdparanoia=3.10.2+debian-13
'http://archive.ubuntu.com/ubuntu/pool/main/c/cdparanoia/cdparanoia_3.10.2+debian-13.dsc' cdparanoia_3.10.2+debian-13.dsc 2195 SHA256:7ddf0ba8b09821d50a4b226c19ad008df8285cbd86d5148e035067092c544551
'http://archive.ubuntu.com/ubuntu/pool/main/c/cdparanoia/cdparanoia_3.10.2+debian.orig.tar.gz' cdparanoia_3.10.2+debian.orig.tar.gz 178436 SHA256:402f8b8b4370dbdc276dfd624f768956d212893542a91ecbaa6b4206b2afef03
'http://archive.ubuntu.com/ubuntu/pool/main/c/cdparanoia/cdparanoia_3.10.2+debian-13.debian.tar.xz' cdparanoia_3.10.2+debian-13.debian.tar.xz 61152 SHA256:cff55e4394f6da0fb226b9d36cf773dbd022d8ac689a01419375d88708da2614
```

### `dpkg` source package: `chromaprint=1.4.3-3build1`

Binary Packages:

- `libchromaprint1:amd64=1.4.3-3build1`

Licenses: (parsed from: `/usr/share/doc/libchromaprint1/copyright`)

- `BSD-3-clause`
- `Expat`
- `LGPL-2.1`
- `LGPL-2.1+`

Source:

```console
$ apt-get source -qq --print-uris chromaprint=1.4.3-3build1
'http://archive.ubuntu.com/ubuntu/pool/universe/c/chromaprint/chromaprint_1.4.3-3build1.dsc' chromaprint_1.4.3-3build1.dsc 2291 SHA256:aeb17ca7aa2e65624469f15940af23bd030fc49835b3aeb5965d3decad695aed
'http://archive.ubuntu.com/ubuntu/pool/universe/c/chromaprint/chromaprint_1.4.3.orig.tar.gz' chromaprint_1.4.3.orig.tar.gz 613718 SHA256:d4ae6596283aad7a015a5b0445012054c634a4b9329ecb23000cd354b40a283b
'http://archive.ubuntu.com/ubuntu/pool/universe/c/chromaprint/chromaprint_1.4.3-3build1.debian.tar.xz' chromaprint_1.4.3-3build1.debian.tar.xz 6736 SHA256:4f0e18ba5a56ae9d294064c6c44b844518c233eae30d4abe462c316ef8746ec7
```

### `dpkg` source package: `clucene-core=2.3.3.4+dfsg-1build1`

Binary Packages:

- `libclucene-contribs1v5:amd64=2.3.3.4+dfsg-1build1`
- `libclucene-core1v5:amd64=2.3.3.4+dfsg-1build1`

Licenses: (parsed from: `/usr/share/doc/libclucene-contribs1v5/copyright`, `/usr/share/doc/libclucene-core1v5/copyright`)

- `Apache-2.0`
- `LGPL-2.1`
- `Reuters-21578 - Distribution 1.0`

Source:

```console
$ apt-get source -qq --print-uris clucene-core=2.3.3.4+dfsg-1build1
'http://archive.ubuntu.com/ubuntu/pool/main/c/clucene-core/clucene-core_2.3.3.4+dfsg-1build1.dsc' clucene-core_2.3.3.4+dfsg-1build1.dsc 2068 SHA256:57a6ebed26d5ede7b7e1ebecb2fa5f637c847f88f069b527ed904837b3472e74
'http://archive.ubuntu.com/ubuntu/pool/main/c/clucene-core/clucene-core_2.3.3.4+dfsg.orig.tar.xz' clucene-core_2.3.3.4+dfsg.orig.tar.xz 826688 SHA256:c70b8202c0afca27f9fa2f1a5d09a41bc4cc57a8f68c854379891ea2e24f1490
'http://archive.ubuntu.com/ubuntu/pool/main/c/clucene-core/clucene-core_2.3.3.4+dfsg-1build1.debian.tar.xz' clucene-core_2.3.3.4+dfsg-1build1.debian.tar.xz 8808 SHA256:e2756237df734de432fa539eafcc54f43a27e632760ed94f84448117aee56bbd
```

### `dpkg` source package: `codec2=0.9.2-2`

Binary Packages:

- `libcodec2-0.9:amd64=0.9.2-2`

Licenses: (parsed from: `/usr/share/doc/libcodec2-0.9/copyright`)

- `COPYING`
- `JMVBSD`
- `KISSFFTBSD`
- `LGPL-2`
- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris codec2=0.9.2-2
'http://archive.ubuntu.com/ubuntu/pool/universe/c/codec2/codec2_0.9.2-2.dsc' codec2_0.9.2-2.dsc 2601 SHA256:926a56d8bbb0b0f57d382f010234b6b281abfa9be44b7508bef91939f89fea3e
'http://archive.ubuntu.com/ubuntu/pool/universe/c/codec2/codec2_0.9.2.orig-lpcnet.tar.gz' codec2_0.9.2.orig-lpcnet.tar.gz 33010724 SHA256:50150390610ac13d2101d65a8d1c6983ca1c2189887bea503e4bffcce6463aaf
'http://archive.ubuntu.com/ubuntu/pool/universe/c/codec2/codec2_0.9.2.orig-lpcnet191005.tar.gz' codec2_0.9.2.orig-lpcnet191005.tar.gz 18396516 SHA256:509440924751fdd87ffaa5683ee3dddd937af5c833b9104ccce65d51614926c8
'http://archive.ubuntu.com/ubuntu/pool/universe/c/codec2/codec2_0.9.2.orig.tar.gz' codec2_0.9.2.orig.tar.gz 12244006 SHA256:19181a446f4df3e6d616b50cabdac4485abb9cd3242cf312a0785f892ed4c76c
'http://archive.ubuntu.com/ubuntu/pool/universe/c/codec2/codec2_0.9.2-2.debian.tar.xz' codec2_0.9.2-2.debian.tar.xz 52172 SHA256:18777f5340680fa0abae55124ecf2d09924a6e5eec96041baa19f442b72a08ac
```

### `dpkg` source package: `colord=1.4.4-2`

Binary Packages:

- `libcolord2:amd64=1.4.4-2`

Licenses: (parsed from: `/usr/share/doc/libcolord2/copyright`)

- `CC0`
- `GFDL-NIV`
- `GPL-2`
- `GPL-2+`
- `LGPL-2.1+`

Source:

```console
$ apt-get source -qq --print-uris colord=1.4.4-2
'http://archive.ubuntu.com/ubuntu/pool/main/c/colord/colord_1.4.4-2.dsc' colord_1.4.4-2.dsc 2914 SHA256:9a8a37cb3215a62580ef793916e5685096cf5717a3cce56d2b255dd26a6cc312
'http://archive.ubuntu.com/ubuntu/pool/main/c/colord/colord_1.4.4.orig.tar.xz' colord_1.4.4.orig.tar.xz 1855696 SHA256:9a0fe80160bf88efddb582a9fc0169f56065276dc3882c47dddb9eecd048c0a5
'http://archive.ubuntu.com/ubuntu/pool/main/c/colord/colord_1.4.4.orig.tar.xz.asc' colord_1.4.4.orig.tar.xz.asc 488 SHA256:6346d37bb24d626001a2ceb9eaa41c810a0207695d4e5493d6a91cc3c2c71bd2
'http://archive.ubuntu.com/ubuntu/pool/main/c/colord/colord_1.4.4-2.debian.tar.xz' colord_1.4.4-2.debian.tar.xz 31408 SHA256:9570a9511d0769c3b407c123af0f39e3e62285119db646e3b730305c85016dfe
```

### `dpkg` source package: `commons-parent=43-1`

Binary Packages:

- `libcommons-parent-java=43-1`

Licenses: (parsed from: `/usr/share/doc/libcommons-parent-java/copyright`)

- `Apache-2.0`

Source:

```console
$ apt-get source -qq --print-uris commons-parent=43-1
'http://archive.ubuntu.com/ubuntu/pool/universe/c/commons-parent/commons-parent_43-1.dsc' commons-parent_43-1.dsc 2231 SHA256:872bdd6878193c4bf1a8117bc3b61615d674e287e1ebcca98e6320950de553b4
'http://archive.ubuntu.com/ubuntu/pool/universe/c/commons-parent/commons-parent_43.orig.tar.xz' commons-parent_43.orig.tar.xz 28364 SHA256:ec67c89ccd15b59dc6b7c295af57bea9975a2482aad150499940587e58fd1986
'http://archive.ubuntu.com/ubuntu/pool/universe/c/commons-parent/commons-parent_43-1.debian.tar.xz' commons-parent_43-1.debian.tar.xz 3404 SHA256:b60aa8fb108431fd15277de8762eeec678733489a7977240c87619eb2fa674ab
```

### `dpkg` source package: `coreutils=8.30-3ubuntu2`

Binary Packages:

- `coreutils=8.30-3ubuntu2`

Licenses: (parsed from: `/usr/share/doc/coreutils/copyright`)

- `GPL-3`

Source:

```console
$ apt-get source -qq --print-uris coreutils=8.30-3ubuntu2
'http://archive.ubuntu.com/ubuntu/pool/main/c/coreutils/coreutils_8.30-3ubuntu2.dsc' coreutils_8.30-3ubuntu2.dsc 2048 SHA256:f36fe0ac14978b240a750b79d2bbd737d6b1939296c3a287899933aa2a1906ea
'http://archive.ubuntu.com/ubuntu/pool/main/c/coreutils/coreutils_8.30.orig.tar.xz' coreutils_8.30.orig.tar.xz 5359532 SHA256:e831b3a86091496cdba720411f9748de81507798f6130adeaef872d206e1b057
'http://archive.ubuntu.com/ubuntu/pool/main/c/coreutils/coreutils_8.30-3ubuntu2.debian.tar.xz' coreutils_8.30-3ubuntu2.debian.tar.xz 39636 SHA256:98204ef9d94e5c567880cd0245fdb7940eaf7592d6c6830c300ad117628b351f
```

### `dpkg` source package: `cryptsetup=2:2.2.2-3ubuntu2.3`

Binary Packages:

- `libcryptsetup12:amd64=2:2.2.2-3ubuntu2.3`

Licenses: (parsed from: `/usr/share/doc/libcryptsetup12/copyright`)

- `Apache-2.0`
- `CC0`
- `CC0-1.0`
- `GPL-2`
- `GPL-2+`
- `GPL-2+ with OpenSSL exception`
- `LGPL-2.1`
- `LGPL-2.1+`
- `LGPL-2.1+ with OpenSSL exception`
- `public-domain`

Source:

```console
$ apt-get source -qq --print-uris cryptsetup=2:2.2.2-3ubuntu2.3
'http://archive.ubuntu.com/ubuntu/pool/main/c/cryptsetup/cryptsetup_2.2.2-3ubuntu2.3.dsc' cryptsetup_2.2.2-3ubuntu2.3.dsc 2993 SHA512:18e16a5a094e56826580541a26a6f474878dd1a6d839961f772a78cccf79ff95b630d8874c2a254e4c9cbcb2bb183f69df487aa3e9a942bfadfa3186658e52f3
'http://archive.ubuntu.com/ubuntu/pool/main/c/cryptsetup/cryptsetup_2.2.2.orig.tar.gz' cryptsetup_2.2.2.orig.tar.gz 10912347 SHA512:e7a19c12a0959fbaf75caf033d9737e3505a3f97da12a544959a435e10aa5c4499376b66309bd94db2b9acc13a9515ba6359145f950614487b8f35914819e9eb
'http://archive.ubuntu.com/ubuntu/pool/main/c/cryptsetup/cryptsetup_2.2.2-3ubuntu2.3.debian.tar.xz' cryptsetup_2.2.2-3ubuntu2.3.debian.tar.xz 125868 SHA512:ac9983c15d6fc273e6eb05f27f81725b5bc7ce2e010730a7c7497c9322dff5cdc6d69c2ae2878ca775e6002edf157ddd554eaecb6df936cfc393c605107024e0
```

### `dpkg` source package: `cups=2.3.1-9ubuntu1.1`

Binary Packages:

- `libcups2:amd64=2.3.1-9ubuntu1.1`

Licenses: (parsed from: `/usr/share/doc/libcups2/copyright`)

- `Apache-2.0`
- `Apache-2.0-with-GPL2-LGPL2-Exception`
- `BSD-2-clause`
- `BSD-3-clause`
- `FSFUL`
- `Zlib`

Source:

```console
$ apt-get source -qq --print-uris cups=2.3.1-9ubuntu1.1
'http://archive.ubuntu.com/ubuntu/pool/main/c/cups/cups_2.3.1-9ubuntu1.1.dsc' cups_2.3.1-9ubuntu1.1.dsc 3415 SHA512:5d9fa460bbac51ea6bbf27a1a75045ca93f27923a4b5961edc2e09df61a8a88b0ab649c56d9cffc48667d3552da3c1ae6ca1006c0a6245242e89d4de5768102a
'http://archive.ubuntu.com/ubuntu/pool/main/c/cups/cups_2.3.1.orig.tar.gz' cups_2.3.1.orig.tar.gz 8135891 SHA512:e3f3ad9e78c1c723d46cc2276957ac67495483882f639421203d9dad227eacbb1259717a92489e710995fdc89e2d575202e4b43117aff08ff1230dcf06674376
'http://archive.ubuntu.com/ubuntu/pool/main/c/cups/cups_2.3.1.orig.tar.gz.asc' cups_2.3.1.orig.tar.gz.asc 858 SHA512:a9770f3346229c687b6ec1a5b42231e68ec547339f7d5b29467014d13a2706daf6f5aa9cdc0dd24efd386bdf966890e927cbab61d7a461612f075127c24a2b28
'http://archive.ubuntu.com/ubuntu/pool/main/c/cups/cups_2.3.1-9ubuntu1.1.debian.tar.xz' cups_2.3.1-9ubuntu1.1.debian.tar.xz 356268 SHA512:376d76abe39fbec9a206cdb3544e71d8e4a3aa18188e175d4a996be7be05733be00487d0240e0cbbf21592ee657f686f3205384d98ee277fc5b548ac4642911e
```

### `dpkg` source package: `curl=7.68.0-1ubuntu2.7`

Binary Packages:

- `curl=7.68.0-1ubuntu2.7`
- `libcurl3-gnutls:amd64=7.68.0-1ubuntu2.7`
- `libcurl4:amd64=7.68.0-1ubuntu2.7`

Licenses: (parsed from: `/usr/share/doc/curl/copyright`, `/usr/share/doc/libcurl3-gnutls/copyright`, `/usr/share/doc/libcurl4/copyright`)

- `BSD-3-Clause`
- `BSD-4-Clause`
- `ISC`
- `curl`
- `other`
- `public-domain`

Source:

```console
$ apt-get source -qq --print-uris curl=7.68.0-1ubuntu2.7
'http://archive.ubuntu.com/ubuntu/pool/main/c/curl/curl_7.68.0-1ubuntu2.7.dsc' curl_7.68.0-1ubuntu2.7.dsc 2733 SHA512:e125a9af90ed2bc17af45f0b23b2c145820a1fb61e8836e0aae2c13c43a4b4fff66c65b7464df47ccd0d54addc8062197afa61fe8a62a03e0a4c43b338301537
'http://archive.ubuntu.com/ubuntu/pool/main/c/curl/curl_7.68.0.orig.tar.gz' curl_7.68.0.orig.tar.gz 4096350 SHA512:58b42c08b1cf4cb6e68f8e469d5b5f6298eebe286ba2677ad29e1a7eefd15b8609af54544f4c5a7dadebbd3b23bd77700830f2f60fbea7ae3f2f306e640010b0
'http://archive.ubuntu.com/ubuntu/pool/main/c/curl/curl_7.68.0-1ubuntu2.7.debian.tar.xz' curl_7.68.0-1ubuntu2.7.debian.tar.xz 48080 SHA512:9312b57d9adee8f77462aa222ae403c87f38f4729b73929b1ca5444c8e7c42c19102753dccefccdf7bfbff96f475aac61cda3600b630989b0f1bd5de8aa68699
```

### `dpkg` source package: `cyrus-sasl2=2.1.27+dfsg-2`

Binary Packages:

- `libsasl2-2:amd64=2.1.27+dfsg-2`
- `libsasl2-modules:amd64=2.1.27+dfsg-2`
- `libsasl2-modules-db:amd64=2.1.27+dfsg-2`

Licenses: (parsed from: `/usr/share/doc/libsasl2-2/copyright`, `/usr/share/doc/libsasl2-modules/copyright`, `/usr/share/doc/libsasl2-modules-db/copyright`)

- `BSD-4-clause`
- `GPL-3`
- `GPL-3+`

Source:

```console
$ apt-get source -qq --print-uris cyrus-sasl2=2.1.27+dfsg-2
'http://archive.ubuntu.com/ubuntu/pool/main/c/cyrus-sasl2/cyrus-sasl2_2.1.27+dfsg-2.dsc' cyrus-sasl2_2.1.27+dfsg-2.dsc 3393 SHA256:e7e09491a1c2589c9947164db091d0f9b21b7d122f128841b6eac1adfc51b6c2
'http://archive.ubuntu.com/ubuntu/pool/main/c/cyrus-sasl2/cyrus-sasl2_2.1.27+dfsg.orig.tar.xz' cyrus-sasl2_2.1.27+dfsg.orig.tar.xz 2058596 SHA256:108b0c691c423837264f05abb559ea76c3dfdd91246555e8abe87c129a6e37cd
'http://archive.ubuntu.com/ubuntu/pool/main/c/cyrus-sasl2/cyrus-sasl2_2.1.27+dfsg-2.debian.tar.xz' cyrus-sasl2_2.1.27+dfsg-2.debian.tar.xz 99956 SHA256:ee894aeee645e842e39b434d5130e1bd15ea24b84c8eeeea3f5077511a87341a
```

### `dpkg` source package: `dash=0.5.10.2-6`

Binary Packages:

- `dash=0.5.10.2-6`

Licenses: (parsed from: `/usr/share/doc/dash/copyright`)

- `GPL`

Source:

```console
$ apt-get source -qq --print-uris dash=0.5.10.2-6
'http://archive.ubuntu.com/ubuntu/pool/main/d/dash/dash_0.5.10.2-6.dsc' dash_0.5.10.2-6.dsc 1756 SHA256:d509a23ebdc4f107c911914590c1400e5a24383f35c5d6904e48d2afeff168ac
'http://archive.ubuntu.com/ubuntu/pool/main/d/dash/dash_0.5.10.2.orig.tar.gz' dash_0.5.10.2.orig.tar.gz 225196 SHA256:3c663919dc5c66ec991da14c7cf7e0be8ad00f3db73986a987c118862b5f6071
'http://archive.ubuntu.com/ubuntu/pool/main/d/dash/dash_0.5.10.2-6.debian.tar.xz' dash_0.5.10.2-6.debian.tar.xz 44232 SHA256:1448fbfc2541be52787da81ce03bb81ad6b1f380cba1b7e747abefdcd44f6c86
```

### `dpkg` source package: `db5.3=5.3.28+dfsg1-0.6ubuntu2`

Binary Packages:

- `libdb5.3:amd64=5.3.28+dfsg1-0.6ubuntu2`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris db5.3=5.3.28+dfsg1-0.6ubuntu2
'http://archive.ubuntu.com/ubuntu/pool/main/d/db5.3/db5.3_5.3.28+dfsg1-0.6ubuntu2.dsc' db5.3_5.3.28+dfsg1-0.6ubuntu2.dsc 3245 SHA256:d879f4921a2f573132031d9371f0eb020005bdce48d6e12b436bf3515dda8663
'http://archive.ubuntu.com/ubuntu/pool/main/d/db5.3/db5.3_5.3.28+dfsg1.orig.tar.xz' db5.3_5.3.28+dfsg1.orig.tar.xz 19723860 SHA256:b19bf3dd8ce74b95a7b215be9a7c8489e8e8f18da60d64d6340a06e75f497749
'http://archive.ubuntu.com/ubuntu/pool/main/d/db5.3/db5.3_5.3.28+dfsg1-0.6ubuntu2.debian.tar.xz' db5.3_5.3.28+dfsg1-0.6ubuntu2.debian.tar.xz 30172 SHA256:e606e7827f077efc92afc6f0d43c921fab4577d619eab06fab23182aefab7506
```

### `dpkg` source package: `dbus-python=1.2.16-1build1`

Binary Packages:

- `python3-dbus=1.2.16-1build1`

Licenses: (parsed from: `/usr/share/doc/python3-dbus/copyright`)

- `AFL-2.1`
- `Expat`
- `GPL-2`
- `GPL-2+`

Source:

```console
$ apt-get source -qq --print-uris dbus-python=1.2.16-1build1
'http://archive.ubuntu.com/ubuntu/pool/main/d/dbus-python/dbus-python_1.2.16-1build1.dsc' dbus-python_1.2.16-1build1.dsc 3641 SHA256:8ecda77e26175c8f2fa6b8960e89161cd2571e3aa4f9d1580f1f1a3136b35a97
'http://archive.ubuntu.com/ubuntu/pool/main/d/dbus-python/dbus-python_1.2.16.orig.tar.gz' dbus-python_1.2.16.orig.tar.gz 576701 SHA256:11238f1d86c995d8aed2e22f04a1e3779f0d70e587caffeab4857f3c662ed5a4
'http://archive.ubuntu.com/ubuntu/pool/main/d/dbus-python/dbus-python_1.2.16.orig.tar.gz.asc' dbus-python_1.2.16.orig.tar.gz.asc 833 SHA256:0fcfcb9844226c5cde1690b74b3c094d802ea735392d3a8829f1b5993837e86c
'http://archive.ubuntu.com/ubuntu/pool/main/d/dbus-python/dbus-python_1.2.16-1build1.debian.tar.xz' dbus-python_1.2.16-1build1.debian.tar.xz 34532 SHA256:691fd294a727e96250e084ba3ee388d9e226b2808ce1edf58d1782000dbe1425
```

### `dpkg` source package: `dbus=1.12.16-2ubuntu2.1`

Binary Packages:

- `dbus=1.12.16-2ubuntu2.1`
- `dbus-user-session=1.12.16-2ubuntu2.1`
- `libdbus-1-3:amd64=1.12.16-2ubuntu2.1`

Licenses: (parsed from: `/usr/share/doc/dbus/copyright`, `/usr/share/doc/dbus-user-session/copyright`, `/usr/share/doc/libdbus-1-3/copyright`)

- `AFL-2.1`
- `AFL-2.1,`
- `BSD-3-clause`
- `BSD-3-clause-generic`
- `Expat`
- `GPL-2`
- `GPL-2+`
- `Tcl-BSDish`
- `g10-permissive`

Source:

```console
$ apt-get source -qq --print-uris dbus=1.12.16-2ubuntu2.1
'http://archive.ubuntu.com/ubuntu/pool/main/d/dbus/dbus_1.12.16-2ubuntu2.1.dsc' dbus_1.12.16-2ubuntu2.1.dsc 3867 SHA512:b635e40d4a52664ace191d6d3ae7f817b6ead4f7d01ce97d498d31f958f3a51a53843c0f51651027a5d1081d4e9987c3f7986e59e406f09406cd1128a91dcbd9
'http://archive.ubuntu.com/ubuntu/pool/main/d/dbus/dbus_1.12.16.orig.tar.gz' dbus_1.12.16.orig.tar.gz 2093296 SHA512:27ae805170e9515a8bb0fba5f29d414edc70e3b6b28b7b65bbea47035b8eafa9ac4820cdc92645be6035f6748f8aa45679e1ffc84ba74a64859a3056d318b9bb
'http://archive.ubuntu.com/ubuntu/pool/main/d/dbus/dbus_1.12.16.orig.tar.gz.asc' dbus_1.12.16.orig.tar.gz.asc 833 SHA512:6d19bf7be86ae1dc70550ba472e5761f3ed1a71007c00679e3a586d567776e82cf9869c9a7021c1324990615657a054b949dc5bbd8e60b0a8843ef6d977eda24
'http://archive.ubuntu.com/ubuntu/pool/main/d/dbus/dbus_1.12.16-2ubuntu2.1.debian.tar.xz' dbus_1.12.16-2ubuntu2.1.debian.tar.xz 69204 SHA512:44c4b8867f6cd12c0e0ced86a270928d612a1ad541b8df9097c1b80a01646b58ccb890bec64b0603b86d6b769c495fe672663b4609a584ad49deb50cd317a06b
```

### `dpkg` source package: `dconf=0.36.0-1`

Binary Packages:

- `dconf-gsettings-backend:amd64=0.36.0-1`
- `dconf-service=0.36.0-1`
- `libdconf1:amd64=0.36.0-1`

Licenses: (parsed from: `/usr/share/doc/dconf-gsettings-backend/copyright`, `/usr/share/doc/dconf-service/copyright`, `/usr/share/doc/libdconf1/copyright`)

- `GPL-3`
- `LGPL-2+`

Source:

```console
$ apt-get source -qq --print-uris dconf=0.36.0-1
'http://archive.ubuntu.com/ubuntu/pool/main/d/dconf/dconf_0.36.0-1.dsc' dconf_0.36.0-1.dsc 2320 SHA256:b5ac5d2b3075705f5922c1da479e0c2faeb4adf696d70ae07e3604143899e608
'http://archive.ubuntu.com/ubuntu/pool/main/d/dconf/dconf_0.36.0.orig.tar.xz' dconf_0.36.0.orig.tar.xz 114828 SHA256:9fe6bb22191fc2a036ad86fd8e7d165e9983c687b9fedccf85d46c799301da2d
'http://archive.ubuntu.com/ubuntu/pool/main/d/dconf/dconf_0.36.0-1.debian.tar.xz' dconf_0.36.0-1.debian.tar.xz 10472 SHA256:bb0d86cbef3d5faffa00fa11b66d17b858974c28ef2fcbaf1d50c8ea2fb54e09
```

### `dpkg` source package: `debconf=1.5.73`

Binary Packages:

- `debconf=1.5.73`

Licenses: (parsed from: `/usr/share/doc/debconf/copyright`)

- `BSD-2-clause`

Source:

```console
$ apt-get source -qq --print-uris debconf=1.5.73
'http://archive.ubuntu.com/ubuntu/pool/main/d/debconf/debconf_1.5.73.dsc' debconf_1.5.73.dsc 2081 SHA256:cdd4c049414cd167a4a9479d883e205bf5cebb19fc4bb6f132000a56291eb670
'http://archive.ubuntu.com/ubuntu/pool/main/d/debconf/debconf_1.5.73.tar.xz' debconf_1.5.73.tar.xz 570780 SHA256:513895b2b77d9fb72542152390e7d4c67fe1e08de75fdad44d54ce1e7d83ecef
```

### `dpkg` source package: `debianutils=4.9.1`

Binary Packages:

- `debianutils=4.9.1`

Licenses: (parsed from: `/usr/share/doc/debianutils/copyright`)

- `GPL`

Source:

```console
$ apt-get source -qq --print-uris debianutils=4.9.1
'http://archive.ubuntu.com/ubuntu/pool/main/d/debianutils/debianutils_4.9.1.dsc' debianutils_4.9.1.dsc 1592 SHA256:d30866ea0352263fa7756010e8743ade350024b2fd491bc5befcbaa9a97626b7
'http://archive.ubuntu.com/ubuntu/pool/main/d/debianutils/debianutils_4.9.1.tar.xz' debianutils_4.9.1.tar.xz 157516 SHA256:af826685d9c56abfa873e84cd392539cd363cb0ba04a09d21187377e1b764091
```

### `dpkg` source package: `dictionaries-common=1.28.1`

Binary Packages:

- `dictionaries-common=1.28.1`

Licenses: (parsed from: `/usr/share/doc/dictionaries-common/copyright`)

- `GPL-2`
- `GPL-2+`
- `GPL-3`
- `GPL-3+`

Source:

```console
$ apt-get source -qq --print-uris dictionaries-common=1.28.1
'http://archive.ubuntu.com/ubuntu/pool/main/d/dictionaries-common/dictionaries-common_1.28.1.dsc' dictionaries-common_1.28.1.dsc 1887 SHA256:cc458494d42ee3a6e4ba0d1733db86b83b58710d98b135c02600851248c9b970
'http://archive.ubuntu.com/ubuntu/pool/main/d/dictionaries-common/dictionaries-common_1.28.1.tar.gz' dictionaries-common_1.28.1.tar.gz 361303 SHA256:b30233ec4df23d3728807fd836c9e20c5b6756a87c8c7520beafdea5e09faffa
```

### `dpkg` source package: `diffutils=1:3.7-3`

Binary Packages:

- `diffutils=1:3.7-3`

Licenses: (parsed from: `/usr/share/doc/diffutils/copyright`)

- `GFDL`
- `GPL`

Source:

```console
$ apt-get source -qq --print-uris diffutils=1:3.7-3
'http://archive.ubuntu.com/ubuntu/pool/main/d/diffutils/diffutils_3.7-3.dsc' diffutils_3.7-3.dsc 1453 SHA256:99dee94cec05454a65a9cb542bea1720dbd4c511d13f9784c9e3741e76a9b9ba
'http://archive.ubuntu.com/ubuntu/pool/main/d/diffutils/diffutils_3.7.orig.tar.xz' diffutils_3.7.orig.tar.xz 1448828 SHA256:b3a7a6221c3dc916085f0d205abf6b8e1ba443d4dd965118da364a1dc1cb3a26
'http://archive.ubuntu.com/ubuntu/pool/main/d/diffutils/diffutils_3.7-3.debian.tar.xz' diffutils_3.7-3.debian.tar.xz 11116 SHA256:a455228f12283b5f3c0165db4ab9b12071adc37fb9dd50dcb5e1b8851c524f1f
```

### `dpkg` source package: `djvulibre=3.5.27.1-14ubuntu0.1`

Binary Packages:

- `libdjvulibre-text=3.5.27.1-14ubuntu0.1`
- `libdjvulibre21:amd64=3.5.27.1-14ubuntu0.1`

Licenses: (parsed from: `/usr/share/doc/libdjvulibre-text/copyright`, `/usr/share/doc/libdjvulibre21/copyright`)

- `GPL-2`

Source:

```console
$ apt-get source -qq --print-uris djvulibre=3.5.27.1-14ubuntu0.1
'http://archive.ubuntu.com/ubuntu/pool/main/d/djvulibre/djvulibre_3.5.27.1-14ubuntu0.1.dsc' djvulibre_3.5.27.1-14ubuntu0.1.dsc 2500 SHA512:46ea7723d3d6fbffeb7af94c0391ae12eb2521da72d1d437462df92cbc51632022b7ee0e11b0160f69e8ed9f699cf22a4a19538920999e1211c2e527c08f04ac
'http://archive.ubuntu.com/ubuntu/pool/main/d/djvulibre/djvulibre_3.5.27.1.orig.tar.gz' djvulibre_3.5.27.1.orig.tar.gz 3231662 SHA512:2ed11daa05995db7bf52113e2f75456c3c804988d2c17d0183b24ab379e52a4ef1871189e8bb132fec6cbc9d629b4d67a4d89ef7df7a995044cb25ff3dcc5de8
'http://archive.ubuntu.com/ubuntu/pool/main/d/djvulibre/djvulibre_3.5.27.1-14ubuntu0.1.debian.tar.xz' djvulibre_3.5.27.1-14ubuntu0.1.debian.tar.xz 78008 SHA512:9b293e1f90432f7a9851efe46d2f199ffe65cbd53e90acaff608bdc21b77b983b0f653a206aa09687f8e68564ae0103a07038466be54730bbedf94a16928c1cf
```

### `dpkg` source package: `dpkg=1.19.7ubuntu3`

Binary Packages:

- `dpkg=1.19.7ubuntu3`

Licenses: (parsed from: `/usr/share/doc/dpkg/copyright`)

- `BSD-2-clause`
- `GPL-2`
- `GPL-2+`
- `public-domain-md5`
- `public-domain-s-s-d`

Source:

```console
$ apt-get source -qq --print-uris dpkg=1.19.7ubuntu3
'http://archive.ubuntu.com/ubuntu/pool/main/d/dpkg/dpkg_1.19.7ubuntu3.dsc' dpkg_1.19.7ubuntu3.dsc 2254 SHA256:462ecb9f8af5612f7fbc1181484d8376569f95bb0dc7b7c53891819a0434e81a
'http://archive.ubuntu.com/ubuntu/pool/main/d/dpkg/dpkg_1.19.7ubuntu3.tar.xz' dpkg_1.19.7ubuntu3.tar.xz 4731220 SHA256:598eba200da2e1b6097f11537c49e9ad7eb2292e148259bd76f9cd184f281853
```

### `dpkg` source package: `e2fsprogs=1.45.5-2ubuntu1`

Binary Packages:

- `e2fsprogs=1.45.5-2ubuntu1`
- `libcom-err2:amd64=1.45.5-2ubuntu1`
- `libext2fs2:amd64=1.45.5-2ubuntu1`
- `libss2:amd64=1.45.5-2ubuntu1`
- `logsave=1.45.5-2ubuntu1`

Licenses: (parsed from: `/usr/share/doc/e2fsprogs/copyright`, `/usr/share/doc/libcom-err2/copyright`, `/usr/share/doc/libext2fs2/copyright`, `/usr/share/doc/libss2/copyright`, `/usr/share/doc/logsave/copyright`)

- `GPL-2`
- `LGPL-2`

Source:

```console
$ apt-get source -qq --print-uris e2fsprogs=1.45.5-2ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/e/e2fsprogs/e2fsprogs_1.45.5-2ubuntu1.dsc' e2fsprogs_1.45.5-2ubuntu1.dsc 3342 SHA256:a4d9f6948bd041523783a727854beff6b8c3786f96f70936e1d63b6cd9596b01
'http://archive.ubuntu.com/ubuntu/pool/main/e/e2fsprogs/e2fsprogs_1.45.5.orig.tar.gz' e2fsprogs_1.45.5.orig.tar.gz 7938826 SHA256:91e72a2f6fee21b89624d8ece5a4b3751a17b28775d32cd048921050b4760ed9
'http://archive.ubuntu.com/ubuntu/pool/main/e/e2fsprogs/e2fsprogs_1.45.5.orig.tar.gz.asc' e2fsprogs_1.45.5.orig.tar.gz.asc 488 SHA256:0f900698a89e3e1996cd86966e5ae0dc6f8d866e2cd8a0f4285c23e7ea696720
'http://archive.ubuntu.com/ubuntu/pool/main/e/e2fsprogs/e2fsprogs_1.45.5-2ubuntu1.debian.tar.xz' e2fsprogs_1.45.5-2ubuntu1.debian.tar.xz 81528 SHA256:bcf259dd0480b50996580a765fded85f89a0a6041f6c81cbbcce94f58944c51b
```

### `dpkg` source package: `el-api=3.0.0-2`

Binary Packages:

- `libel-api-java=3.0.0-2`

Licenses: (parsed from: `/usr/share/doc/libel-api-java/copyright`)

- `Apache-2.0`
- `CDDL-1.1`
- `GPL-2`
- `GPL-2 with Classpath exception`

Source:

```console
$ apt-get source -qq --print-uris el-api=3.0.0-2
'http://archive.ubuntu.com/ubuntu/pool/universe/e/el-api/el-api_3.0.0-2.dsc' el-api_3.0.0-2.dsc 2003 SHA256:4ffcf93fe27f152a0ff3f0cb5d2ea62bde4066cfe808d285fcd0a3a9e20a1a63
'http://archive.ubuntu.com/ubuntu/pool/universe/e/el-api/el-api_3.0.0.orig.tar.xz' el-api_3.0.0.orig.tar.xz 41460 SHA256:3af49a2a357102216ea6a0f2e58596d07509cb1ac92fea2b22b89d0a066785d5
'http://archive.ubuntu.com/ubuntu/pool/universe/e/el-api/el-api_3.0.0-2.debian.tar.xz' el-api_3.0.0-2.debian.tar.xz 8536 SHA256:f84e529f024dc992a2a9045aefb78aeec1329da033ca37a3f8319ccf2d9a621f
```

### `dpkg` source package: `elfutils=0.176-1.1build1`

Binary Packages:

- `libelf1:amd64=0.176-1.1build1`

Licenses: (parsed from: `/usr/share/doc/libelf1/copyright`)

- `GPL-2`
- `GPL-3`
- `LGPL-`

Source:

```console
$ apt-get source -qq --print-uris elfutils=0.176-1.1build1
'http://archive.ubuntu.com/ubuntu/pool/main/e/elfutils/elfutils_0.176-1.1build1.dsc' elfutils_0.176-1.1build1.dsc 2633 SHA256:2d0513bda9230c3fc655473b0df0069cf39aaf954bb99b93d147e1d56205148b
'http://archive.ubuntu.com/ubuntu/pool/main/e/elfutils/elfutils_0.176.orig.tar.bz2' elfutils_0.176.orig.tar.bz2 8646075 SHA256:eb5747c371b0af0f71e86215a5ebb88728533c3a104a43d4231963f308cd1023
'http://archive.ubuntu.com/ubuntu/pool/main/e/elfutils/elfutils_0.176.orig.tar.bz2.asc' elfutils_0.176.orig.tar.bz2.asc 455 SHA256:51474b579b25fc799de0777e241c83605427d2903f8d28524ef6af42f75931fd
'http://archive.ubuntu.com/ubuntu/pool/main/e/elfutils/elfutils_0.176-1.1build1.debian.tar.xz' elfutils_0.176-1.1build1.debian.tar.xz 31696 SHA256:b2af440e20560eb14b8c91493ada2cfeb4f659c52bea22d7f6b3ef741532fd04
```

### `dpkg` source package: `emacsen-common=3.0.4`

Binary Packages:

- `emacsen-common=3.0.4`

Licenses: (parsed from: `/usr/share/doc/emacsen-common/copyright`)

- `GPL`

Source:

```console
$ apt-get source -qq --print-uris emacsen-common=3.0.4
'http://archive.ubuntu.com/ubuntu/pool/main/e/emacsen-common/emacsen-common_3.0.4.dsc' emacsen-common_3.0.4.dsc 1463 SHA256:4c8ee0308972bb43213f1efa68bcca1a90766a836b85f394c9e73fef4de9e83d
'http://archive.ubuntu.com/ubuntu/pool/main/e/emacsen-common/emacsen-common_3.0.4.tar.xz' emacsen-common_3.0.4.tar.xz 16292 SHA256:b20c7202b3553bbbe03290a61087ed10afa52edf407334ca8a7787ef0a876600
```

### `dpkg` source package: `expat=2.2.9-1build1`

Binary Packages:

- `libexpat1:amd64=2.2.9-1build1`

Licenses: (parsed from: `/usr/share/doc/libexpat1/copyright`)

- `MIT`

Source:

```console
$ apt-get source -qq --print-uris expat=2.2.9-1build1
'http://archive.ubuntu.com/ubuntu/pool/main/e/expat/expat_2.2.9-1build1.dsc' expat_2.2.9-1build1.dsc 1998 SHA256:9f2d2e3bf2aec22907e3bf818fac7acc5f1e917821907bdea016f69a5cfe4da0
'http://archive.ubuntu.com/ubuntu/pool/main/e/expat/expat_2.2.9.orig.tar.gz' expat_2.2.9.orig.tar.gz 8273174 SHA256:c341ac8c79e021cc3392a6d76e138e62d1dd287592cb455148540331756a2208
'http://archive.ubuntu.com/ubuntu/pool/main/e/expat/expat_2.2.9-1build1.debian.tar.xz' expat_2.2.9-1build1.debian.tar.xz 10780 SHA256:400872937adfb41255914391a172237cfe317e57f129562ff2ec66773b2b5bbf
```

### `dpkg` source package: `ffmpeg=7:4.2.4-1ubuntu0.1`

Binary Packages:

- `ffmpeg=7:4.2.4-1ubuntu0.1`
- `libavcodec58:amd64=7:4.2.4-1ubuntu0.1`
- `libavdevice58:amd64=7:4.2.4-1ubuntu0.1`
- `libavfilter7:amd64=7:4.2.4-1ubuntu0.1`
- `libavformat58:amd64=7:4.2.4-1ubuntu0.1`
- `libavresample4:amd64=7:4.2.4-1ubuntu0.1`
- `libavutil56:amd64=7:4.2.4-1ubuntu0.1`
- `libpostproc55:amd64=7:4.2.4-1ubuntu0.1`
- `libswresample3:amd64=7:4.2.4-1ubuntu0.1`
- `libswscale5:amd64=7:4.2.4-1ubuntu0.1`

Licenses: (parsed from: `/usr/share/doc/ffmpeg/copyright`, `/usr/share/doc/libavcodec58/copyright`, `/usr/share/doc/libavdevice58/copyright`, `/usr/share/doc/libavfilter7/copyright`, `/usr/share/doc/libavformat58/copyright`, `/usr/share/doc/libavresample4/copyright`, `/usr/share/doc/libavutil56/copyright`, `/usr/share/doc/libpostproc55/copyright`, `/usr/share/doc/libswresample3/copyright`, `/usr/share/doc/libswscale5/copyright`)

- `BSD-1-clause`
- `BSD-2-clause`
- `BSD-3-clause`
- `BSL`
- `Expat`
- `GPL-2`
- `GPL-2+`
- `GPL-2+ with Avisynth exception`
- `GPL-3`
- `GPL-3+`
- `IJG`
- `ISC`
- `LGPL-2+`
- `LGPL-2.1`
- `LGPL-2.1+`
- `Sundry`
- `Zlib`
- `man-page`
- `public-domain`

Source:

```console
$ apt-get source -qq --print-uris ffmpeg=7:4.2.4-1ubuntu0.1
'http://archive.ubuntu.com/ubuntu/pool/universe/f/ffmpeg/ffmpeg_4.2.4-1ubuntu0.1.dsc' ffmpeg_4.2.4-1ubuntu0.1.dsc 5103 SHA512:f43f427d26994168330822383f83b9ec96817b87168b927147466216d2628bede99033630c457a226c33ab3facb012b038781223c961f372894a6bc82e4c1df7
'http://archive.ubuntu.com/ubuntu/pool/universe/f/ffmpeg/ffmpeg_4.2.4.orig.tar.xz' ffmpeg_4.2.4.orig.tar.xz 9102888 SHA512:eced8108335df42065f671ba4e64e8f01c430953d55d2d1012a02aa2ba1cb75b18f80adf2f32433c984f4128985eedb09aa2e0149ec009cd28db9140730887a0
'http://archive.ubuntu.com/ubuntu/pool/universe/f/ffmpeg/ffmpeg_4.2.4-1ubuntu0.1.debian.tar.xz' ffmpeg_4.2.4-1ubuntu0.1.debian.tar.xz 51152 SHA512:7315c28d7fb2d21317bf91b177a69022157bd7297475b5fd26c5d3d9b9a1d4f35843b38a9390e71e605fbbc34f4557bb421c96f9a316ac06abae046b66d1e3da
```

### `dpkg` source package: `fftw3=3.3.8-2ubuntu1`

Binary Packages:

- `libfftw3-double3:amd64=3.3.8-2ubuntu1`

Licenses: (parsed from: `/usr/share/doc/libfftw3-double3/copyright`)

- `GPL-2`

Source:

```console
$ apt-get source -qq --print-uris fftw3=3.3.8-2ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/f/fftw3/fftw3_3.3.8-2ubuntu1.dsc' fftw3_3.3.8-2ubuntu1.dsc 2240 SHA256:f33b0cba612104281fa3de2cdd38e8cedf8aa52a6e28146d9b162a2df11aff8e
'http://archive.ubuntu.com/ubuntu/pool/main/f/fftw3/fftw3_3.3.8.orig.tar.gz' fftw3_3.3.8.orig.tar.gz 4110137 SHA256:6113262f6e92c5bd474f2875fa1b01054c4ad5040f6b0da7c03c98821d9ae303
'http://archive.ubuntu.com/ubuntu/pool/main/f/fftw3/fftw3_3.3.8-2ubuntu1.debian.tar.xz' fftw3_3.3.8-2ubuntu1.debian.tar.xz 14028 SHA256:8bc469ad07cef4ebb1c512feecc061ba58eb851d83f67f732d42bc7db0a4f89b
```

### `dpkg` source package: `file=1:5.38-4`

Binary Packages:

- `file=1:5.38-4`
- `libmagic-mgc=1:5.38-4`
- `libmagic1:amd64=1:5.38-4`

Licenses: (parsed from: `/usr/share/doc/file/copyright`, `/usr/share/doc/libmagic-mgc/copyright`, `/usr/share/doc/libmagic1/copyright`)

- `BSD-2-Clause-alike`
- `BSD-2-Clause-netbsd`
- `BSD-2-Clause-regents`
- `MIT-Old-Style-with-legal-disclaimer-2`
- `public-domain`

Source:

```console
$ apt-get source -qq --print-uris file=1:5.38-4
'http://archive.ubuntu.com/ubuntu/pool/main/f/file/file_5.38-4.dsc' file_5.38-4.dsc 2237 SHA256:d84b2734b112384230e3678beb1c8c5d3a17d4cdec8b6ef21304f2c0b72fda26
'http://archive.ubuntu.com/ubuntu/pool/main/f/file/file_5.38.orig.tar.gz' file_5.38.orig.tar.gz 932528 SHA256:593c2ffc2ab349c5aea0f55fedfe4d681737b6b62376a9b3ad1e77b2cc19fa34
'http://archive.ubuntu.com/ubuntu/pool/main/f/file/file_5.38.orig.tar.gz.asc' file_5.38.orig.tar.gz.asc 169 SHA256:b9c78e39970abda091ec8752401f5241349cef4709a2e1267a378f7ab25115d8
'http://archive.ubuntu.com/ubuntu/pool/main/f/file/file_5.38-4.debian.tar.xz' file_5.38-4.debian.tar.xz 34488 SHA256:b9dfd0dd070ee17a6cc69ecf4d61c329b3f567ca16fdc121f3b31d0111df9381
```

### `dpkg` source package: `findutils=4.7.0-1ubuntu1`

Binary Packages:

- `findutils=4.7.0-1ubuntu1`

Licenses: (parsed from: `/usr/share/doc/findutils/copyright`)

- `GFDL-1.3`
- `GPL-3`

Source:

```console
$ apt-get source -qq --print-uris findutils=4.7.0-1ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/f/findutils/findutils_4.7.0-1ubuntu1.dsc' findutils_4.7.0-1ubuntu1.dsc 2446 SHA256:3d157948919082e66cb74e0f927efa3dd240d9fa9814973874d0fa77f3023ead
'http://archive.ubuntu.com/ubuntu/pool/main/f/findutils/findutils_4.7.0.orig.tar.xz' findutils_4.7.0.orig.tar.xz 1895048 SHA256:c5fefbdf9858f7e4feb86f036e1247a54c79fc2d8e4b7064d5aaa1f47dfa789a
'http://archive.ubuntu.com/ubuntu/pool/main/f/findutils/findutils_4.7.0.orig.tar.xz.asc' findutils_4.7.0.orig.tar.xz.asc 488 SHA256:2f620e6d941e241fac52344a89149ab1ffeefb0fb9e42174e17a508d59a31d0f
'http://archive.ubuntu.com/ubuntu/pool/main/f/findutils/findutils_4.7.0-1ubuntu1.debian.tar.xz' findutils_4.7.0-1ubuntu1.debian.tar.xz 27700 SHA256:dfb2329fd141384c2d76409c2e99f164cc25954115529245d80d5d41e3167731
```

### `dpkg` source package: `firebird3.0=3.0.5.33220.ds4-1build2`

Binary Packages:

- `firebird3.0-common=3.0.5.33220.ds4-1build2`
- `firebird3.0-common-doc=3.0.5.33220.ds4-1build2`
- `firebird3.0-server-core:amd64=3.0.5.33220.ds4-1build2`
- `firebird3.0-utils=3.0.5.33220.ds4-1build2`
- `libfbclient2:amd64=3.0.5.33220.ds4-1build2`
- `libib-util:amd64=3.0.5.33220.ds4-1build2`

Licenses: (parsed from: `/usr/share/doc/firebird3.0-common/copyright`, `/usr/share/doc/firebird3.0-common-doc/copyright`, `/usr/share/doc/firebird3.0-server-core/copyright`, `/usr/share/doc/firebird3.0-utils/copyright`, `/usr/share/doc/libfbclient2/copyright`, `/usr/share/doc/libib-util/copyright`)

- `BSD-3-clause`
- `GPL-2`
- `GPL-3`
- `GPL-3+ with autoconf exception`
- `IDPL-1.0`
- `IPL-1.0`
- `LGPL-2.1`
- `LGPL-2.1 with OSI exception`
- `MPL-1.1`
- `MPL-1.1-or-GPL-2+`
- `bsd-like`
- `other`
- `public-domain`

Source:

```console
$ apt-get source -qq --print-uris firebird3.0=3.0.5.33220.ds4-1build2
'http://archive.ubuntu.com/ubuntu/pool/universe/f/firebird3.0/firebird3.0_3.0.5.33220.ds4-1build2.dsc' firebird3.0_3.0.5.33220.ds4-1build2.dsc 2794 SHA256:598181e8bebc7140fe7848b0f08404c41aae59fd47db3e1e94ef4f5f2f659d59
'http://archive.ubuntu.com/ubuntu/pool/universe/f/firebird3.0/firebird3.0_3.0.5.33220.ds4.orig.tar.xz' firebird3.0_3.0.5.33220.ds4.orig.tar.xz 3386476 SHA256:14327890beed418d910ee46d093b584458179adf4ad2f423871dda7261ae65c3
'http://archive.ubuntu.com/ubuntu/pool/universe/f/firebird3.0/firebird3.0_3.0.5.33220.ds4-1build2.debian.tar.xz' firebird3.0_3.0.5.33220.ds4-1build2.debian.tar.xz 101156 SHA256:1bf076e2274fd105109bf6d24a949981c76b4b11c2541596c6a919e9ef71f68f
```

### `dpkg` source package: `flac=1.3.3-1build1`

Binary Packages:

- `libflac8:amd64=1.3.3-1build1`

Licenses: (parsed from: `/usr/share/doc/libflac8/copyright`)

- `BSD-3-clause`
- `GFDL-1.1+`
- `GFDL-1.2`
- `GPL-2`
- `GPL-2+`
- `ISC`
- `LGPL-2`
- `LGPL-2+`
- `LGPL-2.1`
- `LGPL-2.1+`
- `Public-domain`

Source:

```console
$ apt-get source -qq --print-uris flac=1.3.3-1build1
'http://archive.ubuntu.com/ubuntu/pool/main/f/flac/flac_1.3.3-1build1.dsc' flac_1.3.3-1build1.dsc 2289 SHA256:3fbadc35b06df8a231d20b2a304845065df53de4b84dd80709afb777f0a50d93
'http://archive.ubuntu.com/ubuntu/pool/main/f/flac/flac_1.3.3.orig.tar.xz' flac_1.3.3.orig.tar.xz 1044472 SHA256:213e82bd716c9de6db2f98bcadbc4c24c7e2efe8c75939a1a84e28539c4e1748
'http://archive.ubuntu.com/ubuntu/pool/main/f/flac/flac_1.3.3-1build1.debian.tar.xz' flac_1.3.3-1build1.debian.tar.xz 17096 SHA256:99c6512f88344bfea1106abe530e64c5e73687cc44c89f513d556c540d1c5b19
```

### `dpkg` source package: `flite=2.1-release-3`

Binary Packages:

- `libflite1:amd64=2.1-release-3`

Licenses: (parsed from: `/usr/share/doc/libflite1/copyright`)

- `GPL-2`
- `GPL-3`

Source:

```console
$ apt-get source -qq --print-uris flite=2.1-release-3
'http://archive.ubuntu.com/ubuntu/pool/universe/f/flite/flite_2.1-release-3.dsc' flite_2.1-release-3.dsc 2246 SHA256:3b5c97f6b3c8ff983d1fa76cb9e31e32179c4acc82a8b104a859f69348200489
'http://archive.ubuntu.com/ubuntu/pool/universe/f/flite/flite_2.1-release.orig.tar.bz2' flite_2.1-release.orig.tar.bz2 14816327 SHA256:c73c3f6a2ea764977d6eaf0a287722d1e2066b4697088c552e342c790f3d2b85
'http://archive.ubuntu.com/ubuntu/pool/universe/f/flite/flite_2.1-release-3.debian.tar.xz' flite_2.1-release-3.debian.tar.xz 48480 SHA256:c6dc054e21a06453026e3c9a80a20317eed206f5a055a38e853c32e666961f5c
```

### `dpkg` source package: `fontconfig=2.13.1-2ubuntu3`

Binary Packages:

- `fontconfig=2.13.1-2ubuntu3`
- `fontconfig-config=2.13.1-2ubuntu3`
- `libfontconfig1:amd64=2.13.1-2ubuntu3`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris fontconfig=2.13.1-2ubuntu3
'http://archive.ubuntu.com/ubuntu/pool/main/f/fontconfig/fontconfig_2.13.1-2ubuntu3.dsc' fontconfig_2.13.1-2ubuntu3.dsc 1959 SHA256:a9eebf6e6e88aa64d33fb3852c97718c212579f9714afd67cb6a9b8b116dd7aa
'http://archive.ubuntu.com/ubuntu/pool/main/f/fontconfig/fontconfig_2.13.1.orig.tar.bz2' fontconfig_2.13.1.orig.tar.bz2 1723639 SHA256:f655dd2a986d7aa97e052261b36aa67b0a64989496361eca8d604e6414006741
'http://archive.ubuntu.com/ubuntu/pool/main/f/fontconfig/fontconfig_2.13.1-2ubuntu3.debian.tar.xz' fontconfig_2.13.1-2ubuntu3.debian.tar.xz 26344 SHA256:342671f6a1e6d392958a6eec27541c6bdffc6498b469dcc46eca66c9d23a863a
```

### `dpkg` source package: `fonts-android=1:6.0.1r16-1.1`

Binary Packages:

- `fonts-droid-fallback=1:6.0.1r16-1.1`

Licenses: (parsed from: `/usr/share/doc/fonts-droid-fallback/copyright`)

- `Apache-2`
- `Apache-2.0`

Source:

```console
$ apt-get source -qq --print-uris fonts-android=1:6.0.1r16-1.1
'http://archive.ubuntu.com/ubuntu/pool/main/f/fonts-android/fonts-android_6.0.1r16-1.1.dsc' fonts-android_6.0.1r16-1.1.dsc 2182 SHA256:17cf1a74074c63cd5d0cc03d81d780ecd0fcb1d8d2ab1bdafb0b6dd30c278500
'http://archive.ubuntu.com/ubuntu/pool/main/f/fonts-android/fonts-android_6.0.1r16.orig.tar.gz' fonts-android_6.0.1r16.orig.tar.gz 6759063 SHA256:7f14d7b3f3ab0c7258aa7edac4ea1071cb2f22c5820c4eb617095de2ef90d4d8
'http://archive.ubuntu.com/ubuntu/pool/main/f/fonts-android/fonts-android_6.0.1r16-1.1.debian.tar.xz' fonts-android_6.0.1r16-1.1.debian.tar.xz 6880 SHA256:6beeb5dfb083b73753382d2ca46faaa976c39ca6365dd33f08106d077c525b29
```

### `dpkg` source package: `fonts-crosextra-caladea=20130214-2`

Binary Packages:

- `fonts-crosextra-caladea=20130214-2`

Licenses: (parsed from: `/usr/share/doc/fonts-crosextra-caladea/copyright`)

- `Apache-2.0`

Source:

```console
$ apt-get source -qq --print-uris fonts-crosextra-caladea=20130214-2
'http://archive.ubuntu.com/ubuntu/pool/universe/f/fonts-crosextra-caladea/fonts-crosextra-caladea_20130214-2.dsc' fonts-crosextra-caladea_20130214-2.dsc 2159 SHA256:199586f82712aa579daa428e049d3f4a747712a34ce9d8ebcf22c27072ce3252
'http://archive.ubuntu.com/ubuntu/pool/universe/f/fonts-crosextra-caladea/fonts-crosextra-caladea_20130214.orig.tar.gz' fonts-crosextra-caladea_20130214.orig.tar.gz 112756 SHA256:c48d1c2fd613c9c06c959c34da7b8388059e2408d2bb19845dc3ed35f76e4d09
'http://archive.ubuntu.com/ubuntu/pool/universe/f/fonts-crosextra-caladea/fonts-crosextra-caladea_20130214-2.debian.tar.xz' fonts-crosextra-caladea_20130214-2.debian.tar.xz 2504 SHA256:df38b095635456c3979c19e81e28c656e56675b138e4b989c25bb08b2e2eac11
```

### `dpkg` source package: `fonts-crosextra-carlito=20130920-1`

Binary Packages:

- `fonts-crosextra-carlito=20130920-1`

Licenses: (parsed from: `/usr/share/doc/fonts-crosextra-carlito/copyright`)

- `GPL-2`
- `GPL-2+`
- `OFL-1.1`

Source:

```console
$ apt-get source -qq --print-uris fonts-crosextra-carlito=20130920-1
'http://archive.ubuntu.com/ubuntu/pool/universe/f/fonts-crosextra-carlito/fonts-crosextra-carlito_20130920-1.dsc' fonts-crosextra-carlito_20130920-1.dsc 2137 SHA256:53be1d931294feafcee69fb3b4dd47b62d80d1cc3d42057e007dc4c681ef9e06
'http://archive.ubuntu.com/ubuntu/pool/universe/f/fonts-crosextra-carlito/fonts-crosextra-carlito_20130920.orig.tar.gz' fonts-crosextra-carlito_20130920.orig.tar.gz 1169488 SHA256:4bd12b6cbc321c1cf16da76e2c585c925ce956a08067ae6f6c64eff6ccfdaf5a
'http://archive.ubuntu.com/ubuntu/pool/universe/f/fonts-crosextra-carlito/fonts-crosextra-carlito_20130920-1.debian.tar.gz' fonts-crosextra-carlito_20130920-1.debian.tar.gz 4426 SHA256:cd2dcd85536b873a7f3e5c034efa55b93ed5e1c2e102951ce067042ac7863b38
```

### `dpkg` source package: `fonts-dejavu=2.37-1`

Binary Packages:

- `fonts-dejavu=2.37-1`
- `fonts-dejavu-core=2.37-1`
- `fonts-dejavu-extra=2.37-1`

Licenses: (parsed from: `/usr/share/doc/fonts-dejavu/copyright`, `/usr/share/doc/fonts-dejavu-core/copyright`, `/usr/share/doc/fonts-dejavu-extra/copyright`)

- `GPL-2`
- `GPL-2+`
- `bitstream-vera`

Source:

```console
$ apt-get source -qq --print-uris fonts-dejavu=2.37-1
'http://archive.ubuntu.com/ubuntu/pool/main/f/fonts-dejavu/fonts-dejavu_2.37-1.dsc' fonts-dejavu_2.37-1.dsc 2575 SHA256:f35ff7b2c8dbfda6564c9dedf088ba06cc6d279fdd8e7cccbd1ae08ded1bb71c
'http://archive.ubuntu.com/ubuntu/pool/main/f/fonts-dejavu/fonts-dejavu_2.37.orig.tar.bz2' fonts-dejavu_2.37.orig.tar.bz2 12050109 SHA256:4b21c5203f792343d5e90ab1cb0cf07e99887218abe3d83cd9a98cea9085e799
'http://archive.ubuntu.com/ubuntu/pool/main/f/fonts-dejavu/fonts-dejavu_2.37-1.debian.tar.xz' fonts-dejavu_2.37-1.debian.tar.xz 10424 SHA256:5105cdbfc086f4a83ab6871eb39cc904bf02aa52762402b7cacf33d0938122f7
```

### `dpkg` source package: `fonts-liberation2=2.1.0-1`

Binary Packages:

- `fonts-liberation2=2.1.0-1`

Licenses: (parsed from: `/usr/share/doc/fonts-liberation2/copyright`)

- `GPL-2`
- `GPL-2+`
- `SIL-OFL-1.1`

Source:

```console
$ apt-get source -qq --print-uris fonts-liberation2=2.1.0-1
'http://archive.ubuntu.com/ubuntu/pool/main/f/fonts-liberation2/fonts-liberation2_2.1.0-1.dsc' fonts-liberation2_2.1.0-1.dsc 2222 SHA256:ebfe5d369f83f6bf22edb3d8d4907450e2366509a8966ecb84303fe78690eb3e
'http://archive.ubuntu.com/ubuntu/pool/main/f/fonts-liberation2/fonts-liberation2_2.1.0.orig.tar.gz' fonts-liberation2_2.1.0.orig.tar.gz 4986277 SHA256:a7761e42b43dfd0b4580f411829062ef98a6de6c2bafe0bfaf3041eb057d9107
'http://archive.ubuntu.com/ubuntu/pool/main/f/fonts-liberation2/fonts-liberation2_2.1.0-1.debian.tar.xz' fonts-liberation2_2.1.0-1.debian.tar.xz 7456 SHA256:57291e9eb7ad8c579e80f9da502ca9b3a21d518f85b9ad1fc24959b45b487cad
```

### `dpkg` source package: `fonts-liberation=1:1.07.4-11`

Binary Packages:

- `fonts-liberation=1:1.07.4-11`

Licenses: (parsed from: `/usr/share/doc/fonts-liberation/copyright`)

- `GPL-2`

Source:

```console
$ apt-get source -qq --print-uris fonts-liberation=1:1.07.4-11
'http://archive.ubuntu.com/ubuntu/pool/main/f/fonts-liberation/fonts-liberation_1.07.4-11.dsc' fonts-liberation_1.07.4-11.dsc 2176 SHA256:c9cb6abc8b8ab1887d78f5fc1aef1fc06a220efbfa3424ca7e8bc379242654ba
'http://archive.ubuntu.com/ubuntu/pool/main/f/fonts-liberation/fonts-liberation_1.07.4.orig.tar.gz' fonts-liberation_1.07.4.orig.tar.gz 2937949 SHA256:ad98b7498dc2992f7f0868f79b65ce4a720a3acdb63ab3f1f1cb6881117a5406
'http://archive.ubuntu.com/ubuntu/pool/main/f/fonts-liberation/fonts-liberation_1.07.4-11.debian.tar.xz' fonts-liberation_1.07.4-11.debian.tar.xz 17248 SHA256:45ea32aab7eed82061493edbf5ef621d6fd7f074cb79ae45206014913ca5f313
```

### `dpkg` source package: `fonts-linuxlibertine=5.3.0-4`

Binary Packages:

- `fonts-linuxlibertine=5.3.0-4`

Licenses: (parsed from: `/usr/share/doc/fonts-linuxlibertine/copyright`)

- `GPL-2`
- `GPL-2+`
- `GPL-2+ with Font exception`
- `OFL-1.1`

Source:

```console
$ apt-get source -qq --print-uris fonts-linuxlibertine=5.3.0-4
'http://archive.ubuntu.com/ubuntu/pool/universe/f/fonts-linuxlibertine/fonts-linuxlibertine_5.3.0-4.dsc' fonts-linuxlibertine_5.3.0-4.dsc 2113 SHA256:127569684fe5ab96f2294dedb7425097a410abd31aac3e2e58b54b6fdcc9abab
'http://archive.ubuntu.com/ubuntu/pool/universe/f/fonts-linuxlibertine/fonts-linuxlibertine_5.3.0.orig.tar.xz' fonts-linuxlibertine_5.3.0.orig.tar.xz 3748996 SHA256:9226823829273175c38ab84c4991d61ceaeded7c6ce50a2bab061d14e441a2a7
'http://archive.ubuntu.com/ubuntu/pool/universe/f/fonts-linuxlibertine/fonts-linuxlibertine_5.3.0-4.debian.tar.xz' fonts-linuxlibertine_5.3.0-4.debian.tar.xz 8764 SHA256:d7eb3eac6641411f55578ec8ca02f0da6c76f03dadf3a7cbe1784448ccce2d6f
```

### `dpkg` source package: `fonts-noto=20200323-1build1~ubuntu20.04.1`

Binary Packages:

- `fonts-noto-core=20200323-1build1~ubuntu20.04.1`
- `fonts-noto-extra=20200323-1build1~ubuntu20.04.1`
- `fonts-noto-mono=20200323-1build1~ubuntu20.04.1`
- `fonts-noto-ui-core=20200323-1build1~ubuntu20.04.1`

Licenses: (parsed from: `/usr/share/doc/fonts-noto-core/copyright`, `/usr/share/doc/fonts-noto-extra/copyright`, `/usr/share/doc/fonts-noto-mono/copyright`, `/usr/share/doc/fonts-noto-ui-core/copyright`)

- `Apache-2.0`
- `GPL-3`
- `GPL-3+`
- `OFL-1.1`

Source:

```console
$ apt-get source -qq --print-uris fonts-noto=20200323-1build1~ubuntu20.04.1
'http://archive.ubuntu.com/ubuntu/pool/main/f/fonts-noto/fonts-noto_20200323-1build1~ubuntu20.04.1.dsc' fonts-noto_20200323-1build1~ubuntu20.04.1.dsc 2564 SHA512:f5b486186a324f0ce24564ef7853a475f6bc431b7bd6bb6ae9d699f22442cf16cfb64e0cd81caa134ceef25e4ea16276597c480cffa8f1a3a6792b7441c9257a
'http://archive.ubuntu.com/ubuntu/pool/main/f/fonts-noto/fonts-noto_20200323.orig.tar.gz' fonts-noto_20200323.orig.tar.gz 854165462 SHA512:1611fb2e93cf2227a3270c323ecb99fb87135fdfbd27dbb8595465279cdae8744f57774f9804cd01b064d63e7bcfbfe986212084dae546739b8f341442056ef6
'http://archive.ubuntu.com/ubuntu/pool/main/f/fonts-noto/fonts-noto_20200323-1build1~ubuntu20.04.1.debian.tar.xz' fonts-noto_20200323-1build1~ubuntu20.04.1.debian.tar.xz 110420 SHA512:320c3f90724958a09defa7da4dc399976ad0b98c795328166c3198d8e37a9e570169befc83561a7636a9284a9cb87a0b319ae215929c82c991559b88cfa77e31
```

### `dpkg` source package: `fonts-sil-gentium-basic=1.102-1`

Binary Packages:

- `fonts-sil-gentium-basic=1.102-1`

Licenses: (parsed from: `/usr/share/doc/fonts-sil-gentium-basic/copyright`)

- `OFL-1.1`

Source:

```console
$ apt-get source -qq --print-uris fonts-sil-gentium-basic=1.102-1
'http://archive.ubuntu.com/ubuntu/pool/universe/f/fonts-sil-gentium-basic/fonts-sil-gentium-basic_1.102-1.dsc' fonts-sil-gentium-basic_1.102-1.dsc 2160 SHA256:246df276386d08b62316341e820787a45d2e796f150221f3ddc623633c71b304
'http://archive.ubuntu.com/ubuntu/pool/universe/f/fonts-sil-gentium-basic/fonts-sil-gentium-basic_1.102.orig.tar.xz' fonts-sil-gentium-basic_1.102.orig.tar.xz 380396 SHA256:a7c89371d71ee45a7c12072df75c0ef790674e39fdde747b46d6e4cfb2219f7d
'http://archive.ubuntu.com/ubuntu/pool/universe/f/fonts-sil-gentium-basic/fonts-sil-gentium-basic_1.102-1.debian.tar.xz' fonts-sil-gentium-basic_1.102-1.debian.tar.xz 5184 SHA256:ff836606909f0cc9afc06efde1e7657770f13a779e890b06897b2f599eb00d3b
```

### `dpkg` source package: `fonts-sil-gentium=20081126:1.03-2`

Binary Packages:

- `fonts-sil-gentium=20081126:1.03-2`

Licenses: (parsed from: `/usr/share/doc/fonts-sil-gentium/copyright`)

- `OFL-1.1`

Source:

```console
$ apt-get source -qq --print-uris fonts-sil-gentium=20081126:1.03-2
'http://archive.ubuntu.com/ubuntu/pool/universe/f/fonts-sil-gentium/fonts-sil-gentium_1.03-2.dsc' fonts-sil-gentium_1.03-2.dsc 2107 SHA256:d75b0fded4ab6aa63bd7adec8accdcf78e117274d251709f58ed6b2b3b60f301
'http://archive.ubuntu.com/ubuntu/pool/universe/f/fonts-sil-gentium/fonts-sil-gentium_1.03.orig.tar.xz' fonts-sil-gentium_1.03.orig.tar.xz 428764 SHA256:0754edaec5837f899caf9248496438cf5adc5f2795ee36ac19e49f08bdfd751c
'http://archive.ubuntu.com/ubuntu/pool/universe/f/fonts-sil-gentium/fonts-sil-gentium_1.03-2.debian.tar.xz' fonts-sil-gentium_1.03-2.debian.tar.xz 6528 SHA256:e1b49eb3e9c2a210309b7a6177d83e99b851ed1d1cd60ccd5c9af329b880dbf9
```

### `dpkg` source package: `fonts-urw-base35=20170801.1-3`

Binary Packages:

- `fonts-urw-base35=20170801.1-3`

Licenses: (parsed from: `/usr/share/doc/fonts-urw-base35/copyright`)

- `AGPL-3`
- `AGPL-3 with Font exception`
- `GPL-2`
- `GPL-2+`

Source:

```console
$ apt-get source -qq --print-uris fonts-urw-base35=20170801.1-3
'http://archive.ubuntu.com/ubuntu/pool/main/f/fonts-urw-base35/fonts-urw-base35_20170801.1-3.dsc' fonts-urw-base35_20170801.1-3.dsc 2075 SHA256:6022f3e2e1e1c2504ee7b7565ed2f2682441db90cd15653abbfa8b963de05949
'http://archive.ubuntu.com/ubuntu/pool/main/f/fonts-urw-base35/fonts-urw-base35_20170801.1.orig.tar.gz' fonts-urw-base35_20170801.1.orig.tar.gz 11147338 SHA256:874da009413a9a69175e3a42eb707352e7a1a66ed77868139761f6face220c41
'http://archive.ubuntu.com/ubuntu/pool/main/f/fonts-urw-base35/fonts-urw-base35_20170801.1-3.debian.tar.xz' fonts-urw-base35_20170801.1-3.debian.tar.xz 17728 SHA256:d96fed0783c169f44c57e9c8b0e10f3846deae3a130b580664880172244c9c76
```

### `dpkg` source package: `freetype=2.10.1-2ubuntu0.1`

Binary Packages:

- `libfreetype6:amd64=2.10.1-2ubuntu0.1`

Licenses: (parsed from: `/usr/share/doc/libfreetype6/copyright`)

- `Apache-2.0`
- `BSD-3-Clause`
- `FSFUL`
- `FSFULLR`
- `FTL`
- `GPL-2`
- `GPL-2+`
- `GPL-3`
- `GPL-3+`
- `MIT`
- `MPL-1.1`
- `OFL-1.1`
- `OpenGroup-BSD-like`
- `Permissive`
- `Public-Domain`
- `Zlib`

Source:

```console
$ apt-get source -qq --print-uris freetype=2.10.1-2ubuntu0.1
'http://archive.ubuntu.com/ubuntu/pool/main/f/freetype/freetype_2.10.1-2ubuntu0.1.dsc' freetype_2.10.1-2ubuntu0.1.dsc 3556 SHA512:dc45a1f920cdee10c2b2a6d400d05d089b8467f1f9fa6eca5be61e75567acd58b17c7e2ac1d6faec9ff5edee38cb3489323709231506264bc6d1a9f4a20fe2ac
'http://archive.ubuntu.com/ubuntu/pool/main/f/freetype/freetype_2.10.1.orig-ft2demos.tar.gz' freetype_2.10.1.orig-ft2demos.tar.gz 305328 SHA512:67b9dc1c03ca5588a53edd8b9cb7f27e5b52a5730add6887e6af776176ab66099bfe4a9e69d036511d32ae2f96e822a71a3c9213f1adfcc6fa45be81adf56f77
'http://archive.ubuntu.com/ubuntu/pool/main/f/freetype/freetype_2.10.1.orig-ft2demos.tar.gz.asc' freetype_2.10.1.orig-ft2demos.tar.gz.asc 195 SHA512:8279e9e7ea4da030db388ac5960808d652553b97dc65bc517ebcae90834188d75101fbe29d334a0e2b0a17a723c7121ac28b1f14bab0bf29ec4c9c6df6575a67
'http://archive.ubuntu.com/ubuntu/pool/main/f/freetype/freetype_2.10.1.orig-ft2docs.tar.gz' freetype_2.10.1.orig-ft2docs.tar.gz 2123885 SHA512:05dbe26c291d3fa39c167f3aa5d8da0f3d3992f8e7ec74e936547b3feb17c1a59753a111fc33b2edce12ed991c61161c0ef7084b91c770d73c4679b62edd5b2f
'http://archive.ubuntu.com/ubuntu/pool/main/f/freetype/freetype_2.10.1.orig-ft2docs.tar.gz.asc' freetype_2.10.1.orig-ft2docs.tar.gz.asc 195 SHA512:48e283c72d808901b9754bb20242d493628610326f3492c6d1aa35fcdffffd4ec320f589d18442735cfc6cda7238399f4f339d58e4a536da46e2b5a13864972e
'http://archive.ubuntu.com/ubuntu/pool/main/f/freetype/freetype_2.10.1.orig.tar.gz' freetype_2.10.1.orig.tar.gz 3478158 SHA512:346c682744bcf06ca9d71265c108a242ad7d78443eff20142454b72eef47ba6d76671a6e931ed4c4c9091dd8f8515ebdd71202d94b073d77931345ff93cfeaa7
'http://archive.ubuntu.com/ubuntu/pool/main/f/freetype/freetype_2.10.1.orig.tar.gz.asc' freetype_2.10.1.orig.tar.gz.asc 195 SHA512:2a2750605d0fd11fbfe4f76d47ccd8e300473c3043b28a5c46f4f628e1da2c2f2308ee4f1b1b585daaf2c4b408718ee68eab6c5411e993ad9f95b08c35248178
'http://archive.ubuntu.com/ubuntu/pool/main/f/freetype/freetype_2.10.1-2ubuntu0.1.debian.tar.xz' freetype_2.10.1-2ubuntu0.1.debian.tar.xz 115576 SHA512:c24138956061abdb0c22ade45eb5ef4daa24f798310691701525face08fe372d3e32d5000cc1f078acc733da40623cc061a7ab84dab64c649f484c840b110593
```

### `dpkg` source package: `fribidi=1.0.8-2`

Binary Packages:

- `libfribidi0:amd64=1.0.8-2`

Licenses: (parsed from: `/usr/share/doc/libfribidi0/copyright`)

- `LGPL-2.1`
- `LGPL-2.1+`

Source:

```console
$ apt-get source -qq --print-uris fribidi=1.0.8-2
'http://archive.ubuntu.com/ubuntu/pool/main/f/fribidi/fribidi_1.0.8-2.dsc' fribidi_1.0.8-2.dsc 1987 SHA256:f1b396620cda57e93799725abad47089902429295f7b3555bc3d5b3f00a79340
'http://archive.ubuntu.com/ubuntu/pool/main/f/fribidi/fribidi_1.0.8.orig.tar.bz2' fribidi_1.0.8.orig.tar.bz2 2077095 SHA256:94c7b68d86ad2a9613b4dcffe7bbeb03523d63b5b37918bdf2e4ef34195c1e6c
'http://archive.ubuntu.com/ubuntu/pool/main/f/fribidi/fribidi_1.0.8-2.debian.tar.xz' fribidi_1.0.8-2.debian.tar.xz 8980 SHA256:898fc0b48625ce31e29d2f9501f17b9991b16b03816db6467faaedb85d22f00b
```

### `dpkg` source package: `game-music-emu=0.6.2-1build1`

Binary Packages:

- `libgme0:amd64=0.6.2-1build1`

Licenses: (parsed from: `/usr/share/doc/libgme0/copyright`)

- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris game-music-emu=0.6.2-1build1
'http://archive.ubuntu.com/ubuntu/pool/universe/g/game-music-emu/game-music-emu_0.6.2-1build1.dsc' game-music-emu_0.6.2-1build1.dsc 1900 SHA256:f1e2f34b75e67c2fbe6412441d8d34ba7999cdbda2cbf9410d741c0e85bb3e93
'http://archive.ubuntu.com/ubuntu/pool/universe/g/game-music-emu/game-music-emu_0.6.2.orig.tar.xz' game-music-emu_0.6.2.orig.tar.xz 163052 SHA256:5046cb471d422dbe948b5f5dd4e5552aaef52a0899c4b2688e5a68a556af7342
'http://archive.ubuntu.com/ubuntu/pool/universe/g/game-music-emu/game-music-emu_0.6.2-1build1.debian.tar.xz' game-music-emu_0.6.2-1build1.debian.tar.xz 4496 SHA256:e338369a1656f0ade5b014f714fd5bac196489ac937cf8c5276a005953b92aa8
```

### `dpkg` source package: `gcc-10=10.3.0-1ubuntu1~20.04`

Binary Packages:

- `gcc-10-base:amd64=10.3.0-1ubuntu1~20.04`
- `libgcc-s1:amd64=10.3.0-1ubuntu1~20.04`
- `libgomp1:amd64=10.3.0-1ubuntu1~20.04`
- `libstdc++6:amd64=10.3.0-1ubuntu1~20.04`

Licenses: (parsed from: `/usr/share/doc/gcc-10-base/copyright`, `/usr/share/doc/libgcc-s1/copyright`, `/usr/share/doc/libgomp1/copyright`, `/usr/share/doc/libstdc++6/copyright`)

- `Artistic`
- `GFDL-1.2`
- `GPL`
- `GPL-2`
- `GPL-3`
- `LGPL`

Source:

```console
$ apt-get source -qq --print-uris gcc-10=10.3.0-1ubuntu1~20.04
'http://archive.ubuntu.com/ubuntu/pool/main/g/gcc-10/gcc-10_10.3.0-1ubuntu1~20.04.dsc' gcc-10_10.3.0-1ubuntu1~20.04.dsc 31155 SHA512:c67d612edfbf2a97d4c3f436b1fdbc08b7387907a3e0cc49a9ba2a360db054b3d07082097c4b98abc9ace7b8902637f8d7126de229c164cab743eff4fd5d7520
'http://archive.ubuntu.com/ubuntu/pool/main/g/gcc-10/gcc-10_10.3.0.orig.tar.gz' gcc-10_10.3.0.orig.tar.gz 83679201 SHA512:c7f0fb09c79bc8c372171df7f23af6970a389dcc1237ebc0821c0a754d93c6e4065433f7ad63aed9f9b8fce2da7fce3caa61ae2e2afab494d06bf33b06a10775
'http://archive.ubuntu.com/ubuntu/pool/main/g/gcc-10/gcc-10_10.3.0-1ubuntu1~20.04.debian.tar.xz' gcc-10_10.3.0-1ubuntu1~20.04.debian.tar.xz 580864 SHA512:3ae060b517a67ae2410b48c83a7836ec25e83422cb3db5dbe55cce9e97609aff82062aa55a84c64ca9c52f405a41d2da20c1d016b15521102cac3b82895f3975
```

### `dpkg` source package: `gcc-9=9.3.0-17ubuntu1~20.04`

Binary Packages:

- `cpp-9=9.3.0-17ubuntu1~20.04`
- `gcc-9-base:amd64=9.3.0-17ubuntu1~20.04`

Licenses: (parsed from: `/usr/share/doc/cpp-9/copyright`, `/usr/share/doc/gcc-9-base/copyright`)

- `Artistic`
- `GFDL-1.2`
- `GPL`
- `GPL-2`
- `GPL-3`
- `LGPL`
- `LGPL-2.1+`

Source:

```console
$ apt-get source -qq --print-uris gcc-9=9.3.0-17ubuntu1~20.04
'http://archive.ubuntu.com/ubuntu/pool/main/g/gcc-9/gcc-9_9.3.0-17ubuntu1~20.04.dsc' gcc-9_9.3.0-17ubuntu1~20.04.dsc 23746 SHA512:bae6c156516a4988546a4518934f661a70243a89ed17883fe382bd984ae6533aab7d9b459986c2ebdb715b64b11ac76b9283447b26be3dbaec0b00c02afeb7f6
'http://archive.ubuntu.com/ubuntu/pool/main/g/gcc-9/gcc-9_9.3.0.orig.tar.gz' gcc-9_9.3.0.orig.tar.gz 90490748 SHA512:cebaa27b0ac7485e72f0d7b07e6ef08cd544bf551bc080ee00175cbe319ea8b0592ff54d55633bb189e481f9ba489d513205cf2310e4b5211869a021432ee31f
'http://archive.ubuntu.com/ubuntu/pool/main/g/gcc-9/gcc-9_9.3.0-17ubuntu1~20.04.debian.tar.xz' gcc-9_9.3.0-17ubuntu1~20.04.debian.tar.xz 763792 SHA512:f696a3d92edbadb7cfb29494b68ec00fa9b278ab7657e45933cf2e7564e6a524ac41edde14449114a2e06ad0c1f56473d998687aeb2f98f18c431727d4254d21
```

### `dpkg` source package: `gcc-defaults=1.185.1ubuntu2`

Binary Packages:

- `cpp=4:9.3.0-1ubuntu2`

Licenses: (parsed from: `/usr/share/doc/cpp/copyright`)

- `GPL`

Source:

```console
$ apt-get source -qq --print-uris gcc-defaults=1.185.1ubuntu2
'http://archive.ubuntu.com/ubuntu/pool/main/g/gcc-defaults/gcc-defaults_1.185.1ubuntu2.dsc' gcc-defaults_1.185.1ubuntu2.dsc 16544 SHA256:32c0331bc75ecbc0d013b9e11401d1fc64cbd7b0198274cb25a183a27b5c407f
'http://archive.ubuntu.com/ubuntu/pool/main/g/gcc-defaults/gcc-defaults_1.185.1ubuntu2.tar.gz' gcc-defaults_1.185.1ubuntu2.tar.gz 58807 SHA256:342b5842c03073717bc98d6d9de7eb79027a1239735637743006933e5d44bb05
```

### `dpkg` source package: `gdbm=1.18.1-5`

Binary Packages:

- `libgdbm-compat4:amd64=1.18.1-5`
- `libgdbm6:amd64=1.18.1-5`

Licenses: (parsed from: `/usr/share/doc/libgdbm-compat4/copyright`, `/usr/share/doc/libgdbm6/copyright`)

- `GFDL-NIV-1.3+`
- `GPL-2`
- `GPL-2+`
- `GPL-3`
- `GPL-3+`

Source:

```console
$ apt-get source -qq --print-uris gdbm=1.18.1-5
'http://archive.ubuntu.com/ubuntu/pool/main/g/gdbm/gdbm_1.18.1-5.dsc' gdbm_1.18.1-5.dsc 2635 SHA256:4c0c4498378c673c9d2d8dfb5b319a4830d2dd21e65faaaa8e0f09cb7f71606b
'http://archive.ubuntu.com/ubuntu/pool/main/g/gdbm/gdbm_1.18.1.orig.tar.gz' gdbm_1.18.1.orig.tar.gz 941863 SHA256:86e613527e5dba544e73208f42b78b7c022d4fa5a6d5498bf18c8d6f745b91dc
'http://archive.ubuntu.com/ubuntu/pool/main/g/gdbm/gdbm_1.18.1.orig.tar.gz.asc' gdbm_1.18.1.orig.tar.gz.asc 412 SHA256:3254738e7689e44ac65e78a766806828b8282e6bb1c0e5bb6156a99e567889a5
'http://archive.ubuntu.com/ubuntu/pool/main/g/gdbm/gdbm_1.18.1-5.debian.tar.xz' gdbm_1.18.1-5.debian.tar.xz 16348 SHA256:3c1a0e05b40a97ee51ce77c736c72c37738ba31b2720111d3bc99175a2c3a3ed
```

### `dpkg` source package: `gdk-pixbuf=2.40.0+dfsg-3ubuntu0.2`

Binary Packages:

- `libgdk-pixbuf2.0-0:amd64=2.40.0+dfsg-3ubuntu0.2`
- `libgdk-pixbuf2.0-bin=2.40.0+dfsg-3ubuntu0.2`
- `libgdk-pixbuf2.0-common=2.40.0+dfsg-3ubuntu0.2`

Licenses: (parsed from: `/usr/share/doc/libgdk-pixbuf2.0-0/copyright`, `/usr/share/doc/libgdk-pixbuf2.0-bin/copyright`, `/usr/share/doc/libgdk-pixbuf2.0-common/copyright`)

- `GPL-2`
- `GPL-2+`
- `LGPL-2`
- `LGPL-2+`
- `MPL-1.1-or-LGPL-2+`

Source:

```console
$ apt-get source -qq --print-uris gdk-pixbuf=2.40.0+dfsg-3ubuntu0.2
'http://archive.ubuntu.com/ubuntu/pool/main/g/gdk-pixbuf/gdk-pixbuf_2.40.0+dfsg-3ubuntu0.2.dsc' gdk-pixbuf_2.40.0+dfsg-3ubuntu0.2.dsc 3062 SHA512:95ab33c59837774b9f77348e876a8b8f8ed904a7967f5db3c54aa70b65c0209a218075fcdfe4f9b76cc2b8a7269e8f383eba832ada35c23de0895710f12fc555
'http://archive.ubuntu.com/ubuntu/pool/main/g/gdk-pixbuf/gdk-pixbuf_2.40.0+dfsg.orig.tar.xz' gdk-pixbuf_2.40.0+dfsg.orig.tar.xz 5626144 SHA512:bb8a9d1837bffdc5f50307dba1a1e6f1ac015e6e670ea6cae6d0bc997afa106ff0d928cb847d76848c480a06e1ad3945274b4913eaa4d9a8074797fc82bb7c1f
'http://archive.ubuntu.com/ubuntu/pool/main/g/gdk-pixbuf/gdk-pixbuf_2.40.0+dfsg-3ubuntu0.2.debian.tar.xz' gdk-pixbuf_2.40.0+dfsg-3ubuntu0.2.debian.tar.xz 18560 SHA512:64bbaa7879a19745af00cf4c45a800ccbf43362c2ce11d87af519e05129f65110f94587ca1ef0bcbd0d5fe7a42ee9a68720d53ad4a81ade9041608d3d4be9d54
```

### `dpkg` source package: `ghostscript=9.50~dfsg-5ubuntu4.3`

Binary Packages:

- `ghostscript=9.50~dfsg-5ubuntu4.3`
- `libgs9:amd64=9.50~dfsg-5ubuntu4.3`
- `libgs9-common=9.50~dfsg-5ubuntu4.3`

Licenses: (parsed from: `/usr/share/doc/ghostscript/copyright`, `/usr/share/doc/libgs9/copyright`, `/usr/share/doc/libgs9-common/copyright`)

- `AGPL-3`
- `AGPL-3+`
- `AGPL-3+ with font exception`
- `Apache-2.0`
- `BSD-3-Clause`
- `BSD-3-Clause~Adobe`
- `Expat`
- `Expat~Ghostgum`
- `Expat~SunSoft`
- `Expat~SunSoft with SunSoft exception`
- `FTL`
- `GAP~configure`
- `GPL`
- `GPL-2`
- `GPL-2+`
- `GPL-3`
- `GPL-3+`
- `ISC`
- `LGPL-2.1`
- `NTP~Lucent`
- `NTP~Open`
- `NTP~WSU`
- `ZLIB`
- `other`
- `public-domain`

Source:

```console
$ apt-get source -qq --print-uris ghostscript=9.50~dfsg-5ubuntu4.3
'http://archive.ubuntu.com/ubuntu/pool/main/g/ghostscript/ghostscript_9.50~dfsg-5ubuntu4.3.dsc' ghostscript_9.50~dfsg-5ubuntu4.3.dsc 2914 SHA512:41d4217a8f180ec6563e113ee6be9f97d30ccffa3765c8cb4737f7673563308b756356ee5ffd51731e9311d2d81c79cd204bd940b6c4d3104eb8fe986ea94e82
'http://archive.ubuntu.com/ubuntu/pool/main/g/ghostscript/ghostscript_9.50~dfsg.orig.tar.xz' ghostscript_9.50~dfsg.orig.tar.xz 19139148 SHA512:75730872b02fe7341bf6c84fc4b53002f88d2c4c538dee62f749816a4963740f2a369e3a3760175e0f5b73547d5df805a152f5f0120db79ffecc871f5f14be86
'http://archive.ubuntu.com/ubuntu/pool/main/g/ghostscript/ghostscript_9.50~dfsg-5ubuntu4.3.debian.tar.xz' ghostscript_9.50~dfsg-5ubuntu4.3.debian.tar.xz 131000 SHA512:e76cbba83c5591617920c44bf1b662a696d38427dae5755902bf32e6006ba51a171a82d46c4b289cb0c93290279407e91e8d06f16ec5b8e37dfcbbf3a681ff4c
```

### `dpkg` source package: `giflib=5.1.9-1`

Binary Packages:

- `libgif7:amd64=5.1.9-1`

Licenses: (parsed from: `/usr/share/doc/libgif7/copyright`)

- `ISC`
- `MIT`

Source:

```console
$ apt-get source -qq --print-uris giflib=5.1.9-1
'http://archive.ubuntu.com/ubuntu/pool/main/g/giflib/giflib_5.1.9-1.dsc' giflib_5.1.9-1.dsc 1933 SHA256:1d694ffc438056ab3415fa33ab390ef231b1e9943da393b745c9ec1909029e4e
'http://archive.ubuntu.com/ubuntu/pool/main/g/giflib/giflib_5.1.9.orig.tar.bz2' giflib_5.1.9.orig.tar.bz2 336304 SHA256:292b10b86a87cb05f9dcbe1b6c7b99f3187a106132dd14f1ba79c90f561c3295
'http://archive.ubuntu.com/ubuntu/pool/main/g/giflib/giflib_5.1.9-1.debian.tar.xz' giflib_5.1.9-1.debian.tar.xz 8308 SHA256:fa7d879571e40ecbea6934f0fa3100a7cba0f7313c2de8ff61d62294970ad86d
```

### `dpkg` source package: `glib-networking=2.64.2-1ubuntu0.1`

Binary Packages:

- `glib-networking:amd64=2.64.2-1ubuntu0.1`
- `glib-networking-common=2.64.2-1ubuntu0.1`
- `glib-networking-services=2.64.2-1ubuntu0.1`

Licenses: (parsed from: `/usr/share/doc/glib-networking/copyright`, `/usr/share/doc/glib-networking-common/copyright`, `/usr/share/doc/glib-networking-services/copyright`)

- `LGPL-2`
- `LGPL-2+`
- `LGPL-2.1`
- `LGPL-2.1+`
- `OpenSSL`

Source:

```console
$ apt-get source -qq --print-uris glib-networking=2.64.2-1ubuntu0.1
'http://archive.ubuntu.com/ubuntu/pool/main/g/glib-networking/glib-networking_2.64.2-1ubuntu0.1.dsc' glib-networking_2.64.2-1ubuntu0.1.dsc 2271 SHA512:d1110b603099ec5caceae3faa114e50a8b740f6ec9b551541c2785d1b05e8555d6c251bd66c18fba72428906128dc63cbdee53fbcd391e5cb77ca80fce254266
'http://archive.ubuntu.com/ubuntu/pool/main/g/glib-networking/glib-networking_2.64.2.orig.tar.xz' glib-networking_2.64.2.orig.tar.xz 189680 SHA512:b796d3ef82f7ceea1c69a1d4ef7cf09e88ff8e702e93abd37cca4ea2f09650c1bdfdf8d8c57360a4bbafdd8d3cf5f88f0aad668b5ce77b92b60c08b7456ffa12
'http://archive.ubuntu.com/ubuntu/pool/main/g/glib-networking/glib-networking_2.64.2-1ubuntu0.1.debian.tar.xz' glib-networking_2.64.2-1ubuntu0.1.debian.tar.xz 13028 SHA512:b1c2c41667ce96d3823f1d327ea75d23623a043b6bf200322094b5eaf7aa988f8075c170582d94fb5fcdc86266801b0e229c60a631bc869b6a97796da88c7906
```

### `dpkg` source package: `glib2.0=2.64.6-1~ubuntu20.04.4`

Binary Packages:

- `libglib2.0-0:amd64=2.64.6-1~ubuntu20.04.4`
- `libglib2.0-data=2.64.6-1~ubuntu20.04.4`

Licenses: (parsed from: `/usr/share/doc/libglib2.0-0/copyright`, `/usr/share/doc/libglib2.0-data/copyright`)

- `Expat`
- `GPL-2+`
- `LGPL`

Source:

```console
$ apt-get source -qq --print-uris glib2.0=2.64.6-1~ubuntu20.04.4
'http://archive.ubuntu.com/ubuntu/pool/main/g/glib2.0/glib2.0_2.64.6-1~ubuntu20.04.4.dsc' glib2.0_2.64.6-1~ubuntu20.04.4.dsc 3338 SHA512:536d1dd6d47741e816ebe94008cef11cf754a46070329723cf0cec45e49126fe43f5f58c9db5c6d425c9730d9acda4d1f4acb468d41d1d7e35a8141b13434f01
'http://archive.ubuntu.com/ubuntu/pool/main/g/glib2.0/glib2.0_2.64.6.orig.tar.xz' glib2.0_2.64.6.orig.tar.xz 4781576 SHA512:5cd82c4d9b143e7aa130c24e25fb9def06dd915ef8ad8ed3883931bf5cddecf69c2e669ef6aa1d910484ede75b671e7c48a4f3fe50aa78955bff57b04f0cf958
'http://archive.ubuntu.com/ubuntu/pool/main/g/glib2.0/glib2.0_2.64.6-1~ubuntu20.04.4.debian.tar.xz' glib2.0_2.64.6-1~ubuntu20.04.4.debian.tar.xz 111852 SHA512:4b4f05731fd40715aefa1c70aa06ae03ad8e11d77732246d9b0457fc1ae8b226a3a75b4c5ee86ed143dca4cd60e65504d903c4063735a5c1021bc5f86a17d059
```

### `dpkg` source package: `glibc=2.31-0ubuntu9.2`

Binary Packages:

- `libc-bin=2.31-0ubuntu9.2`
- `libc6:amd64=2.31-0ubuntu9.2`
- `locales=2.31-0ubuntu9.2`

Licenses: (parsed from: `/usr/share/doc/libc-bin/copyright`, `/usr/share/doc/libc6/copyright`, `/usr/share/doc/locales/copyright`)

- `GPL-2`
- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris glibc=2.31-0ubuntu9.2
'http://archive.ubuntu.com/ubuntu/pool/main/g/glibc/glibc_2.31-0ubuntu9.2.dsc' glibc_2.31-0ubuntu9.2.dsc 9561 SHA512:12afcea8313015a2de4bfed05818d5a52dccfa82a4dcf2264f155b38bcb39507d5a765aa6dd58105194af364d2366ae4c88ac1877173c2ef3776b5d0fc75443c
'http://archive.ubuntu.com/ubuntu/pool/main/g/glibc/glibc_2.31.orig.tar.xz' glibc_2.31.orig.tar.xz 17317924 SHA512:2ff56628fe935cacbdf1825534f15d45cb87a159cbdb2e6a981590eeb6174ed4b3ff7041519cdecbd4f624ac20b745e2dd9614c420dd3ea186b8f36bc4c2453c
'http://archive.ubuntu.com/ubuntu/pool/main/g/glibc/glibc_2.31-0ubuntu9.2.debian.tar.xz' glibc_2.31-0ubuntu9.2.debian.tar.xz 847208 SHA512:6985930d47b70f2c3cfd0ece3d5dce8414befd061cfbae7a1e8804f02ef81d73d4a219bc780492c8a6ca3c5a559b58082bea57372b2b9072539059a1694578e0
```

### `dpkg` source package: `gmp=2:6.2.0+dfsg-4`

Binary Packages:

- `libgmp10:amd64=2:6.2.0+dfsg-4`

Licenses: (parsed from: `/usr/share/doc/libgmp10/copyright`)

- `GPL`
- `GPL-2`
- `GPL-3`
- `LGPL-3`

Source:

```console
$ apt-get source -qq --print-uris gmp=2:6.2.0+dfsg-4
'http://archive.ubuntu.com/ubuntu/pool/main/g/gmp/gmp_6.2.0+dfsg-4.dsc' gmp_6.2.0+dfsg-4.dsc 2144 SHA256:4ca8c5bca982c78eb7679256a5d41b2c9363a6c3e3ee15ed765515bc328e9989
'http://archive.ubuntu.com/ubuntu/pool/main/g/gmp/gmp_6.2.0+dfsg.orig.tar.xz' gmp_6.2.0+dfsg.orig.tar.xz 1842912 SHA256:5d7610449498a79aa62d4b9a8f6baaef91b8716726e1009e02b879962dff32ab
'http://archive.ubuntu.com/ubuntu/pool/main/g/gmp/gmp_6.2.0+dfsg-4.debian.tar.xz' gmp_6.2.0+dfsg-4.debian.tar.xz 21120 SHA256:a0772595583dbcf2147e8457602ccf4b524b18227d6804c4a74050df64ece912
```

### `dpkg` source package: `gnupg2=2.2.19-3ubuntu2.1`

Binary Packages:

- `dirmngr=2.2.19-3ubuntu2.1`
- `gnupg=2.2.19-3ubuntu2.1`
- `gnupg-l10n=2.2.19-3ubuntu2.1`
- `gnupg-utils=2.2.19-3ubuntu2.1`
- `gpg=2.2.19-3ubuntu2.1`
- `gpg-agent=2.2.19-3ubuntu2.1`
- `gpg-wks-client=2.2.19-3ubuntu2.1`
- `gpg-wks-server=2.2.19-3ubuntu2.1`
- `gpgconf=2.2.19-3ubuntu2.1`
- `gpgsm=2.2.19-3ubuntu2.1`
- `gpgv=2.2.19-3ubuntu2.1`

Licenses: (parsed from: `/usr/share/doc/dirmngr/copyright`, `/usr/share/doc/gnupg/copyright`, `/usr/share/doc/gnupg-l10n/copyright`, `/usr/share/doc/gnupg-utils/copyright`, `/usr/share/doc/gpg/copyright`, `/usr/share/doc/gpg-agent/copyright`, `/usr/share/doc/gpg-wks-client/copyright`, `/usr/share/doc/gpg-wks-server/copyright`, `/usr/share/doc/gpgconf/copyright`, `/usr/share/doc/gpgsm/copyright`, `/usr/share/doc/gpgv/copyright`)

- `BSD-3-clause`
- `CC0-1.0`
- `Expat`
- `GPL-3`
- `GPL-3+`
- `LGPL-2.1`
- `LGPL-2.1+`
- `LGPL-3`
- `LGPL-3+`
- `RFC-Reference`
- `TinySCHEME`
- `permissive`

Source:

```console
$ apt-get source -qq --print-uris gnupg2=2.2.19-3ubuntu2.1
'http://archive.ubuntu.com/ubuntu/pool/main/g/gnupg2/gnupg2_2.2.19-3ubuntu2.1.dsc' gnupg2_2.2.19-3ubuntu2.1.dsc 3939 SHA512:d756d6b7a95ec68c9fbe7c0369d36248c72921a2742014cef4d7313a2c18c96df251f87ba164c25c8ac2b0e9dda2ee72d154705660aeb57fce70d175770a0e49
'http://archive.ubuntu.com/ubuntu/pool/main/g/gnupg2/gnupg2_2.2.19.orig.tar.bz2' gnupg2_2.2.19.orig.tar.bz2 6754972 SHA512:d7700136ac9f0a8cf04b33da4023a42427fced648c2f90d76250c92904353b85fe728bdd89a713d847e8d38e5900c98d46075614492fdc3d1421f927a92f49dd
'http://archive.ubuntu.com/ubuntu/pool/main/g/gnupg2/gnupg2_2.2.19.orig.tar.bz2.asc' gnupg2_2.2.19.orig.tar.bz2.asc 906 SHA512:8b02ce09a50d2aa0c263f7042424ea815386fac56a8d8cea102d1aea2e75802f91bb2ebc7dc2d7a3157126d748ece554e0693d3bf355f908586cbadbe80c68fb
'http://archive.ubuntu.com/ubuntu/pool/main/g/gnupg2/gnupg2_2.2.19-3ubuntu2.1.debian.tar.xz' gnupg2_2.2.19-3ubuntu2.1.debian.tar.xz 65572 SHA512:ea863eeb3d823a8ed0aad76bd9e76d9452d2061dd1170357dd73f3f7251b11c2ceed29209cfe42adc0c7a7fc17232df121192cfc976d7a7d48e7b43ef1deb3e3
```

### `dpkg` source package: `gnutls28=3.6.13-2ubuntu1.6`

Binary Packages:

- `libgnutls30:amd64=3.6.13-2ubuntu1.6`

Licenses: (parsed from: `/usr/share/doc/libgnutls30/copyright`)

- `Apache-2.0`
- `BSD-3-Clause`
- `CC0 license`
- `Expat`
- `GFDL-1.3`
- `GPL`
- `GPL-3`
- `GPLv3+`
- `LGPL`
- `LGPL-3`
- `LGPLv2.1+`
- `LGPLv3+_or_GPLv2+`
- `The main library is licensed under GNU Lesser`

Source:

```console
$ apt-get source -qq --print-uris gnutls28=3.6.13-2ubuntu1.6
'http://archive.ubuntu.com/ubuntu/pool/main/g/gnutls28/gnutls28_3.6.13-2ubuntu1.6.dsc' gnutls28_3.6.13-2ubuntu1.6.dsc 3594 SHA512:23014d873a5eac6c1388a5d3907172bf9f5e11cb32cfcea0bb75daef42e925385a9934d875ed47c8fe65e74462abc52c600769f282e17b9bca1de59cfeb40b94
'http://archive.ubuntu.com/ubuntu/pool/main/g/gnutls28/gnutls28_3.6.13.orig.tar.xz' gnutls28_3.6.13.orig.tar.xz 5958956 SHA512:23581952cb72c9a34f378c002bb62413d5a1243b74b48ad8dc49eaea4020d33c550f8dc1dd374cf7fbfa4187b0ca1c5698c8a0430398268a8b8a863f8633305c
'http://archive.ubuntu.com/ubuntu/pool/main/g/gnutls28/gnutls28_3.6.13.orig.tar.xz.asc' gnutls28_3.6.13.orig.tar.xz.asc 667 SHA512:b343a8ace6a5c81c0c44b2cb65d8e83dfe5963c9bab04d9131fa8fd03cdf0c6f990d720af8767084e01bf5f7a7dbd0f048aefe68c3b6f1dc1ea1899d567a72f7
'http://archive.ubuntu.com/ubuntu/pool/main/g/gnutls28/gnutls28_3.6.13-2ubuntu1.6.debian.tar.xz' gnutls28_3.6.13-2ubuntu1.6.debian.tar.xz 67248 SHA512:7f01e2c774dcbbdaf4095184660b099b20600b6eca13c96abfaa27e0e69dd1f91a3320ea758ddc00887c2be62fed32c4249362f834bab98df3258a1aa8891179
```

### `dpkg` source package: `gobject-introspection=1.64.1-1~ubuntu20.04.1`

Binary Packages:

- `gir1.2-glib-2.0:amd64=1.64.1-1~ubuntu20.04.1`
- `libgirepository-1.0-1:amd64=1.64.1-1~ubuntu20.04.1`

Licenses: (parsed from: `/usr/share/doc/gir1.2-glib-2.0/copyright`, `/usr/share/doc/libgirepository-1.0-1/copyright`)

- `BSD-2-clause`
- `GPL-2`
- `GPL-2+`
- `LGPL-2`
- `LGPL-2+`
- `MIT`

Source:

```console
$ apt-get source -qq --print-uris gobject-introspection=1.64.1-1~ubuntu20.04.1
'http://archive.ubuntu.com/ubuntu/pool/main/g/gobject-introspection/gobject-introspection_1.64.1-1~ubuntu20.04.1.dsc' gobject-introspection_1.64.1-1~ubuntu20.04.1.dsc 3183 SHA512:93ad02366de092d2aac580d03947df4533e687bf77860e5faae26155253daf28da32cd8af52bbea69c4ba24380862ae13bd459edeb55f02730c1ebaec1135063
'http://archive.ubuntu.com/ubuntu/pool/main/g/gobject-introspection/gobject-introspection_1.64.1.orig.tar.xz' gobject-introspection_1.64.1.orig.tar.xz 1000280 SHA512:7610871f7ed5778ea9813062ed6465d131af58c00bdea1bb51dde7f98f459f44ae453eb6d0c5bdc6f7dcd92d639816f4e0773ccd5673cd065d22dabc6448647c
'http://archive.ubuntu.com/ubuntu/pool/main/g/gobject-introspection/gobject-introspection_1.64.1-1~ubuntu20.04.1.debian.tar.xz' gobject-introspection_1.64.1-1~ubuntu20.04.1.debian.tar.xz 23412 SHA512:9b0e1f889c14d7e883d536fddb6872290c50bb97863d94dab5e74eeda4adb6b31d2f9b5f17d16e46e1188f1511888ab11e9e7afba76fbcbcb5e465f59ac564ee
```

### `dpkg` source package: `gpgme1.0=1.13.1-7ubuntu2`

Binary Packages:

- `libgpgme11:amd64=1.13.1-7ubuntu2`
- `libgpgmepp6:amd64=1.13.1-7ubuntu2`

Licenses: (parsed from: `/usr/share/doc/libgpgme11/copyright`, `/usr/share/doc/libgpgmepp6/copyright`)

- `GPL-2`
- `GPL-2+`
- `GPL-3`
- `GPL-3+`
- `LGPL-2`
- `LGPL-2+`
- `LGPL-2.1`
- `LGPL-2.1+`
- `LGPL-3`
- `LGPL-3+`

Source:

```console
$ apt-get source -qq --print-uris gpgme1.0=1.13.1-7ubuntu2
'http://archive.ubuntu.com/ubuntu/pool/main/g/gpgme1.0/gpgme1.0_1.13.1-7ubuntu2.dsc' gpgme1.0_1.13.1-7ubuntu2.dsc 2915 SHA256:cd281e669d7c382bb02ad86d432b24784c463fb6c8db15e7fd2099dce579325d
'http://archive.ubuntu.com/ubuntu/pool/main/g/gpgme1.0/gpgme1.0_1.13.1.orig.tar.bz2' gpgme1.0_1.13.1.orig.tar.bz2 1759616 SHA256:c4e30b227682374c23cddc7fdb9324a99694d907e79242a25a4deeedb393be46
'http://archive.ubuntu.com/ubuntu/pool/main/g/gpgme1.0/gpgme1.0_1.13.1.orig.tar.bz2.asc' gpgme1.0_1.13.1.orig.tar.bz2.asc 488 SHA256:2759362727500bc9ddee86c6383b63bee0e230bd6159e63ea3cb48dc1de0f303
'http://archive.ubuntu.com/ubuntu/pool/main/g/gpgme1.0/gpgme1.0_1.13.1-7ubuntu2.debian.tar.xz' gpgme1.0_1.13.1-7ubuntu2.debian.tar.xz 22236 SHA256:211f22649de0ab4099aae46bcadf7d29546c576fda23e9fb01daaa8548050d8f
```

### `dpkg` source package: `gpm=1.20.7-5`

Binary Packages:

- `libgpm2:amd64=1.20.7-5`

Licenses: (parsed from: `/usr/share/doc/libgpm2/copyright`)

- `GPL-2`
- `GPL-2.0+`
- `GPL-3`
- `GPL-3.0+`

Source:

```console
$ apt-get source -qq --print-uris gpm=1.20.7-5
'http://archive.ubuntu.com/ubuntu/pool/main/g/gpm/gpm_1.20.7-5.dsc' gpm_1.20.7-5.dsc 1986 SHA256:d5925ddcecd217ece2790c1c81993c6e32d86914865d90cb9bfabbe1bb6595a8
'http://archive.ubuntu.com/ubuntu/pool/main/g/gpm/gpm_1.20.7.orig.tar.gz' gpm_1.20.7.orig.tar.gz 855027 SHA256:c7e4661c24e05ae13547176b649bac8e3a0db2575f7dd57559f9e0b509f90f49
'http://archive.ubuntu.com/ubuntu/pool/main/g/gpm/gpm_1.20.7-5.debian.tar.xz' gpm_1.20.7-5.debian.tar.xz 82740 SHA256:4adbf1434c4975cffe8ce7b180a1bf7047d79b0e4f0e1a8bf68297170df6fdf0
```

### `dpkg` source package: `graphene=1.10.0-1build2`

Binary Packages:

- `libgraphene-1.0-0:amd64=1.10.0-1build2`

Licenses: (parsed from: `/usr/share/doc/libgraphene-1.0-0/copyright`)

- `Expat`

Source:

```console
$ apt-get source -qq --print-uris graphene=1.10.0-1build2
'http://archive.ubuntu.com/ubuntu/pool/main/g/graphene/graphene_1.10.0-1build2.dsc' graphene_1.10.0-1build2.dsc 1788 SHA256:002d8be9883f122cc7f7ba23f8dc99c112cdc305ef6942ac9b65279e9cb8cfb4
'http://archive.ubuntu.com/ubuntu/pool/main/g/graphene/graphene_1.10.0.orig.tar.xz' graphene_1.10.0.orig.tar.xz 243748 SHA256:da85afb87e0366b0785307010a6823bbc3b5f53fd11b234de56a2dbdb6ac8745
'http://archive.ubuntu.com/ubuntu/pool/main/g/graphene/graphene_1.10.0-1build2.debian.tar.xz' graphene_1.10.0-1build2.debian.tar.xz 6484 SHA256:85023ca5e6c0ae377bacf65d7bf52fae363546bb091174db4f9e5b0bb73dcb90
```

### `dpkg` source package: `graphite2=1.3.13-11build1`

Binary Packages:

- `libgraphite2-3:amd64=1.3.13-11build1`

Licenses: (parsed from: `/usr/share/doc/libgraphite2-3/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`
- `GPL-2`
- `GPL-2+`
- `LGPL-2.1`
- `LGPL-2.1+`
- `MPL-1.1`
- `custom-sil-open-font-license`
- `public-domain`

Source:

```console
$ apt-get source -qq --print-uris graphite2=1.3.13-11build1
'http://archive.ubuntu.com/ubuntu/pool/main/g/graphite2/graphite2_1.3.13-11build1.dsc' graphite2_1.3.13-11build1.dsc 2636 SHA256:c0553cdbffa6ec465063753058007acdf956a1d3fda7336c356b663d4b73bd18
'http://archive.ubuntu.com/ubuntu/pool/main/g/graphite2/graphite2_1.3.13.orig.tar.gz' graphite2_1.3.13.orig.tar.gz 6664941 SHA256:2f9f609deeddfe2b193502adc8df3b0396694b799a433c36e85fd1242e654cd9
'http://archive.ubuntu.com/ubuntu/pool/main/g/graphite2/graphite2_1.3.13-11build1.debian.tar.xz' graphite2_1.3.13-11build1.debian.tar.xz 12132 SHA256:b25e456d2810c2965e968403e2e2fdaf159327f3db5f37c87adae905b40efa49
```

### `dpkg` source package: `grep=3.4-1`

Binary Packages:

- `grep=3.4-1`

Licenses: (parsed from: `/usr/share/doc/grep/copyright`)

- `GPL-3`
- `GPL-3+`

Source:

```console
$ apt-get source -qq --print-uris grep=3.4-1
'http://archive.ubuntu.com/ubuntu/pool/main/g/grep/grep_3.4-1.dsc' grep_3.4-1.dsc 1674 SHA256:785f527cede9631f075bdd6c7f35e65e6b82897d009682766cf35839a393277d
'http://archive.ubuntu.com/ubuntu/pool/main/g/grep/grep_3.4.orig.tar.xz' grep_3.4.orig.tar.xz 1555820 SHA256:58e6751c41a7c25bfc6e9363a41786cff3ba5709cf11d5ad903cf7cce31cc3fb
'http://archive.ubuntu.com/ubuntu/pool/main/g/grep/grep_3.4.orig.tar.xz.asc' grep_3.4.orig.tar.xz.asc 833 SHA256:4c1871ff6b79c5e5ce0a192272c171d06ec20762b4b258688b1ca2e47d94b23e
'http://archive.ubuntu.com/ubuntu/pool/main/g/grep/grep_3.4-1.debian.tar.xz' grep_3.4-1.debian.tar.xz 104364 SHA256:582d181804ce72fcfc4c6a9f13ea1dd73ad04c2723b5da346b69ee5cd24a7d08
```

### `dpkg` source package: `gsettings-desktop-schemas=3.36.0-1ubuntu1`

Binary Packages:

- `gsettings-desktop-schemas=3.36.0-1ubuntu1`

Licenses: (parsed from: `/usr/share/doc/gsettings-desktop-schemas/copyright`)

- `LGPL-2.1`
- `LGPL-2.1+`

Source:

```console
$ apt-get source -qq --print-uris gsettings-desktop-schemas=3.36.0-1ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/g/gsettings-desktop-schemas/gsettings-desktop-schemas_3.36.0-1ubuntu1.dsc' gsettings-desktop-schemas_3.36.0-1ubuntu1.dsc 2460 SHA256:a7a43bbbcbb3cb32997a5a68b1474b5154ee6db06691bce2c35b43bfafb08181
'http://archive.ubuntu.com/ubuntu/pool/main/g/gsettings-desktop-schemas/gsettings-desktop-schemas_3.36.0.orig.tar.xz' gsettings-desktop-schemas_3.36.0.orig.tar.xz 664992 SHA256:764ab683286536324533a58d4e95fc57f81adaba7d880dd0ebbbced63e960ea6
'http://archive.ubuntu.com/ubuntu/pool/main/g/gsettings-desktop-schemas/gsettings-desktop-schemas_3.36.0-1ubuntu1.debian.tar.xz' gsettings-desktop-schemas_3.36.0-1ubuntu1.debian.tar.xz 8952 SHA256:96de13d6d4f5c6a4935be307cabdc0b0c6e895538a1a980166d0b5d2b4341391
```

### `dpkg` source package: `gsfonts=1:8.11+urwcyr1.0.7~pre44-4.4`

Binary Packages:

- `gsfonts=1:8.11+urwcyr1.0.7~pre44-4.4`

Licenses: (parsed from: `/usr/share/doc/gsfonts/copyright`)

- `GPL`

Source:

```console
$ apt-get source -qq --print-uris gsfonts=1:8.11+urwcyr1.0.7~pre44-4.4
'http://archive.ubuntu.com/ubuntu/pool/universe/g/gsfonts/gsfonts_8.11+urwcyr1.0.7~pre44-4.4.dsc' gsfonts_8.11+urwcyr1.0.7~pre44-4.4.dsc 2011 SHA256:c532a13a9ca87a19d5e1470e94bf9fe7b822c0c1745d8f758f993d2ed4b2c329
'http://archive.ubuntu.com/ubuntu/pool/universe/g/gsfonts/gsfonts_8.11+urwcyr1.0.7~pre44.orig.tar.gz' gsfonts_8.11+urwcyr1.0.7~pre44.orig.tar.gz 3390551 SHA256:9f2a598998bf05e023546ac981aa2a857aa1635d2e0e3020a3c3004ad564dc00
'http://archive.ubuntu.com/ubuntu/pool/universe/g/gsfonts/gsfonts_8.11+urwcyr1.0.7~pre44-4.4.diff.gz' gsfonts_8.11+urwcyr1.0.7~pre44-4.4.diff.gz 6940 SHA256:b3343e4a90dbf5c7bb59df4a335f76d7877e2e6814d3f68f9988343f227db626
```

### `dpkg` source package: `gst-plugins-base1.0=1.16.2-4ubuntu0.1`

Binary Packages:

- `gstreamer1.0-gl:amd64=1.16.2-4ubuntu0.1`
- `gstreamer1.0-plugins-base:amd64=1.16.2-4ubuntu0.1`
- `libgstreamer-gl1.0-0:amd64=1.16.2-4ubuntu0.1`
- `libgstreamer-plugins-base1.0-0:amd64=1.16.2-4ubuntu0.1`

Licenses: (parsed from: `/usr/share/doc/gstreamer1.0-gl/copyright`, `/usr/share/doc/gstreamer1.0-plugins-base/copyright`, `/usr/share/doc/libgstreamer-gl1.0-0/copyright`, `/usr/share/doc/libgstreamer-plugins-base1.0-0/copyright`)

- `BSD (2 clause)`
- `BSD (3 clause)`
- `GPL-2+`
- `LGPL`
- `LGPL-2+`
- `MIT/X11 (BSD like) LGPL-2+`

Source:

```console
$ apt-get source -qq --print-uris gst-plugins-base1.0=1.16.2-4ubuntu0.1
'http://archive.ubuntu.com/ubuntu/pool/main/g/gst-plugins-base1.0/gst-plugins-base1.0_1.16.2-4ubuntu0.1.dsc' gst-plugins-base1.0_1.16.2-4ubuntu0.1.dsc 4152 SHA512:abab009285c50c474daf3f59de1e2aab058c34defe26e670e68435e1e3bbc7fda28eff72ca9a8068706f2dbb5810192533a606a90f89b5d48d396291a3ff84fd
'http://archive.ubuntu.com/ubuntu/pool/main/g/gst-plugins-base1.0/gst-plugins-base1.0_1.16.2.orig.tar.xz' gst-plugins-base1.0_1.16.2.orig.tar.xz 3939868 SHA512:f28e71bba8ba25d4f18ba3a196f057721151f1ebf1309d808bd6771a3f9a68facfa1970dc4353b6f2fd1e8945edf5272854d328ea11ef399544f8b330f754a42
'http://archive.ubuntu.com/ubuntu/pool/main/g/gst-plugins-base1.0/gst-plugins-base1.0_1.16.2-4ubuntu0.1.debian.tar.xz' gst-plugins-base1.0_1.16.2-4ubuntu0.1.debian.tar.xz 46460 SHA512:26ac713e4ba54ca6dd203ebf2bf6bd7f70ef1ee460954cacff66b51634264042e4c00169d66a5aa9f8b6a8df4c012ab2c111bb0637c10a85597daf83c6fec559
```

### `dpkg` source package: `gst-plugins-good1.0=1.16.2-1ubuntu2.1`

Binary Packages:

- `gstreamer1.0-gtk3:amd64=1.16.2-1ubuntu2.1`

Licenses: (parsed from: `/usr/share/doc/gstreamer1.0-gtk3/copyright`)

- `BSD`
- `BSD (3 clause)`
- `GPL-2+`
- `LGPL`
- `LGPL-2`
- `LGPL-2+`
- `LGPL-2.1+`
- `MIT/X11 (BSD like) LGPL-2+`

Source:

```console
$ apt-get source -qq --print-uris gst-plugins-good1.0=1.16.2-1ubuntu2.1
'http://archive.ubuntu.com/ubuntu/pool/main/g/gst-plugins-good1.0/gst-plugins-good1.0_1.16.2-1ubuntu2.1.dsc' gst-plugins-good1.0_1.16.2-1ubuntu2.1.dsc 4133 SHA512:2852cdf15beb532cc04e2d5480150210d0e1a4efd440611ee9ca13fc738b22562c97a65850d8921dfa4c4c4ca89a1bd531e04a161e16e786ffaae679ba68d25a
'http://archive.ubuntu.com/ubuntu/pool/main/g/gst-plugins-good1.0/gst-plugins-good1.0_1.16.2.orig.tar.xz' gst-plugins-good1.0_1.16.2.orig.tar.xz 3897172 SHA512:ab0dfd51af3ea345db466618547c35c78b5f08e725096b66074a5a7e0a83ca25ac51d2d915b7a8c07b70f8e4c9fc65d51c1851f147a5a5a71fbca58e1eb5ffd4
'http://archive.ubuntu.com/ubuntu/pool/main/g/gst-plugins-good1.0/gst-plugins-good1.0_1.16.2-1ubuntu2.1.debian.tar.xz' gst-plugins-good1.0_1.16.2-1ubuntu2.1.debian.tar.xz 127764 SHA512:aad2a72836331e0d7638917100b75767d1929971e1a7e5cc0e2a8ca342e668f9d6ffcdd5b2cb0cac98728c57e5169b7edcd21fb3ccf8e2f6d7521f6c69ad09af
```

### `dpkg` source package: `gstreamer1.0=1.16.2-2`

Binary Packages:

- `libgstreamer1.0-0:amd64=1.16.2-2`

Licenses: (parsed from: `/usr/share/doc/libgstreamer1.0-0/copyright`)

- `GPL-2+`
- `GPL-3+`
- `LGPL`
- `LGPL-2+`
- `LGPL-2.1+`

Source:

```console
$ apt-get source -qq --print-uris gstreamer1.0=1.16.2-2
'http://archive.ubuntu.com/ubuntu/pool/main/g/gstreamer1.0/gstreamer1.0_1.16.2-2.dsc' gstreamer1.0_1.16.2-2.dsc 3077 SHA256:fe4f5fdc55e6b6cf62542d98e7821815debd395477f3ab1e4ecb734cf3ec41f7
'http://archive.ubuntu.com/ubuntu/pool/main/g/gstreamer1.0/gstreamer1.0_1.16.2.orig.tar.xz' gstreamer1.0_1.16.2.orig.tar.xz 3328600 SHA256:e3f044246783fd685439647373fa13ba14f7ab0b346eadd06437092f8419e94e
'http://archive.ubuntu.com/ubuntu/pool/main/g/gstreamer1.0/gstreamer1.0_1.16.2-2.debian.tar.xz' gstreamer1.0_1.16.2-2.debian.tar.xz 45244 SHA256:51051d2309ae3461e3ee4fb7590c5a26084f3c1f8bff7381e0efe0b917f8d869
```

### `dpkg` source package: `gtk+3.0=3.24.20-0ubuntu1`

Binary Packages:

- `gtk-update-icon-cache=3.24.20-0ubuntu1`
- `libgtk-3-0:amd64=3.24.20-0ubuntu1`
- `libgtk-3-bin=3.24.20-0ubuntu1`
- `libgtk-3-common=3.24.20-0ubuntu1`

Licenses: (parsed from: `/usr/share/doc/gtk-update-icon-cache/copyright`, `/usr/share/doc/libgtk-3-0/copyright`, `/usr/share/doc/libgtk-3-bin/copyright`, `/usr/share/doc/libgtk-3-common/copyright`)

- `Apache-2.0`
- `Expat`
- `LGPL-2`
- `LGPL-2+`
- `LGPL-2.1`
- `LGPL-2.1+`
- `SWL`
- `X11R5-permissive`
- `check-gdk-cairo-permissive`
- `other`

Source:

```console
$ apt-get source -qq --print-uris gtk+3.0=3.24.20-0ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/g/gtk+3.0/gtk+3.0_3.24.20-0ubuntu1.dsc' gtk+3.0_3.24.20-0ubuntu1.dsc 3639 SHA512:d4e5a99df1cad5bb9cd223cb48e3a346161bb9a528959511b2f508a548b8dd6c5cf508ac70237fef8be602cd0253248202e3cf4cb576d153b8ce511b671e8b85
'http://archive.ubuntu.com/ubuntu/pool/main/g/gtk+3.0/gtk+3.0_3.24.20.orig.tar.xz' gtk+3.0_3.24.20.orig.tar.xz 22726768 SHA512:bda8eeacf721afcff4565756fe45857cbf8b053494d1d747ca9b97c899e9fe21fddef6871ecd5ec9a7a0f87b0a3ede2f9d6b5e5712de10a845100d82464a4556
'http://archive.ubuntu.com/ubuntu/pool/main/g/gtk+3.0/gtk+3.0_3.24.20-0ubuntu1.debian.tar.xz' gtk+3.0_3.24.20-0ubuntu1.debian.tar.xz 170604 SHA512:be1cffcf2adc22035537a521ad988692ea6980dd8f9da6c6deda4d215a79c07fec3af9fed4ef6b88b456727324a5700abf41141eec5072d776ba40c898b1230b
```

### `dpkg` source package: `gzip=1.10-0ubuntu4`

Binary Packages:

- `gzip=1.10-0ubuntu4`

Licenses: (parsed from: `/usr/share/doc/gzip/copyright`)

- `GPL`

Source:

```console
$ apt-get source -qq --print-uris gzip=1.10-0ubuntu4
'http://archive.ubuntu.com/ubuntu/pool/main/g/gzip/gzip_1.10-0ubuntu4.dsc' gzip_1.10-0ubuntu4.dsc 2134 SHA256:b1b05c873448fe2ae1029f55cfea8ae5139d0d88a66ed97768da911e833c9578
'http://archive.ubuntu.com/ubuntu/pool/main/g/gzip/gzip_1.10.orig.tar.gz' gzip_1.10.orig.tar.gz 1201421 SHA256:c91f74430bf7bc20402e1f657d0b252cb80aa66ba333a25704512af346633c68
'http://archive.ubuntu.com/ubuntu/pool/main/g/gzip/gzip_1.10-0ubuntu4.debian.tar.xz' gzip_1.10-0ubuntu4.debian.tar.xz 26592 SHA256:da4c6907f9769be8622349c1a5b81ba5d2ab03c82f1e116c2f3f1a9f00bb8055
```

### `dpkg` source package: `harfbuzz=2.6.4-1ubuntu4`

Binary Packages:

- `libharfbuzz-icu0:amd64=2.6.4-1ubuntu4`
- `libharfbuzz0b:amd64=2.6.4-1ubuntu4`

Licenses: (parsed from: `/usr/share/doc/libharfbuzz-icu0/copyright`, `/usr/share/doc/libharfbuzz0b/copyright`)

- `MIT`

Source:

```console
$ apt-get source -qq --print-uris harfbuzz=2.6.4-1ubuntu4
'http://archive.ubuntu.com/ubuntu/pool/main/h/harfbuzz/harfbuzz_2.6.4-1ubuntu4.dsc' harfbuzz_2.6.4-1ubuntu4.dsc 2841 SHA256:b0e09594316a59a21e0cad422d30cb0f539d09b2607e1eecc4f4e7853b35b4bf
'http://archive.ubuntu.com/ubuntu/pool/main/h/harfbuzz/harfbuzz_2.6.4.orig.tar.xz' harfbuzz_2.6.4.orig.tar.xz 5967468 SHA256:9413b8d96132d699687ef914ebb8c50440efc87b3f775d25856d7ec347c03c12
'http://archive.ubuntu.com/ubuntu/pool/main/h/harfbuzz/harfbuzz_2.6.4-1ubuntu4.debian.tar.xz' harfbuzz_2.6.4-1ubuntu4.debian.tar.xz 11332 SHA256:ee06ba3c394bb6623e5c16ad7e728565c6b12fd13653f64ca7a82866af144aad
```

### `dpkg` source package: `heimdal=7.7.0+dfsg-1ubuntu1`

Binary Packages:

- `libasn1-8-heimdal:amd64=7.7.0+dfsg-1ubuntu1`
- `libgssapi3-heimdal:amd64=7.7.0+dfsg-1ubuntu1`
- `libhcrypto4-heimdal:amd64=7.7.0+dfsg-1ubuntu1`
- `libheimbase1-heimdal:amd64=7.7.0+dfsg-1ubuntu1`
- `libheimntlm0-heimdal:amd64=7.7.0+dfsg-1ubuntu1`
- `libhx509-5-heimdal:amd64=7.7.0+dfsg-1ubuntu1`
- `libkrb5-26-heimdal:amd64=7.7.0+dfsg-1ubuntu1`
- `libroken18-heimdal:amd64=7.7.0+dfsg-1ubuntu1`
- `libwind0-heimdal:amd64=7.7.0+dfsg-1ubuntu1`

Licenses: (parsed from: `/usr/share/doc/libasn1-8-heimdal/copyright`, `/usr/share/doc/libgssapi3-heimdal/copyright`, `/usr/share/doc/libhcrypto4-heimdal/copyright`, `/usr/share/doc/libheimbase1-heimdal/copyright`, `/usr/share/doc/libheimntlm0-heimdal/copyright`, `/usr/share/doc/libhx509-5-heimdal/copyright`, `/usr/share/doc/libkrb5-26-heimdal/copyright`, `/usr/share/doc/libroken18-heimdal/copyright`, `/usr/share/doc/libwind0-heimdal/copyright`)

- `BSD-3-clause`
- `GPL-2`
- `GPL-2+`
- `custom`
- `none`

Source:

```console
$ apt-get source -qq --print-uris heimdal=7.7.0+dfsg-1ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/h/heimdal/heimdal_7.7.0+dfsg-1ubuntu1.dsc' heimdal_7.7.0+dfsg-1ubuntu1.dsc 3633 SHA256:46b7873a11c8279f5efec50f16b5ed4abafa5a957224c3f624229f9d888e1ebe
'http://archive.ubuntu.com/ubuntu/pool/main/h/heimdal/heimdal_7.7.0+dfsg.orig.tar.xz' heimdal_7.7.0+dfsg.orig.tar.xz 5945252 SHA256:6822c9547188b753b6325047fda9255744e4ebbbe02bb0dade78c261061fefac
'http://archive.ubuntu.com/ubuntu/pool/main/h/heimdal/heimdal_7.7.0+dfsg-1ubuntu1.debian.tar.xz' heimdal_7.7.0+dfsg-1ubuntu1.debian.tar.xz 128604 SHA256:0c0b4572de525c3c294bcdbde95ebfc3386461c47c2e8d1a86fe0d37da6bd479
```

### `dpkg` source package: `hicolor-icon-theme=0.17-2`

Binary Packages:

- `hicolor-icon-theme=0.17-2`

Licenses: (parsed from: `/usr/share/doc/hicolor-icon-theme/copyright`)

- `GPL-2`
- `GPL-2+`

Source:

```console
$ apt-get source -qq --print-uris hicolor-icon-theme=0.17-2
'http://archive.ubuntu.com/ubuntu/pool/main/h/hicolor-icon-theme/hicolor-icon-theme_0.17-2.dsc' hicolor-icon-theme_0.17-2.dsc 2053 SHA256:9df02b466f82cd6fa13930bc197d001ed8ddac1abc7f8dde3db45ed1708336bd
'http://archive.ubuntu.com/ubuntu/pool/main/h/hicolor-icon-theme/hicolor-icon-theme_0.17.orig.tar.xz' hicolor-icon-theme_0.17.orig.tar.xz 53016 SHA256:317484352271d18cbbcfac3868eab798d67fff1b8402e740baa6ff41d588a9d8
'http://archive.ubuntu.com/ubuntu/pool/main/h/hicolor-icon-theme/hicolor-icon-theme_0.17-2.debian.tar.xz' hicolor-icon-theme_0.17-2.debian.tar.xz 3536 SHA256:97eec9852a2923b95bd13fc59c30fb1b9063ffd1f8a04748544d4975a84e98f2
```

### `dpkg` source package: `hostname=3.23`

Binary Packages:

- `hostname=3.23`

Licenses: (parsed from: `/usr/share/doc/hostname/copyright`)

- `GPL-2`

Source:

```console
$ apt-get source -qq --print-uris hostname=3.23
'http://archive.ubuntu.com/ubuntu/pool/main/h/hostname/hostname_3.23.dsc' hostname_3.23.dsc 1402 SHA256:0694c083fad82da1fd33204557a30bfc745a689a64030ba360062daafe03ede0
'http://archive.ubuntu.com/ubuntu/pool/main/h/hostname/hostname_3.23.tar.gz' hostname_3.23.tar.gz 13672 SHA256:bc6d1954b22849869ff8b2a602e39f08b1702f686d4b58dd7927cdeb5b4876ef
```

### `dpkg` source package: `hsqldb1.8.0=1.8.0.10+dfsg-10`

Binary Packages:

- `libhsqldb1.8.0-java=1.8.0.10+dfsg-10`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris hsqldb1.8.0=1.8.0.10+dfsg-10
'http://archive.ubuntu.com/ubuntu/pool/universe/h/hsqldb1.8.0/hsqldb1.8.0_1.8.0.10+dfsg-10.dsc' hsqldb1.8.0_1.8.0.10+dfsg-10.dsc 1921 SHA256:040581531f55714968f4099f7830106f9267fd9f72ac636658030adb46537cf6
'http://archive.ubuntu.com/ubuntu/pool/universe/h/hsqldb1.8.0/hsqldb1.8.0_1.8.0.10+dfsg.orig.tar.gz' hsqldb1.8.0_1.8.0.10+dfsg.orig.tar.gz 2917677 SHA256:e555da47b3c1c3f364de2297b2c2b76113fbbd903604d6a0a6f782b060a16f48
'http://archive.ubuntu.com/ubuntu/pool/universe/h/hsqldb1.8.0/hsqldb1.8.0_1.8.0.10+dfsg-10.diff.gz' hsqldb1.8.0_1.8.0.10+dfsg-10.diff.gz 29604 SHA256:148a71dd48b7c3176090bb30e79f24a022b0f4ce18cfce5c0131ff16202ffa6d
```

### `dpkg` source package: `humanity-icon-theme=0.6.15`

Binary Packages:

- `humanity-icon-theme=0.6.15`

Licenses: (parsed from: `/usr/share/doc/humanity-icon-theme/copyright`)

- `GPL-2`
- `GPL-3`
- `LGPL-3`

Source:

```console
$ apt-get source -qq --print-uris humanity-icon-theme=0.6.15
'http://archive.ubuntu.com/ubuntu/pool/main/h/humanity-icon-theme/humanity-icon-theme_0.6.15.dsc' humanity-icon-theme_0.6.15.dsc 1631 SHA256:cc3387acdf0e27443d7d3ec65dd07ad691421f46134493279a5f92d0a7706d1a
'http://archive.ubuntu.com/ubuntu/pool/main/h/humanity-icon-theme/humanity-icon-theme_0.6.15.tar.xz' humanity-icon-theme_0.6.15.tar.xz 1755216 SHA256:9dbcb425c2ee2a58c70da1eda4c2c88e32e7ede4094fb59772726864c8214aa6
```

### `dpkg` source package: `hunspell=1.7.0-2build2`

Binary Packages:

- `libhunspell-1.7-0:amd64=1.7.0-2build2`

Licenses: (parsed from: `/usr/share/doc/libhunspell-1.7-0/copyright`)

- `GPL-2`
- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris hunspell=1.7.0-2build2
'http://archive.ubuntu.com/ubuntu/pool/main/h/hunspell/hunspell_1.7.0-2build2.dsc' hunspell_1.7.0-2build2.dsc 2287 SHA256:8e5c9450c4b45a7e96c4479777fb9d70263d182456dd01ffea797c16b78a0468
'http://archive.ubuntu.com/ubuntu/pool/main/h/hunspell/hunspell_1.7.0.orig.tar.gz' hunspell_1.7.0.orig.tar.gz 482156 SHA256:bb27b86eb910a8285407cf3ca33b62643a02798cf2eef468c0a74f6c3ee6bc8a
'http://archive.ubuntu.com/ubuntu/pool/main/h/hunspell/hunspell_1.7.0-2build2.debian.tar.xz' hunspell_1.7.0-2build2.debian.tar.xz 21696 SHA256:df8fa19414139746b4fb96b344f4ddcc4e7ad928ed402956d1a4b91883a5df7f
```

### `dpkg` source package: `hyphen=2.8.8-7`

Binary Packages:

- `libhyphen0:amd64=2.8.8-7`

Licenses: (parsed from: `/usr/share/doc/libhyphen0/copyright`)

- `GPL-2`
- `GPL-2+`
- `LGPL-2.1`
- `LGPL-2.1+`
- `MPL-1.1+`

Source:

```console
$ apt-get source -qq --print-uris hyphen=2.8.8-7
'http://archive.ubuntu.com/ubuntu/pool/main/h/hyphen/hyphen_2.8.8-7.dsc' hyphen_2.8.8-7.dsc 2086 SHA256:f77f10861124cb0a9ac701cac314d037244d1bc362bac113efdf643573120ffe
'http://archive.ubuntu.com/ubuntu/pool/main/h/hyphen/hyphen_2.8.8.orig.tar.gz' hyphen_2.8.8.orig.tar.gz 638369 SHA256:304636d4eccd81a14b6914d07b84c79ebb815288c76fe027b9ebff6ff24d5705
'http://archive.ubuntu.com/ubuntu/pool/main/h/hyphen/hyphen_2.8.8-7.debian.tar.xz' hyphen_2.8.8-7.debian.tar.xz 12540 SHA256:085a0168906304c9033154923e269ae70b64881dcbe6e52854afd4bd2be60aec
```

### `dpkg` source package: `icu=66.1-2ubuntu2`

Binary Packages:

- `libicu66:amd64=66.1-2ubuntu2`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris icu=66.1-2ubuntu2
'http://archive.ubuntu.com/ubuntu/pool/main/i/icu/icu_66.1-2ubuntu2.dsc' icu_66.1-2ubuntu2.dsc 2351 SHA256:a19b1f586160efd41dc7f9eec7babf5ce9d03b578aa59cd302f11c385e86d893
'http://archive.ubuntu.com/ubuntu/pool/main/i/icu/icu_66.1.orig.tar.gz' icu_66.1.orig.tar.gz 24361305 SHA256:52a3f2209ab95559c1cf0a14f24338001f389615bf00e2585ef3dbc43ecf0a2e
'http://archive.ubuntu.com/ubuntu/pool/main/i/icu/icu_66.1.orig.tar.gz.asc' icu_66.1.orig.tar.gz.asc 833 SHA256:08c81d86fb4ed07ce87434afdfdc39a4114ac494908cd4eebc734ba454a80f06
'http://archive.ubuntu.com/ubuntu/pool/main/i/icu/icu_66.1-2ubuntu2.debian.tar.xz' icu_66.1-2ubuntu2.debian.tar.xz 25500 SHA256:480775da69cad60a4dc1aa20097db1d87f5435406bafa3394394c8a546a514df
```

### `dpkg` source package: `ijs=0.35-15`

Binary Packages:

- `libijs-0.35:amd64=0.35-15`

Licenses: (parsed from: `/usr/share/doc/libijs-0.35/copyright`)

- `Expat`
- `Expat~X`
- `Expat~X with X exception`
- `GAP`
- `GAP~Makefile.in`
- `GAP~configure`
- `GPL-2`
- `GPL-2+`
- `GPL-2+ with Autoconf exception`

Source:

```console
$ apt-get source -qq --print-uris ijs=0.35-15
'http://archive.ubuntu.com/ubuntu/pool/main/i/ijs/ijs_0.35-15.dsc' ijs_0.35-15.dsc 2073 SHA256:24ca5cfd3aa05fb20281c81afd81b006d2fad058683d7ef43d51a860614cd18c
'http://archive.ubuntu.com/ubuntu/pool/main/i/ijs/ijs_0.35.orig.tar.gz' ijs_0.35.orig.tar.gz 344262 SHA256:901fffb73e42dae343a8285a31d9c4e82dc3856d36be30adbdb564bdd27161d6
'http://archive.ubuntu.com/ubuntu/pool/main/i/ijs/ijs_0.35-15.debian.tar.xz' ijs_0.35-15.debian.tar.xz 10340 SHA256:5a9e755fb15f822a002308ae20b28b66926f77a352b6e73a57169cd61a4b1e4d
```

### `dpkg` source package: `ilmbase=2.3.0-6build1`

Binary Packages:

- `libilmbase24:amd64=2.3.0-6build1`

Licenses: (parsed from: `/usr/share/doc/libilmbase24/copyright`)

- `boost`
- `ilmbase`

Source:

```console
$ apt-get source -qq --print-uris ilmbase=2.3.0-6build1
'http://archive.ubuntu.com/ubuntu/pool/universe/i/ilmbase/ilmbase_2.3.0-6build1.dsc' ilmbase_2.3.0-6build1.dsc 2367 SHA256:2164e2cff01f17f030b456c2d0920d09461ae76229507459a73cf7c618d4f7f6
'http://archive.ubuntu.com/ubuntu/pool/universe/i/ilmbase/ilmbase_2.3.0.orig.tar.gz' ilmbase_2.3.0.orig.tar.gz 596749 SHA256:0ea21166799bbdd920e7a38a7026236566aafdd6e8638f54c9da1af2219fae82
'http://archive.ubuntu.com/ubuntu/pool/universe/i/ilmbase/ilmbase_2.3.0.orig.tar.gz.asc' ilmbase_2.3.0.orig.tar.gz.asc 566 SHA256:c7ee3f4432322d4f7c63dd1b0ca2188a8d1c4a018821c3c12a3d9db746b54bee
'http://archive.ubuntu.com/ubuntu/pool/universe/i/ilmbase/ilmbase_2.3.0-6build1.debian.tar.xz' ilmbase_2.3.0-6build1.debian.tar.xz 14252 SHA256:1039258f50fda2e94e4dac23e1e8aa702d8e3e83d0e1f12f13a652eadf2ebbf3
```

### `dpkg` source package: `imagemagick=8:6.9.10.23+dfsg-2.1ubuntu11.4`

Binary Packages:

- `imagemagick=8:6.9.10.23+dfsg-2.1ubuntu11.4`
- `imagemagick-6-common=8:6.9.10.23+dfsg-2.1ubuntu11.4`
- `imagemagick-6.q16=8:6.9.10.23+dfsg-2.1ubuntu11.4`
- `libmagickcore-6.q16-6:amd64=8:6.9.10.23+dfsg-2.1ubuntu11.4`
- `libmagickcore-6.q16-6-extra:amd64=8:6.9.10.23+dfsg-2.1ubuntu11.4`
- `libmagickwand-6.q16-6:amd64=8:6.9.10.23+dfsg-2.1ubuntu11.4`

Licenses: (parsed from: `/usr/share/doc/imagemagick/copyright`, `/usr/share/doc/imagemagick-6-common/copyright`, `/usr/share/doc/imagemagick-6.q16/copyright`, `/usr/share/doc/libmagickcore-6.q16-6/copyright`, `/usr/share/doc/libmagickcore-6.q16-6-extra/copyright`, `/usr/share/doc/libmagickwand-6.q16-6/copyright`)

- `Artistic`
- `BSD-with-FSF-change-public-domain`
- `GNU-All-Permissive-License`
- `GPL-1`
- `GPL-2`
- `GPL-2+`
- `GPL-3`
- `GPL2+-with-Autoconf-Macros-exception`
- `GPL3+-with-Autoconf-Macros-exception`
- `GPL3+-with-Autoconf-Macros-exception-GNU`
- `ImageMagick`
- `ImageMagickLicensePartEZXML`
- `ImageMagickLicensePartFIG`
- `ImageMagickLicensePartGsview`
- `ImageMagickLicensePartOpenSSH`
- `ImageMagickPartGraphicsMagick`
- `ImageMagickPartlibjpeg`
- `ImageMagickPartlibsquish`
- `Imagemagick`
- `LGPL-3`
- `LGPL-3+`
- `Magick++`
- `Makefile-in`
- `Perllikelicence`
- `TatcherUlrichPublicDomain`
- `aclocal`

Source:

```console
$ apt-get source -qq --print-uris imagemagick=8:6.9.10.23+dfsg-2.1ubuntu11.4
'http://archive.ubuntu.com/ubuntu/pool/universe/i/imagemagick/imagemagick_6.9.10.23+dfsg-2.1ubuntu11.4.dsc' imagemagick_6.9.10.23+dfsg-2.1ubuntu11.4.dsc 5218 SHA512:c53aca9b43acfb2c7fed2ab45888e7a8f450894847f7300d6d1ac219cce9cb74bd2e12811f975646074046ed64a046158ae9b1e6a87037dfb7c2a1e12001d663
'http://archive.ubuntu.com/ubuntu/pool/universe/i/imagemagick/imagemagick_6.9.10.23+dfsg.orig.tar.xz' imagemagick_6.9.10.23+dfsg.orig.tar.xz 9081188 SHA512:0c5fdfb82d12fef09d79c3a3d03167a57732e566980ca89e977d04502779506eae34e6bc4260c35558db63c572c64fe80d69f6627e180ffc8e8679ebe8574972
'http://archive.ubuntu.com/ubuntu/pool/universe/i/imagemagick/imagemagick_6.9.10.23+dfsg-2.1ubuntu11.4.debian.tar.xz' imagemagick_6.9.10.23+dfsg-2.1ubuntu11.4.debian.tar.xz 250880 SHA512:14e9ec65dc3c639066e8f593d3dc8eb20ee7c1430d2f8af769685c1682f012fa9355c976722a029d55d56403b8ed5ebeb9af332c0b5ceaad14dcb249a27f8ad5
```

### `dpkg` source package: `init-system-helpers=1.57`

Binary Packages:

- `init-system-helpers=1.57`

Licenses: (parsed from: `/usr/share/doc/init-system-helpers/copyright`)

- `BSD-3-clause`
- `GPL-2`
- `GPL-2+`

Source:

```console
$ apt-get source -qq --print-uris init-system-helpers=1.57
'http://archive.ubuntu.com/ubuntu/pool/main/i/init-system-helpers/init-system-helpers_1.57.dsc' init-system-helpers_1.57.dsc 1896 SHA256:88bb5af040c99f010b6d6947ff5c80ae4863ff787e0eeae91e99dcd15a10dbb8
'http://archive.ubuntu.com/ubuntu/pool/main/i/init-system-helpers/init-system-helpers_1.57.tar.xz' init-system-helpers_1.57.tar.xz 40460 SHA256:e9d83fd8756a42666fb5d19a8835813823295846659b4e58f138bb9b54e9f5dd
```

### `dpkg` source package: `intel-gmmlib=20.1.1+ds1-1`

Binary Packages:

- `libigdgmm11:amd64=20.1.1+ds1-1`

Licenses: (parsed from: `/usr/share/doc/libigdgmm11/copyright`)

- `BSD-3-clause`
- `Expat`

Source:

```console
$ apt-get source -qq --print-uris intel-gmmlib=20.1.1+ds1-1
'http://archive.ubuntu.com/ubuntu/pool/universe/i/intel-gmmlib/intel-gmmlib_20.1.1+ds1-1.dsc' intel-gmmlib_20.1.1+ds1-1.dsc 2153 SHA256:dc57cded19c175f37d156440f558fe770d88bddbe89c8add5414a06a2d77698e
'http://archive.ubuntu.com/ubuntu/pool/universe/i/intel-gmmlib/intel-gmmlib_20.1.1+ds1.orig.tar.xz' intel-gmmlib_20.1.1+ds1.orig.tar.xz 400648 SHA256:f794d7be92395e018267208d1b95944269dee6348b2e6a8c54373429c42c768b
'http://archive.ubuntu.com/ubuntu/pool/universe/i/intel-gmmlib/intel-gmmlib_20.1.1+ds1-1.debian.tar.xz' intel-gmmlib_20.1.1+ds1-1.debian.tar.xz 3988 SHA256:e018d6a5d43b1415900064cac740032b7e56769c6ab5f3224d761ecfcc111c3a
```

### `dpkg` source package: `intel-media-driver=20.1.1+dfsg1-1`

Binary Packages:

- `intel-media-va-driver:amd64=20.1.1+dfsg1-1`

Licenses: (parsed from: `/usr/share/doc/intel-media-va-driver/copyright`)

- `BSD-3-clause`
- `Expat`

Source:

```console
$ apt-get source -qq --print-uris intel-media-driver=20.1.1+dfsg1-1
'http://archive.ubuntu.com/ubuntu/pool/universe/i/intel-media-driver/intel-media-driver_20.1.1+dfsg1-1.dsc' intel-media-driver_20.1.1+dfsg1-1.dsc 2377 SHA256:6614cc4c3208d17376ddab528abd0f6fdc7d43554ac25cb889cc247bf6d016a7
'http://archive.ubuntu.com/ubuntu/pool/universe/i/intel-media-driver/intel-media-driver_20.1.1+dfsg1.orig.tar.xz' intel-media-driver_20.1.1+dfsg1.orig.tar.xz 6060444 SHA256:859d5429fe18e198ecc9f296b116491909f40b6d2e684fbc8109e3851e4d2414
'http://archive.ubuntu.com/ubuntu/pool/universe/i/intel-media-driver/intel-media-driver_20.1.1+dfsg1-1.debian.tar.xz' intel-media-driver_20.1.1+dfsg1-1.debian.tar.xz 5560 SHA256:40b2d1c8bcf3810f1504a252247f08211f7bcd781121639371bf0bd9383fab76
```

### `dpkg` source package: `intel-vaapi-driver=2.4.0-0ubuntu1`

Binary Packages:

- `i965-va-driver:amd64=2.4.0-0ubuntu1`

Licenses: (parsed from: `/usr/share/doc/i965-va-driver/copyright`)

- `Apache-2.0`
- `EPL-1.0`
- `Expat`
- `GPL-2`
- `GPL-2+`

Source:

```console
$ apt-get source -qq --print-uris intel-vaapi-driver=2.4.0-0ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/universe/i/intel-vaapi-driver/intel-vaapi-driver_2.4.0-0ubuntu1.dsc' intel-vaapi-driver_2.4.0-0ubuntu1.dsc 2363 SHA256:9bcc4b4b0213e1c22b8752cd06cf30ee6927b9c6b75b5c89b0f18a8b15e67a7d
'http://archive.ubuntu.com/ubuntu/pool/universe/i/intel-vaapi-driver/intel-vaapi-driver_2.4.0.orig.tar.gz' intel-vaapi-driver_2.4.0.orig.tar.gz 3962269 SHA256:58567dac882167021f031489062cbbab76bc646214be0ee44d5f724d960b3d76
'http://archive.ubuntu.com/ubuntu/pool/universe/i/intel-vaapi-driver/intel-vaapi-driver_2.4.0-0ubuntu1.debian.tar.xz' intel-vaapi-driver_2.4.0-0ubuntu1.debian.tar.xz 13220 SHA256:8f3501542b23caaa9214a9089e0df242fd3d4233f2eb36c51269bcc0fb881a8f
```

### `dpkg` source package: `io-stringy=2.111-3`

Binary Packages:

- `libio-stringy-perl=2.111-3`

Licenses: (parsed from: `/usr/share/doc/libio-stringy-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris io-stringy=2.111-3
'http://archive.ubuntu.com/ubuntu/pool/main/i/io-stringy/io-stringy_2.111-3.dsc' io-stringy_2.111-3.dsc 2166 SHA256:b21b87e84a62a9c6fe2b5f421d5937b0af35dca29844c435f8104586c0d0f8c6
'http://archive.ubuntu.com/ubuntu/pool/main/i/io-stringy/io-stringy_2.111.orig.tar.gz' io-stringy_2.111.orig.tar.gz 41515 SHA256:8c67fd6608c3c4e74f7324f1404a856c331dbf48d9deda6aaa8296ea41bf199d
'http://archive.ubuntu.com/ubuntu/pool/main/i/io-stringy/io-stringy_2.111-3.debian.tar.xz' io-stringy_2.111-3.debian.tar.xz 4328 SHA256:e093b904edd3832986d87d0e0f7995b9962aae2b78cdf6da65c4408b0ed38413
```

### `dpkg` source package: `iptables=1.8.4-3ubuntu2`

Binary Packages:

- `libip4tc2:amd64=1.8.4-3ubuntu2`

Licenses: (parsed from: `/usr/share/doc/libip4tc2/copyright`)

- `Artistic`
- `GPL-2`
- `GPL-2+`
- `custom`

Source:

```console
$ apt-get source -qq --print-uris iptables=1.8.4-3ubuntu2
'http://archive.ubuntu.com/ubuntu/pool/main/i/iptables/iptables_1.8.4-3ubuntu2.dsc' iptables_1.8.4-3ubuntu2.dsc 2845 SHA256:be00f2231cd54559bf302ebed37c383123e9610b3b992836fcc69a616d64f77a
'http://archive.ubuntu.com/ubuntu/pool/main/i/iptables/iptables_1.8.4.orig.tar.bz2' iptables_1.8.4.orig.tar.bz2 704312 SHA256:993a3a5490a544c2cbf2ef15cf7e7ed21af1845baf228318d5c36ef8827e157c
'http://archive.ubuntu.com/ubuntu/pool/main/i/iptables/iptables_1.8.4-3ubuntu2.debian.tar.xz' iptables_1.8.4-3ubuntu2.debian.tar.xz 125352 SHA256:bbf74ffc3f931ad9003b6b44360151096dbab4ed49afa8d14d8c247377b59723
```

### `dpkg` source package: `iputils=3:20190709-3`

Binary Packages:

- `iputils-ping=3:20190709-3`

Licenses: (parsed from: `/usr/share/doc/iputils-ping/copyright`)

- `GPL`

Source:

```console
$ apt-get source -qq --print-uris iputils=3:20190709-3
'http://archive.ubuntu.com/ubuntu/pool/main/i/iputils/iputils_20190709-3.dsc' iputils_20190709-3.dsc 2081 SHA256:e169a409efd0238b6c547ffad7de017d402a8506ac9162559de523e884ed0efc
'http://archive.ubuntu.com/ubuntu/pool/main/i/iputils/iputils_20190709.orig.tar.xz' iputils_20190709.orig.tar.xz 361144 SHA256:bec0321ee1489c8f73e88f7d34b6fd40fbec7b3af5b3a1940306bd8d8835c3c0
'http://archive.ubuntu.com/ubuntu/pool/main/i/iputils/iputils_20190709-3.debian.tar.xz' iputils_20190709-3.debian.tar.xz 13816 SHA256:34c3ec0b516db540a748f0934ba2ada6b8c99379941016b26a6fb065be70fb13
```

### `dpkg` source package: `isl=0.22.1-1`

Binary Packages:

- `libisl22:amd64=0.22.1-1`

Licenses: (parsed from: `/usr/share/doc/libisl22/copyright`)

- `BSD-2-clause`
- `LGPL-2`
- `LGPL-2.1+`
- `MIT`

Source:

```console
$ apt-get source -qq --print-uris isl=0.22.1-1
'http://archive.ubuntu.com/ubuntu/pool/main/i/isl/isl_0.22.1-1.dsc' isl_0.22.1-1.dsc 1860 SHA256:9e9925317ef448cf679040edb6572a2874d497f758b613d9fc633bdafab197cb
'http://archive.ubuntu.com/ubuntu/pool/main/i/isl/isl_0.22.1.orig.tar.xz' isl_0.22.1.orig.tar.xz 1676948 SHA256:28658ce0f0bdb95b51fd2eb15df24211c53284f6ca2ac5e897acc3169e55b60f
'http://archive.ubuntu.com/ubuntu/pool/main/i/isl/isl_0.22.1-1.debian.tar.xz' isl_0.22.1-1.debian.tar.xz 25252 SHA256:bbeb62cfc95e51c25448e127c29fa8ac8009a6f471861de28f326bab2404a406
```

### `dpkg` source package: `iso-codes=4.4-1`

Binary Packages:

- `iso-codes=4.4-1`

Licenses: (parsed from: `/usr/share/doc/iso-codes/copyright`)

- `LGPL-2.1`
- `LGPL-2.1+`

Source:

```console
$ apt-get source -qq --print-uris iso-codes=4.4-1
'http://archive.ubuntu.com/ubuntu/pool/main/i/iso-codes/iso-codes_4.4-1.dsc' iso-codes_4.4-1.dsc 1936 SHA256:c5a23261ccd11f56cf398377e1f6590df7ea84586dc5ca966cd2a2421880814b
'http://archive.ubuntu.com/ubuntu/pool/main/i/iso-codes/iso-codes_4.4.orig.tar.xz' iso-codes_4.4.orig.tar.xz 3627156 SHA256:5124ba64e5ce6e1a73c24d1a1cdc42f6a2d0db038791b28ac77aafeb07654e86
'http://archive.ubuntu.com/ubuntu/pool/main/i/iso-codes/iso-codes_4.4-1.debian.tar.xz' iso-codes_4.4-1.debian.tar.xz 23912 SHA256:da078ff222fd3e3d09f00defa33c7941587dd756ad0846d3afef198300769566
```

### `dpkg` source package: `jackd2=1.9.12~dfsg-2ubuntu2`

Binary Packages:

- `libjack-jackd2-0:amd64=1.9.12~dfsg-2ubuntu2`

Licenses: (parsed from: `/usr/share/doc/libjack-jackd2-0/copyright`)

- `BSD-3-clause`
- `Expat`
- `Expat~modrequest`
- `GPL-2`
- `GPL-2+`
- `GPL-2~either`
- `GPL-2~jack-audio-connection-kit`
- `GPL-2~jackd2`
- `GPL-2~or`
- `GPL-3`
- `GPL-3+`
- `LGPL-2`
- `LGPL-2+`
- `LGPL-2.1`
- `LGPL-2.1+`
- `None`
- `public-domain~Kroon`

Source:

```console
$ apt-get source -qq --print-uris jackd2=1.9.12~dfsg-2ubuntu2
'http://archive.ubuntu.com/ubuntu/pool/main/j/jackd2/jackd2_1.9.12~dfsg-2ubuntu2.dsc' jackd2_1.9.12~dfsg-2ubuntu2.dsc 1899 SHA256:be67da27ea2f189af515b489aca05c8a593c4818c0c97860949288ea929cb24f
'http://archive.ubuntu.com/ubuntu/pool/main/j/jackd2/jackd2_1.9.12~dfsg.orig.tar.gz' jackd2_1.9.12~dfsg.orig.tar.gz 1147874 SHA256:059741090d548d1888d34c90647e3ac1650bbee84990dceffcb5144b8f8cd539
'http://archive.ubuntu.com/ubuntu/pool/main/j/jackd2/jackd2_1.9.12~dfsg-2ubuntu2.debian.tar.xz' jackd2_1.9.12~dfsg-2ubuntu2.debian.tar.xz 45228 SHA256:d1f1677e94c34d342463cbe1d04e16d32f44915e773cf45ca4037482302100a7
```

### `dpkg` source package: `java-atk-wrapper=0.37.1-1`

Binary Packages:

- `libatk-wrapper-java=0.37.1-1`
- `libatk-wrapper-java-jni:amd64=0.37.1-1`

Licenses: (parsed from: `/usr/share/doc/libatk-wrapper-java/copyright`, `/usr/share/doc/libatk-wrapper-java-jni/copyright`)

- `GPL-3`
- `LGPL-2.1`
- `LGPL-2.1+`

Source:

```console
$ apt-get source -qq --print-uris java-atk-wrapper=0.37.1-1
'http://archive.ubuntu.com/ubuntu/pool/main/j/java-atk-wrapper/java-atk-wrapper_0.37.1-1.dsc' java-atk-wrapper_0.37.1-1.dsc 2370 SHA256:89876805ad0ee9b1e8d00bf5c6c94af831f9e690e233c9a9301a52abc0be179f
'http://archive.ubuntu.com/ubuntu/pool/main/j/java-atk-wrapper/java-atk-wrapper_0.37.1.orig.tar.xz' java-atk-wrapper_0.37.1.orig.tar.xz 310920 SHA256:5462c1e2b04f87290cb391f2d56c4a8c587a754587338ddbe9706afeed97c807
'http://archive.ubuntu.com/ubuntu/pool/main/j/java-atk-wrapper/java-atk-wrapper_0.37.1-1.debian.tar.xz' java-atk-wrapper_0.37.1-1.debian.tar.xz 5356 SHA256:bfd928e3a6badc4be8697e3b24d533ee4ecdc4ad773038ba09e4dc43cab81da5
```

### `dpkg` source package: `java-common=0.72`

Binary Packages:

- `java-common=0.72`

Licenses: (parsed from: `/usr/share/doc/java-common/copyright`)

- `GPL-2`
- `GPL-2+`

Source:

```console
$ apt-get source -qq --print-uris java-common=0.72
'http://archive.ubuntu.com/ubuntu/pool/main/j/java-common/java-common_0.72.dsc' java-common_0.72.dsc 1985 SHA256:ee412fc8f12e61de5ed18f740277fcc45a7ef7ba83d783d57514775122fe3cd7
'http://archive.ubuntu.com/ubuntu/pool/main/j/java-common/java-common_0.72.tar.xz' java-common_0.72.tar.xz 13180 SHA256:ef8687995572fc889d16de4c0bb29436b4c117ae1b2c9768437e790ed4d4f28f
```

### `dpkg` source package: `jbig2dec=0.18-1ubuntu1`

Binary Packages:

- `libjbig2dec0:amd64=0.18-1ubuntu1`

Licenses: (parsed from: `/usr/share/doc/libjbig2dec0/copyright`)

- `AGPL-3+`
- `BSD-2-clause`
- `GPL-3`
- `GPL-3+`
- `LGPL-2.1`
- `LGPL-2.1+`
- `pubic-domain`
- `public-domain`

Source:

```console
$ apt-get source -qq --print-uris jbig2dec=0.18-1ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/j/jbig2dec/jbig2dec_0.18-1ubuntu1.dsc' jbig2dec_0.18-1ubuntu1.dsc 2128 SHA256:9c5188df694edf5b9523e67cfafca4f6130cb9210184cc170fbd0fc28dd771fe
'http://archive.ubuntu.com/ubuntu/pool/main/j/jbig2dec/jbig2dec_0.18.orig.tar.gz' jbig2dec_0.18.orig.tar.gz 148563 SHA256:3be6eb8105f6dcc8a7d29d6f2b4dc9205c164a412ad940294cedf0d7cc530487
'http://archive.ubuntu.com/ubuntu/pool/main/j/jbig2dec/jbig2dec_0.18-1ubuntu1.debian.tar.xz' jbig2dec_0.18-1ubuntu1.debian.tar.xz 20812 SHA256:9aecb1caaf256a9d1194db01dd1f10006c83420150f204855bf23ec4a80a4836
```

### `dpkg` source package: `jbigkit=2.1-3.1build1`

Binary Packages:

- `libjbig0:amd64=2.1-3.1build1`

Licenses: (parsed from: `/usr/share/doc/libjbig0/copyright`)

- `GPL-2`
- `GPL-2+`

Source:

```console
$ apt-get source -qq --print-uris jbigkit=2.1-3.1build1
'http://archive.ubuntu.com/ubuntu/pool/main/j/jbigkit/jbigkit_2.1-3.1build1.dsc' jbigkit_2.1-3.1build1.dsc 2085 SHA256:fc768c7dac53f37f89c8d0a25760a29cd9afffc5cf55821f92d0d7e8f8f26e38
'http://archive.ubuntu.com/ubuntu/pool/main/j/jbigkit/jbigkit_2.1.orig.tar.gz' jbigkit_2.1.orig.tar.gz 438710 SHA256:de7106b6bfaf495d6865c7dd7ac6ca1381bd12e0d81405ea81e7f2167263d932
'http://archive.ubuntu.com/ubuntu/pool/main/j/jbigkit/jbigkit_2.1-3.1build1.debian.tar.xz' jbigkit_2.1-3.1build1.debian.tar.xz 7672 SHA256:d7151df94f409045aa4d27dab88e538398196330d1ce135b60564dbc5db0a5c4
```

### `dpkg` source package: `json-c=0.13.1+dfsg-7ubuntu0.3`

Binary Packages:

- `libjson-c4:amd64=0.13.1+dfsg-7ubuntu0.3`

Licenses: (parsed from: `/usr/share/doc/libjson-c4/copyright`)

- `Expat`

Source:

```console
$ apt-get source -qq --print-uris json-c=0.13.1+dfsg-7ubuntu0.3
'http://archive.ubuntu.com/ubuntu/pool/main/j/json-c/json-c_0.13.1+dfsg-7ubuntu0.3.dsc' json-c_0.13.1+dfsg-7ubuntu0.3.dsc 2174 SHA512:7c10b379d1783a8d1b529cec0a6d59ec3ae12f50867afeff468afe9de5b7c0053e8e9c8e19f18f74bcc5eba9cb201e674de31ef438bb05c36ccff0b20c1afb3e
'http://archive.ubuntu.com/ubuntu/pool/main/j/json-c/json-c_0.13.1+dfsg.orig.tar.gz' json-c_0.13.1+dfsg.orig.tar.gz 564147 SHA512:4b30602a71181319aa1156e39706ca1693484ee4efc9e20c630381a1219882ea62b458fbd473c1d8bfc96d950b8b91787f522886b1f28dcde4bc4d0a876413cf
'http://archive.ubuntu.com/ubuntu/pool/main/j/json-c/json-c_0.13.1+dfsg-7ubuntu0.3.debian.tar.xz' json-c_0.13.1+dfsg-7ubuntu0.3.debian.tar.xz 10036 SHA512:cc6e4a5567d697ed684ce0ced78819ffb9dd2d899673e977dcc640e1e19f577e1a6860b712c78071368fa3cf8e0fb601214a65926d5ac449df47afb1b7a2b500
```

### `dpkg` source package: `json-glib=1.4.4-2ubuntu2`

Binary Packages:

- `libjson-glib-1.0-0:amd64=1.4.4-2ubuntu2`
- `libjson-glib-1.0-common=1.4.4-2ubuntu2`

Licenses: (parsed from: `/usr/share/doc/libjson-glib-1.0-0/copyright`, `/usr/share/doc/libjson-glib-1.0-common/copyright`)

- `LGPL-2.1`
- `LGPL-2.1+`

Source:

```console
$ apt-get source -qq --print-uris json-glib=1.4.4-2ubuntu2
'http://archive.ubuntu.com/ubuntu/pool/main/j/json-glib/json-glib_1.4.4-2ubuntu2.dsc' json-glib_1.4.4-2ubuntu2.dsc 2170 SHA256:32dd5c725a34ed66e4f92bfc6a01b825c1f64bb80783cd3704c4d5811177680c
'http://archive.ubuntu.com/ubuntu/pool/main/j/json-glib/json-glib_1.4.4.orig.tar.xz' json-glib_1.4.4.orig.tar.xz 150440 SHA256:d37052132c7fd2f12bda8f2a4d6829b6de36378772195920cccfdda2e0ef5ad7
'http://archive.ubuntu.com/ubuntu/pool/main/j/json-glib/json-glib_1.4.4-2ubuntu2.debian.tar.xz' json-glib_1.4.4-2ubuntu2.debian.tar.xz 8256 SHA256:4b1cb46ffd17ac84d670e66e1865e6e33ded45a9ec31b3198cc72949b639c1a4
```

### `dpkg` source package: `jsp-api=2.3.4-2`

Binary Packages:

- `libjsp-api-java=2.3.4-2`

Licenses: (parsed from: `/usr/share/doc/libjsp-api-java/copyright`)

- `Apache-2.0`
- `CDDL-1.1`
- `GPL-2`
- `GPL-2 with Classpath exception`

Source:

```console
$ apt-get source -qq --print-uris jsp-api=2.3.4-2
'http://archive.ubuntu.com/ubuntu/pool/universe/j/jsp-api/jsp-api_2.3.4-2.dsc' jsp-api_2.3.4-2.dsc 2065 SHA256:12fe7e32d736c8e45c00333d1455161ef5ae1d025a82e909bdfa89fce6ec8081
'http://archive.ubuntu.com/ubuntu/pool/universe/j/jsp-api/jsp-api_2.3.4.orig.tar.xz' jsp-api_2.3.4.orig.tar.xz 85592 SHA256:2fab7216496da3e0d87937786ab67af8168bd73bac1ad52b074881dea717509d
'http://archive.ubuntu.com/ubuntu/pool/universe/j/jsp-api/jsp-api_2.3.4-2.debian.tar.xz' jsp-api_2.3.4-2.debian.tar.xz 8544 SHA256:32acce00a4005115e05af125d6a70a90ebbad9fc18491e4c8ec8e2b71029eeb0
```

### `dpkg` source package: `keyutils=1.6-6ubuntu1`

Binary Packages:

- `libkeyutils1:amd64=1.6-6ubuntu1`

Licenses: (parsed from: `/usr/share/doc/libkeyutils1/copyright`)

- `GPL-2`
- `GPL-2+`
- `LGPL-2`
- `LGPL-2+`

Source:

```console
$ apt-get source -qq --print-uris keyutils=1.6-6ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/k/keyutils/keyutils_1.6-6ubuntu1.dsc' keyutils_1.6-6ubuntu1.dsc 2148 SHA256:76dfe0a0d9bb0a417d9c20c2f20b0beb9097dccd30c30a41375ef99cf0a710b6
'http://archive.ubuntu.com/ubuntu/pool/main/k/keyutils/keyutils_1.6.orig.tar.bz2' keyutils_1.6.orig.tar.bz2 93973 SHA256:d3aef20cec0005c0fa6b4be40079885567473185b1a57b629b030e67942c7115
'http://archive.ubuntu.com/ubuntu/pool/main/k/keyutils/keyutils_1.6-6ubuntu1.debian.tar.xz' keyutils_1.6-6ubuntu1.debian.tar.xz 13636 SHA256:a4ae24062e9d20a0e2092e4cb342b664c0211ba7efdfeb7bde5f8d209c9ad1db
```

### `dpkg` source package: `kmod=27-1ubuntu2`

Binary Packages:

- `libkmod2:amd64=27-1ubuntu2`

Licenses: (parsed from: `/usr/share/doc/libkmod2/copyright`)

- `GPL-2`
- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris kmod=27-1ubuntu2
'http://archive.ubuntu.com/ubuntu/pool/main/k/kmod/kmod_27-1ubuntu2.dsc' kmod_27-1ubuntu2.dsc 2094 SHA256:71dc1d23c56654cdb9c805d8f958fa361c15bd4cf9de98798327a91988f01f29
'http://archive.ubuntu.com/ubuntu/pool/main/k/kmod/kmod_27.orig.tar.xz' kmod_27.orig.tar.xz 249572 SHA256:ed96192a8b079b2314cec25566733bd4be97d994049792d6a6a8d72a5bcbec1d
'http://archive.ubuntu.com/ubuntu/pool/main/k/kmod/kmod_27-1ubuntu2.debian.tar.xz' kmod_27-1ubuntu2.debian.tar.xz 12844 SHA256:6db8765843e35c3b0a8ebdf307014b272548d864acb84b54a0a48a610a72b6f9
```

### `dpkg` source package: `krb5=1.17-6ubuntu4.1`

Binary Packages:

- `krb5-locales=1.17-6ubuntu4.1`
- `libgssapi-krb5-2:amd64=1.17-6ubuntu4.1`
- `libk5crypto3:amd64=1.17-6ubuntu4.1`
- `libkrb5-3:amd64=1.17-6ubuntu4.1`
- `libkrb5support0:amd64=1.17-6ubuntu4.1`

Licenses: (parsed from: `/usr/share/doc/krb5-locales/copyright`, `/usr/share/doc/libgssapi-krb5-2/copyright`, `/usr/share/doc/libk5crypto3/copyright`, `/usr/share/doc/libkrb5-3/copyright`, `/usr/share/doc/libkrb5support0/copyright`)

- `GPL-2`

Source:

```console
$ apt-get source -qq --print-uris krb5=1.17-6ubuntu4.1
'http://archive.ubuntu.com/ubuntu/pool/main/k/krb5/krb5_1.17-6ubuntu4.1.dsc' krb5_1.17-6ubuntu4.1.dsc 3637 SHA512:00e77a202038d971b0cc49d77ea6f1c9eef4052016e839235f0b4b60e1dffd476d18bab1330ea5955e6e319731e5679873cf70d144e4cdabd3659aafd209fd37
'http://archive.ubuntu.com/ubuntu/pool/main/k/krb5/krb5_1.17.orig.tar.gz' krb5_1.17.orig.tar.gz 8761763 SHA512:7462a578b936bd17f155a362dbb5d388e157a80a096549028be6c55400b11361c7f8a28e424fd5674801873651df4e694d536cae66728b7ae5e840e532358c52
'http://archive.ubuntu.com/ubuntu/pool/main/k/krb5/krb5_1.17-6ubuntu4.1.debian.tar.xz' krb5_1.17-6ubuntu4.1.debian.tar.xz 145692 SHA512:8184287813d5dbcbe4c5153ccc0e6afab6dbf9f24a9ae993bf1a7648d029decfb6d8d928b0c365f5f0a861e90d89932a4f30ee96f75f4891d64a0287fab667be
```

### `dpkg` source package: `lame=3.100-3`

Binary Packages:

- `libmp3lame0:amd64=3.100-3`

Licenses: (parsed from: `/usr/share/doc/libmp3lame0/copyright`)

- `BSD-3-clause`
- `GPL-1`
- `GPL-1+`
- `LGPL-2`
- `LGPL-2+`
- `LGPL-2.1`
- `LGPL-2.1+`
- `zlib/libpng`

Source:

```console
$ apt-get source -qq --print-uris lame=3.100-3
'http://archive.ubuntu.com/ubuntu/pool/main/l/lame/lame_3.100-3.dsc' lame_3.100-3.dsc 2143 SHA256:5dec57a62002607b803b8eb659f0ed1079350de4ac9f8f8e52faa3d1e8fca08b
'http://archive.ubuntu.com/ubuntu/pool/main/l/lame/lame_3.100.orig.tar.gz' lame_3.100.orig.tar.gz 1524133 SHA256:ddfe36cab873794038ae2c1210557ad34857a4b6bdc515785d1da9e175b1da1e
'http://archive.ubuntu.com/ubuntu/pool/main/l/lame/lame_3.100-3.debian.tar.xz' lame_3.100-3.debian.tar.xz 12236 SHA256:85b6685d3c3c86a1331fa63e3efbd229acfcee360cd530dbdb6ea42b830d50d3
```

### `dpkg` source package: `language-pack-en-base=1:20.04+20210802`

Binary Packages:

- `language-pack-en-base=1:20.04+20210802`

Licenses: (parsed from: `/usr/share/doc/language-pack-en-base/copyright`)

- `GPL`

Source:

```console
$ apt-get source -qq --print-uris language-pack-en-base=1:20.04+20210802
'http://archive.ubuntu.com/ubuntu/pool/main/l/language-pack-en-base/language-pack-en-base_20.04+20210802.dsc' language-pack-en-base_20.04+20210802.dsc 1555 SHA512:75f9fbac3c553a9d6128d1dc489013ed423f72e24f0f6381408848a6bc1d49e0f0651c289dc357f5da102a41bb122633a8fed2928121b4006a80b196eba497c2
'http://archive.ubuntu.com/ubuntu/pool/main/l/language-pack-en-base/language-pack-en-base_20.04+20210802.tar.xz' language-pack-en-base_20.04+20210802.tar.xz 1614536 SHA512:4a726561214e44fb12de9cc03dfc5c64ff55268e3316ea6f107bffe76868503126d8bcdcee9db8180846bd90438f6a5ded657163fb7859b655ab74d68c293ce2
```

### `dpkg` source package: `language-pack-en=1:20.04+20210802`

Binary Packages:

- `language-pack-en=1:20.04+20210802`

Licenses: (parsed from: `/usr/share/doc/language-pack-en/copyright`)

- `GPL`

Source:

```console
$ apt-get source -qq --print-uris language-pack-en=1:20.04+20210802
'http://archive.ubuntu.com/ubuntu/pool/main/l/language-pack-en/language-pack-en_20.04+20210802.dsc' language-pack-en_20.04+20210802.dsc 1516 SHA512:feaa2802ce514669aebbb14c5281aa0b6d7cf0727e3eba25e9918cdb02abb9474952b9a36efa500c876e722bc606aea80434c00f622141a48a1e65829896e6a5
'http://archive.ubuntu.com/ubuntu/pool/main/l/language-pack-en/language-pack-en_20.04+20210802.tar.xz' language-pack-en_20.04+20210802.tar.xz 8092 SHA512:62af4044de4603b2a217c58620c598f0803754a7485d32533e9f246c0383b5fb160bb0cd57acfd456122107095bc75675e3c7bca1509c9c8bdc113f557228167
```

### `dpkg` source package: `language-pack-fr-base=1:20.04+20210802`

Binary Packages:

- `language-pack-fr-base=1:20.04+20210802`

Licenses: (parsed from: `/usr/share/doc/language-pack-fr-base/copyright`)

- `GPL`

Source:

```console
$ apt-get source -qq --print-uris language-pack-fr-base=1:20.04+20210802
'http://archive.ubuntu.com/ubuntu/pool/main/l/language-pack-fr-base/language-pack-fr-base_20.04+20210802.dsc' language-pack-fr-base_20.04+20210802.dsc 1555 SHA512:5fbcb7b13eee9ecb120e6e655b834fb90a9ed1a6b42eca3b054e217bdc2a3f28b082954727c3b2822fffd4f6671fe4e68da27fb978428c534e170bb6a94999bf
'http://archive.ubuntu.com/ubuntu/pool/main/l/language-pack-fr-base/language-pack-fr-base_20.04+20210802.tar.xz' language-pack-fr-base_20.04+20210802.tar.xz 3707536 SHA512:a9c20ee9d5218f2c48d3f295d712b2966f5b9802d774ba36ccf48da48ed944ab677015f3d8af06d58117ce1139fa4797b47a67adcc36c240256c062fb1fe10e8
```

### `dpkg` source package: `language-pack-fr=1:20.04+20210802`

Binary Packages:

- `language-pack-fr=1:20.04+20210802`

Licenses: (parsed from: `/usr/share/doc/language-pack-fr/copyright`)

- `GPL`

Source:

```console
$ apt-get source -qq --print-uris language-pack-fr=1:20.04+20210802
'http://archive.ubuntu.com/ubuntu/pool/main/l/language-pack-fr/language-pack-fr_20.04+20210802.dsc' language-pack-fr_20.04+20210802.dsc 1516 SHA512:c121e868bc87ee5b6433c3e16941776e7474433e265cdb0c6cc1d54cdf05b72911fe78b80a96bfbfbb1b70cf0f98d1a4f75828209c1146d68231190dc08d4c1b
'http://archive.ubuntu.com/ubuntu/pool/main/l/language-pack-fr/language-pack-fr_20.04+20210802.tar.xz' language-pack-fr_20.04+20210802.tar.xz 8096 SHA512:bbdd5d9e7f9efe7023401489c4717b80b9995d03f2b01bff2bfdfdbe4e1015b0c76c9b87917d9287877eec13594bebe5bca77d873ae6c4415500f7898113d59b
```

### `dpkg` source package: `lcms2=2.9-4`

Binary Packages:

- `liblcms2-2:amd64=2.9-4`

Licenses: (parsed from: `/usr/share/doc/liblcms2-2/copyright`)

- `GPL-2`
- `GPL-2+`
- `MIT`

Source:

```console
$ apt-get source -qq --print-uris lcms2=2.9-4
'http://archive.ubuntu.com/ubuntu/pool/main/l/lcms2/lcms2_2.9-4.dsc' lcms2_2.9-4.dsc 1956 SHA256:6db871353515693e8813911a8f81668b92e8c09fa9e6752e701fa8b14247775d
'http://archive.ubuntu.com/ubuntu/pool/main/l/lcms2/lcms2_2.9.orig.tar.gz' lcms2_2.9.orig.tar.gz 10974649 SHA256:48c6fdf98396fa245ed86e622028caf49b96fa22f3e5734f853f806fbc8e7d20
'http://archive.ubuntu.com/ubuntu/pool/main/l/lcms2/lcms2_2.9-4.debian.tar.xz' lcms2_2.9-4.debian.tar.xz 10748 SHA256:3dd811c431bed101269937299d28708dfe91f32070cf9786680bec26f408b65b
```

### `dpkg` source package: `libaacs=0.9.0-2`

Binary Packages:

- `libaacs0:amd64=0.9.0-2`

Licenses: (parsed from: `/usr/share/doc/libaacs0/copyright`)

- `LGPL-2.1`
- `LGPL-2.1+`

Source:

```console
$ apt-get source -qq --print-uris libaacs=0.9.0-2
'http://archive.ubuntu.com/ubuntu/pool/universe/liba/libaacs/libaacs_0.9.0-2.dsc' libaacs_0.9.0-2.dsc 2063 SHA256:06e778d7802214f87a5fcc695593b557e8acdc6989fb9b0988670c41e38ace49
'http://archive.ubuntu.com/ubuntu/pool/universe/liba/libaacs/libaacs_0.9.0.orig.tar.bz2' libaacs_0.9.0.orig.tar.bz2 316323 SHA256:47e0bdc9c9f0f6146ed7b4cc78ed1527a04a537012cf540cf5211e06a248bace
'http://archive.ubuntu.com/ubuntu/pool/universe/liba/libaacs/libaacs_0.9.0-2.debian.tar.xz' libaacs_0.9.0-2.debian.tar.xz 4080 SHA256:3171c13388e0ce79711c6c71e493e7fbcdcc2fb0a66e8428bf9a45ebebfd4857
```

### `dpkg` source package: `libabw=0.1.3-1build1`

Binary Packages:

- `libabw-0.1-1:amd64=0.1.3-1build1`

Licenses: (parsed from: `/usr/share/doc/libabw-0.1-1/copyright`)

- `GPL-3`
- `LGPL-3`
- `MPL-1.1 | GPL-3 | LGPL-3`
- `MPL-2.0`

Source:

```console
$ apt-get source -qq --print-uris libabw=0.1.3-1build1
'http://archive.ubuntu.com/ubuntu/pool/main/liba/libabw/libabw_0.1.3-1build1.dsc' libabw_0.1.3-1build1.dsc 2012 SHA256:ee42e1648eb8b4a22460611551458ff5c005bed8626a86f4451a8afa248547be
'http://archive.ubuntu.com/ubuntu/pool/main/liba/libabw/libabw_0.1.3.orig.tar.xz' libabw_0.1.3.orig.tar.xz 318808 SHA256:e763a9dc21c3d2667402d66e202e3f8ef4db51b34b79ef41f56cacb86dcd6eed
'http://archive.ubuntu.com/ubuntu/pool/main/liba/libabw/libabw_0.1.3-1build1.debian.tar.xz' libabw_0.1.3-1build1.debian.tar.xz 13068 SHA256:9f55446b08384504959248994336c27bb799b0b0af01641cab60e115fbdf0449
```

### `dpkg` source package: `libass=1:0.14.0-2`

Binary Packages:

- `libass9:amd64=1:0.14.0-2`

Licenses: (parsed from: `/usr/share/doc/libass9/copyright`)

- `GPL-2`
- `GPL-2+`
- `ISC`
- `other-1`

Source:

```console
$ apt-get source -qq --print-uris libass=1:0.14.0-2
'http://archive.ubuntu.com/ubuntu/pool/universe/liba/libass/libass_0.14.0-2.dsc' libass_0.14.0-2.dsc 2093 SHA256:efa8465d4acb8352fdb53b503b90076704b1930286ec1f339aaf5b2045316479
'http://archive.ubuntu.com/ubuntu/pool/universe/liba/libass/libass_0.14.0.orig.tar.xz' libass_0.14.0.orig.tar.xz 356256 SHA256:881f2382af48aead75b7a0e02e65d88c5ebd369fe46bc77d9270a94aa8fd38a2
'http://archive.ubuntu.com/ubuntu/pool/universe/liba/libass/libass_0.14.0-2.debian.tar.xz' libass_0.14.0-2.debian.tar.xz 5804 SHA256:f585191f54caf8ddf1608458b4146e62472e4f5713416eea7a48ae1c5647abed
```

### `dpkg` source package: `libassuan=2.5.3-7ubuntu2`

Binary Packages:

- `libassuan0:amd64=2.5.3-7ubuntu2`

Licenses: (parsed from: `/usr/share/doc/libassuan0/copyright`)

- `GAP`
- `GAP~FSF`
- `GPL-2`
- `GPL-2+`
- `GPL-2+ with libtool exception`
- `GPL-3`
- `GPL-3+`
- `LGPL-2.1`
- `LGPL-2.1+`
- `LGPL-3`
- `LGPL-3+`

Source:

```console
$ apt-get source -qq --print-uris libassuan=2.5.3-7ubuntu2
'http://archive.ubuntu.com/ubuntu/pool/main/liba/libassuan/libassuan_2.5.3-7ubuntu2.dsc' libassuan_2.5.3-7ubuntu2.dsc 2647 SHA256:014fbd728fc1d0e954ade2a8d975539fc00d455261ca14a88d78b9e29625ee41
'http://archive.ubuntu.com/ubuntu/pool/main/liba/libassuan/libassuan_2.5.3.orig.tar.bz2' libassuan_2.5.3.orig.tar.bz2 572348 SHA256:91bcb0403866b4e7c4bc1cc52ed4c364a9b5414b3994f718c70303f7f765e702
'http://archive.ubuntu.com/ubuntu/pool/main/liba/libassuan/libassuan_2.5.3.orig.tar.bz2.asc' libassuan_2.5.3.orig.tar.bz2.asc 952 SHA256:53b16a6619a2690b4f22da645a1d0c14b5664825c87b165ca5bd0de32607888a
'http://archive.ubuntu.com/ubuntu/pool/main/liba/libassuan/libassuan_2.5.3-7ubuntu2.debian.tar.xz' libassuan_2.5.3-7ubuntu2.debian.tar.xz 13936 SHA256:586836fdfffdc58b4d47548d0f6e54593daa78098c6276a788d8b66c3616e233
```

### `dpkg` source package: `libasyncns=0.8-6`

Binary Packages:

- `libasyncns0:amd64=0.8-6`

Licenses: (parsed from: `/usr/share/doc/libasyncns0/copyright`)

- `LGPL-2.1`
- `LGPL-2.1+`

Source:

```console
$ apt-get source -qq --print-uris libasyncns=0.8-6
'http://archive.ubuntu.com/ubuntu/pool/main/liba/libasyncns/libasyncns_0.8-6.dsc' libasyncns_0.8-6.dsc 1921 SHA256:d6a3cccafadceda0bd1542c6325c6238ec34a8ff85276d6f2e5914e282c67dc6
'http://archive.ubuntu.com/ubuntu/pool/main/liba/libasyncns/libasyncns_0.8.orig.tar.gz' libasyncns_0.8.orig.tar.gz 341591 SHA256:4f1a66e746cbe54ff3c2fbada5843df4fbbbe7481d80be003e8d11161935ab74
'http://archive.ubuntu.com/ubuntu/pool/main/liba/libasyncns/libasyncns_0.8-6.debian.tar.xz' libasyncns_0.8-6.debian.tar.xz 4564 SHA256:69b23a155b8a3da3bf68b1e440283e117c55e92bd3b4aa308605fe3f1164485e
```

### `dpkg` source package: `libauthen-sasl-perl=2.1600-1`

Binary Packages:

- `libauthen-sasl-perl=2.1600-1`

Licenses: (parsed from: `/usr/share/doc/libauthen-sasl-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libauthen-sasl-perl=2.1600-1
'http://archive.ubuntu.com/ubuntu/pool/main/liba/libauthen-sasl-perl/libauthen-sasl-perl_2.1600-1.dsc' libauthen-sasl-perl_2.1600-1.dsc 2313 SHA256:ddb85abf950c2e63d2403876f1dabb5c00c2390dc7e95e3b6124330c25ca02a2
'http://archive.ubuntu.com/ubuntu/pool/main/liba/libauthen-sasl-perl/libauthen-sasl-perl_2.1600.orig.tar.gz' libauthen-sasl-perl_2.1600.orig.tar.gz 45129 SHA256:6614fa7518f094f853741b63c73f3627168c5d3aca89b1d02b1016dc32854e09
'http://archive.ubuntu.com/ubuntu/pool/main/liba/libauthen-sasl-perl/libauthen-sasl-perl_2.1600-1.debian.tar.xz' libauthen-sasl-perl_2.1600-1.debian.tar.xz 3976 SHA256:edc85675ad2b6c97e4b6df5b4305d1f7afcd6b0af6407c3a4dea5ac58f9750e4
```

### `dpkg` source package: `libavc1394=0.5.4-5`

Binary Packages:

- `libavc1394-0:amd64=0.5.4-5`

Licenses: (parsed from: `/usr/share/doc/libavc1394-0/copyright`)

- `GPL-2`
- `GPL-2+`
- `LGPL-2.1`
- `LGPL-2.1+`

Source:

```console
$ apt-get source -qq --print-uris libavc1394=0.5.4-5
'http://archive.ubuntu.com/ubuntu/pool/main/liba/libavc1394/libavc1394_0.5.4-5.dsc' libavc1394_0.5.4-5.dsc 2122 SHA256:9faa03aa953eecfa46bc4fc98f7c8c2265a1d8cf0b26f04137e196e68b5f2176
'http://archive.ubuntu.com/ubuntu/pool/main/liba/libavc1394/libavc1394_0.5.4.orig.tar.gz' libavc1394_0.5.4.orig.tar.gz 341679 SHA256:7cb1ff09506ae911ca9860bef4af08c2403f3e131f6c913a2cbd6ddca4215b53
'http://archive.ubuntu.com/ubuntu/pool/main/liba/libavc1394/libavc1394_0.5.4-5.debian.tar.xz' libavc1394_0.5.4-5.debian.tar.xz 6600 SHA256:783dde153ec5287c8ca278e0911163ecf4c568f95ac0a9c49307fdd941659ff1
```

### `dpkg` source package: `libbdplus=0.1.2-3`

Binary Packages:

- `libbdplus0:amd64=0.1.2-3`

Licenses: (parsed from: `/usr/share/doc/libbdplus0/copyright`)

- `LGPL-2.1`
- `LGPL-2.1+`
- `public-domain`

Source:

```console
$ apt-get source -qq --print-uris libbdplus=0.1.2-3
'http://archive.ubuntu.com/ubuntu/pool/universe/libb/libbdplus/libbdplus_0.1.2-3.dsc' libbdplus_0.1.2-3.dsc 2084 SHA256:18cc7b1a72e60b7d6bc39a39ea3908ae2973ee7d221f73596a120b9052ad3b22
'http://archive.ubuntu.com/ubuntu/pool/universe/libb/libbdplus/libbdplus_0.1.2.orig.tar.bz2' libbdplus_0.1.2.orig.tar.bz2 319828 SHA256:a631cae3cd34bf054db040b64edbfc8430936e762eb433b1789358ac3d3dc80a
'http://archive.ubuntu.com/ubuntu/pool/universe/libb/libbdplus/libbdplus_0.1.2-3.debian.tar.xz' libbdplus_0.1.2-3.debian.tar.xz 2780 SHA256:a7b93a8a9920911dcd81609aa7ddfeea0b115a296c065be9e17be4797dffa87c
```

### `dpkg` source package: `libbluray=1:1.2.0-1`

Binary Packages:

- `libbluray2:amd64=1:1.2.0-1`

Licenses: (parsed from: `/usr/share/doc/libbluray2/copyright`)

- `BSD-3-clause`
- `GPL-2`
- `GPL-2+`
- `LGPL-2.1`
- `LGPL-2.1+`
- `MPL-1.0`
- `custom`

Source:

```console
$ apt-get source -qq --print-uris libbluray=1:1.2.0-1
'http://archive.ubuntu.com/ubuntu/pool/universe/libb/libbluray/libbluray_1.2.0-1.dsc' libbluray_1.2.0-1.dsc 2420 SHA256:535f4c30f3f8eea2f4e4b0d998e8900c26bad5e6285ac9ef616c433f92470ab9
'http://archive.ubuntu.com/ubuntu/pool/universe/libb/libbluray/libbluray_1.2.0.orig.tar.bz2' libbluray_1.2.0.orig.tar.bz2 747265 SHA256:cd41ea06fd2512a77ebf63872873641908ef81ce2fe4e4c842f6035a47696c11
'http://archive.ubuntu.com/ubuntu/pool/universe/libb/libbluray/libbluray_1.2.0-1.debian.tar.xz' libbluray_1.2.0-1.debian.tar.xz 17240 SHA256:a94a021ae1fe508488334c20f1172e4df6b9e76e7ffba47f6ed2d2b9893b2498
```

### `dpkg` source package: `libbs2b=3.1.0+dfsg-2.2build1`

Binary Packages:

- `libbs2b0:amd64=3.1.0+dfsg-2.2build1`

Licenses: (parsed from: `/usr/share/doc/libbs2b0/copyright`)

- `FSF-unlimited`
- `GPL-2`
- `GPL-2+`
- `GPL-3`
- `GPL-3+`
- `MIT`
- `MIT+FSF-public`

Source:

```console
$ apt-get source -qq --print-uris libbs2b=3.1.0+dfsg-2.2build1
'http://archive.ubuntu.com/ubuntu/pool/universe/libb/libbs2b/libbs2b_3.1.0+dfsg-2.2build1.dsc' libbs2b_3.1.0+dfsg-2.2build1.dsc 2002 SHA256:9daaaa41db6f1b1f45e89e1dfc0ce41a0c11bee56d1a271d653e8063852ad0d3
'http://archive.ubuntu.com/ubuntu/pool/universe/libb/libbs2b/libbs2b_3.1.0+dfsg.orig.tar.gz' libbs2b_3.1.0+dfsg.orig.tar.gz 330675 SHA256:c23faf614f787342c1a1a40f83064f2e5a49391733c029dc31d09fba759cee0a
'http://archive.ubuntu.com/ubuntu/pool/universe/libb/libbs2b/libbs2b_3.1.0+dfsg-2.2build1.debian.tar.xz' libbs2b_3.1.0+dfsg-2.2build1.debian.tar.xz 4672 SHA256:05f690783170f7920717ce1548107d38490e48bcca8237997735550bca472f03
```

### `dpkg` source package: `libbsd=0.10.0-1`

Binary Packages:

- `libbsd0:amd64=0.10.0-1`

Licenses: (parsed from: `/usr/share/doc/libbsd0/copyright`)

- `BSD-2-clause`
- `BSD-2-clause-NetBSD`
- `BSD-2-clause-author`
- `BSD-2-clause-verbatim`
- `BSD-3-clause`
- `BSD-3-clause-John-Birrell`
- `BSD-3-clause-Regents`
- `BSD-3-clause-author`
- `BSD-4-clause-Christopher-G-Demetriou`
- `BSD-4-clause-Niels-Provos`
- `BSD-5-clause-Peter-Wemm`
- `Beerware`
- `Expat`
- `ISC`
- `ISC-Original`
- `public-domain`
- `public-domain-Colin-Plumb`

Source:

```console
$ apt-get source -qq --print-uris libbsd=0.10.0-1
'http://archive.ubuntu.com/ubuntu/pool/main/libb/libbsd/libbsd_0.10.0-1.dsc' libbsd_0.10.0-1.dsc 2197 SHA256:7c05e2c73658f64cbd4e1762b716cc7c4c1d68391191e82c7d266a351430edd6
'http://archive.ubuntu.com/ubuntu/pool/main/libb/libbsd/libbsd_0.10.0.orig.tar.xz' libbsd_0.10.0.orig.tar.xz 393576 SHA256:34b8adc726883d0e85b3118fa13605e179a62b31ba51f676136ecb2d0bc1a887
'http://archive.ubuntu.com/ubuntu/pool/main/libb/libbsd/libbsd_0.10.0.orig.tar.xz.asc' libbsd_0.10.0.orig.tar.xz.asc 833 SHA256:4362f6d811ffc06659ac5cf777d8d01157bedfc28720b41fb485afb0a5acc0c7
'http://archive.ubuntu.com/ubuntu/pool/main/libb/libbsd/libbsd_0.10.0-1.debian.tar.xz' libbsd_0.10.0-1.debian.tar.xz 16660 SHA256:4cf37d6d5b72702b31b07384612e07173e94e081feef71fec206f86ab38f2411
```

### `dpkg` source package: `libcaca=0.99.beta19-2.1ubuntu1.20.04.1`

Binary Packages:

- `libcaca0:amd64=0.99.beta19-2.1ubuntu1.20.04.1`

Licenses: (parsed from: `/usr/share/doc/libcaca0/copyright`)

- `LGPL`

**WARNING:** unable to find source (`apt-get source` failed or returned no results)!  
This is *usually* due to a new package version being released and the old version being removed.


### `dpkg` source package: `libcanberra=0.30-7ubuntu1`

Binary Packages:

- `libcanberra0:amd64=0.30-7ubuntu1`

Licenses: (parsed from: `/usr/share/doc/libcanberra0/copyright`)

- `LGPL-2`
- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris libcanberra=0.30-7ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/libc/libcanberra/libcanberra_0.30-7ubuntu1.dsc' libcanberra_0.30-7ubuntu1.dsc 2618 SHA256:a5d220335e3449fdeea8a2083116dc68a95a9119fbfa99352312666319217bc5
'http://archive.ubuntu.com/ubuntu/pool/main/libc/libcanberra/libcanberra_0.30.orig.tar.xz' libcanberra_0.30.orig.tar.xz 318960 SHA256:c2b671e67e0c288a69fc33dc1b6f1b534d07882c2aceed37004bf48c601afa72
'http://archive.ubuntu.com/ubuntu/pool/main/libc/libcanberra/libcanberra_0.30-7ubuntu1.debian.tar.xz' libcanberra_0.30-7ubuntu1.debian.tar.xz 11080 SHA256:e9e5902961fe438ce422ff9f54d4a9d9437013c6282145fd9e2a0ed2fca2aa43
```

### `dpkg` source package: `libcap-ng=0.7.9-2.1build1`

Binary Packages:

- `libcap-ng0:amd64=0.7.9-2.1build1`

Licenses: (parsed from: `/usr/share/doc/libcap-ng0/copyright`)

- `GPL-2`
- `GPL-3`
- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris libcap-ng=0.7.9-2.1build1
'http://archive.ubuntu.com/ubuntu/pool/main/libc/libcap-ng/libcap-ng_0.7.9-2.1build1.dsc' libcap-ng_0.7.9-2.1build1.dsc 2158 SHA256:6d74cf5c418659d70bce8e9a4bf6f0ef0210dbcadac15e0c4d4471c4671230a1
'http://archive.ubuntu.com/ubuntu/pool/main/libc/libcap-ng/libcap-ng_0.7.9.orig.tar.gz' libcap-ng_0.7.9.orig.tar.gz 449038 SHA256:4a1532bcf3731aade40936f6d6a586ed5a66ca4c7455e1338d1f6c3e09221328
'http://archive.ubuntu.com/ubuntu/pool/main/libc/libcap-ng/libcap-ng_0.7.9-2.1build1.debian.tar.xz' libcap-ng_0.7.9-2.1build1.debian.tar.xz 6256 SHA256:b73a0a36bb0c1c8144828552dedb7b3493f4a08b1c31a0f1d7046cf1682eac7d
```

### `dpkg` source package: `libcap2=1:2.32-1`

Binary Packages:

- `libcap2:amd64=1:2.32-1`
- `libcap2-bin=1:2.32-1`
- `libpam-cap:amd64=1:2.32-1`

Licenses: (parsed from: `/usr/share/doc/libcap2/copyright`, `/usr/share/doc/libcap2-bin/copyright`, `/usr/share/doc/libpam-cap/copyright`)

- `BSD-3-clause`
- `GPL-2`
- `GPL-2+`

Source:

```console
$ apt-get source -qq --print-uris libcap2=1:2.32-1
'http://archive.ubuntu.com/ubuntu/pool/main/libc/libcap2/libcap2_2.32-1.dsc' libcap2_2.32-1.dsc 2197 SHA256:7753a4dd83fca414a014579005d91653d71e95adfdbfd8d6c5c833f2372eb6da
'http://archive.ubuntu.com/ubuntu/pool/main/libc/libcap2/libcap2_2.32.orig.tar.xz' libcap2_2.32.orig.tar.xz 99708 SHA256:1005e3d227f2340ad1e3360ef8b69d15e3c72a29c09f4894d7aac038bd26e2be
'http://archive.ubuntu.com/ubuntu/pool/main/libc/libcap2/libcap2_2.32-1.debian.tar.xz' libcap2_2.32-1.debian.tar.xz 27924 SHA256:827c711e65830910ef953cdf81aeb75b77234b55a56ddfb27541bb8f487cd22c
```

### `dpkg` source package: `libcdio-paranoia=10.2+2.0.0-1`

Binary Packages:

- `libcdio-cdda2:amd64=10.2+2.0.0-1`
- `libcdio-paranoia2:amd64=10.2+2.0.0-1`

Licenses: (parsed from: `/usr/share/doc/libcdio-cdda2/copyright`, `/usr/share/doc/libcdio-paranoia2/copyright`)

- `GFDL-1.2`
- `GPL-3`

Source:

```console
$ apt-get source -qq --print-uris libcdio-paranoia=10.2+2.0.0-1
'http://archive.ubuntu.com/ubuntu/pool/main/libc/libcdio-paranoia/libcdio-paranoia_10.2+2.0.0-1.dsc' libcdio-paranoia_10.2+2.0.0-1.dsc 2217 SHA256:5791ebe76ea978a9ab93cd080b448762731757ec02cf68842a62f0e5f17f121a
'http://archive.ubuntu.com/ubuntu/pool/main/libc/libcdio-paranoia/libcdio-paranoia_10.2+2.0.0.orig.tar.gz' libcdio-paranoia_10.2+2.0.0.orig.tar.gz 2095577 SHA256:69f513a563506f691f782d0f55f4816448852964688321398f196a200153ec95
'http://archive.ubuntu.com/ubuntu/pool/main/libc/libcdio-paranoia/libcdio-paranoia_10.2+2.0.0-1.debian.tar.xz' libcdio-paranoia_10.2+2.0.0-1.debian.tar.xz 7616 SHA256:2592f77a636682be3f2e8edb39d44e7db20b41c4dc9c1ed9199f1c8ce50505c8
```

### `dpkg` source package: `libcdio=2.0.0-2`

Binary Packages:

- `libcdio18:amd64=2.0.0-2`

Licenses: (parsed from: `/usr/share/doc/libcdio18/copyright`)

- `GFDL`
- `GPL`

Source:

```console
$ apt-get source -qq --print-uris libcdio=2.0.0-2
'http://archive.ubuntu.com/ubuntu/pool/main/libc/libcdio/libcdio_2.0.0-2.dsc' libcdio_2.0.0-2.dsc 2179 SHA256:721d1c3da7cbe5aa350676318c5ee7dd39f2f3317f7fb5d0dd60d1c659d72364
'http://archive.ubuntu.com/ubuntu/pool/main/libc/libcdio/libcdio_2.0.0.orig.tar.gz' libcdio_2.0.0.orig.tar.gz 2354813 SHA256:1b481b5da009bea31db875805665974e2fc568e2b2afa516f4036733657cf958
'http://archive.ubuntu.com/ubuntu/pool/main/libc/libcdio/libcdio_2.0.0-2.debian.tar.xz' libcdio_2.0.0-2.debian.tar.xz 10732 SHA256:d49483d113cdc36dd7d556f3fcfaeeef6f77e3381d459eb71d68be5bd08a30f6
```

### `dpkg` source package: `libcdr=0.1.6-1build2`

Binary Packages:

- `libcdr-0.1-1:amd64=0.1.6-1build2`

Licenses: (parsed from: `/usr/share/doc/libcdr-0.1-1/copyright`)

- `MPL-2.0`

Source:

```console
$ apt-get source -qq --print-uris libcdr=0.1.6-1build2
'http://archive.ubuntu.com/ubuntu/pool/main/libc/libcdr/libcdr_0.1.6-1build2.dsc' libcdr_0.1.6-1build2.dsc 2157 SHA256:9b9a1f0d9865fb2ec328d9eae9c0ac812a8584c0919e30d44f3a23ab07fbef4e
'http://archive.ubuntu.com/ubuntu/pool/main/libc/libcdr/libcdr_0.1.6.orig.tar.xz' libcdr_0.1.6.orig.tar.xz 612068 SHA256:01cd00b04a030977e544433c2d127c997205332cd9b8e35ec0ee17110da7f861
'http://archive.ubuntu.com/ubuntu/pool/main/libc/libcdr/libcdr_0.1.6-1build2.debian.tar.xz' libcdr_0.1.6-1build2.debian.tar.xz 8016 SHA256:8c7cdd454e7d315ebc44374f8704c8ea5f6e08f55c080d21a62d538100a87c79
```

### `dpkg` source package: `libcmis=0.5.2-1ubuntu1`

Binary Packages:

- `libcmis-0.5-5v5=0.5.2-1ubuntu1`

Licenses: (parsed from: `/usr/share/doc/libcmis-0.5-5v5/copyright`)

- `GPL`
- `LGPL`
- `MPL | GPL2+ | LGPL2+`

Source:

```console
$ apt-get source -qq --print-uris libcmis=0.5.2-1ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/libc/libcmis/libcmis_0.5.2-1ubuntu1.dsc' libcmis_0.5.2-1ubuntu1.dsc 2239 SHA256:8c05bb632a72d0fbf44410587052adc3c86d1cb8afb07427838cf0b739262bcc
'http://archive.ubuntu.com/ubuntu/pool/main/libc/libcmis/libcmis_0.5.2.orig.tar.gz' libcmis_0.5.2.orig.tar.gz 808619 SHA256:ed6f681a48abbf3c2324564b17a180d21fa9503230e8708825e1ad80daee4f81
'http://archive.ubuntu.com/ubuntu/pool/main/libc/libcmis/libcmis_0.5.2-1ubuntu1.debian.tar.xz' libcmis_0.5.2-1ubuntu1.debian.tar.xz 4856 SHA256:971d98194205f1620dbe145194295665ee672eced8a59959237cdc422dcb1cff
```

### `dpkg` source package: `libcommons-logging-java=1.2-2`

Binary Packages:

- `libcommons-logging-java=1.2-2`

Licenses: (parsed from: `/usr/share/doc/libcommons-logging-java/copyright`)

- `Apache-2.0`

Source:

```console
$ apt-get source -qq --print-uris libcommons-logging-java=1.2-2
'http://archive.ubuntu.com/ubuntu/pool/universe/libc/libcommons-logging-java/libcommons-logging-java_1.2-2.dsc' libcommons-logging-java_1.2-2.dsc 2416 SHA256:98de13c4e77e3cb89291b32d54aecdfbb6e27a6c74698a405da573bf5700b90e
'http://archive.ubuntu.com/ubuntu/pool/universe/libc/libcommons-logging-java/libcommons-logging-java_1.2.orig.tar.xz' libcommons-logging-java_1.2.orig.tar.xz 134940 SHA256:10dda2b5647087c3478083ab8bc5ef4bcb95d4515b4aed79dcc59b524072b3cd
'http://archive.ubuntu.com/ubuntu/pool/universe/libc/libcommons-logging-java/libcommons-logging-java_1.2-2.debian.tar.xz' libcommons-logging-java_1.2-2.debian.tar.xz 7764 SHA256:d76e48eebb08c6cbc3fb3fe8c1d357ae2f18d03b1c04c57d0a65d0349cc552cb
```

### `dpkg` source package: `libdata-dump-perl=1.23-1`

Binary Packages:

- `libdata-dump-perl=1.23-1`

Licenses: (parsed from: `/usr/share/doc/libdata-dump-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libdata-dump-perl=1.23-1
'http://archive.ubuntu.com/ubuntu/pool/main/libd/libdata-dump-perl/libdata-dump-perl_1.23-1.dsc' libdata-dump-perl_1.23-1.dsc 2261 SHA256:5362c6e5f931eeac87ea8347c77bd08b2fc61919f8a1d3e0f93468dadeca8f53
'http://archive.ubuntu.com/ubuntu/pool/main/libd/libdata-dump-perl/libdata-dump-perl_1.23.orig.tar.gz' libdata-dump-perl_1.23.orig.tar.gz 20771 SHA256:af53b05ef1387b4cab4427e6789179283e4f0da8cf036e8db516ddb344512b65
'http://archive.ubuntu.com/ubuntu/pool/main/libd/libdata-dump-perl/libdata-dump-perl_1.23-1.debian.tar.xz' libdata-dump-perl_1.23-1.debian.tar.xz 3456 SHA256:8f4a0f41e4ca3c3bfd5b94585910ade663cfc32972e5a52d6bf031a26648a48e
```

### `dpkg` source package: `libdatrie=0.2.12-3`

Binary Packages:

- `libdatrie1:amd64=0.2.12-3`

Licenses: (parsed from: `/usr/share/doc/libdatrie1/copyright`)

- `GPL-2`
- `GPL-2+`
- `LGPL-2.1`
- `LGPL-2.1+`

Source:

```console
$ apt-get source -qq --print-uris libdatrie=0.2.12-3
'http://archive.ubuntu.com/ubuntu/pool/main/libd/libdatrie/libdatrie_0.2.12-3.dsc' libdatrie_0.2.12-3.dsc 2260 SHA256:631b3aa1b0cf12bcb04df8a19a8370445801a176edce830e74c01f6a55f778aa
'http://archive.ubuntu.com/ubuntu/pool/main/libd/libdatrie/libdatrie_0.2.12.orig.tar.xz' libdatrie_0.2.12.orig.tar.xz 310236 SHA256:452dcc4d3a96c01f80f7c291b42be11863cd1554ff78b93e110becce6e00b149
'http://archive.ubuntu.com/ubuntu/pool/main/libd/libdatrie/libdatrie_0.2.12-3.debian.tar.xz' libdatrie_0.2.12-3.debian.tar.xz 9188 SHA256:10409d93b3762b8ac8e0851bb2b71f76c2c5b57df8999bf8b9686d951c8b7476
```

### `dpkg` source package: `libdc1394-22=2.2.5-2.1`

Binary Packages:

- `libdc1394-22:amd64=2.2.5-2.1`

Licenses: (parsed from: `/usr/share/doc/libdc1394-22/copyright`)

- `GPL-2`
- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris libdc1394-22=2.2.5-2.1
'http://archive.ubuntu.com/ubuntu/pool/universe/libd/libdc1394-22/libdc1394-22_2.2.5-2.1.dsc' libdc1394-22_2.2.5-2.1.dsc 2194 SHA256:0d4a9bf10f1e555e7afa0f957f7f08e9ef9ec26ffa3a0b35449786a2cad15521
'http://archive.ubuntu.com/ubuntu/pool/universe/libd/libdc1394-22/libdc1394-22_2.2.5.orig.tar.gz' libdc1394-22_2.2.5.orig.tar.gz 611918 SHA256:350cc8d08aee5ffc4e1f3049e2e1c2bc6660642d424595157da97ab5b1263337
'http://archive.ubuntu.com/ubuntu/pool/universe/libd/libdc1394-22/libdc1394-22_2.2.5-2.1.debian.tar.xz' libdc1394-22_2.2.5-2.1.debian.tar.xz 6668 SHA256:cf8c6566d08a011a084ca157e984414f849077ff3c6f6659dbf246302fc5d53b
```

### `dpkg` source package: `libdrm=2.4.105-3~20.04.2`

Binary Packages:

- `libdrm-amdgpu1:amd64=2.4.105-3~20.04.2`
- `libdrm-common=2.4.105-3~20.04.2`
- `libdrm-intel1:amd64=2.4.105-3~20.04.2`
- `libdrm-nouveau2:amd64=2.4.105-3~20.04.2`
- `libdrm-radeon1:amd64=2.4.105-3~20.04.2`
- `libdrm2:amd64=2.4.105-3~20.04.2`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libdrm=2.4.105-3~20.04.2
'http://archive.ubuntu.com/ubuntu/pool/main/libd/libdrm/libdrm_2.4.105-3~20.04.2.dsc' libdrm_2.4.105-3~20.04.2.dsc 3116 SHA512:73ef02c2bb4e0ef7725fd6f9a215bad8b30072444f6d712fd646065898aca8ed8fed317c7ea625f3c7cd25a3366ea2404e3dfd05dc825e57d7d2bbdbbc5c0f25
'http://archive.ubuntu.com/ubuntu/pool/main/libd/libdrm/libdrm_2.4.105.orig.tar.xz' libdrm_2.4.105.orig.tar.xz 420376 SHA512:083a04af7208e58be21b89c6ebdbe2db3ba00cd29f0d271bd38bfe97dfca741edafddaaf9b5b95c20fac2c9b700434ea5b21397de26f7073169ad6f5b090f715
'http://archive.ubuntu.com/ubuntu/pool/main/libd/libdrm/libdrm_2.4.105-3~20.04.2.debian.tar.xz' libdrm_2.4.105-3~20.04.2.debian.tar.xz 59772 SHA512:02b8a05f9417726fce08f5aea7796c5440182d7ae5110ca954813f74b6e3d9b8d772b33c4c6db6dcacbb76c2dff9ce9199485d3c876d636375f8a4406dad44aa
```

### `dpkg` source package: `libe-book=0.1.3-1build3`

Binary Packages:

- `libe-book-0.1-1:amd64=0.1.3-1build3`

Licenses: (parsed from: `/usr/share/doc/libe-book-0.1-1/copyright`)

- `MPL-2.0`

Source:

```console
$ apt-get source -qq --print-uris libe-book=0.1.3-1build3
'http://archive.ubuntu.com/ubuntu/pool/main/libe/libe-book/libe-book_0.1.3-1build3.dsc' libe-book_0.1.3-1build3.dsc 2090 SHA256:8cd9cb5a86596cbd22558970e40de2ec181f22b44f093898124e45c90403d1ce
'http://archive.ubuntu.com/ubuntu/pool/main/libe/libe-book/libe-book_0.1.3.orig.tar.xz' libe-book_0.1.3.orig.tar.xz 416268 SHA256:7e8d8ff34f27831aca3bc6f9cc532c2f90d2057c778963b884ff3d1e34dfe1f9
'http://archive.ubuntu.com/ubuntu/pool/main/libe/libe-book/libe-book_0.1.3-1build3.debian.tar.xz' libe-book_0.1.3-1build3.debian.tar.xz 7292 SHA256:c002d8e33ac542498de7a437e67422977514a0c1cb7ca6bfe1f8430aac1206ca
```

### `dpkg` source package: `libedit=3.1-20191231-1`

Binary Packages:

- `libedit2:amd64=3.1-20191231-1`

Licenses: (parsed from: `/usr/share/doc/libedit2/copyright`)

- `BSD-3-clause`

Source:

```console
$ apt-get source -qq --print-uris libedit=3.1-20191231-1
'http://archive.ubuntu.com/ubuntu/pool/main/libe/libedit/libedit_3.1-20191231-1.dsc' libedit_3.1-20191231-1.dsc 2129 SHA256:1be31eebf9cf3b38a9e7c3c4d4b37f002e3f89df48f00dec32506cbe9337ae38
'http://archive.ubuntu.com/ubuntu/pool/main/libe/libedit/libedit_3.1-20191231.orig.tar.gz' libedit_3.1-20191231.orig.tar.gz 516801 SHA256:dbb82cb7e116a5f8025d35ef5b4f7d4a3cdd0a3909a146a39112095a2d229071
'http://archive.ubuntu.com/ubuntu/pool/main/libe/libedit/libedit_3.1-20191231-1.debian.tar.xz' libedit_3.1-20191231-1.debian.tar.xz 14168 SHA256:f815baa1932f9df5d4cdb316a85ebd3cc91441c4d83ba2c8454f342573ed0eab
```

### `dpkg` source package: `libencode-locale-perl=1.05-1`

Binary Packages:

- `libencode-locale-perl=1.05-1`

Licenses: (parsed from: `/usr/share/doc/libencode-locale-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libencode-locale-perl=1.05-1
'http://archive.ubuntu.com/ubuntu/pool/main/libe/libencode-locale-perl/libencode-locale-perl_1.05-1.dsc' libencode-locale-perl_1.05-1.dsc 2107 SHA256:2a91183e11732070009fa8b01febde8509e00b69585f8eb56a5c8dce61a5df51
'http://archive.ubuntu.com/ubuntu/pool/main/libe/libencode-locale-perl/libencode-locale-perl_1.05.orig.tar.gz' libencode-locale-perl_1.05.orig.tar.gz 8355 SHA256:176fa02771f542a4efb1dbc2a4c928e8f4391bf4078473bd6040d8f11adb0ec1
'http://archive.ubuntu.com/ubuntu/pool/main/libe/libencode-locale-perl/libencode-locale-perl_1.05-1.debian.tar.xz' libencode-locale-perl_1.05-1.debian.tar.xz 2528 SHA256:e722122fa3c8cf0d6d5fead77184d791c341c54985f171a3bb9ece3688d94e48
```

### `dpkg` source package: `libeot=0.01-5`

Binary Packages:

- `libeot0:amd64=0.01-5`

Licenses: (parsed from: `/usr/share/doc/libeot0/copyright`)

- `GPL-2`
- `GPL-2+`
- `MPL-2.0`
- `other`

Source:

```console
$ apt-get source -qq --print-uris libeot=0.01-5
'http://archive.ubuntu.com/ubuntu/pool/main/libe/libeot/libeot_0.01-5.dsc' libeot_0.01-5.dsc 1949 SHA256:71933404d061aeffe2c0e5da353ef7c5146fd061131b0a8c31257b16b080cab6
'http://archive.ubuntu.com/ubuntu/pool/main/libe/libeot/libeot_0.01.orig.tar.bz2' libeot_0.01.orig.tar.bz2 260288 SHA256:cf5091fa8e7dcdbe667335eb90a2cfdd0a3fe8f8c7c8d1ece44d9d055736a06a
'http://archive.ubuntu.com/ubuntu/pool/main/libe/libeot/libeot_0.01-5.debian.tar.xz' libeot_0.01-5.debian.tar.xz 7492 SHA256:e6f5685fee36d82d31e1d2b2334314098b8bac7b87de59ee89809795f85b87c5
```

### `dpkg` source package: `libepoxy=1.5.4-1`

Binary Packages:

- `libepoxy0:amd64=1.5.4-1`

Licenses: (parsed from: `/usr/share/doc/libepoxy0/copyright`)

- `Expat`

Source:

```console
$ apt-get source -qq --print-uris libepoxy=1.5.4-1
'http://archive.ubuntu.com/ubuntu/pool/main/libe/libepoxy/libepoxy_1.5.4-1.dsc' libepoxy_1.5.4-1.dsc 2106 SHA256:fb88cb692c384973c4bab06dee97225e434a3abc921f4218e4c9770663d3455a
'http://archive.ubuntu.com/ubuntu/pool/main/libe/libepoxy/libepoxy_1.5.4.orig.tar.gz' libepoxy_1.5.4.orig.tar.gz 337615 SHA256:c926fcc606901f3e03e371027056fd478da43e01ce2da7ffc48b5a0de0ca107c
'http://archive.ubuntu.com/ubuntu/pool/main/libe/libepoxy/libepoxy_1.5.4-1.debian.tar.xz' libepoxy_1.5.4-1.debian.tar.xz 17136 SHA256:b34513260555063a2750affe4694bf528a806d3c1ff9d858bfc9588abd434a63
```

### `dpkg` source package: `libepubgen=0.1.1-1ubuntu2`

Binary Packages:

- `libepubgen-0.1-1:amd64=0.1.1-1ubuntu2`

Licenses: (parsed from: `/usr/share/doc/libepubgen-0.1-1/copyright`)

- `MPL-2.0`
- `other`

Source:

```console
$ apt-get source -qq --print-uris libepubgen=0.1.1-1ubuntu2
'http://archive.ubuntu.com/ubuntu/pool/main/libe/libepubgen/libepubgen_0.1.1-1ubuntu2.dsc' libepubgen_0.1.1-1ubuntu2.dsc 2173 SHA256:f9632e1a51ca9dcd7a9f2317e2f20e9530ee6f46656c11bcf5768ecb020d190c
'http://archive.ubuntu.com/ubuntu/pool/main/libe/libepubgen/libepubgen_0.1.1.orig.tar.xz' libepubgen_0.1.1.orig.tar.xz 324380 SHA256:03e084b994cbeffc8c3dd13303b2cb805f44d8f2c3b79f7690d7e3fc7f6215ad
'http://archive.ubuntu.com/ubuntu/pool/main/libe/libepubgen/libepubgen_0.1.1-1ubuntu2.debian.tar.xz' libepubgen_0.1.1-1ubuntu2.debian.tar.xz 6168 SHA256:157fe0ff0667dc01acef3fb86227cd4f36e62115048a6f3d8c37d984cc4fe5a9
```

### `dpkg` source package: `libetonyek=0.1.9-3`

Binary Packages:

- `libetonyek-0.1-1:amd64=0.1.9-3`

Licenses: (parsed from: `/usr/share/doc/libetonyek-0.1-1/copyright`)

- `MPL 2.0`

Source:

```console
$ apt-get source -qq --print-uris libetonyek=0.1.9-3
'http://archive.ubuntu.com/ubuntu/pool/main/libe/libetonyek/libetonyek_0.1.9-3.dsc' libetonyek_0.1.9-3.dsc 2169 SHA256:4c4ce9bc275bba6ac1bc2910250b7f587f3372aca5022302679c943a7ffd4237
'http://archive.ubuntu.com/ubuntu/pool/main/libe/libetonyek/libetonyek_0.1.9.orig.tar.xz' libetonyek_0.1.9.orig.tar.xz 1477064 SHA256:e61677e8799ce6e55b25afc11aa5339113f6a49cff031f336e32fa58635b1a4a
'http://archive.ubuntu.com/ubuntu/pool/main/libe/libetonyek/libetonyek_0.1.9-3.debian.tar.xz' libetonyek_0.1.9-3.debian.tar.xz 42780 SHA256:4e090ef31b156cd7a073912007987acdd7b6ffc4e31d7f275ea73bccd4ab3b9b
```

### `dpkg` source package: `libexttextcat=3.4.5-1`

Binary Packages:

- `libexttextcat-2.0-0:amd64=3.4.5-1`
- `libexttextcat-data=3.4.5-1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libexttextcat=3.4.5-1
'http://archive.ubuntu.com/ubuntu/pool/main/libe/libexttextcat/libexttextcat_3.4.5-1.dsc' libexttextcat_3.4.5-1.dsc 2099 SHA256:9a5f988e773efec298260e0464df6b4d77b01d82d2a989d317c5529f9c3ac586
'http://archive.ubuntu.com/ubuntu/pool/main/libe/libexttextcat/libexttextcat_3.4.5.orig.tar.xz' libexttextcat_3.4.5.orig.tar.xz 1041268 SHA256:13fdbc9d4c489a4d0519e51933a1aa21fe3fb9eb7da191b87f7a63e82797dac8
'http://archive.ubuntu.com/ubuntu/pool/main/libe/libexttextcat/libexttextcat_3.4.5-1.debian.tar.xz' libexttextcat_3.4.5-1.debian.tar.xz 7224 SHA256:bf214f4c725d236a8e77b4f7199316255de431eb48638b78f5346890fb3c0849
```

### `dpkg` source package: `libffi=3.3-4`

Binary Packages:

- `libffi7:amd64=3.3-4`

Licenses: (parsed from: `/usr/share/doc/libffi7/copyright`)

- `GPL`

Source:

```console
$ apt-get source -qq --print-uris libffi=3.3-4
'http://archive.ubuntu.com/ubuntu/pool/main/libf/libffi/libffi_3.3-4.dsc' libffi_3.3-4.dsc 1932 SHA256:4190ad8e7ae9167a0c67c5926bc3705acb191745cca93ef845dbc06fc097f380
'http://archive.ubuntu.com/ubuntu/pool/main/libf/libffi/libffi_3.3.orig.tar.gz' libffi_3.3.orig.tar.gz 1305466 SHA256:72fba7922703ddfa7a028d513ac15a85c8d54c8d67f55fa5a4802885dc652056
'http://archive.ubuntu.com/ubuntu/pool/main/libf/libffi/libffi_3.3-4.debian.tar.xz' libffi_3.3-4.debian.tar.xz 9016 SHA256:0e8a6d9d87202d04d7646178479c3d365a845f9723da26625d533a169b378100
```

### `dpkg` source package: `libfile-basedir-perl=0.08-1`

Binary Packages:

- `libfile-basedir-perl=0.08-1`

Licenses: (parsed from: `/usr/share/doc/libfile-basedir-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libfile-basedir-perl=0.08-1
'http://archive.ubuntu.com/ubuntu/pool/main/libf/libfile-basedir-perl/libfile-basedir-perl_0.08-1.dsc' libfile-basedir-perl_0.08-1.dsc 2365 SHA256:5e143ea7b74c6c96e3b0e5b1c7e88914f4bc4e0c5a70074f77f0e8f07cb6e5d1
'http://archive.ubuntu.com/ubuntu/pool/main/libf/libfile-basedir-perl/libfile-basedir-perl_0.08.orig.tar.gz' libfile-basedir-perl_0.08.orig.tar.gz 10052 SHA256:c065fcd3e2f22ae769937bcc971b91f80294d5009fac140bfba83bf7d35305e3
'http://archive.ubuntu.com/ubuntu/pool/main/libf/libfile-basedir-perl/libfile-basedir-perl_0.08-1.debian.tar.xz' libfile-basedir-perl_0.08-1.debian.tar.xz 2800 SHA256:337925c540899e9313fa79907557d24fb2dbbdb9c013390b42da4943edfc1718
```

### `dpkg` source package: `libfile-desktopentry-perl=0.22-1`

Binary Packages:

- `libfile-desktopentry-perl=0.22-1`

Licenses: (parsed from: `/usr/share/doc/libfile-desktopentry-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libfile-desktopentry-perl=0.22-1
'http://archive.ubuntu.com/ubuntu/pool/main/libf/libfile-desktopentry-perl/libfile-desktopentry-perl_0.22-1.dsc' libfile-desktopentry-perl_0.22-1.dsc 2400 SHA256:94d12f074e00b4c024af2a83691b96971a6fea19de5f18f0caf2675f9028031a
'http://archive.ubuntu.com/ubuntu/pool/main/libf/libfile-desktopentry-perl/libfile-desktopentry-perl_0.22.orig.tar.gz' libfile-desktopentry-perl_0.22.orig.tar.gz 18366 SHA256:169c01e3dae2f629767bec1a9f1cdbd6ec6d713d1501e0b2786e4dd1235635b8
'http://archive.ubuntu.com/ubuntu/pool/main/libf/libfile-desktopentry-perl/libfile-desktopentry-perl_0.22-1.debian.tar.xz' libfile-desktopentry-perl_0.22-1.debian.tar.xz 3244 SHA256:43ce8359412c3d1a01580c3f128e2071a33808f8bccd9d0a2ebc499d53bb49f2
```

### `dpkg` source package: `libfile-listing-perl=6.04-1`

Binary Packages:

- `libfile-listing-perl=6.04-1`

Licenses: (parsed from: `/usr/share/doc/libfile-listing-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libfile-listing-perl=6.04-1
'http://archive.ubuntu.com/ubuntu/pool/main/libf/libfile-listing-perl/libfile-listing-perl_6.04-1.dsc' libfile-listing-perl_6.04-1.dsc 1493 SHA256:b431527f181f34682315d62422ceac52db806bbeabfecae4cd877714d7dca2f4
'http://archive.ubuntu.com/ubuntu/pool/main/libf/libfile-listing-perl/libfile-listing-perl_6.04.orig.tar.gz' libfile-listing-perl_6.04.orig.tar.gz 51536 SHA256:1e0050fcd6789a2179ec0db282bf1e90fb92be35d1171588bd9c47d52d959cf5
'http://archive.ubuntu.com/ubuntu/pool/main/libf/libfile-listing-perl/libfile-listing-perl_6.04-1.debian.tar.gz' libfile-listing-perl_6.04-1.debian.tar.gz 1972 SHA256:f14177c3171bd6d09912ecb82e0b3a7e10fa2d17f56d8964750aefa16b5a6b7b
```

### `dpkg` source package: `libfile-mimeinfo-perl=0.29-1`

Binary Packages:

- `libfile-mimeinfo-perl=0.29-1`

Licenses: (parsed from: `/usr/share/doc/libfile-mimeinfo-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libfile-mimeinfo-perl=0.29-1
'http://archive.ubuntu.com/ubuntu/pool/main/libf/libfile-mimeinfo-perl/libfile-mimeinfo-perl_0.29-1.dsc' libfile-mimeinfo-perl_0.29-1.dsc 2492 SHA256:c9a1a72f3057b4cbe47993b063f84a4ddae34d9073875c2e5b858fb35f7f2928
'http://archive.ubuntu.com/ubuntu/pool/main/libf/libfile-mimeinfo-perl/libfile-mimeinfo-perl_0.29.orig.tar.gz' libfile-mimeinfo-perl_0.29.orig.tar.gz 33113 SHA256:f1962779652eae1d5a6e722a5220e3c50465deac52bb8dea47e0fbcfb6c908ea
'http://archive.ubuntu.com/ubuntu/pool/main/libf/libfile-mimeinfo-perl/libfile-mimeinfo-perl_0.29-1.debian.tar.xz' libfile-mimeinfo-perl_0.29-1.debian.tar.xz 4320 SHA256:3d1e9a1750f184ab982aa0107cc173ee758cb133db48089cc824183e68b74ac8
```

### `dpkg` source package: `libfont-afm-perl=1.20-2`

Binary Packages:

- `libfont-afm-perl=1.20-2`

Licenses: (parsed from: `/usr/share/doc/libfont-afm-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libfont-afm-perl=1.20-2
'http://archive.ubuntu.com/ubuntu/pool/main/libf/libfont-afm-perl/libfont-afm-perl_1.20-2.dsc' libfont-afm-perl_1.20-2.dsc 2070 SHA256:1b9c82ec407e56149f1d2d61d21d81fcaea0ee0292bd1b57bf8b415943b27735
'http://archive.ubuntu.com/ubuntu/pool/main/libf/libfont-afm-perl/libfont-afm-perl_1.20.orig.tar.gz' libfont-afm-perl_1.20.orig.tar.gz 10421 SHA256:32671166da32596a0f6baacd0c1233825a60acaf25805d79c81a3f18d6088bc1
'http://archive.ubuntu.com/ubuntu/pool/main/libf/libfont-afm-perl/libfont-afm-perl_1.20-2.debian.tar.xz' libfont-afm-perl_1.20-2.debian.tar.xz 2732 SHA256:981bb81a767cb822668840259fd349f19bc71ef0a27f22dfe87590dbc2f2f674
```

### `dpkg` source package: `libfontenc=1:1.1.4-0ubuntu1`

Binary Packages:

- `libfontenc1:amd64=1:1.1.4-0ubuntu1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libfontenc=1:1.1.4-0ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/libf/libfontenc/libfontenc_1.1.4-0ubuntu1.dsc' libfontenc_1.1.4-0ubuntu1.dsc 1666 SHA256:55b5ac947569e2926da53b0c0d9b88f838f5f0a89fed10ef186bb1a42c12b589
'http://archive.ubuntu.com/ubuntu/pool/main/libf/libfontenc/libfontenc_1.1.4.orig.tar.gz' libfontenc_1.1.4.orig.tar.gz 389706 SHA256:895ee0986b32fbfcda7f4f25ef6cbacfa760e1690bf59f02085ce0e7d1eebb41
'http://archive.ubuntu.com/ubuntu/pool/main/libf/libfontenc/libfontenc_1.1.4-0ubuntu1.diff.gz' libfontenc_1.1.4-0ubuntu1.diff.gz 8442 SHA256:89d129587ce4abaf5ac28706b6687cbf72f80abcf05313dbe58cab8eb9fb80ab
```

### `dpkg` source package: `libfreehand=0.1.2-2ubuntu1`

Binary Packages:

- `libfreehand-0.1-1=0.1.2-2ubuntu1`

Licenses: (parsed from: `/usr/share/doc/libfreehand-0.1-1/copyright`)

- `GPL-3`
- `LGPL-3`
- `MPL-1.1 | GPL-3+ | LGPL-3+`
- `MPL-2.0`

Source:

```console
$ apt-get source -qq --print-uris libfreehand=0.1.2-2ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/libf/libfreehand/libfreehand_0.1.2-2ubuntu1.dsc' libfreehand_0.1.2-2ubuntu1.dsc 1413 SHA256:3c886aed0e0df76fbea8c8c6ee884ba14e2e0f1f56c68c0c186344c546d0a6b7
'http://archive.ubuntu.com/ubuntu/pool/main/libf/libfreehand/libfreehand_0.1.2.orig.tar.xz' libfreehand_0.1.2.orig.tar.xz 516132 SHA256:0e422d1564a6dbf22a9af598535425271e583514c0f7ba7d9091676420de34ac
'http://archive.ubuntu.com/ubuntu/pool/main/libf/libfreehand/libfreehand_0.1.2-2ubuntu1.debian.tar.xz' libfreehand_0.1.2-2ubuntu1.debian.tar.xz 13824 SHA256:030f0ca11865f309acfc9ac0e856294eb0287ed7f2a089354274b20740da399f
```

### `dpkg` source package: `libgcrypt20=1.8.5-5ubuntu1.1`

Binary Packages:

- `libgcrypt20:amd64=1.8.5-5ubuntu1.1`

Licenses: (parsed from: `/usr/share/doc/libgcrypt20/copyright`)

- `GPL-2`
- `LGPL`

Source:

```console
$ apt-get source -qq --print-uris libgcrypt20=1.8.5-5ubuntu1.1
'http://archive.ubuntu.com/ubuntu/pool/main/libg/libgcrypt20/libgcrypt20_1.8.5-5ubuntu1.1.dsc' libgcrypt20_1.8.5-5ubuntu1.1.dsc 2915 SHA512:7b1cdda11632962e872b5d70b351851d95a3d5ed896f19650da618ef8ec835ed3aee54905b33f507ed16a7bae7d1ba0d5df8546712a1ee851bbed61d008250f9
'http://archive.ubuntu.com/ubuntu/pool/main/libg/libgcrypt20/libgcrypt20_1.8.5.orig.tar.bz2' libgcrypt20_1.8.5.orig.tar.bz2 2991291 SHA512:b55e16e838d1b1208e7673366971ae7c0f9c1c79e042f41c03d14ed74c5e387fa69ea81d5414ffda3d2b4f82ea5467fe13b00115727e257db22808cf351bde89
'http://archive.ubuntu.com/ubuntu/pool/main/libg/libgcrypt20/libgcrypt20_1.8.5.orig.tar.bz2.asc' libgcrypt20_1.8.5.orig.tar.bz2.asc 488 SHA512:3993c5e3f2f1714f40a9ad1a19782362c5b80c070ed8d76feacc503d8719f6775465f478098a092730e02683c665c5c91cf30e7700215aae2322be6230f207d6
'http://archive.ubuntu.com/ubuntu/pool/main/libg/libgcrypt20/libgcrypt20_1.8.5-5ubuntu1.1.debian.tar.xz' libgcrypt20_1.8.5-5ubuntu1.1.debian.tar.xz 34660 SHA512:ffcb506488147ceefe4c67c65de91f9d736d7e6a49d5ff3f04e8ec91a017a7c112c5bc46f6c71f07ff3dd565b494783cbd5b4f017f05c2a5b59f2955933d664b
```

### `dpkg` source package: `libglvnd=1.3.2-1~ubuntu0.20.04.1`

Binary Packages:

- `libegl1:amd64=1.3.2-1~ubuntu0.20.04.1`
- `libgl1:amd64=1.3.2-1~ubuntu0.20.04.1`
- `libglvnd0:amd64=1.3.2-1~ubuntu0.20.04.1`
- `libglx0:amd64=1.3.2-1~ubuntu0.20.04.1`

Licenses: (parsed from: `/usr/share/doc/libegl1/copyright`, `/usr/share/doc/libgl1/copyright`, `/usr/share/doc/libglvnd0/copyright`, `/usr/share/doc/libglx0/copyright`)

- `Apache-2.0`
- `BSD-1-clause`
- `GPL`
- `GPL-3`
- `GPL-3+`
- `MIT`
- `public-domain`

Source:

```console
$ apt-get source -qq --print-uris libglvnd=1.3.2-1~ubuntu0.20.04.1
'http://archive.ubuntu.com/ubuntu/pool/main/libg/libglvnd/libglvnd_1.3.2-1~ubuntu0.20.04.1.dsc' libglvnd_1.3.2-1~ubuntu0.20.04.1.dsc 2773 SHA512:e1581e975c7ed8d9c1315aa76e9c511265481002c77b1c30aea6e79346095865a101799832f139d0f3bc67a81432b48cb9dc1ddf338562ac8bfaf15874a197ee
'http://archive.ubuntu.com/ubuntu/pool/main/libg/libglvnd/libglvnd_1.3.2.orig.tar.gz' libglvnd_1.3.2.orig.tar.gz 1032413 SHA512:96bdf7adb62a4d20846830e5e0791885afb6ee0748e6345801d645f695eeb58cd7793f53b753f6d44dc630741eb864523dcf0393f3696c5d9774cc3ffa3b9c5b
'http://archive.ubuntu.com/ubuntu/pool/main/libg/libglvnd/libglvnd_1.3.2-1~ubuntu0.20.04.1.debian.tar.xz' libglvnd_1.3.2-1~ubuntu0.20.04.1.debian.tar.xz 21936 SHA512:46b732598d71548252d755fee227d17472763adbe9b186dcfd5f7ab3067ca417a88aeb5aee54f3cddf503bb4fd880b0f8be760069745177622943e08d6f57ca5
```

### `dpkg` source package: `libgpg-error=1.37-1`

Binary Packages:

- `libgpg-error0:amd64=1.37-1`

Licenses: (parsed from: `/usr/share/doc/libgpg-error0/copyright`)

- `BSD-3-clause`
- `GPL-3`
- `GPL-3+`
- `LGPL-2.1`
- `LGPL-2.1+`
- `g10-permissive`

Source:

```console
$ apt-get source -qq --print-uris libgpg-error=1.37-1
'http://archive.ubuntu.com/ubuntu/pool/main/libg/libgpg-error/libgpg-error_1.37-1.dsc' libgpg-error_1.37-1.dsc 2220 SHA256:e789ed6bf791c90e9ba28dc3923f54379862ca65bd286495942176dcfad5d8a7
'http://archive.ubuntu.com/ubuntu/pool/main/libg/libgpg-error/libgpg-error_1.37.orig.tar.bz2' libgpg-error_1.37.orig.tar.bz2 937282 SHA256:b32d6ff72a73cf79797f7f2d039e95e9c6f92f0c1450215410840ab62aea9763
'http://archive.ubuntu.com/ubuntu/pool/main/libg/libgpg-error/libgpg-error_1.37.orig.tar.bz2.asc' libgpg-error_1.37.orig.tar.bz2.asc 488 SHA256:394f0904c386f88e2b2db5042880a2a302cbc6e4ab902bacf3d338ded038066b
'http://archive.ubuntu.com/ubuntu/pool/main/libg/libgpg-error/libgpg-error_1.37-1.debian.tar.xz' libgpg-error_1.37-1.debian.tar.xz 17332 SHA256:09843b599726c1ab7b1fcd86ce617bd91d6378ff754c6da0b7e536ed1c3b6c16
```

### `dpkg` source package: `libgsm=1.0.18-2`

Binary Packages:

- `libgsm1:amd64=1.0.18-2`

Licenses: (parsed from: `/usr/share/doc/libgsm1/copyright`)

- `TU-Berlin-2.0`

Source:

```console
$ apt-get source -qq --print-uris libgsm=1.0.18-2
'http://archive.ubuntu.com/ubuntu/pool/universe/libg/libgsm/libgsm_1.0.18-2.dsc' libgsm_1.0.18-2.dsc 1831 SHA256:8b189db3805aaaf49073971af2e1a0dad13fdd0efd6b60c0aae687f78fd76a0a
'http://archive.ubuntu.com/ubuntu/pool/universe/libg/libgsm/libgsm_1.0.18.orig.tar.gz' libgsm_1.0.18.orig.tar.gz 64549 SHA256:04f68087c3348bf156b78d59f4d8aff545da7f6e14f33be8f47d33f4efae2a10
'http://archive.ubuntu.com/ubuntu/pool/universe/libg/libgsm/libgsm_1.0.18-2.debian.tar.xz' libgsm_1.0.18-2.debian.tar.xz 10276 SHA256:3655a18243e6e3d5706dd069347919c26edd3387d63ecf728fc6ecb242b36b43
```

### `dpkg` source package: `libgudev=233-1`

Binary Packages:

- `libgudev-1.0-0:amd64=1:233-1`

Licenses: (parsed from: `/usr/share/doc/libgudev-1.0-0/copyright`)

- `LGPL-2`
- `LGPL-2+`

Source:

```console
$ apt-get source -qq --print-uris libgudev=233-1
'http://archive.ubuntu.com/ubuntu/pool/main/libg/libgudev/libgudev_233-1.dsc' libgudev_233-1.dsc 2366 SHA256:d9d9c196a3a296cd69a5b8c091c9d78e6139afce3d374858e82c567a585bcdbf
'http://archive.ubuntu.com/ubuntu/pool/main/libg/libgudev/libgudev_233.orig.tar.xz' libgudev_233.orig.tar.xz 271292 SHA256:587c4970eb23f4e2deee2cb1fb7838c94a78c578f41ce12cac0a3f4a80dabb03
'http://archive.ubuntu.com/ubuntu/pool/main/libg/libgudev/libgudev_233-1.debian.tar.xz' libgudev_233-1.debian.tar.xz 4060 SHA256:f83ce6babcab7cfc7d7f7d850581005803006e2baaf005e05ad5490a8f661a52
```

### `dpkg` source package: `libhtml-form-perl=6.07-1`

Binary Packages:

- `libhtml-form-perl=6.07-1`

Licenses: (parsed from: `/usr/share/doc/libhtml-form-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libhtml-form-perl=6.07-1
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhtml-form-perl/libhtml-form-perl_6.07-1.dsc' libhtml-form-perl_6.07-1.dsc 2425 SHA256:7b27fc89d147d47cac5defa1f4325686d1145b5836fcc3999bf210639d6952a4
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhtml-form-perl/libhtml-form-perl_6.07.orig.tar.gz' libhtml-form-perl_6.07.orig.tar.gz 41503 SHA256:7daa8c7eaff4005501c3431c8bf478d58bbee7b836f863581aa14afe1b4b6227
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhtml-form-perl/libhtml-form-perl_6.07-1.debian.tar.xz' libhtml-form-perl_6.07-1.debian.tar.xz 2568 SHA256:388eed6ed7cb39dcb2f48eb60e0baa2c1c0e5ce9d66e82631cdd605c942a510d
```

### `dpkg` source package: `libhtml-format-perl=2.12-1`

Binary Packages:

- `libhtml-format-perl=2.12-1`

Licenses: (parsed from: `/usr/share/doc/libhtml-format-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libhtml-format-perl=2.12-1
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhtml-format-perl/libhtml-format-perl_2.12-1.dsc' libhtml-format-perl_2.12-1.dsc 2253 SHA256:85d329b257604bbbde77073ae1e8e6995a58ffa36f2db9819d880daabd03f3c6
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhtml-format-perl/libhtml-format-perl_2.12.orig.tar.gz' libhtml-format-perl_2.12.orig.tar.gz 50248 SHA256:a8f76839e46a22c64b8635b82072799caf77393d2102fba81041db6348c66899
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhtml-format-perl/libhtml-format-perl_2.12-1.debian.tar.xz' libhtml-format-perl_2.12-1.debian.tar.xz 3944 SHA256:42539c9cb4796a7cb1d68151b7573dbc3d591ab60799e236eee3f5889047b931
```

### `dpkg` source package: `libhtml-parser-perl=3.72-5`

Binary Packages:

- `libhtml-parser-perl=3.72-5`

Licenses: (parsed from: `/usr/share/doc/libhtml-parser-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libhtml-parser-perl=3.72-5
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhtml-parser-perl/libhtml-parser-perl_3.72-5.dsc' libhtml-parser-perl_3.72-5.dsc 2558 SHA256:7fe41195a16dbaefa10b583875340a72a33f60272af3cccc2154f1092f7fa4de
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhtml-parser-perl/libhtml-parser-perl_3.72.orig.tar.gz' libhtml-parser-perl_3.72.orig.tar.gz 90680 SHA256:ec28c7e1d9e67c45eca197077f7cdc41ead1bb4c538c7f02a3296a4bb92f608b
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhtml-parser-perl/libhtml-parser-perl_3.72-5.debian.tar.xz' libhtml-parser-perl_3.72-5.debian.tar.xz 9140 SHA256:81c1bb70cc5ce5ce36238d84eefa4c70d7ea787909364a559bea261c412521bd
```

### `dpkg` source package: `libhtml-tagset-perl=3.20-4`

Binary Packages:

- `libhtml-tagset-perl=3.20-4`

Licenses: (parsed from: `/usr/share/doc/libhtml-tagset-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libhtml-tagset-perl=3.20-4
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhtml-tagset-perl/libhtml-tagset-perl_3.20-4.dsc' libhtml-tagset-perl_3.20-4.dsc 2394 SHA256:b985a802342e9aeba142f1789f6502bb0c04336165ca08ef721985bcc7fb0015
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhtml-tagset-perl/libhtml-tagset-perl_3.20.orig.tar.gz' libhtml-tagset-perl_3.20.orig.tar.gz 8150 SHA256:adb17dac9e36cd011f5243881c9739417fd102fce760f8de4e9be4c7131108e2
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhtml-tagset-perl/libhtml-tagset-perl_3.20-4.debian.tar.xz' libhtml-tagset-perl_3.20-4.debian.tar.xz 3288 SHA256:f68fc45bdb0df7f8864806a98f43aa92e75533d37530ddf1c2084fd027655434
```

### `dpkg` source package: `libhtml-tree-perl=5.07-2`

Binary Packages:

- `libhtml-tree-perl=5.07-2`

Licenses: (parsed from: `/usr/share/doc/libhtml-tree-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libhtml-tree-perl=5.07-2
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhtml-tree-perl/libhtml-tree-perl_5.07-2.dsc' libhtml-tree-perl_5.07-2.dsc 2363 SHA256:ba57fd6f95628dc731a32be5d2308f0c5dca855a16a187e9191202f2dc9cb94c
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhtml-tree-perl/libhtml-tree-perl_5.07.orig.tar.gz' libhtml-tree-perl_5.07.orig.tar.gz 150477 SHA256:f0374db84731c204b86c1d5b90975fef0d30a86bd9def919343e554e31a9dbbf
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhtml-tree-perl/libhtml-tree-perl_5.07-2.debian.tar.xz' libhtml-tree-perl_5.07-2.debian.tar.xz 6184 SHA256:9c28e370d84b932ef9c978281c017d24bbd8e25ba3ee5a03cf9c9e29efd1e1ef
```

### `dpkg` source package: `libhttp-cookies-perl=6.08-1`

Binary Packages:

- `libhttp-cookies-perl=6.08-1`

Licenses: (parsed from: `/usr/share/doc/libhttp-cookies-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libhttp-cookies-perl=6.08-1
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhttp-cookies-perl/libhttp-cookies-perl_6.08-1.dsc' libhttp-cookies-perl_6.08-1.dsc 2444 SHA256:b9c4888c264a10b490e62ce9979b8f3d32684896c139cb81952b88eb29cd9eb9
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhttp-cookies-perl/libhttp-cookies-perl_6.08.orig.tar.gz' libhttp-cookies-perl_6.08.orig.tar.gz 41482 SHA256:49ebb73576eb41063c04bc079477df094496deec805ae033f3be338c23c3af59
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhttp-cookies-perl/libhttp-cookies-perl_6.08-1.debian.tar.xz' libhttp-cookies-perl_6.08-1.debian.tar.xz 2876 SHA256:2c311bcb4b8b5c196914dc1828734c151e60e03579147de434b0e6b67446e9fd
```

### `dpkg` source package: `libhttp-daemon-perl=6.06-1`

Binary Packages:

- `libhttp-daemon-perl=6.06-1`

Licenses: (parsed from: `/usr/share/doc/libhttp-daemon-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libhttp-daemon-perl=6.06-1
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhttp-daemon-perl/libhttp-daemon-perl_6.06-1.dsc' libhttp-daemon-perl_6.06-1.dsc 2676 SHA256:186124c497c85c86dc4e3c0f3a7f386c25cd57802530add88ef669706a064caa
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhttp-daemon-perl/libhttp-daemon-perl_6.06.orig.tar.gz' libhttp-daemon-perl_6.06.orig.tar.gz 45576 SHA256:fc03a161b54553f766457a4267e7066767f54ad01cacfe9a91d7caa2a0319bad
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhttp-daemon-perl/libhttp-daemon-perl_6.06-1.debian.tar.xz' libhttp-daemon-perl_6.06-1.debian.tar.xz 2932 SHA256:066a8263d5b606ab32ce1448a9f959b7890ed69046e8da797035f5f03a34363f
```

### `dpkg` source package: `libhttp-date-perl=6.05-1`

Binary Packages:

- `libhttp-date-perl=6.05-1`

Licenses: (parsed from: `/usr/share/doc/libhttp-date-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libhttp-date-perl=6.05-1
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhttp-date-perl/libhttp-date-perl_6.05-1.dsc' libhttp-date-perl_6.05-1.dsc 2156 SHA256:7276bab3333e6e2df17f43e0033f1ec5a3501a45a53b381e29e39238151c274d
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhttp-date-perl/libhttp-date-perl_6.05.orig.tar.gz' libhttp-date-perl_6.05.orig.tar.gz 29234 SHA256:cb0894bf15a27c69971da50818f87cbb505990b5a0d844597cd0edd13cc90d8e
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhttp-date-perl/libhttp-date-perl_6.05-1.debian.tar.xz' libhttp-date-perl_6.05-1.debian.tar.xz 2196 SHA256:ec511e437cb61f33c1ea3a74ee8cbe3e6a0d00b3f6894bcb0ca72568e17cf42b
```

### `dpkg` source package: `libhttp-message-perl=6.22-1`

Binary Packages:

- `libhttp-message-perl=6.22-1`

Licenses: (parsed from: `/usr/share/doc/libhttp-message-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libhttp-message-perl=6.22-1
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhttp-message-perl/libhttp-message-perl_6.22-1.dsc' libhttp-message-perl_6.22-1.dsc 2595 SHA256:3290d1c67414b127f62fef5f9c0f16fa450ad8ab349eba12f793c283de0b51ea
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhttp-message-perl/libhttp-message-perl_6.22.orig.tar.gz' libhttp-message-perl_6.22.orig.tar.gz 86328 SHA256:970efd151b81c95831d2a5f9e117f8032b63a1768cd2cd3f092ad634c85175c3
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhttp-message-perl/libhttp-message-perl_6.22-1.debian.tar.xz' libhttp-message-perl_6.22-1.debian.tar.xz 3264 SHA256:cc91ee4318feaed31fdc92e7dd6d27d96df4d66540598d79b389672fa3a4a444
```

### `dpkg` source package: `libhttp-negotiate-perl=6.01-1`

Binary Packages:

- `libhttp-negotiate-perl=6.01-1`

Licenses: (parsed from: `/usr/share/doc/libhttp-negotiate-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libhttp-negotiate-perl=6.01-1
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhttp-negotiate-perl/libhttp-negotiate-perl_6.01-1.dsc' libhttp-negotiate-perl_6.01-1.dsc 2293 SHA256:e59589c57f9f641d7d5d4f3cb843b8cb2be5979e3e00b8e6a80a0f3b2f3b1015
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhttp-negotiate-perl/libhttp-negotiate-perl_6.01.orig.tar.gz' libhttp-negotiate-perl_6.01.orig.tar.gz 8596 SHA256:1c729c1ea63100e878405cda7d66f9adfd3ed4f1d6cacaca0ee9152df728e016
'http://archive.ubuntu.com/ubuntu/pool/main/libh/libhttp-negotiate-perl/libhttp-negotiate-perl_6.01-1.debian.tar.xz' libhttp-negotiate-perl_6.01-1.debian.tar.xz 2116 SHA256:75032f80856028754228213b1453a5673367a74382c28b541ed7fd63de9d13e4
```

### `dpkg` source package: `libice=2:1.0.10-0ubuntu1`

Binary Packages:

- `libice-dev:amd64=2:1.0.10-0ubuntu1`
- `libice6:amd64=2:1.0.10-0ubuntu1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libice=2:1.0.10-0ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/libi/libice/libice_1.0.10-0ubuntu1.dsc' libice_1.0.10-0ubuntu1.dsc 1629 SHA256:51f58a0e5a5c5ea780baa3a057b61a921001831a4817da8825dbf592afccbdd6
'http://archive.ubuntu.com/ubuntu/pool/main/libi/libice/libice_1.0.10.orig.tar.gz' libice_1.0.10.orig.tar.gz 481960 SHA256:1116bc64c772fd127a0d0c0ffa2833479905e3d3d8197740b3abd5f292f22d2d
'http://archive.ubuntu.com/ubuntu/pool/main/libi/libice/libice_1.0.10-0ubuntu1.diff.gz' libice_1.0.10-0ubuntu1.diff.gz 6470 SHA256:a9187c11c1b372b0f4cb58c2fb21f780e9236fd7011bb32c4188c7b37112e8de
```

### `dpkg` source package: `libidn2=2.2.0-2`

Binary Packages:

- `libidn2-0:amd64=2.2.0-2`

Licenses: (parsed from: `/usr/share/doc/libidn2-0/copyright`)

- `GPL-2`
- `GPL-2+`
- `GPL-3`
- `GPL-3+`
- `LGPL-3`
- `LGPL-3+`
- `Unicode`

Source:

```console
$ apt-get source -qq --print-uris libidn2=2.2.0-2
'http://archive.ubuntu.com/ubuntu/pool/main/libi/libidn2/libidn2_2.2.0-2.dsc' libidn2_2.2.0-2.dsc 2436 SHA256:a5c5ece3748beaba9ce0a0b29cdab2fe9d861a965a7a96101a49f194acf759d6
'http://archive.ubuntu.com/ubuntu/pool/main/libi/libidn2/libidn2_2.2.0.orig.tar.gz' libidn2_2.2.0.orig.tar.gz 2110743 SHA256:fc734732b506d878753ec6606982bf7b936e868c25c30ddb0d83f7d7056381fe
'http://archive.ubuntu.com/ubuntu/pool/main/libi/libidn2/libidn2_2.2.0-2.debian.tar.xz' libidn2_2.2.0-2.debian.tar.xz 11184 SHA256:b38ce002d7eb1abbf2c870ac9570cd06a5087693f359b133defbf44b06f8784d
```

### `dpkg` source package: `libidn=1.33-2.2ubuntu2`

Binary Packages:

- `libidn11:amd64=1.33-2.2ubuntu2`

Licenses: (parsed from: `/usr/share/doc/libidn11/copyright`)

- `GAP`
- `GFDL-1.3`
- `GFDL-1.3+`
- `GPL-2`
- `GPL-2+`
- `GPL-3`
- `GPL-3+`
- `LGPL-2`
- `LGPL-2.1`
- `LGPL-2.1+`
- `LGPL-3`
- `LGPL-3+`

Source:

```console
$ apt-get source -qq --print-uris libidn=1.33-2.2ubuntu2
'http://archive.ubuntu.com/ubuntu/pool/main/libi/libidn/libidn_1.33-2.2ubuntu2.dsc' libidn_1.33-2.2ubuntu2.dsc 2200 SHA256:0f77a95ae991742de8989d439a518b025bd4415312559711ca3e447df8abd9ec
'http://archive.ubuntu.com/ubuntu/pool/main/libi/libidn/libidn_1.33.orig.tar.gz' libidn_1.33.orig.tar.gz 3501056 SHA256:44a7aab635bb721ceef6beecc4d49dfd19478325e1b47f3196f7d2acc4930e19
'http://archive.ubuntu.com/ubuntu/pool/main/libi/libidn/libidn_1.33-2.2ubuntu2.debian.tar.xz' libidn_1.33-2.2ubuntu2.debian.tar.xz 65936 SHA256:b856b815814396183eb80493a04caa008e96e112516cabda0716c80d5d1c11f8
```

### `dpkg` source package: `libiec61883=1.2.0-3`

Binary Packages:

- `libiec61883-0:amd64=1.2.0-3`

Licenses: (parsed from: `/usr/share/doc/libiec61883-0/copyright`)

- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris libiec61883=1.2.0-3
'http://archive.ubuntu.com/ubuntu/pool/main/libi/libiec61883/libiec61883_1.2.0-3.dsc' libiec61883_1.2.0-3.dsc 1984 SHA256:1e6c7729cd431e53c8516ae49b4c0ebc0ee255ebeccc7eb629262c7901da6a5a
'http://archive.ubuntu.com/ubuntu/pool/main/libi/libiec61883/libiec61883_1.2.0.orig.tar.gz' libiec61883_1.2.0.orig.tar.gz 339064 SHA256:7c7879c6b9add3148baea697dfbfdcefffbc8ac74e8e6bcf46125ec1d21b373a
'http://archive.ubuntu.com/ubuntu/pool/main/libi/libiec61883/libiec61883_1.2.0-3.debian.tar.xz' libiec61883_1.2.0-3.debian.tar.xz 12800 SHA256:baf4b8031737c2030a1291e1197adff98215b85d830aecb36153034d758d4c39
```

### `dpkg` source package: `libio-html-perl=1.001-1`

Binary Packages:

- `libio-html-perl=1.001-1`

Licenses: (parsed from: `/usr/share/doc/libio-html-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`
- `GPL-3`
- `GPL-3+`

Source:

```console
$ apt-get source -qq --print-uris libio-html-perl=1.001-1
'http://archive.ubuntu.com/ubuntu/pool/main/libi/libio-html-perl/libio-html-perl_1.001-1.dsc' libio-html-perl_1.001-1.dsc 2143 SHA256:d9065afdd12b5e0c534938f17a846e9fec028457053d4a064f7f95ebb68f2e5c
'http://archive.ubuntu.com/ubuntu/pool/main/libi/libio-html-perl/libio-html-perl_1.001.orig.tar.gz' libio-html-perl_1.001.orig.tar.gz 19375 SHA256:ea78d2d743794adc028bc9589538eb867174b4e165d7d8b5f63486e6b828e7e0
'http://archive.ubuntu.com/ubuntu/pool/main/libi/libio-html-perl/libio-html-perl_1.001-1.debian.tar.xz' libio-html-perl_1.001-1.debian.tar.xz 3124 SHA256:89c86132e54f7f967b4f31eb42ff922eb4017c60e45ba2264e42ba620146f844
```

### `dpkg` source package: `libio-socket-ssl-perl=2.067-1`

Binary Packages:

- `libio-socket-ssl-perl=2.067-1`

Licenses: (parsed from: `/usr/share/doc/libio-socket-ssl-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libio-socket-ssl-perl=2.067-1
'http://archive.ubuntu.com/ubuntu/pool/main/libi/libio-socket-ssl-perl/libio-socket-ssl-perl_2.067-1.dsc' libio-socket-ssl-perl_2.067-1.dsc 2725 SHA256:a62a2f38550c94f216e4520b37c0875b6c06a125a95ea2c09e168b1c548880ef
'http://archive.ubuntu.com/ubuntu/pool/main/libi/libio-socket-ssl-perl/libio-socket-ssl-perl_2.067.orig.tar.gz' libio-socket-ssl-perl_2.067.orig.tar.gz 244899 SHA256:ef8842806d047cf56e2af64add4ed30b24547bcbb50e8df33cee0c54609af1c1
'http://archive.ubuntu.com/ubuntu/pool/main/libi/libio-socket-ssl-perl/libio-socket-ssl-perl_2.067-1.debian.tar.xz' libio-socket-ssl-perl_2.067-1.debian.tar.xz 10268 SHA256:b8f2ece82be07235eb4347a86c41f0bfe1c37385648611658bde5863cd18ced5
```

### `dpkg` source package: `libipc-system-simple-perl=1.26-1`

Binary Packages:

- `libipc-system-simple-perl=1.26-1`

Licenses: (parsed from: `/usr/share/doc/libipc-system-simple-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libipc-system-simple-perl=1.26-1
'http://archive.ubuntu.com/ubuntu/pool/main/libi/libipc-system-simple-perl/libipc-system-simple-perl_1.26-1.dsc' libipc-system-simple-perl_1.26-1.dsc 2515 SHA256:d817622b20f8ce7c4cb9ebfd7b6069989c73bc83cfa04a9906c0f4a401cbe3e3
'http://archive.ubuntu.com/ubuntu/pool/main/libi/libipc-system-simple-perl/libipc-system-simple-perl_1.26.orig.tar.gz' libipc-system-simple-perl_1.26.orig.tar.gz 29989 SHA256:57177f21d8e8625bba32ea454f10a1fda16f93c1baf1aa80d106ab1951b465fd
'http://archive.ubuntu.com/ubuntu/pool/main/libi/libipc-system-simple-perl/libipc-system-simple-perl_1.26-1.debian.tar.xz' libipc-system-simple-perl_1.26-1.debian.tar.xz 3304 SHA256:057db1c853f431de5ce244131cd46043b8409e703baef9568ddb978fd2f84674
```

### `dpkg` source package: `libjpeg-turbo=2.0.3-0ubuntu1.20.04.1`

Binary Packages:

- `libjpeg-turbo8:amd64=2.0.3-0ubuntu1.20.04.1`

Licenses: (parsed from: `/usr/share/doc/libjpeg-turbo8/copyright`)

- `JPEG`
- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris libjpeg-turbo=2.0.3-0ubuntu1.20.04.1
'http://archive.ubuntu.com/ubuntu/pool/main/libj/libjpeg-turbo/libjpeg-turbo_2.0.3-0ubuntu1.20.04.1.dsc' libjpeg-turbo_2.0.3-0ubuntu1.20.04.1.dsc 2337 SHA512:e1d8abdd94178045b38edb0d29909199c5e57cb4692fc90a0248a60d19b0d900896c0e16f5938f7abe697e9834b7bb805bfe38ee7f32e99f46585c44931a5c4c
'http://archive.ubuntu.com/ubuntu/pool/main/libj/libjpeg-turbo/libjpeg-turbo_2.0.3.orig.tar.gz' libjpeg-turbo_2.0.3.orig.tar.gz 2161279 SHA512:745cc3d50b43dd84721bc3c341d561ffd7f54eda5bbe2d56cad62f4b51ea76da3b18aba9ca694a9db79379aba7a9971cb146387979e96ca6ece950871276cf2f
'http://archive.ubuntu.com/ubuntu/pool/main/libj/libjpeg-turbo/libjpeg-turbo_2.0.3-0ubuntu1.20.04.1.debian.tar.xz' libjpeg-turbo_2.0.3-0ubuntu1.20.04.1.debian.tar.xz 18228 SHA512:d814ebc94f8f2c6d3af415f9a983050bf512dc181bb52171bb170835c5d1338de676fe1e7593ab963aa1ebb291089024c3af2b1390c6731163cf0ce76575b3af
```

### `dpkg` source package: `libjpeg8-empty=8c-2ubuntu8`

Binary Packages:

- `libjpeg8:amd64=8c-2ubuntu8`

Licenses: (parsed from: `/usr/share/doc/libjpeg8/copyright`)

- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris libjpeg8-empty=8c-2ubuntu8
'http://archive.ubuntu.com/ubuntu/pool/main/libj/libjpeg8-empty/libjpeg8-empty_8c-2ubuntu8.dsc' libjpeg8-empty_8c-2ubuntu8.dsc 1637 SHA256:e7f575dcb3e0d462513b6f928179baa0ff1d145273934b1041b714515096b407
'http://archive.ubuntu.com/ubuntu/pool/main/libj/libjpeg8-empty/libjpeg8-empty_8c-2ubuntu8.tar.gz' libjpeg8-empty_8c-2ubuntu8.tar.gz 1770 SHA256:48a4227e9fc70851a4f304b10624e02875bf6f4e2debfcbe4ba0dd85a3ec05c6
```

### `dpkg` source package: `libksba=1.3.5-2`

Binary Packages:

- `libksba8:amd64=1.3.5-2`

Licenses: (parsed from: `/usr/share/doc/libksba8/copyright`)

- `GPL-3`

Source:

```console
$ apt-get source -qq --print-uris libksba=1.3.5-2
'http://archive.ubuntu.com/ubuntu/pool/main/libk/libksba/libksba_1.3.5-2.dsc' libksba_1.3.5-2.dsc 2526 SHA256:4fd08fd129f97ab1df86c220b88b7b2c6e4e04aa90bfd3ae364d18022256bef8
'http://archive.ubuntu.com/ubuntu/pool/main/libk/libksba/libksba_1.3.5.orig.tar.bz2' libksba_1.3.5.orig.tar.bz2 620649 SHA256:41444fd7a6ff73a79ad9728f985e71c9ba8cd3e5e53358e70d5f066d35c1a340
'http://archive.ubuntu.com/ubuntu/pool/main/libk/libksba/libksba_1.3.5.orig.tar.bz2.asc' libksba_1.3.5.orig.tar.bz2.asc 287 SHA256:a954b03144ee882c838853da24fd7b6868b78df72a18c71079217d968698a76f
'http://archive.ubuntu.com/ubuntu/pool/main/libk/libksba/libksba_1.3.5-2.debian.tar.xz' libksba_1.3.5-2.debian.tar.xz 13852 SHA256:98c985bff973be1aecc702fa15887ff1e5b8de481d1dc3e99423a587754eaabd
```

### `dpkg` source package: `liblangtag=0.6.3-1`

Binary Packages:

- `liblangtag-common=0.6.3-1`
- `liblangtag1:amd64=0.6.3-1`

Licenses: (parsed from: `/usr/share/doc/liblangtag-common/copyright`, `/usr/share/doc/liblangtag1/copyright`)

- `GPL-2`
- `GPL-2+`
- `LGPL | MPL`

Source:

```console
$ apt-get source -qq --print-uris liblangtag=0.6.3-1
'http://archive.ubuntu.com/ubuntu/pool/main/libl/liblangtag/liblangtag_0.6.3-1.dsc' liblangtag_0.6.3-1.dsc 2409 SHA256:c0dca4af195dd5972990b3689172fb0c5565b2ebd69083e25bbe0be3250ea331
'http://archive.ubuntu.com/ubuntu/pool/main/libl/liblangtag/liblangtag_0.6.3.orig.tar.bz2' liblangtag_0.6.3.orig.tar.bz2 755492 SHA256:1f12a20a02ec3a8d22e54dedb8b683a43c9c160bda1ba337bf1060607ae733bd
'http://archive.ubuntu.com/ubuntu/pool/main/libl/liblangtag/liblangtag_0.6.3-1.debian.tar.xz' liblangtag_0.6.3-1.debian.tar.xz 6352 SHA256:8e71e75220bb7a4006ac8e8c3c1413871a29a87afcd006d45ddcd5acd79a5d9e
```

### `dpkg` source package: `liblqr=0.4.2-2.1`

Binary Packages:

- `liblqr-1-0:amd64=0.4.2-2.1`

Licenses: (parsed from: `/usr/share/doc/liblqr-1-0/copyright`)

- `GPL-3`
- `GPLv3`
- `LGPL-3`

Source:

```console
$ apt-get source -qq --print-uris liblqr=0.4.2-2.1
'http://archive.ubuntu.com/ubuntu/pool/universe/libl/liblqr/liblqr_0.4.2-2.1.dsc' liblqr_0.4.2-2.1.dsc 2095 SHA256:c54c34cd2f7470a29366eeacde2ca4859a97d684a406fb81a918b970c01d617c
'http://archive.ubuntu.com/ubuntu/pool/universe/libl/liblqr/liblqr_0.4.2.orig.tar.gz' liblqr_0.4.2.orig.tar.gz 439884 SHA256:d4c22373432cca749e4326cd41fce365e6ff857c0bfd7a5302b8eb34b69f0336
'http://archive.ubuntu.com/ubuntu/pool/universe/libl/liblqr/liblqr_0.4.2-2.1.debian.tar.xz' liblqr_0.4.2-2.1.debian.tar.xz 5300 SHA256:284a002f1ecac63ac17b1aafbb230da9ce7bd9efe2d5b94e8cad49b607eb2564
```

### `dpkg` source package: `liblwp-mediatypes-perl=6.04-1`

Binary Packages:

- `liblwp-mediatypes-perl=6.04-1`

Licenses: (parsed from: `/usr/share/doc/liblwp-mediatypes-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris liblwp-mediatypes-perl=6.04-1
'http://archive.ubuntu.com/ubuntu/pool/main/libl/liblwp-mediatypes-perl/liblwp-mediatypes-perl_6.04-1.dsc' liblwp-mediatypes-perl_6.04-1.dsc 2167 SHA256:3bd2934d4bbb99bfa80c666711f484d15cd4266a67339f26f5056eb23669c55a
'http://archive.ubuntu.com/ubuntu/pool/main/libl/liblwp-mediatypes-perl/liblwp-mediatypes-perl_6.04.orig.tar.gz' liblwp-mediatypes-perl_6.04.orig.tar.gz 40000 SHA256:8f1bca12dab16a1c2a7c03a49c5e58cce41a6fec9519f0aadfba8dad997919d9
'http://archive.ubuntu.com/ubuntu/pool/main/libl/liblwp-mediatypes-perl/liblwp-mediatypes-perl_6.04-1.debian.tar.xz' liblwp-mediatypes-perl_6.04-1.debian.tar.xz 2244 SHA256:48590b8cf59c79d9ac1f7a735a725ae1099c4f9cf43a3817e994bb18d811ac48
```

### `dpkg` source package: `liblwp-protocol-https-perl=6.07-2ubuntu2`

Binary Packages:

- `liblwp-protocol-https-perl=6.07-2ubuntu2`

Licenses: (parsed from: `/usr/share/doc/liblwp-protocol-https-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris liblwp-protocol-https-perl=6.07-2ubuntu2
'http://archive.ubuntu.com/ubuntu/pool/main/libl/liblwp-protocol-https-perl/liblwp-protocol-https-perl_6.07-2ubuntu2.dsc' liblwp-protocol-https-perl_6.07-2ubuntu2.dsc 2497 SHA256:a8dd64d7b93b338ca28e1079ecc2f3f51e23447a86372fb934d2ee4084dd15b2
'http://archive.ubuntu.com/ubuntu/pool/main/libl/liblwp-protocol-https-perl/liblwp-protocol-https-perl_6.07.orig.tar.gz' liblwp-protocol-https-perl_6.07.orig.tar.gz 9184 SHA256:522cc946cf84a1776304a5737a54b8822ec9e79b264d0ba0722a70473dbfb9e7
'http://archive.ubuntu.com/ubuntu/pool/main/libl/liblwp-protocol-https-perl/liblwp-protocol-https-perl_6.07-2ubuntu2.debian.tar.xz' liblwp-protocol-https-perl_6.07-2ubuntu2.debian.tar.xz 4416 SHA256:f59cde43caa5667c21df86a9572ed6d3e9f0e5b007f4803173f3b8a60937c1a4
```

### `dpkg` source package: `libmailtools-perl=2.21-1`

Binary Packages:

- `libmailtools-perl=2.21-1`

Licenses: (parsed from: `/usr/share/doc/libmailtools-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libmailtools-perl=2.21-1
'http://archive.ubuntu.com/ubuntu/pool/main/libm/libmailtools-perl/libmailtools-perl_2.21-1.dsc' libmailtools-perl_2.21-1.dsc 2326 SHA256:ff030ed490005c751f275d9af4e1ad98d509ab7bfe573efa32f6db1006483bf0
'http://archive.ubuntu.com/ubuntu/pool/main/libm/libmailtools-perl/libmailtools-perl_2.21.orig.tar.gz' libmailtools-perl_2.21.orig.tar.gz 58013 SHA256:4ad9bd6826b6f03a2727332466b1b7d29890c8d99a32b4b3b0a8d926ee1a44cb
'http://archive.ubuntu.com/ubuntu/pool/main/libm/libmailtools-perl/libmailtools-perl_2.21-1.debian.tar.xz' libmailtools-perl_2.21-1.debian.tar.xz 6808 SHA256:6023c5ba5e1b7d304f2f42d0d6381d842662e6a11ff37f06e5ff88456aae22d3
```

### `dpkg` source package: `libmspub=0.1.4-1build3`

Binary Packages:

- `libmspub-0.1-1:amd64=0.1.4-1build3`

Licenses: (parsed from: `/usr/share/doc/libmspub-0.1-1/copyright`)

- `MPL-2.0`

Source:

```console
$ apt-get source -qq --print-uris libmspub=0.1.4-1build3
'http://archive.ubuntu.com/ubuntu/pool/main/libm/libmspub/libmspub_0.1.4-1build3.dsc' libmspub_0.1.4-1build3.dsc 2154 SHA256:17cc3b067d0b16e292c267caf3610f95a9751aaaafb998d9f5311f573ebcd67d
'http://archive.ubuntu.com/ubuntu/pool/main/libm/libmspub/libmspub_0.1.4.orig.tar.xz' libmspub_0.1.4.orig.tar.xz 377472 SHA256:ef36c1a1aabb2ba3b0bedaaafe717bf4480be2ba8de6f3894be5fd3702b013ba
'http://archive.ubuntu.com/ubuntu/pool/main/libm/libmspub/libmspub_0.1.4-1build3.debian.tar.xz' libmspub_0.1.4-1build3.debian.tar.xz 7264 SHA256:64ab5ad15ebb9366290cff2704362535bce85336344ec8e936f41f3b17390f1d
```

### `dpkg` source package: `libmwaw=0.3.15-2build1`

Binary Packages:

- `libmwaw-0.3-3:amd64=0.3.15-2build1`

Licenses: (parsed from: `/usr/share/doc/libmwaw-0.3-3/copyright`)

- `BSD`
- `LGPL`
- `MPL2.0 | LGPL-2+`

Source:

```console
$ apt-get source -qq --print-uris libmwaw=0.3.15-2build1
'http://archive.ubuntu.com/ubuntu/pool/main/libm/libmwaw/libmwaw_0.3.15-2build1.dsc' libmwaw_0.3.15-2build1.dsc 2121 SHA256:776f7aab6ff69fdf322857af187a4912843395e8a3bfde7b64cc7ee8b2a0133b
'http://archive.ubuntu.com/ubuntu/pool/main/libm/libmwaw/libmwaw_0.3.15.orig.tar.xz' libmwaw_0.3.15.orig.tar.xz 1289744 SHA256:0440bb09f05e3419423d8dfa36ee847056ebfd837f9cbc091fdb5b057daab0b1
'http://archive.ubuntu.com/ubuntu/pool/main/libm/libmwaw/libmwaw_0.3.15-2build1.debian.tar.xz' libmwaw_0.3.15-2build1.debian.tar.xz 8276 SHA256:3b190346318d6296a9c9974109edc5d4036e3356be5ecfc59bfaa9372d701699
```

### `dpkg` source package: `libmysofa=1.0~dfsg0-1`

Binary Packages:

- `libmysofa1:amd64=1.0~dfsg0-1`

Licenses: (parsed from: `/usr/share/doc/libmysofa1/copyright`)

- `BSD-3-clause`
- `CC-BY-4.0`
- `CC-BY-SA-3.0`
- `cipic`
- `listen-ircam`
- `mit-kemar`

Source:

```console
$ apt-get source -qq --print-uris libmysofa=1.0~dfsg0-1
'http://archive.ubuntu.com/ubuntu/pool/universe/libm/libmysofa/libmysofa_1.0~dfsg0-1.dsc' libmysofa_1.0~dfsg0-1.dsc 2318 SHA256:2e4ae110a3ecb90ddb34224345ee5ddfc890649ffb42027dc4470090a7a58d83
'http://archive.ubuntu.com/ubuntu/pool/universe/libm/libmysofa/libmysofa_1.0~dfsg0.orig.tar.xz' libmysofa_1.0~dfsg0.orig.tar.xz 44282004 SHA256:7728b958826f36ee4b17a505426881d6ef2ecc1e79a8feaefb2ead309e90f63c
'http://archive.ubuntu.com/ubuntu/pool/universe/libm/libmysofa/libmysofa_1.0~dfsg0-1.debian.tar.xz' libmysofa_1.0~dfsg0-1.debian.tar.xz 15292 SHA256:9871ba69864f753e5ad2c69cef1236c6043213a24fc8e6259819c0d46c16d6fe
```

### `dpkg` source package: `libnet-dbus-perl=1.2.0-1`

Binary Packages:

- `libnet-dbus-perl=1.2.0-1`

Licenses: (parsed from: `/usr/share/doc/libnet-dbus-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libnet-dbus-perl=1.2.0-1
'http://archive.ubuntu.com/ubuntu/pool/main/libn/libnet-dbus-perl/libnet-dbus-perl_1.2.0-1.dsc' libnet-dbus-perl_1.2.0-1.dsc 2482 SHA256:fa8060493e2efc732582e8606c96e142eb41eb01f212d09721002021441a9fce
'http://archive.ubuntu.com/ubuntu/pool/main/libn/libnet-dbus-perl/libnet-dbus-perl_1.2.0.orig.tar.gz' libnet-dbus-perl_1.2.0.orig.tar.gz 108296 SHA256:e7a1ac9ef4a1235b3fdbd5888f86c347182306467bd79abc9b0756a64b441cbc
'http://archive.ubuntu.com/ubuntu/pool/main/libn/libnet-dbus-perl/libnet-dbus-perl_1.2.0-1.debian.tar.xz' libnet-dbus-perl_1.2.0-1.debian.tar.xz 3792 SHA256:2ee97c31b9b9ea8013fb44ccd88edf70940555d7aac696e1011201841a39071a
```

### `dpkg` source package: `libnet-http-perl=6.19-1`

Binary Packages:

- `libnet-http-perl=6.19-1`

Licenses: (parsed from: `/usr/share/doc/libnet-http-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libnet-http-perl=6.19-1
'http://archive.ubuntu.com/ubuntu/pool/main/libn/libnet-http-perl/libnet-http-perl_6.19-1.dsc' libnet-http-perl_6.19-1.dsc 2134 SHA256:216a35aeb08df6bf856d622fa7596e17ad38aa2c3ed468db6fcce56d46c509b6
'http://archive.ubuntu.com/ubuntu/pool/main/libn/libnet-http-perl/libnet-http-perl_6.19.orig.tar.gz' libnet-http-perl_6.19.orig.tar.gz 36937 SHA256:52b76ec13959522cae64d965f15da3d99dcb445eddd85d2ce4e4f4df385b2fc4
'http://archive.ubuntu.com/ubuntu/pool/main/libn/libnet-http-perl/libnet-http-perl_6.19-1.debian.tar.xz' libnet-http-perl_6.19-1.debian.tar.xz 3468 SHA256:e54277bfa8e102b2fea9e3f39ff64748d4877813269da864a5978339a136f446
```

### `dpkg` source package: `libnet-smtp-ssl-perl=1.04-1`

Binary Packages:

- `libnet-smtp-ssl-perl=1.04-1`

Licenses: (parsed from: `/usr/share/doc/libnet-smtp-ssl-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libnet-smtp-ssl-perl=1.04-1
'http://archive.ubuntu.com/ubuntu/pool/main/libn/libnet-smtp-ssl-perl/libnet-smtp-ssl-perl_1.04-1.dsc' libnet-smtp-ssl-perl_1.04-1.dsc 2268 SHA256:9429f4671338f756fc68b344cbf3a92a8624e71af68471c841c284ae32907b70
'http://archive.ubuntu.com/ubuntu/pool/main/libn/libnet-smtp-ssl-perl/libnet-smtp-ssl-perl_1.04.orig.tar.gz' libnet-smtp-ssl-perl_1.04.orig.tar.gz 2457 SHA256:7b29c45add19d3d5084b751f7ba89a8e40479a446ce21cfd9cc741e558332a00
'http://archive.ubuntu.com/ubuntu/pool/main/libn/libnet-smtp-ssl-perl/libnet-smtp-ssl-perl_1.04-1.debian.tar.xz' libnet-smtp-ssl-perl_1.04-1.debian.tar.xz 2696 SHA256:5826b6569145f52a575084a6f8da3601b361e4a2940218149b1591146f9499c6
```

### `dpkg` source package: `libnet-ssleay-perl=1.88-2ubuntu1`

Binary Packages:

- `libnet-ssleay-perl=1.88-2ubuntu1`

Licenses: (parsed from: `/usr/share/doc/libnet-ssleay-perl/copyright`)

- `Artistic`
- `Artistic-2.0`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libnet-ssleay-perl=1.88-2ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/libn/libnet-ssleay-perl/libnet-ssleay-perl_1.88-2ubuntu1.dsc' libnet-ssleay-perl_1.88-2ubuntu1.dsc 2421 SHA256:6f5ba55727facfc4256296757444b53340112c85e92d5a0c83f4db606b939e7b
'http://archive.ubuntu.com/ubuntu/pool/main/libn/libnet-ssleay-perl/libnet-ssleay-perl_1.88.orig.tar.xz' libnet-ssleay-perl_1.88.orig.tar.xz 343508 SHA256:43db8c0c60b5ecd5dd7cb40cc6dcfe57de18ce246734e24d86b21436661983f3
'http://archive.ubuntu.com/ubuntu/pool/main/libn/libnet-ssleay-perl/libnet-ssleay-perl_1.88-2ubuntu1.debian.tar.xz' libnet-ssleay-perl_1.88-2ubuntu1.debian.tar.xz 12072 SHA256:45d729b9497ff46fa6ac3cd744377bc38809f75e00043d0dea804ec7faeaae06
```

### `dpkg` source package: `libodfgen=0.1.7-1ubuntu2`

Binary Packages:

- `libodfgen-0.1-1:amd64=0.1.7-1ubuntu2`

Licenses: (parsed from: `/usr/share/doc/libodfgen-0.1-1/copyright`)

- `LGPL`
- `MPL-2.0 | LGPL-2+`

Source:

```console
$ apt-get source -qq --print-uris libodfgen=0.1.7-1ubuntu2
'http://archive.ubuntu.com/ubuntu/pool/main/libo/libodfgen/libodfgen_0.1.7-1ubuntu2.dsc' libodfgen_0.1.7-1ubuntu2.dsc 1989 SHA256:ff0e105c47b9d653ce72e5e351dcb3899c733eca62afa7e4b188ea8ffda6451e
'http://archive.ubuntu.com/ubuntu/pool/main/libo/libodfgen/libodfgen_0.1.7.orig.tar.xz' libodfgen_0.1.7.orig.tar.xz 384760 SHA256:323e491f956c8ca2abb12c998e350670930a32317bf9662b0615dd4b3922b831
'http://archive.ubuntu.com/ubuntu/pool/main/libo/libodfgen/libodfgen_0.1.7-1ubuntu2.debian.tar.xz' libodfgen_0.1.7-1ubuntu2.debian.tar.xz 7044 SHA256:5a8615a9def13ee730ecbf7d887c2152275ef5f1a685723928b48bfd9b328421
```

### `dpkg` source package: `libogg=1.3.4-0ubuntu1`

Binary Packages:

- `libogg0:amd64=1.3.4-0ubuntu1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libogg=1.3.4-0ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/libo/libogg/libogg_1.3.4-0ubuntu1.dsc' libogg_1.3.4-0ubuntu1.dsc 1089 SHA256:e223bf149636be0a870768c0e1ecdb178d144dd424a9ebe1899f3ac8979f321f
'http://archive.ubuntu.com/ubuntu/pool/main/libo/libogg/libogg_1.3.4-0ubuntu1.tar.gz' libogg_1.3.4-0ubuntu1.tar.gz 601888 SHA256:4edf841124d651c65fbacf38507d5b7001481b3c084d5d9050f19c2d35b1ee81
```

### `dpkg` source package: `libopenmpt=0.4.11-1build1`

Binary Packages:

- `libopenmpt0:amd64=0.4.11-1build1`

Licenses: (parsed from: `/usr/share/doc/libopenmpt0/copyright`)

- `BSD-3-clause`
- `GNU-All-Permissive-License`
- `GNU-All-Permissive-License-FSF`
- `GPL-2`
- `GPL-2+ with Autoconf exception`
- `GPL-2+ with LibTool exception`
- `GPL-3`
- `GPL-3+ with AutoConf exception`
- `GPL-3+ with Autoconf Macros exception`
- `X11`

Source:

```console
$ apt-get source -qq --print-uris libopenmpt=0.4.11-1build1
'http://archive.ubuntu.com/ubuntu/pool/universe/libo/libopenmpt/libopenmpt_0.4.11-1build1.dsc' libopenmpt_0.4.11-1build1.dsc 2619 SHA256:c7a9925957127f8d63bc06f7a19dd6095d687340ebad20b421aa259dbafb7c36
'http://archive.ubuntu.com/ubuntu/pool/universe/libo/libopenmpt/libopenmpt_0.4.11.orig.tar.gz' libopenmpt_0.4.11.orig.tar.gz 1471760 SHA256:260e92cc2f6af37113442bff2c75a3c36a09eba4078dc593203a0502f95d26bd
'http://archive.ubuntu.com/ubuntu/pool/universe/libo/libopenmpt/libopenmpt_0.4.11-1build1.debian.tar.xz' libopenmpt_0.4.11-1build1.debian.tar.xz 13116 SHA256:cd4ea753ecc9890cc2ce36bf3d6d5ca4323162275af7623909b8390f4d8f973c
```

### `dpkg` source package: `liborcus=0.15.3-3build2`

Binary Packages:

- `liborcus-0.15-0:amd64=0.15.3-3build2`

Licenses: (parsed from: `/usr/share/doc/liborcus-0.15-0/copyright`)

- `Expat`
- `GPL-2`
- `GPL-2+`
- `GPL-3+`
- `MIT`
- `MPL-2.0`
- `other`

Source:

```console
$ apt-get source -qq --print-uris liborcus=0.15.3-3build2
'http://archive.ubuntu.com/ubuntu/pool/main/libo/liborcus/liborcus_0.15.3-3build2.dsc' liborcus_0.15.3-3build2.dsc 2842 SHA256:ad4431a8fa578bbdcfaf13808cef1518b04f5e81ef5392cbe756f20b1fe16dbe
'http://archive.ubuntu.com/ubuntu/pool/main/libo/liborcus/liborcus_0.15.3.orig.tar.xz' liborcus_0.15.3.orig.tar.xz 1988748 SHA256:3a6d50127937f2347b145c5ca3e9cf9401421c09e004d535a94ed8b235b6eb91
'http://archive.ubuntu.com/ubuntu/pool/main/libo/liborcus/liborcus_0.15.3-3build2.debian.tar.xz' liborcus_0.15.3-3build2.debian.tar.xz 12524 SHA256:5ce82f625fdedd325e5465ba6b4b488144093ed716662b1f3a8375eb85da5034
```

### `dpkg` source package: `libpagemaker=0.0.4-1build1`

Binary Packages:

- `libpagemaker-0.0-0:amd64=0.0.4-1build1`

Licenses: (parsed from: `/usr/share/doc/libpagemaker-0.0-0/copyright`)

- `MPL-2.0`

Source:

```console
$ apt-get source -qq --print-uris libpagemaker=0.0.4-1build1
'http://archive.ubuntu.com/ubuntu/pool/main/libp/libpagemaker/libpagemaker_0.0.4-1build1.dsc' libpagemaker_0.0.4-1build1.dsc 2057 SHA256:47e960e567fca3a8514d3e3c543faa13d5633f55003ee69c7410559b32c827e0
'http://archive.ubuntu.com/ubuntu/pool/main/libp/libpagemaker/libpagemaker_0.0.4.orig.tar.xz' libpagemaker_0.0.4.orig.tar.xz 306496 SHA256:66adacd705a7d19895e08eac46d1e851332adf2e736c566bef1164e7a442519d
'http://archive.ubuntu.com/ubuntu/pool/main/libp/libpagemaker/libpagemaker_0.0.4-1build1.debian.tar.xz' libpagemaker_0.0.4-1build1.debian.tar.xz 6708 SHA256:b60d1571012ead641b4589f6d50f4c175647e74db6da31ed2a2791b7fd06db45
```

### `dpkg` source package: `libpaper=1.1.28`

Binary Packages:

- `libpaper-utils=1.1.28`
- `libpaper1:amd64=1.1.28`

Licenses: (parsed from: `/usr/share/doc/libpaper-utils/copyright`, `/usr/share/doc/libpaper1/copyright`)

- `GPL-2`

Source:

```console
$ apt-get source -qq --print-uris libpaper=1.1.28
'http://archive.ubuntu.com/ubuntu/pool/main/libp/libpaper/libpaper_1.1.28.dsc' libpaper_1.1.28.dsc 1633 SHA256:298d6347d84ece2f55088e371facc13362c8f4731d80f94c6ad84190309de8b4
'http://archive.ubuntu.com/ubuntu/pool/main/libp/libpaper/libpaper_1.1.28.tar.gz' libpaper_1.1.28.tar.gz 42356 SHA256:c8bb946ec93d3c2c72bbb1d7257e90172a22a44a07a07fb6b802a5bb2c95fddc
```

### `dpkg` source package: `libpciaccess=0.16-0ubuntu1`

Binary Packages:

- `libpciaccess0:amd64=0.16-0ubuntu1`

Licenses: (parsed from: `/usr/share/doc/libpciaccess0/copyright`)

- `GPL`

Source:

```console
$ apt-get source -qq --print-uris libpciaccess=0.16-0ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/libp/libpciaccess/libpciaccess_0.16-0ubuntu1.dsc' libpciaccess_0.16-0ubuntu1.dsc 1554 SHA256:7a69bc588b31fddaced85ec551fa75b92a8654c1da090326fba0571ca74c9129
'http://archive.ubuntu.com/ubuntu/pool/main/libp/libpciaccess/libpciaccess_0.16.orig.tar.gz' libpciaccess_0.16.orig.tar.gz 470061 SHA256:84413553994aef0070cf420050aa5c0a51b1956b404920e21b81e96db6a61a27
'http://archive.ubuntu.com/ubuntu/pool/main/libp/libpciaccess/libpciaccess_0.16-0ubuntu1.diff.gz' libpciaccess_0.16-0ubuntu1.diff.gz 25561 SHA256:d5beb9c0d60fa77c3c9ec90d188bb14875d64e0be5644716d1e615caa93d9461
```

### `dpkg` source package: `libpgm=5.2.122~dfsg-3ubuntu1`

Binary Packages:

- `libpgm-5.2-0:amd64=5.2.122~dfsg-3ubuntu1`

Licenses: (parsed from: `/usr/share/doc/libpgm-5.2-0/copyright`)

- `BSD-3-clause`
- `ISC`
- `LGPL-2`
- `LGPL-2+`
- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris libpgm=5.2.122~dfsg-3ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/universe/libp/libpgm/libpgm_5.2.122~dfsg-3ubuntu1.dsc' libpgm_5.2.122~dfsg-3ubuntu1.dsc 1875 SHA256:148f9d50cdd3c60c237face64cf6a6697e7a8f475a468227175320f025256654
'http://archive.ubuntu.com/ubuntu/pool/universe/libp/libpgm/libpgm_5.2.122~dfsg.orig.tar.xz' libpgm_5.2.122~dfsg.orig.tar.xz 550996 SHA256:d6e5ec0918216d4e9b14459f5742f6f8416df965f03ac4d854bd5d111709b507
'http://archive.ubuntu.com/ubuntu/pool/universe/libp/libpgm/libpgm_5.2.122~dfsg-3ubuntu1.debian.tar.xz' libpgm_5.2.122~dfsg-3ubuntu1.debian.tar.xz 7420 SHA256:fcb1ace182328e64ba48ca2086b5a9f5bd23a515d9c79174346b27d6fc8fd363
```

### `dpkg` source package: `libpng1.6=1.6.37-2`

Binary Packages:

- `libpng16-16:amd64=1.6.37-2`

Licenses: (parsed from: `/usr/share/doc/libpng16-16/copyright`)

- `Apache-2.0`
- `BSD-3-clause`
- `BSD-like-with-advertising-clause`
- `GPL-2`
- `GPL-2+`
- `expat`
- `libpng`
- `libpng OR Apache-2.0 OR BSD-3-clause`

Source:

```console
$ apt-get source -qq --print-uris libpng1.6=1.6.37-2
'http://archive.ubuntu.com/ubuntu/pool/main/libp/libpng1.6/libpng1.6_1.6.37-2.dsc' libpng1.6_1.6.37-2.dsc 2225 SHA256:4567a54b5804e068e61477e9cd78346557b85b72add10ef10f130a5be169662e
'http://archive.ubuntu.com/ubuntu/pool/main/libp/libpng1.6/libpng1.6_1.6.37.orig.tar.gz' libpng1.6_1.6.37.orig.tar.gz 1508805 SHA256:ca74a0dace179a8422187671aee97dd3892b53e168627145271cad5b5ac81307
'http://archive.ubuntu.com/ubuntu/pool/main/libp/libpng1.6/libpng1.6_1.6.37-2.debian.tar.xz' libpng1.6_1.6.37-2.debian.tar.xz 31844 SHA256:097cee0f0da4013d0231d37e090204ab3fa592b4fecdaaed3fca8d13affcaae8
```

### `dpkg` source package: `libproxy=0.4.15-10ubuntu1.2`

Binary Packages:

- `libproxy1v5:amd64=0.4.15-10ubuntu1.2`

Licenses: (parsed from: `/usr/share/doc/libproxy1v5/copyright`)

- `GPL`
- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris libproxy=0.4.15-10ubuntu1.2
'http://archive.ubuntu.com/ubuntu/pool/main/libp/libproxy/libproxy_0.4.15-10ubuntu1.2.dsc' libproxy_0.4.15-10ubuntu1.2.dsc 3613 SHA512:6774a3833b3f78c63f9d7b9e26b9c467070f8c2ae7b531fef4d64a639fb851df69449a46cc2dc6d9a76583063dfc7383868824d1c554cec629ca74e3babc5f58
'http://archive.ubuntu.com/ubuntu/pool/main/libp/libproxy/libproxy_0.4.15.orig.tar.gz' libproxy_0.4.15.orig.tar.gz 93084 SHA512:8f68bd56e44aeb3f553f4657bef82a5d14302780508dafa32454d6f724b724c884ceed6042f8df53a081d26ea0b05598cf35eab44823257c47c5ef8afb36442b
'http://archive.ubuntu.com/ubuntu/pool/main/libp/libproxy/libproxy_0.4.15-10ubuntu1.2.debian.tar.xz' libproxy_0.4.15-10ubuntu1.2.debian.tar.xz 15888 SHA512:e667a569e7db42c8f812d51d26354cf57dd5ee8796b657c7f648722f58788032e1ba199a8684b167ffbc41e53449997efe7e4e3e582db1fcd1fa8355eabe1a69
```

### `dpkg` source package: `libpsl=0.21.0-1ubuntu1`

Binary Packages:

- `libpsl5:amd64=0.21.0-1ubuntu1`

Licenses: (parsed from: `/usr/share/doc/libpsl5/copyright`)

- `Chromium`
- `MIT`

Source:

```console
$ apt-get source -qq --print-uris libpsl=0.21.0-1ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/libp/libpsl/libpsl_0.21.0-1ubuntu1.dsc' libpsl_0.21.0-1ubuntu1.dsc 2383 SHA256:38d6cf06b8ac1929efe109ac3d5f37ea6e89ea82f7a5125db4dc7a7b5f3faf94
'http://archive.ubuntu.com/ubuntu/pool/main/libp/libpsl/libpsl_0.21.0.orig.tar.gz' libpsl_0.21.0.orig.tar.gz 8598583 SHA256:055aa87ec166c7afb985d0816c07ff440e1eb899881a318c51c69a0aeea8e279
'http://archive.ubuntu.com/ubuntu/pool/main/libp/libpsl/libpsl_0.21.0-1ubuntu1.debian.tar.xz' libpsl_0.21.0-1ubuntu1.debian.tar.xz 12476 SHA256:efd6c7ae8c244b582d6af943b5925d95a31a183abf695301f2fa49de9f694671
```

### `dpkg` source package: `libpthread-stubs=0.4-1`

Binary Packages:

- `libpthread-stubs0-dev:amd64=0.4-1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libpthread-stubs=0.4-1
'http://archive.ubuntu.com/ubuntu/pool/main/libp/libpthread-stubs/libpthread-stubs_0.4-1.dsc' libpthread-stubs_0.4-1.dsc 1927 SHA256:8923683ac365475d2cc515e5f16f4adc8bd8e37453e1a2a6bedeb9246922829f
'http://archive.ubuntu.com/ubuntu/pool/main/libp/libpthread-stubs/libpthread-stubs_0.4.orig.tar.gz' libpthread-stubs_0.4.orig.tar.gz 71252 SHA256:50d5686b79019ccea08bcbd7b02fe5a40634abcfd4146b6e75c6420cc170e9d9
'http://archive.ubuntu.com/ubuntu/pool/main/libp/libpthread-stubs/libpthread-stubs_0.4-1.diff.gz' libpthread-stubs_0.4-1.diff.gz 2346 SHA256:ec435ba2852ad4b0522010943a5b7d39fc7e088067367879778cf10e57f5cc3f
```

### `dpkg` source package: `libraw1394=2.1.2-1`

Binary Packages:

- `libraw1394-11:amd64=2.1.2-1`

Licenses: (parsed from: `/usr/share/doc/libraw1394-11/copyright`)

- `GPL`
- `LGPL`

Source:

```console
$ apt-get source -qq --print-uris libraw1394=2.1.2-1
'http://archive.ubuntu.com/ubuntu/pool/main/libr/libraw1394/libraw1394_2.1.2-1.dsc' libraw1394_2.1.2-1.dsc 2080 SHA256:d8b7cb13f4a73fa0dae8d61d5b4ded82b3f02d6b3584ac77c671432d250988f4
'http://archive.ubuntu.com/ubuntu/pool/main/libr/libraw1394/libraw1394_2.1.2.orig.tar.gz' libraw1394_2.1.2.orig.tar.gz 458134 SHA256:ddc4e32721cdfe680d964aaede68ac606a20cd17dd2ba70e2d7e0692086ab57c
'http://archive.ubuntu.com/ubuntu/pool/main/libr/libraw1394/libraw1394_2.1.2-1.debian.tar.xz' libraw1394_2.1.2-1.debian.tar.xz 8760 SHA256:5cee0e0049d820a8e4e5d3dbd94fb2c3d7b782ec09134c6c714ed523829dc1c3
```

### `dpkg` source package: `libreoffice=1:6.4.7-0ubuntu0.20.04.1`

Binary Packages:

- `fonts-opensymbol=2:102.11+LibO6.4.7-0ubuntu0.20.04.1`
- `libjuh-java=1:6.4.7-0ubuntu0.20.04.1`
- `libjurt-java=1:6.4.7-0ubuntu0.20.04.1`
- `libreoffice=1:6.4.7-0ubuntu0.20.04.1`
- `libreoffice-base=1:6.4.7-0ubuntu0.20.04.1`
- `libreoffice-base-core=1:6.4.7-0ubuntu0.20.04.1`
- `libreoffice-base-drivers=1:6.4.7-0ubuntu0.20.04.1`
- `libreoffice-calc=1:6.4.7-0ubuntu0.20.04.1`
- `libreoffice-common=1:6.4.7-0ubuntu0.20.04.1`
- `libreoffice-core=1:6.4.7-0ubuntu0.20.04.1`
- `libreoffice-draw=1:6.4.7-0ubuntu0.20.04.1`
- `libreoffice-gnome=1:6.4.7-0ubuntu0.20.04.1`
- `libreoffice-gtk3=1:6.4.7-0ubuntu0.20.04.1`
- `libreoffice-impress=1:6.4.7-0ubuntu0.20.04.1`
- `libreoffice-java-common=1:6.4.7-0ubuntu0.20.04.1`
- `libreoffice-math=1:6.4.7-0ubuntu0.20.04.1`
- `libreoffice-nlpsolver=0.9+LibO6.4.7-0ubuntu0.20.04.1`
- `libreoffice-report-builder=1:6.4.7-0ubuntu0.20.04.1`
- `libreoffice-report-builder-bin=1:6.4.7-0ubuntu0.20.04.1`
- `libreoffice-script-provider-bsh=1:6.4.7-0ubuntu0.20.04.1`
- `libreoffice-script-provider-js=1:6.4.7-0ubuntu0.20.04.1`
- `libreoffice-script-provider-python=1:6.4.7-0ubuntu0.20.04.1`
- `libreoffice-sdbc-firebird=1:6.4.7-0ubuntu0.20.04.1`
- `libreoffice-sdbc-hsqldb=1:6.4.7-0ubuntu0.20.04.1`
- `libreoffice-sdbc-mysql=1:6.4.7-0ubuntu0.20.04.1`
- `libreoffice-sdbc-postgresql=1:6.4.7-0ubuntu0.20.04.1`
- `libreoffice-style-colibre=1:6.4.7-0ubuntu0.20.04.1`
- `libreoffice-style-elementary=1:6.4.7-0ubuntu0.20.04.1`
- `libreoffice-style-tango=1:6.4.7-0ubuntu0.20.04.1`
- `libreoffice-wiki-publisher=1.2.0+LibO6.4.7-0ubuntu0.20.04.1`
- `libreoffice-writer=1:6.4.7-0ubuntu0.20.04.1`
- `libridl-java=1:6.4.7-0ubuntu0.20.04.1`
- `libuno-cppu3=1:6.4.7-0ubuntu0.20.04.1`
- `libuno-cppuhelpergcc3-3=1:6.4.7-0ubuntu0.20.04.1`
- `libuno-purpenvhelpergcc3-3=1:6.4.7-0ubuntu0.20.04.1`
- `libuno-sal3=1:6.4.7-0ubuntu0.20.04.1`
- `libuno-salhelpergcc3-3=1:6.4.7-0ubuntu0.20.04.1`
- `libunoil-java=1:6.4.7-0ubuntu0.20.04.1`
- `libunoloader-java=1:6.4.7-0ubuntu0.20.04.1`
- `python3-uno=1:6.4.7-0ubuntu0.20.04.1`
- `uno-libs-private=1:6.4.7-0ubuntu0.20.04.1`
- `ure=1:6.4.7-0ubuntu0.20.04.1`

Licenses: (parsed from: `/usr/share/doc/fonts-opensymbol/copyright`, `/usr/share/doc/libjuh-java/copyright`, `/usr/share/doc/libjurt-java/copyright`, `/usr/share/doc/libreoffice/copyright`, `/usr/share/doc/libreoffice-base/copyright`, `/usr/share/doc/libreoffice-base-core/copyright`, `/usr/share/doc/libreoffice-base-drivers/copyright`, `/usr/share/doc/libreoffice-calc/copyright`, `/usr/share/doc/libreoffice-common/copyright`, `/usr/share/doc/libreoffice-core/copyright`, `/usr/share/doc/libreoffice-draw/copyright`, `/usr/share/doc/libreoffice-gnome/copyright`, `/usr/share/doc/libreoffice-gtk3/copyright`, `/usr/share/doc/libreoffice-impress/copyright`, `/usr/share/doc/libreoffice-java-common/copyright`, `/usr/share/doc/libreoffice-math/copyright`, `/usr/share/doc/libreoffice-nlpsolver/copyright`, `/usr/share/doc/libreoffice-report-builder/copyright`, `/usr/share/doc/libreoffice-report-builder-bin/copyright`, `/usr/share/doc/libreoffice-script-provider-bsh/copyright`, `/usr/share/doc/libreoffice-script-provider-js/copyright`, `/usr/share/doc/libreoffice-script-provider-python/copyright`, `/usr/share/doc/libreoffice-sdbc-firebird/copyright`, `/usr/share/doc/libreoffice-sdbc-hsqldb/copyright`, `/usr/share/doc/libreoffice-sdbc-mysql/copyright`, `/usr/share/doc/libreoffice-sdbc-postgresql/copyright`, `/usr/share/doc/libreoffice-style-colibre/copyright`, `/usr/share/doc/libreoffice-style-elementary/copyright`, `/usr/share/doc/libreoffice-style-tango/copyright`, `/usr/share/doc/libreoffice-wiki-publisher/copyright`, `/usr/share/doc/libreoffice-writer/copyright`, `/usr/share/doc/libridl-java/copyright`, `/usr/share/doc/libuno-cppu3/copyright`, `/usr/share/doc/libuno-cppuhelpergcc3-3/copyright`, `/usr/share/doc/libuno-purpenvhelpergcc3-3/copyright`, `/usr/share/doc/libuno-sal3/copyright`, `/usr/share/doc/libuno-salhelpergcc3-3/copyright`, `/usr/share/doc/libunoil-java/copyright`, `/usr/share/doc/libunoloader-java/copyright`, `/usr/share/doc/python3-uno/copyright`, `/usr/share/doc/uno-libs-private/copyright`, `/usr/share/doc/ure/copyright`)

- `Apache-2.0`
- `CC-BY-SA-3.0`
- `CC-BY-SA-3.0 `
- `CC0-1.0`
- `Expat`
- `GPL-2`
- `GPL-2+`
- `GPL-3`
- `GPL-3+`
- `LGPL-2`
- `LGPL-3`
- `LGPL-3+`
- `MPL-1.1`
- `MPL-2.0`
- `other`

Source:

```console
$ apt-get source -qq --print-uris libreoffice=1:6.4.7-0ubuntu0.20.04.1
'http://archive.ubuntu.com/ubuntu/pool/main/libr/libreoffice/libreoffice_6.4.7-0ubuntu0.20.04.1.dsc' libreoffice_6.4.7-0ubuntu0.20.04.1.dsc 30534 SHA512:c5de057994eaf36d8a1cd7f14301378982b8e42b96a3292dc2b1ce269e84c144d05fb9dfbaef354d4369b8ceae0319106f13ee4c506540ddd5932e54674ae1ba
'http://archive.ubuntu.com/ubuntu/pool/main/libr/libreoffice/libreoffice_6.4.7.orig-helpcontent2.tar.xz' libreoffice_6.4.7.orig-helpcontent2.tar.xz 88073752 SHA512:3d6459cd4eab764d1fc15eb6edd6c1b16272f51869de1e9379328e81e03adb146d73c2acaf147d13b8ccc9a99721e023d404a0cf2f99a05264cc5536604828da
'http://archive.ubuntu.com/ubuntu/pool/main/libr/libreoffice/libreoffice_6.4.7.orig-tarballs.tar.xz' libreoffice_6.4.7.orig-tarballs.tar.xz 200916520 SHA512:10dc6e7b0521dff9aea57859312bb9802e1671f52d97f0f3fb7e030e779f778842088160ee14f446a8cc457d67b21b683c90c94bf0aa797ff890b2dea344f5ab
'http://archive.ubuntu.com/ubuntu/pool/main/libr/libreoffice/libreoffice_6.4.7.orig-translations.tar.xz' libreoffice_6.4.7.orig-translations.tar.xz 179847328 SHA512:6496ce816b136504c9511aeac5e881f329360e144ea48f28e9b56720ed727719339703431e0e0cee931d721c0e2b308366f0a3af771ebc980952c410f400c1cd
'http://archive.ubuntu.com/ubuntu/pool/main/libr/libreoffice/libreoffice_6.4.7.orig.tar.xz' libreoffice_6.4.7.orig.tar.xz 211530416 SHA512:a0ee0c0e617b33655dce0fbb14e2af12c3d810a3a9631d4b2204a75c8ac64c8797ab52735b799d078e1fff5a14d188ee850c239dab7bce4035a7ca704d6caa97
'http://archive.ubuntu.com/ubuntu/pool/main/libr/libreoffice/libreoffice_6.4.7-0ubuntu0.20.04.1.debian.tar.xz' libreoffice_6.4.7-0ubuntu0.20.04.1.debian.tar.xz 2374272 SHA512:78883ca16b441714a761c7f1d66067ee5f2635700ac1668bf948329ebd8d875e28bbf643151d543ab6016fed7476540ae0f496757c5a20bafd9b7a66caabd2a0
```

### `dpkg` source package: `librest=0.8.1-1`

Binary Packages:

- `librest-0.7-0:amd64=0.8.1-1`

Licenses: (parsed from: `/usr/share/doc/librest-0.7-0/copyright`)

- `FSF-INSTALL`
- `FSF-aclocal`
- `GPL-2`
- `GPL-2+`
- `LGPL-2.1`
- `MIT with XConsortium exception `

Source:

```console
$ apt-get source -qq --print-uris librest=0.8.1-1
'http://archive.ubuntu.com/ubuntu/pool/main/libr/librest/librest_0.8.1-1.dsc' librest_0.8.1-1.dsc 2418 SHA256:0ec122ae048847cc8203b72a7377da475b614ee91c37654163e0622194f122bb
'http://archive.ubuntu.com/ubuntu/pool/main/libr/librest/librest_0.8.1.orig.tar.bz2' librest_0.8.1.orig.tar.bz2 68249 SHA256:9063b9906c3a4684bef6ccaad9462e8409e1025fe37b7c9596fcf2f5f7507904
'http://archive.ubuntu.com/ubuntu/pool/main/libr/librest/librest_0.8.1-1.debian.tar.xz' librest_0.8.1-1.debian.tar.xz 6696 SHA256:9bfb3d85e7904cf8d740932a3bba10b5baf7f2ca371887d9fe0b16af8d34fc32
```

### `dpkg` source package: `librevenge=0.0.4-6ubuntu5`

Binary Packages:

- `librevenge-0.0-0:amd64=0.0.4-6ubuntu5`

Licenses: (parsed from: `/usr/share/doc/librevenge-0.0-0/copyright`)

- `LGPL-2.1`
- `MPL-1.1 | GPL-3+ | LGPL-3+`
- `MPL-2.0`

Source:

```console
$ apt-get source -qq --print-uris librevenge=0.0.4-6ubuntu5
'http://archive.ubuntu.com/ubuntu/pool/main/libr/librevenge/librevenge_0.0.4-6ubuntu5.dsc' librevenge_0.0.4-6ubuntu5.dsc 2147 SHA256:98bfa1d8acc54dcbcff288d539990bc308bc63547d6352b27e9c60a57c98b778
'http://archive.ubuntu.com/ubuntu/pool/main/libr/librevenge/librevenge_0.0.4.orig.tar.bz2' librevenge_0.0.4.orig.tar.bz2 529833 SHA256:c51601cd08320b75702812c64aae0653409164da7825fd0f451ac2c5dbe77cbf
'http://archive.ubuntu.com/ubuntu/pool/main/libr/librevenge/librevenge_0.0.4-6ubuntu5.debian.tar.xz' librevenge_0.0.4-6ubuntu5.debian.tar.xz 13916 SHA256:5676593079a965cf7121aef78ff5a030e4160ca8c100082e0e1d9ec34c84e8e9
```

### `dpkg` source package: `librsvg=2.48.9-1ubuntu0.20.04.1`

Binary Packages:

- `librsvg2-2:amd64=2.48.9-1ubuntu0.20.04.1`
- `librsvg2-common:amd64=2.48.9-1ubuntu0.20.04.1`

Licenses: (parsed from: `/usr/share/doc/librsvg2-2/copyright`, `/usr/share/doc/librsvg2-common/copyright`)

- `Apache-2.0`
- `BSD-2-clause`
- `BSD-3-clause`
- `Boost-1.0`
- `Expat`
- `LGPL-2`
- `LGPL-2+`
- `MPL-2.0`
- `Sun-permissive`
- `Unlicense`

Source:

```console
$ apt-get source -qq --print-uris librsvg=2.48.9-1ubuntu0.20.04.1
'http://archive.ubuntu.com/ubuntu/pool/main/libr/librsvg/librsvg_2.48.9-1ubuntu0.20.04.1.dsc' librsvg_2.48.9-1ubuntu0.20.04.1.dsc 2643 SHA512:31d3922daecaec10ef5f0dceb49067183cfcc4fe5f4422785def2b7c1056908753ebe7db31ba89a9b6fbaef0ee4c3435edc06cede57bb5a82ab8a8599ad21643
'http://archive.ubuntu.com/ubuntu/pool/main/libr/librsvg/librsvg_2.48.9.orig.tar.xz' librsvg_2.48.9.orig.tar.xz 12661188 SHA512:4edfba4d667536ef921e72da53e4a357e56cae6db5e7e93c23d6792241b31b2397e68633dbb0df465129ce68f0ef1ba55cc51f2321adc6f4874fe7089715bb5c
'http://archive.ubuntu.com/ubuntu/pool/main/libr/librsvg/librsvg_2.48.9-1ubuntu0.20.04.1.debian.tar.xz' librsvg_2.48.9-1ubuntu0.20.04.1.debian.tar.xz 24260 SHA512:11d05100ea44f7d3474907516f0869b5b43517eaccd56cc70780418935f39de97294e3a0b31fa8e36382d3a6809ca1812220931b9673842a1e2a236e153a3e40
```

### `dpkg` source package: `libsamplerate=0.1.9-2`

Binary Packages:

- `libsamplerate0:amd64=0.1.9-2`

Licenses: (parsed from: `/usr/share/doc/libsamplerate0/copyright`)

- `BSD-2-clause`
- `GPL-2`
- `GPL-2+`

Source:

```console
$ apt-get source -qq --print-uris libsamplerate=0.1.9-2
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libsamplerate/libsamplerate_0.1.9-2.dsc' libsamplerate_0.1.9-2.dsc 2159 SHA256:a424910e1cdcfc8596a086c3256af8b63af450f4d0bc244fab3163cbb8e1707a
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libsamplerate/libsamplerate_0.1.9.orig.tar.gz' libsamplerate_0.1.9.orig.tar.gz 4336641 SHA256:0a7eb168e2f21353fb6d84da152e4512126f7dc48ccb0be80578c565413444c1
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libsamplerate/libsamplerate_0.1.9-2.debian.tar.xz' libsamplerate_0.1.9-2.debian.tar.xz 7496 SHA256:9fb3e5e7724f327272b7228ea267bfbb53be214db35778d85e3a9ce5e618634b
```

### `dpkg` source package: `libsdl2=2.0.10+dfsg1-3`

Binary Packages:

- `libsdl2-2.0-0:amd64=2.0.10+dfsg1-3`

Licenses: (parsed from: `/usr/share/doc/libsdl2-2.0-0/copyright`)

- `BSD-3-clause`
- `BSD-3-clause-chromium`
- `BSD-3-clause-kitware`
- `BrownUn_UnCalifornia_ErikCorry`
- `Expat-like`
- `Gareth_McCaughan`
- `LGPL-2.1`
- `LGPL-2.1+`
- `MIT/X11`
- `PublicDomain_David_Ludwig`
- `PublicDomain_Edgar_Simo`
- `PublicDomain_Sam_Lantinga`
- `RSA_Data_Security`
- `SGI-Free-Software-License-B`
- `SunPro`
- `zlib/libpng`

Source:

```console
$ apt-get source -qq --print-uris libsdl2=2.0.10+dfsg1-3
'http://archive.ubuntu.com/ubuntu/pool/universe/libs/libsdl2/libsdl2_2.0.10+dfsg1-3.dsc' libsdl2_2.0.10+dfsg1-3.dsc 2827 SHA256:e91f04119fdf20cbb28c7cdc437a97dd8910d1c0c29c6a526345acf5a5236c01
'http://archive.ubuntu.com/ubuntu/pool/universe/libs/libsdl2/libsdl2_2.0.10+dfsg1.orig.tar.xz' libsdl2_2.0.10+dfsg1.orig.tar.xz 2550516 SHA256:8a425d050c492cd0c352b522beea2c379938c4be0d47607db5b1b68f2ddc7fee
'http://archive.ubuntu.com/ubuntu/pool/universe/libs/libsdl2/libsdl2_2.0.10+dfsg1-3.debian.tar.xz' libsdl2_2.0.10+dfsg1-3.debian.tar.xz 22512 SHA256:fb6b7a079c7f50df6f205952b95d6d1897b1fee4b2ca85c0ffbc7ff683f794e6
```

### `dpkg` source package: `libseccomp=2.5.1-1ubuntu1~20.04.1`

Binary Packages:

- `libseccomp2:amd64=2.5.1-1ubuntu1~20.04.1`

Licenses: (parsed from: `/usr/share/doc/libseccomp2/copyright`)

- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris libseccomp=2.5.1-1ubuntu1~20.04.1
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libseccomp/libseccomp_2.5.1-1ubuntu1~20.04.1.dsc' libseccomp_2.5.1-1ubuntu1~20.04.1.dsc 2578 SHA512:99a05f089274b5505f01d1fc8adb3bb000e0dda5e640900526470c2f558f3e485cb9392070bb5e78f11511f451eca04fb56d3f9f356e8a44f50044ef18d98f78
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libseccomp/libseccomp_2.5.1.orig.tar.gz' libseccomp_2.5.1.orig.tar.gz 638811 SHA512:2be80a6323f9282dbeae8791724e5778b32e2382b2a3d1b0f77366371ec4072ea28128204f675cce101c091c0420d12c497e1a9ccbb7dc5bcbf61bfd777160af
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libseccomp/libseccomp_2.5.1-1ubuntu1~20.04.1.debian.tar.xz' libseccomp_2.5.1-1ubuntu1~20.04.1.debian.tar.xz 21124 SHA512:a89687c96274b7f0129e1440daf0d5921ab4c1e9498187da30b491a173b52f9bb82081f556a8a8d3b7b194d87158b430e5540d033a6b995169166e00e6fee097
```

### `dpkg` source package: `libselinux=3.0-1build2`

Binary Packages:

- `libselinux1:amd64=3.0-1build2`

Licenses: (parsed from: `/usr/share/doc/libselinux1/copyright`)

- `GPL-2`
- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris libselinux=3.0-1build2
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libselinux/libselinux_3.0-1build2.dsc' libselinux_3.0-1build2.dsc 2565 SHA256:9a8d6c354ed06350606c009d899d117e71fda20887792b2c25b38222d0190d93
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libselinux/libselinux_3.0.orig.tar.gz' libselinux_3.0.orig.tar.gz 212096 SHA256:2ea2b30f671dae9d6b1391cbe8fb2ce5d36a3ee4fb1cd3c32f0d933c31b82433
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libselinux/libselinux_3.0-1build2.debian.tar.xz' libselinux_3.0-1build2.debian.tar.xz 23720 SHA256:ed85da0fe5561205c95f0f622562425dc7d8dd61ffd213a7fa914d778fe8da71
```

### `dpkg` source package: `libsemanage=3.0-1build2`

Binary Packages:

- `libsemanage-common=3.0-1build2`
- `libsemanage1:amd64=3.0-1build2`

Licenses: (parsed from: `/usr/share/doc/libsemanage-common/copyright`, `/usr/share/doc/libsemanage1/copyright`)

- `GPL`
- `LGPL`

Source:

```console
$ apt-get source -qq --print-uris libsemanage=3.0-1build2
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libsemanage/libsemanage_3.0-1build2.dsc' libsemanage_3.0-1build2.dsc 2678 SHA256:6231f4b00991657fafef2595eb571b2bcbe437de4ec9dc9929c0e69187db5f33
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libsemanage/libsemanage_3.0.orig.tar.gz' libsemanage_3.0.orig.tar.gz 180745 SHA256:a497b0720d54eac427f1f3f618eed417e50ed8f4e47ed0f7a1d391bd416e84cf
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libsemanage/libsemanage_3.0-1build2.debian.tar.xz' libsemanage_3.0-1build2.debian.tar.xz 17176 SHA256:38a646f91532c920c8c15a695c3585397ddbf032ecf49c52eb89d53c8eac48fb
```

### `dpkg` source package: `libsepol=3.0-1`

Binary Packages:

- `libsepol1:amd64=3.0-1`

Licenses: (parsed from: `/usr/share/doc/libsepol1/copyright`)

- `GPL`
- `LGPL`

Source:

```console
$ apt-get source -qq --print-uris libsepol=3.0-1
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libsepol/libsepol_3.0-1.dsc' libsepol_3.0-1.dsc 1770 SHA256:0073de5844605d380dd56f6630678ad91459496dc768fa9eb4d8cc7f693f5c1a
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libsepol/libsepol_3.0.orig.tar.gz' libsepol_3.0.orig.tar.gz 473864 SHA256:5b7ae1881909f1048b06f7a0c364c5c8a86ec12e0ec76e740fe9595a6033eb79
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libsepol/libsepol_3.0-1.debian.tar.xz' libsepol_3.0-1.debian.tar.xz 14224 SHA256:a16b5bc3c041e016d01794d1a1b9826ed4426862622c05526e93607c325ec328
```

### `dpkg` source package: `libsm=2:1.2.3-1`

Binary Packages:

- `libsm-dev:amd64=2:1.2.3-1`
- `libsm6:amd64=2:1.2.3-1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libsm=2:1.2.3-1
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libsm/libsm_1.2.3-1.dsc' libsm_1.2.3-1.dsc 2063 SHA256:5488f8de81d53c32cbb5f062b6a6f262cd067283b8082041392dc60f0d04002c
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libsm/libsm_1.2.3.orig.tar.gz' libsm_1.2.3.orig.tar.gz 445362 SHA256:1e92408417cb6c6c477a8a6104291001a40b3bb56a4a60608fdd9cd2c5a0f320
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libsm/libsm_1.2.3-1.diff.gz' libsm_1.2.3-1.diff.gz 8929 SHA256:7eb99ab50b19f26d1470f89e4b46891f6a697cb1794a58ed0d1376cceaf1b6a9
```

### `dpkg` source package: `libsndfile=1.0.28-7ubuntu0.1`

Binary Packages:

- `libsndfile1:amd64=1.0.28-7ubuntu0.1`

Licenses: (parsed from: `/usr/share/doc/libsndfile1/copyright`)

- `Apache-2.0`
- `BSD-3-clause`
- `FSFAP`
- `GPL-2`
- `GPL-2+`
- `LGPL-2`
- `LGPL-2+`
- `LGPL-2.1`
- `LGPL-2.1+`
- `gsm`
- `sun`

Source:

```console
$ apt-get source -qq --print-uris libsndfile=1.0.28-7ubuntu0.1
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libsndfile/libsndfile_1.0.28-7ubuntu0.1.dsc' libsndfile_1.0.28-7ubuntu0.1.dsc 2007 SHA512:390b4e7ffd668889f454aa1787202740a01f538812a55eae274fcf729069bd0ae6a35dc87a9d5ae4452b935a8b54d026cca7baf651c2506bb37871e113ceab58
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libsndfile/libsndfile_1.0.28.orig.tar.gz' libsndfile_1.0.28.orig.tar.gz 1202833 SHA512:890731a6b8173f714155ce05eaf6d991b31632c8ab207fbae860968861a107552df26fcf85602df2e7f65502c7256c1b41735e1122485a3a07ddb580aa83b57f
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libsndfile/libsndfile_1.0.28-7ubuntu0.1.debian.tar.xz' libsndfile_1.0.28-7ubuntu0.1.debian.tar.xz 17248 SHA512:490c8360f04d8af7f3ddc7db6a943399fd614cb395fa581810292a785e468d19f3f0734d82a08d8715f0bb3fc0fdb79cb566175769b1159e6c7bfe411039235c
```

### `dpkg` source package: `libsodium=1.0.18-1`

Binary Packages:

- `libsodium23:amd64=1.0.18-1`

Licenses: (parsed from: `/usr/share/doc/libsodium23/copyright`)

- `BSD-2-clause`
- `CC0`
- `CC0-1.0`
- `GPL-2`
- `GPL-2+`
- `ISC`
- `MIT`
- `public-domain`

Source:

```console
$ apt-get source -qq --print-uris libsodium=1.0.18-1
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libsodium/libsodium_1.0.18-1.dsc' libsodium_1.0.18-1.dsc 1913 SHA256:037b3ac05a50409cb462e2c21c7a67f983d193a22d2486f4ab3fdc793f5a731c
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libsodium/libsodium_1.0.18.orig.tar.gz' libsodium_1.0.18.orig.tar.gz 1619527 SHA256:d59323c6b712a1519a5daf710b68f5e7fde57040845ffec53850911f10a5d4f4
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libsodium/libsodium_1.0.18-1.debian.tar.xz' libsodium_1.0.18-1.debian.tar.xz 7440 SHA256:50863d8fc4f0a2a86f7b69745514455f0b9d74cf45906523c675ffe5b8db0377
```

### `dpkg` source package: `libsoup2.4=2.70.0-1`

Binary Packages:

- `libsoup-gnome2.4-1:amd64=2.70.0-1`
- `libsoup2.4-1:amd64=2.70.0-1`

Licenses: (parsed from: `/usr/share/doc/libsoup-gnome2.4-1/copyright`, `/usr/share/doc/libsoup2.4-1/copyright`)

- `Expat`
- `LGPL-2`
- `LGPL-2+`

Source:

```console
$ apt-get source -qq --print-uris libsoup2.4=2.70.0-1
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libsoup2.4/libsoup2.4_2.70.0-1.dsc' libsoup2.4_2.70.0-1.dsc 3318 SHA256:f9912c8963b6f38c8f60c530a22f47089eeedc68d748716be5c9777a5170c114
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libsoup2.4/libsoup2.4_2.70.0.orig.tar.xz' libsoup2.4_2.70.0.orig.tar.xz 1494220 SHA256:54b020f74aefa438918d8e53cff62e2b1e59efe2de53e06b19a4b07b1f4d5342
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libsoup2.4/libsoup2.4_2.70.0-1.debian.tar.xz' libsoup2.4_2.70.0-1.debian.tar.xz 24272 SHA256:108bcfc24d745bfd7e5204cfc12e1f4d894634481da222c11118ef065cac99b9
```

### `dpkg` source package: `libsoxr=0.1.3-2build1`

Binary Packages:

- `libsoxr0:amd64=0.1.3-2build1`

Licenses: (parsed from: `/usr/share/doc/libsoxr0/copyright`)

- `LGPL-2.1`
- `LGPL-2.1+`
- `Spherepack`
- `permissive1`
- `permissive2`

Source:

```console
$ apt-get source -qq --print-uris libsoxr=0.1.3-2build1
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libsoxr/libsoxr_0.1.3-2build1.dsc' libsoxr_0.1.3-2build1.dsc 1468 SHA256:9efa85a763a8620252d222771a14978659557d4310c87554752208ffa20909cf
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libsoxr/libsoxr_0.1.3.orig.tar.xz' libsoxr_0.1.3.orig.tar.xz 94384 SHA256:b111c15fdc8c029989330ff559184198c161100a59312f5dc19ddeb9b5a15889
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libsoxr/libsoxr_0.1.3-2build1.debian.tar.xz' libsoxr_0.1.3-2build1.debian.tar.xz 5144 SHA256:a4babd7b0e752627846e7e7c9b2bba98a30b2f75a89b1dd9b242343dbaff5a70
```

### `dpkg` source package: `libssh=0.9.3-2ubuntu2.2`

Binary Packages:

- `libssh-4:amd64=0.9.3-2ubuntu2.2`
- `libssh-gcrypt-4:amd64=0.9.3-2ubuntu2.2`

Licenses: (parsed from: `/usr/share/doc/libssh-4/copyright`, `/usr/share/doc/libssh-gcrypt-4/copyright`)

- `BSD-2-clause`
- `BSD-3-clause`
- `LGPL-2.1`
- `LGPL-2.1+~OpenSSL`
- `public-domain`

Source:

```console
$ apt-get source -qq --print-uris libssh=0.9.3-2ubuntu2.2
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libssh/libssh_0.9.3-2ubuntu2.2.dsc' libssh_0.9.3-2ubuntu2.2.dsc 2538 SHA512:cede02f6296d5cfc401bbcded5278f9b07c7f450e2193d6bbd7ab5f562e7cee68a402855320776bfddf9769e8adbb9be3ffffe05a7202355d6afc7ddbe50088c
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libssh/libssh_0.9.3.orig.tar.xz' libssh_0.9.3.orig.tar.xz 500068 SHA512:6e59718565daeca6d224426cc1095a112deff9af8e0b021917e04f08bb7409263c35724de95f591f38e26f0fb3bbbbc69b679b6775edc21dec158d241b076c6f
'http://archive.ubuntu.com/ubuntu/pool/main/libs/libssh/libssh_0.9.3-2ubuntu2.2.debian.tar.xz' libssh_0.9.3-2ubuntu2.2.debian.tar.xz 30192 SHA512:a32781839e68eabcb17d4ccd446b4b3ec84e9891737d6df08e1dddc28ada65db677d2b0b1589cb1fe72b9256bb3d1b1dbed3fb3594afc723a223dcced73c75fe
```

### `dpkg` source package: `libtasn1-6=4.16.0-2`

Binary Packages:

- `libtasn1-6:amd64=4.16.0-2`

Licenses: (parsed from: `/usr/share/doc/libtasn1-6/copyright`)

- `GFDL-1.3`
- `GPL-3`
- `LGPL`
- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris libtasn1-6=4.16.0-2
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libtasn1-6/libtasn1-6_4.16.0-2.dsc' libtasn1-6_4.16.0-2.dsc 2586 SHA256:fd4a387c71f95c3eceb1072a3f42c7021d73128027ea41a18d6efc6cbfdd764a
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libtasn1-6/libtasn1-6_4.16.0.orig.tar.gz' libtasn1-6_4.16.0.orig.tar.gz 1812442 SHA256:0e0fb0903839117cb6e3b56e68222771bebf22ad7fc2295a0ed7d576e8d4329d
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libtasn1-6/libtasn1-6_4.16.0.orig.tar.gz.asc' libtasn1-6_4.16.0.orig.tar.gz.asc 488 SHA256:06c201e8c3b43c27465ed79294d4c4ec8dcd3e95e4a6176ecbf273229ee3e2d0
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libtasn1-6/libtasn1-6_4.16.0-2.debian.tar.xz' libtasn1-6_4.16.0-2.debian.tar.xz 17740 SHA256:c1a89b0bac0fb7c83ebac4eafbca0475c24350ade6ccaef31266424725610624
```

### `dpkg` source package: `libtext-iconv-perl=1.7-7`

Binary Packages:

- `libtext-iconv-perl=1.7-7`

Licenses: (parsed from: `/usr/share/doc/libtext-iconv-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libtext-iconv-perl=1.7-7
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libtext-iconv-perl/libtext-iconv-perl_1.7-7.dsc' libtext-iconv-perl_1.7-7.dsc 2242 SHA256:7fd0b1d78c4d461acbc397469f2f712c3eb2b47f1ba0b7c7369c6b394b61a3df
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libtext-iconv-perl/libtext-iconv-perl_1.7.orig.tar.bz2' libtext-iconv-perl_1.7.orig.tar.bz2 9977 SHA256:815c5169b7afc40bc6f681b4c615ff8fb0e073d87422280c8c759a4666567490
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libtext-iconv-perl/libtext-iconv-perl_1.7-7.debian.tar.xz' libtext-iconv-perl_1.7-7.debian.tar.xz 3388 SHA256:7a190875bc60395d81ae36d73290694cd8f0158abbe6ea6759b038a65d838286
```

### `dpkg` source package: `libthai=0.1.28-3`

Binary Packages:

- `libthai-data=0.1.28-3`
- `libthai0:amd64=0.1.28-3`

Licenses: (parsed from: `/usr/share/doc/libthai-data/copyright`, `/usr/share/doc/libthai0/copyright`)

- `GPL-2`
- `GPL-2+`
- `LGPL-2.1`
- `LGPL-2.1+`

Source:

```console
$ apt-get source -qq --print-uris libthai=0.1.28-3
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libthai/libthai_0.1.28-3.dsc' libthai_0.1.28-3.dsc 2346 SHA256:a6317b6a8e4ba40cedb10a9a659fc23885bfbe5eb8cf3a8b325a86064b0a542d
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libthai/libthai_0.1.28.orig.tar.xz' libthai_0.1.28.orig.tar.xz 413592 SHA256:ffe0a17b4b5aa11b153c15986800eca19f6c93a4025ffa5cf2cab2dcdf1ae911
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libthai/libthai_0.1.28-3.debian.tar.xz' libthai_0.1.28-3.debian.tar.xz 12128 SHA256:bca48abd9d040e844ebcb1f91a6ab4bcdfad66e36c1143f79d60461e933fddf9
```

### `dpkg` source package: `libtheora=1.1.1+dfsg.1-15ubuntu2`

Binary Packages:

- `libtheora0:amd64=1.1.1+dfsg.1-15ubuntu2`

Licenses: (parsed from: `/usr/share/doc/libtheora0/copyright`)

- `BSD-3-Clause`

Source:

```console
$ apt-get source -qq --print-uris libtheora=1.1.1+dfsg.1-15ubuntu2
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libtheora/libtheora_1.1.1+dfsg.1-15ubuntu2.dsc' libtheora_1.1.1+dfsg.1-15ubuntu2.dsc 2747 SHA256:55ca08c02cd83b9921afa0a2d009b578d4efea97b02f6a881102104c842a5a52
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libtheora/libtheora_1.1.1+dfsg.1.orig.tar.gz' libtheora_1.1.1+dfsg.1.orig.tar.gz 2100495 SHA256:c59b0f07a7314dfe2ade15c41bc9f637f8a450fc6b340af61b81760629f28f90
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libtheora/libtheora_1.1.1+dfsg.1-15ubuntu2.debian.tar.xz' libtheora_1.1.1+dfsg.1-15ubuntu2.debian.tar.xz 10964 SHA256:17ec83d759a100abb448e94dd4f3ffc81665f02972e02e19fdb1c2abb76183ff
```

### `dpkg` source package: `libtie-ixhash-perl=1.23-2`

Binary Packages:

- `libtie-ixhash-perl=1.23-2`

Licenses: (parsed from: `/usr/share/doc/libtie-ixhash-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libtie-ixhash-perl=1.23-2
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libtie-ixhash-perl/libtie-ixhash-perl_1.23-2.dsc' libtie-ixhash-perl_1.23-2.dsc 2144 SHA256:01c7243e392562381da974596b60bbcdacb3afb663ef3757593e8f96df45c113
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libtie-ixhash-perl/libtie-ixhash-perl_1.23.orig.tar.gz' libtie-ixhash-perl_1.23.orig.tar.gz 9352 SHA256:fabb0b8c97e67c9b34b6cc18ed66f6c5e01c55b257dcf007555e0b027d4caf56
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libtie-ixhash-perl/libtie-ixhash-perl_1.23-2.debian.tar.xz' libtie-ixhash-perl_1.23-2.debian.tar.xz 2036 SHA256:2a80c08ef174e7797b1f32feac55169b9579d6401392703c7989de287234720b
```

### `dpkg` source package: `libtimedate-perl=2.3200-1`

Binary Packages:

- `libtimedate-perl=2.3200-1`

Licenses: (parsed from: `/usr/share/doc/libtimedate-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libtimedate-perl=2.3200-1
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libtimedate-perl/libtimedate-perl_2.3200-1.dsc' libtimedate-perl_2.3200-1.dsc 2384 SHA256:8855e2b3f28dd8f5c556ab138489735b44364dc1bcff313257e97566508e5889
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libtimedate-perl/libtimedate-perl_2.3200.orig.tar.gz' libtimedate-perl_2.3200.orig.tar.gz 29973 SHA256:34eca099e375e2d142ea6cc935922c4980dc21c65ce7c24823ca08457c4bb3d6
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libtimedate-perl/libtimedate-perl_2.3200-1.debian.tar.xz' libtimedate-perl_2.3200-1.debian.tar.xz 5048 SHA256:ae2c40149b7cb28f6ab7b422876b3a16e5f7c91efbaa9715393f67bf264d9bec
```

### `dpkg` source package: `libtommath=1.2.0-3`

Binary Packages:

- `libtommath1:amd64=1.2.0-3`

Licenses: (parsed from: `/usr/share/doc/libtommath1/copyright`)

- `Unlicense`
- `public-domain`

Source:

```console
$ apt-get source -qq --print-uris libtommath=1.2.0-3
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libtommath/libtommath_1.2.0-3.dsc' libtommath_1.2.0-3.dsc 2457 SHA256:1aaa16f6f9f3e8783e85b77db1925a6d9c8d467a6d375964b16767e75ca717a2
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libtommath/libtommath_1.2.0.orig.tar.xz' libtommath_1.2.0.orig.tar.xz 622120 SHA256:b7c75eecf680219484055fcedd686064409254ae44bc31a96c5032843c0e18b1
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libtommath/libtommath_1.2.0.orig.tar.xz.asc' libtommath_1.2.0.orig.tar.xz.asc 240 SHA256:d90e7b82cbd5ced20e37cf96dea17a54878003f86021635d59245dde09bc0b74
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libtommath/libtommath_1.2.0-3.debian.tar.xz' libtommath_1.2.0-3.debian.tar.xz 19524 SHA256:4165b936fb2dfbbc96bcf4a59d894903b15f68afa5c3c9771e758512218af93c
```

### `dpkg` source package: `libtool=2.4.6-14`

Binary Packages:

- `libltdl7:amd64=2.4.6-14`

Licenses: (parsed from: `/usr/share/doc/libltdl7/copyright`)

- `GFDL`
- `GPL`

Source:

```console
$ apt-get source -qq --print-uris libtool=2.4.6-14
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libtool/libtool_2.4.6-14.dsc' libtool_2.4.6-14.dsc 2500 SHA256:939797b7ce62f69641d319e5d38e53b1608cee649355046eec74271e9fcfb9df
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libtool/libtool_2.4.6.orig.tar.xz' libtool_2.4.6.orig.tar.xz 973080 SHA256:7c87a8c2c8c0fc9cd5019e402bed4292462d00a718a7cd5f11218153bf28b26f
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libtool/libtool_2.4.6.orig.tar.xz.asc' libtool_2.4.6.orig.tar.xz.asc 380 SHA256:ab68ebc45d60128a71fc36167cd29dcf3c3d6d639fd28663905ebaf3e2f43d6a
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libtool/libtool_2.4.6-14.debian.tar.xz' libtool_2.4.6-14.debian.tar.xz 50832 SHA256:3ef693ea30def97a19fd94ffb2fa5421d5dc35cf7ad897a7161bd647eb4f2415
```

### `dpkg` source package: `libtry-tiny-perl=0.30-1`

Binary Packages:

- `libtry-tiny-perl=0.30-1`

Licenses: (parsed from: `/usr/share/doc/libtry-tiny-perl/copyright`)

- `Expat`

Source:

```console
$ apt-get source -qq --print-uris libtry-tiny-perl=0.30-1
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libtry-tiny-perl/libtry-tiny-perl_0.30-1.dsc' libtry-tiny-perl_0.30-1.dsc 2364 SHA256:8739ddcb041194c8a22ba8fdbcf84ccc7faeb414819a608062d19ec4dc4aa998
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libtry-tiny-perl/libtry-tiny-perl_0.30.orig.tar.gz' libtry-tiny-perl_0.30.orig.tar.gz 34395 SHA256:da5bd0d5c903519bbf10bb9ba0cb7bcac0563882bcfe4503aee3fb143eddef6b
'http://archive.ubuntu.com/ubuntu/pool/main/libt/libtry-tiny-perl/libtry-tiny-perl_0.30-1.debian.tar.xz' libtry-tiny-perl_0.30-1.debian.tar.xz 3532 SHA256:ace34ed42919a033206b51570b96b763f76cff1225685c9da275b57cbf29a9a4
```

### `dpkg` source package: `libunistring=0.9.10-2`

Binary Packages:

- `libunistring2:amd64=0.9.10-2`

Licenses: (parsed from: `/usr/share/doc/libunistring2/copyright`)

- `FreeSoftware`
- `GFDL-1.2`
- `GFDL-1.2+`
- `GPL-2`
- `GPL-2+`
- `GPL-2+ with distribution exception`
- `GPL-3`
- `GPL-3+`
- `LGPL-3`
- `LGPL-3+`
- `MIT`

Source:

```console
$ apt-get source -qq --print-uris libunistring=0.9.10-2
'http://archive.ubuntu.com/ubuntu/pool/main/libu/libunistring/libunistring_0.9.10-2.dsc' libunistring_0.9.10-2.dsc 2206 SHA256:c6faf64e2d978ec074ebf88264730121dfd03cc1639df94b5dc3eb05b1678532
'http://archive.ubuntu.com/ubuntu/pool/main/libu/libunistring/libunistring_0.9.10.orig.tar.xz' libunistring_0.9.10.orig.tar.xz 2051320 SHA256:eb8fb2c3e4b6e2d336608377050892b54c3c983b646c561836550863003c05d7
'http://archive.ubuntu.com/ubuntu/pool/main/libu/libunistring/libunistring_0.9.10.orig.tar.xz.asc' libunistring_0.9.10.orig.tar.xz.asc 1310 SHA256:e1606f691034fa21b00e08269622743547c16d21cca6c8a64156b4774a49e78e
'http://archive.ubuntu.com/ubuntu/pool/main/libu/libunistring/libunistring_0.9.10-2.debian.tar.xz' libunistring_0.9.10-2.debian.tar.xz 40708 SHA256:5e291a1a15549d12c64575c72868a8c94586715d35062b5efb48fe9a9d09924e
```

### `dpkg` source package: `liburi-perl=1.76-2`

Binary Packages:

- `liburi-perl=1.76-2`

Licenses: (parsed from: `/usr/share/doc/liburi-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris liburi-perl=1.76-2
'http://archive.ubuntu.com/ubuntu/pool/main/libu/liburi-perl/liburi-perl_1.76-2.dsc' liburi-perl_1.76-2.dsc 2363 SHA256:d46dde698ea5739137e74ee105f62e78d123d271b609a6bbe3c4ccb4e8919bff
'http://archive.ubuntu.com/ubuntu/pool/main/libu/liburi-perl/liburi-perl_1.76.orig.tar.gz' liburi-perl_1.76.orig.tar.gz 107578 SHA256:b2c98e1d50d6f572483ee538a6f4ccc8d9185f91f0073fd8af7390898254413e
'http://archive.ubuntu.com/ubuntu/pool/main/libu/liburi-perl/liburi-perl_1.76-2.debian.tar.xz' liburi-perl_1.76-2.debian.tar.xz 5612 SHA256:0dd6cb2219f583b52037291f023622ae06894cccbcff76107ad3ca3863372a62
```

### `dpkg` source package: `libusb-1.0=2:1.0.23-2build1`

Binary Packages:

- `libusb-1.0-0:amd64=2:1.0.23-2build1`

Licenses: (parsed from: `/usr/share/doc/libusb-1.0-0/copyright`)

- `GPL-2`
- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris libusb-1.0=2:1.0.23-2build1
'http://archive.ubuntu.com/ubuntu/pool/main/libu/libusb-1.0/libusb-1.0_1.0.23-2build1.dsc' libusb-1.0_1.0.23-2build1.dsc 1462 SHA256:2f68c34daad14a3bef624787ed907debf613ed31bde3705d703d7c70b7c7d1d8
'http://archive.ubuntu.com/ubuntu/pool/main/libu/libusb-1.0/libusb-1.0_1.0.23.orig.tar.bz2' libusb-1.0_1.0.23.orig.tar.bz2 602860 SHA256:db11c06e958a82dac52cf3c65cb4dd2c3f339c8a988665110e0d24d19312ad8d
'http://archive.ubuntu.com/ubuntu/pool/main/libu/libusb-1.0/libusb-1.0_1.0.23-2build1.debian.tar.xz' libusb-1.0_1.0.23-2build1.debian.tar.xz 13768 SHA256:070f5b921f16ada6deeff9bb20405ec7653653c5e79284d1fe2b73c05adb9db6
```

### `dpkg` source package: `libva=2.7.0-2`

Binary Packages:

- `libva-drm2:amd64=2.7.0-2`
- `libva-x11-2:amd64=2.7.0-2`
- `libva2:amd64=2.7.0-2`
- `va-driver-all:amd64=2.7.0-2`

Licenses: (parsed from: `/usr/share/doc/libva-drm2/copyright`, `/usr/share/doc/libva-x11-2/copyright`, `/usr/share/doc/libva2/copyright`, `/usr/share/doc/va-driver-all/copyright`)

- `Expat`
- `Expat-advertising`
- `GPL-2`
- `GPL-2+`
- `other`

Source:

```console
$ apt-get source -qq --print-uris libva=2.7.0-2
'http://archive.ubuntu.com/ubuntu/pool/universe/libv/libva/libva_2.7.0-2.dsc' libva_2.7.0-2.dsc 2408 SHA256:abc15533ab9ca10c58572bbcbf99e681939461de8f2ea1ede38e575419ef41b1
'http://archive.ubuntu.com/ubuntu/pool/universe/libv/libva/libva_2.7.0.orig.tar.gz' libva_2.7.0.orig.tar.gz 236349 SHA256:0952ee21069c560f48ebafcba91e1ffb72e5a8736dbab3559af959221b51598f
'http://archive.ubuntu.com/ubuntu/pool/universe/libv/libva/libva_2.7.0-2.debian.tar.xz' libva_2.7.0-2.debian.tar.xz 13232 SHA256:9f8006b4d1577aa488eaa8e2843a8686e2053bf566acab6932d436b8bab4f3b3
```

### `dpkg` source package: `libvdpau=1.3-1ubuntu2`

Binary Packages:

- `libvdpau1:amd64=1.3-1ubuntu2`
- `vdpau-driver-all:amd64=1.3-1ubuntu2`

Licenses: (parsed from: `/usr/share/doc/libvdpau1/copyright`, `/usr/share/doc/vdpau-driver-all/copyright`)

- `Expat`
- `other`

Source:

```console
$ apt-get source -qq --print-uris libvdpau=1.3-1ubuntu2
'http://archive.ubuntu.com/ubuntu/pool/main/libv/libvdpau/libvdpau_1.3-1ubuntu2.dsc' libvdpau_1.3-1ubuntu2.dsc 2394 SHA256:48e42640618fca592d58792939f199f9736bc872302b25645a699380de82cde4
'http://archive.ubuntu.com/ubuntu/pool/main/libv/libvdpau/libvdpau_1.3.orig.tar.bz2' libvdpau_1.3.orig.tar.bz2 139009 SHA256:b5a52eeac9417edbc396f26c40591ba5df0cd18285f68d84614ef8f06196e50e
'http://archive.ubuntu.com/ubuntu/pool/main/libv/libvdpau/libvdpau_1.3-1ubuntu2.debian.tar.xz' libvdpau_1.3-1ubuntu2.debian.tar.xz 11608 SHA256:2a77de4f403898c37030360a716f2a56035271760e9522e3e5e2d9f5a6eecbc2
```

### `dpkg` source package: `libvidstab=1.1.0-2`

Binary Packages:

- `libvidstab1.1:amd64=1.1.0-2`

Licenses: (parsed from: `/usr/share/doc/libvidstab1.1/copyright`)

- `GPL-2`
- `GPL-2.0+`

Source:

```console
$ apt-get source -qq --print-uris libvidstab=1.1.0-2
'http://archive.ubuntu.com/ubuntu/pool/universe/libv/libvidstab/libvidstab_1.1.0-2.dsc' libvidstab_1.1.0-2.dsc 1826 SHA256:fe500228434c80b7dc3798552a3c4023b1d086eeb18ce8d111f460e608972526
'http://archive.ubuntu.com/ubuntu/pool/universe/libv/libvidstab/libvidstab_1.1.0.orig.tar.gz' libvidstab_1.1.0.orig.tar.gz 77736 SHA256:14d2a053e56edad4f397be0cb3ef8eb1ec3150404ce99a426c4eb641861dc0bb
'http://archive.ubuntu.com/ubuntu/pool/universe/libv/libvidstab/libvidstab_1.1.0-2.debian.tar.xz' libvidstab_1.1.0-2.debian.tar.xz 3876 SHA256:c7a8ff87c37d68666c69f589929de5d25383f4932b6629af674c60e94f7e2ea6
```

### `dpkg` source package: `libvisio=0.1.7-1build2`

Binary Packages:

- `libvisio-0.1-1:amd64=0.1.7-1build2`

Licenses: (parsed from: `/usr/share/doc/libvisio-0.1-1/copyright`)

- `MIT | GPL-2`
- `MPL-2.0`

Source:

```console
$ apt-get source -qq --print-uris libvisio=0.1.7-1build2
'http://archive.ubuntu.com/ubuntu/pool/main/libv/libvisio/libvisio_0.1.7-1build2.dsc' libvisio_0.1.7-1build2.dsc 2237 SHA256:b2471036513e4d718c6998be27e0df47a642cd220093e87cd80af5d8c2f07fe2
'http://archive.ubuntu.com/ubuntu/pool/main/libv/libvisio/libvisio_0.1.7.orig.tar.xz' libvisio_0.1.7.orig.tar.xz 854296 SHA256:8faf8df870cb27b09a787a1959d6c646faa44d0d8ab151883df408b7166bea4c
'http://archive.ubuntu.com/ubuntu/pool/main/libv/libvisio/libvisio_0.1.7-1build2.debian.tar.xz' libvisio_0.1.7-1build2.debian.tar.xz 8196 SHA256:504d6acf5c330a1d3315e4413bfd22b76f07ccc0b9a6a14d9022585d1d34537c
```

### `dpkg` source package: `libvisual=0.4.0-17`

Binary Packages:

- `libvisual-0.4-0:amd64=0.4.0-17`

Licenses: (parsed from: `/usr/share/doc/libvisual-0.4-0/copyright`)

- `LGPL-2`
- `LGPL-2+`
- `LGPL-2.1`
- `LGPL-2.1+`

Source:

```console
$ apt-get source -qq --print-uris libvisual=0.4.0-17
'http://archive.ubuntu.com/ubuntu/pool/main/libv/libvisual/libvisual_0.4.0-17.dsc' libvisual_0.4.0-17.dsc 1945 SHA256:34f2ae4274167adfb6ad8d2c9bac55ca38f35c911581b50fbda8cbfdb89bebdc
'http://archive.ubuntu.com/ubuntu/pool/main/libv/libvisual/libvisual_0.4.0.orig.tar.gz' libvisual_0.4.0.orig.tar.gz 583386 SHA256:0b4dfdb87125e129567752089e3c8b54cefed601eef169d2533d8659da8dc1d7
'http://archive.ubuntu.com/ubuntu/pool/main/libv/libvisual/libvisual_0.4.0-17.debian.tar.xz' libvisual_0.4.0-17.debian.tar.xz 19984 SHA256:c91628df87d5826586bf9d350a37e4f9ef60ec24571da5749e7260b350d76681
```

### `dpkg` source package: `libvorbis=1.3.6-2ubuntu1`

Binary Packages:

- `libvorbis0a:amd64=1.3.6-2ubuntu1`
- `libvorbisenc2:amd64=1.3.6-2ubuntu1`
- `libvorbisfile3:amd64=1.3.6-2ubuntu1`

Licenses: (parsed from: `/usr/share/doc/libvorbis0a/copyright`, `/usr/share/doc/libvorbisenc2/copyright`, `/usr/share/doc/libvorbisfile3/copyright`)

- `BSD-3-Clause`
- `RFC-special`

Source:

```console
$ apt-get source -qq --print-uris libvorbis=1.3.6-2ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/libv/libvorbis/libvorbis_1.3.6-2ubuntu1.dsc' libvorbis_1.3.6-2ubuntu1.dsc 2365 SHA256:9328f0bb009c3e1e546b34a0de7c12da45d2747ea8478163702171f820c5b6b7
'http://archive.ubuntu.com/ubuntu/pool/main/libv/libvorbis/libvorbis_1.3.6.orig.tar.gz' libvorbis_1.3.6.orig.tar.gz 1634357 SHA256:6ed40e0241089a42c48604dc00e362beee00036af2d8b3f46338031c9e0351cb
'http://archive.ubuntu.com/ubuntu/pool/main/libv/libvorbis/libvorbis_1.3.6-2ubuntu1.debian.tar.xz' libvorbis_1.3.6-2ubuntu1.debian.tar.xz 12236 SHA256:fec088a9ea864bb22d964bea6fbdeb49e5517f3501ffc9428524c27a09d04128
```

### `dpkg` source package: `libvpx=1.8.2-1build1`

Binary Packages:

- `libvpx6:amd64=1.8.2-1build1`

Licenses: (parsed from: `/usr/share/doc/libvpx6/copyright`)

- `BSD-3-Clause`
- `ISC`
- `public-domain`

Source:

```console
$ apt-get source -qq --print-uris libvpx=1.8.2-1build1
'http://archive.ubuntu.com/ubuntu/pool/main/libv/libvpx/libvpx_1.8.2-1build1.dsc' libvpx_1.8.2-1build1.dsc 2296 SHA256:696dec32be1d760ed1be551a9c53ba6219f0a80b5ef388d50b0ad2c5823bfb90
'http://archive.ubuntu.com/ubuntu/pool/main/libv/libvpx/libvpx_1.8.2.orig.tar.gz' libvpx_1.8.2.orig.tar.gz 5312988 SHA256:8735d9fcd1a781ae6917f28f239a8aa358ce4864ba113ea18af4bb2dc8b474ac
'http://archive.ubuntu.com/ubuntu/pool/main/libv/libvpx/libvpx_1.8.2-1build1.debian.tar.xz' libvpx_1.8.2-1build1.debian.tar.xz 11628 SHA256:6cee87c352b4b6e6ce6e49769f374674757b5ce356ab59235342779135380a94
```

### `dpkg` source package: `libwebp=0.6.1-2ubuntu0.20.04.1`

Binary Packages:

- `libwebp6:amd64=0.6.1-2ubuntu0.20.04.1`
- `libwebpmux3:amd64=0.6.1-2ubuntu0.20.04.1`

Licenses: (parsed from: `/usr/share/doc/libwebp6/copyright`, `/usr/share/doc/libwebpmux3/copyright`)

- `Apache-2.0`

Source:

```console
$ apt-get source -qq --print-uris libwebp=0.6.1-2ubuntu0.20.04.1
'http://archive.ubuntu.com/ubuntu/pool/main/libw/libwebp/libwebp_0.6.1-2ubuntu0.20.04.1.dsc' libwebp_0.6.1-2ubuntu0.20.04.1.dsc 2185 SHA512:00c898e9ee8682a702751e255921821ac73a095404c3dfc9d47f69517de4a2bf74a62b4f3f0a9325370266c7bb951fee5bea830c8ab2c3a4b91652e9caa8ade6
'http://archive.ubuntu.com/ubuntu/pool/main/libw/libwebp/libwebp_0.6.1.orig.tar.gz' libwebp_0.6.1.orig.tar.gz 3554290 SHA512:313b345a01c91eb07c2e4d46b93fcda9c50dca9e05e39f757238a679355514a2e9bc9bc220f3d3eb6d6a55148957cb2be14dac330203953337759841af1a32bf
'http://archive.ubuntu.com/ubuntu/pool/main/libw/libwebp/libwebp_0.6.1-2ubuntu0.20.04.1.debian.tar.xz' libwebp_0.6.1-2ubuntu0.20.04.1.debian.tar.xz 16748 SHA512:46fe77068114343821bd4a74c9654a91048e5ebf7f6b397acdc8853482668fe170f3c7552be32293e26b965826f8d0f2c69da2418284b2851f3d89e64f7f62c1
```

### `dpkg` source package: `libwmf=0.2.8.4-17ubuntu1`

Binary Packages:

- `libwmf0.2-7:amd64=0.2.8.4-17ubuntu1`

Licenses: (parsed from: `/usr/share/doc/libwmf0.2-7/copyright`)

- `LGPL-2`

Source:

```console
$ apt-get source -qq --print-uris libwmf=0.2.8.4-17ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/libw/libwmf/libwmf_0.2.8.4-17ubuntu1.dsc' libwmf_0.2.8.4-17ubuntu1.dsc 1642 SHA256:31f409e280954b2388e28305ac1c39b85eb141e7823b6fc5eff89194196c4e2a
'http://archive.ubuntu.com/ubuntu/pool/main/libw/libwmf/libwmf_0.2.8.4.orig.tar.gz' libwmf_0.2.8.4.orig.tar.gz 2169375 SHA256:5b345c69220545d003ad52bfd035d5d6f4f075e65204114a9e875e84895a7cf8
'http://archive.ubuntu.com/ubuntu/pool/main/libw/libwmf/libwmf_0.2.8.4-17ubuntu1.debian.tar.xz' libwmf_0.2.8.4-17ubuntu1.debian.tar.xz 12968 SHA256:3d78073cbb035aa87d780d617f647a3e42f4cf5e9c1ada5899f7d80ac306f318
```

### `dpkg` source package: `libwpd=0.10.3-1build1`

Binary Packages:

- `libwpd-0.10-10:amd64=0.10.3-1build1`

Licenses: (parsed from: `/usr/share/doc/libwpd-0.10-10/copyright`)

- `LGPL`
- `MPL-2.0 | LGPL-2+`

Source:

```console
$ apt-get source -qq --print-uris libwpd=0.10.3-1build1
'http://archive.ubuntu.com/ubuntu/pool/main/libw/libwpd/libwpd_0.10.3-1build1.dsc' libwpd_0.10.3-1build1.dsc 2098 SHA256:140519dab91f32bc5e25e054b26788090519ac8d1df63f81e412334fa4f08cb3
'http://archive.ubuntu.com/ubuntu/pool/main/libw/libwpd/libwpd_0.10.3.orig.tar.xz' libwpd_0.10.3.orig.tar.xz 534712 SHA256:2465b0b662fdc5d4e3bebcdc9a79027713fb629ca2bff04a3c9251fdec42dd09
'http://archive.ubuntu.com/ubuntu/pool/main/libw/libwpd/libwpd_0.10.3-1build1.debian.tar.xz' libwpd_0.10.3-1build1.debian.tar.xz 11628 SHA256:9d56c0452e4095d73c277b2f62a112c00ff96b66d82ac1aa6a001a93c0690ee5
```

### `dpkg` source package: `libwpg=0.3.3-1build1`

Binary Packages:

- `libwpg-0.3-3:amd64=0.3.3-1build1`

Licenses: (parsed from: `/usr/share/doc/libwpg-0.3-3/copyright`)

- `GPL`
- `LGPL`

Source:

```console
$ apt-get source -qq --print-uris libwpg=0.3.3-1build1
'http://archive.ubuntu.com/ubuntu/pool/main/libw/libwpg/libwpg_0.3.3-1build1.dsc' libwpg_0.3.3-1build1.dsc 2070 SHA256:a4d91aa6d74fd56f02c637369f268d8c9de406355f64173f58f2b57240ab3d67
'http://archive.ubuntu.com/ubuntu/pool/main/libw/libwpg/libwpg_0.3.3.orig.tar.xz' libwpg_0.3.3.orig.tar.xz 328664 SHA256:99b3f7f8832385748582ab8130fbb9e5607bd5179bebf9751ac1d51a53099d1c
'http://archive.ubuntu.com/ubuntu/pool/main/libw/libwpg/libwpg_0.3.3-1build1.debian.tar.xz' libwpg_0.3.3-1build1.debian.tar.xz 9304 SHA256:221682e3c280bd4362aafc442991833e78b186d3432cf51cae43c1b41133f1e4
```

### `dpkg` source package: `libwps=0.4.10-1build1`

Binary Packages:

- `libwps-0.4-4:amd64=0.4.10-1build1`

Licenses: (parsed from: `/usr/share/doc/libwps-0.4-4/copyright`)

- `LGPL-2.1`
- `LGPL-2.1+`
- `MPL-2.0`

Source:

```console
$ apt-get source -qq --print-uris libwps=0.4.10-1build1
'http://archive.ubuntu.com/ubuntu/pool/main/libw/libwps/libwps_0.4.10-1build1.dsc' libwps_0.4.10-1build1.dsc 2287 SHA256:c78004bb7f820762d483ffdf2f8ba14a5a652d88cbce34a07eb972d8281873b2
'http://archive.ubuntu.com/ubuntu/pool/main/libw/libwps/libwps_0.4.10.orig.tar.xz' libwps_0.4.10.orig.tar.xz 695448 SHA256:1421e034286a9f96d3168a1c54ea570ee7aa008ca07b89de005ad5ce49fb29ca
'http://archive.ubuntu.com/ubuntu/pool/main/libw/libwps/libwps_0.4.10-1build1.debian.tar.xz' libwps_0.4.10-1build1.debian.tar.xz 9080 SHA256:da0d2e3637c899432e2f53234aa1d29b790700445af24aa9ea18d1c9d46c62e6
```

### `dpkg` source package: `libwww-perl=6.43-1`

Binary Packages:

- `libwww-perl=6.43-1`

Licenses: (parsed from: `/usr/share/doc/libwww-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libwww-perl=6.43-1
'http://archive.ubuntu.com/ubuntu/pool/main/libw/libwww-perl/libwww-perl_6.43-1.dsc' libwww-perl_6.43-1.dsc 2981 SHA256:695a90408de415a15e64aa7349c2309e4aab021a6addee2ca65107b14a5cdfbc
'http://archive.ubuntu.com/ubuntu/pool/main/libw/libwww-perl/libwww-perl_6.43.orig.tar.gz' libwww-perl_6.43.orig.tar.gz 174966 SHA256:e9849d7ee6fd0e89cc999e63d7612c951afd6aeea6bc721b767870d9df4ac40d
'http://archive.ubuntu.com/ubuntu/pool/main/libw/libwww-perl/libwww-perl_6.43-1.debian.tar.xz' libwww-perl_6.43-1.debian.tar.xz 10496 SHA256:42f7d52af4f9d3d656f07375d849b6ac7b6295e0b60fef1ad7518b8c3d43e347
```

### `dpkg` source package: `libwww-robotrules-perl=6.02-1`

Binary Packages:

- `libwww-robotrules-perl=6.02-1`

Licenses: (parsed from: `/usr/share/doc/libwww-robotrules-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libwww-robotrules-perl=6.02-1
'http://archive.ubuntu.com/ubuntu/pool/main/libw/libwww-robotrules-perl/libwww-robotrules-perl_6.02-1.dsc' libwww-robotrules-perl_6.02-1.dsc 2284 SHA256:8419a4bac65737229e54cf2356e2f0ab90a8738d7fefb82a1883480a5747b469
'http://archive.ubuntu.com/ubuntu/pool/main/libw/libwww-robotrules-perl/libwww-robotrules-perl_6.02.orig.tar.gz' libwww-robotrules-perl_6.02.orig.tar.gz 9059 SHA256:46b502e7a288d559429891eeb5d979461dd3ecc6a5c491ead85d165b6e03a51e
'http://archive.ubuntu.com/ubuntu/pool/main/libw/libwww-robotrules-perl/libwww-robotrules-perl_6.02-1.debian.tar.xz' libwww-robotrules-perl_6.02-1.debian.tar.xz 2220 SHA256:d9a0bde5423038c69616c5099a8c03158bfa8bdb6ae99eba3edbe76b8018ceeb
```

### `dpkg` source package: `libx11-protocol-perl=0.56-7`

Binary Packages:

- `libx11-protocol-perl=0.56-7`

Licenses: (parsed from: `/usr/share/doc/libx11-protocol-perl/copyright`)

- `Artistic`
- `Expat`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libx11-protocol-perl=0.56-7
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libx11-protocol-perl/libx11-protocol-perl_0.56-7.dsc' libx11-protocol-perl_0.56-7.dsc 2267 SHA256:49322d7a9aa54245b10c96daebeebd16924edc4459526637b4d809335876b3b3
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libx11-protocol-perl/libx11-protocol-perl_0.56.orig.tar.gz' libx11-protocol-perl_0.56.orig.tar.gz 101227 SHA256:de96dd6c7c1f25f3287aa7af64902bf84acaaa8e0c3bb76aa1676367e04a08b7
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libx11-protocol-perl/libx11-protocol-perl_0.56-7.debian.tar.xz' libx11-protocol-perl_0.56-7.debian.tar.xz 4348 SHA256:07a1bc718bc433d858c8b997300c41dfec7c0a4ba458977761a1e3549a75674f
```

### `dpkg` source package: `libx11=2:1.6.9-2ubuntu1.2`

Binary Packages:

- `libx11-6:amd64=2:1.6.9-2ubuntu1.2`
- `libx11-data=2:1.6.9-2ubuntu1.2`
- `libx11-dev:amd64=2:1.6.9-2ubuntu1.2`
- `libx11-xcb1:amd64=2:1.6.9-2ubuntu1.2`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libx11=2:1.6.9-2ubuntu1.2
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libx11/libx11_1.6.9-2ubuntu1.2.dsc' libx11_1.6.9-2ubuntu1.2.dsc 2671 SHA512:1468c2b4f7240b9e59dc7a5fa528383dab4787aec56fb9a0d6d4cbb182eb95e59c0f6478e37aa03ef45e1fffcb73e8b30cdf04b14a656f6edff2f3a6a2a18471
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libx11/libx11_1.6.9.orig.tar.gz' libx11_1.6.9.orig.tar.gz 2994329 SHA512:c79cf0924e920a2e8d2e9af45e73ed42b565dea79ac68d4c3889033738274694b29cedb62c057fec1aa7f7ad7dcf843334fccb43470bbae7922d42373c1c6045
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libx11/libx11_1.6.9.orig.tar.gz.asc' libx11_1.6.9.orig.tar.gz.asc 659 SHA512:56e53d1481be4e12f89af2fbcd297a3612996f5ca1eae39d6fe336f9b52832ea430ac0568e556b9e57291562c56590086871c08ec7ac046f15af4211f680adee
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libx11/libx11_1.6.9-2ubuntu1.2.diff.gz' libx11_1.6.9-2ubuntu1.2.diff.gz 67310 SHA512:f2f28664298eff341177a3bb5bc1f1290d2e0c94fa2c39771b51ec69d987a75b38cc03fbe0a0561a7807da9ac1e32c5b3c617dc7020922c6f2c1c0ca060f99de
```

### `dpkg` source package: `libxau=1:1.0.9-0ubuntu1`

Binary Packages:

- `libxau-dev:amd64=1:1.0.9-0ubuntu1`
- `libxau6:amd64=1:1.0.9-0ubuntu1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libxau=1:1.0.9-0ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxau/libxau_1.0.9-0ubuntu1.dsc' libxau_1.0.9-0ubuntu1.dsc 1563 SHA256:b59509d1f8f6c0e21b8bbd46ac1dffcd7a21a635ff3ce9c0acf68ba60fcb5e11
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxau/libxau_1.0.9.orig.tar.gz' libxau_1.0.9.orig.tar.gz 394068 SHA256:1f123d8304b082ad63a9e89376400a3b1d4c29e67e3ea07b3f659cccca690eea
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxau/libxau_1.0.9-0ubuntu1.diff.gz' libxau_1.0.9-0ubuntu1.diff.gz 15142 SHA256:cf7e9d50c3b3b8dde3486ee6fcf9bb96585e2af32924e91c10c8612e48b5dce5
```

### `dpkg` source package: `libxaw=2:1.0.13-1`

Binary Packages:

- `libxaw7:amd64=2:1.0.13-1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libxaw=2:1.0.13-1
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxaw/libxaw_1.0.13-1.dsc' libxaw_1.0.13-1.dsc 2196 SHA256:9fdf48f9ff66c0889cda5030997fe919e5320e7988f32e20bb96602daa37e7f7
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxaw/libxaw_1.0.13.orig.tar.gz' libxaw_1.0.13.orig.tar.gz 848997 SHA256:7e74ac3e5f67def549722ff0333d6e6276b8becd9d89615cda011e71238ab694
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxaw/libxaw_1.0.13-1.diff.gz' libxaw_1.0.13-1.diff.gz 12643 SHA256:241f21ba0810d9d859a98ab60f100a366bc9e98cd946c736566a8ed1353a1bcc
```

### `dpkg` source package: `libxcb=1.14-2`

Binary Packages:

- `libxcb-dri2-0:amd64=1.14-2`
- `libxcb-dri3-0:amd64=1.14-2`
- `libxcb-glx0:amd64=1.14-2`
- `libxcb-present0:amd64=1.14-2`
- `libxcb-randr0:amd64=1.14-2`
- `libxcb-render0:amd64=1.14-2`
- `libxcb-shape0:amd64=1.14-2`
- `libxcb-shm0:amd64=1.14-2`
- `libxcb-sync1:amd64=1.14-2`
- `libxcb-xfixes0:amd64=1.14-2`
- `libxcb1:amd64=1.14-2`
- `libxcb1-dev:amd64=1.14-2`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libxcb=1.14-2
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxcb/libxcb_1.14-2.dsc' libxcb_1.14-2.dsc 5344 SHA256:997dfadefa35a243a7160b62d628bb25e45439f61687459d581502905bcf1fb2
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxcb/libxcb_1.14.orig.tar.gz' libxcb_1.14.orig.tar.gz 640322 SHA256:2c7fcddd1da34d9b238c9caeda20d3bd7486456fc50b3cc6567185dbd5b0ad02
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxcb/libxcb_1.14-2.diff.gz' libxcb_1.14-2.diff.gz 25716 SHA256:92d7e0a80c3c7f2a5b5afd0c0702183f1c483338d678d67d8d0e61fd8989ba85
```

### `dpkg` source package: `libxcomposite=1:0.4.5-1`

Binary Packages:

- `libxcomposite1:amd64=1:0.4.5-1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libxcomposite=1:0.4.5-1
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxcomposite/libxcomposite_0.4.5-1.dsc' libxcomposite_0.4.5-1.dsc 2067 SHA256:41b93952b43dfac6b0acde3ea9b6330d44fd7c96b6a38eba6d82094b88e436e8
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxcomposite/libxcomposite_0.4.5.orig.tar.gz' libxcomposite_0.4.5.orig.tar.gz 386513 SHA256:581c7fc0f41a99af38b1c36b9be64bc13ef3f60091cd3f01105bbc7c01617d6c
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxcomposite/libxcomposite_0.4.5-1.diff.gz' libxcomposite_0.4.5-1.diff.gz 7547 SHA256:b5e99d78f5260a7c27ac4c166f981e8c2eab44cff08f9ab64da3ee7150854410
```

### `dpkg` source package: `libxcrypt=1:4.4.10-10ubuntu4`

Binary Packages:

- `libcrypt1:amd64=1:4.4.10-10ubuntu4`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libxcrypt=1:4.4.10-10ubuntu4
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxcrypt/libxcrypt_4.4.10-10ubuntu4.dsc' libxcrypt_4.4.10-10ubuntu4.dsc 2216 SHA256:457576b36eaa34dcf28b19e942908221d0618e9e4a2c0b9e11ba9693770756a2
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxcrypt/libxcrypt_4.4.10.orig.tar.xz' libxcrypt_4.4.10.orig.tar.xz 372652 SHA256:f790a8eac4e4af3124d2844a24a7afb3a972368e4dff63d701599c2f2d065fd3
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxcrypt/libxcrypt_4.4.10-10ubuntu4.debian.tar.xz' libxcrypt_4.4.10-10ubuntu4.debian.tar.xz 5760 SHA256:b2e665b5224911d24dbcbddfc61b7a27428c3ecb744f29ceea1b2984496f2ffa
```

### `dpkg` source package: `libxcursor=1:1.2.0-2`

Binary Packages:

- `libxcursor1:amd64=1:1.2.0-2`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libxcursor=1:1.2.0-2
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxcursor/libxcursor_1.2.0-2.dsc' libxcursor_1.2.0-2.dsc 2260 SHA256:27a4e948a79d0caf2d5d8bcd2f036c1b14b9abb933fd7c3f12f933885c0124ed
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxcursor/libxcursor_1.2.0.orig.tar.gz' libxcursor_1.2.0.orig.tar.gz 408135 SHA256:ad5b2574fccaa4c3fa67b9874fbed863d29ad230c784e9a08b20692418f6a1f8
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxcursor/libxcursor_1.2.0-2.debian.tar.xz' libxcursor_1.2.0-2.debian.tar.xz 8988 SHA256:3fd5d8bf9c47d43e416ec6533b63796616c689eecfdbfb28bb9adefdaa079e51
```

### `dpkg` source package: `libxdamage=1:1.1.5-2`

Binary Packages:

- `libxdamage1:amd64=1:1.1.5-2`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libxdamage=1:1.1.5-2
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxdamage/libxdamage_1.1.5-2.dsc' libxdamage_1.1.5-2.dsc 2124 SHA256:c2ef651aa7dad2f3f1e57a9ef8e12c72eb5f23c9e507473dccd3996a54ae229a
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxdamage/libxdamage_1.1.5.orig.tar.gz' libxdamage_1.1.5.orig.tar.gz 370586 SHA256:630ec53abb8c2d6dac5cd9f06c1f73ffb4a3167f8118fdebd77afd639dbc2019
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxdamage/libxdamage_1.1.5-2.debian.tar.xz' libxdamage_1.1.5-2.debian.tar.xz 5996 SHA256:c6333b68d2e109ea44a6031bc79e23c433b580c7ae69a9cd5cf40729896913fc
```

### `dpkg` source package: `libxdmcp=1:1.1.3-0ubuntu1`

Binary Packages:

- `libxdmcp-dev:amd64=1:1.1.3-0ubuntu1`
- `libxdmcp6:amd64=1:1.1.3-0ubuntu1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libxdmcp=1:1.1.3-0ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxdmcp/libxdmcp_1.1.3-0ubuntu1.dsc' libxdmcp_1.1.3-0ubuntu1.dsc 1608 SHA256:3f98e3917b5de252eb517c55743bcc5682b43c9f70ead33231ac4318bbc816e1
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxdmcp/libxdmcp_1.1.3.orig.tar.gz' libxdmcp_1.1.3.orig.tar.gz 429668 SHA256:2ef9653d32e09d1bf1b837d0e0311024979653fe755ad3aaada8db1aa6ea180c
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxdmcp/libxdmcp_1.1.3-0ubuntu1.diff.gz' libxdmcp_1.1.3-0ubuntu1.diff.gz 18079 SHA256:3037a57202b724ecd7db70c21a601f58277c02ba89e7e5d999973e5baf6d05ca
```

### `dpkg` source package: `libxext=2:1.3.4-0ubuntu1`

Binary Packages:

- `libxext6:amd64=2:1.3.4-0ubuntu1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libxext=2:1.3.4-0ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxext/libxext_1.3.4-0ubuntu1.dsc' libxext_1.3.4-0ubuntu1.dsc 1727 SHA256:8319de2750f28c78e01267a5593776f10afd3f863d4820abe72dbf855a3a77ae
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxext/libxext_1.3.4.orig.tar.gz' libxext_1.3.4.orig.tar.gz 494434 SHA256:8ef0789f282826661ff40a8eef22430378516ac580167da35cc948be9041aac1
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxext/libxext_1.3.4-0ubuntu1.diff.gz' libxext_1.3.4-0ubuntu1.diff.gz 20663 SHA256:87a4d23f1f9ff53f3a6cd7cc35252a1249dc63d274c566ea7e23b23585a86170
```

### `dpkg` source package: `libxfixes=1:5.0.3-2`

Binary Packages:

- `libxfixes3:amd64=1:5.0.3-2`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libxfixes=1:5.0.3-2
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxfixes/libxfixes_5.0.3-2.dsc' libxfixes_5.0.3-2.dsc 2014 SHA256:f47f131ee1d1ce791daa514fd43fe45e099f3a83462247bc75fc7ba0c8be42a6
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxfixes/libxfixes_5.0.3.orig.tar.gz' libxfixes_5.0.3.orig.tar.gz 360412 SHA256:9ab6c13590658501ce4bd965a8a5d32ba4d8b3bb39a5a5bc9901edffc5666570
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxfixes/libxfixes_5.0.3-2.diff.gz' libxfixes_5.0.3-2.diff.gz 14717 SHA256:aeb108219d65550e107d6f81dc2dac11e06363a55306f2505234190f96f1e961
```

### `dpkg` source package: `libxi=2:1.7.10-0ubuntu1`

Binary Packages:

- `libxi6:amd64=2:1.7.10-0ubuntu1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libxi=2:1.7.10-0ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxi/libxi_1.7.10-0ubuntu1.dsc' libxi_1.7.10-0ubuntu1.dsc 1701 SHA256:a02526779438726cb073d08123e03bd656a8fc514fc4df1c74b2e5e006cb8a92
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxi/libxi_1.7.10.orig.tar.gz' libxi_1.7.10.orig.tar.gz 606701 SHA256:b51e106c445a49409f3da877aa2f9129839001b24697d75a54e5c60507e9a5e3
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxi/libxi_1.7.10-0ubuntu1.diff.gz' libxi_1.7.10-0ubuntu1.diff.gz 53514 SHA256:ec42321e719d6059c1803260d6fae076d777430c4caecebea6229fe1cfa47eeb
```

### `dpkg` source package: `libxinerama=2:1.1.4-2`

Binary Packages:

- `libxinerama1:amd64=2:1.1.4-2`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libxinerama=2:1.1.4-2
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxinerama/libxinerama_1.1.4-2.dsc' libxinerama_1.1.4-2.dsc 2100 SHA256:02e4c8406fd1eae8abfe356894d95d610e2e612a761688ef5afe5e7c60d162e9
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxinerama/libxinerama_1.1.4.orig.tar.gz' libxinerama_1.1.4.orig.tar.gz 380740 SHA256:64de45e18cc76b8e703cb09b3c9d28bd16e3d05d5cd99f2d630de2d62c3acc18
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxinerama/libxinerama_1.1.4-2.diff.gz' libxinerama_1.1.4-2.diff.gz 8732 SHA256:06ce6602862839ded43d914d7dd5e5bcd7d7a1477c775f5f47a6c20b1c9b52b6
```

### `dpkg` source package: `libxkbcommon=0.10.0-1`

Binary Packages:

- `libxkbcommon0:amd64=0.10.0-1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libxkbcommon=0.10.0-1
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxkbcommon/libxkbcommon_0.10.0-1.dsc' libxkbcommon_0.10.0-1.dsc 2451 SHA256:5f60bd2c91196acba229e2d5a932798c64a18efd83577512b4fd8cd4bcdf4afa
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxkbcommon/libxkbcommon_0.10.0.orig.tar.gz' libxkbcommon_0.10.0.orig.tar.gz 600904 SHA256:54b6f38a4ce77052d343e881aafece8e993f9bfc3db35a107e9bdca9b5b1a055
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxkbcommon/libxkbcommon_0.10.0-1.diff.gz' libxkbcommon_0.10.0-1.diff.gz 29199 SHA256:8fd8e43b53ff47c240505bb0b44dbb11456a0234f69a4ae28975ed6fec6bdfd3
```

### `dpkg` source package: `libxkbfile=1:1.1.0-1`

Binary Packages:

- `libxkbfile1:amd64=1:1.1.0-1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libxkbfile=1:1.1.0-1
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxkbfile/libxkbfile_1.1.0-1.dsc' libxkbfile_1.1.0-1.dsc 2279 SHA256:d389523e40c07beb98d214e9aa417dac4c89a285b3a9873b1e7fa88d6c60857a
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxkbfile/libxkbfile_1.1.0.orig.tar.gz' libxkbfile_1.1.0.orig.tar.gz 441021 SHA256:2a92adda3992aa7cbad758ef0b8dfeaedebb49338b772c64ddf369d78c1c51d3
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxkbfile/libxkbfile_1.1.0.orig.tar.gz.asc' libxkbfile_1.1.0.orig.tar.gz.asc 801 SHA256:998e90cb1bf5cff3386ea0c1715370fd9753c4bfdb49e81c9131de6ba60aa1fa
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxkbfile/libxkbfile_1.1.0-1.diff.gz' libxkbfile_1.1.0-1.diff.gz 10049 SHA256:5d8fee55b59aba5e585a9b3b13d5b9ff93a174b2d460fef278736a589283a078
```

### `dpkg` source package: `libxml-parser-perl=2.46-1`

Binary Packages:

- `libxml-parser-perl=2.46-1`

Licenses: (parsed from: `/usr/share/doc/libxml-parser-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libxml-parser-perl=2.46-1
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxml-parser-perl/libxml-parser-perl_2.46-1.dsc' libxml-parser-perl_2.46-1.dsc 2138 SHA256:f48ca44284563a17ef4854c3d2df33d576b67e94ec92e0d8d4b2c4e388f4253c
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxml-parser-perl/libxml-parser-perl_2.46.orig.tar.gz' libxml-parser-perl_2.46.orig.tar.gz 254763 SHA256:d331332491c51cccfb4cb94ffc44f9cd73378e618498d4a37df9e043661c515d
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxml-parser-perl/libxml-parser-perl_2.46-1.debian.tar.xz' libxml-parser-perl_2.46-1.debian.tar.xz 57924 SHA256:c2cb81c489434ca33a9233b6bb30bb72bf5382bc2aa73c070c60adc18439a262
```

### `dpkg` source package: `libxml-twig-perl=1:3.50-2`

Binary Packages:

- `libxml-twig-perl=1:3.50-2`

Licenses: (parsed from: `/usr/share/doc/libxml-twig-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`
- `GPL-2`
- `GPL-2+`

Source:

```console
$ apt-get source -qq --print-uris libxml-twig-perl=1:3.50-2
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxml-twig-perl/libxml-twig-perl_3.50-2.dsc' libxml-twig-perl_3.50-2.dsc 3077 SHA256:6e777ffe6a4f5c93d39223a6bc6a9826be88e87701e67589b94b722edd1524c7
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxml-twig-perl/libxml-twig-perl_3.50.orig.tar.gz' libxml-twig-perl_3.50.orig.tar.gz 403387 SHA256:62005aced4e844651d75c2a54c2dcd8df5e32447d0b8e449c40cf6f83f382b80
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxml-twig-perl/libxml-twig-perl_3.50-2.debian.tar.xz' libxml-twig-perl_3.50-2.debian.tar.xz 7944 SHA256:a63ef53333ddfe0dd2ee224e27176328a7c5da4e98f114550a57f0464ff3c48d
```

### `dpkg` source package: `libxml-xpathengine-perl=0.14-1`

Binary Packages:

- `libxml-xpathengine-perl=0.14-1`

Licenses: (parsed from: `/usr/share/doc/libxml-xpathengine-perl/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris libxml-xpathengine-perl=0.14-1
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxml-xpathengine-perl/libxml-xpathengine-perl_0.14-1.dsc' libxml-xpathengine-perl_0.14-1.dsc 2232 SHA256:f5e132c7f5f1f8bd2616ba48f03bb12826ca3b8a6223c72237a815b207394d47
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxml-xpathengine-perl/libxml-xpathengine-perl_0.14.orig.tar.gz' libxml-xpathengine-perl_0.14.orig.tar.gz 26118 SHA256:d2fe7bcbbd0beba1444f4a733401e7b8aa5282fad4266d42735dd74582b2e264
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxml-xpathengine-perl/libxml-xpathengine-perl_0.14-1.debian.tar.xz' libxml-xpathengine-perl_0.14-1.debian.tar.xz 4680 SHA256:2e66d2548a546d97963344a04a4b8ffedc3a5206f04ca7900eba0ee49760401e
```

### `dpkg` source package: `libxml2=2.9.10+dfsg-5ubuntu0.20.04.1`

Binary Packages:

- `libxml2:amd64=2.9.10+dfsg-5ubuntu0.20.04.1`

Licenses: (parsed from: `/usr/share/doc/libxml2/copyright`)

- `ISC`
- `MIT-1`

Source:

```console
$ apt-get source -qq --print-uris libxml2=2.9.10+dfsg-5ubuntu0.20.04.1
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxml2/libxml2_2.9.10+dfsg-5ubuntu0.20.04.1.dsc' libxml2_2.9.10+dfsg-5ubuntu0.20.04.1.dsc 2947 SHA512:04713d4977b666662b0461ebe0ad53316dfc03d28410b51ff80996de9d12d30f70048d5eb21ae7f4b2d63c221637eb5e5fdc7a09d333155a0a5cfc4adf05cedd
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxml2/libxml2_2.9.10+dfsg.orig.tar.xz' libxml2_2.9.10+dfsg.orig.tar.xz 2503560 SHA512:605c6c0f8bf2c53208d0a036ff09a4025843f45139b711c90dc83066feda2f285a5578d55d4a58d33eedbe7485a5c1ec5608ba6c6beed1fb55649f87dca0cec3
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxml2/libxml2_2.9.10+dfsg-5ubuntu0.20.04.1.debian.tar.xz' libxml2_2.9.10+dfsg-5ubuntu0.20.04.1.debian.tar.xz 31560 SHA512:88f21cdf4a3dc22d9a37204ddd7953eb1b30609530ac784317018be8b0cccb8c1388a3b44794d399d3f04c0c297d1cdab593725a29ece2cbec252cfb2f01a258
```

### `dpkg` source package: `libxmu=2:1.1.3-0ubuntu1`

Binary Packages:

- `libxmu6:amd64=2:1.1.3-0ubuntu1`
- `libxmuu1:amd64=2:1.1.3-0ubuntu1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libxmu=2:1.1.3-0ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxmu/libxmu_1.1.3-0ubuntu1.dsc' libxmu_1.1.3-0ubuntu1.dsc 1797 SHA256:ba64fdbc1b602eac436ae7ea58f57d72a45ee23b016eba542ce8b704508f717c
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxmu/libxmu_1.1.3.orig.tar.gz' libxmu_1.1.3.orig.tar.gz 497343 SHA256:5bd9d4ed1ceaac9ea023d86bf1c1632cd3b172dce4a193a72a94e1d9df87a62e
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxmu/libxmu_1.1.3-0ubuntu1.diff.gz' libxmu_1.1.3-0ubuntu1.diff.gz 6373 SHA256:7519cc7be957da29adc420426b57e1366228448c6205c5e4b89d04bfa948ffa7
```

### `dpkg` source package: `libxpm=1:3.5.12-1`

Binary Packages:

- `libxpm4:amd64=1:3.5.12-1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libxpm=1:3.5.12-1
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxpm/libxpm_3.5.12-1.dsc' libxpm_3.5.12-1.dsc 2061 SHA256:1b5d07d820d656030d0f79a15a0652f258c9d2be0cd6064ec37c40853906f7e8
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxpm/libxpm_3.5.12.orig.tar.gz' libxpm_3.5.12.orig.tar.gz 529302 SHA256:2523acc780eac01db5163267b36f5b94374bfb0de26fc0b5a7bee76649fd8501
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxpm/libxpm_3.5.12-1.diff.gz' libxpm_3.5.12-1.diff.gz 9458 SHA256:4103400f8d73d0ec567f87e8aa9824c4a07d068e81da6efe54fb535ec897e326
```

### `dpkg` source package: `libxrandr=2:1.5.2-0ubuntu1`

Binary Packages:

- `libxrandr2:amd64=2:1.5.2-0ubuntu1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libxrandr=2:1.5.2-0ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxrandr/libxrandr_1.5.2-0ubuntu1.dsc' libxrandr_1.5.2-0ubuntu1.dsc 1538 SHA256:6e7f5ee8e853de4b13196a222dc004db7d521563444500dbb0c82fb4edf98877
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxrandr/libxrandr_1.5.2.orig.tar.gz' libxrandr_1.5.2.orig.tar.gz 411714 SHA256:3f10813ab355e7a09f17e147d61b0ce090d898a5ea5b5519acd0ef68675dcf8e
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxrandr/libxrandr_1.5.2-0ubuntu1.diff.gz' libxrandr_1.5.2-0ubuntu1.diff.gz 16450 SHA256:86d5c529dde1b4d9424c58957b817f78c891fe6d202a05e579e696ccbe0fce17
```

### `dpkg` source package: `libxrender=1:0.9.10-1`

Binary Packages:

- `libxrender1:amd64=1:0.9.10-1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libxrender=1:0.9.10-1
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxrender/libxrender_0.9.10-1.dsc' libxrender_0.9.10-1.dsc 2064 SHA256:95d6471218b44f4e60c48cea60cfb4865bbe861530add23f6c859515bee92dbd
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxrender/libxrender_0.9.10.orig.tar.gz' libxrender_0.9.10.orig.tar.gz 373717 SHA256:770527cce42500790433df84ec3521e8bf095dfe5079454a92236494ab296adf
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxrender/libxrender_0.9.10-1.diff.gz' libxrender_0.9.10-1.diff.gz 15399 SHA256:ff56a0a00119383adc5f1731e86155ae5c2de069e1d059a9da1d777917430588
```

### `dpkg` source package: `libxshmfence=1.3-1`

Binary Packages:

- `libxshmfence1:amd64=1.3-1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libxshmfence=1.3-1
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxshmfence/libxshmfence_1.3-1.dsc' libxshmfence_1.3-1.dsc 2096 SHA256:7da3e1195622ab34427bd5d09167b1f44ed1a3e828782fa8e618f1181c56194a
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxshmfence/libxshmfence_1.3.orig.tar.gz' libxshmfence_1.3.orig.tar.gz 378960 SHA256:7eb3d46ad91bab444f121d475b11b39273142d090f7e9ac43e6a87f4ff5f902c
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxshmfence/libxshmfence_1.3-1.diff.gz' libxshmfence_1.3-1.diff.gz 17456 SHA256:85422af90300523b8fb27e697b59418f18bd7cd5c849161fd0be64c91ce94698
```

### `dpkg` source package: `libxslt=1.1.34-4`

Binary Packages:

- `libxslt1.1:amd64=1.1.34-4`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libxslt=1.1.34-4
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxslt/libxslt_1.1.34-4.dsc' libxslt_1.1.34-4.dsc 2375 SHA256:29447f928b2fd534bd819aaf74005ff286f3786689a2684b9cbc61c1c65c2212
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxslt/libxslt_1.1.34.orig.tar.gz' libxslt_1.1.34.orig.tar.gz 3552258 SHA256:98b1bd46d6792925ad2dfe9a87452ea2adebf69dcb9919ffd55bf926a7f93f7f
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxslt/libxslt_1.1.34.orig.tar.gz.asc' libxslt_1.1.34.orig.tar.gz.asc 488 SHA256:673d1477552bdd5b0cc665704e77ca70e6be5d2f257e6a5a341c846719d747cf
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxslt/libxslt_1.1.34-4.debian.tar.xz' libxslt_1.1.34-4.debian.tar.xz 21464 SHA256:24d5490d6e33f42391d2ce449dd8fec0830ced115a6af886ee03af985a727dc9
```

### `dpkg` source package: `libxss=1:1.2.3-1`

Binary Packages:

- `libxss1:amd64=1:1.2.3-1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libxss=1:1.2.3-1
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxss/libxss_1.2.3-1.dsc' libxss_1.2.3-1.dsc 2203 SHA256:783dbcd49a0934d994693af676ee98734dad070ab2434a6afe831c2de0ecca1d
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxss/libxss_1.2.3.orig.tar.gz' libxss_1.2.3.orig.tar.gz 385215 SHA256:4f74e7e412144591d8e0616db27f433cfc9f45aae6669c6c4bb03e6bf9be809a
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxss/libxss_1.2.3.orig.tar.gz.asc' libxss_1.2.3.orig.tar.gz.asc 705 SHA256:4e900524d56c8e7263365267efa91bb3671110c9eb28ccab58f70e2188f0b91b
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxss/libxss_1.2.3-1.diff.gz' libxss_1.2.3-1.diff.gz 7145 SHA256:9d381b48f1377f27c506113e1f9b7d6ee286b856421f7f2b27017f01dccfef04
```

### `dpkg` source package: `libxt=1:1.1.5-1`

Binary Packages:

- `libxt-dev:amd64=1:1.1.5-1`
- `libxt6:amd64=1:1.1.5-1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libxt=1:1.1.5-1
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxt/libxt_1.1.5-1.dsc' libxt_1.1.5-1.dsc 2109 SHA256:f44ae1393c9fd02c0b3dd03576c7b26e6c7b09de3271a87e018efadeed311639
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxt/libxt_1.1.5.orig.tar.gz' libxt_1.1.5.orig.tar.gz 962169 SHA256:b59bee38a9935565fa49dc1bfe84cb30173e2e07e1dcdf801430d4b54eb0caa3
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxt/libxt_1.1.5-1.diff.gz' libxt_1.1.5-1.diff.gz 14462 SHA256:822fe813d1ea9213e6fde91cbb607c0b6874341dc19b77b0f6649b8be8472d82
```

### `dpkg` source package: `libxtst=2:1.2.3-1`

Binary Packages:

- `libxtst6:amd64=2:1.2.3-1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libxtst=2:1.2.3-1
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxtst/libxtst_1.2.3-1.dsc' libxtst_1.2.3-1.dsc 2243 SHA256:979f05e505ea319c3f75955e10345338f77a512f5a6a0a887d6f4633d6bd4633
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxtst/libxtst_1.2.3.orig.tar.gz' libxtst_1.2.3.orig.tar.gz 400197 SHA256:a0c83acce02d4923018c744662cb28eb0dbbc33b4adc027726879ccf68fbc2c2
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxtst/libxtst_1.2.3-1.diff.gz' libxtst_1.2.3-1.diff.gz 10177 SHA256:c4739fc7ccda7caaffcf36f934b7c33463390e71d567c7d62f635db1946b74ed
```

### `dpkg` source package: `libxv=2:1.0.11-1`

Binary Packages:

- `libxv1:amd64=2:1.0.11-1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libxv=2:1.0.11-1
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxv/libxv_1.0.11-1.dsc' libxv_1.0.11-1.dsc 1959 SHA256:7753e8d4496ec0d3f32417b03cfc8b344e2dff486e46f630158a6a52e4bd8542
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxv/libxv_1.0.11.orig.tar.gz' libxv_1.0.11.orig.tar.gz 387057 SHA256:c4112532889b210e21cf05f46f0f2f8354ff7e1b58061e12d7a76c95c0d47bb1
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxv/libxv_1.0.11-1.diff.gz' libxv_1.0.11-1.diff.gz 8235 SHA256:529ed2bcbccc9340c9c7987e8c5ed933a0fa41d6e4e67ef71ce3925ac83d93b6
```

### `dpkg` source package: `libxxf86dga=2:1.1.5-0ubuntu1`

Binary Packages:

- `libxxf86dga1:amd64=2:1.1.5-0ubuntu1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libxxf86dga=2:1.1.5-0ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxxf86dga/libxxf86dga_1.1.5-0ubuntu1.dsc' libxxf86dga_1.1.5-0ubuntu1.dsc 1652 SHA256:08734d98453a31f345a208d3a1bdcd8a714d2655ab4984902b93317a20370f7c
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxxf86dga/libxxf86dga_1.1.5.orig.tar.gz' libxxf86dga_1.1.5.orig.tar.gz 389388 SHA256:715e2bf5caf6276f0858eb4b11a1aef1a26beeb40dce2942387339da395bef69
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxxf86dga/libxxf86dga_1.1.5-0ubuntu1.diff.gz' libxxf86dga_1.1.5-0ubuntu1.diff.gz 14977 SHA256:4e9d6ac475ff331dc634f71755eac415cdaab57545810f3835019470422a0e9d
```

### `dpkg` source package: `libxxf86vm=1:1.1.4-1build1`

Binary Packages:

- `libxxf86vm1:amd64=1:1.1.4-1build1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris libxxf86vm=1:1.1.4-1build1
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxxf86vm/libxxf86vm_1.1.4-1build1.dsc' libxxf86vm_1.1.4-1build1.dsc 2141 SHA256:ce09c08963029e55668fc69ec2ae963aa6301841968ebd544cbf8f74769988be
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxxf86vm/libxxf86vm_1.1.4.orig.tar.gz' libxxf86vm_1.1.4.orig.tar.gz 363146 SHA256:5108553c378a25688dcb57dca383664c36e293d60b1505815f67980ba9318a99
'http://archive.ubuntu.com/ubuntu/pool/main/libx/libxxf86vm/libxxf86vm_1.1.4-1build1.diff.gz' libxxf86vm_1.1.4-1build1.diff.gz 8101 SHA256:8bfdacf91f781ddad0b2cc13cb00c3135383cbe07682571be09575d968b8bb95
```

### `dpkg` source package: `libzstd=1.4.4+dfsg-3ubuntu0.1`

Binary Packages:

- `libzstd1:amd64=1.4.4+dfsg-3ubuntu0.1`

Licenses: (parsed from: `/usr/share/doc/libzstd1/copyright`)

- `BSD-3-clause`
- `Expat`
- `GPL-2`
- `GPL-2+`
- `zlib`

Source:

```console
$ apt-get source -qq --print-uris libzstd=1.4.4+dfsg-3ubuntu0.1
'http://archive.ubuntu.com/ubuntu/pool/main/libz/libzstd/libzstd_1.4.4+dfsg-3ubuntu0.1.dsc' libzstd_1.4.4+dfsg-3ubuntu0.1.dsc 2381 SHA512:a135412be4afdea573f991d8e4822f9885dbd607c87fb22e72d2defa160cf64f85a6047a9c9120b6eda3b8927306407278779f9e7a6976d7b15fb08750f32f74
'http://archive.ubuntu.com/ubuntu/pool/main/libz/libzstd/libzstd_1.4.4+dfsg.orig.tar.xz' libzstd_1.4.4+dfsg.orig.tar.xz 1357144 SHA512:85c64662303dda72d61fcbe41dfc6b310e63b20b043f41d4fb5a5ebc38ea83986c8c217fb259dfc2c024538ee8a519bb944914542a0b3a5c4dd988d5fdb248b7
'http://archive.ubuntu.com/ubuntu/pool/main/libz/libzstd/libzstd_1.4.4+dfsg-3ubuntu0.1.debian.tar.xz' libzstd_1.4.4+dfsg-3ubuntu0.1.debian.tar.xz 17300 SHA512:0484891be5603d00bd57b799c708b9395fccbaa8c6c44f535377f6fa2c7ac22c01c8a3c1b45e1c1f3c30f19dc74d510626bf82067fcbfb53c39f1bcc2249affe
```

### `dpkg` source package: `lilv=0.24.6-1ubuntu0.1`

Binary Packages:

- `liblilv-0-0:amd64=0.24.6-1ubuntu0.1`

Licenses: (parsed from: `/usr/share/doc/liblilv-0-0/copyright`)

- `BSD-3-clause`
- `ISC`

Source:

```console
$ apt-get source -qq --print-uris lilv=0.24.6-1ubuntu0.1
'http://archive.ubuntu.com/ubuntu/pool/universe/l/lilv/lilv_0.24.6-1ubuntu0.1.dsc' lilv_0.24.6-1ubuntu0.1.dsc 1929 SHA512:785f89c0055bd1cfd7788f43c1e0657c187aebd6bb1da7effd695e511369a031b572a0519493040a7a87702a3b23c6fabe1ea0ceeaf3362f9fbf6ebc9ae103a4
'http://archive.ubuntu.com/ubuntu/pool/universe/l/lilv/lilv_0.24.6.orig.tar.bz2' lilv_0.24.6.orig.tar.bz2 541765 SHA512:0cf89d7dac97727e744579d3f4f4b330f56cf72f1a30bd03abfd645233aaf04e9bbf2abf460a0e4b963ed40e1ae78ed5f36127553bdab16e3e7dd3e93b70cdad
'http://archive.ubuntu.com/ubuntu/pool/universe/l/lilv/lilv_0.24.6-1ubuntu0.1.debian.tar.xz' lilv_0.24.6-1ubuntu0.1.debian.tar.xz 18808 SHA512:c7145aebd247bc912107aed05ea5ae4a660cc02971272d3177bdfe7ff98e079ea35fd60c6d88f1e9c95ebb8ecf41fe463112a5c703db0bf6b23fadc279f43bff
```

### `dpkg` source package: `llvm-toolchain-12=1:12.0.0-3ubuntu1~20.04.4`

Binary Packages:

- `libllvm12:amd64=1:12.0.0-3ubuntu1~20.04.4`

Licenses: (parsed from: `/usr/share/doc/libllvm12/copyright`)

- `APACHE-2-LLVM-EXCEPTIONS`
- `Apache-2.0`
- `BSD-3-Clause`
- `BSD-3-clause`
- `MIT`
- `Python`
- `solar-public-domain`

Source:

```console
$ apt-get source -qq --print-uris llvm-toolchain-12=1:12.0.0-3ubuntu1~20.04.4
'http://archive.ubuntu.com/ubuntu/pool/main/l/llvm-toolchain-12/llvm-toolchain-12_12.0.0-3ubuntu1~20.04.4.dsc' llvm-toolchain-12_12.0.0-3ubuntu1~20.04.4.dsc 6307 SHA512:f9d8573a6df9a213ebcba4dd33b64d2d3bb4f8a57268082aa571504c467be5de07fb45e6a5bbc7300f1b2e165c6c84a093b0230e764c3162fe963d5f8704d36b
'http://archive.ubuntu.com/ubuntu/pool/main/l/llvm-toolchain-12/llvm-toolchain-12_12.0.0.orig.tar.xz' llvm-toolchain-12_12.0.0.orig.tar.xz 86089384 SHA512:34083aa8541f03c7a6408b3ac1a9305f5fad5da6c843396521652271f653c78a86a798e501ee17226c133cc3690d054aa086d9b371266afc4859072f6afc4dd6
'http://archive.ubuntu.com/ubuntu/pool/main/l/llvm-toolchain-12/llvm-toolchain-12_12.0.0-3ubuntu1~20.04.4.debian.tar.xz' llvm-toolchain-12_12.0.0-3ubuntu1~20.04.4.debian.tar.xz 127556 SHA512:34138a7f9164b6ac987d7f1368de932b676acf192c3a1c135f61b03541e406e3fae65415ff7c6733b54f9043352a1f21c8927d17ea6c1c096ca8443baf7bcebe
```

### `dpkg` source package: `lm-sensors=1:3.6.0-2ubuntu1`

Binary Packages:

- `libsensors-config=1:3.6.0-2ubuntu1`
- `libsensors5:amd64=1:3.6.0-2ubuntu1`

Licenses: (parsed from: `/usr/share/doc/libsensors-config/copyright`, `/usr/share/doc/libsensors5/copyright`)

- `GPL-2`
- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris lm-sensors=1:3.6.0-2ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/l/lm-sensors/lm-sensors_3.6.0-2ubuntu1.dsc' lm-sensors_3.6.0-2ubuntu1.dsc 2111 SHA256:3e835b76c8e00eaaebb506a3ca16ac65d233eb180fac22a6af8e345cae496bc0
'http://archive.ubuntu.com/ubuntu/pool/main/l/lm-sensors/lm-sensors_3.6.0.orig.tar.gz' lm-sensors_3.6.0.orig.tar.gz 273209 SHA256:0591f9fa0339f0d15e75326d0365871c2d4e2ed8aa1ff759b3a55d3734b7d197
'http://archive.ubuntu.com/ubuntu/pool/main/l/lm-sensors/lm-sensors_3.6.0-2ubuntu1.debian.tar.xz' lm-sensors_3.6.0-2ubuntu1.debian.tar.xz 25792 SHA256:7150f4d1b87644031d9dc2f529ddde8242a857947cce65812dc4e3262bce9141
```

### `dpkg` source package: `lp-solve=5.5.0.15-4build1`

Binary Packages:

- `lp-solve=5.5.0.15-4build1`

Licenses: (parsed from: `/usr/share/doc/lp-solve/copyright`)

- `LGPL`

Source:

```console
$ apt-get source -qq --print-uris lp-solve=5.5.0.15-4build1
'http://archive.ubuntu.com/ubuntu/pool/main/l/lp-solve/lp-solve_5.5.0.15-4build1.dsc' lp-solve_5.5.0.15-4build1.dsc 2242 SHA256:54793e62079b91ae7e47a7a282650ec46740ed9881284efe0e5b39090ac7a267
'http://archive.ubuntu.com/ubuntu/pool/main/l/lp-solve/lp-solve_5.5.0.15.orig-doc.tar.gz' lp-solve_5.5.0.15.orig-doc.tar.gz 1484929 SHA256:a9dcfa62148a283a6e11c0bb9524f4d5a4a4ecf06511e32cbd2faec04f791e17
'http://archive.ubuntu.com/ubuntu/pool/main/l/lp-solve/lp-solve_5.5.0.15.orig.tar.gz' lp-solve_5.5.0.15.orig.tar.gz 802881 SHA256:ea1243e8aa2f0d52172dc0a90d1c2a8d2a4f696a39fc9cf07321810363d18985
'http://archive.ubuntu.com/ubuntu/pool/main/l/lp-solve/lp-solve_5.5.0.15-4build1.debian.tar.xz' lp-solve_5.5.0.15-4build1.debian.tar.xz 9716 SHA256:c8d47e23c925e601669624e6b90f577596f4a5d534b71f31b5093621f6315469
```

### `dpkg` source package: `lsb=11.1.0ubuntu2`

Binary Packages:

- `lsb-base=11.1.0ubuntu2`

Licenses: (parsed from: `/usr/share/doc/lsb-base/copyright`)

- `BSD-3-clause`
- `GPL-2`

Source:

```console
$ apt-get source -qq --print-uris lsb=11.1.0ubuntu2
'http://archive.ubuntu.com/ubuntu/pool/main/l/lsb/lsb_11.1.0ubuntu2.dsc' lsb_11.1.0ubuntu2.dsc 2230 SHA256:983ff4ab1ab2b39af974e4b8f4373ab4028d0ee5a409e7cd40401fa8e6ecabde
'http://archive.ubuntu.com/ubuntu/pool/main/l/lsb/lsb_11.1.0ubuntu2.tar.xz' lsb_11.1.0ubuntu2.tar.xz 46024 SHA256:c6ab63b6702dc633988690aacde8ece3e460f8acd8f1af8e6a67ab2fe0798f41
```

### `dpkg` source package: `lvm2=2.03.07-1ubuntu1`

Binary Packages:

- `dmsetup=2:1.02.167-1ubuntu1`
- `libdevmapper1.02.1:amd64=2:1.02.167-1ubuntu1`

Licenses: (parsed from: `/usr/share/doc/dmsetup/copyright`, `/usr/share/doc/libdevmapper1.02.1/copyright`)

- `BSD-2-Clause`
- `GPL-2`
- `GPL-2.0`
- `GPL-2.0+`
- `LGPL-2`
- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris lvm2=2.03.07-1ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/l/lvm2/lvm2_2.03.07-1ubuntu1.dsc' lvm2_2.03.07-1ubuntu1.dsc 3169 SHA256:61da0e6fe82073f75bbc864ab999369bf864155eb0e80f7051d3172093862e19
'http://archive.ubuntu.com/ubuntu/pool/main/l/lvm2/lvm2_2.03.07.orig.tar.xz' lvm2_2.03.07.orig.tar.xz 1658348 SHA256:c25bbe5c30c96e48f24bc0de645022c306acbb3ab0b44553d25e816befca7c32
'http://archive.ubuntu.com/ubuntu/pool/main/l/lvm2/lvm2_2.03.07-1ubuntu1.debian.tar.xz' lvm2_2.03.07-1ubuntu1.debian.tar.xz 42924 SHA256:fc89edc32f4ce3e71dc97dc4fa8ba72e8b848d952718094519bcff603b81bb9f
```

### `dpkg` source package: `lz4=1.9.2-2ubuntu0.20.04.1`

Binary Packages:

- `liblz4-1:amd64=1.9.2-2ubuntu0.20.04.1`

Licenses: (parsed from: `/usr/share/doc/liblz4-1/copyright`)

- `BSD-2-clause`
- `GPL-2`
- `GPL-2+`

Source:

```console
$ apt-get source -qq --print-uris lz4=1.9.2-2ubuntu0.20.04.1
'http://archive.ubuntu.com/ubuntu/pool/main/l/lz4/lz4_1.9.2-2ubuntu0.20.04.1.dsc' lz4_1.9.2-2ubuntu0.20.04.1.dsc 2095 SHA512:249c1370a5e277575429a778fe2be185a997eb82eb77e88f83da38ddb271956ff1d2ae96403c599d430ed13a0f37e125b4410d21e3d42fe2d47a1a376bff70ad
'http://archive.ubuntu.com/ubuntu/pool/main/l/lz4/lz4_1.9.2.orig.tar.gz' lz4_1.9.2.orig.tar.gz 305796 SHA512:ae714c61ec8e33ed91359b63f2896cfa102d66b730dce112b74696ec5850e59d88bd5527173e01e354a70fbe8f036557a47c767ee0766bc5f9c257978116c3c1
'http://archive.ubuntu.com/ubuntu/pool/main/l/lz4/lz4_1.9.2-2ubuntu0.20.04.1.debian.tar.xz' lz4_1.9.2-2ubuntu0.20.04.1.debian.tar.xz 13228 SHA512:330f522c3afd0c9a36c6d8b882cfd59aa32258906ad6bbcab3a5bcd4a530ce226905d8108f384615dedd749dc5faaa45f320b0eda98effabee433e97124fabc0
```

### `dpkg` source package: `mawk=1.3.4.20200120-2`

Binary Packages:

- `mawk=1.3.4.20200120-2`

Licenses: (parsed from: `/usr/share/doc/mawk/copyright`)

- `GPL-2`

Source:

```console
$ apt-get source -qq --print-uris mawk=1.3.4.20200120-2
'http://archive.ubuntu.com/ubuntu/pool/main/m/mawk/mawk_1.3.4.20200120-2.dsc' mawk_1.3.4.20200120-2.dsc 1915 SHA256:5069c46872ac74f5221250dfb88b31b1f2dbb8a2617c1e013f8f80cc34638c6d
'http://archive.ubuntu.com/ubuntu/pool/main/m/mawk/mawk_1.3.4.20200120.orig.tar.gz' mawk_1.3.4.20200120.orig.tar.gz 468855 SHA256:7fd4cd1e1fae9290fe089171181bbc6291dfd9bca939ca804f0ddb851c8b8237
'http://archive.ubuntu.com/ubuntu/pool/main/m/mawk/mawk_1.3.4.20200120-2.debian.tar.xz' mawk_1.3.4.20200120-2.debian.tar.xz 7504 SHA256:b772ed2f016b0286980c46cbc1f1f4ae62887ef2aa3dff6ef10cae638f923f26
```

### `dpkg` source package: `mesa=21.0.3-0ubuntu0.3~20.04.3`

Binary Packages:

- `libegl-mesa0:amd64=21.0.3-0ubuntu0.3~20.04.3`
- `libgbm1:amd64=21.0.3-0ubuntu0.3~20.04.3`
- `libgl1-mesa-dri:amd64=21.0.3-0ubuntu0.3~20.04.3`
- `libglapi-mesa:amd64=21.0.3-0ubuntu0.3~20.04.3`
- `libglx-mesa0:amd64=21.0.3-0ubuntu0.3~20.04.3`
- `mesa-va-drivers:amd64=21.0.3-0ubuntu0.3~20.04.3`
- `mesa-vdpau-drivers:amd64=21.0.3-0ubuntu0.3~20.04.3`
- `mesa-vulkan-drivers:amd64=21.0.3-0ubuntu0.3~20.04.3`

Licenses: (parsed from: `/usr/share/doc/libegl-mesa0/copyright`, `/usr/share/doc/libgbm1/copyright`, `/usr/share/doc/libgl1-mesa-dri/copyright`, `/usr/share/doc/libglapi-mesa/copyright`, `/usr/share/doc/libglx-mesa0/copyright`, `/usr/share/doc/mesa-va-drivers/copyright`, `/usr/share/doc/mesa-vdpau-drivers/copyright`, `/usr/share/doc/mesa-vulkan-drivers/copyright`)

- `Apache-2.0`
- `BSD-2-clause`
- `BSD-3-google`
- `BSL`
- `GPL`
- `Khronos`
- `MIT`
- `MLAA`
- `SGI`

Source:

```console
$ apt-get source -qq --print-uris mesa=21.0.3-0ubuntu0.3~20.04.3
'http://archive.ubuntu.com/ubuntu/pool/main/m/mesa/mesa_21.0.3-0ubuntu0.3~20.04.3.dsc' mesa_21.0.3-0ubuntu0.3~20.04.3.dsc 5523 SHA512:20eae21266901b508125e346d07541c881a2b444d9b8fcfe5a20a2b56edaa85218712c5dd5e5ebeca7d13b7b42b82abd123f485aebfc8d5cfed84f5688fe1024
'http://archive.ubuntu.com/ubuntu/pool/main/m/mesa/mesa_21.0.3.orig.tar.gz' mesa_21.0.3.orig.tar.gz 22634551 SHA512:bcd4cfdc624f2e8adedaaa71b0bbc506ef96e7b9122dbc4ef94bdd544ca13de5b02d2497ad9a646f46d5b290f689b927eb87272d36327f1a1f336c0bd67bb5af
'http://archive.ubuntu.com/ubuntu/pool/main/m/mesa/mesa_21.0.3-0ubuntu0.3~20.04.3.diff.gz' mesa_21.0.3-0ubuntu0.3~20.04.3.diff.gz 121491 SHA512:37af3e5c2a675bafa5e3dbec3e57f82415519cbf138780b3ee2afe870d5abe67ff269d7cf17173abd278eca33b2cbd8d47556b0449442018bfbe8f17e1c8be3f
```

### `dpkg` source package: `mhash=0.9.9.9-8`

Binary Packages:

- `libmhash2:amd64=0.9.9.9-8`

Licenses: (parsed from: `/usr/share/doc/libmhash2/copyright`)

- `LGPL-2`

Source:

```console
$ apt-get source -qq --print-uris mhash=0.9.9.9-8
'http://archive.ubuntu.com/ubuntu/pool/main/m/mhash/mhash_0.9.9.9-8.dsc' mhash_0.9.9.9-8.dsc 1904 SHA256:26474c432fffc2827b04f78e2e47e55222fd610911f8718eff4dbf24190bc987
'http://archive.ubuntu.com/ubuntu/pool/main/m/mhash/mhash_0.9.9.9.orig.tar.gz' mhash_0.9.9.9.orig.tar.gz 577533 SHA256:73991e9e54bb392484a510943d4c5d395462181cc4abe53f863edec13c335403
'http://archive.ubuntu.com/ubuntu/pool/main/m/mhash/mhash_0.9.9.9-8.debian.tar.xz' mhash_0.9.9.9-8.debian.tar.xz 11996 SHA256:28b964a9b978ab4c5784aca2381cab6f6efaf37a295cf048265614963840e839
```

### `dpkg` source package: `mime-support=3.64ubuntu1`

Binary Packages:

- `mime-support=3.64ubuntu1`

Licenses: (parsed from: `/usr/share/doc/mime-support/copyright`)

- `Bellcore`
- `ad-hoc`

Source:

```console
$ apt-get source -qq --print-uris mime-support=3.64ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/m/mime-support/mime-support_3.64ubuntu1.dsc' mime-support_3.64ubuntu1.dsc 1729 SHA256:669ba4f3fd7594f1c32731b5636b499f44f21c7667148f6f0d16043708743fdc
'http://archive.ubuntu.com/ubuntu/pool/main/m/mime-support/mime-support_3.64ubuntu1.tar.xz' mime-support_3.64ubuntu1.tar.xz 33980 SHA256:5007d2ebc25935bfca6d4bdac0efdfc089a38c1be49d19f0422559f666e4f2c4
```

### `dpkg` source package: `mpclib3=1.1.0-1`

Binary Packages:

- `libmpc3:amd64=1.1.0-1`

Licenses: (parsed from: `/usr/share/doc/libmpc3/copyright`)

- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris mpclib3=1.1.0-1
'http://archive.ubuntu.com/ubuntu/pool/main/m/mpclib3/mpclib3_1.1.0-1.dsc' mpclib3_1.1.0-1.dsc 1990 SHA256:bb57824015b735bf72399a53f8c6a241e6a8bd402753b0fdcdaa5b99d0aef790
'http://archive.ubuntu.com/ubuntu/pool/main/m/mpclib3/mpclib3_1.1.0.orig.tar.gz' mpclib3_1.1.0.orig.tar.gz 701263 SHA256:6985c538143c1208dcb1ac42cedad6ff52e267b47e5f970183a3e75125b43c2e
'http://archive.ubuntu.com/ubuntu/pool/main/m/mpclib3/mpclib3_1.1.0-1.diff.gz' mpclib3_1.1.0-1.diff.gz 3794 SHA256:84b10a4ae958b3015e136b75be5fee22961255d19be655f7d0adae8d4f3bc977
```

### `dpkg` source package: `mpdecimal=2.4.2-3`

Binary Packages:

- `libmpdec2:amd64=2.4.2-3`

Licenses: (parsed from: `/usr/share/doc/libmpdec2/copyright`)

- `BSD`
- `GPL-2`
- `GPL-2+`

Source:

```console
$ apt-get source -qq --print-uris mpdecimal=2.4.2-3
'http://archive.ubuntu.com/ubuntu/pool/main/m/mpdecimal/mpdecimal_2.4.2-3.dsc' mpdecimal_2.4.2-3.dsc 1932 SHA256:4cdd04de9915af3c9d787f4922affc1993d76c25cd0715ffdd2658da37c86753
'http://archive.ubuntu.com/ubuntu/pool/main/m/mpdecimal/mpdecimal_2.4.2.orig.tar.gz' mpdecimal_2.4.2.orig.tar.gz 2271529 SHA256:83c628b90f009470981cf084c5418329c88b19835d8af3691b930afccb7d79c7
'http://archive.ubuntu.com/ubuntu/pool/main/m/mpdecimal/mpdecimal_2.4.2-3.debian.tar.xz' mpdecimal_2.4.2-3.debian.tar.xz 6352 SHA256:1baf12776a911bc77f76e16aa7600d4ace21a27817f4a56373093065205a9292
```

### `dpkg` source package: `mpfr4=4.0.2-1`

Binary Packages:

- `libmpfr6:amd64=4.0.2-1`

Licenses: (parsed from: `/usr/share/doc/libmpfr6/copyright`)

- `GFDL-1.2`
- `LGPL-3`

Source:

```console
$ apt-get source -qq --print-uris mpfr4=4.0.2-1
'http://archive.ubuntu.com/ubuntu/pool/main/m/mpfr4/mpfr4_4.0.2-1.dsc' mpfr4_4.0.2-1.dsc 1972 SHA256:9021ec2462ed0e73ea1379266740473abf5f826be819226497729f6c6b02e672
'http://archive.ubuntu.com/ubuntu/pool/main/m/mpfr4/mpfr4_4.0.2.orig.tar.xz' mpfr4_4.0.2.orig.tar.xz 1441996 SHA256:1d3be708604eae0e42d578ba93b390c2a145f17743a744d8f3f8c2ad5855a38a
'http://archive.ubuntu.com/ubuntu/pool/main/m/mpfr4/mpfr4_4.0.2-1.debian.tar.xz' mpfr4_4.0.2-1.debian.tar.xz 10544 SHA256:99c4d35654f33340f0efdec67142a34753157b20334cadad9018f5eab29738da
```

### `dpkg` source package: `mpg123=1.25.13-1`

Binary Packages:

- `libmpg123-0:amd64=1.25.13-1`

Licenses: (parsed from: `/usr/share/doc/libmpg123-0/copyright`)

- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris mpg123=1.25.13-1
'http://archive.ubuntu.com/ubuntu/pool/main/m/mpg123/mpg123_1.25.13-1.dsc' mpg123_1.25.13-1.dsc 2533 SHA256:68479f2f2aabd87af24f55b12f3f40691c24bd567b5fa9594e90f5430ea62ed1
'http://archive.ubuntu.com/ubuntu/pool/main/m/mpg123/mpg123_1.25.13.orig.tar.bz2' mpg123_1.25.13.orig.tar.bz2 910176 SHA256:90306848359c793fd43b9906e52201df18775742dc3c81c06ab67a806509890a
'http://archive.ubuntu.com/ubuntu/pool/main/m/mpg123/mpg123_1.25.13.orig.tar.bz2.asc' mpg123_1.25.13.orig.tar.bz2.asc 833 SHA256:86621d3e6f9c792d3d6f288175f284f20662520669c1faa308064416cb90465b
'http://archive.ubuntu.com/ubuntu/pool/main/m/mpg123/mpg123_1.25.13-1.debian.tar.xz' mpg123_1.25.13-1.debian.tar.xz 23768 SHA256:34a93a2afcbd3c7529a7c9c3c7f24b310a6f2921ce21e7b3325d497a18601fdb
```

### `dpkg` source package: `mysql-8.0=8.0.26-0ubuntu0.20.04.3`

Binary Packages:

- `libmysqlclient21:amd64=8.0.26-0ubuntu0.20.04.3`

Licenses: (parsed from: `/usr/share/doc/libmysqlclient21/copyright`)

- `Artistic`
- `BSD-2-clause`
- `BSD-3-clause`
- `BSD-like`
- `Boost-1.0`
- `GPL-2`
- `GPL-2+`
- `ISC`
- `LGPL`
- `LGPL-2`
- `public-domain`
- `zlib/libpng`

**WARNING:** unable to find source (`apt-get source` failed or returned no results)!  
This is *usually* due to a new package version being released and the old version being removed.


### `dpkg` source package: `mysql-defaults=1.0.5ubuntu2`

Binary Packages:

- `mysql-common=5.8+1.0.5ubuntu2`

Licenses: (parsed from: `/usr/share/doc/mysql-common/copyright`)

- `GPL-2`
- `GPL-2+`

Source:

```console
$ apt-get source -qq --print-uris mysql-defaults=1.0.5ubuntu2
'http://archive.ubuntu.com/ubuntu/pool/main/m/mysql-defaults/mysql-defaults_1.0.5ubuntu2.dsc' mysql-defaults_1.0.5ubuntu2.dsc 2251 SHA256:788762eca77d2718a5ecc8e5fc49f90b32e81639a4a06169789e8f34fc35d379
'http://archive.ubuntu.com/ubuntu/pool/main/m/mysql-defaults/mysql-defaults_1.0.5ubuntu2.tar.xz' mysql-defaults_1.0.5ubuntu2.tar.xz 7168 SHA256:d1b17de186bf8afba5cfc0041ab3c3646dbbed653e72010e2222bb52396e54c0
```

### `dpkg` source package: `mythes=2:1.2.4-3build1`

Binary Packages:

- `libmythes-1.2-0:amd64=2:1.2.4-3build1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris mythes=2:1.2.4-3build1
'http://archive.ubuntu.com/ubuntu/pool/main/m/mythes/mythes_1.2.4-3build1.dsc' mythes_1.2.4-3build1.dsc 1909 SHA256:6da8cdbbc943c304c3599fb438b5fc73b4bd56655df06af7580ef13b665b9f2f
'http://archive.ubuntu.com/ubuntu/pool/main/m/mythes/mythes_1.2.4.orig.tar.gz' mythes_1.2.4.orig.tar.gz 4910303 SHA256:1e81f395d8c851c3e4e75b568e20fa2fa549354e75ab397f9de4b0e0790a305f
'http://archive.ubuntu.com/ubuntu/pool/main/m/mythes/mythes_1.2.4-3build1.debian.tar.xz' mythes_1.2.4-3build1.debian.tar.xz 5128 SHA256:dbf1f93698963e8924803fe85fc3baaeb958b2641c8e65cd8d64997a861c67b6
```

### `dpkg` source package: `ncurses=6.2-0ubuntu2`

Binary Packages:

- `libncurses6:amd64=6.2-0ubuntu2`
- `libncursesw6:amd64=6.2-0ubuntu2`
- `libtinfo6:amd64=6.2-0ubuntu2`
- `ncurses-base=6.2-0ubuntu2`
- `ncurses-bin=6.2-0ubuntu2`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris ncurses=6.2-0ubuntu2
'http://archive.ubuntu.com/ubuntu/pool/main/n/ncurses/ncurses_6.2-0ubuntu2.dsc' ncurses_6.2-0ubuntu2.dsc 3831 SHA256:b580e8d50864a61bad0cedb17c8005ec6c24cd85d8ebbe472d1170552c8cd3bd
'http://archive.ubuntu.com/ubuntu/pool/main/n/ncurses/ncurses_6.2.orig.tar.gz' ncurses_6.2.orig.tar.gz 3425862 SHA256:30306e0c76e0f9f1f0de987cf1c82a5c21e1ce6568b9227f7da5b71cbea86c9d
'http://archive.ubuntu.com/ubuntu/pool/main/n/ncurses/ncurses_6.2-0ubuntu2.debian.tar.xz' ncurses_6.2-0ubuntu2.debian.tar.xz 61192 SHA256:3377d203f2ab08b119ed22ac420152f3c28872201e35b25e62dfe07641ed750a
```

### `dpkg` source package: `neon27=0.30.2-4`

Binary Packages:

- `libneon27-gnutls:amd64=0.30.2-4`

Licenses: (parsed from: `/usr/share/doc/libneon27-gnutls/copyright`)

- `GPL-2`
- `LGPL-2`

Source:

```console
$ apt-get source -qq --print-uris neon27=0.30.2-4
'http://archive.ubuntu.com/ubuntu/pool/main/n/neon27/neon27_0.30.2-4.dsc' neon27_0.30.2-4.dsc 2158 SHA256:b6b52513403a28e68ceb801425bc938b3fcc6fcbecddc2a0d51ba2433a9798e0
'http://archive.ubuntu.com/ubuntu/pool/main/n/neon27/neon27_0.30.2.orig.tar.gz' neon27_0.30.2.orig.tar.gz 932779 SHA256:db0bd8cdec329b48f53a6f00199c92d5ba40b0f015b153718d1b15d3d967fbca
'http://archive.ubuntu.com/ubuntu/pool/main/n/neon27/neon27_0.30.2-4.debian.tar.xz' neon27_0.30.2-4.debian.tar.xz 13848 SHA256:1e13ba75874a8c8187cae1bf739a8662dce5d4e587e899f56195a5aa78780c34
```

### `dpkg` source package: `net-tools=1.60+git20180626.aebd88e-1ubuntu1`

Binary Packages:

- `net-tools=1.60+git20180626.aebd88e-1ubuntu1`

Licenses: (parsed from: `/usr/share/doc/net-tools/copyright`)

- `GPL-2`
- `GPL-2+`

Source:

```console
$ apt-get source -qq --print-uris net-tools=1.60+git20180626.aebd88e-1ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/n/net-tools/net-tools_1.60+git20180626.aebd88e-1ubuntu1.dsc' net-tools_1.60+git20180626.aebd88e-1ubuntu1.dsc 2218 SHA256:63cacbc58a0a2fa6f6f9866df17a94b052ce7236def1007fbac5de16af6e90ad
'http://archive.ubuntu.com/ubuntu/pool/main/n/net-tools/net-tools_1.60+git20180626.aebd88e.orig.tar.gz' net-tools_1.60+git20180626.aebd88e.orig.tar.gz 288458 SHA256:ac85b0381922ad8ecbd004192a0f7b0b22ec11834862182f18e21aa3007d9d8e
'http://archive.ubuntu.com/ubuntu/pool/main/n/net-tools/net-tools_1.60+git20180626.aebd88e-1ubuntu1.debian.tar.xz' net-tools_1.60+git20180626.aebd88e-1ubuntu1.debian.tar.xz 58808 SHA256:d7e6188b66c988df26bd1e29747eb49e7e65fd0392e4d129156617f2b5365c47
```

### `dpkg` source package: `netbase=6.1`

Binary Packages:

- `netbase=6.1`

Licenses: (parsed from: `/usr/share/doc/netbase/copyright`)

- `GPL-2`

Source:

```console
$ apt-get source -qq --print-uris netbase=6.1
'http://archive.ubuntu.com/ubuntu/pool/main/n/netbase/netbase_6.1.dsc' netbase_6.1.dsc 1480 SHA256:d3d24cf00001259d3311c0509b4e23ac150cffea27b546e3a204864f52824556
'http://archive.ubuntu.com/ubuntu/pool/main/n/netbase/netbase_6.1.tar.xz' netbase_6.1.tar.xz 31984 SHA256:084d743bd84d4d9380bac4c71c51e57406dce44f5a69289bb823c903e9b035d8
```

### `dpkg` source package: `netpbm-free=2:10.0-15.3build1`

Binary Packages:

- `libnetpbm10=2:10.0-15.3build1`
- `netpbm=2:10.0-15.3build1`

Licenses: (parsed from: `/usr/share/doc/libnetpbm10/copyright`, `/usr/share/doc/netpbm/copyright`)

- `BSD`
- `GPL`

Source:

```console
$ apt-get source -qq --print-uris netpbm-free=2:10.0-15.3build1
'http://archive.ubuntu.com/ubuntu/pool/universe/n/netpbm-free/netpbm-free_10.0-15.3build1.dsc' netpbm-free_10.0-15.3build1.dsc 2184 SHA256:57100bdb3b2fc0c357d966979ff9b677a0d772ef5db67368076ec0118ebf8981
'http://archive.ubuntu.com/ubuntu/pool/universe/n/netpbm-free/netpbm-free_10.0.orig.tar.gz' netpbm-free_10.0.orig.tar.gz 1926538 SHA256:ea3a653f3e5a32e09cea903c5861138f6a597670dff79e2b54e902f140cff2f3
'http://archive.ubuntu.com/ubuntu/pool/universe/n/netpbm-free/netpbm-free_10.0-15.3build1.diff.gz' netpbm-free_10.0-15.3build1.diff.gz 72115 SHA256:fb187f41d676e9ec20d1f48c32738726bc13826ce068de47666fd3b3098eef9f
```

### `dpkg` source package: `nettle=3.5.1+really3.5.1-2ubuntu0.2`

Binary Packages:

- `libhogweed5:amd64=3.5.1+really3.5.1-2ubuntu0.2`
- `libnettle7:amd64=3.5.1+really3.5.1-2ubuntu0.2`

Licenses: (parsed from: `/usr/share/doc/libhogweed5/copyright`, `/usr/share/doc/libnettle7/copyright`)

- `GAP`
- `GPL`
- `GPL-2`
- `GPL-2+`
- `GPL-2+ with Autoconf exception`
- `LGPL`
- `LGPL-2`
- `LGPL-2+`
- `LGPL-2.1+`
- `other`
- `public-domain`

Source:

```console
$ apt-get source -qq --print-uris nettle=3.5.1+really3.5.1-2ubuntu0.2
'http://archive.ubuntu.com/ubuntu/pool/main/n/nettle/nettle_3.5.1+really3.5.1-2ubuntu0.2.dsc' nettle_3.5.1+really3.5.1-2ubuntu0.2.dsc 2490 SHA512:a5b45f1154e48fd7d6c48c57ae17cdcb7cd4a352d6b97bb408a49f5f4f3b40388d23bc12b09602fc9d0d6e91e8bc5525b12f98568ec64c18c4d6ca9fe5048c36
'http://archive.ubuntu.com/ubuntu/pool/main/n/nettle/nettle_3.5.1+really3.5.1.orig.tar.gz' nettle_3.5.1+really3.5.1.orig.tar.gz 1989593 SHA512:f738121b9091cbe79435fb5d46b45cf6f10912320c233829356908127bab1cac6946ca56e022a832380c44f2c10f21d2feef64cb0f4f41e3da4a681dc0131784
'http://archive.ubuntu.com/ubuntu/pool/main/n/nettle/nettle_3.5.1+really3.5.1.orig.tar.gz.asc' nettle_3.5.1+really3.5.1.orig.tar.gz.asc 573 SHA512:d8921622f2165fb4a05e7e75f75d82c0eabb816f265bae3f3267def20d81386b1da1a29ebfc52bbe26875b94b2050dd5493119d0efcb5143bc21e2f69b8449dd
'http://archive.ubuntu.com/ubuntu/pool/main/n/nettle/nettle_3.5.1+really3.5.1-2ubuntu0.2.debian.tar.xz' nettle_3.5.1+really3.5.1-2ubuntu0.2.debian.tar.xz 27228 SHA512:389c303e679b6b6714f824f22bc8675c1ea4bdab0108b69e9514613109573d01592e3f80bf9a144866a159a9a09fa0f3218d62b9f6978bdfe5e95a18bcfe3a88
```

### `dpkg` source package: `networkd-dispatcher=2.1-2~ubuntu20.04.1`

Binary Packages:

- `networkd-dispatcher=2.1-2~ubuntu20.04.1`

Licenses: (parsed from: `/usr/share/doc/networkd-dispatcher/copyright`)

- `GPL-3`
- `GPL-3+`

Source:

```console
$ apt-get source -qq --print-uris networkd-dispatcher=2.1-2~ubuntu20.04.1
'http://archive.ubuntu.com/ubuntu/pool/main/n/networkd-dispatcher/networkd-dispatcher_2.1-2~ubuntu20.04.1.dsc' networkd-dispatcher_2.1-2~ubuntu20.04.1.dsc 2236 SHA512:3cf61c7e6cdcad316d48ed4e7fa469f74c707a125a1413469ff50fc952bd3d4fc45519205b60512e0df408fc261fccc3ef07e934cf34547bbb211af91586279c
'http://archive.ubuntu.com/ubuntu/pool/main/n/networkd-dispatcher/networkd-dispatcher_2.1.orig.tar.gz' networkd-dispatcher_2.1.orig.tar.gz 29650 SHA512:99c456bbb82158dd0dd18a24825d7303f14c650851f9848ad288dcd00af3ec4c4c175c2d54945e1670d4eb5288612f0ec6fb37ec7b9cbca3fd1cc66231cd634d
'http://archive.ubuntu.com/ubuntu/pool/main/n/networkd-dispatcher/networkd-dispatcher_2.1-2~ubuntu20.04.1.debian.tar.xz' networkd-dispatcher_2.1-2~ubuntu20.04.1.debian.tar.xz 5176 SHA512:830e5f41d4cc05a4b38b24d586e4f2e0db6a8699e65bbb6cb2ab18363744228608b76286085a9dc60fb0dc99f9761c5acbd64436bf6b441a57303bf85c498271
```

### `dpkg` source package: `nghttp2=1.40.0-1build1`

Binary Packages:

- `libnghttp2-14:amd64=1.40.0-1build1`

Licenses: (parsed from: `/usr/share/doc/libnghttp2-14/copyright`)

- `BSD-2-clause`
- `Expat`
- `GPL-3`
- `GPL-3+ with autoconf exception`
- `MIT`
- `SIL-OFL-1.1`
- `all-permissive`

Source:

```console
$ apt-get source -qq --print-uris nghttp2=1.40.0-1build1
'http://archive.ubuntu.com/ubuntu/pool/main/n/nghttp2/nghttp2_1.40.0-1build1.dsc' nghttp2_1.40.0-1build1.dsc 2572 SHA256:2f945c4799485cae7ad8f0d1cf1720986bf13f6c65bab7582ef2ae51a48e3661
'http://archive.ubuntu.com/ubuntu/pool/main/n/nghttp2/nghttp2_1.40.0.orig.tar.bz2' nghttp2_1.40.0.orig.tar.bz2 1937537 SHA256:82758e13727945f2408d0612762e4655180b039f058d5ff40d055fa1497bd94f
'http://archive.ubuntu.com/ubuntu/pool/main/n/nghttp2/nghttp2_1.40.0-1build1.debian.tar.xz' nghttp2_1.40.0-1build1.debian.tar.xz 12772 SHA256:d71772f726b343c803954c0bccf3150b736d0e1a7c82a03dc7560fc8bd3a0189
```

### `dpkg` source package: `norm=1.5.8+dfsg2-2build1`

Binary Packages:

- `libnorm1:amd64=1.5.8+dfsg2-2build1`

Licenses: (parsed from: `/usr/share/doc/libnorm1/copyright`)

- `BSD-2-clause`
- `BSD-3-clause`
- `BSD-4-clause-UC`
- `NRL-2-clause`
- `NRL-3-clause`

Source:

```console
$ apt-get source -qq --print-uris norm=1.5.8+dfsg2-2build1
'http://archive.ubuntu.com/ubuntu/pool/universe/n/norm/norm_1.5.8+dfsg2-2build1.dsc' norm_1.5.8+dfsg2-2build1.dsc 1933 SHA256:e15736f0e68b1561c717a29428febc28bae6aac8f703d722138d0c79f518c7e3
'http://archive.ubuntu.com/ubuntu/pool/universe/n/norm/norm_1.5.8+dfsg2.orig.tar.gz' norm_1.5.8+dfsg2.orig.tar.gz 2320548 SHA256:31cde2ef09da189c8ad168cd68c53119ce9e0e56e0de7e37c2e37c81f4c6347d
'http://archive.ubuntu.com/ubuntu/pool/universe/n/norm/norm_1.5.8+dfsg2-2build1.debian.tar.xz' norm_1.5.8+dfsg2-2build1.debian.tar.xz 11536 SHA256:11b7bb0f5cde86eebe8cc6fb00ab32710c911e332267b6d992bff506654cb1a9
```

### `dpkg` source package: `npth=1.6-1`

Binary Packages:

- `libnpth0:amd64=1.6-1`

Licenses: (parsed from: `/usr/share/doc/libnpth0/copyright`)

- `LGPL-2.1`
- `LGPL-2.1+`

Source:

```console
$ apt-get source -qq --print-uris npth=1.6-1
'http://archive.ubuntu.com/ubuntu/pool/main/n/npth/npth_1.6-1.dsc' npth_1.6-1.dsc 1925 SHA256:2c327ce494f702482e79ed620445cba303c4449dd0768fecee3ee7d5ade2544a
'http://archive.ubuntu.com/ubuntu/pool/main/n/npth/npth_1.6.orig.tar.bz2' npth_1.6.orig.tar.bz2 300486 SHA256:1393abd9adcf0762d34798dc34fdcf4d0d22a8410721e76f1e3afcd1daa4e2d1
'http://archive.ubuntu.com/ubuntu/pool/main/n/npth/npth_1.6-1.debian.tar.xz' npth_1.6-1.debian.tar.xz 10532 SHA256:d312d4a3cf1d082e2f2cf3ea752c41d34f7e120f77a941c6c1680e6093834353
```

### `dpkg` source package: `nspr=2:4.25-1`

Binary Packages:

- `libnspr4:amd64=2:4.25-1`

Licenses: (parsed from: `/usr/share/doc/libnspr4/copyright`)

- `MPL-2.0`

Source:

```console
$ apt-get source -qq --print-uris nspr=2:4.25-1
'http://archive.ubuntu.com/ubuntu/pool/main/n/nspr/nspr_4.25-1.dsc' nspr_4.25-1.dsc 1988 SHA256:ce30a697467b115fb2fe9d909ed4ca03ef0a03bef5aaa89be770bc6ef195e98b
'http://archive.ubuntu.com/ubuntu/pool/main/n/nspr/nspr_4.25.orig.tar.gz' nspr_4.25.orig.tar.gz 1079633 SHA256:0bc309be21f91da4474c56df90415101c7f0c7c7cab2943cd943cd7896985256
'http://archive.ubuntu.com/ubuntu/pool/main/n/nspr/nspr_4.25-1.debian.tar.xz' nspr_4.25-1.debian.tar.xz 10684 SHA256:c0d680968c3bb80f3c66e658a29c27608f22923bfcabccbac11fc5ff40f96d08
```

### `dpkg` source package: `nss=2:3.49.1-1ubuntu1.5`

Binary Packages:

- `libnss3:amd64=2:3.49.1-1ubuntu1.5`

Licenses: (parsed from: `/usr/share/doc/libnss3/copyright`)

- `BSD-3`
- `MIT`
- `MPL-2.0`
- `Zlib`
- `public-domain`

Source:

```console
$ apt-get source -qq --print-uris nss=2:3.49.1-1ubuntu1.5
'http://archive.ubuntu.com/ubuntu/pool/main/n/nss/nss_3.49.1-1ubuntu1.5.dsc' nss_3.49.1-1ubuntu1.5.dsc 2287 SHA512:af3f45c01e8ee4610771c9614d5ab162758831a15f1e5aff0f23c841d485846ffbe5659649cc6d4aed47c1593e94dc9fa9f284ee3d74252cb0540376fb01075c
'http://archive.ubuntu.com/ubuntu/pool/main/n/nss/nss_3.49.1.orig.tar.gz' nss_3.49.1.orig.tar.gz 76489134 SHA512:e463c9d71537ac30dbd2998cbdbc0cadc734768a6f3a316c57b6a6d01ad6d26ca732dff65e9c88555a834ae7d71fc857e4cbc1799438069f544a1e27f75985e8
'http://archive.ubuntu.com/ubuntu/pool/main/n/nss/nss_3.49.1-1ubuntu1.5.debian.tar.xz' nss_3.49.1-1ubuntu1.5.debian.tar.xz 198856 SHA512:1d7e71691396c4104f470cc67c34e7ae74680d9e58a45c88c0ea418aaf859a2f03f10fc1665923ad9bc32e8e2dd8fbe8e7edfcc7676178e3092b5406455c2408
```

### `dpkg` source package: `numactl=2.0.12-1`

Binary Packages:

- `libnuma1:amd64=2.0.12-1`

Licenses: (parsed from: `/usr/share/doc/libnuma1/copyright`)

- `GPL`
- `LGPL`

Source:

```console
$ apt-get source -qq --print-uris numactl=2.0.12-1
'http://archive.ubuntu.com/ubuntu/pool/main/n/numactl/numactl_2.0.12-1.dsc' numactl_2.0.12-1.dsc 2033 SHA256:3b308b110de0728c5524b3135d871e55ebb6e4b93cdc583e93c4222219fe4d08
'http://archive.ubuntu.com/ubuntu/pool/main/n/numactl/numactl_2.0.12.orig.tar.gz' numactl_2.0.12.orig.tar.gz 421425 SHA256:2e67513a62168de4777da20d89cdab66d75bcd3badc4256f6b190a8111cd93f8
'http://archive.ubuntu.com/ubuntu/pool/main/n/numactl/numactl_2.0.12-1.debian.tar.xz' numactl_2.0.12-1.debian.tar.xz 6756 SHA256:966724cac8f309b33959ae9922b3e5ab58ea821e2e802d96425e1eaada639a33
```

### `dpkg` source package: `ocl-icd=2.2.11-1ubuntu1`

Binary Packages:

- `ocl-icd-libopencl1:amd64=2.2.11-1ubuntu1`

Licenses: (parsed from: `/usr/share/doc/ocl-icd-libopencl1/copyright`)

- `BSD-2-Clause`

Source:

```console
$ apt-get source -qq --print-uris ocl-icd=2.2.11-1ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/o/ocl-icd/ocl-icd_2.2.11-1ubuntu1.dsc' ocl-icd_2.2.11-1ubuntu1.dsc 2101 SHA256:e786ea5edf5223f3ad32fa4b8d9a4455507b376e45342954ec5b36b21cb4904d
'http://archive.ubuntu.com/ubuntu/pool/main/o/ocl-icd/ocl-icd_2.2.11.orig.tar.gz' ocl-icd_2.2.11.orig.tar.gz 455800 SHA256:02fa41da98ae2807e92742196831d320e3fc2f4cb1118d0061d9f51dda867730
'http://archive.ubuntu.com/ubuntu/pool/main/o/ocl-icd/ocl-icd_2.2.11-1ubuntu1.debian.tar.xz' ocl-icd_2.2.11-1ubuntu1.debian.tar.xz 11204 SHA256:2baece01c46beada400992cfcbe5b0facb822bae07bda5d98f1a7ad4d474415c
```

### `dpkg` source package: `openal-soft=1:1.19.1-1`

Binary Packages:

- `libopenal-data=1:1.19.1-1`
- `libopenal1:amd64=1:1.19.1-1`

Licenses: (parsed from: `/usr/share/doc/libopenal-data/copyright`, `/usr/share/doc/libopenal1/copyright`)

- `Apache`
- `BSD-3-clause-cmake`
- `Expat`
- `GPL-2`
- `GPL-2+`
- `GPL-3`
- `GPL-3+`
- `LGPL-2+`
- `LGPL-2.1+`

Source:

```console
$ apt-get source -qq --print-uris openal-soft=1:1.19.1-1
'http://archive.ubuntu.com/ubuntu/pool/universe/o/openal-soft/openal-soft_1.19.1-1.dsc' openal-soft_1.19.1-1.dsc 2524 SHA256:1ba42d3f53a4b394d1c7077b1281dfd4c8d10b1435c889d7033cd90e468468f4
'http://archive.ubuntu.com/ubuntu/pool/universe/o/openal-soft/openal-soft_1.19.1.orig.tar.gz' openal-soft_1.19.1.orig.tar.gz 683061 SHA256:9f3536ab2bb7781dbafabc6a61e0b34b17edd16bd6c2eaf2ae71bc63078f98c7
'http://archive.ubuntu.com/ubuntu/pool/universe/o/openal-soft/openal-soft_1.19.1-1.debian.tar.xz' openal-soft_1.19.1-1.debian.tar.xz 12768 SHA256:6bb1a5c6dbfdc02e5ff1d0eca00c7f2af43ca1be532424513cea20726ad48646
```

### `dpkg` source package: `openexr=2.3.0-6ubuntu0.5`

Binary Packages:

- `libopenexr24:amd64=2.3.0-6ubuntu0.5`

Licenses: (parsed from: `/usr/share/doc/libopenexr24/copyright`)

- `BSD-3-clause`
- `GPL-2`
- `GPL-2+`
- `openexr`

Source:

```console
$ apt-get source -qq --print-uris openexr=2.3.0-6ubuntu0.5
'http://archive.ubuntu.com/ubuntu/pool/universe/o/openexr/openexr_2.3.0-6ubuntu0.5.dsc' openexr_2.3.0-6ubuntu0.5.dsc 2638 SHA512:b77e6d69deaf92ca2a3990578aaf21bfc4ec5c6fab9e42b93819dce0fe4c5432feeeae4abaf2cdbc160ff9ac5099b1622a36fbcd41845c4c1355bc76a28a6dcc
'http://archive.ubuntu.com/ubuntu/pool/universe/o/openexr/openexr_2.3.0.orig.tar.gz' openexr_2.3.0.orig.tar.gz 18416222 SHA512:f6810505428674451627ef09e5dfbf13d7413e118f9defec4d160d9f1327b47699fe770a96b61da7820d2a357ccb722ad909ba4ba0924703fa5fd532cdf0da69
'http://archive.ubuntu.com/ubuntu/pool/universe/o/openexr/openexr_2.3.0.orig.tar.gz.asc' openexr_2.3.0.orig.tar.gz.asc 566 SHA512:7110ddb22b2be7b570dcb1df278b2f7f39f2c5afd470094fd2a41c2f376d3991f756cbc5bef76dfc5bd7e1f55442bc8dff468d47224a495838083ef7de0c2a40
'http://archive.ubuntu.com/ubuntu/pool/universe/o/openexr/openexr_2.3.0-6ubuntu0.5.debian.tar.xz' openexr_2.3.0-6ubuntu0.5.debian.tar.xz 40348 SHA512:a7454052906c5b683ab905a80933a323de29dfbbbe9ed2b5160e7c5fe6db4197c03d6bb463b428c7e706702a2f0c9112debbfdc5f9a430225e81b57f949c9200
```

### `dpkg` source package: `openjdk-lts=11.0.11+9-0ubuntu2~20.04`

Binary Packages:

- `openjdk-11-jdk:amd64=11.0.11+9-0ubuntu2~20.04`
- `openjdk-11-jdk-headless:amd64=11.0.11+9-0ubuntu2~20.04`
- `openjdk-11-jre:amd64=11.0.11+9-0ubuntu2~20.04`
- `openjdk-11-jre-headless:amd64=11.0.11+9-0ubuntu2~20.04`

Licenses: (parsed from: `/usr/share/doc/openjdk-11-jdk/copyright`, `/usr/share/doc/openjdk-11-jdk-headless/copyright`, `/usr/share/doc/openjdk-11-jre/copyright`, `/usr/share/doc/openjdk-11-jre-headless/copyright`)

- `Apache-2.0`
- `GPL-2`
- `LGPL-2`
- `LGPL-2-1`

Source:

```console
$ apt-get source -qq --print-uris openjdk-lts=11.0.11+9-0ubuntu2~20.04
'http://archive.ubuntu.com/ubuntu/pool/main/o/openjdk-lts/openjdk-lts_11.0.11+9-0ubuntu2~20.04.dsc' openjdk-lts_11.0.11+9-0ubuntu2~20.04.dsc 4720 SHA512:9b919dfad1e69d09435cd1139dae448ca28058b1b785595cc9fa8c3cfea65832f9df954c2a29bd91353705d695d822e9c132ceefd8a3f8a469711e52646e5dbb
'http://archive.ubuntu.com/ubuntu/pool/main/o/openjdk-lts/openjdk-lts_11.0.11+9.orig.tar.xz' openjdk-lts_11.0.11+9.orig.tar.xz 78079764 SHA512:3baeb4480215bec453d5935ba4cba94716aa75d51f2a65cf2c94e7ea95c3e54e44e5b36d4690d2b0b7ec4cd95c70eee3484846c0690ea4b406b3dcd8911a6d4f
'http://archive.ubuntu.com/ubuntu/pool/main/o/openjdk-lts/openjdk-lts_11.0.11+9-0ubuntu2~20.04.debian.tar.xz' openjdk-lts_11.0.11+9-0ubuntu2~20.04.debian.tar.xz 171772 SHA512:8eebbb45b186164131b8f2b040936bc822e6f9649feca1dc5056aa77f92567b1b207094bca4627ee5e4e6a0f4c32764ff6a3c613bb084a03928e3edcb27780a2
```

### `dpkg` source package: `openjpeg2=2.3.1-1ubuntu4.20.04.1`

Binary Packages:

- `libopenjp2-7:amd64=2.3.1-1ubuntu4.20.04.1`

Licenses: (parsed from: `/usr/share/doc/libopenjp2-7/copyright`)

- `BSD-2`
- `BSD-3`
- `LIBPNG`
- `LIBTIFF`
- `LIBTIFF-GLARSON`
- `LIBTIFF-PIXAR`
- `MIT`
- `ZLIB`
- `public-domain`

Source:

```console
$ apt-get source -qq --print-uris openjpeg2=2.3.1-1ubuntu4.20.04.1
'http://archive.ubuntu.com/ubuntu/pool/main/o/openjpeg2/openjpeg2_2.3.1-1ubuntu4.20.04.1.dsc' openjpeg2_2.3.1-1ubuntu4.20.04.1.dsc 2874 SHA512:4024af1873e8a1db734ed44d18a04111d1a26ecf6247db79660a6e4558fbc4f371b36c89f80d0d59d79c80a911c67aa290b86c3ce162db361bcbdf70b01a27e9
'http://archive.ubuntu.com/ubuntu/pool/main/o/openjpeg2/openjpeg2_2.3.1.orig.tar.xz' openjpeg2_2.3.1.orig.tar.xz 1381768 SHA512:1346fae5f554102c46ad26e59888c693bf57b3ffaccfb5040b6c177f2ca510dd0915966d6bfd252b4293c0c098290c8e6cd923c265ca288e95e1fb7522b66b32
'http://archive.ubuntu.com/ubuntu/pool/main/o/openjpeg2/openjpeg2_2.3.1-1ubuntu4.20.04.1.debian.tar.xz' openjpeg2_2.3.1-1ubuntu4.20.04.1.debian.tar.xz 25176 SHA512:6f708d4c6b4516a0ad29a18b8ec06de9c2cf25ec63c016f0b97288bb96b6515a3309a7229f01e4ce98f6f9ed9b52901dae4b6ee62a5d1993e8c250905e186717
```

### `dpkg` source package: `openldap=2.4.49+dfsg-2ubuntu1.8`

Binary Packages:

- `libldap-2.4-2:amd64=2.4.49+dfsg-2ubuntu1.8`
- `libldap-common=2.4.49+dfsg-2ubuntu1.8`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris openldap=2.4.49+dfsg-2ubuntu1.8
'http://archive.ubuntu.com/ubuntu/pool/main/o/openldap/openldap_2.4.49+dfsg-2ubuntu1.8.dsc' openldap_2.4.49+dfsg-2ubuntu1.8.dsc 3136 SHA512:fce0af3ba41daa23607c15ff9152277f27b185ec6e9ebfa8b1bfe1817cf13c6f00b86ad93b597a0c283ab2ea326194259aff83fb49fac99454b260dff317f350
'http://archive.ubuntu.com/ubuntu/pool/main/o/openldap/openldap_2.4.49+dfsg.orig.tar.gz' openldap_2.4.49+dfsg.orig.tar.gz 4844726 SHA512:c2096f6e37bae8e4d4dcc5cc8dad783996bc8677e7e62a06b9f55857f8950726ca3e3b0d8368563c8985123175f63625354ad5ac271db8b55d3ac62e8906d4c7
'http://archive.ubuntu.com/ubuntu/pool/main/o/openldap/openldap_2.4.49+dfsg-2ubuntu1.8.debian.tar.xz' openldap_2.4.49+dfsg-2ubuntu1.8.debian.tar.xz 187956 SHA512:918483e21af3233daa5a8b04da0c87fc010049ad5e66230d36a3737f511cd395f5f1f6753139f13520266f9fe86c07ae9c06c90128ca800680ecd3d23bc4b9a4
```

### `dpkg` source package: `openssl=1.1.1f-1ubuntu2.8`

Binary Packages:

- `libssl1.1:amd64=1.1.1f-1ubuntu2.8`
- `openssl=1.1.1f-1ubuntu2.8`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris openssl=1.1.1f-1ubuntu2.8
'http://archive.ubuntu.com/ubuntu/pool/main/o/openssl/openssl_1.1.1f-1ubuntu2.8.dsc' openssl_1.1.1f-1ubuntu2.8.dsc 2466 SHA512:79514668a1c50eb32665786dae9b2e60ec4c908c173a7c66feb97ce3bf87d46e5c5dd8786a3c8d20aab20a9c4705821fa833ff4f54e92a1c033215a1c3749c07
'http://archive.ubuntu.com/ubuntu/pool/main/o/openssl/openssl_1.1.1f.orig.tar.gz' openssl_1.1.1f.orig.tar.gz 9792828 SHA512:b00bd9b5ad5298fbceeec6bb19c1ab0c106ca5cfb31178497c58bf7e0e0cf30fcc19c20f84e23af31cc126bf2447d3e4f8461db97bafa7bd78f69561932f000c
'http://archive.ubuntu.com/ubuntu/pool/main/o/openssl/openssl_1.1.1f-1ubuntu2.8.debian.tar.xz' openssl_1.1.1f-1ubuntu2.8.debian.tar.xz 166152 SHA512:97fc740bd4fe474019878dd90d7130200644aec105fc6cf926791dada9a2bb8f330091b7e91fa6410e516dfbfb656120520b86190b3e417fcb9d461069b604d1
```

### `dpkg` source package: `opus=1.3.1-0ubuntu1`

Binary Packages:

- `libopus0:amd64=1.3.1-0ubuntu1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris opus=1.3.1-0ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/o/opus/opus_1.3.1-0ubuntu1.dsc' opus_1.3.1-0ubuntu1.dsc 1389 SHA256:b95a1010640a307e474cc4229055682ef268b700895fd305170a118e66d04353
'http://archive.ubuntu.com/ubuntu/pool/main/o/opus/opus_1.3.1.orig.tar.gz' opus_1.3.1.orig.tar.gz 1040054 SHA256:65b58e1e25b2a114157014736a3d9dfeaad8d41be1c8179866f144a2fb44ff9d
'http://archive.ubuntu.com/ubuntu/pool/main/o/opus/opus_1.3.1-0ubuntu1.diff.gz' opus_1.3.1-0ubuntu1.diff.gz 8936 SHA256:292df4c7a83b25becb54792ba71be6bbe1bd0fb232e16ac4e69d39b6c6c26ca3
```

### `dpkg` source package: `orc=1:0.4.31-1`

Binary Packages:

- `liborc-0.4-0:amd64=1:0.4.31-1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris orc=1:0.4.31-1
'http://archive.ubuntu.com/ubuntu/pool/main/o/orc/orc_0.4.31-1.dsc' orc_0.4.31-1.dsc 2380 SHA256:aade946190c893e3b0cee5531380a72283965b5150c811945aed8f19345d3372
'http://archive.ubuntu.com/ubuntu/pool/main/o/orc/orc_0.4.31.orig.tar.xz' orc_0.4.31.orig.tar.xz 177768 SHA256:a0ab5f10a6a9ae7c3a6b4218246564c3bf00d657cbdf587e6d34ec3ef0616075
'http://archive.ubuntu.com/ubuntu/pool/main/o/orc/orc_0.4.31-1.debian.tar.xz' orc_0.4.31-1.debian.tar.xz 5592 SHA256:02891a9abd10141d9845a488f562070148f552fe58bb7135224dc0972f9ac616
```

### `dpkg` source package: `p11-kit=0.23.20-1ubuntu0.1`

Binary Packages:

- `libp11-kit0:amd64=0.23.20-1ubuntu0.1`

Licenses: (parsed from: `/usr/share/doc/libp11-kit0/copyright`)

- `BSD-3-Clause`
- `ISC`
- `ISC+IBM`
- `permissive-like-automake-output`
- `same-as-rest-of-p11kit`

Source:

```console
$ apt-get source -qq --print-uris p11-kit=0.23.20-1ubuntu0.1
'http://archive.ubuntu.com/ubuntu/pool/main/p/p11-kit/p11-kit_0.23.20-1ubuntu0.1.dsc' p11-kit_0.23.20-1ubuntu0.1.dsc 2532 SHA512:8b315f15df7cd3a09d11046030baa864a0f61a3dfba80d97d708590f54a5fc5c31c81428ccc40bf04e9e769abda1204ef5cd4753a24e743e2728d38cdfe14803
'http://archive.ubuntu.com/ubuntu/pool/main/p/p11-kit/p11-kit_0.23.20.orig.tar.xz' p11-kit_0.23.20.orig.tar.xz 822588 SHA512:1eb88773fdd49dd48c7e089744e9dbbf6c1033a4863f3bfe75a68d842804baa3c373cb1b28ee625dd69a6e16c89df4ac755e0928495dccf38c007c530f6cfa57
'http://archive.ubuntu.com/ubuntu/pool/main/p/p11-kit/p11-kit_0.23.20.orig.tar.xz.asc' p11-kit_0.23.20.orig.tar.xz.asc 854 SHA512:9f0e0e690698637269b7d020aafd92ab3d487770196e13357ce0e5425fa02d5e279f9524b3858bce8bdb925e1e4d9fa2219a68e5888c06e48c3b085a77d329e9
'http://archive.ubuntu.com/ubuntu/pool/main/p/p11-kit/p11-kit_0.23.20-1ubuntu0.1.debian.tar.xz' p11-kit_0.23.20-1ubuntu0.1.debian.tar.xz 24592 SHA512:b30c6640bb02f0651955447da65911942cd2e302bc5af0ab96787646e776486e317420682dd644079a47ac48d4e2732218545af56da7ec3d3af5fd0c7e55fb21
```

### `dpkg` source package: `pam=1.3.1-5ubuntu4.3`

Binary Packages:

- `libpam-modules:amd64=1.3.1-5ubuntu4.3`
- `libpam-modules-bin=1.3.1-5ubuntu4.3`
- `libpam-runtime=1.3.1-5ubuntu4.3`
- `libpam0g:amd64=1.3.1-5ubuntu4.3`

Licenses: (parsed from: `/usr/share/doc/libpam-modules/copyright`, `/usr/share/doc/libpam-modules-bin/copyright`, `/usr/share/doc/libpam-runtime/copyright`, `/usr/share/doc/libpam0g/copyright`)

- `GPL`

Source:

```console
$ apt-get source -qq --print-uris pam=1.3.1-5ubuntu4.3
'http://archive.ubuntu.com/ubuntu/pool/main/p/pam/pam_1.3.1-5ubuntu4.3.dsc' pam_1.3.1-5ubuntu4.3.dsc 2761 SHA512:314394037f6006a11778f96aaac57c1a9997bc1b2b1c67224a46e670ea1707ae5d00e3c2d7ef42890e13f87d4edac16481050ae412b6092061e5fef27f6a80e4
'http://archive.ubuntu.com/ubuntu/pool/main/p/pam/pam_1.3.1.orig.tar.xz' pam_1.3.1.orig.tar.xz 912332 SHA512:6bc8e2a5b64686f0a23846221c5228c88418ba485b17c53b3a12f91262b5bb73566d6b6a5daa1f63bbae54310aee918b987e44a72ce809b4e7c668f0fadfe08e
'http://archive.ubuntu.com/ubuntu/pool/main/p/pam/pam_1.3.1-5ubuntu4.3.debian.tar.xz' pam_1.3.1-5ubuntu4.3.debian.tar.xz 170884 SHA512:0d10c47ed3aa1a1ef040413a746543c833b9bef971e70586d053d59ac8de2ad480ca69a5ed1c56df5322e793675b2fd8f7e65ddde1b2ee8340bb05f77fea846e
```

### `dpkg` source package: `pango1.0=1.44.7-2ubuntu4`

Binary Packages:

- `libpango-1.0-0:amd64=1.44.7-2ubuntu4`
- `libpangocairo-1.0-0:amd64=1.44.7-2ubuntu4`
- `libpangoft2-1.0-0:amd64=1.44.7-2ubuntu4`

Licenses: (parsed from: `/usr/share/doc/libpango-1.0-0/copyright`, `/usr/share/doc/libpangocairo-1.0-0/copyright`, `/usr/share/doc/libpangoft2-1.0-0/copyright`)

- `Chromium-BSD-style`
- `Example`
- `ICU`
- `LGPL-2`
- `LGPL-2+`
- `TCL`
- `Unicode`

Source:

```console
$ apt-get source -qq --print-uris pango1.0=1.44.7-2ubuntu4
'http://archive.ubuntu.com/ubuntu/pool/main/p/pango1.0/pango1.0_1.44.7-2ubuntu4.dsc' pango1.0_1.44.7-2ubuntu4.dsc 2915 SHA256:e7d7027628a38d12ee9e6f29f4f6d275757d9b1fdc9e55948194f233c55251fc
'http://archive.ubuntu.com/ubuntu/pool/main/p/pango1.0/pango1.0_1.44.7.orig.tar.xz' pango1.0_1.44.7.orig.tar.xz 521384 SHA256:66a5b6cc13db73efed67b8e933584509f8ddb7b10a8a40c3850ca4a985ea1b1f
'http://archive.ubuntu.com/ubuntu/pool/main/p/pango1.0/pango1.0_1.44.7-2ubuntu4.debian.tar.xz' pango1.0_1.44.7-2ubuntu4.debian.tar.xz 33516 SHA256:6f5f8c66299af90a94c4dbdfa146e840eec8bc2d183cd1fb42e8e7de6f335df5
```

### `dpkg` source package: `pcre2=10.34-7`

Binary Packages:

- `libpcre2-8-0:amd64=10.34-7`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris pcre2=10.34-7
'http://archive.ubuntu.com/ubuntu/pool/main/p/pcre2/pcre2_10.34-7.dsc' pcre2_10.34-7.dsc 2286 SHA256:c3e2bfd8fabf594238b3f17074dc8ac483aaf80a9f12dbfe927b80a74558732e
'http://archive.ubuntu.com/ubuntu/pool/main/p/pcre2/pcre2_10.34.orig.tar.gz' pcre2_10.34.orig.tar.gz 2271533 SHA256:da6aba7ba2509e918e41f4f744a59fa41a2425c59a298a232e7fe85691e00379
'http://archive.ubuntu.com/ubuntu/pool/main/p/pcre2/pcre2_10.34-7.diff.gz' pcre2_10.34-7.diff.gz 7068 SHA256:7d44ac1b171ef7f7051213a3a8505b28f3809ed3e2fb348567a29fdf5f2b5fdf
```

### `dpkg` source package: `pcre3=2:8.39-12build1`

Binary Packages:

- `libpcre3:amd64=2:8.39-12build1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris pcre3=2:8.39-12build1
'http://archive.ubuntu.com/ubuntu/pool/main/p/pcre3/pcre3_8.39-12build1.dsc' pcre3_8.39-12build1.dsc 2133 SHA256:e1dd0e352e5ba90aa89016dc3ad8b5990c1a8743c1550c613c7a9d9079a2da67
'http://archive.ubuntu.com/ubuntu/pool/main/p/pcre3/pcre3_8.39.orig.tar.bz2' pcre3_8.39.orig.tar.bz2 1560758 SHA256:b858099f82483031ee02092711689e7245586ada49e534a06e678b8ea9549e8b
'http://archive.ubuntu.com/ubuntu/pool/main/p/pcre3/pcre3_8.39-12build1.debian.tar.gz' pcre3_8.39-12build1.debian.tar.gz 26478 SHA256:8f92c016f9200aad9a9028bbd96eded68394c756b09df76db2c0a54e8a1802c6
```

### `dpkg` source package: `pcsc-lite=1.8.26-3`

Binary Packages:

- `libpcsclite1:amd64=1.8.26-3`

Licenses: (parsed from: `/usr/share/doc/libpcsclite1/copyright`)

- `BSD-3-clause`
- `Expat`
- `GPL-3`
- `GPL-3+`
- `ISC`

Source:

```console
$ apt-get source -qq --print-uris pcsc-lite=1.8.26-3
'http://archive.ubuntu.com/ubuntu/pool/main/p/pcsc-lite/pcsc-lite_1.8.26-3.dsc' pcsc-lite_1.8.26-3.dsc 2092 SHA256:e48f0831f3e3b44bed686438a59bc659082ebd8d18f1f79e04db4f24a0947c17
'http://archive.ubuntu.com/ubuntu/pool/main/p/pcsc-lite/pcsc-lite_1.8.26.orig.tar.bz2' pcsc-lite_1.8.26.orig.tar.bz2 754838 SHA256:3eb7be7d6ef618c0a444316cf5c1f2f9d7227aedba7a192f389fe3e7c0dfbbd9
'http://archive.ubuntu.com/ubuntu/pool/main/p/pcsc-lite/pcsc-lite_1.8.26-3.debian.tar.xz' pcsc-lite_1.8.26-3.debian.tar.xz 18812 SHA256:d341855fcaeb4bd28bba502ece78e18c01d0e63d7bba01c6489b6ce1705db8f4
```

### `dpkg` source package: `perl-openssl-defaults=4`

Binary Packages:

- `perl-openssl-defaults:amd64=4`

Licenses: (parsed from: `/usr/share/doc/perl-openssl-defaults/copyright`)

- `Artistic`
- `GPL-1`
- `GPL-1+`

Source:

```console
$ apt-get source -qq --print-uris perl-openssl-defaults=4
'http://archive.ubuntu.com/ubuntu/pool/main/p/perl-openssl-defaults/perl-openssl-defaults_4.dsc' perl-openssl-defaults_4.dsc 1861 SHA256:3b766d088375a30c5985bdfce3147150ba747c812ce1d87fcd4cb3cb6ef42703
'http://archive.ubuntu.com/ubuntu/pool/main/p/perl-openssl-defaults/perl-openssl-defaults_4.tar.xz' perl-openssl-defaults_4.tar.xz 4392 SHA256:81653b03e48f9f8d638dd5a2bcd82dd5738d02ed66cdffa56c6f2882f08faa1e
```

### `dpkg` source package: `perl=5.30.0-9ubuntu0.2`

Binary Packages:

- `libperl5.30:amd64=5.30.0-9ubuntu0.2`
- `perl=5.30.0-9ubuntu0.2`
- `perl-base=5.30.0-9ubuntu0.2`
- `perl-modules-5.30=5.30.0-9ubuntu0.2`

Licenses: (parsed from: `/usr/share/doc/libperl5.30/copyright`, `/usr/share/doc/perl/copyright`, `/usr/share/doc/perl-base/copyright`, `/usr/share/doc/perl-modules-5.30/copyright`)

- `Artistic`
- `Artistic,`
- `Artistic-2`
- `Artistic-dist`
- `BSD-3-clause`
- `BSD-3-clause-GENERIC`
- `BSD-3-clause-with-weird-numbering`
- `BSD-4-clause-POWERDOG`
- `BZIP`
- `CC0-1.0`
- `DONT-CHANGE-THE-GPL`
- `Expat`
- `GPL-1`
- `GPL-1+`
- `GPL-2`
- `GPL-2+`
- `GPL-3+-WITH-BISON-EXCEPTION`
- `HSIEH-BSD`
- `HSIEH-DERIVATIVE`
- `LGPL-2.1`
- `REGCOMP`
- `REGCOMP,`
- `RRA-KEEP-THIS-NOTICE`
- `SDBM-PUBLIC-DOMAIN`
- `TEXT-TABS`
- `Unicode`
- `ZLIB`

Source:

```console
$ apt-get source -qq --print-uris perl=5.30.0-9ubuntu0.2
'http://archive.ubuntu.com/ubuntu/pool/main/p/perl/perl_5.30.0-9ubuntu0.2.dsc' perl_5.30.0-9ubuntu0.2.dsc 2962 SHA512:497f54e6e2d142b7a03cdee4999ea8d861911e2aa8115bb6895c244c4f285a00648bd6e1038c56017b7370c9b4b474dbe212071a65054c7435dbab66d2cd0ed3
'http://archive.ubuntu.com/ubuntu/pool/main/p/perl/perl_5.30.0.orig-regen-configure.tar.gz' perl_5.30.0.orig-regen-configure.tar.gz 833235 SHA512:ab977887b53249a2423708aa38ecbb8bdbfdb7ba533a795eaa20bac427b2eb326756b076ca11088036550a4db24418903c0565d168fe9641e18077a76d04274a
'http://archive.ubuntu.com/ubuntu/pool/main/p/perl/perl_5.30.0.orig.tar.xz' perl_5.30.0.orig.tar.xz 12419868 SHA512:68a295eccd64debd9d6a10f0d5577f872a19ad8c2d702798f6b0f45b8c3af6ab3230768056e2131e9e2e2506d1035b27cfd627c845e32263fe448649c4b98ae9
'http://archive.ubuntu.com/ubuntu/pool/main/p/perl/perl_5.30.0-9ubuntu0.2.debian.tar.xz' perl_5.30.0-9ubuntu0.2.debian.tar.xz 167248 SHA512:841504df745d755603b6a592484ff91a44e4346adc96b01a0c0bce09205f46d4250a6aafbb7462510a150d1d2904ef6889f014bd5d33efc5f18bafd71cef8bce
```

### `dpkg` source package: `pinentry=1.1.0-3build1`

Binary Packages:

- `pinentry-curses=1.1.0-3build1`

Licenses: (parsed from: `/usr/share/doc/pinentry-curses/copyright`)

- `GPL-2`
- `GPL-2+`
- `LGPL-3`
- `LGPL-3+`
- `X11`

Source:

```console
$ apt-get source -qq --print-uris pinentry=1.1.0-3build1
'http://archive.ubuntu.com/ubuntu/pool/main/p/pinentry/pinentry_1.1.0-3build1.dsc' pinentry_1.1.0-3build1.dsc 2714 SHA256:69f7f343287886eebadb94177767d9aa74890d9f8420e3ab254803fcd21852bf
'http://archive.ubuntu.com/ubuntu/pool/main/p/pinentry/pinentry_1.1.0.orig.tar.bz2' pinentry_1.1.0.orig.tar.bz2 467702 SHA256:68076686fa724a290ea49cdf0d1c0c1500907d1b759a3bcbfbec0293e8f56570
'http://archive.ubuntu.com/ubuntu/pool/main/p/pinentry/pinentry_1.1.0-3build1.debian.tar.xz' pinentry_1.1.0-3build1.debian.tar.xz 17224 SHA256:2a11ee552389ba0499d6a9e1bfc38ee65a28bb97758832b982bbede68d2cb1b9
```

### `dpkg` source package: `pixman=0.38.4-0ubuntu1`

Binary Packages:

- `libpixman-1-0:amd64=0.38.4-0ubuntu1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris pixman=0.38.4-0ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/p/pixman/pixman_0.38.4-0ubuntu1.dsc' pixman_0.38.4-0ubuntu1.dsc 1468 SHA256:310e4d2fb12e80dc9f11e7c29332f40f5830b98c06b45e0cd62181feea0d7165
'http://archive.ubuntu.com/ubuntu/pool/main/p/pixman/pixman_0.38.4.orig.tar.gz' pixman_0.38.4.orig.tar.gz 897926 SHA256:da66d6fd6e40aee70f7bd02e4f8f76fc3f006ec879d346bae6a723025cfbdde7
'http://archive.ubuntu.com/ubuntu/pool/main/p/pixman/pixman_0.38.4-0ubuntu1.diff.gz' pixman_0.38.4-0ubuntu1.diff.gz 322901 SHA256:7a7ed16483d2bc68a18769828450d941b1eb8fd0934a275c09502a0075dcccdc
```

### `dpkg` source package: `poppler-data=0.4.9-2`

Binary Packages:

- `poppler-data=0.4.9-2`

Licenses: (parsed from: `/usr/share/doc/poppler-data/copyright`)

- `AGPL-3+`
- `BSD-3-cluase`
- `GPL-2`
- `MIT`

Source:

```console
$ apt-get source -qq --print-uris poppler-data=0.4.9-2
'http://archive.ubuntu.com/ubuntu/pool/main/p/poppler-data/poppler-data_0.4.9-2.dsc' poppler-data_0.4.9-2.dsc 2456 SHA256:da4b19cc39f2b0d767dfd500c04949db7aa2139324c4e0d3278ed86d3edcfde5
'http://archive.ubuntu.com/ubuntu/pool/main/p/poppler-data/poppler-data_0.4.9.orig-ai0.tar.gz' poppler-data_0.4.9.orig-ai0.tar.gz 3515 SHA256:755a3a7cec6019b7cb6a7ac89828820e90d5105e66ebc2a7aacecacfb3ed4f1d
'http://archive.ubuntu.com/ubuntu/pool/main/p/poppler-data/poppler-data_0.4.9.orig-from-ghostscript.tar.xz' poppler-data_0.4.9.orig-from-ghostscript.tar.xz 2320 SHA256:5070e1f3645080c809d80c42ee2e736648fe37bc2a68c3f54d1f9fce01086215
'http://archive.ubuntu.com/ubuntu/pool/main/p/poppler-data/poppler-data_0.4.9.orig.tar.gz' poppler-data_0.4.9.orig.tar.gz 4196919 SHA256:1f9c7e7de9ecd0db6ab287349e31bf815ca108a5a175cf906a90163bdbe32012
'http://archive.ubuntu.com/ubuntu/pool/main/p/poppler-data/poppler-data_0.4.9-2.debian.tar.xz' poppler-data_0.4.9-2.debian.tar.xz 19504 SHA256:300792a153c1bfcf2413807875e333c7ba31a30a71f64d97bca58de307589d70
```

### `dpkg` source package: `poppler=0.86.1-0ubuntu1`

Binary Packages:

- `libpoppler97:amd64=0.86.1-0ubuntu1`

Licenses: (parsed from: `/usr/share/doc/libpoppler97/copyright`)

- `Apache-2.0`
- `GPL-2`

Source:

```console
$ apt-get source -qq --print-uris poppler=0.86.1-0ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/p/poppler/poppler_0.86.1-0ubuntu1.dsc' poppler_0.86.1-0ubuntu1.dsc 2658 SHA256:e279f4042e1dd99d6f52ad235e9463b61f63b217e6414f368567c8428162749f
'http://archive.ubuntu.com/ubuntu/pool/main/p/poppler/poppler_0.86.1.orig.tar.xz' poppler_0.86.1.orig.tar.xz 1593856 SHA256:af630a277c8e194c31339c5446241834aed6ed3d4b4dc7080311e51c66257f6c
'http://archive.ubuntu.com/ubuntu/pool/main/p/poppler/poppler_0.86.1-0ubuntu1.debian.tar.xz' poppler_0.86.1-0ubuntu1.debian.tar.xz 34832 SHA256:fe88ed5df596c68ecf5ffa7e2ff6dc4cdedea4ae3ba8c79b7fefc9689a9b6db9
```

### `dpkg` source package: `postgresql-12=12.8-0ubuntu0.20.04.1`

Binary Packages:

- `libpq5:amd64=12.8-0ubuntu0.20.04.1`

Licenses: (parsed from: `/usr/share/doc/libpq5/copyright`)

- `Artistic`
- `BSD-2-clause`
- `BSD-3-Clause`
- `BSD-3-clause`
- `Custom-Unicode`
- `Custom-pg_dump`
- `Custom-regex`
- `GPL-1`
- `PostgreSQL`
- `Tcl`
- `blf`
- `double-metaphone`
- `imath`
- `nagaysau-ishii`
- `rijndael`

Source:

```console
$ apt-get source -qq --print-uris postgresql-12=12.8-0ubuntu0.20.04.1
'http://archive.ubuntu.com/ubuntu/pool/main/p/postgresql-12/postgresql-12_12.8-0ubuntu0.20.04.1.dsc' postgresql-12_12.8-0ubuntu0.20.04.1.dsc 3760 SHA512:8d6120e0143ccf55a1dc05dbc05f35608417ec9e1c546efb611f41bee3fa2dc6fb543745c059107bbf236dc05cea63bf019631ef1054cc5db4a6042e6e4095d2
'http://archive.ubuntu.com/ubuntu/pool/main/p/postgresql-12/postgresql-12_12.8.orig.tar.bz2' postgresql-12_12.8.orig.tar.bz2 20849478 SHA512:970fe1041e427ac1c8a786c93e2079b0a9c8b3fcaf9d38877894eb02e8a9afc7cd73d7ac28078c455845a922a1b7d9c1e22cb7990d8d523dd6496af9442fba01
'http://archive.ubuntu.com/ubuntu/pool/main/p/postgresql-12/postgresql-12_12.8-0ubuntu0.20.04.1.debian.tar.xz' postgresql-12_12.8-0ubuntu0.20.04.1.debian.tar.xz 25972 SHA512:72666a6bc3b95535871cd8e438eb754376bad1c7f62bbcfb1563713902f6660bd1ba566bee8140e244876e4b21fb7fec3386be3aa11db035e3c7f4a4d9d98f4a
```

### `dpkg` source package: `procps=2:3.3.16-1ubuntu2.3`

Binary Packages:

- `libprocps8:amd64=2:3.3.16-1ubuntu2.3`
- `procps=2:3.3.16-1ubuntu2.3`

Licenses: (parsed from: `/usr/share/doc/libprocps8/copyright`, `/usr/share/doc/procps/copyright`)

- `GPL-2`
- `GPL-2.0+`
- `LGPL-2`
- `LGPL-2.0+`
- `LGPL-2.1`
- `LGPL-2.1+`

Source:

```console
$ apt-get source -qq --print-uris procps=2:3.3.16-1ubuntu2.3
'http://archive.ubuntu.com/ubuntu/pool/main/p/procps/procps_3.3.16-1ubuntu2.3.dsc' procps_3.3.16-1ubuntu2.3.dsc 1897 SHA512:48d20e1d3d1b097f060b3d2d0eba35dc255498f9890e912cfde1a04434959f9af6ee794e5690a026cc19171dbff509e2814fceed6fbfa542971beed529cf685d
'http://archive.ubuntu.com/ubuntu/pool/main/p/procps/procps_3.3.16.orig.tar.xz' procps_3.3.16.orig.tar.xz 621892 SHA512:38db4f72fe40c2f027b23b18bbc8c29cfcdf6bcdb029199fe4bebede153943aa884157f56e792c399f9a4949cc514687500bb99a75a5e7ad7b9e878f52090304
'http://archive.ubuntu.com/ubuntu/pool/main/p/procps/procps_3.3.16-1ubuntu2.3.debian.tar.xz' procps_3.3.16-1ubuntu2.3.debian.tar.xz 33972 SHA512:cbeb9513f4932d159eed653fbc568b4b26e61e92184f8d8e6fd4122cfdda62ff04a7e9dbba1914efff0fe7addfbd380a0def4b8cf1c61f4e91107b02b3b61064
```

### `dpkg` source package: `publicsuffix=20200303.0012-1`

Binary Packages:

- `publicsuffix=20200303.0012-1`

Licenses: (parsed from: `/usr/share/doc/publicsuffix/copyright`)

- `CC0`
- `MPL-2.0`

Source:

```console
$ apt-get source -qq --print-uris publicsuffix=20200303.0012-1
'http://archive.ubuntu.com/ubuntu/pool/main/p/publicsuffix/publicsuffix_20200303.0012-1.dsc' publicsuffix_20200303.0012-1.dsc 1406 SHA256:ada2841021e758d6ebb15063d3caf243f545b01b5edf6adf65ecdf187fa2493c
'http://archive.ubuntu.com/ubuntu/pool/main/p/publicsuffix/publicsuffix_20200303.0012.orig.tar.gz' publicsuffix_20200303.0012.orig.tar.gz 94164 SHA256:048bf6efaf055c4cfed1c79b204f4c1f8f2d1f66ad0424979a227f43ef8df243
'http://archive.ubuntu.com/ubuntu/pool/main/p/publicsuffix/publicsuffix_20200303.0012-1.debian.tar.xz' publicsuffix_20200303.0012-1.debian.tar.xz 15328 SHA256:3dbbd7b1e20bafc3e5ad73732cb026a4b8e6e5dafa25a9047151e9a28b251647
```

### `dpkg` source package: `pulseaudio=1:13.99.1-1ubuntu3.11`

Binary Packages:

- `libpulse0:amd64=1:13.99.1-1ubuntu3.11`

Licenses: (parsed from: `/usr/share/doc/libpulse0/copyright`)

- `GPL-2`
- `GPL-2+`
- `LGPL-2`
- `LGPL-2+`
- `LGPL-2.1`
- `LGPL-2.1+`

**WARNING:** unable to find source (`apt-get source` failed or returned no results)!  
This is *usually* due to a new package version being released and the old version being removed.


### `dpkg` source package: `pygobject=3.36.0-1`

Binary Packages:

- `python3-gi=3.36.0-1`

Licenses: (parsed from: `/usr/share/doc/python3-gi/copyright`)

- `Expat`
- `LGPL-2`
- `LGPL-2.1+`

Source:

```console
$ apt-get source -qq --print-uris pygobject=3.36.0-1
'http://archive.ubuntu.com/ubuntu/pool/main/p/pygobject/pygobject_3.36.0-1.dsc' pygobject_3.36.0-1.dsc 2829 SHA256:dece737c3e4fe1119fe8b97f9a33ee26af55deea9e91a7fb83e1430fb8444496
'http://archive.ubuntu.com/ubuntu/pool/main/p/pygobject/pygobject_3.36.0.orig.tar.xz' pygobject_3.36.0.orig.tar.xz 555592 SHA256:8683d2dfb5baa9e501a9a64eeba5c2c1117eadb781ab1cd7a9d255834af6daef
'http://archive.ubuntu.com/ubuntu/pool/main/p/pygobject/pygobject_3.36.0-1.debian.tar.xz' pygobject_3.36.0-1.debian.tar.xz 21904 SHA256:3758d828c276e893b95e2280dd254d1984eb0ef7413fc317431037adea5384a9
```

### `dpkg` source package: `python3-defaults=3.8.2-0ubuntu2`

Binary Packages:

- `libpython3-stdlib:amd64=3.8.2-0ubuntu2`
- `python3=3.8.2-0ubuntu2`
- `python3-minimal=3.8.2-0ubuntu2`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris python3-defaults=3.8.2-0ubuntu2
'http://archive.ubuntu.com/ubuntu/pool/main/p/python3-defaults/python3-defaults_3.8.2-0ubuntu2.dsc' python3-defaults_3.8.2-0ubuntu2.dsc 2879 SHA256:3fa296ea2cd52738ebc44a1b83a8df500bf654356336d9bf057144171fe9ee7d
'http://archive.ubuntu.com/ubuntu/pool/main/p/python3-defaults/python3-defaults_3.8.2-0ubuntu2.tar.gz' python3-defaults_3.8.2-0ubuntu2.tar.gz 138226 SHA256:e4969a54306421ebfd195d0c064935db7c53f9f152d8abaae63da33819235e9a
```

### `dpkg` source package: `python3.8=3.8.10-0ubuntu1~20.04.1`

Binary Packages:

- `libpython3.8:amd64=3.8.10-0ubuntu1~20.04.1`
- `libpython3.8-minimal:amd64=3.8.10-0ubuntu1~20.04.1`
- `libpython3.8-stdlib:amd64=3.8.10-0ubuntu1~20.04.1`
- `python3.8=3.8.10-0ubuntu1~20.04.1`
- `python3.8-minimal=3.8.10-0ubuntu1~20.04.1`

Licenses: (parsed from: `/usr/share/doc/libpython3.8/copyright`, `/usr/share/doc/libpython3.8-minimal/copyright`, `/usr/share/doc/libpython3.8-stdlib/copyright`, `/usr/share/doc/python3.8/copyright`, `/usr/share/doc/python3.8-minimal/copyright`)

- `* Permission to use this software in any way is granted without`
- `By obtaining, using, and/or copying this software and/or its`
- `GPL-2`
- `Permission  is  hereby granted,  free  of charge,  to  any person`
- `Permission is hereby granted, free of charge, to any person obtaining`
- `Permission to use, copy, modify,`
- `Redistribution`
- `This software is provided 'as-is', without any express`
- `This software is provided as-is, without express`
- `binary forms, with`
- `distribute this software`
- `distribute this software and`
- `distribute this software for any`
- `implied`
- `its`
- `use in source`
- `without`

Source:

```console
$ apt-get source -qq --print-uris python3.8=3.8.10-0ubuntu1~20.04.1
'http://archive.ubuntu.com/ubuntu/pool/main/p/python3.8/python3.8_3.8.10-0ubuntu1~20.04.1.dsc' python3.8_3.8.10-0ubuntu1~20.04.1.dsc 3504 SHA512:6fa58a364a94a0fa92feef12e9237726a3ee5e03c479524a85c87d505819b1c8cb75ecac74ba37dc3ff3471e5e380da84db8a583b25a3eb2d4d9f123ee2c034b
'http://archive.ubuntu.com/ubuntu/pool/main/p/python3.8/python3.8_3.8.10.orig.tar.xz' python3.8_3.8.10.orig.tar.xz 18433456 SHA512:0be69705483ff9692e12048a96180e586f9d84c8d53066629f7fb2389585eb75c0f3506bb8182936e322508f58b71f4d8c6dfebbab9049b31b49da11d3b98e80
'http://archive.ubuntu.com/ubuntu/pool/main/p/python3.8/python3.8_3.8.10-0ubuntu1~20.04.1.debian.tar.xz' python3.8_3.8.10-0ubuntu1~20.04.1.debian.tar.xz 212920 SHA512:0ddc7a9f161e328f2f7d7686e3736c6f2a0fd248c78472a57a35026c069a08b5d1c56ae8e4d929d5029fa86e2bb2750572ccfd40de656620f117876c2c2a60ec
```

### `dpkg` source package: `raptor2=2.0.15-0ubuntu1.20.04.1`

Binary Packages:

- `libraptor2-0:amd64=2.0.15-0ubuntu1.20.04.1`

Licenses: (parsed from: `/usr/share/doc/libraptor2-0/copyright`)

- `Apache-2.0`
- `GPL-2`
- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris raptor2=2.0.15-0ubuntu1.20.04.1
'http://archive.ubuntu.com/ubuntu/pool/main/r/raptor2/raptor2_2.0.15-0ubuntu1.20.04.1.dsc' raptor2_2.0.15-0ubuntu1.20.04.1.dsc 2252 SHA512:9c68ad686bda3453efaabd80cc47fb4ecb8d5b8598b72716e6873a25f5b0f9d863755be680204310f27f2299336c11595c1a8a782c34559f6711a2ce9bb6944c
'http://archive.ubuntu.com/ubuntu/pool/main/r/raptor2/raptor2_2.0.15.orig.tar.gz' raptor2_2.0.15.orig.tar.gz 1886657 SHA512:563dd01869eb4df8524ec12e2c0a541653874dcd834bd1eb265bc2943bb616968f624121d4688579cdce11b4f00a8ab53b7099f1a0850e256bb0a2c16ba048ee
'http://archive.ubuntu.com/ubuntu/pool/main/r/raptor2/raptor2_2.0.15-0ubuntu1.20.04.1.debian.tar.xz' raptor2_2.0.15-0ubuntu1.20.04.1.debian.tar.xz 8496 SHA512:a18a37d2749d893871555c1c3e980eb5ab480060a33c11de9339c024eb924e50f0716923dd0749c59ab765eaff419ff5d50aeae5c2fd6ee2bf25103328279426
```

### `dpkg` source package: `rasqal=0.9.33-0.1`

Binary Packages:

- `librasqal3:amd64=0.9.33-0.1`

Licenses: (parsed from: `/usr/share/doc/librasqal3/copyright`)

- `Apache-2.0`
- `Apache-2.0+`
- `GPL-2`
- `GPL-2+`
- `LGPL-2.1`
- `LGPL-2.1+`
- `public-domain`

Source:

```console
$ apt-get source -qq --print-uris rasqal=0.9.33-0.1
'http://archive.ubuntu.com/ubuntu/pool/main/r/rasqal/rasqal_0.9.33-0.1.dsc' rasqal_0.9.33-0.1.dsc 2071 SHA256:8eec59c6f2c1d9492e625b8e1b0f76a77eb4bbce04395285dffb062eb778087a
'http://archive.ubuntu.com/ubuntu/pool/main/r/rasqal/rasqal_0.9.33.orig.tar.gz' rasqal_0.9.33.orig.tar.gz 1595647 SHA256:6924c9ac6570bd241a9669f83b467c728a322470bf34f4b2da4f69492ccfd97c
'http://archive.ubuntu.com/ubuntu/pool/main/r/rasqal/rasqal_0.9.33-0.1.debian.tar.xz' rasqal_0.9.33-0.1.debian.tar.xz 5980 SHA256:28916ca977362ff9edf41432faa33c5db25595719d34ede560c15318b970c9b1
```

### `dpkg` source package: `readline=8.0-4`

Binary Packages:

- `libreadline8:amd64=8.0-4`
- `readline-common=8.0-4`

Licenses: (parsed from: `/usr/share/doc/libreadline8/copyright`, `/usr/share/doc/readline-common/copyright`)

- `GFDL`
- `GPL-3`

Source:

```console
$ apt-get source -qq --print-uris readline=8.0-4
'http://archive.ubuntu.com/ubuntu/pool/main/r/readline/readline_8.0-4.dsc' readline_8.0-4.dsc 2434 SHA256:ac9c7bb7380fe740aef09f54becf482eb81032a33dc11f1a8f00e933c5f168f4
'http://archive.ubuntu.com/ubuntu/pool/main/r/readline/readline_8.0.orig.tar.gz' readline_8.0.orig.tar.gz 2975937 SHA256:e339f51971478d369f8a053a330a190781acb9864cf4c541060f12078948e461
'http://archive.ubuntu.com/ubuntu/pool/main/r/readline/readline_8.0-4.debian.tar.xz' readline_8.0-4.debian.tar.xz 30408 SHA256:60ed18dab6d6b7fc998a263d917f06d9cce6e1ccd19cd8bf4a9d33c5350cf8d6
```

### `dpkg` source package: `redland=1.0.17-1.1ubuntu1`

Binary Packages:

- `librdf0:amd64=1.0.17-1.1ubuntu1`

Licenses: (parsed from: `/usr/share/doc/librdf0/copyright`)

- `Apache-2.0`
- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris redland=1.0.17-1.1ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/r/redland/redland_1.0.17-1.1ubuntu1.dsc' redland_1.0.17-1.1ubuntu1.dsc 2460 SHA256:ec639fe72a9fb3d9973c20a1f73edf5c9507a1ff89c1d9c7095a6a6752de3fbb
'http://archive.ubuntu.com/ubuntu/pool/main/r/redland/redland_1.0.17.orig.tar.gz' redland_1.0.17.orig.tar.gz 1621566 SHA256:de1847f7b59021c16bdc72abb4d8e2d9187cd6124d69156f3326dd34ee043681
'http://archive.ubuntu.com/ubuntu/pool/main/r/redland/redland_1.0.17-1.1ubuntu1.debian.tar.xz' redland_1.0.17-1.1ubuntu1.debian.tar.xz 8696 SHA256:234374b8c10a7c226a91e5765a25abd54481baf09bc07836d536d2eb26f0f421
```

### `dpkg` source package: `rtmpdump=2.4+20151223.gitfa8646d.1-2build1`

Binary Packages:

- `librtmp1:amd64=2.4+20151223.gitfa8646d.1-2build1`

Licenses: (parsed from: `/usr/share/doc/librtmp1/copyright`)

- `GPL-2`
- `LGPL-2.1`

Source:

```console
$ apt-get source -qq --print-uris rtmpdump=2.4+20151223.gitfa8646d.1-2build1
'http://archive.ubuntu.com/ubuntu/pool/main/r/rtmpdump/rtmpdump_2.4+20151223.gitfa8646d.1-2build1.dsc' rtmpdump_2.4+20151223.gitfa8646d.1-2build1.dsc 2439 SHA256:fd89213f2d41b00c212a411a945146c6b2e00fce1d1819a9ec380b0d91bd1077
'http://archive.ubuntu.com/ubuntu/pool/main/r/rtmpdump/rtmpdump_2.4+20151223.gitfa8646d.1.orig.tar.gz' rtmpdump_2.4+20151223.gitfa8646d.1.orig.tar.gz 142213 SHA256:5c032f5c8cc2937eb55a81a94effdfed3b0a0304b6376147b86f951e225e3ab5
'http://archive.ubuntu.com/ubuntu/pool/main/r/rtmpdump/rtmpdump_2.4+20151223.gitfa8646d.1-2build1.debian.tar.xz' rtmpdump_2.4+20151223.gitfa8646d.1-2build1.debian.tar.xz 8216 SHA256:b256cc2aa96c9b99918052c4badfab0339ba95a852eab5ae37aa8b53c259efd2
```

### `dpkg` source package: `rubberband=1.8.2-1build1`

Binary Packages:

- `librubberband2:amd64=1.8.2-1build1`

Licenses: (parsed from: `/usr/share/doc/librubberband2/copyright`)

- `GPL-2`
- `GPL-2+`
- `other-1`
- `other-bsd-3-clause-kissft`
- `other-bsd-3-clause-speex`
- `other-bsd-4-clause-1`
- `other-bsd-4-clause-2`
- `zlib`

Source:

```console
$ apt-get source -qq --print-uris rubberband=1.8.2-1build1
'http://archive.ubuntu.com/ubuntu/pool/universe/r/rubberband/rubberband_1.8.2-1build1.dsc' rubberband_1.8.2-1build1.dsc 2376 SHA256:078ea7ff612d1cc20135d4b2229e5845dc7ff2ce92555a34cbe0d8531cb3b10e
'http://archive.ubuntu.com/ubuntu/pool/universe/r/rubberband/rubberband_1.8.2.orig.tar.bz2' rubberband_1.8.2.orig.tar.bz2 182232 SHA256:86bed06b7115b64441d32ae53634fcc0539a50b9b648ef87443f936782f6c3ca
'http://archive.ubuntu.com/ubuntu/pool/universe/r/rubberband/rubberband_1.8.2-1build1.debian.tar.xz' rubberband_1.8.2-1build1.debian.tar.xz 9484 SHA256:d1c232d14fb6ceac8804d0de74e93b142d2e60b8c25949131f95ff5270885a41
```

### `dpkg` source package: `scowl=2018.04.16-1`

Binary Packages:

- `hunspell-en-us=1:2018.04.16-1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris scowl=2018.04.16-1
'http://archive.ubuntu.com/ubuntu/pool/main/s/scowl/scowl_2018.04.16-1.dsc' scowl_2018.04.16-1.dsc 2935 SHA256:b2ad55853fbad895df408a380edfd23661195e0b5ce2fd47fd2db5a295c3192a
'http://archive.ubuntu.com/ubuntu/pool/main/s/scowl/scowl_2018.04.16.orig.tar.gz' scowl_2018.04.16.orig.tar.gz 2545797 SHA256:3a97d5a92a8705a35611ac21a61ed72ad9a19baa64168273deb5b8cca1cd9386
'http://archive.ubuntu.com/ubuntu/pool/main/s/scowl/scowl_2018.04.16-1.debian.tar.xz' scowl_2018.04.16-1.debian.tar.xz 16316 SHA256:2b8b879377cd470a242003abf138a2fc6a1b4e3e232bdd67ca507470d858d034
```

### `dpkg` source package: `sed=4.7-1`

Binary Packages:

- `sed=4.7-1`

Licenses: (parsed from: `/usr/share/doc/sed/copyright`)

- `GPL-3`

Source:

```console
$ apt-get source -qq --print-uris sed=4.7-1
'http://archive.ubuntu.com/ubuntu/pool/main/s/sed/sed_4.7-1.dsc' sed_4.7-1.dsc 1880 SHA256:dd0e8daed987929920f7729771f9c7a5b48d094923aaf686efd2ab19db776108
'http://archive.ubuntu.com/ubuntu/pool/main/s/sed/sed_4.7.orig.tar.xz' sed_4.7.orig.tar.xz 1298316 SHA256:2885768cd0a29ff8d58a6280a270ff161f6a3deb5690b2be6c49f46d4c67bd6a
'http://archive.ubuntu.com/ubuntu/pool/main/s/sed/sed_4.7-1.debian.tar.xz' sed_4.7-1.debian.tar.xz 59824 SHA256:a2ab8d50807fd2242f86d6c6257399e790445ab6f8932f7f487d34361b4fc483
```

### `dpkg` source package: `sensible-utils=0.0.12+nmu1`

Binary Packages:

- `sensible-utils=0.0.12+nmu1`

Licenses: (parsed from: `/usr/share/doc/sensible-utils/copyright`)

- `All-permissive`
- `GPL-2`
- `GPL-2+`
- `configure`
- `installsh`

Source:

```console
$ apt-get source -qq --print-uris sensible-utils=0.0.12+nmu1
'http://archive.ubuntu.com/ubuntu/pool/main/s/sensible-utils/sensible-utils_0.0.12+nmu1.dsc' sensible-utils_0.0.12+nmu1.dsc 1753 SHA256:68bcb3e542e29a8a0bf281d9145d0e4cd9def529af2ba0cfe0afee3c5af958bc
'http://archive.ubuntu.com/ubuntu/pool/main/s/sensible-utils/sensible-utils_0.0.12+nmu1.tar.xz' sensible-utils_0.0.12+nmu1.tar.xz 61988 SHA256:53c6606facf083adbbf0da04e6d774b31ff3f46c7ba36a82d3f182779f4c3f5b
```

### `dpkg` source package: `serd=0.30.2-1`

Binary Packages:

- `libserd-0-0:amd64=0.30.2-1`

Licenses: (parsed from: `/usr/share/doc/libserd-0-0/copyright`)

- `BSD-3-clause`
- `ISC`

Source:

```console
$ apt-get source -qq --print-uris serd=0.30.2-1
'http://archive.ubuntu.com/ubuntu/pool/universe/s/serd/serd_0.30.2-1.dsc' serd_0.30.2-1.dsc 2160 SHA256:389ecebd3ca416d9f16645a31d9d1e69ae7b7288b5dd132027fc9768f3dd85b1
'http://archive.ubuntu.com/ubuntu/pool/universe/s/serd/serd_0.30.2.orig.tar.bz2' serd_0.30.2.orig.tar.bz2 590337 SHA256:9d3102701172804f823f2215ca3147c50eba992641f9fbe014272355f4937202
'http://archive.ubuntu.com/ubuntu/pool/universe/s/serd/serd_0.30.2-1.debian.tar.xz' serd_0.30.2-1.debian.tar.xz 6440 SHA256:4382463bb2e194966868d16b6839e63dfe6c424fbe8edfdb246bf8c0afe14e6b
```

### `dpkg` source package: `servlet-api=4.0.1-2`

Binary Packages:

- `libservlet-api-java=4.0.1-2`
- `libservlet3.1-java=1:4.0.1-2`

Licenses: (parsed from: `/usr/share/doc/libservlet-api-java/copyright`, `/usr/share/doc/libservlet3.1-java/copyright`)

- `Apache-2.0`
- `CDDL-1.1`
- `GPL-2`
- `GPL-2 with Classpath exception`

Source:

```console
$ apt-get source -qq --print-uris servlet-api=4.0.1-2
'http://archive.ubuntu.com/ubuntu/pool/universe/s/servlet-api/servlet-api_4.0.1-2.dsc' servlet-api_4.0.1-2.dsc 2258 SHA256:9659543651f44d13bf137ac61f2ea4e6d33f21bed3deb0ad8bf6c9ab3e8837f8
'http://archive.ubuntu.com/ubuntu/pool/universe/s/servlet-api/servlet-api_4.0.1.orig.tar.xz' servlet-api_4.0.1.orig.tar.xz 94792 SHA256:26328ec380389cf60b9968ede81bab261409f6a2976635a826d3c39dbd8bacc4
'http://archive.ubuntu.com/ubuntu/pool/universe/s/servlet-api/servlet-api_4.0.1-2.debian.tar.xz' servlet-api_4.0.1-2.debian.tar.xz 10856 SHA256:5505b670fa2a3cba350b5cc98c2276948804e35a9d9edb15dbc0f786f595a9ef
```

### `dpkg` source package: `shadow=1:4.8.1-1ubuntu5.20.04.1`

Binary Packages:

- `login=1:4.8.1-1ubuntu5.20.04.1`
- `passwd=1:4.8.1-1ubuntu5.20.04.1`

Licenses: (parsed from: `/usr/share/doc/login/copyright`, `/usr/share/doc/passwd/copyright`)

- `GPL-2`

Source:

```console
$ apt-get source -qq --print-uris shadow=1:4.8.1-1ubuntu5.20.04.1
'http://archive.ubuntu.com/ubuntu/pool/main/s/shadow/shadow_4.8.1-1ubuntu5.20.04.1.dsc' shadow_4.8.1-1ubuntu5.20.04.1.dsc 2377 SHA512:e82923df5238b076edab6dda322213e2231e156c0c571d404fde0e7a91e1ee3491948af13acb40f3405b54169935411357b4b878849dac04bee7d73e13a42bb8
'http://archive.ubuntu.com/ubuntu/pool/main/s/shadow/shadow_4.8.1.orig.tar.xz' shadow_4.8.1.orig.tar.xz 1611196 SHA512:780a983483d847ed3c91c82064a0fa902b6f4185225978241bc3bc03fcc3aa143975b46aee43151c6ba43efcfdb1819516b76ba7ad3d1d3c34fcc38ea42e917b
'http://archive.ubuntu.com/ubuntu/pool/main/s/shadow/shadow_4.8.1-1ubuntu5.20.04.1.debian.tar.xz' shadow_4.8.1-1ubuntu5.20.04.1.debian.tar.xz 86252 SHA512:cd31ba719b2555f8465f36889253bd9fbe997e97010f2e1f40961d55cb4ff08a438e6b1a5e3560028e81e8616338af3888620a058743bb113e1464ec4c01949b
```

### `dpkg` source package: `shared-mime-info=1.15-1`

Binary Packages:

- `shared-mime-info=1.15-1`

Licenses: (parsed from: `/usr/share/doc/shared-mime-info/copyright`)

- `GPL`

Source:

```console
$ apt-get source -qq --print-uris shared-mime-info=1.15-1
'http://archive.ubuntu.com/ubuntu/pool/main/s/shared-mime-info/shared-mime-info_1.15-1.dsc' shared-mime-info_1.15-1.dsc 2198 SHA256:dca6ea0156110b4a2694dd96a721d34ad4f42b51f3d3a20d0d711b77bde5115d
'http://archive.ubuntu.com/ubuntu/pool/main/s/shared-mime-info/shared-mime-info_1.15.orig.tar.xz' shared-mime-info_1.15.orig.tar.xz 772708 SHA256:f482b027437c99e53b81037a9843fccd549243fd52145d016e9c7174a4f5db90
'http://archive.ubuntu.com/ubuntu/pool/main/s/shared-mime-info/shared-mime-info_1.15-1.debian.tar.xz' shared-mime-info_1.15-1.debian.tar.xz 9728 SHA256:02c4fa8b2b3073c745287dd0e00c69c9f1ba028c7c6496105e3ecdcc02d9f1dd
```

### `dpkg` source package: `shine=3.1.1-2`

Binary Packages:

- `libshine3:amd64=3.1.1-2`

Licenses: (parsed from: `/usr/share/doc/libshine3/copyright`)

- `GPL-2`
- `GPL-2+`
- `LGPL-2`

Source:

```console
$ apt-get source -qq --print-uris shine=3.1.1-2
'http://archive.ubuntu.com/ubuntu/pool/universe/s/shine/shine_3.1.1-2.dsc' shine_3.1.1-2.dsc 1999 SHA256:57792862005a2482a7c1ee94544dd30bdeeacbf8b4cad842ad741b65545e8a16
'http://archive.ubuntu.com/ubuntu/pool/universe/s/shine/shine_3.1.1.orig.tar.gz' shine_3.1.1.orig.tar.gz 940443 SHA256:565b87867d6f8e6616a236445d194e36f4daa9b4e7af823fcf5010af7610c49e
'http://archive.ubuntu.com/ubuntu/pool/universe/s/shine/shine_3.1.1-2.debian.tar.xz' shine_3.1.1-2.debian.tar.xz 3624 SHA256:a9f669c5af27f11c0cca98c736decc49b056ccfe32893f85a6064161f36b1b5b
```

### `dpkg` source package: `slang2=2.3.2-4`

Binary Packages:

- `libslang2:amd64=2.3.2-4`

Licenses: (parsed from: `/usr/share/doc/libslang2/copyright`)

- `GPL-2`
- `GPL-2+`

Source:

```console
$ apt-get source -qq --print-uris slang2=2.3.2-4
'http://archive.ubuntu.com/ubuntu/pool/main/s/slang2/slang2_2.3.2-4.dsc' slang2_2.3.2-4.dsc 2294 SHA256:ea599dbb3a16618acdc2c11b7e7d140477478c9ebfd1d15f359daf623815eec9
'http://archive.ubuntu.com/ubuntu/pool/main/s/slang2/slang2_2.3.2.orig.tar.xz' slang2_2.3.2.orig.tar.xz 1309848 SHA256:18c99f4c5ad9710eb0fcd4c82f7c32427f94c9c93a5ba04a88318e521db2cadf
'http://archive.ubuntu.com/ubuntu/pool/main/s/slang2/slang2_2.3.2-4.debian.tar.xz' slang2_2.3.2-4.debian.tar.xz 22136 SHA256:6c9f670b8a0000e6960aff0a6c817acb9dea31ac7ae356e9d841626fede518e4
```

### `dpkg` source package: `snappy=1.1.8-1build1`

Binary Packages:

- `libsnappy1v5:amd64=1.1.8-1build1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris snappy=1.1.8-1build1
'http://archive.ubuntu.com/ubuntu/pool/main/s/snappy/snappy_1.1.8-1build1.dsc' snappy_1.1.8-1build1.dsc 1845 SHA256:7fcb2d9b6b28da43d3db8e4f3b5907cdce080f3e05881bb8e55a3d0d07cd39fb
'http://archive.ubuntu.com/ubuntu/pool/main/s/snappy/snappy_1.1.8.orig.tar.gz' snappy_1.1.8.orig.tar.gz 1096137 SHA256:16b677f07832a612b0836178db7f374e414f94657c138e6993cbfc5dcc58651f
'http://archive.ubuntu.com/ubuntu/pool/main/s/snappy/snappy_1.1.8-1build1.debian.tar.xz' snappy_1.1.8-1build1.debian.tar.xz 5680 SHA256:715185588364cd9625bff4a0d3f508054ee04f88573e3adce55167ee6baf9fd2
```

### `dpkg` source package: `sndio=1.5.0-3`

Binary Packages:

- `libsndio7.0:amd64=1.5.0-3`

Licenses: (parsed from: `/usr/share/doc/libsndio7.0/copyright`)

- `ISC`
- `ISC-packaging`

Source:

```console
$ apt-get source -qq --print-uris sndio=1.5.0-3
'http://archive.ubuntu.com/ubuntu/pool/universe/s/sndio/sndio_1.5.0-3.dsc' sndio_1.5.0-3.dsc 1942 SHA256:e024ba6ddd4bcc81bf955689a55c454a8a031b729addaed6aa0bb05afc2ad3b1
'http://archive.ubuntu.com/ubuntu/pool/universe/s/sndio/sndio_1.5.0.orig.tar.gz' sndio_1.5.0.orig.tar.gz 125661 SHA256:12c70044749ad9cb7eaeb26c936816aa6b314fe4be71ef479d12272e4c5ad253
'http://archive.ubuntu.com/ubuntu/pool/universe/s/sndio/sndio_1.5.0-3.debian.tar.xz' sndio_1.5.0-3.debian.tar.xz 5780 SHA256:325417b7a391a106ede0d1f30cbc0e1bbbda56ef2713c7598a1436c1d92c7d03
```

### `dpkg` source package: `sord=0.16.4-1`

Binary Packages:

- `libsord-0-0:amd64=0.16.4-1`

Licenses: (parsed from: `/usr/share/doc/libsord-0-0/copyright`)

- `BSD-3-clause`
- `ISC`

Source:

```console
$ apt-get source -qq --print-uris sord=0.16.4-1
'http://archive.ubuntu.com/ubuntu/pool/universe/s/sord/sord_0.16.4-1.dsc' sord_0.16.4-1.dsc 2180 SHA256:17eeeb1e265fe490ccd1f35437424644ff6e55a315f97d56b1252bddb68fc532
'http://archive.ubuntu.com/ubuntu/pool/universe/s/sord/sord_0.16.4.orig.tar.bz2' sord_0.16.4.orig.tar.bz2 510920 SHA256:b15998f4e7ad958201346009477d6696e90ee5d3e9aff25e7e9be074372690d7
'http://archive.ubuntu.com/ubuntu/pool/universe/s/sord/sord_0.16.4-1.debian.tar.xz' sord_0.16.4-1.debian.tar.xz 5080 SHA256:0f9e5903950cd3010e8d0c3311a4c19413b49b43862f1264a57a4cd08e39d277
```

### `dpkg` source package: `sound-theme-freedesktop=0.8-2ubuntu1`

Binary Packages:

- `sound-theme-freedesktop=0.8-2ubuntu1`

Licenses: (parsed from: `/usr/share/doc/sound-theme-freedesktop/copyright`)

- `CC-BY-3.0`
- `CC-BY-SA-3.0`
- `GPL-2`
- `GPL-2+`

Source:

```console
$ apt-get source -qq --print-uris sound-theme-freedesktop=0.8-2ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/s/sound-theme-freedesktop/sound-theme-freedesktop_0.8-2ubuntu1.dsc' sound-theme-freedesktop_0.8-2ubuntu1.dsc 2335 SHA256:41ad1232719c5bb49c98c370c5386a4285ab130627a39a7405a12f1e97471474
'http://archive.ubuntu.com/ubuntu/pool/main/s/sound-theme-freedesktop/sound-theme-freedesktop_0.8.orig.tar.bz2' sound-theme-freedesktop_0.8.orig.tar.bz2 478237 SHA256:cb518b20eef05ec2e82dda1fa89a292c1760dc023aba91b8aa69bafac85e8a14
'http://archive.ubuntu.com/ubuntu/pool/main/s/sound-theme-freedesktop/sound-theme-freedesktop_0.8-2ubuntu1.debian.tar.xz' sound-theme-freedesktop_0.8-2ubuntu1.debian.tar.xz 10120 SHA256:74a5b6a722c7f48e70e7e36312adf5526c20337f914fcbf32282a387cf06f8b3
```

### `dpkg` source package: `speex=1.2~rc1.2-1.1ubuntu1`

Binary Packages:

- `libspeex1:amd64=1.2~rc1.2-1.1ubuntu1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris speex=1.2~rc1.2-1.1ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/s/speex/speex_1.2~rc1.2-1.1ubuntu1.dsc' speex_1.2~rc1.2-1.1ubuntu1.dsc 2323 SHA256:4c84e22ebae537e8cda353d6d780ae2e7e8d324cd2d5697ba544981d1985ecf9
'http://archive.ubuntu.com/ubuntu/pool/main/s/speex/speex_1.2~rc1.2.orig.tar.gz' speex_1.2~rc1.2.orig.tar.gz 1069339 SHA256:8320fb86a024dfe1b6a78a7d57bc2388e5f8cb7f2fa10c946db2704e1e5d2805
'http://archive.ubuntu.com/ubuntu/pool/main/s/speex/speex_1.2~rc1.2-1.1ubuntu1.diff.gz' speex_1.2~rc1.2-1.1ubuntu1.diff.gz 10477 SHA256:afe21b8b229dcdf8dd272d0ca724e9973aa4393ad5ff5959357cce7d23e0680b
```

### `dpkg` source package: `sqlite3=3.31.1-4ubuntu0.2`

Binary Packages:

- `libsqlite3-0:amd64=3.31.1-4ubuntu0.2`

Licenses: (parsed from: `/usr/share/doc/libsqlite3-0/copyright`)

- `GPL-2`
- `GPL-2+`
- `public-domain`

Source:

```console
$ apt-get source -qq --print-uris sqlite3=3.31.1-4ubuntu0.2
'http://archive.ubuntu.com/ubuntu/pool/main/s/sqlite3/sqlite3_3.31.1-4ubuntu0.2.dsc' sqlite3_3.31.1-4ubuntu0.2.dsc 2519 SHA512:d7adfe86e7996d2a45238bc3b782de6894a585bba1498b3747aacb15313160354bc072ab2e257a2a034ce44921bd07329b8a9172290428f753274b6d7b00326e
'http://archive.ubuntu.com/ubuntu/pool/main/s/sqlite3/sqlite3_3.31.1.orig-www.tar.xz' sqlite3_3.31.1.orig-www.tar.xz 5764424 SHA512:a47adacd46c673cfd674cb64fb54b054e69560aed8c8c429773f0eccdcdbce4be538397506eca8e2d169f4b46d0d47442b273e12d82f8c87e1aadf3ade458db6
'http://archive.ubuntu.com/ubuntu/pool/main/s/sqlite3/sqlite3_3.31.1.orig.tar.xz' sqlite3_3.31.1.orig.tar.xz 7108036 SHA512:67e1050efe2988fa3d0d7e4a87e147a8114c6ff9b6ca5307a068befb38e861930eaee0135048ff1abb1e6323b507cbc68a0aac3a8fe5f095d6fcea1547a7efaf
'http://archive.ubuntu.com/ubuntu/pool/main/s/sqlite3/sqlite3_3.31.1-4ubuntu0.2.debian.tar.xz' sqlite3_3.31.1-4ubuntu0.2.debian.tar.xz 33492 SHA512:87cc51bce108d6306d815683a1825b43a6e7a7d8dc2bade8ce34d3850b8a4437034b1383f5e60b8d9c4569e8ab5bb3eb28febda7745f4a9db01b5e1b0b11d5ae
```

### `dpkg` source package: `sratom=0.6.4-1`

Binary Packages:

- `libsratom-0-0:amd64=0.6.4-1`

Licenses: (parsed from: `/usr/share/doc/libsratom-0-0/copyright`)

- `BSD-3-clause`
- `ISC`

Source:

```console
$ apt-get source -qq --print-uris sratom=0.6.4-1
'http://archive.ubuntu.com/ubuntu/pool/universe/s/sratom/sratom_0.6.4-1.dsc' sratom_0.6.4-1.dsc 2211 SHA256:62a58eec187f9878db7da6b9da7ec2ed2dcb29b34f8f983c415637c82416bd64
'http://archive.ubuntu.com/ubuntu/pool/universe/s/sratom/sratom_0.6.4.orig.tar.bz2' sratom_0.6.4.orig.tar.bz2 339707 SHA256:146c8f14b8902ac3c8fa8c2e0a014eb8a38fab60090c5adbfbff3e3b7c5c006e
'http://archive.ubuntu.com/ubuntu/pool/universe/s/sratom/sratom_0.6.4-1.debian.tar.xz' sratom_0.6.4-1.debian.tar.xz 4420 SHA256:7c2aa9b8abc9ceb6343f86143526e6e405dc198f64e11a8196cdbbadc42833f7
```

### `dpkg` source package: `suitesparse=1:5.7.1+dfsg-2`

Binary Packages:

- `libcolamd2:amd64=1:5.7.1+dfsg-2`
- `libsuitesparseconfig5:amd64=1:5.7.1+dfsg-2`

Licenses: (parsed from: `/usr/share/doc/libcolamd2/copyright`, `/usr/share/doc/libsuitesparseconfig5/copyright`)

- `Apache-2.0`
- `BSD-2-clause`
- `BSD-2-clause-lagraph`
- `BSD-3-clause`
- `GPL-2`
- `GPL-2+`
- `GPL-3`
- `GPL-3+`
- `LGPL-2.1`
- `LGPL-2.1+`
- `permissive`
- `permissive-2`

Source:

```console
$ apt-get source -qq --print-uris suitesparse=1:5.7.1+dfsg-2
'http://archive.ubuntu.com/ubuntu/pool/main/s/suitesparse/suitesparse_5.7.1+dfsg-2.dsc' suitesparse_5.7.1+dfsg-2.dsc 3094 SHA256:4da625fc62ec35bfe9a91c62ff8d508407ebe69364e15c8819508d32ea4d8110
'http://archive.ubuntu.com/ubuntu/pool/main/s/suitesparse/suitesparse_5.7.1+dfsg.orig.tar.xz' suitesparse_5.7.1+dfsg.orig.tar.xz 36676832 SHA256:47edbe6d1ed2ed00a0d08ecdbad0a57712d87efd3968c7d362cdf475ad597af7
'http://archive.ubuntu.com/ubuntu/pool/main/s/suitesparse/suitesparse_5.7.1+dfsg-2.debian.tar.xz' suitesparse_5.7.1+dfsg-2.debian.tar.xz 42744 SHA256:c8c1681fa3a2518795c31f482ebd15160d33f0f9edff81558a09f84c701516f9
```

### `dpkg` source package: `systemd=245.4-4ubuntu3.13`

Binary Packages:

- `libnss-systemd:amd64=245.4-4ubuntu3.13`
- `libpam-systemd:amd64=245.4-4ubuntu3.13`
- `libsystemd0:amd64=245.4-4ubuntu3.13`
- `libudev1:amd64=245.4-4ubuntu3.13`
- `systemd=245.4-4ubuntu3.13`
- `systemd-sysv=245.4-4ubuntu3.13`
- `systemd-timesyncd=245.4-4ubuntu3.13`

Licenses: (parsed from: `/usr/share/doc/libnss-systemd/copyright`, `/usr/share/doc/libpam-systemd/copyright`, `/usr/share/doc/libsystemd0/copyright`, `/usr/share/doc/libudev1/copyright`, `/usr/share/doc/systemd/copyright`, `/usr/share/doc/systemd-sysv/copyright`, `/usr/share/doc/systemd-timesyncd/copyright`)

- `CC0-1.0`
- `Expat`
- `GPL-2`
- `GPL-2 with Linux-syscall-note exception`
- `GPL-2+`
- `LGPL-2.1`
- `LGPL-2.1+`
- `public-domain`

Source:

```console
$ apt-get source -qq --print-uris systemd=245.4-4ubuntu3.13
'http://archive.ubuntu.com/ubuntu/pool/main/s/systemd/systemd_245.4-4ubuntu3.13.dsc' systemd_245.4-4ubuntu3.13.dsc 5262 SHA512:a36f8fbe78b309da85efb204c1b37c99a441619a09fe7770b35e4e6aa1958667733b737b1cc3b54c98209091a744c5f5e98377086a346b0a551fb9d0cda484d4
'http://archive.ubuntu.com/ubuntu/pool/main/s/systemd/systemd_245.4.orig.tar.gz' systemd_245.4.orig.tar.gz 9000780 SHA512:02036bb1ab05301a9d0dfdd4b9c9376e90134474482531e6e292122380be2f24f99177493dd3af6f8af1a8ed2599ee0996da91a3b1b7872bbfaf26a1c3e61b4c
'http://archive.ubuntu.com/ubuntu/pool/main/s/systemd/systemd_245.4-4ubuntu3.13.debian.tar.xz' systemd_245.4-4ubuntu3.13.debian.tar.xz 265612 SHA512:86690ede8cf3f3a5b9f8971b0d99c616297a0c895f71fc0c6c6fb34a771f5a7db2a951426a469779e3f9f4ca3d2f974c3c47d347bae6f247dc2bf4a082bb3b0f
```

### `dpkg` source package: `sysvinit=2.96-2.1ubuntu1`

Binary Packages:

- `sysvinit-utils=2.96-2.1ubuntu1`

Licenses: (parsed from: `/usr/share/doc/sysvinit-utils/copyright`)

- `GPL-2`
- `GPL-2+`

Source:

```console
$ apt-get source -qq --print-uris sysvinit=2.96-2.1ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/s/sysvinit/sysvinit_2.96-2.1ubuntu1.dsc' sysvinit_2.96-2.1ubuntu1.dsc 2751 SHA256:c8b5f2ef86c4c1b8bf6b8a48408a4aa0815b0cf416df51dc0a9b6b8134f7e42c
'http://archive.ubuntu.com/ubuntu/pool/main/s/sysvinit/sysvinit_2.96.orig.tar.xz' sysvinit_2.96.orig.tar.xz 122164 SHA256:2a2e26b72aa235a23ab1c8471005f890309ce1196c83fbc9413c57b9ab62b587
'http://archive.ubuntu.com/ubuntu/pool/main/s/sysvinit/sysvinit_2.96.orig.tar.xz.asc' sysvinit_2.96.orig.tar.xz.asc 313 SHA256:dfc184b95da12c8c888c8ae6b0f26fe8a23b07fbcdd240f6600a8a78b9439fa0
'http://archive.ubuntu.com/ubuntu/pool/main/s/sysvinit/sysvinit_2.96-2.1ubuntu1.debian.tar.xz' sysvinit_2.96-2.1ubuntu1.debian.tar.xz 128840 SHA256:528041e261c90a957d9794bddb07217c89484d9c76a0279da508baec9684c4e6
```

### `dpkg` source package: `tar=1.30+dfsg-7ubuntu0.20.04.1`

Binary Packages:

- `tar=1.30+dfsg-7ubuntu0.20.04.1`

Licenses: (parsed from: `/usr/share/doc/tar/copyright`)

- `GPL-2`
- `GPL-3`

Source:

```console
$ apt-get source -qq --print-uris tar=1.30+dfsg-7ubuntu0.20.04.1
'http://archive.ubuntu.com/ubuntu/pool/main/t/tar/tar_1.30+dfsg-7ubuntu0.20.04.1.dsc' tar_1.30+dfsg-7ubuntu0.20.04.1.dsc 1946 SHA512:beea3a39a93de0702e33e7ef666bf489e6b1521091b9068ab3bab757e8427cefa6fbf9dbb971fbb391188e8463cf465176f8b36869c9eea9fb198171b2617614
'http://archive.ubuntu.com/ubuntu/pool/main/t/tar/tar_1.30+dfsg.orig.tar.xz' tar_1.30+dfsg.orig.tar.xz 1883220 SHA512:f9b3843bd4da03f58d6f88de70ecb36b8ac29312714fd2120ff00f17c99e6d77cc82a8f9de348f4c2bdba9a6cc8e8c6c78039b6c14cdee15d68f2517000c36f2
'http://archive.ubuntu.com/ubuntu/pool/main/t/tar/tar_1.30+dfsg-7ubuntu0.20.04.1.debian.tar.xz' tar_1.30+dfsg-7ubuntu0.20.04.1.debian.tar.xz 22616 SHA512:015cf0e3eadb05e07b1c555afadf2f8d6af21f82a375ad0f87682af5e2c4b10e38e0f7ee6ac72fa5ecea5bc40cd4600cd0f2aed06c3c8051a6191d51d124003e
```

### `dpkg` source package: `tcp-wrappers=7.6.q-30`

Binary Packages:

- `libwrap0:amd64=7.6.q-30`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris tcp-wrappers=7.6.q-30
'http://archive.ubuntu.com/ubuntu/pool/main/t/tcp-wrappers/tcp-wrappers_7.6.q-30.dsc' tcp-wrappers_7.6.q-30.dsc 1900 SHA256:b3096434ab68ae8a48cc78a1553e461867c8c29840053554b64fcce0e83c82e2
'http://archive.ubuntu.com/ubuntu/pool/main/t/tcp-wrappers/tcp-wrappers_7.6.q.orig.tar.gz' tcp-wrappers_7.6.q.orig.tar.gz 99438 SHA256:9543d7adedf78a6de0b221ccbbd1952e08b5138717f4ade814039bb489a4315d
'http://archive.ubuntu.com/ubuntu/pool/main/t/tcp-wrappers/tcp-wrappers_7.6.q-30.debian.tar.xz' tcp-wrappers_7.6.q-30.debian.tar.xz 36204 SHA256:71661be900202a909144ba1f49f7dceb83a619b88a11aca3b9d867934c0dbb36
```

### `dpkg` source package: `tdb=1.4.2-3build1`

Binary Packages:

- `libtdb1:amd64=1.4.2-3build1`

Licenses: (parsed from: `/usr/share/doc/libtdb1/copyright`)

- `BSD-3`
- `GPL-3`
- `GPL-3.0+`
- `ISC`
- `LGPL-3`
- `LGPL-3.0+`
- `PostgreSQL`

Source:

```console
$ apt-get source -qq --print-uris tdb=1.4.2-3build1
'http://archive.ubuntu.com/ubuntu/pool/main/t/tdb/tdb_1.4.2-3build1.dsc' tdb_1.4.2-3build1.dsc 2210 SHA256:fb7bb1a4f7144043bd942c0a90088504730e3900b10f91cd2fddf9bb59668065
'http://archive.ubuntu.com/ubuntu/pool/main/t/tdb/tdb_1.4.2.orig.tar.gz' tdb_1.4.2.orig.tar.gz 706236 SHA256:9040b2cce4028e392f063f91bbe76b8b28fecc2b7c0c6071c67b5eb3168e004a
'http://archive.ubuntu.com/ubuntu/pool/main/t/tdb/tdb_1.4.2-3build1.debian.tar.xz' tdb_1.4.2-3build1.debian.tar.xz 17940 SHA256:9b1398f03a10972c5ed258b11abaa09ff6614c340c4157e8db6dd1b31663e2d7
```

### `dpkg` source package: `tiff=4.1.0+git191117-2ubuntu0.20.04.2`

Binary Packages:

- `libtiff5:amd64=4.1.0+git191117-2ubuntu0.20.04.2`

Licenses: (parsed from: `/usr/share/doc/libtiff5/copyright`)

- `Hylafax`

Source:

```console
$ apt-get source -qq --print-uris tiff=4.1.0+git191117-2ubuntu0.20.04.2
'http://archive.ubuntu.com/ubuntu/pool/main/t/tiff/tiff_4.1.0+git191117-2ubuntu0.20.04.2.dsc' tiff_4.1.0+git191117-2ubuntu0.20.04.2.dsc 2381 SHA512:50cda0d283ddce816b6dc5643a6e1bb37c7ae9da89f4245e6917613181c9629c166470205609093ec14b3ed0063196afcac00582c87c3ec0773b9b31f7843676
'http://archive.ubuntu.com/ubuntu/pool/main/t/tiff/tiff_4.1.0+git191117.orig.tar.xz' tiff_4.1.0+git191117.orig.tar.xz 1533524 SHA512:25b4bc4522fc2e7f3ca6857b87acd4481d8643566b1120c755020afc8b48949238ee2078bc43dd3ba7407eaa4e36b1b712d7056f101ddaf60f94dab8607870b8
'http://archive.ubuntu.com/ubuntu/pool/main/t/tiff/tiff_4.1.0+git191117-2ubuntu0.20.04.2.debian.tar.xz' tiff_4.1.0+git191117-2ubuntu0.20.04.2.debian.tar.xz 21004 SHA512:e5e4c25d0ec9bd5adfff6892ea72eae7e54919e0ee063bd2f820cb16e0876a34b8f160764ee5d47eead4a621fa68f7f87e6c926f24a3144b2a74a5d3ebe0c2a2
```

### `dpkg` source package: `twolame=0.4.0-2`

Binary Packages:

- `libtwolame0:amd64=0.4.0-2`

Licenses: (parsed from: `/usr/share/doc/libtwolame0/copyright`)

- `LGPL-2`
- `LGPL-2+`

Source:

```console
$ apt-get source -qq --print-uris twolame=0.4.0-2
'http://archive.ubuntu.com/ubuntu/pool/main/t/twolame/twolame_0.4.0-2.dsc' twolame_0.4.0-2.dsc 2048 SHA256:b5e662bbb076be28d1de5004ba6325b2df340eb7812d5071ba9d7f5d64faf7cc
'http://archive.ubuntu.com/ubuntu/pool/main/t/twolame/twolame_0.4.0.orig.tar.gz' twolame_0.4.0.orig.tar.gz 890908 SHA256:cc35424f6019a88c6f52570b63e1baf50f62963a3eac52a03a800bb070d7c87d
'http://archive.ubuntu.com/ubuntu/pool/main/t/twolame/twolame_0.4.0-2.debian.tar.xz' twolame_0.4.0-2.debian.tar.xz 4740 SHA256:c11737831db6c13b0346b5d23d39ab40235ad09373d57356dd8b23263499d67a
```

### `dpkg` source package: `tzdata=2021a-2ubuntu0.20.04`

Binary Packages:

- `tzdata=2021a-2ubuntu0.20.04`

Licenses: (parsed from: `/usr/share/doc/tzdata/copyright`)

- `ICU`

**WARNING:** unable to find source (`apt-get source` failed or returned no results)!  
This is *usually* due to a new package version being released and the old version being removed.


### `dpkg` source package: `ubuntu-keyring=2020.02.11.4`

Binary Packages:

- `ubuntu-keyring=2020.02.11.4`

Licenses: (parsed from: `/usr/share/doc/ubuntu-keyring/copyright`)

- `GPL`

Source:

```console
$ apt-get source -qq --print-uris ubuntu-keyring=2020.02.11.4
'http://archive.ubuntu.com/ubuntu/pool/main/u/ubuntu-keyring/ubuntu-keyring_2020.02.11.4.dsc' ubuntu-keyring_2020.02.11.4.dsc 1863 SHA512:1232fc109f9afe7f4245f841cb992aeb7329ec1c3d310a174b837c0584005a7c46ce73f6d49a52a3e6c0eea03369ea5f308093c1a849e8f6597f6df792a87fb1
'http://archive.ubuntu.com/ubuntu/pool/main/u/ubuntu-keyring/ubuntu-keyring_2020.02.11.4.tar.gz' ubuntu-keyring_2020.02.11.4.tar.gz 39250 SHA512:318562b6892dad995e334ec44f08f065b4c6abed2d29c1f96f6ee0fa4d91a5cedc9b62a152c56cdf26a30c3ea97a58c1d037e892d155af5593a4e26b9a25a1ae
```

### `dpkg` source package: `ubuntu-themes=19.04-0ubuntu3`

Binary Packages:

- `ubuntu-mono=19.04-0ubuntu3`

Licenses: (parsed from: `/usr/share/doc/ubuntu-mono/copyright`)

- `CC-BY-SA-3.0`
- `GPL-3`

Source:

```console
$ apt-get source -qq --print-uris ubuntu-themes=19.04-0ubuntu3
'http://archive.ubuntu.com/ubuntu/pool/main/u/ubuntu-themes/ubuntu-themes_19.04-0ubuntu3.dsc' ubuntu-themes_19.04-0ubuntu3.dsc 1601 SHA256:9a1c1911b35fba5beaa6f248e89fc37f32dd84b11ed27c25d94a3f0b34abfcad
'http://archive.ubuntu.com/ubuntu/pool/main/u/ubuntu-themes/ubuntu-themes_19.04.orig.tar.gz' ubuntu-themes_19.04.orig.tar.gz 16081764 SHA256:30be3e9fd04371ebb3d89f1731afad151f06f424ae122fa805483681636dc2b7
'http://archive.ubuntu.com/ubuntu/pool/main/u/ubuntu-themes/ubuntu-themes_19.04-0ubuntu3.diff.gz' ubuntu-themes_19.04-0ubuntu3.diff.gz 29445 SHA256:1e878b82db0d2d1e1269c6e071e0549ee7f69bbc7c8d61302b3a2984893ae9fa
```

### `dpkg` source package: `ucf=3.0038+nmu1`

Binary Packages:

- `ucf=3.0038+nmu1`

Licenses: (parsed from: `/usr/share/doc/ucf/copyright`)

- `GPL-2`

Source:

```console
$ apt-get source -qq --print-uris ucf=3.0038+nmu1
'http://archive.ubuntu.com/ubuntu/pool/main/u/ucf/ucf_3.0038+nmu1.dsc' ucf_3.0038+nmu1.dsc 1420 SHA256:89b6f921a30e04a946f62e6996be7c16f2f7c383d20783cd4704b502c6d5b125
'http://archive.ubuntu.com/ubuntu/pool/main/u/ucf/ucf_3.0038+nmu1.tar.xz' ucf_3.0038+nmu1.tar.xz 65860 SHA256:d00bc3dd8d2f91317f52b5352fe129023c72babad55bc0dd4ece7b34183c7436
```

### `dpkg` source package: `unzip=6.0-25ubuntu1`

Binary Packages:

- `unzip=6.0-25ubuntu1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris unzip=6.0-25ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/u/unzip/unzip_6.0-25ubuntu1.dsc' unzip_6.0-25ubuntu1.dsc 1833 SHA256:893fcd9e73c91c4df06716f9620adf1ec701a4846b6269c513785a125e55550c
'http://archive.ubuntu.com/ubuntu/pool/main/u/unzip/unzip_6.0.orig.tar.gz' unzip_6.0.orig.tar.gz 1376845 SHA256:036d96991646d0449ed0aa952e4fbe21b476ce994abc276e49d30e686708bd37
'http://archive.ubuntu.com/ubuntu/pool/main/u/unzip/unzip_6.0-25ubuntu1.debian.tar.xz' unzip_6.0-25ubuntu1.debian.tar.xz 26276 SHA256:6a22b0d23cf8b9e1a74626d7d9af5efe1257e157f20006272dc68693a13f3b45
```

### `dpkg` source package: `util-linux=2.34-0.1ubuntu9.1`

Binary Packages:

- `bsdutils=1:2.34-0.1ubuntu9.1`
- `fdisk=2.34-0.1ubuntu9.1`
- `libblkid1:amd64=2.34-0.1ubuntu9.1`
- `libfdisk1:amd64=2.34-0.1ubuntu9.1`
- `libmount1:amd64=2.34-0.1ubuntu9.1`
- `libsmartcols1:amd64=2.34-0.1ubuntu9.1`
- `libuuid1:amd64=2.34-0.1ubuntu9.1`
- `mount=2.34-0.1ubuntu9.1`
- `util-linux=2.34-0.1ubuntu9.1`

Licenses: (parsed from: `/usr/share/doc/bsdutils/copyright`, `/usr/share/doc/fdisk/copyright`, `/usr/share/doc/libblkid1/copyright`, `/usr/share/doc/libfdisk1/copyright`, `/usr/share/doc/libmount1/copyright`, `/usr/share/doc/libsmartcols1/copyright`, `/usr/share/doc/libuuid1/copyright`, `/usr/share/doc/mount/copyright`, `/usr/share/doc/util-linux/copyright`)

- `BSD-2-clause`
- `BSD-3-clause`
- `BSD-4-clause`
- `GPL-2`
- `GPL-2+`
- `GPL-3`
- `GPL-3+`
- `LGPL`
- `LGPL-2`
- `LGPL-2+`
- `LGPL-2.1`
- `LGPL-2.1+`
- `LGPL-3`
- `LGPL-3+`
- `MIT`
- `public-domain`

Source:

```console
$ apt-get source -qq --print-uris util-linux=2.34-0.1ubuntu9.1
'http://archive.ubuntu.com/ubuntu/pool/main/u/util-linux/util-linux_2.34-0.1ubuntu9.1.dsc' util-linux_2.34-0.1ubuntu9.1.dsc 4067 SHA512:cd98079e1a347852d84c0ebbfac643e7d7d66d7524a060a8bbb8df45153728dae957793b0cefacd4744fbbe4396efcd45e32beeaf8ab3afe347124373554ddbe
'http://archive.ubuntu.com/ubuntu/pool/main/u/util-linux/util-linux_2.34.orig.tar.xz' util-linux_2.34.orig.tar.xz 4974812 SHA512:2d0b76f63d32e7afb7acf61a83fabbfd58baa34ab78b3a331ce87f9c676a5fd71c56a493ded95039540d2c46b6048caaa38d7fb4491eb3d52d7b09dc54655cd7
'http://archive.ubuntu.com/ubuntu/pool/main/u/util-linux/util-linux_2.34-0.1ubuntu9.1.debian.tar.xz' util-linux_2.34-0.1ubuntu9.1.debian.tar.xz 91620 SHA512:b0ed129f1e7febe6a7370ef2becaca376b7bea478af084769f61c0b78cf4d3f5a0b5d8fe7c75017e74a8c4c6d22a37a2a9d7221c60822677220fa719d7f3b609
```

### `dpkg` source package: `vim=2:8.1.2269-1ubuntu5.3`

Binary Packages:

- `vim=2:8.1.2269-1ubuntu5.3`
- `vim-common=2:8.1.2269-1ubuntu5.3`
- `vim-runtime=2:8.1.2269-1ubuntu5.3`
- `xxd=2:8.1.2269-1ubuntu5.3`

Licenses: (parsed from: `/usr/share/doc/vim/copyright`, `/usr/share/doc/vim-common/copyright`, `/usr/share/doc/vim-runtime/copyright`, `/usr/share/doc/xxd/copyright`)

- `Apache`
- `Apache-2.0`
- `Artistic`
- `Artistic-1`
- `BSD-2-clause`
- `BSD-3-clause`
- `Compaq`
- `EDL-1`
- `Expat`
- `GPL-1`
- `GPL-1+`
- `GPL-2`
- `GPL-2+`
- `LGPL-2.1`
- `LGPL-2.1+`
- `OPL-1+`
- `SRA`
- `UC`
- `Vim`
- `Vim-Regexp`
- `X11`
- `XPM`
- `public-domain`

Source:

```console
$ apt-get source -qq --print-uris vim=2:8.1.2269-1ubuntu5.3
'http://archive.ubuntu.com/ubuntu/pool/main/v/vim/vim_8.1.2269-1ubuntu5.3.dsc' vim_8.1.2269-1ubuntu5.3.dsc 2940 SHA512:460587dc3a93468419b410d55e6079a753a3673d6ff3a0df6e1411e32201ed3e6f8cbf07272b4b39c83e25103deb30d3b1075266e4963e09ea7a17ea2e29c502
'http://archive.ubuntu.com/ubuntu/pool/main/v/vim/vim_8.1.2269.orig.tar.gz' vim_8.1.2269.orig.tar.gz 14590233 SHA512:5194817a5665e08816e87e3b10219e19510ca87c5c377ddc0a50aa84ffd4d7391badad3e993f83802d6da5e9da8834635c1acbdd4a909e75b4947f99f13c1746
'http://archive.ubuntu.com/ubuntu/pool/main/v/vim/vim_8.1.2269-1ubuntu5.3.debian.tar.xz' vim_8.1.2269-1ubuntu5.3.debian.tar.xz 210348 SHA512:e32016a601572ca2c2c7d16c1dfa1635df92cf8a2c5aaeef040aefa293eb33845de4b61a1fc67b6ecf9ce9b2e809ac735d41e4f2ffb8646ddd3fc56e1900ba70
```

### `dpkg` source package: `vulkan-loader=1.2.131.2-1`

Binary Packages:

- `libvulkan1:amd64=1.2.131.2-1`

Licenses: (parsed from: `/usr/share/doc/libvulkan1/copyright`)

- `Apache-2.0`
- `MIT`

Source:

```console
$ apt-get source -qq --print-uris vulkan-loader=1.2.131.2-1
'http://archive.ubuntu.com/ubuntu/pool/main/v/vulkan-loader/vulkan-loader_1.2.131.2-1.dsc' vulkan-loader_1.2.131.2-1.dsc 2181 SHA256:fce11e5d9ce1df38cfca46ac461b22f1c97a9429b51732b4a7957b810a91f765
'http://archive.ubuntu.com/ubuntu/pool/main/v/vulkan-loader/vulkan-loader_1.2.131.2.orig.tar.xz' vulkan-loader_1.2.131.2.orig.tar.xz 1733244 SHA256:942893eeb963a49cec5aad17fcbdee685f18b5a7ab50544e665c77607fd7d7e7
'http://archive.ubuntu.com/ubuntu/pool/main/v/vulkan-loader/vulkan-loader_1.2.131.2-1.debian.tar.xz' vulkan-loader_1.2.131.2-1.debian.tar.xz 5888 SHA256:e95414f98551a1ae58707bfcea7bf11c30009d3d58649dd32a556c02e759d221
```

### `dpkg` source package: `wavpack=5.2.0-1ubuntu0.1`

Binary Packages:

- `libwavpack1:amd64=5.2.0-1ubuntu0.1`

Licenses: (parsed from: `/usr/share/doc/libwavpack1/copyright`)

- `BSD-2-clause`
- `BSD-3-clause`
- `public-domain`

Source:

```console
$ apt-get source -qq --print-uris wavpack=5.2.0-1ubuntu0.1
'http://archive.ubuntu.com/ubuntu/pool/main/w/wavpack/wavpack_5.2.0-1ubuntu0.1.dsc' wavpack_5.2.0-1ubuntu0.1.dsc 2174 SHA512:006e5de760ccd12e29423b9a3cd5d3b1e4b0020e9b9a21acd5daa67f38f00363e5408d0c1cdf2b4dcef76090175d7bfb622eb4de979b9a95301b3fa51179d81f
'http://archive.ubuntu.com/ubuntu/pool/main/w/wavpack/wavpack_5.2.0.orig.tar.bz2' wavpack_5.2.0.orig.tar.bz2 849348 SHA512:456da78fb5d01b33a8ed71b43cb6809a25ca0d54e53858b93bbb3eb26923bfa6de4c6a3c01caca947c0852aea74d1b14667205dae344148a01619e67eb2c7e71
'http://archive.ubuntu.com/ubuntu/pool/main/w/wavpack/wavpack_5.2.0-1ubuntu0.1.debian.tar.xz' wavpack_5.2.0-1ubuntu0.1.debian.tar.xz 7076 SHA512:9e0ae292c78f9697eac6217f613c7f0cf89ed7b6305683f0be0573e42c35e5582686bd35e2637d73a07ead3c68473cfa0082ef1ef1a905bdfaebf808c83a6280
```

### `dpkg` source package: `wayland=1.18.0-1`

Binary Packages:

- `libwayland-client0:amd64=1.18.0-1`
- `libwayland-cursor0:amd64=1.18.0-1`
- `libwayland-egl1:amd64=1.18.0-1`
- `libwayland-server0:amd64=1.18.0-1`

Licenses: (parsed from: `/usr/share/doc/libwayland-client0/copyright`, `/usr/share/doc/libwayland-cursor0/copyright`, `/usr/share/doc/libwayland-egl1/copyright`, `/usr/share/doc/libwayland-server0/copyright`)

- `X11`

Source:

```console
$ apt-get source -qq --print-uris wayland=1.18.0-1
'http://archive.ubuntu.com/ubuntu/pool/main/w/wayland/wayland_1.18.0-1.dsc' wayland_1.18.0-1.dsc 2565 SHA256:c3a7625f8ed02443815aa607e8875f4c01f41f4037b48d1cc97689500c7083e7
'http://archive.ubuntu.com/ubuntu/pool/main/w/wayland/wayland_1.18.0.orig.tar.gz' wayland_1.18.0.orig.tar.gz 736301 SHA256:102d5556db1524a7737ab76e358b03e5b92a7718f1ac1520bf9686bfd09c8e4a
'http://archive.ubuntu.com/ubuntu/pool/main/w/wayland/wayland_1.18.0-1.diff.gz' wayland_1.18.0-1.diff.gz 16180 SHA256:3c93bd642992a55340bcf24d4f20f9e77a0bf4ab188fb76f78117e704e3a3fba
```

### `dpkg` source package: `websocket-api=1.1-1`

Binary Packages:

- `libwebsocket-api-java=1.1-1`

Licenses: (parsed from: `/usr/share/doc/libwebsocket-api-java/copyright`)

- `Apache-2.0`
- `CDDL-1.1`
- `GPL-2`
- `GPL-2 with Classpath exception`

Source:

```console
$ apt-get source -qq --print-uris websocket-api=1.1-1
'http://archive.ubuntu.com/ubuntu/pool/universe/w/websocket-api/websocket-api_1.1-1.dsc' websocket-api_1.1-1.dsc 2026 SHA256:f39064f78befb06483c8134a989dc83dc986cf2baef77eae1cc85b12e83d8096
'http://archive.ubuntu.com/ubuntu/pool/universe/w/websocket-api/websocket-api_1.1.orig.tar.xz' websocket-api_1.1.orig.tar.xz 28884 SHA256:53c0c1eff9d4bda5abb28ac47f874407c019e546e40c061541b4b4a096e9fa7b
'http://archive.ubuntu.com/ubuntu/pool/universe/w/websocket-api/websocket-api_1.1-1.debian.tar.xz' websocket-api_1.1-1.debian.tar.xz 8436 SHA256:c6d79e97ed8f2d6836dcbd2bc30777a12d3ab4f5bdd95cc4a37fb6d77ec35653
```

### `dpkg` source package: `wget=1.20.3-1ubuntu1`

Binary Packages:

- `wget=1.20.3-1ubuntu1`

Licenses: (parsed from: `/usr/share/doc/wget/copyright`)

- `GFDL-1.2`
- `GPL-3`

Source:

```console
$ apt-get source -qq --print-uris wget=1.20.3-1ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/w/wget/wget_1.20.3-1ubuntu1.dsc' wget_1.20.3-1ubuntu1.dsc 2280 SHA256:e40e00e9f93f1c049cd2b7942c9c9e31504acd56350b31f1ecad6ae220a44dfd
'http://archive.ubuntu.com/ubuntu/pool/main/w/wget/wget_1.20.3.orig.tar.gz' wget_1.20.3.orig.tar.gz 4489249 SHA256:31cccfc6630528db1c8e3a06f6decf2a370060b982841cfab2b8677400a5092e
'http://archive.ubuntu.com/ubuntu/pool/main/w/wget/wget_1.20.3.orig.tar.gz.asc' wget_1.20.3.orig.tar.gz.asc 833 SHA256:7b295c84ab6f90c328a203e234e4b2f5f45cb8d2e29eac43a977073933cd49a2
'http://archive.ubuntu.com/ubuntu/pool/main/w/wget/wget_1.20.3-1ubuntu1.debian.tar.xz' wget_1.20.3-1ubuntu1.debian.tar.xz 63312 SHA256:95ae56081866b9e6dfb2a2d2dc2208ba0cf3c76bb5d7e680cc56b18b3ec66c1e
```

### `dpkg` source package: `x11-utils=7.7+5`

Binary Packages:

- `x11-utils=7.7+5`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris x11-utils=7.7+5
'http://archive.ubuntu.com/ubuntu/pool/main/x/x11-utils/x11-utils_7.7+5.dsc' x11-utils_7.7+5.dsc 2136 SHA256:080991680b95466c8f124356d443483ab7c44c383f3cc029f3deb66ddccfbe72
'http://archive.ubuntu.com/ubuntu/pool/main/x/x11-utils/x11-utils_7.7+5.tar.gz' x11-utils_7.7+5.tar.gz 3056692 SHA256:10801f586876ee483510b4a2c5c837ddd0858475860f9a72600f38cb3553722b
```

### `dpkg` source package: `x11-xserver-utils=7.7+8`

Binary Packages:

- `x11-xserver-utils=7.7+8`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris x11-xserver-utils=7.7+8
'http://archive.ubuntu.com/ubuntu/pool/main/x/x11-xserver-utils/x11-xserver-utils_7.7+8.dsc' x11-xserver-utils_7.7+8.dsc 1949 SHA256:e9f3fe561444bac31e40fe9405ab64a1d10e3cca3d0a7dfed870cb8921a8a95d
'http://archive.ubuntu.com/ubuntu/pool/main/x/x11-xserver-utils/x11-xserver-utils_7.7+8.tar.gz' x11-xserver-utils_7.7+8.tar.gz 2645046 SHA256:2f571846006d7671777dbcac2e89e48f780f97506b75b7c871744637125a29d8
```

### `dpkg` source package: `x264=2:0.155.2917+git0a84d98-2`

Binary Packages:

- `libx264-155:amd64=2:0.155.2917+git0a84d98-2`

Licenses: (parsed from: `/usr/share/doc/libx264-155/copyright`)

- `BSD-3-clause`
- `Expat`
- `GPL-2`
- `GPL-2+`
- `GPL-2+ with other exception`
- `ISC`
- `LGPL-2.1+`
- `public-domain`

Source:

```console
$ apt-get source -qq --print-uris x264=2:0.155.2917+git0a84d98-2
'http://archive.ubuntu.com/ubuntu/pool/universe/x/x264/x264_0.155.2917+git0a84d98-2.dsc' x264_0.155.2917+git0a84d98-2.dsc 2407 SHA256:b296d3069efcbbf6a7a9c3a6bfd1ec99fa559ece8c5959158859e47e0092a393
'http://archive.ubuntu.com/ubuntu/pool/universe/x/x264/x264_0.155.2917+git0a84d98.orig.tar.gz' x264_0.155.2917+git0a84d98.orig.tar.gz 934501 SHA256:814e8d233a7a98a66b4c592bec60c531369bac453d679ba6c006bdcd2677e7e8
'http://archive.ubuntu.com/ubuntu/pool/universe/x/x264/x264_0.155.2917+git0a84d98-2.debian.tar.xz' x264_0.155.2917+git0a84d98-2.debian.tar.xz 23260 SHA256:9058a14889abcb6e28e1219ba3b5a78c00125f91877a1ecf3ac7d3aa352b19c3
```

### `dpkg` source package: `x265=3.2.1-1build1`

Binary Packages:

- `libx265-179:amd64=3.2.1-1build1`

Licenses: (parsed from: `/usr/share/doc/libx265-179/copyright`)

- `Expat`
- `GPL-2`
- `GPL-2+`
- `ISC`
- `LGPL-2.1`
- `LGPL-2.1+`

Source:

```console
$ apt-get source -qq --print-uris x265=3.2.1-1build1
'http://archive.ubuntu.com/ubuntu/pool/universe/x/x265/x265_3.2.1-1build1.dsc' x265_3.2.1-1build1.dsc 2292 SHA256:4dc61864d62248c3f5d9d404ad5127434310b31f38c1cac9432150363a9e8d9f
'http://archive.ubuntu.com/ubuntu/pool/universe/x/x265/x265_3.2.1.orig.tar.gz' x265_3.2.1.orig.tar.gz 1426255 SHA256:fb9badcf92364fd3567f8b5aa0e5e952aeea7a39a2b864387cec31e3b58cbbcc
'http://archive.ubuntu.com/ubuntu/pool/universe/x/x265/x265_3.2.1-1build1.debian.tar.xz' x265_3.2.1-1build1.debian.tar.xz 13172 SHA256:cffcaac1a8202c9021e3999dc617d115fc5d4a8fb7810a3aff6d9e122dea2942
```

### `dpkg` source package: `xdg-user-dirs=0.17-2ubuntu1`

Binary Packages:

- `xdg-user-dirs=0.17-2ubuntu1`

Licenses: (parsed from: `/usr/share/doc/xdg-user-dirs/copyright`)

- `GPL-2`

Source:

```console
$ apt-get source -qq --print-uris xdg-user-dirs=0.17-2ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/x/xdg-user-dirs/xdg-user-dirs_0.17-2ubuntu1.dsc' xdg-user-dirs_0.17-2ubuntu1.dsc 1671 SHA256:47a6d5715554995558197b0a79735a3335d9ff248cef3db1e34cb8be286ffa23
'http://archive.ubuntu.com/ubuntu/pool/main/x/xdg-user-dirs/xdg-user-dirs_0.17.orig.tar.gz' xdg-user-dirs_0.17.orig.tar.gz 257291 SHA256:2a07052823788e8614925c5a19ef5b968d8db734fdee656699ea4f97d132418c
'http://archive.ubuntu.com/ubuntu/pool/main/x/xdg-user-dirs/xdg-user-dirs_0.17-2ubuntu1.debian.tar.xz' xdg-user-dirs_0.17-2ubuntu1.debian.tar.xz 28704 SHA256:4883d7162a09f35c2640c25103c3a9914b916f13170ee63f873213823d6550fc
```

### `dpkg` source package: `xdg-utils=1.1.3-2ubuntu1.20.04.2`

Binary Packages:

- `xdg-utils=1.1.3-2ubuntu1.20.04.2`

Licenses: (parsed from: `/usr/share/doc/xdg-utils/copyright`)

- `Expat`

Source:

```console
$ apt-get source -qq --print-uris xdg-utils=1.1.3-2ubuntu1.20.04.2
'http://archive.ubuntu.com/ubuntu/pool/main/x/xdg-utils/xdg-utils_1.1.3-2ubuntu1.20.04.2.dsc' xdg-utils_1.1.3-2ubuntu1.20.04.2.dsc 2207 SHA512:fffbe67c440328294b789c895cd54185e456ad26d0f7a953993f27413ed9b5028e13ac2e37981521da66391bf16b51e62c8f1a3c089d6b5502abdafa5d74a865
'http://archive.ubuntu.com/ubuntu/pool/main/x/xdg-utils/xdg-utils_1.1.3.orig.tar.gz' xdg-utils_1.1.3.orig.tar.gz 297170 SHA512:d1f819a211eb4104a90dfdc6fedcb640fd46b15ccfc8762266f8f538c49d74cb00027b8c1af991fb2a200acb4379986ae375700e06a2aa08fb41a38f883acb3e
'http://archive.ubuntu.com/ubuntu/pool/main/x/xdg-utils/xdg-utils_1.1.3-2ubuntu1.20.04.2.debian.tar.xz' xdg-utils_1.1.3-2ubuntu1.20.04.2.debian.tar.xz 12048 SHA512:1484b5a938bf740058af70d9f697ab92d1d86359f9e6cac707046a0438df6753ea493e92a59ccba4a0ceebd3f375055b5520cef4d8ec1cde30d73ed44db56b26
```

### `dpkg` source package: `xft=2.3.3-0ubuntu1`

Binary Packages:

- `libxft2:amd64=2.3.3-0ubuntu1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris xft=2.3.3-0ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/x/xft/xft_2.3.3-0ubuntu1.dsc' xft_2.3.3-0ubuntu1.dsc 1492 SHA256:5919894493ea8ebf6ec9775ed576beaf012a3094f7b77782515ccd8a12bce9f2
'http://archive.ubuntu.com/ubuntu/pool/main/x/xft/xft_2.3.3.orig.tar.gz' xft_2.3.3.orig.tar.gz 425784 SHA256:3c3cf88b1a96e49a3d87d67d9452d34b6e25e96ae83959b8d0a980935014d701
'http://archive.ubuntu.com/ubuntu/pool/main/x/xft/xft_2.3.3-0ubuntu1.diff.gz' xft_2.3.3-0ubuntu1.diff.gz 10421 SHA256:4b081e5a4e86189fe74c35873793b01a623bd198481411a17e42c454f9d5d2eb
```

### `dpkg` source package: `xkeyboard-config=2.29-2`

Binary Packages:

- `xkb-data=2.29-2`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris xkeyboard-config=2.29-2
'http://archive.ubuntu.com/ubuntu/pool/main/x/xkeyboard-config/xkeyboard-config_2.29-2.dsc' xkeyboard-config_2.29-2.dsc 2367 SHA256:dca40afb9a196907a2f1c80b147d574329f126f01b8834f7886fbe8448162971
'http://archive.ubuntu.com/ubuntu/pool/main/x/xkeyboard-config/xkeyboard-config_2.29.orig.tar.gz' xkeyboard-config_2.29.orig.tar.gz 2701654 SHA256:d8034c8b771b51140409039f8e3351e90a0092238b81af04239794e8d3dc0813
'http://archive.ubuntu.com/ubuntu/pool/main/x/xkeyboard-config/xkeyboard-config_2.29.orig.tar.gz.asc' xkeyboard-config_2.29.orig.tar.gz.asc 488 SHA256:30b9b0fd406caf048a74627f29dca01868b87a6c6d722003a6c0a8834165c08d
'http://archive.ubuntu.com/ubuntu/pool/main/x/xkeyboard-config/xkeyboard-config_2.29-2.diff.gz' xkeyboard-config_2.29-2.diff.gz 987137 SHA256:8dc241399c899e7c4d2ae41e442ec310b45e70b7935aaffec66c2d1d0c065bf4
```

### `dpkg` source package: `xmlsec1=1.2.28-2`

Binary Packages:

- `libxmlsec1:amd64=1.2.28-2`
- `libxmlsec1-nss:amd64=1.2.28-2`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris xmlsec1=1.2.28-2
'http://archive.ubuntu.com/ubuntu/pool/main/x/xmlsec1/xmlsec1_1.2.28-2.dsc' xmlsec1_1.2.28-2.dsc 2616 SHA256:2dc2ce5fec43ae103263f8d11ec05e57499b4d0a96d5d5de660b57925b5a86d7
'http://archive.ubuntu.com/ubuntu/pool/main/x/xmlsec1/xmlsec1_1.2.28.orig.tar.gz' xmlsec1_1.2.28.orig.tar.gz 2014642 SHA256:162125da1425f9ab786bab6c69e04679828cb8566c7566a51d32a4ce993669c4
'http://archive.ubuntu.com/ubuntu/pool/main/x/xmlsec1/xmlsec1_1.2.28-2.debian.tar.xz' xmlsec1_1.2.28-2.debian.tar.xz 8732 SHA256:854821664d3e19a6fb15f85394635271aa245a395bccdc1dca8eb9e63b8ecd68
```

### `dpkg` source package: `xorg-sgml-doctools=1:1.11-1`

Binary Packages:

- `xorg-sgml-doctools=1:1.11-1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris xorg-sgml-doctools=1:1.11-1
'http://archive.ubuntu.com/ubuntu/pool/main/x/xorg-sgml-doctools/xorg-sgml-doctools_1.11-1.dsc' xorg-sgml-doctools_1.11-1.dsc 1975 SHA256:1f4a12a38420b0ddab35553b9588fdf43ab39577958aed70fca435c9a747141a
'http://archive.ubuntu.com/ubuntu/pool/main/x/xorg-sgml-doctools/xorg-sgml-doctools_1.11.orig.tar.gz' xorg-sgml-doctools_1.11.orig.tar.gz 150367 SHA256:986326d7b4dd2ad298f61d8d41fe3929ac6191c6000d6d7e47a8ffc0c34e7426
'http://archive.ubuntu.com/ubuntu/pool/main/x/xorg-sgml-doctools/xorg-sgml-doctools_1.11-1.diff.gz' xorg-sgml-doctools_1.11-1.diff.gz 3194 SHA256:18eeb355cb0efff9f47f8ed8e852eee322d9733a427419f4b39f43bc4df630c1
```

### `dpkg` source package: `xorg=1:7.7+19ubuntu14`

Binary Packages:

- `x11-common=1:7.7+19ubuntu14`

Licenses: (parsed from: `/usr/share/doc/x11-common/copyright`)

- `GPL`

Source:

```console
$ apt-get source -qq --print-uris xorg=1:7.7+19ubuntu14
'http://archive.ubuntu.com/ubuntu/pool/main/x/xorg/xorg_7.7+19ubuntu14.dsc' xorg_7.7+19ubuntu14.dsc 2107 SHA256:d9d6449510066c3b34216cf08f797f00f64df3494567b5478a60d0feb50b9d95
'http://archive.ubuntu.com/ubuntu/pool/main/x/xorg/xorg_7.7+19ubuntu14.tar.gz' xorg_7.7+19ubuntu14.tar.gz 299269 SHA256:b8a1c0f7b24ae5565f6f22ccf01cd0c8e46c4f5dad6c14bce4f3495e82138213
```

### `dpkg` source package: `xorgproto=2019.2-1ubuntu1`

Binary Packages:

- `x11proto-core-dev=2019.2-1ubuntu1`
- `x11proto-dev=2019.2-1ubuntu1`

Licenses: (parsed from: `/usr/share/doc/x11proto-core-dev/copyright`, `/usr/share/doc/x11proto-dev/copyright`)

- `MIT`
- `SGI`

Source:

```console
$ apt-get source -qq --print-uris xorgproto=2019.2-1ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/x/xorgproto/xorgproto_2019.2-1ubuntu1.dsc' xorgproto_2019.2-1ubuntu1.dsc 4096 SHA256:1b0fede1501745c7cfed22b86ea951ba6792ae6eda404fafae9533b01fbb2ee2
'http://archive.ubuntu.com/ubuntu/pool/main/x/xorgproto/xorgproto_2019.2.orig.tar.gz' xorgproto_2019.2.orig.tar.gz 1080686 SHA256:ebfcfce48b66bec25d5dff0e9510e04053ef78e51a8eabeeee4c00e399226d61
'http://archive.ubuntu.com/ubuntu/pool/main/x/xorgproto/xorgproto_2019.2.orig.tar.gz.asc' xorgproto_2019.2.orig.tar.gz.asc 659 SHA256:75da45caac1d85fe37a5e7f33a087d456cad1dc38f2743b7f7df63d7ca583293
'http://archive.ubuntu.com/ubuntu/pool/main/x/xorgproto/xorgproto_2019.2-1ubuntu1.diff.gz' xorgproto_2019.2-1ubuntu1.diff.gz 21111 SHA256:9162224ecb85b35b37a51fbb2a1c53fc8262339fd3208ded60e141607aa835e8
```

### `dpkg` source package: `xtrans=1.4.0-1`

Binary Packages:

- `xtrans-dev=1.4.0-1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris xtrans=1.4.0-1
'http://archive.ubuntu.com/ubuntu/pool/main/x/xtrans/xtrans_1.4.0-1.dsc' xtrans_1.4.0-1.dsc 1919 SHA256:dd74ab9199e8f45215b566a9317cac7953bf063ce6893c185eccaf0fb4d84d8f
'http://archive.ubuntu.com/ubuntu/pool/main/x/xtrans/xtrans_1.4.0.orig.tar.gz' xtrans_1.4.0.orig.tar.gz 225941 SHA256:48ed850ce772fef1b44ca23639b0a57e38884045ed2cbb18ab137ef33ec713f9
'http://archive.ubuntu.com/ubuntu/pool/main/x/xtrans/xtrans_1.4.0-1.diff.gz' xtrans_1.4.0-1.diff.gz 9522 SHA256:0dac18165654d79e0796b80fab4c1104998d29e6d0b098af0426a1d72399521e
```

### `dpkg` source package: `xvidcore=2:1.3.7-1`

Binary Packages:

- `libxvidcore4:amd64=2:1.3.7-1`

Licenses: (parsed from: `/usr/share/doc/libxvidcore4/copyright`)

- `GPL-2`
- `GPL-2+`
- `LGPL-2`
- `LGPL-2+`

Source:

```console
$ apt-get source -qq --print-uris xvidcore=2:1.3.7-1
'http://archive.ubuntu.com/ubuntu/pool/universe/x/xvidcore/xvidcore_1.3.7-1.dsc' xvidcore_1.3.7-1.dsc 2129 SHA256:163919a59468677ed948011371e93b7183d83d3b0e77bc84ac02fe27b1510548
'http://archive.ubuntu.com/ubuntu/pool/universe/x/xvidcore/xvidcore_1.3.7.orig.tar.bz2' xvidcore_1.3.7.orig.tar.bz2 698615 SHA256:aeeaae952d4db395249839a3bd03841d6844843f5a4f84c271ff88f7aa1acff7
'http://archive.ubuntu.com/ubuntu/pool/universe/x/xvidcore/xvidcore_1.3.7-1.debian.tar.xz' xvidcore_1.3.7-1.debian.tar.xz 6464 SHA256:0293cfb7f9c9036ffc40647ae81e98f12c880372eafbe58e22810032f9227338
```

### `dpkg` source package: `xz-utils=5.2.4-1ubuntu1`

Binary Packages:

- `liblzma5:amd64=5.2.4-1ubuntu1`
- `xz-utils=5.2.4-1ubuntu1`

Licenses: (parsed from: `/usr/share/doc/liblzma5/copyright`, `/usr/share/doc/xz-utils/copyright`)

- `Autoconf`
- `GPL-2`
- `GPL-2+`
- `GPL-3`
- `LGPL-2`
- `LGPL-2.1`
- `LGPL-2.1+`
- `PD`
- `PD-debian`
- `config-h`
- `noderivs`
- `none`
- `permissive-fsf`
- `permissive-nowarranty`
- `probably-PD`

Source:

```console
$ apt-get source -qq --print-uris xz-utils=5.2.4-1ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/main/x/xz-utils/xz-utils_5.2.4-1ubuntu1.dsc' xz-utils_5.2.4-1ubuntu1.dsc 2629 SHA512:09c0668c76bd1653460ae2207f2666785d6ec7bae424d168e2f5dc2c98d2c34b7f983963be27c39ac05df0ad76ccfe088b55a64a09319f26b785544d5c8ffb66
'http://archive.ubuntu.com/ubuntu/pool/main/x/xz-utils/xz-utils_5.2.4.orig.tar.xz' xz-utils_5.2.4.orig.tar.xz 1053868 SHA512:00db7dd31a61541b1ce6946e0f21106f418dd1ac3f27cdb8682979cbc3bd777cd6dd1f04f9ba257a0a7e24041e15ca40d0dd5c130380dce62280af67a0beb97f
'http://archive.ubuntu.com/ubuntu/pool/main/x/xz-utils/xz-utils_5.2.4.orig.tar.xz.asc' xz-utils_5.2.4.orig.tar.xz.asc 879 SHA512:dbfce0556bc85545ce3566a01c25e4876f560409fc2d48f2dc382b10fbd2538c61d8f2c3667d86fc7313aec86c05e53926015000320f19615e97875adae42450
'http://archive.ubuntu.com/ubuntu/pool/main/x/xz-utils/xz-utils_5.2.4-1ubuntu1.debian.tar.xz' xz-utils_5.2.4-1ubuntu1.debian.tar.xz 135512 SHA512:9ec339da084b6aedd5d9dfafe879f7b90ae6dc473458dd8eda234e087f3aa80480b7b0792b54588d57e1b41a2c42f28ef87b8e6a8cd4bb51d43e2517f701724f
```

### `dpkg` source package: `yajl=2.1.0-3`

Binary Packages:

- `libyajl2:amd64=2.1.0-3`

Licenses: (parsed from: `/usr/share/doc/libyajl2/copyright`)

- `ISC`

Source:

```console
$ apt-get source -qq --print-uris yajl=2.1.0-3
'http://archive.ubuntu.com/ubuntu/pool/main/y/yajl/yajl_2.1.0-3.dsc' yajl_2.1.0-3.dsc 1934 SHA256:bb35b92eda156bf114902e231859f241b67207d7b978878f6a595a995e5cf29d
'http://archive.ubuntu.com/ubuntu/pool/main/y/yajl/yajl_2.1.0.orig.tar.gz' yajl_2.1.0.orig.tar.gz 83997 SHA256:3fb73364a5a30efe615046d07e6db9d09fd2b41c763c5f7d3bfb121cd5c5ac5a
'http://archive.ubuntu.com/ubuntu/pool/main/y/yajl/yajl_2.1.0-3.debian.tar.xz' yajl_2.1.0-3.debian.tar.xz 5616 SHA256:b8056025a0d41af27127bc0993ffbff2ff3c09285494f4498f8ad769443a7463
```

### `dpkg` source package: `zeromq3=4.3.2-2ubuntu1`

Binary Packages:

- `libzmq5:amd64=4.3.2-2ubuntu1`

Licenses: (parsed from: `/usr/share/doc/libzmq5/copyright`)

- `LGPL-2`
- `LGPL-2.0+`
- `LGPL-3`
- `LGPL-3.0+`
- `MIT`

Source:

```console
$ apt-get source -qq --print-uris zeromq3=4.3.2-2ubuntu1
'http://archive.ubuntu.com/ubuntu/pool/universe/z/zeromq3/zeromq3_4.3.2-2ubuntu1.dsc' zeromq3_4.3.2-2ubuntu1.dsc 1968 SHA256:72f40e07d61a6733aefa8ad58f12ad9382bd355c7b3c58054a20595eac2f15c5
'http://archive.ubuntu.com/ubuntu/pool/universe/z/zeromq3/zeromq3_4.3.2.orig.tar.gz' zeromq3_4.3.2.orig.tar.gz 836655 SHA256:02ecc88466ae38cf2c8d79f09cfd2675ba299a439680b64ade733e26a349edeb
'http://archive.ubuntu.com/ubuntu/pool/universe/z/zeromq3/zeromq3_4.3.2-2ubuntu1.debian.tar.xz' zeromq3_4.3.2-2ubuntu1.debian.tar.xz 22828 SHA256:70cd7c1c1a83fd60405f30317c3416992fe1dfd39b26e2cfc61372cbf70f267c
```

### `dpkg` source package: `zip=3.0-11build1`

Binary Packages:

- `zip=3.0-11build1`

**WARNING:** unable to detect licenses! (package likely not compliant with DEP-5)  
If source is available (seen below), check the contents of `debian/copyright` within it.


Source:

```console
$ apt-get source -qq --print-uris zip=3.0-11build1
'http://archive.ubuntu.com/ubuntu/pool/main/z/zip/zip_3.0-11build1.dsc' zip_3.0-11build1.dsc 1658 SHA256:47bc14d9970340a3469117770adca913c1c1803547f847366f37562d78979904
'http://archive.ubuntu.com/ubuntu/pool/main/z/zip/zip_3.0.orig.tar.gz' zip_3.0.orig.tar.gz 1118845 SHA256:f0e8bb1f9b7eb0b01285495a2699df3a4b766784c1765a8f1aeedf63c0806369
'http://archive.ubuntu.com/ubuntu/pool/main/z/zip/zip_3.0-11build1.debian.tar.xz' zip_3.0-11build1.debian.tar.xz 8308 SHA256:3011af4bcde82439198f97af23220e6ba4837de9aaa68811688bd48a990c7981
```

### `dpkg` source package: `zlib=1:1.2.11.dfsg-2ubuntu1.2`

Binary Packages:

- `zlib1g:amd64=1:1.2.11.dfsg-2ubuntu1.2`

Licenses: (parsed from: `/usr/share/doc/zlib1g/copyright`)

- `Zlib`

Source:

```console
$ apt-get source -qq --print-uris zlib=1:1.2.11.dfsg-2ubuntu1.2
'http://archive.ubuntu.com/ubuntu/pool/main/z/zlib/zlib_1.2.11.dfsg-2ubuntu1.2.dsc' zlib_1.2.11.dfsg-2ubuntu1.2.dsc 2953 SHA512:aa6c4c9bebb54f713912130e9484272027d530e6c0294444474b729828c5a610bc37bbdd6a6193753c5c52b20986956003c62b3daf426ee0fe52955ace35ae9f
'http://archive.ubuntu.com/ubuntu/pool/main/z/zlib/zlib_1.2.11.dfsg.orig.tar.gz' zlib_1.2.11.dfsg.orig.tar.gz 370248 SHA512:92819807c0b8de655021bb2d5d182f9b6b381d3072d8c8dc1df34bbaa25d36bcba140c85f754a43cc466aac65850b7a7366aa0c93e804180e5b255e61d5748de
'http://archive.ubuntu.com/ubuntu/pool/main/z/zlib/zlib_1.2.11.dfsg-2ubuntu1.2.debian.tar.xz' zlib_1.2.11.dfsg-2ubuntu1.2.debian.tar.xz 50828 SHA512:aecfe27ac946a24f4dc0bffc0656a1dac5fb4c0e1d1e884eaec43139669d2fb34fea307e0a896b16056944c86e4a691bae4804df5d8e77b00d00696dce2b4964
```

### `dpkg` source package: `zvbi=0.2.35-17`

Binary Packages:

- `libzvbi-common=0.2.35-17`
- `libzvbi0:amd64=0.2.35-17`

Licenses: (parsed from: `/usr/share/doc/libzvbi-common/copyright`, `/usr/share/doc/libzvbi0/copyright`)

- `BSD-2-Clause`
- `BSD-3-Clause`
- `GPL-2`
- `GPL-2+`
- `LGPL-2`
- `LGPL-2+`
- `LGPL-2.1`
- `LGPL-2.1+`
- `MIT`

Source:

```console
$ apt-get source -qq --print-uris zvbi=0.2.35-17
'http://archive.ubuntu.com/ubuntu/pool/universe/z/zvbi/zvbi_0.2.35-17.dsc' zvbi_0.2.35-17.dsc 2119 SHA256:0f7b46b509461ca37b8ee53fa69aa6cf8f14c0f3120e6d5a48ce89f7ae953470
'http://archive.ubuntu.com/ubuntu/pool/universe/z/zvbi/zvbi_0.2.35.orig.tar.bz2' zvbi_0.2.35.orig.tar.bz2 1047761 SHA256:fc883c34111a487c4a783f91b1b2bb5610d8d8e58dcba80c7ab31e67e4765318
'http://archive.ubuntu.com/ubuntu/pool/universe/z/zvbi/zvbi_0.2.35-17.debian.tar.xz' zvbi_0.2.35-17.debian.tar.xz 15868 SHA256:92e3aecc9ca185f82772223b2bdf6458f7cce288910c1ebb698d23bde28a828d
```
