## `odoo:15.0`

```console
$ docker pull odoo@sha256:cef84f3dd21de701ce1c1500b7557b8050fce5c8d53bd668a00c79aebbf0fa76
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `odoo:15.0` - linux; amd64

```console
$ docker pull odoo@sha256:b6cdd0e2de596e440cb30043ee53f99c3096fce0fb6e7b23a6a804104e9fce07
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **542.6 MB (542592410 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:62ae3f4898cd2a040419215578ff13034a38282cff1be6af78da806217409f6b`
-	Entrypoint: `["\/entrypoint.sh"]`
-	Default Command: `["odoo"]`
-	`SHELL`: `["\/bin\/bash","-xo","pipefail","-c"]`

```dockerfile
# Tue, 12 Oct 2021 01:20:42 GMT
ADD file:16dc2c6d1932194edec28d730b004fd6deca3d0f0e1a07bc5b8b6e8a1662f7af in / 
# Tue, 12 Oct 2021 01:20:42 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 12:01:12 GMT
MAINTAINER Odoo S.A. <info@odoo.com>
# Tue, 12 Oct 2021 12:01:12 GMT
SHELL [/bin/bash -xo pipefail -c]
# Tue, 12 Oct 2021 12:01:12 GMT
ENV LANG=C.UTF-8
# Mon, 01 Nov 2021 18:41:08 GMT
RUN apt-get update &&     apt-get install -y --no-install-recommends         ca-certificates         curl         dirmngr         fonts-noto-cjk         gnupg         libssl-dev         node-less         npm         python3-num2words         python3-pdfminer         python3-pip         python3-phonenumbers         python3-pyldap         python3-qrcode         python3-renderpm         python3-setuptools         python3-slugify         python3-vobject         python3-watchdog         python3-xlrd         python3-xlwt         xz-utils     && curl -o wkhtmltox.deb -sSL https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.buster_amd64.deb     && echo 'ea8277df4297afc507c61122f3c349af142f31e5 wkhtmltox.deb' | sha1sum -c -     && apt-get install -y --no-install-recommends ./wkhtmltox.deb     && rm -rf /var/lib/apt/lists/* wkhtmltox.deb
# Mon, 01 Nov 2021 18:41:25 GMT
RUN echo 'deb http://apt.postgresql.org/pub/repos/apt/ bullseye-pgdg main' > /etc/apt/sources.list.d/pgdg.list     && GNUPGHOME="$(mktemp -d)"     && export GNUPGHOME     && repokey='B97B0AFCAA1A47F044F244A07FCC7D46ACCC4CF8'     && gpg --batch --keyserver keyserver.ubuntu.com --recv-keys "${repokey}"     && gpg --batch --armor --export "${repokey}" > /etc/apt/trusted.gpg.d/pgdg.gpg.asc     && gpgconf --kill all     && rm -rf "$GNUPGHOME"     && apt-get update      && apt-get install --no-install-recommends -y postgresql-client     && rm -f /etc/apt/sources.list.d/pgdg.list     && rm -rf /var/lib/apt/lists/*
# Mon, 01 Nov 2021 18:41:27 GMT
RUN npm install -g rtlcss
# Mon, 01 Nov 2021 18:41:27 GMT
ENV ODOO_VERSION=15.0
# Mon, 01 Nov 2021 18:41:27 GMT
ARG ODOO_RELEASE=20211029
# Mon, 01 Nov 2021 18:41:28 GMT
ARG ODOO_SHA=1b0d6aac881aa968a77e57f529d357688786a12c
# Mon, 01 Nov 2021 18:42:42 GMT
# ARGS: ODOO_RELEASE=20211029 ODOO_SHA=1b0d6aac881aa968a77e57f529d357688786a12c
RUN curl -o odoo.deb -sSL http://nightly.odoo.com/${ODOO_VERSION}/nightly/deb/odoo_${ODOO_VERSION}.${ODOO_RELEASE}_all.deb     && echo "${ODOO_SHA} odoo.deb" | sha1sum -c -     && apt-get update     && apt-get -y install --no-install-recommends ./odoo.deb     && rm -rf /var/lib/apt/lists/* odoo.deb
# Mon, 01 Nov 2021 18:42:46 GMT
COPY file:cbe34bc5236465e4ed439e0c8f6504d2d221f79f7c70b37fe62b56662bd0ab6d in / 
# Mon, 01 Nov 2021 18:42:46 GMT
COPY file:1e7209cce5525d270c422815db614f496d4d0da4820de1ab0000e9e592223235 in /etc/odoo/ 
# Mon, 01 Nov 2021 18:42:47 GMT
# ARGS: ODOO_RELEASE=20211029 ODOO_SHA=1b0d6aac881aa968a77e57f529d357688786a12c
RUN chown odoo /etc/odoo/odoo.conf     && mkdir -p /mnt/extra-addons     && chown -R odoo /mnt/extra-addons
# Mon, 01 Nov 2021 18:42:47 GMT
VOLUME [/var/lib/odoo /mnt/extra-addons]
# Mon, 01 Nov 2021 18:42:47 GMT
EXPOSE 8069 8071 8072
# Mon, 01 Nov 2021 18:42:48 GMT
ENV ODOO_RC=/etc/odoo/odoo.conf
# Mon, 01 Nov 2021 18:42:48 GMT
COPY file:0bc771c66dfeb517d19d13ea2699a0d3cbbbba684c8851640e6b87fe85b40619 in /usr/local/bin/wait-for-psql.py 
# Mon, 01 Nov 2021 18:42:48 GMT
USER odoo
# Mon, 01 Nov 2021 18:42:48 GMT
ENTRYPOINT ["/entrypoint.sh"]
# Mon, 01 Nov 2021 18:42:48 GMT
CMD ["odoo"]
```

-	Layers:
	-	`sha256:7d63c13d9b9b6ec5f05a2b07daadacaa9c610d01102a662ae9b1d082105f1ffa`  
		Last Modified: Tue, 12 Oct 2021 01:26:05 GMT  
		Size: 31.4 MB (31357311 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:6b2d273bb832b81c8fc74ca81a08380146f9747b3f7637bf5433fef5384fb5e9`  
		Last Modified: Mon, 01 Nov 2021 18:46:48 GMT  
		Size: 220.3 MB (220290324 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:e891e65741060a6859b64e8966d6bcbe4731e71dbb1cd01aca896ef2b991ba26`  
		Last Modified: Mon, 01 Nov 2021 18:46:18 GMT  
		Size: 2.5 MB (2504919 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:bd209f4a5fab9d306ff448dfa6b447ffbcb548d0ae1feefc6623f66dc2fd43a9`  
		Last Modified: Mon, 01 Nov 2021 18:46:17 GMT  
		Size: 856.1 KB (856062 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:7737a242428cd8250f85c14d968cc493d487f384432acfc1e3372d2d8e84cc14`  
		Last Modified: Mon, 01 Nov 2021 18:46:53 GMT  
		Size: 287.6 MB (287581332 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:0cf627bb0177b3581e858b43ef5b204bd578754745eca507109b34fadf6bc959`  
		Last Modified: Mon, 01 Nov 2021 18:46:15 GMT  
		Size: 705.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:1d45e604ee8cbd9ff88f4efff193c71b9ba248a54844d79ece8466fb16a7b184`  
		Last Modified: Mon, 01 Nov 2021 18:46:15 GMT  
		Size: 554.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:5afafacabc64ee82578c66cf2f453bcfc5a79a0b40779ae02ddc369a97d358bc`  
		Last Modified: Mon, 01 Nov 2021 18:46:15 GMT  
		Size: 620.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:fdecd849609950186ce8c926d417767e2bab6477ba87eb23198f692ee880454b`  
		Last Modified: Mon, 01 Nov 2021 18:46:15 GMT  
		Size: 583.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
