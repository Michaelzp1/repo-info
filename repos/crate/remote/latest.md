## `crate:latest`

```console
$ docker pull crate@sha256:6037307285a47571edb1d52c4684488c5ee71723a494745e1a340d0ec3c0a20d
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 2
	-	linux; amd64
	-	linux; arm64 variant v8

### `crate:latest` - linux; amd64

```console
$ docker pull crate@sha256:9ada59f61dbd3a04baa6be96bb0e5523fee2a7d6ee158223feb203b4631dfeee
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **332.9 MB (332885273 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:879339ea06ecc83e9df41ca6dcacea0086ff248c4ba77a657bad24570bdae843`
-	Entrypoint: `["\/docker-entrypoint.sh"]`
-	Default Command: `["crate"]`

```dockerfile
# Wed, 15 Sep 2021 18:20:23 GMT
ADD file:b3ebbe8bd304723d43b7b44a6d990cd657b63d93d6a2a9293983a30bfc1dfa53 in / 
# Wed, 15 Sep 2021 18:20:23 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20201113 org.opencontainers.image.title=CentOS Base Image org.opencontainers.image.vendor=CentOS org.opencontainers.image.licenses=GPL-2.0-only org.opencontainers.image.created=2020-11-13 00:00:00+00:00
# Wed, 15 Sep 2021 18:20:23 GMT
CMD ["/bin/bash"]
# Wed, 15 Sep 2021 18:37:51 GMT
RUN groupadd crate && useradd -u 1000 -g crate -d /crate crate
# Tue, 05 Oct 2021 22:22:22 GMT
RUN yum install -y yum-utils     && yum makecache     && yum install -y python36 openssl     && yum clean all     && rm -rf /var/cache/yum     && export PLATFORM="$(         case $(uname --m) in             x86_64)  echo x64_linux ;;             aarch64) echo aarch64_linux ;;         esac)"     && export CRATE_URL=https://cdn.crate.io/downloads/releases/cratedb/${PLATFORM}/crate-4.6.4.tar.gz     && curl -fSL -O ${CRATE_URL}     && curl -fSL -O ${CRATE_URL}.asc     && export GNUPGHOME="$(mktemp -d)"     && gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 90C23FC6585BC0717F8FBFC37FAAE51A06F6EAEB     && gpg --batch --verify crate-4.6.4.tar.gz.asc crate-4.6.4.tar.gz     && rm -rf "$GNUPGHOME" crate-4.6.4.tar.gz.asc     && tar -xf crate-4.6.4.tar.gz -C /crate --strip-components=1     && rm crate-4.6.4.tar.gz     && ln -sf /usr/bin/python3.6 /usr/bin/python3
# Tue, 05 Oct 2021 22:22:28 GMT
RUN curl -fSL -O https://cdn.crate.io/downloads/releases/crash_standalone_0.27.0     && curl -fSL -O https://cdn.crate.io/downloads/releases/crash_standalone_0.27.0.asc     && export GNUPGHOME="$(mktemp -d)"     && gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 90C23FC6585BC0717F8FBFC37FAAE51A06F6EAEB     && gpg --batch --verify crash_standalone_0.27.0.asc crash_standalone_0.27.0     && rm -rf "$GNUPGHOME" crash_standalone_0.27.0.asc     && mv crash_standalone_0.27.0 /usr/local/bin/crash     && chmod +x /usr/local/bin/crash
# Tue, 05 Oct 2021 22:22:28 GMT
ENV PATH=/crate/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
# Tue, 05 Oct 2021 22:22:28 GMT
ENV CRATE_HEAP_SIZE=512M
# Tue, 05 Oct 2021 22:22:29 GMT
RUN mkdir -p /data/data /data/log
# Tue, 05 Oct 2021 22:22:29 GMT
VOLUME [/data]
# Tue, 05 Oct 2021 22:22:29 GMT
WORKDIR /data
# Tue, 05 Oct 2021 22:22:30 GMT
EXPOSE 4200 4300 5432
# Tue, 05 Oct 2021 22:22:30 GMT
COPY --chown=1000:0file:bff8d2f33b7a44d36fcd66fc7e7d92b0ee463d0eb0df2a56e42511d4f1b3e9b2 in /crate/config/crate.yml 
# Tue, 05 Oct 2021 22:22:30 GMT
COPY --chown=1000:0file:5f0d1b776d3a6517508a00a88f8053bd0933a642599374c9dff00dc3b632fd09 in /crate/config/log4j2.properties 
# Tue, 05 Oct 2021 22:22:30 GMT
LABEL maintainer=Crate.io <office@crate.io> org.opencontainers.image.created=2021-09-30T17:09:13.685861 org.opencontainers.image.title=crate org.opencontainers.image.description=CrateDB is a distributed SQL database handles massive amounts of machine data in real-time. org.opencontainers.image.url=https://crate.io/products/cratedb/ org.opencontainers.image.source=https://github.com/crate/docker-crate org.opencontainers.image.vendor=Crate.io org.opencontainers.image.version=4.6.4
# Tue, 05 Oct 2021 22:22:30 GMT
COPY file:2e0f9e8c9006d6d56e9be42bd5646f68ec854481fcfbe51bafbf8695dc44b38a in / 
# Tue, 05 Oct 2021 22:22:31 GMT
ENTRYPOINT ["/docker-entrypoint.sh"]
# Tue, 05 Oct 2021 22:22:31 GMT
CMD ["crate"]
```

-	Layers:
	-	`sha256:2d473b07cdd5f0912cd6f1a703352c82b512407db6b05b43f2553732b55df3bc`  
		Last Modified: Sat, 14 Nov 2020 00:21:39 GMT  
		Size: 76.1 MB (76097157 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:b64b4c75f30c9b7afb780c16245b96bddad3d255e679114717859265250a0f64`  
		Last Modified: Wed, 15 Sep 2021 18:46:27 GMT  
		Size: 2.3 KB (2257 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:a005fb0a10da8a793f14f6d255fbc0523a581c642c527994e881af63cf5952c9`  
		Last Modified: Tue, 05 Oct 2021 22:23:37 GMT  
		Size: 255.2 MB (255201778 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:60be56f4c6c2c38c0c8ecf2f5f109577b5c94b2986e5fcf35a2caefab310f37c`  
		Last Modified: Tue, 05 Oct 2021 22:23:15 GMT  
		Size: 1.6 MB (1582196 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:2231b661e7b9b5e12f7fee501b02e503e16dd1ffd4541999fa1b3b7c0ecdf03a`  
		Last Modified: Tue, 05 Oct 2021 22:23:14 GMT  
		Size: 156.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:d72a020cfc7e3a4ea0ee5222d11046ee05ad33c9c9852f9a26c7277490fa47f9`  
		Last Modified: Tue, 05 Oct 2021 22:23:14 GMT  
		Size: 266.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:5841c0d2246c632837ec6ea295df94e0d74c0da602ddc26cbab74663cedc328d`  
		Last Modified: Tue, 05 Oct 2021 22:23:14 GMT  
		Size: 959.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:8dfa95e2d2ee6a5b0b99546a53f3e2af785ae31693be72398481aa0bef2bdb00`  
		Last Modified: Tue, 05 Oct 2021 22:23:14 GMT  
		Size: 504.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `crate:latest` - linux; arm64 variant v8

```console
$ docker pull crate@sha256:76398e5ec8661eb1d809a31dfd3728ffebf48d4b5d8c16eecc33175181797c16
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **362.0 MB (361950546 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:41fb4f1c16ee8b5fa0df0c1efb2a45c6864f311e68dbf46b103896dfdd0a165f`
-	Entrypoint: `["\/docker-entrypoint.sh"]`
-	Default Command: `["crate"]`

```dockerfile
# Wed, 15 Sep 2021 17:39:58 GMT
ADD file:5b1e63a3cb041177b802b501dedcd71a86f1773ea0f69f048f2eb3901097711d in / 
# Wed, 15 Sep 2021 17:39:58 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20201113 org.opencontainers.image.title=CentOS Base Image org.opencontainers.image.vendor=CentOS org.opencontainers.image.licenses=GPL-2.0-only org.opencontainers.image.created=2020-11-13 00:00:00+00:00
# Wed, 15 Sep 2021 17:39:59 GMT
CMD ["/bin/bash"]
# Wed, 15 Sep 2021 18:03:27 GMT
RUN groupadd crate && useradd -u 1000 -g crate -d /crate crate
# Tue, 05 Oct 2021 22:42:33 GMT
RUN yum install -y yum-utils     && yum makecache     && yum install -y python36 openssl     && yum clean all     && rm -rf /var/cache/yum     && export PLATFORM="$(         case $(uname --m) in             x86_64)  echo x64_linux ;;             aarch64) echo aarch64_linux ;;         esac)"     && export CRATE_URL=https://cdn.crate.io/downloads/releases/cratedb/${PLATFORM}/crate-4.6.4.tar.gz     && curl -fSL -O ${CRATE_URL}     && curl -fSL -O ${CRATE_URL}.asc     && export GNUPGHOME="$(mktemp -d)"     && gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 90C23FC6585BC0717F8FBFC37FAAE51A06F6EAEB     && gpg --batch --verify crate-4.6.4.tar.gz.asc crate-4.6.4.tar.gz     && rm -rf "$GNUPGHOME" crate-4.6.4.tar.gz.asc     && tar -xf crate-4.6.4.tar.gz -C /crate --strip-components=1     && rm crate-4.6.4.tar.gz     && ln -sf /usr/bin/python3.6 /usr/bin/python3
# Tue, 05 Oct 2021 22:42:47 GMT
RUN curl -fSL -O https://cdn.crate.io/downloads/releases/crash_standalone_0.27.0     && curl -fSL -O https://cdn.crate.io/downloads/releases/crash_standalone_0.27.0.asc     && export GNUPGHOME="$(mktemp -d)"     && gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 90C23FC6585BC0717F8FBFC37FAAE51A06F6EAEB     && gpg --batch --verify crash_standalone_0.27.0.asc crash_standalone_0.27.0     && rm -rf "$GNUPGHOME" crash_standalone_0.27.0.asc     && mv crash_standalone_0.27.0 /usr/local/bin/crash     && chmod +x /usr/local/bin/crash
# Tue, 05 Oct 2021 22:42:47 GMT
ENV PATH=/crate/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
# Tue, 05 Oct 2021 22:42:47 GMT
ENV CRATE_HEAP_SIZE=512M
# Tue, 05 Oct 2021 22:42:48 GMT
RUN mkdir -p /data/data /data/log
# Tue, 05 Oct 2021 22:42:48 GMT
VOLUME [/data]
# Tue, 05 Oct 2021 22:42:48 GMT
WORKDIR /data
# Tue, 05 Oct 2021 22:42:49 GMT
EXPOSE 4200 4300 5432
# Tue, 05 Oct 2021 22:42:49 GMT
COPY --chown=1000:0file:bff8d2f33b7a44d36fcd66fc7e7d92b0ee463d0eb0df2a56e42511d4f1b3e9b2 in /crate/config/crate.yml 
# Tue, 05 Oct 2021 22:42:49 GMT
COPY --chown=1000:0file:5f0d1b776d3a6517508a00a88f8053bd0933a642599374c9dff00dc3b632fd09 in /crate/config/log4j2.properties 
# Tue, 05 Oct 2021 22:42:49 GMT
LABEL maintainer=Crate.io <office@crate.io> org.opencontainers.image.created=2021-09-30T17:09:13.685861 org.opencontainers.image.title=crate org.opencontainers.image.description=CrateDB is a distributed SQL database handles massive amounts of machine data in real-time. org.opencontainers.image.url=https://crate.io/products/cratedb/ org.opencontainers.image.source=https://github.com/crate/docker-crate org.opencontainers.image.vendor=Crate.io org.opencontainers.image.version=4.6.4
# Tue, 05 Oct 2021 22:42:49 GMT
COPY file:2e0f9e8c9006d6d56e9be42bd5646f68ec854481fcfbe51bafbf8695dc44b38a in / 
# Tue, 05 Oct 2021 22:42:50 GMT
ENTRYPOINT ["/docker-entrypoint.sh"]
# Tue, 05 Oct 2021 22:42:50 GMT
CMD ["crate"]
```

-	Layers:
	-	`sha256:6717b8ec66cd6add0272c6391165585613c31314a43ff77d9751b53010e531ec`  
		Last Modified: Sat, 14 Nov 2020 00:41:36 GMT  
		Size: 108.4 MB (108374945 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:29ba2218bab9ebc3c118f07cc854153c5a0c3c9b251c26673a1c24bbfe59f44d`  
		Last Modified: Wed, 15 Sep 2021 18:14:59 GMT  
		Size: 2.3 KB (2259 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:60520358f86a6d9fa0f0ec3cb6cdb07a56d948a288c48a579b7c84e945e04af2`  
		Last Modified: Tue, 05 Oct 2021 22:48:23 GMT  
		Size: 252.0 MB (251989270 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:f3bba2862655723cf05165c816f4665c52f3b2e9eadc31ff23f2f6b2136b4c0a`  
		Last Modified: Tue, 05 Oct 2021 22:47:56 GMT  
		Size: 1.6 MB (1582188 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:2ecf5ed3503cd74b35038b0395ea63386b136a6a4c6d1d70b953b01baf03bbb4`  
		Last Modified: Tue, 05 Oct 2021 22:47:55 GMT  
		Size: 157.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:9799643010467d3de3bd93930aa0347cf4b5903061bdbcfefb8530d195b2d9b7`  
		Last Modified: Tue, 05 Oct 2021 22:47:55 GMT  
		Size: 265.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:afe435123f7a61476250dc2d81426731b63239a1b739c4b87891e09536121056`  
		Last Modified: Tue, 05 Oct 2021 22:47:55 GMT  
		Size: 957.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:d956140fa2388d1557babd620ff2a416bd29296fa446f52199b436201abf1c62`  
		Last Modified: Tue, 05 Oct 2021 22:47:55 GMT  
		Size: 505.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
