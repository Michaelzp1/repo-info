<!-- THIS FILE IS GENERATED VIA './update-remote.sh' -->

# Tags of `archlinux`

-	[`archlinux:base`](#archlinuxbase)
-	[`archlinux:base-20211024.0.37588`](#archlinuxbase-20211024037588)
-	[`archlinux:base-devel`](#archlinuxbase-devel)
-	[`archlinux:base-devel-20211024.0.37588`](#archlinuxbase-devel-20211024037588)
-	[`archlinux:latest`](#archlinuxlatest)

## `archlinux:base`

```console
$ docker pull archlinux@sha256:7801d9eb7882c8c7f81e469ae64040272c7f5c79f2446c8f82be7c2547b54f86
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `archlinux:base` - linux; amd64

```console
$ docker pull archlinux@sha256:04d0ec982fe298e0a8fbda48461945c8ed6f0b24dc12492e68b31eac576fc327
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **134.4 MB (134389197 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:254eab97f3973cd515efe6d6d167fa39154019370fd0e187ff4d251e1ec58c58`
-	Default Command: `["\/usr\/bin\/bash"]`

```dockerfile
# Mon, 25 Oct 2021 18:20:13 GMT
COPY dir:d23bf4670f6d9f9875347e6a6b14b47a2edb1595fe8c222e842842410ccd235f in / 
# Mon, 25 Oct 2021 18:20:15 GMT
RUN ldconfig
# Mon, 25 Oct 2021 18:20:15 GMT
ENV LANG=en_US.UTF-8
# Mon, 25 Oct 2021 18:20:15 GMT
CMD ["/usr/bin/bash"]
```

-	Layers:
	-	`sha256:41a9f69bb30e916394ca46db4ee56a396d5b26d80078cb3c3b1accdeabfab9b4`  
		Last Modified: Mon, 25 Oct 2021 18:22:05 GMT  
		Size: 134.4 MB (134382438 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:0a6f83522215daf2a4cd1d23a1c332484deaf27f9e9ac3e36b3e1b3dfd53271a`  
		Last Modified: Mon, 25 Oct 2021 18:21:45 GMT  
		Size: 6.8 KB (6759 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `archlinux:base-20211024.0.37588`

```console
$ docker pull archlinux@sha256:7801d9eb7882c8c7f81e469ae64040272c7f5c79f2446c8f82be7c2547b54f86
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `archlinux:base-20211024.0.37588` - linux; amd64

```console
$ docker pull archlinux@sha256:04d0ec982fe298e0a8fbda48461945c8ed6f0b24dc12492e68b31eac576fc327
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **134.4 MB (134389197 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:254eab97f3973cd515efe6d6d167fa39154019370fd0e187ff4d251e1ec58c58`
-	Default Command: `["\/usr\/bin\/bash"]`

```dockerfile
# Mon, 25 Oct 2021 18:20:13 GMT
COPY dir:d23bf4670f6d9f9875347e6a6b14b47a2edb1595fe8c222e842842410ccd235f in / 
# Mon, 25 Oct 2021 18:20:15 GMT
RUN ldconfig
# Mon, 25 Oct 2021 18:20:15 GMT
ENV LANG=en_US.UTF-8
# Mon, 25 Oct 2021 18:20:15 GMT
CMD ["/usr/bin/bash"]
```

-	Layers:
	-	`sha256:41a9f69bb30e916394ca46db4ee56a396d5b26d80078cb3c3b1accdeabfab9b4`  
		Last Modified: Mon, 25 Oct 2021 18:22:05 GMT  
		Size: 134.4 MB (134382438 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:0a6f83522215daf2a4cd1d23a1c332484deaf27f9e9ac3e36b3e1b3dfd53271a`  
		Last Modified: Mon, 25 Oct 2021 18:21:45 GMT  
		Size: 6.8 KB (6759 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `archlinux:base-devel`

```console
$ docker pull archlinux@sha256:4371fd5b5d2d99ee73e25b3b9a8d7b2258e371852dd0c5681802bf94b069aadf
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `archlinux:base-devel` - linux; amd64

```console
$ docker pull archlinux@sha256:e9788016d8b59ce59bdce42dc9bcabb79f1cb2ecd157914c9d9392ffd8a301f1
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **232.4 MB (232413790 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b4052b95a0be67fbed47bc09a1af68c5c62919cac5bd47ddabfe99e1d6edaa63`
-	Default Command: `["\/usr\/bin\/bash"]`

```dockerfile
# Mon, 25 Oct 2021 18:21:24 GMT
COPY dir:a6e3df4c664aa26ceb659e3e8b8701841f11e1d6b4978d74cecb3d0ed939c4cb in / 
# Mon, 25 Oct 2021 18:21:27 GMT
RUN ldconfig
# Mon, 25 Oct 2021 18:21:27 GMT
ENV LANG=en_US.UTF-8
# Mon, 25 Oct 2021 18:21:27 GMT
CMD ["/usr/bin/bash"]
```

-	Layers:
	-	`sha256:5f3642192ce75e3fed0afaca556519d83a8e0a5043008e4fe36a6b5606d63772`  
		Last Modified: Mon, 25 Oct 2021 18:22:50 GMT  
		Size: 232.4 MB (232406537 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:dd4daf05c160cfa201a18c68ba2a805b861e3f06bac4e91f47a411939167b496`  
		Last Modified: Mon, 25 Oct 2021 18:22:17 GMT  
		Size: 7.3 KB (7253 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `archlinux:base-devel-20211024.0.37588`

```console
$ docker pull archlinux@sha256:4371fd5b5d2d99ee73e25b3b9a8d7b2258e371852dd0c5681802bf94b069aadf
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `archlinux:base-devel-20211024.0.37588` - linux; amd64

```console
$ docker pull archlinux@sha256:e9788016d8b59ce59bdce42dc9bcabb79f1cb2ecd157914c9d9392ffd8a301f1
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **232.4 MB (232413790 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b4052b95a0be67fbed47bc09a1af68c5c62919cac5bd47ddabfe99e1d6edaa63`
-	Default Command: `["\/usr\/bin\/bash"]`

```dockerfile
# Mon, 25 Oct 2021 18:21:24 GMT
COPY dir:a6e3df4c664aa26ceb659e3e8b8701841f11e1d6b4978d74cecb3d0ed939c4cb in / 
# Mon, 25 Oct 2021 18:21:27 GMT
RUN ldconfig
# Mon, 25 Oct 2021 18:21:27 GMT
ENV LANG=en_US.UTF-8
# Mon, 25 Oct 2021 18:21:27 GMT
CMD ["/usr/bin/bash"]
```

-	Layers:
	-	`sha256:5f3642192ce75e3fed0afaca556519d83a8e0a5043008e4fe36a6b5606d63772`  
		Last Modified: Mon, 25 Oct 2021 18:22:50 GMT  
		Size: 232.4 MB (232406537 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:dd4daf05c160cfa201a18c68ba2a805b861e3f06bac4e91f47a411939167b496`  
		Last Modified: Mon, 25 Oct 2021 18:22:17 GMT  
		Size: 7.3 KB (7253 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `archlinux:latest`

```console
$ docker pull archlinux@sha256:7801d9eb7882c8c7f81e469ae64040272c7f5c79f2446c8f82be7c2547b54f86
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `archlinux:latest` - linux; amd64

```console
$ docker pull archlinux@sha256:04d0ec982fe298e0a8fbda48461945c8ed6f0b24dc12492e68b31eac576fc327
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **134.4 MB (134389197 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:254eab97f3973cd515efe6d6d167fa39154019370fd0e187ff4d251e1ec58c58`
-	Default Command: `["\/usr\/bin\/bash"]`

```dockerfile
# Mon, 25 Oct 2021 18:20:13 GMT
COPY dir:d23bf4670f6d9f9875347e6a6b14b47a2edb1595fe8c222e842842410ccd235f in / 
# Mon, 25 Oct 2021 18:20:15 GMT
RUN ldconfig
# Mon, 25 Oct 2021 18:20:15 GMT
ENV LANG=en_US.UTF-8
# Mon, 25 Oct 2021 18:20:15 GMT
CMD ["/usr/bin/bash"]
```

-	Layers:
	-	`sha256:41a9f69bb30e916394ca46db4ee56a396d5b26d80078cb3c3b1accdeabfab9b4`  
		Last Modified: Mon, 25 Oct 2021 18:22:05 GMT  
		Size: 134.4 MB (134382438 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:0a6f83522215daf2a4cd1d23a1c332484deaf27f9e9ac3e36b3e1b3dfd53271a`  
		Last Modified: Mon, 25 Oct 2021 18:21:45 GMT  
		Size: 6.8 KB (6759 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
