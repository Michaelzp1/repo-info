# `archlinux:base-devel`

## Docker Metadata

- Image ID: `sha256:b4052b95a0be67fbed47bc09a1af68c5c62919cac5bd47ddabfe99e1d6edaa63`
- Created: `2021-10-25T18:21:27.879462753Z`
- Virtual Size: ~ 698.82 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["/usr/bin/bash"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `LANG=en_US.UTF-8`
