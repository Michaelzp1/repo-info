# `archlinux:latest`

## Docker Metadata

- Image ID: `sha256:254eab97f3973cd515efe6d6d167fa39154019370fd0e187ff4d251e1ec58c58`
- Created: `2021-10-25T18:20:15.530834121Z`
- Virtual Size: ~ 384.42 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["/usr/bin/bash"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `LANG=en_US.UTF-8`
