## `archlinux:base`

```console
$ docker pull archlinux@sha256:7801d9eb7882c8c7f81e469ae64040272c7f5c79f2446c8f82be7c2547b54f86
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `archlinux:base` - linux; amd64

```console
$ docker pull archlinux@sha256:04d0ec982fe298e0a8fbda48461945c8ed6f0b24dc12492e68b31eac576fc327
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **134.4 MB (134389197 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:254eab97f3973cd515efe6d6d167fa39154019370fd0e187ff4d251e1ec58c58`
-	Default Command: `["\/usr\/bin\/bash"]`

```dockerfile
# Mon, 25 Oct 2021 18:20:13 GMT
COPY dir:d23bf4670f6d9f9875347e6a6b14b47a2edb1595fe8c222e842842410ccd235f in / 
# Mon, 25 Oct 2021 18:20:15 GMT
RUN ldconfig
# Mon, 25 Oct 2021 18:20:15 GMT
ENV LANG=en_US.UTF-8
# Mon, 25 Oct 2021 18:20:15 GMT
CMD ["/usr/bin/bash"]
```

-	Layers:
	-	`sha256:41a9f69bb30e916394ca46db4ee56a396d5b26d80078cb3c3b1accdeabfab9b4`  
		Last Modified: Mon, 25 Oct 2021 18:22:05 GMT  
		Size: 134.4 MB (134382438 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:0a6f83522215daf2a4cd1d23a1c332484deaf27f9e9ac3e36b3e1b3dfd53271a`  
		Last Modified: Mon, 25 Oct 2021 18:21:45 GMT  
		Size: 6.8 KB (6759 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
