<!-- THIS FILE IS GENERATED VIA './update-remote.sh' -->

# Tags of `busybox`

-	[`busybox:1`](#busybox1)
-	[`busybox:1-glibc`](#busybox1-glibc)
-	[`busybox:1-musl`](#busybox1-musl)
-	[`busybox:1-uclibc`](#busybox1-uclibc)
-	[`busybox:1.34`](#busybox134)
-	[`busybox:1.34-glibc`](#busybox134-glibc)
-	[`busybox:1.34-musl`](#busybox134-musl)
-	[`busybox:1.34-uclibc`](#busybox134-uclibc)
-	[`busybox:1.34.1`](#busybox1341)
-	[`busybox:1.34.1-glibc`](#busybox1341-glibc)
-	[`busybox:1.34.1-musl`](#busybox1341-musl)
-	[`busybox:1.34.1-uclibc`](#busybox1341-uclibc)
-	[`busybox:glibc`](#busyboxglibc)
-	[`busybox:latest`](#busyboxlatest)
-	[`busybox:musl`](#busyboxmusl)
-	[`busybox:stable`](#busyboxstable)
-	[`busybox:stable-glibc`](#busyboxstable-glibc)
-	[`busybox:stable-musl`](#busyboxstable-musl)
-	[`busybox:stable-uclibc`](#busyboxstable-uclibc)
-	[`busybox:uclibc`](#busyboxuclibc)

## `busybox:1`

```console
$ docker pull busybox@sha256:15e927f78df2cc772b70713543d6b651e3cd8370abf86b2ea4644a9fba21107f
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 10
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v6
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; riscv64
	-	linux; s390x

### `busybox:1` - linux; amd64

```console
$ docker pull busybox@sha256:6066ca124f8c2686b7ae71aa1d6583b28c6dc3df3bdc386f2c89b92162c597d9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **772.8 KB (772798 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cabb9f684f8ba3edb303d578bfd7d709d853539ea1b420a3f6c81a08e85bb3d7`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:19:45 GMT
ADD file:88b76d15f53403ca217f156c3fa01df961ddf224a193cf39ebe51678b77cf9cf in / 
# Wed, 27 Oct 2021 17:19:45 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:01c2cdc137396a9b78be86c47e5773388e6eed953dec79e257777518134b45f1`  
		Last Modified: Wed, 27 Oct 2021 17:20:27 GMT  
		Size: 772.8 KB (772798 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1` - linux; arm variant v5

```console
$ docker pull busybox@sha256:399e1e4a0d587717dc9e3a85150cec8498cb6dc73dcb7eddb94959fedb331104
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **756.2 KB (756158 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f73013f2ba16094e6beade24ce90aa5fcd023baff78a1438c7221592d922d780`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:48:26 GMT
ADD file:c9324c40aae3b3e81b9315fe32797d78d508fb1823de84f7cd1d79e474418a8a in / 
# Wed, 27 Oct 2021 17:48:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3a7a3789f9861a9ab57dd6819c9dcdfcc9a4ea3172b32c88423f9630fb2c0816`  
		Last Modified: Wed, 27 Oct 2021 17:50:10 GMT  
		Size: 756.2 KB (756158 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1` - linux; arm variant v6

```console
$ docker pull busybox@sha256:4ecc3dc2e06a24df931cb719c3784611d15721c3cb64ab069141071b73f6598b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **952.5 KB (952546 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:327d97ae18db017776599d9b3713503c03974d14346adb99117a186bed6a4d47`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:49:25 GMT
ADD file:c9029a61e764f7eee2bb75e9c227970c7a9e7e81d2d2b2370b7cfa8cff8dddde in / 
# Wed, 27 Oct 2021 17:49:26 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:dac1aaad9ca43e5b6a33470c2cfc6cf3127115e539cae8bc865a8c99d9232727`  
		Last Modified: Wed, 27 Oct 2021 17:50:33 GMT  
		Size: 952.5 KB (952546 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1` - linux; arm variant v7

```console
$ docker pull busybox@sha256:53c212bcc0501f011c232df0fb6c837651d0b2f3257b6478a50c0e006b0dabc5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **724.2 KB (724238 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:17d83c72aaa7f419b771dad67eb87d285faa57f1b0d93ed2c9b5b45ea6879065`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 16:57:33 GMT
ADD file:2b15d7b82fc9c84090d98a80296a522d922e42b63fbd9ea47aa2bf62c14aa768 in / 
# Wed, 27 Oct 2021 16:57:33 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:da72ebe61afbc4deef433f5180ccb61df896dfb324e08c0a03b7c43a7c0347b4`  
		Last Modified: Wed, 27 Oct 2021 17:00:02 GMT  
		Size: 724.2 KB (724238 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1` - linux; arm64 variant v8

```console
$ docker pull busybox@sha256:ce53e9b0310447d0e851ff0d2c9b90f358dbffe719a723147e84b93a4799396c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **828.5 KB (828502 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cc54699a1d7e60dedfeabdf0e0fee681842a16a038ae551ad93b923457b031ab`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:39:27 GMT
ADD file:0e9e223690df05d052434f07b187d500e9544cf4a10992cb1cf17bbf0f9d261d in / 
# Wed, 27 Oct 2021 17:39:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:8ba530ca112c38dd01fa8c96a709f82e5e8cbb425af71f1185efc9ed2c1561d6`  
		Last Modified: Wed, 27 Oct 2021 17:40:27 GMT  
		Size: 828.5 KB (828502 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1` - linux; 386

```console
$ docker pull busybox@sha256:c71cb4f7e8ececaffb34037c2637dc86820e4185100e18b4d02d613a9bd772af
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **728.2 KB (728191 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:046e74e02754f28137f587f303f9852c82f3ec76fff1f2f278655b3e1193ad3e`
-	Default Command: `["sh"]`

```dockerfile
# Tue, 14 Sep 2021 01:38:33 GMT
ADD file:4143b23f3d54e871d1a69c3518704e9e6ebed52dcc278b5f1dbcd11343940898 in / 
# Tue, 14 Sep 2021 01:38:33 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:f835a1acb88b23855be36becf1ebe0a310ab5761b04c3fb24d0c94c41f7c3e41`  
		Last Modified: Tue, 14 Sep 2021 01:40:49 GMT  
		Size: 728.2 KB (728191 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1` - linux; mips64le

```console
$ docker pull busybox@sha256:77df281071dd7e01972ec2c4a33c1a6c00d13b24238375fd6622fce97f622fa2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **960.5 KB (960459 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:572b466fbb42b288f43e2d90cf55e26d33cbdd19d667aa31dd20c2628e344f38`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:07:29 GMT
ADD file:82547d0ac8de48ae2eb3f40f43a0e1635155361fc9e20b223e87d9de0ca27e96 in / 
# Wed, 27 Oct 2021 17:07:29 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3d8d4c508e7c9189254e23bfdd5f30c93223516f57715ee5bd0cc85b94d849ee`  
		Last Modified: Wed, 27 Oct 2021 17:07:59 GMT  
		Size: 960.5 KB (960459 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1` - linux; ppc64le

```console
$ docker pull busybox@sha256:b70f0f45692830c2990b42f770aa29488c20ac41f1c3dcaa242920b73cb1399b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.5 MB (2484095 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:defb34012ae5b6a308b2629e4a6b299d20a193e3475fc98c4fba27f35819061b`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 19:12:06 GMT
ADD file:79fcd19d69e0c4f07016224ba1b588b8c92aa08c3af515be806d4714d6118923 in / 
# Wed, 27 Oct 2021 19:12:09 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:8dcfc896d3701ab1ec378f31cf3076493c9b8c588f103af7901ce812bb09b317`  
		Last Modified: Wed, 27 Oct 2021 19:13:34 GMT  
		Size: 2.5 MB (2484095 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1` - linux; riscv64

```console
$ docker pull busybox@sha256:06b206c1f1a38094697c7e8bf868f9d326e56a256bc516dbb8ff0ee9c1178999
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **905.3 KB (905306 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a021c599555f76331fddec6e6ecc87cfb92cfd26ab42ef0b968dacb9890cefdc`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:12:42 GMT
ADD file:bbfcdc6a5eda2a3b7965fc2b73a783bf34893ee2cced227315f8468489ec1dee in / 
# Wed, 27 Oct 2021 17:12:43 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:0b2bb6a84cb70a09486f5568f13ac57db414fb39197ce05043689bdeae75744e`  
		Last Modified: Wed, 27 Oct 2021 17:23:32 GMT  
		Size: 905.3 KB (905306 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1` - linux; s390x

```console
$ docker pull busybox@sha256:86824a27910bd2a8c6a8478fe99206e6cf4bcada7cb8435c0060cbe885559e53
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.0 MB (2006829 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9e119a54a2534d8ae95d15840f3c4feed03866eb4aee947c49e679206be94893`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:41:19 GMT
ADD file:09207d85b622b42ab9880ec4cbfe2212390ba7c1b0704b91bd3db041db9d9e03 in / 
# Wed, 27 Oct 2021 17:41:19 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:120da903ae50712b69209ab560f7855f3ffed8e6cd9c78dcd74b7e8754c5b168`  
		Last Modified: Wed, 27 Oct 2021 17:42:10 GMT  
		Size: 2.0 MB (2006829 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `busybox:1-glibc`

```console
$ docker pull busybox@sha256:49ccc099fce12d9673767a60f88761bd56f2ddef85bbdc605fc9c5db2dd5bcb3
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `busybox:1-glibc` - linux; amd64

```console
$ docker pull busybox@sha256:a158839ba99a90008ddd922dddb869d4a998f60339ee0c41ef9009e60995247a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2566111 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f17f5d94ca617134e143b569c2ec48a7d5864d8387d1191e03116c7ad428bfdf`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:19:53 GMT
ADD file:05820ca2a136d0bd67d2c84ac5294fb0b112df68fcd3482ed77f4d47d30c2106 in / 
# Wed, 27 Oct 2021 17:19:53 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:df20d59888d8cd750954e0854d27df703b73b98999981beeb0ec769c8a707226`  
		Last Modified: Wed, 27 Oct 2021 17:20:43 GMT  
		Size: 2.6 MB (2566111 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1-glibc` - linux; arm variant v5

```console
$ docker pull busybox@sha256:068abb3d195234a48605f5067881edfa228840ea5c83245d61e3798979c02961
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **1.9 MB (1930775 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:454a6225af3074772111413ba8b568c07d177f2475e59cbbe6d909156742023f`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:48:41 GMT
ADD file:7bff807316ba841e7e1c392a909bc4d4ab078c4e0ee9e99e62bd89c5002bbc21 in / 
# Wed, 27 Oct 2021 17:48:41 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:58e53cd44176b2564294ddc75394d3683931c011e7c2db9794e90ef796228270`  
		Last Modified: Wed, 27 Oct 2021 17:50:34 GMT  
		Size: 1.9 MB (1930775 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1-glibc` - linux; arm variant v7

```console
$ docker pull busybox@sha256:3c56c483948e49726761f4bf2ae09cf227db38d75362ab40024cfc7deda4b04f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **1.7 MB (1678694 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:393a97107921f708c9df011fd1841f3a1c34a1de86c96cd9ef6033c9708f1f11`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 16:57:50 GMT
ADD file:a8b26211fe41f82dec07ffe639f4c6ef4880076f74d8b5f0eb59e34546b325e9 in / 
# Wed, 27 Oct 2021 16:57:50 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:fa418c43645700e527bff2dfb63459af17af7b059c31f60f412699e48fe122a2`  
		Last Modified: Wed, 27 Oct 2021 17:00:27 GMT  
		Size: 1.7 MB (1678694 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1-glibc` - linux; arm64 variant v8

```console
$ docker pull busybox@sha256:a72f19c71777a59bf2ea290ceefbe6537e2c8c544463cd57859ef5d432fb244b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.0 MB (1996159 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6fb1464c3dc04a6f8a7c92cdb9f3dfc5ab1c9116061a85ddc90825680be154eb`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:39:34 GMT
ADD file:8bdeb927d5560194f8ba5688d4d9ee1ff7254928145aafe79f7aa326f70ab234 in / 
# Wed, 27 Oct 2021 17:39:34 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:aae080e05ab28f73910de7f8e9f092f5c7eb9f2658363454a3361e69e5723954`  
		Last Modified: Wed, 27 Oct 2021 17:40:46 GMT  
		Size: 2.0 MB (1996159 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1-glibc` - linux; 386

```console
$ docker pull busybox@sha256:f2005b8003d6cba4248292df72d03ce8b1aaa048e27eb46a2772a3c486de9212
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.2 MB (2226613 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:edd609bf27e9452d3644678d41c01a84556e59c3f1ab16560448a7161d552546`
-	Default Command: `["sh"]`

```dockerfile
# Tue, 14 Sep 2021 01:38:42 GMT
ADD file:0fa6993c66b449fe9530e9027f7237d4fd0cc9e161a32ff73a9014864cfe7bfd in / 
# Tue, 14 Sep 2021 01:38:42 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:63502a2694813d8f0580c464540ecf7c7f5c6017f74a988c7f6d3b669adfc9c5`  
		Last Modified: Tue, 14 Sep 2021 01:41:11 GMT  
		Size: 2.2 MB (2226613 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1-glibc` - linux; mips64le

```console
$ docker pull busybox@sha256:6a2a782c46dedb94c1370a60192df26e00da690b90fa16cb4eb11352e45ec665
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.2 MB (2220711 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:25bbb10f71e32f9c63967dfd90d2213df5f3c8ef303bb47171f3920bb066ae7f`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:07:38 GMT
ADD file:47a6e39f530c1c6dcfc7fbcce83900c83627949ac78e6a74132145f11b427b59 in / 
# Wed, 27 Oct 2021 17:07:38 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:b7954f921f64e29ca6451787b44b6ed22eb7177760e8d8fa31f60c312d7ae676`  
		Last Modified: Wed, 27 Oct 2021 17:08:17 GMT  
		Size: 2.2 MB (2220711 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1-glibc` - linux; ppc64le

```console
$ docker pull busybox@sha256:b70f0f45692830c2990b42f770aa29488c20ac41f1c3dcaa242920b73cb1399b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.5 MB (2484095 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:defb34012ae5b6a308b2629e4a6b299d20a193e3475fc98c4fba27f35819061b`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 19:12:06 GMT
ADD file:79fcd19d69e0c4f07016224ba1b588b8c92aa08c3af515be806d4714d6118923 in / 
# Wed, 27 Oct 2021 19:12:09 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:8dcfc896d3701ab1ec378f31cf3076493c9b8c588f103af7901ce812bb09b317`  
		Last Modified: Wed, 27 Oct 2021 19:13:34 GMT  
		Size: 2.5 MB (2484095 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1-glibc` - linux; s390x

```console
$ docker pull busybox@sha256:86824a27910bd2a8c6a8478fe99206e6cf4bcada7cb8435c0060cbe885559e53
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.0 MB (2006829 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9e119a54a2534d8ae95d15840f3c4feed03866eb4aee947c49e679206be94893`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:41:19 GMT
ADD file:09207d85b622b42ab9880ec4cbfe2212390ba7c1b0704b91bd3db041db9d9e03 in / 
# Wed, 27 Oct 2021 17:41:19 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:120da903ae50712b69209ab560f7855f3ffed8e6cd9c78dcd74b7e8754c5b168`  
		Last Modified: Wed, 27 Oct 2021 17:42:10 GMT  
		Size: 2.0 MB (2006829 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `busybox:1-musl`

```console
$ docker pull busybox@sha256:13b27cf92cc81d1f4c1584360942bedce72e98af80aa273f77382715a33f68c2
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 7
	-	linux; amd64
	-	linux; arm variant v6
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; ppc64le
	-	linux; s390x

### `busybox:1-musl` - linux; amd64

```console
$ docker pull busybox@sha256:c57be995c4c3bb22b723c49d74fc3b5a4510dfbd8be28b5d021d7a4a53a4bab1
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **843.3 KB (843266 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1b408f772ab08633f27bf8918481dfd3fe2f7689b23a50897cb7cef2e17a6cb8`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:19:58 GMT
ADD file:58dbb8135e92be6f243e74462af8e85fd67417ba0281b81ea7742af344794c3f in / 
# Wed, 27 Oct 2021 17:19:58 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:b0e194f8faadf770ca46f1caf5b5ce0bb82ef76806729e22c1a20aff0c29c1f6`  
		Last Modified: Wed, 27 Oct 2021 17:20:59 GMT  
		Size: 843.3 KB (843266 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1-musl` - linux; arm variant v6

```console
$ docker pull busybox@sha256:4ecc3dc2e06a24df931cb719c3784611d15721c3cb64ab069141071b73f6598b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **952.5 KB (952546 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:327d97ae18db017776599d9b3713503c03974d14346adb99117a186bed6a4d47`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:49:25 GMT
ADD file:c9029a61e764f7eee2bb75e9c227970c7a9e7e81d2d2b2370b7cfa8cff8dddde in / 
# Wed, 27 Oct 2021 17:49:26 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:dac1aaad9ca43e5b6a33470c2cfc6cf3127115e539cae8bc865a8c99d9232727`  
		Last Modified: Wed, 27 Oct 2021 17:50:33 GMT  
		Size: 952.5 KB (952546 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1-musl` - linux; arm variant v7

```console
$ docker pull busybox@sha256:e23f636436b365735d7c726df3e36e48fe309afaa422a503bd8ecf9740742f9c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **834.7 KB (834677 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:27fba482d17b4205fe8a01218cc62d830b45f0a1e9ee13eeb81cf2cd7a69390d`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 16:58:07 GMT
ADD file:242e49f9869dfe3d8b59aee3801ef14dac3c58176f6cea670614284afd50ce8d in / 
# Wed, 27 Oct 2021 16:58:08 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:9d172075913815538a77dd7a7ad3bbefc76474354936eddd73cd933e9776b8e2`  
		Last Modified: Wed, 27 Oct 2021 17:00:50 GMT  
		Size: 834.7 KB (834677 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1-musl` - linux; arm64 variant v8

```console
$ docker pull busybox@sha256:976ce0d1af7874bc0216c19efbd9178cc0455513a963cd834a35b9e732e298fb
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **882.2 KB (882176 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:69552745f9d2985d9bb00c5d912723a5f4a9afd133966cf8982b06067a3f7cf7`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:39:41 GMT
ADD file:4562e1c40e298277fc65259e3b29d3b72f89a28088f94bcae8e543df2ede1f5d in / 
# Wed, 27 Oct 2021 17:39:41 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:20a2e0c8f8c2704a1cd5560c3a242607af57c42ae6a3109108be0303d32092b1`  
		Last Modified: Wed, 27 Oct 2021 17:41:05 GMT  
		Size: 882.2 KB (882176 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1-musl` - linux; 386

```console
$ docker pull busybox@sha256:2eed3c17cd8b978262caaed66dca91a6d23f654f7898f3fabeb09151b31c32e3
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **842.7 KB (842669 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f8748766cfd16c8edfbd2abe6a441ddfdeae7dd62276b21a0b5f2314568db712`
-	Default Command: `["sh"]`

```dockerfile
# Tue, 14 Sep 2021 01:38:53 GMT
ADD file:6ff60306035d5794506a2bec937e683fb5a799fabf489986cf76830451f679d5 in / 
# Tue, 14 Sep 2021 01:38:53 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:7d0e04faa0be851392cfcbd49724f856f34d5fb7c10d0b1ed1dea8bc94d4d4b1`  
		Last Modified: Tue, 14 Sep 2021 01:41:31 GMT  
		Size: 842.7 KB (842669 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1-musl` - linux; ppc64le

```console
$ docker pull busybox@sha256:7696de5d912dc3db582ba08c435f8b1b01a0cf845db7e44f1ad4269b656b627a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **919.2 KB (919168 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:0d6acacaf155166a2a58e39cd33187987fe4b36400dedbe22607902b50f3793d`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 19:12:21 GMT
ADD file:b19d93c361eebcc52af7de98cdbacec78af10e600d8cd334b9301752ba712766 in / 
# Wed, 27 Oct 2021 19:12:30 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:2abb4f53c2ba292747e959a7d205240ab584ca7650ff38823327a68680916549`  
		Last Modified: Wed, 27 Oct 2021 19:13:53 GMT  
		Size: 919.2 KB (919168 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1-musl` - linux; s390x

```console
$ docker pull busybox@sha256:2839b6e6e257d487803c6942666415c052a1e1c11e3d41d57fe30d25c50da924
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **893.9 KB (893870 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4c5d5f91ce1964dd844df1a44fba72b596f170eb0554cf45c195d2ec65849f23`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:41:26 GMT
ADD file:5722acc1a5fd74b7ff5c73c86ee2da0bfa9560c036f0aee24eac818305c5b2b6 in / 
# Wed, 27 Oct 2021 17:41:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3f4a2ae691cff8edfecb1d55dc2d2b5a308572dbba3c2d79fc65547c60d35752`  
		Last Modified: Wed, 27 Oct 2021 17:42:26 GMT  
		Size: 893.9 KB (893870 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `busybox:1-uclibc`

```console
$ docker pull busybox@sha256:ae11b37e026fe97c34657514b4de97bf18712777cc5de20eac9ccfb1972bf24f
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 7
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; riscv64

### `busybox:1-uclibc` - linux; amd64

```console
$ docker pull busybox@sha256:6066ca124f8c2686b7ae71aa1d6583b28c6dc3df3bdc386f2c89b92162c597d9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **772.8 KB (772798 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cabb9f684f8ba3edb303d578bfd7d709d853539ea1b420a3f6c81a08e85bb3d7`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:19:45 GMT
ADD file:88b76d15f53403ca217f156c3fa01df961ddf224a193cf39ebe51678b77cf9cf in / 
# Wed, 27 Oct 2021 17:19:45 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:01c2cdc137396a9b78be86c47e5773388e6eed953dec79e257777518134b45f1`  
		Last Modified: Wed, 27 Oct 2021 17:20:27 GMT  
		Size: 772.8 KB (772798 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1-uclibc` - linux; arm variant v5

```console
$ docker pull busybox@sha256:399e1e4a0d587717dc9e3a85150cec8498cb6dc73dcb7eddb94959fedb331104
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **756.2 KB (756158 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f73013f2ba16094e6beade24ce90aa5fcd023baff78a1438c7221592d922d780`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:48:26 GMT
ADD file:c9324c40aae3b3e81b9315fe32797d78d508fb1823de84f7cd1d79e474418a8a in / 
# Wed, 27 Oct 2021 17:48:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3a7a3789f9861a9ab57dd6819c9dcdfcc9a4ea3172b32c88423f9630fb2c0816`  
		Last Modified: Wed, 27 Oct 2021 17:50:10 GMT  
		Size: 756.2 KB (756158 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1-uclibc` - linux; arm variant v7

```console
$ docker pull busybox@sha256:53c212bcc0501f011c232df0fb6c837651d0b2f3257b6478a50c0e006b0dabc5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **724.2 KB (724238 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:17d83c72aaa7f419b771dad67eb87d285faa57f1b0d93ed2c9b5b45ea6879065`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 16:57:33 GMT
ADD file:2b15d7b82fc9c84090d98a80296a522d922e42b63fbd9ea47aa2bf62c14aa768 in / 
# Wed, 27 Oct 2021 16:57:33 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:da72ebe61afbc4deef433f5180ccb61df896dfb324e08c0a03b7c43a7c0347b4`  
		Last Modified: Wed, 27 Oct 2021 17:00:02 GMT  
		Size: 724.2 KB (724238 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1-uclibc` - linux; arm64 variant v8

```console
$ docker pull busybox@sha256:ce53e9b0310447d0e851ff0d2c9b90f358dbffe719a723147e84b93a4799396c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **828.5 KB (828502 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cc54699a1d7e60dedfeabdf0e0fee681842a16a038ae551ad93b923457b031ab`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:39:27 GMT
ADD file:0e9e223690df05d052434f07b187d500e9544cf4a10992cb1cf17bbf0f9d261d in / 
# Wed, 27 Oct 2021 17:39:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:8ba530ca112c38dd01fa8c96a709f82e5e8cbb425af71f1185efc9ed2c1561d6`  
		Last Modified: Wed, 27 Oct 2021 17:40:27 GMT  
		Size: 828.5 KB (828502 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1-uclibc` - linux; 386

```console
$ docker pull busybox@sha256:c71cb4f7e8ececaffb34037c2637dc86820e4185100e18b4d02d613a9bd772af
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **728.2 KB (728191 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:046e74e02754f28137f587f303f9852c82f3ec76fff1f2f278655b3e1193ad3e`
-	Default Command: `["sh"]`

```dockerfile
# Tue, 14 Sep 2021 01:38:33 GMT
ADD file:4143b23f3d54e871d1a69c3518704e9e6ebed52dcc278b5f1dbcd11343940898 in / 
# Tue, 14 Sep 2021 01:38:33 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:f835a1acb88b23855be36becf1ebe0a310ab5761b04c3fb24d0c94c41f7c3e41`  
		Last Modified: Tue, 14 Sep 2021 01:40:49 GMT  
		Size: 728.2 KB (728191 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1-uclibc` - linux; mips64le

```console
$ docker pull busybox@sha256:77df281071dd7e01972ec2c4a33c1a6c00d13b24238375fd6622fce97f622fa2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **960.5 KB (960459 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:572b466fbb42b288f43e2d90cf55e26d33cbdd19d667aa31dd20c2628e344f38`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:07:29 GMT
ADD file:82547d0ac8de48ae2eb3f40f43a0e1635155361fc9e20b223e87d9de0ca27e96 in / 
# Wed, 27 Oct 2021 17:07:29 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3d8d4c508e7c9189254e23bfdd5f30c93223516f57715ee5bd0cc85b94d849ee`  
		Last Modified: Wed, 27 Oct 2021 17:07:59 GMT  
		Size: 960.5 KB (960459 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1-uclibc` - linux; riscv64

```console
$ docker pull busybox@sha256:06b206c1f1a38094697c7e8bf868f9d326e56a256bc516dbb8ff0ee9c1178999
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **905.3 KB (905306 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a021c599555f76331fddec6e6ecc87cfb92cfd26ab42ef0b968dacb9890cefdc`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:12:42 GMT
ADD file:bbfcdc6a5eda2a3b7965fc2b73a783bf34893ee2cced227315f8468489ec1dee in / 
# Wed, 27 Oct 2021 17:12:43 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:0b2bb6a84cb70a09486f5568f13ac57db414fb39197ce05043689bdeae75744e`  
		Last Modified: Wed, 27 Oct 2021 17:23:32 GMT  
		Size: 905.3 KB (905306 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `busybox:1.34`

```console
$ docker pull busybox@sha256:864b0cf2b6c0e575f62fadda2e70f73219ed2dc606dbfbfa334af0a44e220af9
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 10
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v6
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; riscv64
	-	linux; s390x

### `busybox:1.34` - linux; amd64

```console
$ docker pull busybox@sha256:6066ca124f8c2686b7ae71aa1d6583b28c6dc3df3bdc386f2c89b92162c597d9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **772.8 KB (772798 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cabb9f684f8ba3edb303d578bfd7d709d853539ea1b420a3f6c81a08e85bb3d7`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:19:45 GMT
ADD file:88b76d15f53403ca217f156c3fa01df961ddf224a193cf39ebe51678b77cf9cf in / 
# Wed, 27 Oct 2021 17:19:45 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:01c2cdc137396a9b78be86c47e5773388e6eed953dec79e257777518134b45f1`  
		Last Modified: Wed, 27 Oct 2021 17:20:27 GMT  
		Size: 772.8 KB (772798 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34` - linux; arm variant v5

```console
$ docker pull busybox@sha256:399e1e4a0d587717dc9e3a85150cec8498cb6dc73dcb7eddb94959fedb331104
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **756.2 KB (756158 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f73013f2ba16094e6beade24ce90aa5fcd023baff78a1438c7221592d922d780`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:48:26 GMT
ADD file:c9324c40aae3b3e81b9315fe32797d78d508fb1823de84f7cd1d79e474418a8a in / 
# Wed, 27 Oct 2021 17:48:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3a7a3789f9861a9ab57dd6819c9dcdfcc9a4ea3172b32c88423f9630fb2c0816`  
		Last Modified: Wed, 27 Oct 2021 17:50:10 GMT  
		Size: 756.2 KB (756158 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34` - linux; arm variant v6

```console
$ docker pull busybox@sha256:4ecc3dc2e06a24df931cb719c3784611d15721c3cb64ab069141071b73f6598b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **952.5 KB (952546 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:327d97ae18db017776599d9b3713503c03974d14346adb99117a186bed6a4d47`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:49:25 GMT
ADD file:c9029a61e764f7eee2bb75e9c227970c7a9e7e81d2d2b2370b7cfa8cff8dddde in / 
# Wed, 27 Oct 2021 17:49:26 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:dac1aaad9ca43e5b6a33470c2cfc6cf3127115e539cae8bc865a8c99d9232727`  
		Last Modified: Wed, 27 Oct 2021 17:50:33 GMT  
		Size: 952.5 KB (952546 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34` - linux; arm variant v7

```console
$ docker pull busybox@sha256:53c212bcc0501f011c232df0fb6c837651d0b2f3257b6478a50c0e006b0dabc5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **724.2 KB (724238 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:17d83c72aaa7f419b771dad67eb87d285faa57f1b0d93ed2c9b5b45ea6879065`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 16:57:33 GMT
ADD file:2b15d7b82fc9c84090d98a80296a522d922e42b63fbd9ea47aa2bf62c14aa768 in / 
# Wed, 27 Oct 2021 16:57:33 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:da72ebe61afbc4deef433f5180ccb61df896dfb324e08c0a03b7c43a7c0347b4`  
		Last Modified: Wed, 27 Oct 2021 17:00:02 GMT  
		Size: 724.2 KB (724238 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34` - linux; arm64 variant v8

```console
$ docker pull busybox@sha256:ce53e9b0310447d0e851ff0d2c9b90f358dbffe719a723147e84b93a4799396c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **828.5 KB (828502 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cc54699a1d7e60dedfeabdf0e0fee681842a16a038ae551ad93b923457b031ab`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:39:27 GMT
ADD file:0e9e223690df05d052434f07b187d500e9544cf4a10992cb1cf17bbf0f9d261d in / 
# Wed, 27 Oct 2021 17:39:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:8ba530ca112c38dd01fa8c96a709f82e5e8cbb425af71f1185efc9ed2c1561d6`  
		Last Modified: Wed, 27 Oct 2021 17:40:27 GMT  
		Size: 828.5 KB (828502 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34` - linux; 386

```console
$ docker pull busybox@sha256:c310b030e2f503dd0257ef83af9f375f8f8560f88e650cd2ab0057dc8ee49754
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **736.1 KB (736144 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b8432ccdb44def8db2316470c8ae44a35631a768371dcd0ff306ba4089a284fe`
-	Default Command: `["sh"]`

```dockerfile
# Tue, 14 Sep 2021 01:39:07 GMT
ADD file:7112d87025288b110a74b76fcc41abdd1e5d0644384d36d9c9c77aa977b68edf in / 
# Tue, 14 Sep 2021 01:39:07 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:b9adb0785fca325248aab55c34eaf9a5ad0b44445c8ca1ff6dd198f0e75f6f9e`  
		Last Modified: Tue, 14 Sep 2021 01:42:08 GMT  
		Size: 736.1 KB (736144 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34` - linux; mips64le

```console
$ docker pull busybox@sha256:77df281071dd7e01972ec2c4a33c1a6c00d13b24238375fd6622fce97f622fa2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **960.5 KB (960459 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:572b466fbb42b288f43e2d90cf55e26d33cbdd19d667aa31dd20c2628e344f38`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:07:29 GMT
ADD file:82547d0ac8de48ae2eb3f40f43a0e1635155361fc9e20b223e87d9de0ca27e96 in / 
# Wed, 27 Oct 2021 17:07:29 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3d8d4c508e7c9189254e23bfdd5f30c93223516f57715ee5bd0cc85b94d849ee`  
		Last Modified: Wed, 27 Oct 2021 17:07:59 GMT  
		Size: 960.5 KB (960459 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34` - linux; ppc64le

```console
$ docker pull busybox@sha256:b70f0f45692830c2990b42f770aa29488c20ac41f1c3dcaa242920b73cb1399b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.5 MB (2484095 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:defb34012ae5b6a308b2629e4a6b299d20a193e3475fc98c4fba27f35819061b`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 19:12:06 GMT
ADD file:79fcd19d69e0c4f07016224ba1b588b8c92aa08c3af515be806d4714d6118923 in / 
# Wed, 27 Oct 2021 19:12:09 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:8dcfc896d3701ab1ec378f31cf3076493c9b8c588f103af7901ce812bb09b317`  
		Last Modified: Wed, 27 Oct 2021 19:13:34 GMT  
		Size: 2.5 MB (2484095 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34` - linux; riscv64

```console
$ docker pull busybox@sha256:06b206c1f1a38094697c7e8bf868f9d326e56a256bc516dbb8ff0ee9c1178999
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **905.3 KB (905306 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a021c599555f76331fddec6e6ecc87cfb92cfd26ab42ef0b968dacb9890cefdc`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:12:42 GMT
ADD file:bbfcdc6a5eda2a3b7965fc2b73a783bf34893ee2cced227315f8468489ec1dee in / 
# Wed, 27 Oct 2021 17:12:43 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:0b2bb6a84cb70a09486f5568f13ac57db414fb39197ce05043689bdeae75744e`  
		Last Modified: Wed, 27 Oct 2021 17:23:32 GMT  
		Size: 905.3 KB (905306 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34` - linux; s390x

```console
$ docker pull busybox@sha256:86824a27910bd2a8c6a8478fe99206e6cf4bcada7cb8435c0060cbe885559e53
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.0 MB (2006829 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9e119a54a2534d8ae95d15840f3c4feed03866eb4aee947c49e679206be94893`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:41:19 GMT
ADD file:09207d85b622b42ab9880ec4cbfe2212390ba7c1b0704b91bd3db041db9d9e03 in / 
# Wed, 27 Oct 2021 17:41:19 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:120da903ae50712b69209ab560f7855f3ffed8e6cd9c78dcd74b7e8754c5b168`  
		Last Modified: Wed, 27 Oct 2021 17:42:10 GMT  
		Size: 2.0 MB (2006829 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `busybox:1.34-glibc`

```console
$ docker pull busybox@sha256:0c7b5d6ed481504f3cca796a2cf0031acefbf0686637c7f4426f18693a3a8b6a
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `busybox:1.34-glibc` - linux; amd64

```console
$ docker pull busybox@sha256:a158839ba99a90008ddd922dddb869d4a998f60339ee0c41ef9009e60995247a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2566111 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f17f5d94ca617134e143b569c2ec48a7d5864d8387d1191e03116c7ad428bfdf`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:19:53 GMT
ADD file:05820ca2a136d0bd67d2c84ac5294fb0b112df68fcd3482ed77f4d47d30c2106 in / 
# Wed, 27 Oct 2021 17:19:53 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:df20d59888d8cd750954e0854d27df703b73b98999981beeb0ec769c8a707226`  
		Last Modified: Wed, 27 Oct 2021 17:20:43 GMT  
		Size: 2.6 MB (2566111 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34-glibc` - linux; arm variant v5

```console
$ docker pull busybox@sha256:068abb3d195234a48605f5067881edfa228840ea5c83245d61e3798979c02961
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **1.9 MB (1930775 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:454a6225af3074772111413ba8b568c07d177f2475e59cbbe6d909156742023f`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:48:41 GMT
ADD file:7bff807316ba841e7e1c392a909bc4d4ab078c4e0ee9e99e62bd89c5002bbc21 in / 
# Wed, 27 Oct 2021 17:48:41 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:58e53cd44176b2564294ddc75394d3683931c011e7c2db9794e90ef796228270`  
		Last Modified: Wed, 27 Oct 2021 17:50:34 GMT  
		Size: 1.9 MB (1930775 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34-glibc` - linux; arm variant v7

```console
$ docker pull busybox@sha256:3c56c483948e49726761f4bf2ae09cf227db38d75362ab40024cfc7deda4b04f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **1.7 MB (1678694 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:393a97107921f708c9df011fd1841f3a1c34a1de86c96cd9ef6033c9708f1f11`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 16:57:50 GMT
ADD file:a8b26211fe41f82dec07ffe639f4c6ef4880076f74d8b5f0eb59e34546b325e9 in / 
# Wed, 27 Oct 2021 16:57:50 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:fa418c43645700e527bff2dfb63459af17af7b059c31f60f412699e48fe122a2`  
		Last Modified: Wed, 27 Oct 2021 17:00:27 GMT  
		Size: 1.7 MB (1678694 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34-glibc` - linux; arm64 variant v8

```console
$ docker pull busybox@sha256:a72f19c71777a59bf2ea290ceefbe6537e2c8c544463cd57859ef5d432fb244b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.0 MB (1996159 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6fb1464c3dc04a6f8a7c92cdb9f3dfc5ab1c9116061a85ddc90825680be154eb`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:39:34 GMT
ADD file:8bdeb927d5560194f8ba5688d4d9ee1ff7254928145aafe79f7aa326f70ab234 in / 
# Wed, 27 Oct 2021 17:39:34 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:aae080e05ab28f73910de7f8e9f092f5c7eb9f2658363454a3361e69e5723954`  
		Last Modified: Wed, 27 Oct 2021 17:40:46 GMT  
		Size: 2.0 MB (1996159 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34-glibc` - linux; 386

```console
$ docker pull busybox@sha256:26710180ebee340e96d5aa3efc731379c12b4347eed4d3c17cd1dead5946f8df
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.2 MB (2233835 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:289c3971c7e1bb7f580a37d0e078b15b86da9176a08c02d8374f48557f2ddcdf`
-	Default Command: `["sh"]`

```dockerfile
# Tue, 14 Sep 2021 01:39:16 GMT
ADD file:1b7fcbec3550909892fc281335756959253000d1ab671dbffcca94e4b9b4607d in / 
# Tue, 14 Sep 2021 01:39:16 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:7cef1eb86d0e136d57a0416a4d28f8c0421b949f01ec837694d2ca3f7979384f`  
		Last Modified: Tue, 14 Sep 2021 01:42:23 GMT  
		Size: 2.2 MB (2233835 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34-glibc` - linux; mips64le

```console
$ docker pull busybox@sha256:6a2a782c46dedb94c1370a60192df26e00da690b90fa16cb4eb11352e45ec665
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.2 MB (2220711 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:25bbb10f71e32f9c63967dfd90d2213df5f3c8ef303bb47171f3920bb066ae7f`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:07:38 GMT
ADD file:47a6e39f530c1c6dcfc7fbcce83900c83627949ac78e6a74132145f11b427b59 in / 
# Wed, 27 Oct 2021 17:07:38 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:b7954f921f64e29ca6451787b44b6ed22eb7177760e8d8fa31f60c312d7ae676`  
		Last Modified: Wed, 27 Oct 2021 17:08:17 GMT  
		Size: 2.2 MB (2220711 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34-glibc` - linux; ppc64le

```console
$ docker pull busybox@sha256:b70f0f45692830c2990b42f770aa29488c20ac41f1c3dcaa242920b73cb1399b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.5 MB (2484095 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:defb34012ae5b6a308b2629e4a6b299d20a193e3475fc98c4fba27f35819061b`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 19:12:06 GMT
ADD file:79fcd19d69e0c4f07016224ba1b588b8c92aa08c3af515be806d4714d6118923 in / 
# Wed, 27 Oct 2021 19:12:09 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:8dcfc896d3701ab1ec378f31cf3076493c9b8c588f103af7901ce812bb09b317`  
		Last Modified: Wed, 27 Oct 2021 19:13:34 GMT  
		Size: 2.5 MB (2484095 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34-glibc` - linux; s390x

```console
$ docker pull busybox@sha256:86824a27910bd2a8c6a8478fe99206e6cf4bcada7cb8435c0060cbe885559e53
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.0 MB (2006829 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9e119a54a2534d8ae95d15840f3c4feed03866eb4aee947c49e679206be94893`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:41:19 GMT
ADD file:09207d85b622b42ab9880ec4cbfe2212390ba7c1b0704b91bd3db041db9d9e03 in / 
# Wed, 27 Oct 2021 17:41:19 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:120da903ae50712b69209ab560f7855f3ffed8e6cd9c78dcd74b7e8754c5b168`  
		Last Modified: Wed, 27 Oct 2021 17:42:10 GMT  
		Size: 2.0 MB (2006829 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `busybox:1.34-musl`

```console
$ docker pull busybox@sha256:19816977774693498cda015a8bfe00f964644f365b07eda225c0e08fe95cc351
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 7
	-	linux; amd64
	-	linux; arm variant v6
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; ppc64le
	-	linux; s390x

### `busybox:1.34-musl` - linux; amd64

```console
$ docker pull busybox@sha256:c57be995c4c3bb22b723c49d74fc3b5a4510dfbd8be28b5d021d7a4a53a4bab1
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **843.3 KB (843266 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1b408f772ab08633f27bf8918481dfd3fe2f7689b23a50897cb7cef2e17a6cb8`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:19:58 GMT
ADD file:58dbb8135e92be6f243e74462af8e85fd67417ba0281b81ea7742af344794c3f in / 
# Wed, 27 Oct 2021 17:19:58 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:b0e194f8faadf770ca46f1caf5b5ce0bb82ef76806729e22c1a20aff0c29c1f6`  
		Last Modified: Wed, 27 Oct 2021 17:20:59 GMT  
		Size: 843.3 KB (843266 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34-musl` - linux; arm variant v6

```console
$ docker pull busybox@sha256:4ecc3dc2e06a24df931cb719c3784611d15721c3cb64ab069141071b73f6598b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **952.5 KB (952546 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:327d97ae18db017776599d9b3713503c03974d14346adb99117a186bed6a4d47`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:49:25 GMT
ADD file:c9029a61e764f7eee2bb75e9c227970c7a9e7e81d2d2b2370b7cfa8cff8dddde in / 
# Wed, 27 Oct 2021 17:49:26 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:dac1aaad9ca43e5b6a33470c2cfc6cf3127115e539cae8bc865a8c99d9232727`  
		Last Modified: Wed, 27 Oct 2021 17:50:33 GMT  
		Size: 952.5 KB (952546 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34-musl` - linux; arm variant v7

```console
$ docker pull busybox@sha256:e23f636436b365735d7c726df3e36e48fe309afaa422a503bd8ecf9740742f9c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **834.7 KB (834677 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:27fba482d17b4205fe8a01218cc62d830b45f0a1e9ee13eeb81cf2cd7a69390d`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 16:58:07 GMT
ADD file:242e49f9869dfe3d8b59aee3801ef14dac3c58176f6cea670614284afd50ce8d in / 
# Wed, 27 Oct 2021 16:58:08 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:9d172075913815538a77dd7a7ad3bbefc76474354936eddd73cd933e9776b8e2`  
		Last Modified: Wed, 27 Oct 2021 17:00:50 GMT  
		Size: 834.7 KB (834677 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34-musl` - linux; arm64 variant v8

```console
$ docker pull busybox@sha256:976ce0d1af7874bc0216c19efbd9178cc0455513a963cd834a35b9e732e298fb
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **882.2 KB (882176 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:69552745f9d2985d9bb00c5d912723a5f4a9afd133966cf8982b06067a3f7cf7`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:39:41 GMT
ADD file:4562e1c40e298277fc65259e3b29d3b72f89a28088f94bcae8e543df2ede1f5d in / 
# Wed, 27 Oct 2021 17:39:41 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:20a2e0c8f8c2704a1cd5560c3a242607af57c42ae6a3109108be0303d32092b1`  
		Last Modified: Wed, 27 Oct 2021 17:41:05 GMT  
		Size: 882.2 KB (882176 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34-musl` - linux; 386

```console
$ docker pull busybox@sha256:ea1b8a8f1cf2cb29b28b2cf4ef4224eaba7b3738783e2bf81f4b57910428833e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **849.6 KB (849567 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:07ad4269cee396c8d9eaec82e9a42d5d745b46533a73fb10b9e909dfcfa7b367`
-	Default Command: `["sh"]`

```dockerfile
# Tue, 14 Sep 2021 01:39:23 GMT
ADD file:50d760d03fb3c5142eef5be511bf850be442152763da11c41479bfdc9da074ee in / 
# Tue, 14 Sep 2021 01:39:24 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:bf0ff580edbd1e62d0fee554499c1d034404f12f5238e0fac3564381534ac8dd`  
		Last Modified: Tue, 14 Sep 2021 01:42:37 GMT  
		Size: 849.6 KB (849567 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34-musl` - linux; ppc64le

```console
$ docker pull busybox@sha256:7696de5d912dc3db582ba08c435f8b1b01a0cf845db7e44f1ad4269b656b627a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **919.2 KB (919168 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:0d6acacaf155166a2a58e39cd33187987fe4b36400dedbe22607902b50f3793d`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 19:12:21 GMT
ADD file:b19d93c361eebcc52af7de98cdbacec78af10e600d8cd334b9301752ba712766 in / 
# Wed, 27 Oct 2021 19:12:30 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:2abb4f53c2ba292747e959a7d205240ab584ca7650ff38823327a68680916549`  
		Last Modified: Wed, 27 Oct 2021 19:13:53 GMT  
		Size: 919.2 KB (919168 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34-musl` - linux; s390x

```console
$ docker pull busybox@sha256:2839b6e6e257d487803c6942666415c052a1e1c11e3d41d57fe30d25c50da924
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **893.9 KB (893870 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4c5d5f91ce1964dd844df1a44fba72b596f170eb0554cf45c195d2ec65849f23`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:41:26 GMT
ADD file:5722acc1a5fd74b7ff5c73c86ee2da0bfa9560c036f0aee24eac818305c5b2b6 in / 
# Wed, 27 Oct 2021 17:41:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3f4a2ae691cff8edfecb1d55dc2d2b5a308572dbba3c2d79fc65547c60d35752`  
		Last Modified: Wed, 27 Oct 2021 17:42:26 GMT  
		Size: 893.9 KB (893870 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `busybox:1.34-uclibc`

```console
$ docker pull busybox@sha256:e14e014a2a11a9508e6d1d80418362a5d38c4cf6c7ba07f02d12c4ef5b4f4f04
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 7
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; riscv64

### `busybox:1.34-uclibc` - linux; amd64

```console
$ docker pull busybox@sha256:6066ca124f8c2686b7ae71aa1d6583b28c6dc3df3bdc386f2c89b92162c597d9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **772.8 KB (772798 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cabb9f684f8ba3edb303d578bfd7d709d853539ea1b420a3f6c81a08e85bb3d7`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:19:45 GMT
ADD file:88b76d15f53403ca217f156c3fa01df961ddf224a193cf39ebe51678b77cf9cf in / 
# Wed, 27 Oct 2021 17:19:45 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:01c2cdc137396a9b78be86c47e5773388e6eed953dec79e257777518134b45f1`  
		Last Modified: Wed, 27 Oct 2021 17:20:27 GMT  
		Size: 772.8 KB (772798 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34-uclibc` - linux; arm variant v5

```console
$ docker pull busybox@sha256:399e1e4a0d587717dc9e3a85150cec8498cb6dc73dcb7eddb94959fedb331104
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **756.2 KB (756158 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f73013f2ba16094e6beade24ce90aa5fcd023baff78a1438c7221592d922d780`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:48:26 GMT
ADD file:c9324c40aae3b3e81b9315fe32797d78d508fb1823de84f7cd1d79e474418a8a in / 
# Wed, 27 Oct 2021 17:48:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3a7a3789f9861a9ab57dd6819c9dcdfcc9a4ea3172b32c88423f9630fb2c0816`  
		Last Modified: Wed, 27 Oct 2021 17:50:10 GMT  
		Size: 756.2 KB (756158 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34-uclibc` - linux; arm variant v7

```console
$ docker pull busybox@sha256:53c212bcc0501f011c232df0fb6c837651d0b2f3257b6478a50c0e006b0dabc5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **724.2 KB (724238 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:17d83c72aaa7f419b771dad67eb87d285faa57f1b0d93ed2c9b5b45ea6879065`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 16:57:33 GMT
ADD file:2b15d7b82fc9c84090d98a80296a522d922e42b63fbd9ea47aa2bf62c14aa768 in / 
# Wed, 27 Oct 2021 16:57:33 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:da72ebe61afbc4deef433f5180ccb61df896dfb324e08c0a03b7c43a7c0347b4`  
		Last Modified: Wed, 27 Oct 2021 17:00:02 GMT  
		Size: 724.2 KB (724238 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34-uclibc` - linux; arm64 variant v8

```console
$ docker pull busybox@sha256:ce53e9b0310447d0e851ff0d2c9b90f358dbffe719a723147e84b93a4799396c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **828.5 KB (828502 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cc54699a1d7e60dedfeabdf0e0fee681842a16a038ae551ad93b923457b031ab`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:39:27 GMT
ADD file:0e9e223690df05d052434f07b187d500e9544cf4a10992cb1cf17bbf0f9d261d in / 
# Wed, 27 Oct 2021 17:39:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:8ba530ca112c38dd01fa8c96a709f82e5e8cbb425af71f1185efc9ed2c1561d6`  
		Last Modified: Wed, 27 Oct 2021 17:40:27 GMT  
		Size: 828.5 KB (828502 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34-uclibc` - linux; 386

```console
$ docker pull busybox@sha256:c310b030e2f503dd0257ef83af9f375f8f8560f88e650cd2ab0057dc8ee49754
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **736.1 KB (736144 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b8432ccdb44def8db2316470c8ae44a35631a768371dcd0ff306ba4089a284fe`
-	Default Command: `["sh"]`

```dockerfile
# Tue, 14 Sep 2021 01:39:07 GMT
ADD file:7112d87025288b110a74b76fcc41abdd1e5d0644384d36d9c9c77aa977b68edf in / 
# Tue, 14 Sep 2021 01:39:07 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:b9adb0785fca325248aab55c34eaf9a5ad0b44445c8ca1ff6dd198f0e75f6f9e`  
		Last Modified: Tue, 14 Sep 2021 01:42:08 GMT  
		Size: 736.1 KB (736144 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34-uclibc` - linux; mips64le

```console
$ docker pull busybox@sha256:77df281071dd7e01972ec2c4a33c1a6c00d13b24238375fd6622fce97f622fa2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **960.5 KB (960459 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:572b466fbb42b288f43e2d90cf55e26d33cbdd19d667aa31dd20c2628e344f38`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:07:29 GMT
ADD file:82547d0ac8de48ae2eb3f40f43a0e1635155361fc9e20b223e87d9de0ca27e96 in / 
# Wed, 27 Oct 2021 17:07:29 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3d8d4c508e7c9189254e23bfdd5f30c93223516f57715ee5bd0cc85b94d849ee`  
		Last Modified: Wed, 27 Oct 2021 17:07:59 GMT  
		Size: 960.5 KB (960459 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34-uclibc` - linux; riscv64

```console
$ docker pull busybox@sha256:06b206c1f1a38094697c7e8bf868f9d326e56a256bc516dbb8ff0ee9c1178999
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **905.3 KB (905306 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a021c599555f76331fddec6e6ecc87cfb92cfd26ab42ef0b968dacb9890cefdc`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:12:42 GMT
ADD file:bbfcdc6a5eda2a3b7965fc2b73a783bf34893ee2cced227315f8468489ec1dee in / 
# Wed, 27 Oct 2021 17:12:43 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:0b2bb6a84cb70a09486f5568f13ac57db414fb39197ce05043689bdeae75744e`  
		Last Modified: Wed, 27 Oct 2021 17:23:32 GMT  
		Size: 905.3 KB (905306 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `busybox:1.34.1`

```console
$ docker pull busybox@sha256:6955be0010982829472167f0d18e190c11d0de5fe07e8b0cde168a581935cfd3
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 9
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v6
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; riscv64
	-	linux; s390x

### `busybox:1.34.1` - linux; amd64

```console
$ docker pull busybox@sha256:6066ca124f8c2686b7ae71aa1d6583b28c6dc3df3bdc386f2c89b92162c597d9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **772.8 KB (772798 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cabb9f684f8ba3edb303d578bfd7d709d853539ea1b420a3f6c81a08e85bb3d7`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:19:45 GMT
ADD file:88b76d15f53403ca217f156c3fa01df961ddf224a193cf39ebe51678b77cf9cf in / 
# Wed, 27 Oct 2021 17:19:45 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:01c2cdc137396a9b78be86c47e5773388e6eed953dec79e257777518134b45f1`  
		Last Modified: Wed, 27 Oct 2021 17:20:27 GMT  
		Size: 772.8 KB (772798 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34.1` - linux; arm variant v5

```console
$ docker pull busybox@sha256:399e1e4a0d587717dc9e3a85150cec8498cb6dc73dcb7eddb94959fedb331104
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **756.2 KB (756158 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f73013f2ba16094e6beade24ce90aa5fcd023baff78a1438c7221592d922d780`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:48:26 GMT
ADD file:c9324c40aae3b3e81b9315fe32797d78d508fb1823de84f7cd1d79e474418a8a in / 
# Wed, 27 Oct 2021 17:48:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3a7a3789f9861a9ab57dd6819c9dcdfcc9a4ea3172b32c88423f9630fb2c0816`  
		Last Modified: Wed, 27 Oct 2021 17:50:10 GMT  
		Size: 756.2 KB (756158 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34.1` - linux; arm variant v6

```console
$ docker pull busybox@sha256:4ecc3dc2e06a24df931cb719c3784611d15721c3cb64ab069141071b73f6598b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **952.5 KB (952546 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:327d97ae18db017776599d9b3713503c03974d14346adb99117a186bed6a4d47`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:49:25 GMT
ADD file:c9029a61e764f7eee2bb75e9c227970c7a9e7e81d2d2b2370b7cfa8cff8dddde in / 
# Wed, 27 Oct 2021 17:49:26 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:dac1aaad9ca43e5b6a33470c2cfc6cf3127115e539cae8bc865a8c99d9232727`  
		Last Modified: Wed, 27 Oct 2021 17:50:33 GMT  
		Size: 952.5 KB (952546 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34.1` - linux; arm variant v7

```console
$ docker pull busybox@sha256:53c212bcc0501f011c232df0fb6c837651d0b2f3257b6478a50c0e006b0dabc5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **724.2 KB (724238 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:17d83c72aaa7f419b771dad67eb87d285faa57f1b0d93ed2c9b5b45ea6879065`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 16:57:33 GMT
ADD file:2b15d7b82fc9c84090d98a80296a522d922e42b63fbd9ea47aa2bf62c14aa768 in / 
# Wed, 27 Oct 2021 16:57:33 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:da72ebe61afbc4deef433f5180ccb61df896dfb324e08c0a03b7c43a7c0347b4`  
		Last Modified: Wed, 27 Oct 2021 17:00:02 GMT  
		Size: 724.2 KB (724238 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34.1` - linux; arm64 variant v8

```console
$ docker pull busybox@sha256:ce53e9b0310447d0e851ff0d2c9b90f358dbffe719a723147e84b93a4799396c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **828.5 KB (828502 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cc54699a1d7e60dedfeabdf0e0fee681842a16a038ae551ad93b923457b031ab`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:39:27 GMT
ADD file:0e9e223690df05d052434f07b187d500e9544cf4a10992cb1cf17bbf0f9d261d in / 
# Wed, 27 Oct 2021 17:39:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:8ba530ca112c38dd01fa8c96a709f82e5e8cbb425af71f1185efc9ed2c1561d6`  
		Last Modified: Wed, 27 Oct 2021 17:40:27 GMT  
		Size: 828.5 KB (828502 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34.1` - linux; mips64le

```console
$ docker pull busybox@sha256:77df281071dd7e01972ec2c4a33c1a6c00d13b24238375fd6622fce97f622fa2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **960.5 KB (960459 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:572b466fbb42b288f43e2d90cf55e26d33cbdd19d667aa31dd20c2628e344f38`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:07:29 GMT
ADD file:82547d0ac8de48ae2eb3f40f43a0e1635155361fc9e20b223e87d9de0ca27e96 in / 
# Wed, 27 Oct 2021 17:07:29 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3d8d4c508e7c9189254e23bfdd5f30c93223516f57715ee5bd0cc85b94d849ee`  
		Last Modified: Wed, 27 Oct 2021 17:07:59 GMT  
		Size: 960.5 KB (960459 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34.1` - linux; ppc64le

```console
$ docker pull busybox@sha256:b70f0f45692830c2990b42f770aa29488c20ac41f1c3dcaa242920b73cb1399b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.5 MB (2484095 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:defb34012ae5b6a308b2629e4a6b299d20a193e3475fc98c4fba27f35819061b`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 19:12:06 GMT
ADD file:79fcd19d69e0c4f07016224ba1b588b8c92aa08c3af515be806d4714d6118923 in / 
# Wed, 27 Oct 2021 19:12:09 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:8dcfc896d3701ab1ec378f31cf3076493c9b8c588f103af7901ce812bb09b317`  
		Last Modified: Wed, 27 Oct 2021 19:13:34 GMT  
		Size: 2.5 MB (2484095 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34.1` - linux; riscv64

```console
$ docker pull busybox@sha256:06b206c1f1a38094697c7e8bf868f9d326e56a256bc516dbb8ff0ee9c1178999
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **905.3 KB (905306 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a021c599555f76331fddec6e6ecc87cfb92cfd26ab42ef0b968dacb9890cefdc`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:12:42 GMT
ADD file:bbfcdc6a5eda2a3b7965fc2b73a783bf34893ee2cced227315f8468489ec1dee in / 
# Wed, 27 Oct 2021 17:12:43 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:0b2bb6a84cb70a09486f5568f13ac57db414fb39197ce05043689bdeae75744e`  
		Last Modified: Wed, 27 Oct 2021 17:23:32 GMT  
		Size: 905.3 KB (905306 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34.1` - linux; s390x

```console
$ docker pull busybox@sha256:86824a27910bd2a8c6a8478fe99206e6cf4bcada7cb8435c0060cbe885559e53
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.0 MB (2006829 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9e119a54a2534d8ae95d15840f3c4feed03866eb4aee947c49e679206be94893`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:41:19 GMT
ADD file:09207d85b622b42ab9880ec4cbfe2212390ba7c1b0704b91bd3db041db9d9e03 in / 
# Wed, 27 Oct 2021 17:41:19 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:120da903ae50712b69209ab560f7855f3ffed8e6cd9c78dcd74b7e8754c5b168`  
		Last Modified: Wed, 27 Oct 2021 17:42:10 GMT  
		Size: 2.0 MB (2006829 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `busybox:1.34.1-glibc`

```console
$ docker pull busybox@sha256:576f1fc34b9b72905e73d9bae15a98da3daafaf4a9967840e3930b2fccfc77bf
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 7
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `busybox:1.34.1-glibc` - linux; amd64

```console
$ docker pull busybox@sha256:a158839ba99a90008ddd922dddb869d4a998f60339ee0c41ef9009e60995247a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2566111 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f17f5d94ca617134e143b569c2ec48a7d5864d8387d1191e03116c7ad428bfdf`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:19:53 GMT
ADD file:05820ca2a136d0bd67d2c84ac5294fb0b112df68fcd3482ed77f4d47d30c2106 in / 
# Wed, 27 Oct 2021 17:19:53 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:df20d59888d8cd750954e0854d27df703b73b98999981beeb0ec769c8a707226`  
		Last Modified: Wed, 27 Oct 2021 17:20:43 GMT  
		Size: 2.6 MB (2566111 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34.1-glibc` - linux; arm variant v5

```console
$ docker pull busybox@sha256:068abb3d195234a48605f5067881edfa228840ea5c83245d61e3798979c02961
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **1.9 MB (1930775 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:454a6225af3074772111413ba8b568c07d177f2475e59cbbe6d909156742023f`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:48:41 GMT
ADD file:7bff807316ba841e7e1c392a909bc4d4ab078c4e0ee9e99e62bd89c5002bbc21 in / 
# Wed, 27 Oct 2021 17:48:41 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:58e53cd44176b2564294ddc75394d3683931c011e7c2db9794e90ef796228270`  
		Last Modified: Wed, 27 Oct 2021 17:50:34 GMT  
		Size: 1.9 MB (1930775 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34.1-glibc` - linux; arm variant v7

```console
$ docker pull busybox@sha256:3c56c483948e49726761f4bf2ae09cf227db38d75362ab40024cfc7deda4b04f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **1.7 MB (1678694 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:393a97107921f708c9df011fd1841f3a1c34a1de86c96cd9ef6033c9708f1f11`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 16:57:50 GMT
ADD file:a8b26211fe41f82dec07ffe639f4c6ef4880076f74d8b5f0eb59e34546b325e9 in / 
# Wed, 27 Oct 2021 16:57:50 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:fa418c43645700e527bff2dfb63459af17af7b059c31f60f412699e48fe122a2`  
		Last Modified: Wed, 27 Oct 2021 17:00:27 GMT  
		Size: 1.7 MB (1678694 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34.1-glibc` - linux; arm64 variant v8

```console
$ docker pull busybox@sha256:a72f19c71777a59bf2ea290ceefbe6537e2c8c544463cd57859ef5d432fb244b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.0 MB (1996159 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6fb1464c3dc04a6f8a7c92cdb9f3dfc5ab1c9116061a85ddc90825680be154eb`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:39:34 GMT
ADD file:8bdeb927d5560194f8ba5688d4d9ee1ff7254928145aafe79f7aa326f70ab234 in / 
# Wed, 27 Oct 2021 17:39:34 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:aae080e05ab28f73910de7f8e9f092f5c7eb9f2658363454a3361e69e5723954`  
		Last Modified: Wed, 27 Oct 2021 17:40:46 GMT  
		Size: 2.0 MB (1996159 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34.1-glibc` - linux; mips64le

```console
$ docker pull busybox@sha256:6a2a782c46dedb94c1370a60192df26e00da690b90fa16cb4eb11352e45ec665
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.2 MB (2220711 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:25bbb10f71e32f9c63967dfd90d2213df5f3c8ef303bb47171f3920bb066ae7f`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:07:38 GMT
ADD file:47a6e39f530c1c6dcfc7fbcce83900c83627949ac78e6a74132145f11b427b59 in / 
# Wed, 27 Oct 2021 17:07:38 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:b7954f921f64e29ca6451787b44b6ed22eb7177760e8d8fa31f60c312d7ae676`  
		Last Modified: Wed, 27 Oct 2021 17:08:17 GMT  
		Size: 2.2 MB (2220711 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34.1-glibc` - linux; ppc64le

```console
$ docker pull busybox@sha256:b70f0f45692830c2990b42f770aa29488c20ac41f1c3dcaa242920b73cb1399b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.5 MB (2484095 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:defb34012ae5b6a308b2629e4a6b299d20a193e3475fc98c4fba27f35819061b`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 19:12:06 GMT
ADD file:79fcd19d69e0c4f07016224ba1b588b8c92aa08c3af515be806d4714d6118923 in / 
# Wed, 27 Oct 2021 19:12:09 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:8dcfc896d3701ab1ec378f31cf3076493c9b8c588f103af7901ce812bb09b317`  
		Last Modified: Wed, 27 Oct 2021 19:13:34 GMT  
		Size: 2.5 MB (2484095 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34.1-glibc` - linux; s390x

```console
$ docker pull busybox@sha256:86824a27910bd2a8c6a8478fe99206e6cf4bcada7cb8435c0060cbe885559e53
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.0 MB (2006829 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9e119a54a2534d8ae95d15840f3c4feed03866eb4aee947c49e679206be94893`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:41:19 GMT
ADD file:09207d85b622b42ab9880ec4cbfe2212390ba7c1b0704b91bd3db041db9d9e03 in / 
# Wed, 27 Oct 2021 17:41:19 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:120da903ae50712b69209ab560f7855f3ffed8e6cd9c78dcd74b7e8754c5b168`  
		Last Modified: Wed, 27 Oct 2021 17:42:10 GMT  
		Size: 2.0 MB (2006829 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `busybox:1.34.1-musl`

```console
$ docker pull busybox@sha256:363b6b396fcbb01b7eba6a29fdc917729515de568ec678d86dd06ddf5f8fcd01
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 6
	-	linux; amd64
	-	linux; arm variant v6
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; ppc64le
	-	linux; s390x

### `busybox:1.34.1-musl` - linux; amd64

```console
$ docker pull busybox@sha256:c57be995c4c3bb22b723c49d74fc3b5a4510dfbd8be28b5d021d7a4a53a4bab1
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **843.3 KB (843266 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1b408f772ab08633f27bf8918481dfd3fe2f7689b23a50897cb7cef2e17a6cb8`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:19:58 GMT
ADD file:58dbb8135e92be6f243e74462af8e85fd67417ba0281b81ea7742af344794c3f in / 
# Wed, 27 Oct 2021 17:19:58 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:b0e194f8faadf770ca46f1caf5b5ce0bb82ef76806729e22c1a20aff0c29c1f6`  
		Last Modified: Wed, 27 Oct 2021 17:20:59 GMT  
		Size: 843.3 KB (843266 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34.1-musl` - linux; arm variant v6

```console
$ docker pull busybox@sha256:4ecc3dc2e06a24df931cb719c3784611d15721c3cb64ab069141071b73f6598b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **952.5 KB (952546 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:327d97ae18db017776599d9b3713503c03974d14346adb99117a186bed6a4d47`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:49:25 GMT
ADD file:c9029a61e764f7eee2bb75e9c227970c7a9e7e81d2d2b2370b7cfa8cff8dddde in / 
# Wed, 27 Oct 2021 17:49:26 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:dac1aaad9ca43e5b6a33470c2cfc6cf3127115e539cae8bc865a8c99d9232727`  
		Last Modified: Wed, 27 Oct 2021 17:50:33 GMT  
		Size: 952.5 KB (952546 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34.1-musl` - linux; arm variant v7

```console
$ docker pull busybox@sha256:e23f636436b365735d7c726df3e36e48fe309afaa422a503bd8ecf9740742f9c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **834.7 KB (834677 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:27fba482d17b4205fe8a01218cc62d830b45f0a1e9ee13eeb81cf2cd7a69390d`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 16:58:07 GMT
ADD file:242e49f9869dfe3d8b59aee3801ef14dac3c58176f6cea670614284afd50ce8d in / 
# Wed, 27 Oct 2021 16:58:08 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:9d172075913815538a77dd7a7ad3bbefc76474354936eddd73cd933e9776b8e2`  
		Last Modified: Wed, 27 Oct 2021 17:00:50 GMT  
		Size: 834.7 KB (834677 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34.1-musl` - linux; arm64 variant v8

```console
$ docker pull busybox@sha256:976ce0d1af7874bc0216c19efbd9178cc0455513a963cd834a35b9e732e298fb
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **882.2 KB (882176 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:69552745f9d2985d9bb00c5d912723a5f4a9afd133966cf8982b06067a3f7cf7`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:39:41 GMT
ADD file:4562e1c40e298277fc65259e3b29d3b72f89a28088f94bcae8e543df2ede1f5d in / 
# Wed, 27 Oct 2021 17:39:41 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:20a2e0c8f8c2704a1cd5560c3a242607af57c42ae6a3109108be0303d32092b1`  
		Last Modified: Wed, 27 Oct 2021 17:41:05 GMT  
		Size: 882.2 KB (882176 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34.1-musl` - linux; ppc64le

```console
$ docker pull busybox@sha256:7696de5d912dc3db582ba08c435f8b1b01a0cf845db7e44f1ad4269b656b627a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **919.2 KB (919168 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:0d6acacaf155166a2a58e39cd33187987fe4b36400dedbe22607902b50f3793d`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 19:12:21 GMT
ADD file:b19d93c361eebcc52af7de98cdbacec78af10e600d8cd334b9301752ba712766 in / 
# Wed, 27 Oct 2021 19:12:30 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:2abb4f53c2ba292747e959a7d205240ab584ca7650ff38823327a68680916549`  
		Last Modified: Wed, 27 Oct 2021 19:13:53 GMT  
		Size: 919.2 KB (919168 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34.1-musl` - linux; s390x

```console
$ docker pull busybox@sha256:2839b6e6e257d487803c6942666415c052a1e1c11e3d41d57fe30d25c50da924
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **893.9 KB (893870 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4c5d5f91ce1964dd844df1a44fba72b596f170eb0554cf45c195d2ec65849f23`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:41:26 GMT
ADD file:5722acc1a5fd74b7ff5c73c86ee2da0bfa9560c036f0aee24eac818305c5b2b6 in / 
# Wed, 27 Oct 2021 17:41:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3f4a2ae691cff8edfecb1d55dc2d2b5a308572dbba3c2d79fc65547c60d35752`  
		Last Modified: Wed, 27 Oct 2021 17:42:26 GMT  
		Size: 893.9 KB (893870 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `busybox:1.34.1-uclibc`

```console
$ docker pull busybox@sha256:727a688a1d86d277a6323f18cb1d47777455b5a39edcb4346fb79601798ce6a7
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 6
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; mips64le
	-	linux; riscv64

### `busybox:1.34.1-uclibc` - linux; amd64

```console
$ docker pull busybox@sha256:6066ca124f8c2686b7ae71aa1d6583b28c6dc3df3bdc386f2c89b92162c597d9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **772.8 KB (772798 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cabb9f684f8ba3edb303d578bfd7d709d853539ea1b420a3f6c81a08e85bb3d7`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:19:45 GMT
ADD file:88b76d15f53403ca217f156c3fa01df961ddf224a193cf39ebe51678b77cf9cf in / 
# Wed, 27 Oct 2021 17:19:45 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:01c2cdc137396a9b78be86c47e5773388e6eed953dec79e257777518134b45f1`  
		Last Modified: Wed, 27 Oct 2021 17:20:27 GMT  
		Size: 772.8 KB (772798 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34.1-uclibc` - linux; arm variant v5

```console
$ docker pull busybox@sha256:399e1e4a0d587717dc9e3a85150cec8498cb6dc73dcb7eddb94959fedb331104
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **756.2 KB (756158 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f73013f2ba16094e6beade24ce90aa5fcd023baff78a1438c7221592d922d780`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:48:26 GMT
ADD file:c9324c40aae3b3e81b9315fe32797d78d508fb1823de84f7cd1d79e474418a8a in / 
# Wed, 27 Oct 2021 17:48:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3a7a3789f9861a9ab57dd6819c9dcdfcc9a4ea3172b32c88423f9630fb2c0816`  
		Last Modified: Wed, 27 Oct 2021 17:50:10 GMT  
		Size: 756.2 KB (756158 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34.1-uclibc` - linux; arm variant v7

```console
$ docker pull busybox@sha256:53c212bcc0501f011c232df0fb6c837651d0b2f3257b6478a50c0e006b0dabc5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **724.2 KB (724238 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:17d83c72aaa7f419b771dad67eb87d285faa57f1b0d93ed2c9b5b45ea6879065`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 16:57:33 GMT
ADD file:2b15d7b82fc9c84090d98a80296a522d922e42b63fbd9ea47aa2bf62c14aa768 in / 
# Wed, 27 Oct 2021 16:57:33 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:da72ebe61afbc4deef433f5180ccb61df896dfb324e08c0a03b7c43a7c0347b4`  
		Last Modified: Wed, 27 Oct 2021 17:00:02 GMT  
		Size: 724.2 KB (724238 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34.1-uclibc` - linux; arm64 variant v8

```console
$ docker pull busybox@sha256:ce53e9b0310447d0e851ff0d2c9b90f358dbffe719a723147e84b93a4799396c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **828.5 KB (828502 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cc54699a1d7e60dedfeabdf0e0fee681842a16a038ae551ad93b923457b031ab`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:39:27 GMT
ADD file:0e9e223690df05d052434f07b187d500e9544cf4a10992cb1cf17bbf0f9d261d in / 
# Wed, 27 Oct 2021 17:39:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:8ba530ca112c38dd01fa8c96a709f82e5e8cbb425af71f1185efc9ed2c1561d6`  
		Last Modified: Wed, 27 Oct 2021 17:40:27 GMT  
		Size: 828.5 KB (828502 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34.1-uclibc` - linux; mips64le

```console
$ docker pull busybox@sha256:77df281071dd7e01972ec2c4a33c1a6c00d13b24238375fd6622fce97f622fa2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **960.5 KB (960459 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:572b466fbb42b288f43e2d90cf55e26d33cbdd19d667aa31dd20c2628e344f38`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:07:29 GMT
ADD file:82547d0ac8de48ae2eb3f40f43a0e1635155361fc9e20b223e87d9de0ca27e96 in / 
# Wed, 27 Oct 2021 17:07:29 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3d8d4c508e7c9189254e23bfdd5f30c93223516f57715ee5bd0cc85b94d849ee`  
		Last Modified: Wed, 27 Oct 2021 17:07:59 GMT  
		Size: 960.5 KB (960459 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:1.34.1-uclibc` - linux; riscv64

```console
$ docker pull busybox@sha256:06b206c1f1a38094697c7e8bf868f9d326e56a256bc516dbb8ff0ee9c1178999
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **905.3 KB (905306 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a021c599555f76331fddec6e6ecc87cfb92cfd26ab42ef0b968dacb9890cefdc`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:12:42 GMT
ADD file:bbfcdc6a5eda2a3b7965fc2b73a783bf34893ee2cced227315f8468489ec1dee in / 
# Wed, 27 Oct 2021 17:12:43 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:0b2bb6a84cb70a09486f5568f13ac57db414fb39197ce05043689bdeae75744e`  
		Last Modified: Wed, 27 Oct 2021 17:23:32 GMT  
		Size: 905.3 KB (905306 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `busybox:glibc`

```console
$ docker pull busybox@sha256:49ccc099fce12d9673767a60f88761bd56f2ddef85bbdc605fc9c5db2dd5bcb3
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `busybox:glibc` - linux; amd64

```console
$ docker pull busybox@sha256:a158839ba99a90008ddd922dddb869d4a998f60339ee0c41ef9009e60995247a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2566111 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f17f5d94ca617134e143b569c2ec48a7d5864d8387d1191e03116c7ad428bfdf`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:19:53 GMT
ADD file:05820ca2a136d0bd67d2c84ac5294fb0b112df68fcd3482ed77f4d47d30c2106 in / 
# Wed, 27 Oct 2021 17:19:53 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:df20d59888d8cd750954e0854d27df703b73b98999981beeb0ec769c8a707226`  
		Last Modified: Wed, 27 Oct 2021 17:20:43 GMT  
		Size: 2.6 MB (2566111 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:glibc` - linux; arm variant v5

```console
$ docker pull busybox@sha256:068abb3d195234a48605f5067881edfa228840ea5c83245d61e3798979c02961
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **1.9 MB (1930775 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:454a6225af3074772111413ba8b568c07d177f2475e59cbbe6d909156742023f`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:48:41 GMT
ADD file:7bff807316ba841e7e1c392a909bc4d4ab078c4e0ee9e99e62bd89c5002bbc21 in / 
# Wed, 27 Oct 2021 17:48:41 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:58e53cd44176b2564294ddc75394d3683931c011e7c2db9794e90ef796228270`  
		Last Modified: Wed, 27 Oct 2021 17:50:34 GMT  
		Size: 1.9 MB (1930775 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:glibc` - linux; arm variant v7

```console
$ docker pull busybox@sha256:3c56c483948e49726761f4bf2ae09cf227db38d75362ab40024cfc7deda4b04f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **1.7 MB (1678694 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:393a97107921f708c9df011fd1841f3a1c34a1de86c96cd9ef6033c9708f1f11`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 16:57:50 GMT
ADD file:a8b26211fe41f82dec07ffe639f4c6ef4880076f74d8b5f0eb59e34546b325e9 in / 
# Wed, 27 Oct 2021 16:57:50 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:fa418c43645700e527bff2dfb63459af17af7b059c31f60f412699e48fe122a2`  
		Last Modified: Wed, 27 Oct 2021 17:00:27 GMT  
		Size: 1.7 MB (1678694 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:glibc` - linux; arm64 variant v8

```console
$ docker pull busybox@sha256:a72f19c71777a59bf2ea290ceefbe6537e2c8c544463cd57859ef5d432fb244b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.0 MB (1996159 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6fb1464c3dc04a6f8a7c92cdb9f3dfc5ab1c9116061a85ddc90825680be154eb`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:39:34 GMT
ADD file:8bdeb927d5560194f8ba5688d4d9ee1ff7254928145aafe79f7aa326f70ab234 in / 
# Wed, 27 Oct 2021 17:39:34 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:aae080e05ab28f73910de7f8e9f092f5c7eb9f2658363454a3361e69e5723954`  
		Last Modified: Wed, 27 Oct 2021 17:40:46 GMT  
		Size: 2.0 MB (1996159 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:glibc` - linux; 386

```console
$ docker pull busybox@sha256:f2005b8003d6cba4248292df72d03ce8b1aaa048e27eb46a2772a3c486de9212
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.2 MB (2226613 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:edd609bf27e9452d3644678d41c01a84556e59c3f1ab16560448a7161d552546`
-	Default Command: `["sh"]`

```dockerfile
# Tue, 14 Sep 2021 01:38:42 GMT
ADD file:0fa6993c66b449fe9530e9027f7237d4fd0cc9e161a32ff73a9014864cfe7bfd in / 
# Tue, 14 Sep 2021 01:38:42 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:63502a2694813d8f0580c464540ecf7c7f5c6017f74a988c7f6d3b669adfc9c5`  
		Last Modified: Tue, 14 Sep 2021 01:41:11 GMT  
		Size: 2.2 MB (2226613 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:glibc` - linux; mips64le

```console
$ docker pull busybox@sha256:6a2a782c46dedb94c1370a60192df26e00da690b90fa16cb4eb11352e45ec665
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.2 MB (2220711 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:25bbb10f71e32f9c63967dfd90d2213df5f3c8ef303bb47171f3920bb066ae7f`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:07:38 GMT
ADD file:47a6e39f530c1c6dcfc7fbcce83900c83627949ac78e6a74132145f11b427b59 in / 
# Wed, 27 Oct 2021 17:07:38 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:b7954f921f64e29ca6451787b44b6ed22eb7177760e8d8fa31f60c312d7ae676`  
		Last Modified: Wed, 27 Oct 2021 17:08:17 GMT  
		Size: 2.2 MB (2220711 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:glibc` - linux; ppc64le

```console
$ docker pull busybox@sha256:b70f0f45692830c2990b42f770aa29488c20ac41f1c3dcaa242920b73cb1399b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.5 MB (2484095 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:defb34012ae5b6a308b2629e4a6b299d20a193e3475fc98c4fba27f35819061b`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 19:12:06 GMT
ADD file:79fcd19d69e0c4f07016224ba1b588b8c92aa08c3af515be806d4714d6118923 in / 
# Wed, 27 Oct 2021 19:12:09 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:8dcfc896d3701ab1ec378f31cf3076493c9b8c588f103af7901ce812bb09b317`  
		Last Modified: Wed, 27 Oct 2021 19:13:34 GMT  
		Size: 2.5 MB (2484095 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:glibc` - linux; s390x

```console
$ docker pull busybox@sha256:86824a27910bd2a8c6a8478fe99206e6cf4bcada7cb8435c0060cbe885559e53
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.0 MB (2006829 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9e119a54a2534d8ae95d15840f3c4feed03866eb4aee947c49e679206be94893`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:41:19 GMT
ADD file:09207d85b622b42ab9880ec4cbfe2212390ba7c1b0704b91bd3db041db9d9e03 in / 
# Wed, 27 Oct 2021 17:41:19 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:120da903ae50712b69209ab560f7855f3ffed8e6cd9c78dcd74b7e8754c5b168`  
		Last Modified: Wed, 27 Oct 2021 17:42:10 GMT  
		Size: 2.0 MB (2006829 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `busybox:latest`

```console
$ docker pull busybox@sha256:15e927f78df2cc772b70713543d6b651e3cd8370abf86b2ea4644a9fba21107f
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 10
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v6
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; riscv64
	-	linux; s390x

### `busybox:latest` - linux; amd64

```console
$ docker pull busybox@sha256:6066ca124f8c2686b7ae71aa1d6583b28c6dc3df3bdc386f2c89b92162c597d9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **772.8 KB (772798 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cabb9f684f8ba3edb303d578bfd7d709d853539ea1b420a3f6c81a08e85bb3d7`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:19:45 GMT
ADD file:88b76d15f53403ca217f156c3fa01df961ddf224a193cf39ebe51678b77cf9cf in / 
# Wed, 27 Oct 2021 17:19:45 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:01c2cdc137396a9b78be86c47e5773388e6eed953dec79e257777518134b45f1`  
		Last Modified: Wed, 27 Oct 2021 17:20:27 GMT  
		Size: 772.8 KB (772798 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:latest` - linux; arm variant v5

```console
$ docker pull busybox@sha256:399e1e4a0d587717dc9e3a85150cec8498cb6dc73dcb7eddb94959fedb331104
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **756.2 KB (756158 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f73013f2ba16094e6beade24ce90aa5fcd023baff78a1438c7221592d922d780`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:48:26 GMT
ADD file:c9324c40aae3b3e81b9315fe32797d78d508fb1823de84f7cd1d79e474418a8a in / 
# Wed, 27 Oct 2021 17:48:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3a7a3789f9861a9ab57dd6819c9dcdfcc9a4ea3172b32c88423f9630fb2c0816`  
		Last Modified: Wed, 27 Oct 2021 17:50:10 GMT  
		Size: 756.2 KB (756158 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:latest` - linux; arm variant v6

```console
$ docker pull busybox@sha256:4ecc3dc2e06a24df931cb719c3784611d15721c3cb64ab069141071b73f6598b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **952.5 KB (952546 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:327d97ae18db017776599d9b3713503c03974d14346adb99117a186bed6a4d47`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:49:25 GMT
ADD file:c9029a61e764f7eee2bb75e9c227970c7a9e7e81d2d2b2370b7cfa8cff8dddde in / 
# Wed, 27 Oct 2021 17:49:26 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:dac1aaad9ca43e5b6a33470c2cfc6cf3127115e539cae8bc865a8c99d9232727`  
		Last Modified: Wed, 27 Oct 2021 17:50:33 GMT  
		Size: 952.5 KB (952546 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:latest` - linux; arm variant v7

```console
$ docker pull busybox@sha256:53c212bcc0501f011c232df0fb6c837651d0b2f3257b6478a50c0e006b0dabc5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **724.2 KB (724238 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:17d83c72aaa7f419b771dad67eb87d285faa57f1b0d93ed2c9b5b45ea6879065`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 16:57:33 GMT
ADD file:2b15d7b82fc9c84090d98a80296a522d922e42b63fbd9ea47aa2bf62c14aa768 in / 
# Wed, 27 Oct 2021 16:57:33 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:da72ebe61afbc4deef433f5180ccb61df896dfb324e08c0a03b7c43a7c0347b4`  
		Last Modified: Wed, 27 Oct 2021 17:00:02 GMT  
		Size: 724.2 KB (724238 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:latest` - linux; arm64 variant v8

```console
$ docker pull busybox@sha256:ce53e9b0310447d0e851ff0d2c9b90f358dbffe719a723147e84b93a4799396c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **828.5 KB (828502 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cc54699a1d7e60dedfeabdf0e0fee681842a16a038ae551ad93b923457b031ab`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:39:27 GMT
ADD file:0e9e223690df05d052434f07b187d500e9544cf4a10992cb1cf17bbf0f9d261d in / 
# Wed, 27 Oct 2021 17:39:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:8ba530ca112c38dd01fa8c96a709f82e5e8cbb425af71f1185efc9ed2c1561d6`  
		Last Modified: Wed, 27 Oct 2021 17:40:27 GMT  
		Size: 828.5 KB (828502 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:latest` - linux; 386

```console
$ docker pull busybox@sha256:c71cb4f7e8ececaffb34037c2637dc86820e4185100e18b4d02d613a9bd772af
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **728.2 KB (728191 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:046e74e02754f28137f587f303f9852c82f3ec76fff1f2f278655b3e1193ad3e`
-	Default Command: `["sh"]`

```dockerfile
# Tue, 14 Sep 2021 01:38:33 GMT
ADD file:4143b23f3d54e871d1a69c3518704e9e6ebed52dcc278b5f1dbcd11343940898 in / 
# Tue, 14 Sep 2021 01:38:33 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:f835a1acb88b23855be36becf1ebe0a310ab5761b04c3fb24d0c94c41f7c3e41`  
		Last Modified: Tue, 14 Sep 2021 01:40:49 GMT  
		Size: 728.2 KB (728191 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:latest` - linux; mips64le

```console
$ docker pull busybox@sha256:77df281071dd7e01972ec2c4a33c1a6c00d13b24238375fd6622fce97f622fa2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **960.5 KB (960459 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:572b466fbb42b288f43e2d90cf55e26d33cbdd19d667aa31dd20c2628e344f38`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:07:29 GMT
ADD file:82547d0ac8de48ae2eb3f40f43a0e1635155361fc9e20b223e87d9de0ca27e96 in / 
# Wed, 27 Oct 2021 17:07:29 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3d8d4c508e7c9189254e23bfdd5f30c93223516f57715ee5bd0cc85b94d849ee`  
		Last Modified: Wed, 27 Oct 2021 17:07:59 GMT  
		Size: 960.5 KB (960459 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:latest` - linux; ppc64le

```console
$ docker pull busybox@sha256:b70f0f45692830c2990b42f770aa29488c20ac41f1c3dcaa242920b73cb1399b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.5 MB (2484095 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:defb34012ae5b6a308b2629e4a6b299d20a193e3475fc98c4fba27f35819061b`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 19:12:06 GMT
ADD file:79fcd19d69e0c4f07016224ba1b588b8c92aa08c3af515be806d4714d6118923 in / 
# Wed, 27 Oct 2021 19:12:09 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:8dcfc896d3701ab1ec378f31cf3076493c9b8c588f103af7901ce812bb09b317`  
		Last Modified: Wed, 27 Oct 2021 19:13:34 GMT  
		Size: 2.5 MB (2484095 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:latest` - linux; riscv64

```console
$ docker pull busybox@sha256:06b206c1f1a38094697c7e8bf868f9d326e56a256bc516dbb8ff0ee9c1178999
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **905.3 KB (905306 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a021c599555f76331fddec6e6ecc87cfb92cfd26ab42ef0b968dacb9890cefdc`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:12:42 GMT
ADD file:bbfcdc6a5eda2a3b7965fc2b73a783bf34893ee2cced227315f8468489ec1dee in / 
# Wed, 27 Oct 2021 17:12:43 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:0b2bb6a84cb70a09486f5568f13ac57db414fb39197ce05043689bdeae75744e`  
		Last Modified: Wed, 27 Oct 2021 17:23:32 GMT  
		Size: 905.3 KB (905306 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:latest` - linux; s390x

```console
$ docker pull busybox@sha256:86824a27910bd2a8c6a8478fe99206e6cf4bcada7cb8435c0060cbe885559e53
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.0 MB (2006829 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9e119a54a2534d8ae95d15840f3c4feed03866eb4aee947c49e679206be94893`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:41:19 GMT
ADD file:09207d85b622b42ab9880ec4cbfe2212390ba7c1b0704b91bd3db041db9d9e03 in / 
# Wed, 27 Oct 2021 17:41:19 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:120da903ae50712b69209ab560f7855f3ffed8e6cd9c78dcd74b7e8754c5b168`  
		Last Modified: Wed, 27 Oct 2021 17:42:10 GMT  
		Size: 2.0 MB (2006829 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `busybox:musl`

```console
$ docker pull busybox@sha256:13b27cf92cc81d1f4c1584360942bedce72e98af80aa273f77382715a33f68c2
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 7
	-	linux; amd64
	-	linux; arm variant v6
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; ppc64le
	-	linux; s390x

### `busybox:musl` - linux; amd64

```console
$ docker pull busybox@sha256:c57be995c4c3bb22b723c49d74fc3b5a4510dfbd8be28b5d021d7a4a53a4bab1
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **843.3 KB (843266 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1b408f772ab08633f27bf8918481dfd3fe2f7689b23a50897cb7cef2e17a6cb8`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:19:58 GMT
ADD file:58dbb8135e92be6f243e74462af8e85fd67417ba0281b81ea7742af344794c3f in / 
# Wed, 27 Oct 2021 17:19:58 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:b0e194f8faadf770ca46f1caf5b5ce0bb82ef76806729e22c1a20aff0c29c1f6`  
		Last Modified: Wed, 27 Oct 2021 17:20:59 GMT  
		Size: 843.3 KB (843266 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:musl` - linux; arm variant v6

```console
$ docker pull busybox@sha256:4ecc3dc2e06a24df931cb719c3784611d15721c3cb64ab069141071b73f6598b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **952.5 KB (952546 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:327d97ae18db017776599d9b3713503c03974d14346adb99117a186bed6a4d47`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:49:25 GMT
ADD file:c9029a61e764f7eee2bb75e9c227970c7a9e7e81d2d2b2370b7cfa8cff8dddde in / 
# Wed, 27 Oct 2021 17:49:26 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:dac1aaad9ca43e5b6a33470c2cfc6cf3127115e539cae8bc865a8c99d9232727`  
		Last Modified: Wed, 27 Oct 2021 17:50:33 GMT  
		Size: 952.5 KB (952546 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:musl` - linux; arm variant v7

```console
$ docker pull busybox@sha256:e23f636436b365735d7c726df3e36e48fe309afaa422a503bd8ecf9740742f9c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **834.7 KB (834677 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:27fba482d17b4205fe8a01218cc62d830b45f0a1e9ee13eeb81cf2cd7a69390d`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 16:58:07 GMT
ADD file:242e49f9869dfe3d8b59aee3801ef14dac3c58176f6cea670614284afd50ce8d in / 
# Wed, 27 Oct 2021 16:58:08 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:9d172075913815538a77dd7a7ad3bbefc76474354936eddd73cd933e9776b8e2`  
		Last Modified: Wed, 27 Oct 2021 17:00:50 GMT  
		Size: 834.7 KB (834677 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:musl` - linux; arm64 variant v8

```console
$ docker pull busybox@sha256:976ce0d1af7874bc0216c19efbd9178cc0455513a963cd834a35b9e732e298fb
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **882.2 KB (882176 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:69552745f9d2985d9bb00c5d912723a5f4a9afd133966cf8982b06067a3f7cf7`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:39:41 GMT
ADD file:4562e1c40e298277fc65259e3b29d3b72f89a28088f94bcae8e543df2ede1f5d in / 
# Wed, 27 Oct 2021 17:39:41 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:20a2e0c8f8c2704a1cd5560c3a242607af57c42ae6a3109108be0303d32092b1`  
		Last Modified: Wed, 27 Oct 2021 17:41:05 GMT  
		Size: 882.2 KB (882176 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:musl` - linux; 386

```console
$ docker pull busybox@sha256:2eed3c17cd8b978262caaed66dca91a6d23f654f7898f3fabeb09151b31c32e3
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **842.7 KB (842669 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f8748766cfd16c8edfbd2abe6a441ddfdeae7dd62276b21a0b5f2314568db712`
-	Default Command: `["sh"]`

```dockerfile
# Tue, 14 Sep 2021 01:38:53 GMT
ADD file:6ff60306035d5794506a2bec937e683fb5a799fabf489986cf76830451f679d5 in / 
# Tue, 14 Sep 2021 01:38:53 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:7d0e04faa0be851392cfcbd49724f856f34d5fb7c10d0b1ed1dea8bc94d4d4b1`  
		Last Modified: Tue, 14 Sep 2021 01:41:31 GMT  
		Size: 842.7 KB (842669 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:musl` - linux; ppc64le

```console
$ docker pull busybox@sha256:7696de5d912dc3db582ba08c435f8b1b01a0cf845db7e44f1ad4269b656b627a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **919.2 KB (919168 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:0d6acacaf155166a2a58e39cd33187987fe4b36400dedbe22607902b50f3793d`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 19:12:21 GMT
ADD file:b19d93c361eebcc52af7de98cdbacec78af10e600d8cd334b9301752ba712766 in / 
# Wed, 27 Oct 2021 19:12:30 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:2abb4f53c2ba292747e959a7d205240ab584ca7650ff38823327a68680916549`  
		Last Modified: Wed, 27 Oct 2021 19:13:53 GMT  
		Size: 919.2 KB (919168 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:musl` - linux; s390x

```console
$ docker pull busybox@sha256:2839b6e6e257d487803c6942666415c052a1e1c11e3d41d57fe30d25c50da924
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **893.9 KB (893870 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4c5d5f91ce1964dd844df1a44fba72b596f170eb0554cf45c195d2ec65849f23`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:41:26 GMT
ADD file:5722acc1a5fd74b7ff5c73c86ee2da0bfa9560c036f0aee24eac818305c5b2b6 in / 
# Wed, 27 Oct 2021 17:41:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3f4a2ae691cff8edfecb1d55dc2d2b5a308572dbba3c2d79fc65547c60d35752`  
		Last Modified: Wed, 27 Oct 2021 17:42:26 GMT  
		Size: 893.9 KB (893870 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `busybox:stable`

```console
$ docker pull busybox@sha256:15e927f78df2cc772b70713543d6b651e3cd8370abf86b2ea4644a9fba21107f
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 10
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v6
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; riscv64
	-	linux; s390x

### `busybox:stable` - linux; amd64

```console
$ docker pull busybox@sha256:6066ca124f8c2686b7ae71aa1d6583b28c6dc3df3bdc386f2c89b92162c597d9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **772.8 KB (772798 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cabb9f684f8ba3edb303d578bfd7d709d853539ea1b420a3f6c81a08e85bb3d7`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:19:45 GMT
ADD file:88b76d15f53403ca217f156c3fa01df961ddf224a193cf39ebe51678b77cf9cf in / 
# Wed, 27 Oct 2021 17:19:45 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:01c2cdc137396a9b78be86c47e5773388e6eed953dec79e257777518134b45f1`  
		Last Modified: Wed, 27 Oct 2021 17:20:27 GMT  
		Size: 772.8 KB (772798 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable` - linux; arm variant v5

```console
$ docker pull busybox@sha256:399e1e4a0d587717dc9e3a85150cec8498cb6dc73dcb7eddb94959fedb331104
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **756.2 KB (756158 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f73013f2ba16094e6beade24ce90aa5fcd023baff78a1438c7221592d922d780`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:48:26 GMT
ADD file:c9324c40aae3b3e81b9315fe32797d78d508fb1823de84f7cd1d79e474418a8a in / 
# Wed, 27 Oct 2021 17:48:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3a7a3789f9861a9ab57dd6819c9dcdfcc9a4ea3172b32c88423f9630fb2c0816`  
		Last Modified: Wed, 27 Oct 2021 17:50:10 GMT  
		Size: 756.2 KB (756158 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable` - linux; arm variant v6

```console
$ docker pull busybox@sha256:4ecc3dc2e06a24df931cb719c3784611d15721c3cb64ab069141071b73f6598b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **952.5 KB (952546 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:327d97ae18db017776599d9b3713503c03974d14346adb99117a186bed6a4d47`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:49:25 GMT
ADD file:c9029a61e764f7eee2bb75e9c227970c7a9e7e81d2d2b2370b7cfa8cff8dddde in / 
# Wed, 27 Oct 2021 17:49:26 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:dac1aaad9ca43e5b6a33470c2cfc6cf3127115e539cae8bc865a8c99d9232727`  
		Last Modified: Wed, 27 Oct 2021 17:50:33 GMT  
		Size: 952.5 KB (952546 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable` - linux; arm variant v7

```console
$ docker pull busybox@sha256:53c212bcc0501f011c232df0fb6c837651d0b2f3257b6478a50c0e006b0dabc5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **724.2 KB (724238 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:17d83c72aaa7f419b771dad67eb87d285faa57f1b0d93ed2c9b5b45ea6879065`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 16:57:33 GMT
ADD file:2b15d7b82fc9c84090d98a80296a522d922e42b63fbd9ea47aa2bf62c14aa768 in / 
# Wed, 27 Oct 2021 16:57:33 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:da72ebe61afbc4deef433f5180ccb61df896dfb324e08c0a03b7c43a7c0347b4`  
		Last Modified: Wed, 27 Oct 2021 17:00:02 GMT  
		Size: 724.2 KB (724238 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable` - linux; arm64 variant v8

```console
$ docker pull busybox@sha256:ce53e9b0310447d0e851ff0d2c9b90f358dbffe719a723147e84b93a4799396c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **828.5 KB (828502 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cc54699a1d7e60dedfeabdf0e0fee681842a16a038ae551ad93b923457b031ab`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:39:27 GMT
ADD file:0e9e223690df05d052434f07b187d500e9544cf4a10992cb1cf17bbf0f9d261d in / 
# Wed, 27 Oct 2021 17:39:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:8ba530ca112c38dd01fa8c96a709f82e5e8cbb425af71f1185efc9ed2c1561d6`  
		Last Modified: Wed, 27 Oct 2021 17:40:27 GMT  
		Size: 828.5 KB (828502 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable` - linux; 386

```console
$ docker pull busybox@sha256:c71cb4f7e8ececaffb34037c2637dc86820e4185100e18b4d02d613a9bd772af
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **728.2 KB (728191 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:046e74e02754f28137f587f303f9852c82f3ec76fff1f2f278655b3e1193ad3e`
-	Default Command: `["sh"]`

```dockerfile
# Tue, 14 Sep 2021 01:38:33 GMT
ADD file:4143b23f3d54e871d1a69c3518704e9e6ebed52dcc278b5f1dbcd11343940898 in / 
# Tue, 14 Sep 2021 01:38:33 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:f835a1acb88b23855be36becf1ebe0a310ab5761b04c3fb24d0c94c41f7c3e41`  
		Last Modified: Tue, 14 Sep 2021 01:40:49 GMT  
		Size: 728.2 KB (728191 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable` - linux; mips64le

```console
$ docker pull busybox@sha256:77df281071dd7e01972ec2c4a33c1a6c00d13b24238375fd6622fce97f622fa2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **960.5 KB (960459 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:572b466fbb42b288f43e2d90cf55e26d33cbdd19d667aa31dd20c2628e344f38`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:07:29 GMT
ADD file:82547d0ac8de48ae2eb3f40f43a0e1635155361fc9e20b223e87d9de0ca27e96 in / 
# Wed, 27 Oct 2021 17:07:29 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3d8d4c508e7c9189254e23bfdd5f30c93223516f57715ee5bd0cc85b94d849ee`  
		Last Modified: Wed, 27 Oct 2021 17:07:59 GMT  
		Size: 960.5 KB (960459 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable` - linux; ppc64le

```console
$ docker pull busybox@sha256:b70f0f45692830c2990b42f770aa29488c20ac41f1c3dcaa242920b73cb1399b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.5 MB (2484095 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:defb34012ae5b6a308b2629e4a6b299d20a193e3475fc98c4fba27f35819061b`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 19:12:06 GMT
ADD file:79fcd19d69e0c4f07016224ba1b588b8c92aa08c3af515be806d4714d6118923 in / 
# Wed, 27 Oct 2021 19:12:09 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:8dcfc896d3701ab1ec378f31cf3076493c9b8c588f103af7901ce812bb09b317`  
		Last Modified: Wed, 27 Oct 2021 19:13:34 GMT  
		Size: 2.5 MB (2484095 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable` - linux; riscv64

```console
$ docker pull busybox@sha256:06b206c1f1a38094697c7e8bf868f9d326e56a256bc516dbb8ff0ee9c1178999
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **905.3 KB (905306 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a021c599555f76331fddec6e6ecc87cfb92cfd26ab42ef0b968dacb9890cefdc`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:12:42 GMT
ADD file:bbfcdc6a5eda2a3b7965fc2b73a783bf34893ee2cced227315f8468489ec1dee in / 
# Wed, 27 Oct 2021 17:12:43 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:0b2bb6a84cb70a09486f5568f13ac57db414fb39197ce05043689bdeae75744e`  
		Last Modified: Wed, 27 Oct 2021 17:23:32 GMT  
		Size: 905.3 KB (905306 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable` - linux; s390x

```console
$ docker pull busybox@sha256:86824a27910bd2a8c6a8478fe99206e6cf4bcada7cb8435c0060cbe885559e53
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.0 MB (2006829 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9e119a54a2534d8ae95d15840f3c4feed03866eb4aee947c49e679206be94893`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:41:19 GMT
ADD file:09207d85b622b42ab9880ec4cbfe2212390ba7c1b0704b91bd3db041db9d9e03 in / 
# Wed, 27 Oct 2021 17:41:19 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:120da903ae50712b69209ab560f7855f3ffed8e6cd9c78dcd74b7e8754c5b168`  
		Last Modified: Wed, 27 Oct 2021 17:42:10 GMT  
		Size: 2.0 MB (2006829 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `busybox:stable-glibc`

```console
$ docker pull busybox@sha256:49ccc099fce12d9673767a60f88761bd56f2ddef85bbdc605fc9c5db2dd5bcb3
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `busybox:stable-glibc` - linux; amd64

```console
$ docker pull busybox@sha256:a158839ba99a90008ddd922dddb869d4a998f60339ee0c41ef9009e60995247a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2566111 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f17f5d94ca617134e143b569c2ec48a7d5864d8387d1191e03116c7ad428bfdf`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:19:53 GMT
ADD file:05820ca2a136d0bd67d2c84ac5294fb0b112df68fcd3482ed77f4d47d30c2106 in / 
# Wed, 27 Oct 2021 17:19:53 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:df20d59888d8cd750954e0854d27df703b73b98999981beeb0ec769c8a707226`  
		Last Modified: Wed, 27 Oct 2021 17:20:43 GMT  
		Size: 2.6 MB (2566111 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable-glibc` - linux; arm variant v5

```console
$ docker pull busybox@sha256:068abb3d195234a48605f5067881edfa228840ea5c83245d61e3798979c02961
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **1.9 MB (1930775 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:454a6225af3074772111413ba8b568c07d177f2475e59cbbe6d909156742023f`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:48:41 GMT
ADD file:7bff807316ba841e7e1c392a909bc4d4ab078c4e0ee9e99e62bd89c5002bbc21 in / 
# Wed, 27 Oct 2021 17:48:41 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:58e53cd44176b2564294ddc75394d3683931c011e7c2db9794e90ef796228270`  
		Last Modified: Wed, 27 Oct 2021 17:50:34 GMT  
		Size: 1.9 MB (1930775 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable-glibc` - linux; arm variant v7

```console
$ docker pull busybox@sha256:3c56c483948e49726761f4bf2ae09cf227db38d75362ab40024cfc7deda4b04f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **1.7 MB (1678694 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:393a97107921f708c9df011fd1841f3a1c34a1de86c96cd9ef6033c9708f1f11`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 16:57:50 GMT
ADD file:a8b26211fe41f82dec07ffe639f4c6ef4880076f74d8b5f0eb59e34546b325e9 in / 
# Wed, 27 Oct 2021 16:57:50 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:fa418c43645700e527bff2dfb63459af17af7b059c31f60f412699e48fe122a2`  
		Last Modified: Wed, 27 Oct 2021 17:00:27 GMT  
		Size: 1.7 MB (1678694 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable-glibc` - linux; arm64 variant v8

```console
$ docker pull busybox@sha256:a72f19c71777a59bf2ea290ceefbe6537e2c8c544463cd57859ef5d432fb244b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.0 MB (1996159 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6fb1464c3dc04a6f8a7c92cdb9f3dfc5ab1c9116061a85ddc90825680be154eb`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:39:34 GMT
ADD file:8bdeb927d5560194f8ba5688d4d9ee1ff7254928145aafe79f7aa326f70ab234 in / 
# Wed, 27 Oct 2021 17:39:34 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:aae080e05ab28f73910de7f8e9f092f5c7eb9f2658363454a3361e69e5723954`  
		Last Modified: Wed, 27 Oct 2021 17:40:46 GMT  
		Size: 2.0 MB (1996159 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable-glibc` - linux; 386

```console
$ docker pull busybox@sha256:f2005b8003d6cba4248292df72d03ce8b1aaa048e27eb46a2772a3c486de9212
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.2 MB (2226613 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:edd609bf27e9452d3644678d41c01a84556e59c3f1ab16560448a7161d552546`
-	Default Command: `["sh"]`

```dockerfile
# Tue, 14 Sep 2021 01:38:42 GMT
ADD file:0fa6993c66b449fe9530e9027f7237d4fd0cc9e161a32ff73a9014864cfe7bfd in / 
# Tue, 14 Sep 2021 01:38:42 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:63502a2694813d8f0580c464540ecf7c7f5c6017f74a988c7f6d3b669adfc9c5`  
		Last Modified: Tue, 14 Sep 2021 01:41:11 GMT  
		Size: 2.2 MB (2226613 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable-glibc` - linux; mips64le

```console
$ docker pull busybox@sha256:6a2a782c46dedb94c1370a60192df26e00da690b90fa16cb4eb11352e45ec665
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.2 MB (2220711 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:25bbb10f71e32f9c63967dfd90d2213df5f3c8ef303bb47171f3920bb066ae7f`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:07:38 GMT
ADD file:47a6e39f530c1c6dcfc7fbcce83900c83627949ac78e6a74132145f11b427b59 in / 
# Wed, 27 Oct 2021 17:07:38 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:b7954f921f64e29ca6451787b44b6ed22eb7177760e8d8fa31f60c312d7ae676`  
		Last Modified: Wed, 27 Oct 2021 17:08:17 GMT  
		Size: 2.2 MB (2220711 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable-glibc` - linux; ppc64le

```console
$ docker pull busybox@sha256:b70f0f45692830c2990b42f770aa29488c20ac41f1c3dcaa242920b73cb1399b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.5 MB (2484095 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:defb34012ae5b6a308b2629e4a6b299d20a193e3475fc98c4fba27f35819061b`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 19:12:06 GMT
ADD file:79fcd19d69e0c4f07016224ba1b588b8c92aa08c3af515be806d4714d6118923 in / 
# Wed, 27 Oct 2021 19:12:09 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:8dcfc896d3701ab1ec378f31cf3076493c9b8c588f103af7901ce812bb09b317`  
		Last Modified: Wed, 27 Oct 2021 19:13:34 GMT  
		Size: 2.5 MB (2484095 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable-glibc` - linux; s390x

```console
$ docker pull busybox@sha256:86824a27910bd2a8c6a8478fe99206e6cf4bcada7cb8435c0060cbe885559e53
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.0 MB (2006829 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9e119a54a2534d8ae95d15840f3c4feed03866eb4aee947c49e679206be94893`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:41:19 GMT
ADD file:09207d85b622b42ab9880ec4cbfe2212390ba7c1b0704b91bd3db041db9d9e03 in / 
# Wed, 27 Oct 2021 17:41:19 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:120da903ae50712b69209ab560f7855f3ffed8e6cd9c78dcd74b7e8754c5b168`  
		Last Modified: Wed, 27 Oct 2021 17:42:10 GMT  
		Size: 2.0 MB (2006829 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `busybox:stable-musl`

```console
$ docker pull busybox@sha256:13b27cf92cc81d1f4c1584360942bedce72e98af80aa273f77382715a33f68c2
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 7
	-	linux; amd64
	-	linux; arm variant v6
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; ppc64le
	-	linux; s390x

### `busybox:stable-musl` - linux; amd64

```console
$ docker pull busybox@sha256:c57be995c4c3bb22b723c49d74fc3b5a4510dfbd8be28b5d021d7a4a53a4bab1
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **843.3 KB (843266 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1b408f772ab08633f27bf8918481dfd3fe2f7689b23a50897cb7cef2e17a6cb8`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:19:58 GMT
ADD file:58dbb8135e92be6f243e74462af8e85fd67417ba0281b81ea7742af344794c3f in / 
# Wed, 27 Oct 2021 17:19:58 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:b0e194f8faadf770ca46f1caf5b5ce0bb82ef76806729e22c1a20aff0c29c1f6`  
		Last Modified: Wed, 27 Oct 2021 17:20:59 GMT  
		Size: 843.3 KB (843266 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable-musl` - linux; arm variant v6

```console
$ docker pull busybox@sha256:4ecc3dc2e06a24df931cb719c3784611d15721c3cb64ab069141071b73f6598b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **952.5 KB (952546 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:327d97ae18db017776599d9b3713503c03974d14346adb99117a186bed6a4d47`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:49:25 GMT
ADD file:c9029a61e764f7eee2bb75e9c227970c7a9e7e81d2d2b2370b7cfa8cff8dddde in / 
# Wed, 27 Oct 2021 17:49:26 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:dac1aaad9ca43e5b6a33470c2cfc6cf3127115e539cae8bc865a8c99d9232727`  
		Last Modified: Wed, 27 Oct 2021 17:50:33 GMT  
		Size: 952.5 KB (952546 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable-musl` - linux; arm variant v7

```console
$ docker pull busybox@sha256:e23f636436b365735d7c726df3e36e48fe309afaa422a503bd8ecf9740742f9c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **834.7 KB (834677 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:27fba482d17b4205fe8a01218cc62d830b45f0a1e9ee13eeb81cf2cd7a69390d`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 16:58:07 GMT
ADD file:242e49f9869dfe3d8b59aee3801ef14dac3c58176f6cea670614284afd50ce8d in / 
# Wed, 27 Oct 2021 16:58:08 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:9d172075913815538a77dd7a7ad3bbefc76474354936eddd73cd933e9776b8e2`  
		Last Modified: Wed, 27 Oct 2021 17:00:50 GMT  
		Size: 834.7 KB (834677 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable-musl` - linux; arm64 variant v8

```console
$ docker pull busybox@sha256:976ce0d1af7874bc0216c19efbd9178cc0455513a963cd834a35b9e732e298fb
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **882.2 KB (882176 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:69552745f9d2985d9bb00c5d912723a5f4a9afd133966cf8982b06067a3f7cf7`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:39:41 GMT
ADD file:4562e1c40e298277fc65259e3b29d3b72f89a28088f94bcae8e543df2ede1f5d in / 
# Wed, 27 Oct 2021 17:39:41 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:20a2e0c8f8c2704a1cd5560c3a242607af57c42ae6a3109108be0303d32092b1`  
		Last Modified: Wed, 27 Oct 2021 17:41:05 GMT  
		Size: 882.2 KB (882176 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable-musl` - linux; 386

```console
$ docker pull busybox@sha256:2eed3c17cd8b978262caaed66dca91a6d23f654f7898f3fabeb09151b31c32e3
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **842.7 KB (842669 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f8748766cfd16c8edfbd2abe6a441ddfdeae7dd62276b21a0b5f2314568db712`
-	Default Command: `["sh"]`

```dockerfile
# Tue, 14 Sep 2021 01:38:53 GMT
ADD file:6ff60306035d5794506a2bec937e683fb5a799fabf489986cf76830451f679d5 in / 
# Tue, 14 Sep 2021 01:38:53 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:7d0e04faa0be851392cfcbd49724f856f34d5fb7c10d0b1ed1dea8bc94d4d4b1`  
		Last Modified: Tue, 14 Sep 2021 01:41:31 GMT  
		Size: 842.7 KB (842669 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable-musl` - linux; ppc64le

```console
$ docker pull busybox@sha256:7696de5d912dc3db582ba08c435f8b1b01a0cf845db7e44f1ad4269b656b627a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **919.2 KB (919168 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:0d6acacaf155166a2a58e39cd33187987fe4b36400dedbe22607902b50f3793d`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 19:12:21 GMT
ADD file:b19d93c361eebcc52af7de98cdbacec78af10e600d8cd334b9301752ba712766 in / 
# Wed, 27 Oct 2021 19:12:30 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:2abb4f53c2ba292747e959a7d205240ab584ca7650ff38823327a68680916549`  
		Last Modified: Wed, 27 Oct 2021 19:13:53 GMT  
		Size: 919.2 KB (919168 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable-musl` - linux; s390x

```console
$ docker pull busybox@sha256:2839b6e6e257d487803c6942666415c052a1e1c11e3d41d57fe30d25c50da924
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **893.9 KB (893870 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4c5d5f91ce1964dd844df1a44fba72b596f170eb0554cf45c195d2ec65849f23`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:41:26 GMT
ADD file:5722acc1a5fd74b7ff5c73c86ee2da0bfa9560c036f0aee24eac818305c5b2b6 in / 
# Wed, 27 Oct 2021 17:41:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3f4a2ae691cff8edfecb1d55dc2d2b5a308572dbba3c2d79fc65547c60d35752`  
		Last Modified: Wed, 27 Oct 2021 17:42:26 GMT  
		Size: 893.9 KB (893870 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `busybox:stable-uclibc`

```console
$ docker pull busybox@sha256:ae11b37e026fe97c34657514b4de97bf18712777cc5de20eac9ccfb1972bf24f
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 7
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; riscv64

### `busybox:stable-uclibc` - linux; amd64

```console
$ docker pull busybox@sha256:6066ca124f8c2686b7ae71aa1d6583b28c6dc3df3bdc386f2c89b92162c597d9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **772.8 KB (772798 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cabb9f684f8ba3edb303d578bfd7d709d853539ea1b420a3f6c81a08e85bb3d7`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:19:45 GMT
ADD file:88b76d15f53403ca217f156c3fa01df961ddf224a193cf39ebe51678b77cf9cf in / 
# Wed, 27 Oct 2021 17:19:45 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:01c2cdc137396a9b78be86c47e5773388e6eed953dec79e257777518134b45f1`  
		Last Modified: Wed, 27 Oct 2021 17:20:27 GMT  
		Size: 772.8 KB (772798 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable-uclibc` - linux; arm variant v5

```console
$ docker pull busybox@sha256:399e1e4a0d587717dc9e3a85150cec8498cb6dc73dcb7eddb94959fedb331104
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **756.2 KB (756158 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f73013f2ba16094e6beade24ce90aa5fcd023baff78a1438c7221592d922d780`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:48:26 GMT
ADD file:c9324c40aae3b3e81b9315fe32797d78d508fb1823de84f7cd1d79e474418a8a in / 
# Wed, 27 Oct 2021 17:48:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3a7a3789f9861a9ab57dd6819c9dcdfcc9a4ea3172b32c88423f9630fb2c0816`  
		Last Modified: Wed, 27 Oct 2021 17:50:10 GMT  
		Size: 756.2 KB (756158 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable-uclibc` - linux; arm variant v7

```console
$ docker pull busybox@sha256:53c212bcc0501f011c232df0fb6c837651d0b2f3257b6478a50c0e006b0dabc5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **724.2 KB (724238 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:17d83c72aaa7f419b771dad67eb87d285faa57f1b0d93ed2c9b5b45ea6879065`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 16:57:33 GMT
ADD file:2b15d7b82fc9c84090d98a80296a522d922e42b63fbd9ea47aa2bf62c14aa768 in / 
# Wed, 27 Oct 2021 16:57:33 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:da72ebe61afbc4deef433f5180ccb61df896dfb324e08c0a03b7c43a7c0347b4`  
		Last Modified: Wed, 27 Oct 2021 17:00:02 GMT  
		Size: 724.2 KB (724238 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable-uclibc` - linux; arm64 variant v8

```console
$ docker pull busybox@sha256:ce53e9b0310447d0e851ff0d2c9b90f358dbffe719a723147e84b93a4799396c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **828.5 KB (828502 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cc54699a1d7e60dedfeabdf0e0fee681842a16a038ae551ad93b923457b031ab`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:39:27 GMT
ADD file:0e9e223690df05d052434f07b187d500e9544cf4a10992cb1cf17bbf0f9d261d in / 
# Wed, 27 Oct 2021 17:39:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:8ba530ca112c38dd01fa8c96a709f82e5e8cbb425af71f1185efc9ed2c1561d6`  
		Last Modified: Wed, 27 Oct 2021 17:40:27 GMT  
		Size: 828.5 KB (828502 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable-uclibc` - linux; 386

```console
$ docker pull busybox@sha256:c71cb4f7e8ececaffb34037c2637dc86820e4185100e18b4d02d613a9bd772af
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **728.2 KB (728191 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:046e74e02754f28137f587f303f9852c82f3ec76fff1f2f278655b3e1193ad3e`
-	Default Command: `["sh"]`

```dockerfile
# Tue, 14 Sep 2021 01:38:33 GMT
ADD file:4143b23f3d54e871d1a69c3518704e9e6ebed52dcc278b5f1dbcd11343940898 in / 
# Tue, 14 Sep 2021 01:38:33 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:f835a1acb88b23855be36becf1ebe0a310ab5761b04c3fb24d0c94c41f7c3e41`  
		Last Modified: Tue, 14 Sep 2021 01:40:49 GMT  
		Size: 728.2 KB (728191 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable-uclibc` - linux; mips64le

```console
$ docker pull busybox@sha256:77df281071dd7e01972ec2c4a33c1a6c00d13b24238375fd6622fce97f622fa2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **960.5 KB (960459 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:572b466fbb42b288f43e2d90cf55e26d33cbdd19d667aa31dd20c2628e344f38`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:07:29 GMT
ADD file:82547d0ac8de48ae2eb3f40f43a0e1635155361fc9e20b223e87d9de0ca27e96 in / 
# Wed, 27 Oct 2021 17:07:29 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3d8d4c508e7c9189254e23bfdd5f30c93223516f57715ee5bd0cc85b94d849ee`  
		Last Modified: Wed, 27 Oct 2021 17:07:59 GMT  
		Size: 960.5 KB (960459 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable-uclibc` - linux; riscv64

```console
$ docker pull busybox@sha256:06b206c1f1a38094697c7e8bf868f9d326e56a256bc516dbb8ff0ee9c1178999
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **905.3 KB (905306 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a021c599555f76331fddec6e6ecc87cfb92cfd26ab42ef0b968dacb9890cefdc`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:12:42 GMT
ADD file:bbfcdc6a5eda2a3b7965fc2b73a783bf34893ee2cced227315f8468489ec1dee in / 
# Wed, 27 Oct 2021 17:12:43 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:0b2bb6a84cb70a09486f5568f13ac57db414fb39197ce05043689bdeae75744e`  
		Last Modified: Wed, 27 Oct 2021 17:23:32 GMT  
		Size: 905.3 KB (905306 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `busybox:uclibc`

```console
$ docker pull busybox@sha256:ae11b37e026fe97c34657514b4de97bf18712777cc5de20eac9ccfb1972bf24f
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 7
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; riscv64

### `busybox:uclibc` - linux; amd64

```console
$ docker pull busybox@sha256:6066ca124f8c2686b7ae71aa1d6583b28c6dc3df3bdc386f2c89b92162c597d9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **772.8 KB (772798 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cabb9f684f8ba3edb303d578bfd7d709d853539ea1b420a3f6c81a08e85bb3d7`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:19:45 GMT
ADD file:88b76d15f53403ca217f156c3fa01df961ddf224a193cf39ebe51678b77cf9cf in / 
# Wed, 27 Oct 2021 17:19:45 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:01c2cdc137396a9b78be86c47e5773388e6eed953dec79e257777518134b45f1`  
		Last Modified: Wed, 27 Oct 2021 17:20:27 GMT  
		Size: 772.8 KB (772798 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:uclibc` - linux; arm variant v5

```console
$ docker pull busybox@sha256:399e1e4a0d587717dc9e3a85150cec8498cb6dc73dcb7eddb94959fedb331104
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **756.2 KB (756158 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f73013f2ba16094e6beade24ce90aa5fcd023baff78a1438c7221592d922d780`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:48:26 GMT
ADD file:c9324c40aae3b3e81b9315fe32797d78d508fb1823de84f7cd1d79e474418a8a in / 
# Wed, 27 Oct 2021 17:48:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3a7a3789f9861a9ab57dd6819c9dcdfcc9a4ea3172b32c88423f9630fb2c0816`  
		Last Modified: Wed, 27 Oct 2021 17:50:10 GMT  
		Size: 756.2 KB (756158 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:uclibc` - linux; arm variant v7

```console
$ docker pull busybox@sha256:53c212bcc0501f011c232df0fb6c837651d0b2f3257b6478a50c0e006b0dabc5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **724.2 KB (724238 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:17d83c72aaa7f419b771dad67eb87d285faa57f1b0d93ed2c9b5b45ea6879065`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 16:57:33 GMT
ADD file:2b15d7b82fc9c84090d98a80296a522d922e42b63fbd9ea47aa2bf62c14aa768 in / 
# Wed, 27 Oct 2021 16:57:33 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:da72ebe61afbc4deef433f5180ccb61df896dfb324e08c0a03b7c43a7c0347b4`  
		Last Modified: Wed, 27 Oct 2021 17:00:02 GMT  
		Size: 724.2 KB (724238 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:uclibc` - linux; arm64 variant v8

```console
$ docker pull busybox@sha256:ce53e9b0310447d0e851ff0d2c9b90f358dbffe719a723147e84b93a4799396c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **828.5 KB (828502 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cc54699a1d7e60dedfeabdf0e0fee681842a16a038ae551ad93b923457b031ab`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:39:27 GMT
ADD file:0e9e223690df05d052434f07b187d500e9544cf4a10992cb1cf17bbf0f9d261d in / 
# Wed, 27 Oct 2021 17:39:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:8ba530ca112c38dd01fa8c96a709f82e5e8cbb425af71f1185efc9ed2c1561d6`  
		Last Modified: Wed, 27 Oct 2021 17:40:27 GMT  
		Size: 828.5 KB (828502 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:uclibc` - linux; 386

```console
$ docker pull busybox@sha256:c71cb4f7e8ececaffb34037c2637dc86820e4185100e18b4d02d613a9bd772af
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **728.2 KB (728191 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:046e74e02754f28137f587f303f9852c82f3ec76fff1f2f278655b3e1193ad3e`
-	Default Command: `["sh"]`

```dockerfile
# Tue, 14 Sep 2021 01:38:33 GMT
ADD file:4143b23f3d54e871d1a69c3518704e9e6ebed52dcc278b5f1dbcd11343940898 in / 
# Tue, 14 Sep 2021 01:38:33 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:f835a1acb88b23855be36becf1ebe0a310ab5761b04c3fb24d0c94c41f7c3e41`  
		Last Modified: Tue, 14 Sep 2021 01:40:49 GMT  
		Size: 728.2 KB (728191 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:uclibc` - linux; mips64le

```console
$ docker pull busybox@sha256:77df281071dd7e01972ec2c4a33c1a6c00d13b24238375fd6622fce97f622fa2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **960.5 KB (960459 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:572b466fbb42b288f43e2d90cf55e26d33cbdd19d667aa31dd20c2628e344f38`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:07:29 GMT
ADD file:82547d0ac8de48ae2eb3f40f43a0e1635155361fc9e20b223e87d9de0ca27e96 in / 
# Wed, 27 Oct 2021 17:07:29 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3d8d4c508e7c9189254e23bfdd5f30c93223516f57715ee5bd0cc85b94d849ee`  
		Last Modified: Wed, 27 Oct 2021 17:07:59 GMT  
		Size: 960.5 KB (960459 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:uclibc` - linux; riscv64

```console
$ docker pull busybox@sha256:06b206c1f1a38094697c7e8bf868f9d326e56a256bc516dbb8ff0ee9c1178999
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **905.3 KB (905306 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a021c599555f76331fddec6e6ecc87cfb92cfd26ab42ef0b968dacb9890cefdc`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:12:42 GMT
ADD file:bbfcdc6a5eda2a3b7965fc2b73a783bf34893ee2cced227315f8468489ec1dee in / 
# Wed, 27 Oct 2021 17:12:43 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:0b2bb6a84cb70a09486f5568f13ac57db414fb39197ce05043689bdeae75744e`  
		Last Modified: Wed, 27 Oct 2021 17:23:32 GMT  
		Size: 905.3 KB (905306 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
