## `busybox:musl`

```console
$ docker pull busybox@sha256:13b27cf92cc81d1f4c1584360942bedce72e98af80aa273f77382715a33f68c2
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 7
	-	linux; amd64
	-	linux; arm variant v6
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; ppc64le
	-	linux; s390x

### `busybox:musl` - linux; amd64

```console
$ docker pull busybox@sha256:c57be995c4c3bb22b723c49d74fc3b5a4510dfbd8be28b5d021d7a4a53a4bab1
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **843.3 KB (843266 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1b408f772ab08633f27bf8918481dfd3fe2f7689b23a50897cb7cef2e17a6cb8`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:19:58 GMT
ADD file:58dbb8135e92be6f243e74462af8e85fd67417ba0281b81ea7742af344794c3f in / 
# Wed, 27 Oct 2021 17:19:58 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:b0e194f8faadf770ca46f1caf5b5ce0bb82ef76806729e22c1a20aff0c29c1f6`  
		Last Modified: Wed, 27 Oct 2021 17:20:59 GMT  
		Size: 843.3 KB (843266 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:musl` - linux; arm variant v6

```console
$ docker pull busybox@sha256:4ecc3dc2e06a24df931cb719c3784611d15721c3cb64ab069141071b73f6598b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **952.5 KB (952546 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:327d97ae18db017776599d9b3713503c03974d14346adb99117a186bed6a4d47`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:49:25 GMT
ADD file:c9029a61e764f7eee2bb75e9c227970c7a9e7e81d2d2b2370b7cfa8cff8dddde in / 
# Wed, 27 Oct 2021 17:49:26 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:dac1aaad9ca43e5b6a33470c2cfc6cf3127115e539cae8bc865a8c99d9232727`  
		Last Modified: Wed, 27 Oct 2021 17:50:33 GMT  
		Size: 952.5 KB (952546 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:musl` - linux; arm variant v7

```console
$ docker pull busybox@sha256:e23f636436b365735d7c726df3e36e48fe309afaa422a503bd8ecf9740742f9c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **834.7 KB (834677 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:27fba482d17b4205fe8a01218cc62d830b45f0a1e9ee13eeb81cf2cd7a69390d`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 16:58:07 GMT
ADD file:242e49f9869dfe3d8b59aee3801ef14dac3c58176f6cea670614284afd50ce8d in / 
# Wed, 27 Oct 2021 16:58:08 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:9d172075913815538a77dd7a7ad3bbefc76474354936eddd73cd933e9776b8e2`  
		Last Modified: Wed, 27 Oct 2021 17:00:50 GMT  
		Size: 834.7 KB (834677 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:musl` - linux; arm64 variant v8

```console
$ docker pull busybox@sha256:976ce0d1af7874bc0216c19efbd9178cc0455513a963cd834a35b9e732e298fb
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **882.2 KB (882176 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:69552745f9d2985d9bb00c5d912723a5f4a9afd133966cf8982b06067a3f7cf7`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:39:41 GMT
ADD file:4562e1c40e298277fc65259e3b29d3b72f89a28088f94bcae8e543df2ede1f5d in / 
# Wed, 27 Oct 2021 17:39:41 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:20a2e0c8f8c2704a1cd5560c3a242607af57c42ae6a3109108be0303d32092b1`  
		Last Modified: Wed, 27 Oct 2021 17:41:05 GMT  
		Size: 882.2 KB (882176 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:musl` - linux; 386

```console
$ docker pull busybox@sha256:2eed3c17cd8b978262caaed66dca91a6d23f654f7898f3fabeb09151b31c32e3
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **842.7 KB (842669 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f8748766cfd16c8edfbd2abe6a441ddfdeae7dd62276b21a0b5f2314568db712`
-	Default Command: `["sh"]`

```dockerfile
# Tue, 14 Sep 2021 01:38:53 GMT
ADD file:6ff60306035d5794506a2bec937e683fb5a799fabf489986cf76830451f679d5 in / 
# Tue, 14 Sep 2021 01:38:53 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:7d0e04faa0be851392cfcbd49724f856f34d5fb7c10d0b1ed1dea8bc94d4d4b1`  
		Last Modified: Tue, 14 Sep 2021 01:41:31 GMT  
		Size: 842.7 KB (842669 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:musl` - linux; ppc64le

```console
$ docker pull busybox@sha256:7696de5d912dc3db582ba08c435f8b1b01a0cf845db7e44f1ad4269b656b627a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **919.2 KB (919168 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:0d6acacaf155166a2a58e39cd33187987fe4b36400dedbe22607902b50f3793d`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 19:12:21 GMT
ADD file:b19d93c361eebcc52af7de98cdbacec78af10e600d8cd334b9301752ba712766 in / 
# Wed, 27 Oct 2021 19:12:30 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:2abb4f53c2ba292747e959a7d205240ab584ca7650ff38823327a68680916549`  
		Last Modified: Wed, 27 Oct 2021 19:13:53 GMT  
		Size: 919.2 KB (919168 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:musl` - linux; s390x

```console
$ docker pull busybox@sha256:2839b6e6e257d487803c6942666415c052a1e1c11e3d41d57fe30d25c50da924
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **893.9 KB (893870 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4c5d5f91ce1964dd844df1a44fba72b596f170eb0554cf45c195d2ec65849f23`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:41:26 GMT
ADD file:5722acc1a5fd74b7ff5c73c86ee2da0bfa9560c036f0aee24eac818305c5b2b6 in / 
# Wed, 27 Oct 2021 17:41:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3f4a2ae691cff8edfecb1d55dc2d2b5a308572dbba3c2d79fc65547c60d35752`  
		Last Modified: Wed, 27 Oct 2021 17:42:26 GMT  
		Size: 893.9 KB (893870 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
