## `busybox:latest`

```console
$ docker pull busybox@sha256:15e927f78df2cc772b70713543d6b651e3cd8370abf86b2ea4644a9fba21107f
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 10
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v6
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; riscv64
	-	linux; s390x

### `busybox:latest` - linux; amd64

```console
$ docker pull busybox@sha256:6066ca124f8c2686b7ae71aa1d6583b28c6dc3df3bdc386f2c89b92162c597d9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **772.8 KB (772798 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cabb9f684f8ba3edb303d578bfd7d709d853539ea1b420a3f6c81a08e85bb3d7`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:19:45 GMT
ADD file:88b76d15f53403ca217f156c3fa01df961ddf224a193cf39ebe51678b77cf9cf in / 
# Wed, 27 Oct 2021 17:19:45 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:01c2cdc137396a9b78be86c47e5773388e6eed953dec79e257777518134b45f1`  
		Last Modified: Wed, 27 Oct 2021 17:20:27 GMT  
		Size: 772.8 KB (772798 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:latest` - linux; arm variant v5

```console
$ docker pull busybox@sha256:399e1e4a0d587717dc9e3a85150cec8498cb6dc73dcb7eddb94959fedb331104
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **756.2 KB (756158 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f73013f2ba16094e6beade24ce90aa5fcd023baff78a1438c7221592d922d780`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:48:26 GMT
ADD file:c9324c40aae3b3e81b9315fe32797d78d508fb1823de84f7cd1d79e474418a8a in / 
# Wed, 27 Oct 2021 17:48:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3a7a3789f9861a9ab57dd6819c9dcdfcc9a4ea3172b32c88423f9630fb2c0816`  
		Last Modified: Wed, 27 Oct 2021 17:50:10 GMT  
		Size: 756.2 KB (756158 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:latest` - linux; arm variant v6

```console
$ docker pull busybox@sha256:4ecc3dc2e06a24df931cb719c3784611d15721c3cb64ab069141071b73f6598b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **952.5 KB (952546 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:327d97ae18db017776599d9b3713503c03974d14346adb99117a186bed6a4d47`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:49:25 GMT
ADD file:c9029a61e764f7eee2bb75e9c227970c7a9e7e81d2d2b2370b7cfa8cff8dddde in / 
# Wed, 27 Oct 2021 17:49:26 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:dac1aaad9ca43e5b6a33470c2cfc6cf3127115e539cae8bc865a8c99d9232727`  
		Last Modified: Wed, 27 Oct 2021 17:50:33 GMT  
		Size: 952.5 KB (952546 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:latest` - linux; arm variant v7

```console
$ docker pull busybox@sha256:53c212bcc0501f011c232df0fb6c837651d0b2f3257b6478a50c0e006b0dabc5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **724.2 KB (724238 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:17d83c72aaa7f419b771dad67eb87d285faa57f1b0d93ed2c9b5b45ea6879065`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 16:57:33 GMT
ADD file:2b15d7b82fc9c84090d98a80296a522d922e42b63fbd9ea47aa2bf62c14aa768 in / 
# Wed, 27 Oct 2021 16:57:33 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:da72ebe61afbc4deef433f5180ccb61df896dfb324e08c0a03b7c43a7c0347b4`  
		Last Modified: Wed, 27 Oct 2021 17:00:02 GMT  
		Size: 724.2 KB (724238 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:latest` - linux; arm64 variant v8

```console
$ docker pull busybox@sha256:ce53e9b0310447d0e851ff0d2c9b90f358dbffe719a723147e84b93a4799396c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **828.5 KB (828502 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cc54699a1d7e60dedfeabdf0e0fee681842a16a038ae551ad93b923457b031ab`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:39:27 GMT
ADD file:0e9e223690df05d052434f07b187d500e9544cf4a10992cb1cf17bbf0f9d261d in / 
# Wed, 27 Oct 2021 17:39:27 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:8ba530ca112c38dd01fa8c96a709f82e5e8cbb425af71f1185efc9ed2c1561d6`  
		Last Modified: Wed, 27 Oct 2021 17:40:27 GMT  
		Size: 828.5 KB (828502 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:latest` - linux; 386

```console
$ docker pull busybox@sha256:c71cb4f7e8ececaffb34037c2637dc86820e4185100e18b4d02d613a9bd772af
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **728.2 KB (728191 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:046e74e02754f28137f587f303f9852c82f3ec76fff1f2f278655b3e1193ad3e`
-	Default Command: `["sh"]`

```dockerfile
# Tue, 14 Sep 2021 01:38:33 GMT
ADD file:4143b23f3d54e871d1a69c3518704e9e6ebed52dcc278b5f1dbcd11343940898 in / 
# Tue, 14 Sep 2021 01:38:33 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:f835a1acb88b23855be36becf1ebe0a310ab5761b04c3fb24d0c94c41f7c3e41`  
		Last Modified: Tue, 14 Sep 2021 01:40:49 GMT  
		Size: 728.2 KB (728191 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:latest` - linux; mips64le

```console
$ docker pull busybox@sha256:77df281071dd7e01972ec2c4a33c1a6c00d13b24238375fd6622fce97f622fa2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **960.5 KB (960459 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:572b466fbb42b288f43e2d90cf55e26d33cbdd19d667aa31dd20c2628e344f38`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:07:29 GMT
ADD file:82547d0ac8de48ae2eb3f40f43a0e1635155361fc9e20b223e87d9de0ca27e96 in / 
# Wed, 27 Oct 2021 17:07:29 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:3d8d4c508e7c9189254e23bfdd5f30c93223516f57715ee5bd0cc85b94d849ee`  
		Last Modified: Wed, 27 Oct 2021 17:07:59 GMT  
		Size: 960.5 KB (960459 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:latest` - linux; ppc64le

```console
$ docker pull busybox@sha256:b70f0f45692830c2990b42f770aa29488c20ac41f1c3dcaa242920b73cb1399b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.5 MB (2484095 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:defb34012ae5b6a308b2629e4a6b299d20a193e3475fc98c4fba27f35819061b`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 19:12:06 GMT
ADD file:79fcd19d69e0c4f07016224ba1b588b8c92aa08c3af515be806d4714d6118923 in / 
# Wed, 27 Oct 2021 19:12:09 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:8dcfc896d3701ab1ec378f31cf3076493c9b8c588f103af7901ce812bb09b317`  
		Last Modified: Wed, 27 Oct 2021 19:13:34 GMT  
		Size: 2.5 MB (2484095 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:latest` - linux; riscv64

```console
$ docker pull busybox@sha256:06b206c1f1a38094697c7e8bf868f9d326e56a256bc516dbb8ff0ee9c1178999
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **905.3 KB (905306 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a021c599555f76331fddec6e6ecc87cfb92cfd26ab42ef0b968dacb9890cefdc`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:12:42 GMT
ADD file:bbfcdc6a5eda2a3b7965fc2b73a783bf34893ee2cced227315f8468489ec1dee in / 
# Wed, 27 Oct 2021 17:12:43 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:0b2bb6a84cb70a09486f5568f13ac57db414fb39197ce05043689bdeae75744e`  
		Last Modified: Wed, 27 Oct 2021 17:23:32 GMT  
		Size: 905.3 KB (905306 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:latest` - linux; s390x

```console
$ docker pull busybox@sha256:86824a27910bd2a8c6a8478fe99206e6cf4bcada7cb8435c0060cbe885559e53
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.0 MB (2006829 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9e119a54a2534d8ae95d15840f3c4feed03866eb4aee947c49e679206be94893`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:41:19 GMT
ADD file:09207d85b622b42ab9880ec4cbfe2212390ba7c1b0704b91bd3db041db9d9e03 in / 
# Wed, 27 Oct 2021 17:41:19 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:120da903ae50712b69209ab560f7855f3ffed8e6cd9c78dcd74b7e8754c5b168`  
		Last Modified: Wed, 27 Oct 2021 17:42:10 GMT  
		Size: 2.0 MB (2006829 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
