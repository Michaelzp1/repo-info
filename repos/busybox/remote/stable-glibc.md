## `busybox:stable-glibc`

```console
$ docker pull busybox@sha256:49ccc099fce12d9673767a60f88761bd56f2ddef85bbdc605fc9c5db2dd5bcb3
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `busybox:stable-glibc` - linux; amd64

```console
$ docker pull busybox@sha256:a158839ba99a90008ddd922dddb869d4a998f60339ee0c41ef9009e60995247a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2566111 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f17f5d94ca617134e143b569c2ec48a7d5864d8387d1191e03116c7ad428bfdf`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:19:53 GMT
ADD file:05820ca2a136d0bd67d2c84ac5294fb0b112df68fcd3482ed77f4d47d30c2106 in / 
# Wed, 27 Oct 2021 17:19:53 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:df20d59888d8cd750954e0854d27df703b73b98999981beeb0ec769c8a707226`  
		Last Modified: Wed, 27 Oct 2021 17:20:43 GMT  
		Size: 2.6 MB (2566111 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable-glibc` - linux; arm variant v5

```console
$ docker pull busybox@sha256:068abb3d195234a48605f5067881edfa228840ea5c83245d61e3798979c02961
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **1.9 MB (1930775 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:454a6225af3074772111413ba8b568c07d177f2475e59cbbe6d909156742023f`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:48:41 GMT
ADD file:7bff807316ba841e7e1c392a909bc4d4ab078c4e0ee9e99e62bd89c5002bbc21 in / 
# Wed, 27 Oct 2021 17:48:41 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:58e53cd44176b2564294ddc75394d3683931c011e7c2db9794e90ef796228270`  
		Last Modified: Wed, 27 Oct 2021 17:50:34 GMT  
		Size: 1.9 MB (1930775 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable-glibc` - linux; arm variant v7

```console
$ docker pull busybox@sha256:3c56c483948e49726761f4bf2ae09cf227db38d75362ab40024cfc7deda4b04f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **1.7 MB (1678694 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:393a97107921f708c9df011fd1841f3a1c34a1de86c96cd9ef6033c9708f1f11`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 16:57:50 GMT
ADD file:a8b26211fe41f82dec07ffe639f4c6ef4880076f74d8b5f0eb59e34546b325e9 in / 
# Wed, 27 Oct 2021 16:57:50 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:fa418c43645700e527bff2dfb63459af17af7b059c31f60f412699e48fe122a2`  
		Last Modified: Wed, 27 Oct 2021 17:00:27 GMT  
		Size: 1.7 MB (1678694 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable-glibc` - linux; arm64 variant v8

```console
$ docker pull busybox@sha256:a72f19c71777a59bf2ea290ceefbe6537e2c8c544463cd57859ef5d432fb244b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.0 MB (1996159 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6fb1464c3dc04a6f8a7c92cdb9f3dfc5ab1c9116061a85ddc90825680be154eb`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:39:34 GMT
ADD file:8bdeb927d5560194f8ba5688d4d9ee1ff7254928145aafe79f7aa326f70ab234 in / 
# Wed, 27 Oct 2021 17:39:34 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:aae080e05ab28f73910de7f8e9f092f5c7eb9f2658363454a3361e69e5723954`  
		Last Modified: Wed, 27 Oct 2021 17:40:46 GMT  
		Size: 2.0 MB (1996159 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable-glibc` - linux; 386

```console
$ docker pull busybox@sha256:f2005b8003d6cba4248292df72d03ce8b1aaa048e27eb46a2772a3c486de9212
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.2 MB (2226613 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:edd609bf27e9452d3644678d41c01a84556e59c3f1ab16560448a7161d552546`
-	Default Command: `["sh"]`

```dockerfile
# Tue, 14 Sep 2021 01:38:42 GMT
ADD file:0fa6993c66b449fe9530e9027f7237d4fd0cc9e161a32ff73a9014864cfe7bfd in / 
# Tue, 14 Sep 2021 01:38:42 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:63502a2694813d8f0580c464540ecf7c7f5c6017f74a988c7f6d3b669adfc9c5`  
		Last Modified: Tue, 14 Sep 2021 01:41:11 GMT  
		Size: 2.2 MB (2226613 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable-glibc` - linux; mips64le

```console
$ docker pull busybox@sha256:6a2a782c46dedb94c1370a60192df26e00da690b90fa16cb4eb11352e45ec665
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.2 MB (2220711 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:25bbb10f71e32f9c63967dfd90d2213df5f3c8ef303bb47171f3920bb066ae7f`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:07:38 GMT
ADD file:47a6e39f530c1c6dcfc7fbcce83900c83627949ac78e6a74132145f11b427b59 in / 
# Wed, 27 Oct 2021 17:07:38 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:b7954f921f64e29ca6451787b44b6ed22eb7177760e8d8fa31f60c312d7ae676`  
		Last Modified: Wed, 27 Oct 2021 17:08:17 GMT  
		Size: 2.2 MB (2220711 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable-glibc` - linux; ppc64le

```console
$ docker pull busybox@sha256:b70f0f45692830c2990b42f770aa29488c20ac41f1c3dcaa242920b73cb1399b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.5 MB (2484095 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:defb34012ae5b6a308b2629e4a6b299d20a193e3475fc98c4fba27f35819061b`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 19:12:06 GMT
ADD file:79fcd19d69e0c4f07016224ba1b588b8c92aa08c3af515be806d4714d6118923 in / 
# Wed, 27 Oct 2021 19:12:09 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:8dcfc896d3701ab1ec378f31cf3076493c9b8c588f103af7901ce812bb09b317`  
		Last Modified: Wed, 27 Oct 2021 19:13:34 GMT  
		Size: 2.5 MB (2484095 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `busybox:stable-glibc` - linux; s390x

```console
$ docker pull busybox@sha256:86824a27910bd2a8c6a8478fe99206e6cf4bcada7cb8435c0060cbe885559e53
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.0 MB (2006829 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9e119a54a2534d8ae95d15840f3c4feed03866eb4aee947c49e679206be94893`
-	Default Command: `["sh"]`

```dockerfile
# Wed, 27 Oct 2021 17:41:19 GMT
ADD file:09207d85b622b42ab9880ec4cbfe2212390ba7c1b0704b91bd3db041db9d9e03 in / 
# Wed, 27 Oct 2021 17:41:19 GMT
CMD ["sh"]
```

-	Layers:
	-	`sha256:120da903ae50712b69209ab560f7855f3ffed8e6cd9c78dcd74b7e8754c5b168`  
		Last Modified: Wed, 27 Oct 2021 17:42:10 GMT  
		Size: 2.0 MB (2006829 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
