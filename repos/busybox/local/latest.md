# `busybox:1.34.1`

## Docker Metadata

- Image ID: `sha256:cabb9f684f8ba3edb303d578bfd7d709d853539ea1b420a3f6c81a08e85bb3d7`
- Created: `2021-10-27T17:19:45.337061231Z`
- Virtual Size: ~ 1.24 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["sh"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
