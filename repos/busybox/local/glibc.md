# `busybox:1.34.1-glibc`

## Docker Metadata

- Image ID: `sha256:f17f5d94ca617134e143b569c2ec48a7d5864d8387d1191e03116c7ad428bfdf`
- Created: `2021-10-27T17:19:53.416951769Z`
- Virtual Size: ~ 4.79 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["sh"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
