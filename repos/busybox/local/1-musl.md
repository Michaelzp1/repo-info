# `busybox:1.34.1-musl`

## Docker Metadata

- Image ID: `sha256:1b408f772ab08633f27bf8918481dfd3fe2f7689b23a50897cb7cef2e17a6cb8`
- Created: `2021-10-27T17:19:58.822233007Z`
- Virtual Size: ~ 1.43 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["sh"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
