# `znc:1.8.2`

## Docker Metadata

- Image ID: `sha256:82de5c6341c02822af90820d98d694244ef1beb5d8ef9c6cf270a5d6cf003bd0`
- Created: `2021-10-01T23:09:18.755398815Z`
- Virtual Size: ~ 408.19 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/entrypoint.sh"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `GPG_KEY=D5823CACB477191CAC0075555AE420CC0209989E`
  - `ZNC_VERSION=1.8.2`

## `apk` (`.apk`-based packages)

### `apk` package: `alpine-baselayout`

```console
alpine-baselayout-3.2.0-r8 description:
Alpine base dir structure and init scripts

alpine-baselayout-3.2.0-r8 webpage:
https://git.alpinelinux.org/cgit/aports/tree/main/alpine-baselayout

alpine-baselayout-3.2.0-r8 installed size:
400 KiB

alpine-baselayout-3.2.0-r8 license:
GPL-2.0-only

```

### `apk` package: `alpine-keys`

```console
alpine-keys-2.2-r0 description:
Public keys for Alpine Linux packages

alpine-keys-2.2-r0 webpage:
https://alpinelinux.org

alpine-keys-2.2-r0 installed size:
104 KiB

alpine-keys-2.2-r0 license:
MIT

```

### `apk` package: `apk-tools`

```console
apk-tools-2.12.7-r0 description:
Alpine Package Keeper - package manager for alpine

apk-tools-2.12.7-r0 webpage:
https://gitlab.alpinelinux.org/alpine/apk-tools

apk-tools-2.12.7-r0 installed size:
304 KiB

apk-tools-2.12.7-r0 license:
GPL-2.0-only

```

### `apk` package: `binutils`

```console
binutils-2.35.2-r1 description:
Tools necessary to build programs

binutils-2.35.2-r1 webpage:
https://www.gnu.org/software/binutils/

binutils-2.35.2-r1 installed size:
9596 KiB

binutils-2.35.2-r1 license:
GPL-2.0 GPL-3.0-or-later LGPL-2.0 BSD

```

### `apk` package: `boost`

```console
boost-1.72.0-r6 description:
Free peer-reviewed portable C++ source libraries

boost-1.72.0-r6 webpage:
https://www.boost.org/

boost-1.72.0-r6 installed size:
952 KiB

boost-1.72.0-r6 license:
custom

```

### `apk` package: `boost-atomic`

```console
boost-atomic-1.72.0-r6 description:
Boost atomic shared library

boost-atomic-1.72.0-r6 webpage:
https://www.boost.org/

boost-atomic-1.72.0-r6 installed size:
28 KiB

boost-atomic-1.72.0-r6 license:
custom

```

### `apk` package: `boost-chrono`

```console
boost-chrono-1.72.0-r6 description:
Boost chrono shared library

boost-chrono-1.72.0-r6 webpage:
https://www.boost.org/

boost-chrono-1.72.0-r6 installed size:
60 KiB

boost-chrono-1.72.0-r6 license:
custom

```

### `apk` package: `boost-container`

```console
boost-container-1.72.0-r6 description:
Boost container shared library

boost-container-1.72.0-r6 webpage:
https://www.boost.org/

boost-container-1.72.0-r6 installed size:
96 KiB

boost-container-1.72.0-r6 license:
custom

```

### `apk` package: `boost-context`

```console
boost-context-1.72.0-r6 description:
Boost context shared library

boost-context-1.72.0-r6 webpage:
https://www.boost.org/

boost-context-1.72.0-r6 installed size:
28 KiB

boost-context-1.72.0-r6 license:
custom

```

### `apk` package: `boost-contract`

```console
boost-contract-1.72.0-r6 description:
Boost contract shared library

boost-contract-1.72.0-r6 webpage:
https://www.boost.org/

boost-contract-1.72.0-r6 installed size:
152 KiB

boost-contract-1.72.0-r6 license:
custom

```

### `apk` package: `boost-coroutine`

```console
boost-coroutine-1.72.0-r6 description:
Boost coroutine shared library

boost-coroutine-1.72.0-r6 webpage:
https://www.boost.org/

boost-coroutine-1.72.0-r6 installed size:
76 KiB

boost-coroutine-1.72.0-r6 license:
custom

```

### `apk` package: `boost-date_time`

```console
boost-date_time-1.72.0-r6 description:
Boost date_time shared library

boost-date_time-1.72.0-r6 webpage:
https://www.boost.org/

boost-date_time-1.72.0-r6 installed size:
76 KiB

boost-date_time-1.72.0-r6 license:
custom

```

### `apk` package: `boost-fiber`

```console
boost-fiber-1.72.0-r6 description:
Boost fiber shared library

boost-fiber-1.72.0-r6 webpage:
https://www.boost.org/

boost-fiber-1.72.0-r6 installed size:
92 KiB

boost-fiber-1.72.0-r6 license:
custom

```

### `apk` package: `boost-filesystem`

```console
boost-filesystem-1.72.0-r6 description:
Boost filesystem shared library

boost-filesystem-1.72.0-r6 webpage:
https://www.boost.org/

boost-filesystem-1.72.0-r6 installed size:
136 KiB

boost-filesystem-1.72.0-r6 license:
custom

```

### `apk` package: `boost-graph`

```console
boost-graph-1.72.0-r6 description:
Boost graph shared library

boost-graph-1.72.0-r6 webpage:
https://www.boost.org/

boost-graph-1.72.0-r6 installed size:
316 KiB

boost-graph-1.72.0-r6 license:
custom

```

### `apk` package: `boost-iostreams`

```console
boost-iostreams-1.72.0-r6 description:
Boost iostreams shared library

boost-iostreams-1.72.0-r6 webpage:
https://www.boost.org/

boost-iostreams-1.72.0-r6 installed size:
132 KiB

boost-iostreams-1.72.0-r6 license:
custom

```

### `apk` package: `boost-libs`

```console
boost-libs-1.72.0-r6 description:
Boost shared libraries

boost-libs-1.72.0-r6 webpage:
https://www.boost.org/

boost-libs-1.72.0-r6 installed size:
4096 B

boost-libs-1.72.0-r6 license:
custom

```

### `apk` package: `boost-locale`

```console
boost-locale-1.72.0-r6 description:
Boost locale shared library

boost-locale-1.72.0-r6 webpage:
https://www.boost.org/

boost-locale-1.72.0-r6 installed size:
648 KiB

boost-locale-1.72.0-r6 license:
custom

```

### `apk` package: `boost-log`

```console
boost-log-1.72.0-r6 description:
Boost log shared library

boost-log-1.72.0-r6 webpage:
https://www.boost.org/

boost-log-1.72.0-r6 installed size:
936 KiB

boost-log-1.72.0-r6 license:
custom

```

### `apk` package: `boost-log_setup`

```console
boost-log_setup-1.72.0-r6 description:
Boost log_setup shared library

boost-log_setup-1.72.0-r6 webpage:
https://www.boost.org/

boost-log_setup-1.72.0-r6 installed size:
776 KiB

boost-log_setup-1.72.0-r6 license:
custom

```

### `apk` package: `boost-math`

```console
boost-math-1.72.0-r6 description:
Boost math shared library

boost-math-1.72.0-r6 webpage:
https://www.boost.org/

boost-math-1.72.0-r6 installed size:
1724 KiB

boost-math-1.72.0-r6 license:
custom

```

### `apk` package: `boost-prg_exec_monitor`

```console
boost-prg_exec_monitor-1.72.0-r6 description:
Boost prg_exec_monitor shared library

boost-prg_exec_monitor-1.72.0-r6 webpage:
https://www.boost.org/

boost-prg_exec_monitor-1.72.0-r6 installed size:
100 KiB

boost-prg_exec_monitor-1.72.0-r6 license:
custom

```

### `apk` package: `boost-program_options`

```console
boost-program_options-1.72.0-r6 description:
Boost program_options shared library

boost-program_options-1.72.0-r6 webpage:
https://www.boost.org/

boost-program_options-1.72.0-r6 installed size:
540 KiB

boost-program_options-1.72.0-r6 license:
custom

```

### `apk` package: `boost-python3`

```console
boost-python3-1.72.0-r6 description:
Boost python3 shared library

boost-python3-1.72.0-r6 webpage:
https://www.boost.org/

boost-python3-1.72.0-r6 installed size:
268 KiB

boost-python3-1.72.0-r6 license:
custom

```

### `apk` package: `boost-random`

```console
boost-random-1.72.0-r6 description:
Boost random shared library

boost-random-1.72.0-r6 webpage:
https://www.boost.org/

boost-random-1.72.0-r6 installed size:
56 KiB

boost-random-1.72.0-r6 license:
custom

```

### `apk` package: `boost-regex`

```console
boost-regex-1.72.0-r6 description:
Boost regex shared library

boost-regex-1.72.0-r6 webpage:
https://www.boost.org/

boost-regex-1.72.0-r6 installed size:
1164 KiB

boost-regex-1.72.0-r6 license:
custom

```

### `apk` package: `boost-serialization`

```console
boost-serialization-1.72.0-r6 description:
Boost serialization shared library

boost-serialization-1.72.0-r6 webpage:
https://www.boost.org/

boost-serialization-1.72.0-r6 installed size:
284 KiB

boost-serialization-1.72.0-r6 license:
custom

```

### `apk` package: `boost-stacktrace_basic`

```console
boost-stacktrace_basic-1.72.0-r6 description:
Boost stacktrace_basic shared library

boost-stacktrace_basic-1.72.0-r6 webpage:
https://www.boost.org/

boost-stacktrace_basic-1.72.0-r6 installed size:
32 KiB

boost-stacktrace_basic-1.72.0-r6 license:
custom

```

### `apk` package: `boost-stacktrace_noop`

```console
boost-stacktrace_noop-1.72.0-r6 description:
Boost stacktrace_noop shared library

boost-stacktrace_noop-1.72.0-r6 webpage:
https://www.boost.org/

boost-stacktrace_noop-1.72.0-r6 installed size:
28 KiB

boost-stacktrace_noop-1.72.0-r6 license:
custom

```

### `apk` package: `boost-system`

```console
boost-system-1.72.0-r6 description:
Boost system shared library

boost-system-1.72.0-r6 webpage:
https://www.boost.org/

boost-system-1.72.0-r6 installed size:
28 KiB

boost-system-1.72.0-r6 license:
custom

```

### `apk` package: `boost-thread`

```console
boost-thread-1.72.0-r6 description:
Boost thread shared library

boost-thread-1.72.0-r6 webpage:
https://www.boost.org/

boost-thread-1.72.0-r6 installed size:
172 KiB

boost-thread-1.72.0-r6 license:
custom

```

### `apk` package: `boost-timer`

```console
boost-timer-1.72.0-r6 description:
Boost timer shared library

boost-timer-1.72.0-r6 webpage:
https://www.boost.org/

boost-timer-1.72.0-r6 installed size:
48 KiB

boost-timer-1.72.0-r6 license:
custom

```

### `apk` package: `boost-type_erasure`

```console
boost-type_erasure-1.72.0-r6 description:
Boost type_erasure shared library

boost-type_erasure-1.72.0-r6 webpage:
https://www.boost.org/

boost-type_erasure-1.72.0-r6 installed size:
92 KiB

boost-type_erasure-1.72.0-r6 license:
custom

```

### `apk` package: `boost-unit_test_framework`

```console
boost-unit_test_framework-1.72.0-r6 description:
Boost unit_test_framework shared library

boost-unit_test_framework-1.72.0-r6 webpage:
https://www.boost.org/

boost-unit_test_framework-1.72.0-r6 installed size:
728 KiB

boost-unit_test_framework-1.72.0-r6 license:
custom

```

### `apk` package: `boost-wave`

```console
boost-wave-1.72.0-r6 description:
Boost wave shared library

boost-wave-1.72.0-r6 webpage:
https://www.boost.org/

boost-wave-1.72.0-r6 installed size:
856 KiB

boost-wave-1.72.0-r6 license:
custom

```

### `apk` package: `boost-wserialization`

```console
boost-wserialization-1.72.0-r6 description:
Boost wserialization shared library

boost-wserialization-1.72.0-r6 webpage:
https://www.boost.org/

boost-wserialization-1.72.0-r6 installed size:
196 KiB

boost-wserialization-1.72.0-r6 license:
custom

```

### `apk` package: `brotli-libs`

```console
brotli-libs-1.0.9-r3 description:
Generic lossless compressor (libraries)

brotli-libs-1.0.9-r3 webpage:
https://github.com/google/brotli

brotli-libs-1.0.9-r3 installed size:
720 KiB

brotli-libs-1.0.9-r3 license:
MIT

```

### `apk` package: `build-base`

```console
build-base-0.5-r2 description:
Meta package for build base

build-base-0.5-r2 webpage:
http://dev.alpinelinux.org/cgit

build-base-0.5-r2 installed size:
4096 B

build-base-0.5-r2 license:
none

```

### `apk` package: `busybox`

```console
busybox-1.32.1-r6 description:
Size optimized toolbox of many common UNIX utilities

busybox-1.32.1-r6 webpage:
https://busybox.net/

busybox-1.32.1-r6 installed size:
924 KiB

busybox-1.32.1-r6 license:
GPL-2.0-only

```

### `apk` package: `ca-certificates`

```console
ca-certificates-20191127-r5 description:
Common CA certificates PEM files from Mozilla

ca-certificates-20191127-r5 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-20191127-r5 installed size:
672 KiB

ca-certificates-20191127-r5 license:
MPL-2.0 AND MIT

```

### `apk` package: `ca-certificates-bundle`

```console
ca-certificates-bundle-20191127-r5 description:
Pre generated bundle of Mozilla certificates

ca-certificates-bundle-20191127-r5 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-bundle-20191127-r5 installed size:
228 KiB

ca-certificates-bundle-20191127-r5 license:
MPL-2.0 AND MIT

```

### `apk` package: `cmake`

```console
cmake-3.18.4-r1 description:
Cross-platform, open-source make system

cmake-3.18.4-r1 webpage:
https://www.cmake.org/

cmake-3.18.4-r1 installed size:
44 MiB

cmake-3.18.4-r1 license:
BSD-3-Clause

```

### `apk` package: `cyrus-sasl`

```console
cyrus-sasl-2.1.27-r10 description:
Cyrus Simple Authentication Service Layer (SASL)

cyrus-sasl-2.1.27-r10 webpage:
https://www.cyrusimap.org/sasl/

cyrus-sasl-2.1.27-r10 installed size:
160 KiB

cyrus-sasl-2.1.27-r10 license:
custom

```

### `apk` package: `expat`

```console
expat-2.2.10-r1 description:
XML Parser library written in C

expat-2.2.10-r1 webpage:
http://www.libexpat.org/

expat-2.2.10-r1 installed size:
184 KiB

expat-2.2.10-r1 license:
MIT

```

### `apk` package: `file`

```console
file-5.39-r0 description:
File type identification utility

file-5.39-r0 webpage:
https://www.darwinsys.com/file

file-5.39-r0 installed size:
40 KiB

file-5.39-r0 license:
BSD-2-Clause

```

### `apk` package: `fortify-headers`

```console
fortify-headers-1.1-r0 description:
standalone fortify source implementation

fortify-headers-1.1-r0 webpage:
http://git.2f30.org/fortify-headers/

fortify-headers-1.1-r0 installed size:
68 KiB

fortify-headers-1.1-r0 license:
BSD-0

```

### `apk` package: `g++`

```console
g++-10.2.1_pre1-r3 description:
GNU C++ standard library and compiler

g++-10.2.1_pre1-r3 webpage:
https://gcc.gnu.org

g++-10.2.1_pre1-r3 installed size:
61 MiB

g++-10.2.1_pre1-r3 license:
GPL-2.0-or-later LGPL-2.1-or-later

```

### `apk` package: `gcc`

```console
gcc-10.2.1_pre1-r3 description:
The GNU Compiler Collection

gcc-10.2.1_pre1-r3 webpage:
https://gcc.gnu.org

gcc-10.2.1_pre1-r3 installed size:
87 MiB

gcc-10.2.1_pre1-r3 license:
GPL-2.0-or-later LGPL-2.1-or-later

```

### `apk` package: `gdbm`

```console
gdbm-1.19-r0 description:
GNU dbm is a set of database routines that use extensible hashing

gdbm-1.19-r0 webpage:
https://www.gnu.org/software/gdbm/

gdbm-1.19-r0 installed size:
224 KiB

gdbm-1.19-r0 license:
GPL-3.0-or-later

```

### `apk` package: `gmp`

```console
gmp-6.2.1-r0 description:
free library for arbitrary precision arithmetic

gmp-6.2.1-r0 webpage:
https://gmplib.org/

gmp-6.2.1-r0 installed size:
416 KiB

gmp-6.2.1-r0 license:
LGPL-3.0-or-later OR GPL-2.0-or-later

```

### `apk` package: `heimdal-libs`

```console
heimdal-libs-7.7.0-r4 description:
Heimdal libraries

heimdal-libs-7.7.0-r4 webpage:
https://github.com/heimdal/

heimdal-libs-7.7.0-r4 installed size:
2912 KiB

heimdal-libs-7.7.0-r4 license:
BSD-3-Clause

```

### `apk` package: `icu`

```console
icu-67.1-r2 description:
International Components for Unicode library

icu-67.1-r2 webpage:
http://site.icu-project.org/

icu-67.1-r2 installed size:
648 KiB

icu-67.1-r2 license:
MIT ICU Unicode-TOU

```

### `apk` package: `icu-dev`

```console
icu-dev-67.1-r2 description:
International Components for Unicode library (development files)

icu-dev-67.1-r2 webpage:
http://site.icu-project.org/

icu-dev-67.1-r2 installed size:
4960 KiB

icu-dev-67.1-r2 license:
MIT ICU Unicode-TOU

```

### `apk` package: `icu-libs`

```console
icu-libs-67.1-r2 description:
International Components for Unicode library (libraries)

icu-libs-67.1-r2 webpage:
http://site.icu-project.org/

icu-libs-67.1-r2 installed size:
31 MiB

icu-libs-67.1-r2 license:
MIT ICU Unicode-TOU

```

### `apk` package: `isl22`

```console
isl22-0.22-r0 description:
An Integer Set Library for the Polyhedral Model

isl22-0.22-r0 webpage:
http://isl.gforge.inria.fr/

isl22-0.22-r0 installed size:
1648 KiB

isl22-0.22-r0 license:
MIT

```

### `apk` package: `krb5-conf`

```console
krb5-conf-1.0-r2 description:
Shared krb5.conf for both MIT krb5 and heimdal

krb5-conf-1.0-r2 webpage:
https://web.mit.edu/kerberos/www/

krb5-conf-1.0-r2 installed size:
12 KiB

krb5-conf-1.0-r2 license:
MIT

```

### `apk` package: `libacl`

```console
libacl-2.2.53-r0 description:
Dynamic library for access control list support

libacl-2.2.53-r0 webpage:
https://savannah.nongnu.org/projects/acl

libacl-2.2.53-r0 installed size:
44 KiB

libacl-2.2.53-r0 license:
LGPL-2.1-or-later AND GPL-2.0-or-later

```

### `apk` package: `libarchive`

```console
libarchive-3.5.1-r0 description:
library that can create and read several streaming archive formats

libarchive-3.5.1-r0 webpage:
https://libarchive.org/

libarchive-3.5.1-r0 installed size:
664 KiB

libarchive-3.5.1-r0 license:
BSD-2-Clause AND BSD-3-Clause AND Public-Domain

```

### `apk` package: `libatomic`

```console
libatomic-10.2.1_pre1-r3 description:
GCC Atomic library

libatomic-10.2.1_pre1-r3 webpage:
https://gcc.gnu.org

libatomic-10.2.1_pre1-r3 installed size:
40 KiB

libatomic-10.2.1_pre1-r3 license:
GPL-2.0-or-later LGPL-2.1-or-later

```

### `apk` package: `libbz2`

```console
libbz2-1.0.8-r1 description:
Shared library for bz2

libbz2-1.0.8-r1 webpage:
http://sources.redhat.com/bzip2

libbz2-1.0.8-r1 installed size:
72 KiB

libbz2-1.0.8-r1 license:
bzip2-1.0.6

```

### `apk` package: `libc-dev`

```console
libc-dev-0.7.2-r3 description:
Meta package to pull in correct libc

libc-dev-0.7.2-r3 webpage:
https://alpinelinux.org

libc-dev-0.7.2-r3 installed size:
4096 B

libc-dev-0.7.2-r3 license:
BSD-2-Clause AND BSD-3-Clause

```

### `apk` package: `libc-utils`

```console
libc-utils-0.7.2-r3 description:
Meta package to pull in correct libc

libc-utils-0.7.2-r3 webpage:
https://alpinelinux.org

libc-utils-0.7.2-r3 installed size:
4096 B

libc-utils-0.7.2-r3 license:
BSD-2-Clause AND BSD-3-Clause

```

### `apk` package: `libcom_err`

```console
libcom_err-1.45.7-r0 description:
Common error description library

libcom_err-1.45.7-r0 webpage:
http://e2fsprogs.sourceforge.net

libcom_err-1.45.7-r0 installed size:
24 KiB

libcom_err-1.45.7-r0 license:
GPL-2.0-or-later AND LGPL-2.0-or-later AND BSD-3-Clause AND MIT

```

### `apk` package: `libcrypto1.1`

```console
libcrypto1.1-1.1.1l-r0 description:
Crypto library from openssl

libcrypto1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libcrypto1.1-1.1.1l-r0 installed size:
2704 KiB

libcrypto1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libcurl`

```console
libcurl-7.79.1-r0 description:
The multiprotocol file transfer library

libcurl-7.79.1-r0 webpage:
https://curl.se/

libcurl-7.79.1-r0 installed size:
500 KiB

libcurl-7.79.1-r0 license:
MIT

```

### `apk` package: `libffi`

```console
libffi-3.3-r2 description:
A portable, high level programming interface to various calling conventions.

libffi-3.3-r2 webpage:
https://sourceware.org/libffi

libffi-3.3-r2 installed size:
52 KiB

libffi-3.3-r2 license:
MIT

```

### `apk` package: `libgcc`

```console
libgcc-10.2.1_pre1-r3 description:
GNU C compiler runtime libraries

libgcc-10.2.1_pre1-r3 webpage:
https://gcc.gnu.org

libgcc-10.2.1_pre1-r3 installed size:
112 KiB

libgcc-10.2.1_pre1-r3 license:
GPL-2.0-or-later LGPL-2.1-or-later

```

### `apk` package: `libgomp`

```console
libgomp-10.2.1_pre1-r3 description:
GCC shared-memory parallel programming API library

libgomp-10.2.1_pre1-r3 webpage:
https://gcc.gnu.org

libgomp-10.2.1_pre1-r3 installed size:
264 KiB

libgomp-10.2.1_pre1-r3 license:
GPL-2.0-or-later LGPL-2.1-or-later

```

### `apk` package: `libgphobos`

```console
libgphobos-10.2.1_pre1-r3 description:
D programming language standard library for GCC

libgphobos-10.2.1_pre1-r3 webpage:
https://gcc.gnu.org

libgphobos-10.2.1_pre1-r3 installed size:
7104 KiB

libgphobos-10.2.1_pre1-r3 license:
GPL-2.0-or-later LGPL-2.1-or-later

```

### `apk` package: `libmagic`

```console
libmagic-5.39-r0 description:
File type identification library

libmagic-5.39-r0 webpage:
https://www.darwinsys.com/file

libmagic-5.39-r0 installed size:
6660 KiB

libmagic-5.39-r0 license:
BSD-2-Clause

```

### `apk` package: `libsasl`

```console
libsasl-2.1.27-r10 description:
Cyrus Simple Authentication and Security Layer (SASL) library

libsasl-2.1.27-r10 webpage:
https://www.cyrusimap.org/sasl/

libsasl-2.1.27-r10 installed size:
192 KiB

libsasl-2.1.27-r10 license:
custom

```

### `apk` package: `libssl1.1`

```console
libssl1.1-1.1.1l-r0 description:
SSL shared libraries

libssl1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libssl1.1-1.1.1l-r0 installed size:
528 KiB

libssl1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libstdc++`

```console
libstdc++-10.2.1_pre1-r3 description:
GNU C++ standard runtime library

libstdc++-10.2.1_pre1-r3 webpage:
https://gcc.gnu.org

libstdc++-10.2.1_pre1-r3 installed size:
1668 KiB

libstdc++-10.2.1_pre1-r3 license:
GPL-2.0-or-later LGPL-2.1-or-later

```

### `apk` package: `libtls-standalone`

```console
libtls-standalone-2.9.1-r1 description:
libtls extricated from libressl sources

libtls-standalone-2.9.1-r1 webpage:
https://www.libressl.org/

libtls-standalone-2.9.1-r1 installed size:
108 KiB

libtls-standalone-2.9.1-r1 license:
ISC

```

### `apk` package: `libuv`

```console
libuv-1.40.0-r0 description:
Cross-platform asychronous I/O

libuv-1.40.0-r0 webpage:
https://libuv.org

libuv-1.40.0-r0 installed size:
196 KiB

libuv-1.40.0-r0 license:
MIT AND ISC

```

### `apk` package: `lz4-libs`

```console
lz4-libs-1.9.2-r0 description:
LZ4 is lossless compression algorithm with fast decoder @ multiple GB/s per core. (libraries)

lz4-libs-1.9.2-r0 webpage:
https://github.com/lz4/lz4

lz4-libs-1.9.2-r0 installed size:
216 KiB

lz4-libs-1.9.2-r0 license:
BSD-2-Clause GPL-2.0-only

```

### `apk` package: `make`

```console
make-4.3-r0 description:
GNU make utility to maintain groups of programs

make-4.3-r0 webpage:
https://www.gnu.org/software/make

make-4.3-r0 installed size:
232 KiB

make-4.3-r0 license:
GPL-3.0-or-later

```

### `apk` package: `mpc1`

```console
mpc1-1.2.0-r0 description:
Multiprecision C library

mpc1-1.2.0-r0 webpage:
http://www.multiprecision.org/

mpc1-1.2.0-r0 installed size:
108 KiB

mpc1-1.2.0-r0 license:
LGPL-3.0-or-later

```

### `apk` package: `mpfr4`

```console
mpfr4-4.1.0-r0 description:
multiple-precision floating-point library

mpfr4-4.1.0-r0 webpage:
https://www.mpfr.org/

mpfr4-4.1.0-r0 installed size:
2676 KiB

mpfr4-4.1.0-r0 license:
LGPL-3.0-or-later

```

### `apk` package: `musl`

```console
musl-1.2.2-r1 description:
the musl c library (libc) implementation

musl-1.2.2-r1 webpage:
https://musl.libc.org/

musl-1.2.2-r1 installed size:
608 KiB

musl-1.2.2-r1 license:
MIT

```

### `apk` package: `musl-dev`

```console
musl-dev-1.2.2-r1 description:
the musl c library (libc) implementation (development files)

musl-dev-1.2.2-r1 webpage:
https://musl.libc.org/

musl-dev-1.2.2-r1 installed size:
10 MiB

musl-dev-1.2.2-r1 license:
MIT

```

### `apk` package: `musl-utils`

```console
musl-utils-1.2.2-r1 description:
the musl c library (libc) implementation

musl-utils-1.2.2-r1 webpage:
https://musl.libc.org/

musl-utils-1.2.2-r1 installed size:
140 KiB

musl-utils-1.2.2-r1 license:
MIT BSD GPL2+

```

### `apk` package: `ncurses-libs`

```console
ncurses-libs-6.2_p20210109-r0 description:
Ncurses libraries

ncurses-libs-6.2_p20210109-r0 webpage:
https://invisible-island.net/ncurses/

ncurses-libs-6.2_p20210109-r0 installed size:
496 KiB

ncurses-libs-6.2_p20210109-r0 license:
MIT

```

### `apk` package: `ncurses-terminfo-base`

```console
ncurses-terminfo-base-6.2_p20210109-r0 description:
Descriptions of common terminals

ncurses-terminfo-base-6.2_p20210109-r0 webpage:
https://invisible-island.net/ncurses/

ncurses-terminfo-base-6.2_p20210109-r0 installed size:
216 KiB

ncurses-terminfo-base-6.2_p20210109-r0 license:
MIT

```

### `apk` package: `nghttp2-libs`

```console
nghttp2-libs-1.42.0-r1 description:
Experimental HTTP/2 client, server and proxy (libraries)

nghttp2-libs-1.42.0-r1 webpage:
https://nghttp2.org

nghttp2-libs-1.42.0-r1 installed size:
168 KiB

nghttp2-libs-1.42.0-r1 license:
MIT

```

### `apk` package: `openssl`

```console
openssl-1.1.1l-r0 description:
Toolkit for Transport Layer Security (TLS)

openssl-1.1.1l-r0 webpage:
https://www.openssl.org/

openssl-1.1.1l-r0 installed size:
660 KiB

openssl-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `openssl-dev`

```console
openssl-dev-1.1.1l-r0 description:
Toolkit for Transport Layer Security (TLS) (development files)

openssl-dev-1.1.1l-r0 webpage:
https://www.openssl.org/

openssl-dev-1.1.1l-r0 installed size:
1596 KiB

openssl-dev-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `patch`

```console
patch-2.7.6-r7 description:
Utility to apply diffs to files

patch-2.7.6-r7 webpage:
https://www.gnu.org/software/patch/patch.html

patch-2.7.6-r7 installed size:
172 KiB

patch-2.7.6-r7 license:
GPL-3.0-or-later

```

### `apk` package: `perl`

```console
perl-5.32.0-r0 description:
Larry Wall's Practical Extraction and Report Language

perl-5.32.0-r0 webpage:
https://www.perl.org/

perl-5.32.0-r0 installed size:
37 MiB

perl-5.32.0-r0 license:
Artistic-Perl OR GPL-1.0-or-later

```

### `apk` package: `pkgconf`

```console
pkgconf-1.7.3-r0 description:
development framework configuration tools

pkgconf-1.7.3-r0 webpage:
https://git.sr.ht/~kaniini/pkgconf

pkgconf-1.7.3-r0 installed size:
140 KiB

pkgconf-1.7.3-r0 license:
ISC

```

### `apk` package: `python3`

```console
python3-3.8.10-r0 description:
A high-level scripting language

python3-3.8.10-r0 webpage:
https://www.python.org/

python3-3.8.10-r0 installed size:
44 MiB

python3-3.8.10-r0 license:
PSF-2.0

```

### `apk` package: `readline`

```console
readline-8.1.0-r0 description:
GNU readline library

readline-8.1.0-r0 webpage:
https://tiswww.cwru.edu/php/chet/readline/rltop.html

readline-8.1.0-r0 installed size:
308 KiB

readline-8.1.0-r0 license:
GPL-2.0-or-later

```

### `apk` package: `rhash-libs`

```console
rhash-libs-1.4.1-r0 description:
Utility for calculation and verification of hash sums and magnet links (libraries)

rhash-libs-1.4.1-r0 webpage:
http://rhash.anz.ru

rhash-libs-1.4.1-r0 installed size:
216 KiB

rhash-libs-1.4.1-r0 license:
0BSD

```

### `apk` package: `runtime-dependencies`

```console
runtime-dependencies-20211001.230430 description:
virtual meta package

runtime-dependencies-20211001.230430 webpage:


runtime-dependencies-20211001.230430 installed size:
0 B

runtime-dependencies-20211001.230430 license:


```

### `apk` package: `scanelf`

```console
scanelf-1.2.8-r0 description:
Scan ELF binaries for stuff

scanelf-1.2.8-r0 webpage:
https://wiki.gentoo.org/wiki/Hardened/PaX_Utilities

scanelf-1.2.8-r0 installed size:
92 KiB

scanelf-1.2.8-r0 license:
GPL-2.0-only

```

### `apk` package: `sqlite-libs`

```console
sqlite-libs-3.34.1-r0 description:
Sqlite3 library

sqlite-libs-3.34.1-r0 webpage:
https://www.sqlite.org/

sqlite-libs-3.34.1-r0 installed size:
948 KiB

sqlite-libs-3.34.1-r0 license:
Public-Domain

```

### `apk` package: `ssl_client`

```console
ssl_client-1.32.1-r6 description:
EXternal ssl_client for busybox wget

ssl_client-1.32.1-r6 webpage:
https://busybox.net/

ssl_client-1.32.1-r6 installed size:
28 KiB

ssl_client-1.32.1-r6 license:
GPL-2.0-only

```

### `apk` package: `su-exec`

```console
su-exec-0.2-r1 description:
switch user and group id, setgroups and exec

su-exec-0.2-r1 webpage:
https://github.com/ncopa/su-exec

su-exec-0.2-r1 installed size:
24 KiB

su-exec-0.2-r1 license:
MIT

```

### `apk` package: `tini`

```console
tini-0.19.0-r0 description:
A tiny but valid init for containers

tini-0.19.0-r0 webpage:
https://github.com/krallin/tini

tini-0.19.0-r0 installed size:
36 KiB

tini-0.19.0-r0 license:
MIT

```

### `apk` package: `tzdata`

```console
tzdata-2021b-r0 description:
Timezone data

tzdata-2021b-r0 webpage:
https://www.iana.org/time-zones

tzdata-2021b-r0 installed size:
3376 KiB

tzdata-2021b-r0 license:
Public-Domain

```

### `apk` package: `xz-libs`

```console
xz-libs-5.2.5-r0 description:
Library and CLI tools for XZ and LZMA compressed files (libraries)

xz-libs-5.2.5-r0 webpage:
https://tukaani.org/xz

xz-libs-5.2.5-r0 installed size:
148 KiB

xz-libs-5.2.5-r0 license:
GPL-2.0-or-later AND Public-Domain AND LGPL-2.1-or-later

```

### `apk` package: `zlib`

```console
zlib-1.2.11-r3 description:
A compression/decompression Library

zlib-1.2.11-r3 webpage:
https://zlib.net/

zlib-1.2.11-r3 installed size:
108 KiB

zlib-1.2.11-r3 license:
Zlib

```

### `apk` package: `zstd-libs`

```console
zstd-libs-1.4.9-r0 description:
Zstandard - Fast real-time compression algorithm (libraries)

zstd-libs-1.4.9-r0 webpage:
https://www.zstd.net

zstd-libs-1.4.9-r0 installed size:
736 KiB

zstd-libs-1.4.9-r0 license:
BSD-3-Clause GPL-2.0-or-later

```
