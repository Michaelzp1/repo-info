## `registry:latest`

```console
$ docker pull registry@sha256:265d4a5ed8bf0df27d1107edb00b70e658ee9aa5acb3f37336c5a17db634481e
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 3
	-	linux; amd64
	-	linux; arm variant v6
	-	linux; arm64 variant v8

### `registry:latest` - linux; amd64

```console
$ docker pull registry@sha256:b0b8dd398630cbb819d9a9c2fbd50561370856874b5d5d935be2e0af07c0ff4c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **9.9 MB (9941481 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b2cb11db9d3d60af38d9d6841d3b8b053e5972c0b7e4e6351e9ea4374ed37d8c`
-	Entrypoint: `["\/entrypoint.sh"]`
-	Default Command: `["\/etc\/docker\/registry\/config.yml"]`

```dockerfile
# Tue, 31 Aug 2021 23:18:31 GMT
ADD file:9d14b11183983923090d9e6d15cc51ee210466296e913bfefbfd580b3de59c95 in / 
# Tue, 31 Aug 2021 23:18:31 GMT
CMD ["/bin/sh"]
# Wed, 01 Sep 2021 05:45:59 GMT
RUN set -ex     && apk add --no-cache ca-certificates
# Wed, 01 Sep 2021 05:46:00 GMT
COPY file:21256ff7df5369f7ad2e19c6d020a644303aded200bdbec4d46648f38d55df78 in /bin/registry 
# Wed, 01 Sep 2021 05:46:00 GMT
COPY file:4544cc1555469403b322faecc1cf1ca584667c43a6a60b17300f97840c04196e in /etc/docker/registry/config.yml 
# Wed, 01 Sep 2021 05:46:00 GMT
VOLUME [/var/lib/registry]
# Wed, 01 Sep 2021 05:46:00 GMT
EXPOSE 5000
# Wed, 01 Sep 2021 05:46:01 GMT
COPY file:507caa54f88c1f3862e5876e09a108b2083630ba24c57ad124e356a2de861d62 in /entrypoint.sh 
# Wed, 01 Sep 2021 05:46:01 GMT
ENTRYPOINT ["/entrypoint.sh"]
# Wed, 01 Sep 2021 05:46:01 GMT
CMD ["/etc/docker/registry/config.yml"]
```

-	Layers:
	-	`sha256:6a428f9f83b0a29f1fdd2ccccca19a9bab805a925b8eddf432a5a3d3da04afbc`  
		Last Modified: Tue, 31 Aug 2021 23:19:15 GMT  
		Size: 2.8 MB (2817307 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:90cad49de35d1824bc606b0b4034f18bcd9f0cea7b9e637388e0664762adc935`  
		Last Modified: Wed, 01 Sep 2021 05:46:13 GMT  
		Size: 299.6 KB (299648 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:b215d0b4084623113492622ecb9f29eafab51566513f5bc5314fbdf3cab50706`  
		Last Modified: Wed, 01 Sep 2021 05:46:14 GMT  
		Size: 6.8 MB (6823914 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:429305b6c15c7c8cdddc3991149d63e539f7a3d9d7581f4ea901454cac48bd22`  
		Last Modified: Wed, 01 Sep 2021 05:46:12 GMT  
		Size: 399.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:6f7e10a4e907eb65fa50e308b01a1ab9ad08a553ae856427a5fc09f778f75f7f`  
		Last Modified: Wed, 01 Sep 2021 05:46:13 GMT  
		Size: 213.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `registry:latest` - linux; arm variant v6

```console
$ docker pull registry@sha256:6de6b4d5063876c92220d0438ae6068c778d9a2d3845b3d5c57a04a307998df6
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **9.3 MB (9314668 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:49d1f6cfdf73a8409ea7c7180bf28babdb82731b61419d1a5f72d47a37c85a34`
-	Entrypoint: `["\/entrypoint.sh"]`
-	Default Command: `["\/etc\/docker\/registry\/config.yml"]`

```dockerfile
# Tue, 31 Aug 2021 22:30:57 GMT
ADD file:3e83d6b5df3a951968e475c7326baf5ef90a22f04163693db34f3b4fc5812434 in / 
# Tue, 31 Aug 2021 22:30:57 GMT
CMD ["/bin/sh"]
# Wed, 01 Sep 2021 07:05:37 GMT
RUN set -ex     && apk add --no-cache ca-certificates
# Wed, 01 Sep 2021 07:05:38 GMT
COPY file:29c6c1625420a558a03cc7ed253192f8138cba6212b64e30217fb6488af668e2 in /bin/registry 
# Wed, 01 Sep 2021 07:05:39 GMT
COPY file:4544cc1555469403b322faecc1cf1ca584667c43a6a60b17300f97840c04196e in /etc/docker/registry/config.yml 
# Wed, 01 Sep 2021 07:05:39 GMT
VOLUME [/var/lib/registry]
# Wed, 01 Sep 2021 07:05:39 GMT
EXPOSE 5000
# Wed, 01 Sep 2021 07:05:40 GMT
COPY file:507caa54f88c1f3862e5876e09a108b2083630ba24c57ad124e356a2de861d62 in /entrypoint.sh 
# Wed, 01 Sep 2021 07:05:40 GMT
ENTRYPOINT ["/entrypoint.sh"]
# Wed, 01 Sep 2021 07:05:41 GMT
CMD ["/etc/docker/registry/config.yml"]
```

-	Layers:
	-	`sha256:7fe987b00bcb1f14c5b65f89813475143c021e2f5c156705ac3525abe1b397a1`  
		Last Modified: Tue, 31 Aug 2021 22:32:38 GMT  
		Size: 2.6 MB (2623044 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:b4d47cc325425d225535b1d155c10d7a2d2eaa5b68e27ac3e5c124474a86a02b`  
		Last Modified: Wed, 01 Sep 2021 07:06:15 GMT  
		Size: 299.9 KB (299926 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:32397bf2318c20cd4dd9abbc247cbfdbb5d938e3c8ac386b796a84baa1ce22a3`  
		Last Modified: Wed, 01 Sep 2021 07:06:18 GMT  
		Size: 6.4 MB (6391085 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:d8bb944b080bc5e328200d19dc5140d40a85b59619be01e14ade2a783182780b`  
		Last Modified: Wed, 01 Sep 2021 07:06:15 GMT  
		Size: 400.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:e4b4afe7c5d3001b3bd85d309a9fe997d1c0b6b57b5d673bd761933dd8b10d74`  
		Last Modified: Wed, 01 Sep 2021 07:06:14 GMT  
		Size: 213.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `registry:latest` - linux; arm64 variant v8

```console
$ docker pull registry@sha256:c11a277a91045f91866550314a988f937366bc2743859aa0f6ec8ef57b0458ce
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **9.3 MB (9269294 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:979f2f24c32b2553fa72c6589287d88c57241a139992bf73e3feadd7cf607cf8`
-	Entrypoint: `["\/entrypoint.sh"]`
-	Default Command: `["\/etc\/docker\/registry\/config.yml"]`

```dockerfile
# Wed, 01 Sep 2021 02:50:59 GMT
ADD file:da6c0ac7cb9f819998546d88fb489b746004eb2ad6da64a39210696ef0e66e54 in / 
# Wed, 01 Sep 2021 02:50:59 GMT
CMD ["/bin/sh"]
# Wed, 01 Sep 2021 14:55:08 GMT
RUN set -ex     && apk add --no-cache ca-certificates
# Wed, 01 Sep 2021 14:55:08 GMT
COPY file:51a441e6eceff49ef32609e7070b64e8d5690648e4f915cc825274e6fe37aed2 in /bin/registry 
# Wed, 01 Sep 2021 14:55:08 GMT
COPY file:4544cc1555469403b322faecc1cf1ca584667c43a6a60b17300f97840c04196e in /etc/docker/registry/config.yml 
# Wed, 01 Sep 2021 14:55:08 GMT
VOLUME [/var/lib/registry]
# Wed, 01 Sep 2021 14:55:08 GMT
EXPOSE 5000
# Wed, 01 Sep 2021 14:55:09 GMT
COPY file:507caa54f88c1f3862e5876e09a108b2083630ba24c57ad124e356a2de861d62 in /entrypoint.sh 
# Wed, 01 Sep 2021 14:55:09 GMT
ENTRYPOINT ["/entrypoint.sh"]
# Wed, 01 Sep 2021 14:55:09 GMT
CMD ["/etc/docker/registry/config.yml"]
```

-	Layers:
	-	`sha256:07d756952c5cd45726cf9e8a292a3e05ca67eee5da176df7d632be8c5bb0ad04`  
		Last Modified: Wed, 01 Sep 2021 02:52:00 GMT  
		Size: 2.7 MB (2728407 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:21c823007724f3d0d321cde6a30ac42ecf83d9a2187f0d33e670a00ecfbdbd2e`  
		Last Modified: Wed, 01 Sep 2021 14:55:31 GMT  
		Size: 300.1 KB (300078 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:fee51d9f7f10cbec94d0f6f8a4c1d5561966ef8ca4ed8e88d1b3f8ca90c7fca5`  
		Last Modified: Wed, 01 Sep 2021 14:55:32 GMT  
		Size: 6.2 MB (6240198 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:978c88a00d22dbb2ffc9214fcacf21d80f4f95e9209c83d7952d180cff52ea46`  
		Last Modified: Wed, 01 Sep 2021 14:55:31 GMT  
		Size: 399.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:16e349687c3ed74e14540a722ce6642eba19836da529becd6603e830f6d8a71a`  
		Last Modified: Wed, 01 Sep 2021 14:55:30 GMT  
		Size: 212.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
