# `composer:2.1.11`

## Docker Metadata

- Image ID: `sha256:be92812690566863616c3065015ef57f4d710b354ffcb4d64d4460df4fe0a2cd`
- Created: `2021-11-02T20:21:58.525021765Z`
- Virtual Size: ~ 181.09 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/docker-entrypoint.sh"]`
- Command: `["composer"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `PHPIZE_DEPS=autoconf 		dpkg-dev dpkg 		file 		g++ 		gcc 		libc-dev 		make 		pkgconf 		re2c`
  - `PHP_INI_DIR=/usr/local/etc/php`
  - `PHP_CFLAGS=-fstack-protector-strong -fpic -fpie -O2 -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64`
  - `PHP_CPPFLAGS=-fstack-protector-strong -fpic -fpie -O2 -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64`
  - `PHP_LDFLAGS=-Wl,-O1 -pie`
  - `GPG_KEYS=1729F83938DA44E27BA0F4D3DBDB397470D12172 BFDDD28642824F8118EF77909B67A5C12229118F`
  - `PHP_VERSION=8.0.12`
  - `PHP_URL=https://www.php.net/distributions/php-8.0.12.tar.xz`
  - `PHP_ASC_URL=https://www.php.net/distributions/php-8.0.12.tar.xz.asc`
  - `PHP_SHA256=a501017b3b0fd3023223ea25d98e87369b782f8a82310c4033d7ea6a989fea0a`
  - `COMPOSER_ALLOW_SUPERUSER=1`
  - `COMPOSER_HOME=/tmp`
  - `COMPOSER_VERSION=2.1.11`

## `apk` (`.apk`-based packages)

### `apk` package: `alpine-baselayout`

```console
alpine-baselayout-3.2.0-r16 description:
Alpine base dir structure and init scripts

alpine-baselayout-3.2.0-r16 webpage:
https://git.alpinelinux.org/cgit/aports/tree/main/alpine-baselayout

alpine-baselayout-3.2.0-r16 installed size:
404 KiB

alpine-baselayout-3.2.0-r16 license:
GPL-2.0-only

```

### `apk` package: `alpine-keys`

```console
alpine-keys-2.4-r0 description:
Public keys for Alpine Linux packages

alpine-keys-2.4-r0 webpage:
https://alpinelinux.org

alpine-keys-2.4-r0 installed size:
156 KiB

alpine-keys-2.4-r0 license:
MIT

```

### `apk` package: `apk-tools`

```console
apk-tools-2.12.7-r0 description:
Alpine Package Keeper - package manager for alpine

apk-tools-2.12.7-r0 webpage:
https://gitlab.alpinelinux.org/alpine/apk-tools

apk-tools-2.12.7-r0 installed size:
304 KiB

apk-tools-2.12.7-r0 license:
GPL-2.0-only

```

### `apk` package: `apr`

```console
apr-1.7.0-r0 description:
The Apache Portable Runtime

apr-1.7.0-r0 webpage:
http://apr.apache.org/

apr-1.7.0-r0 installed size:
220 KiB

apr-1.7.0-r0 license:
Apache-2.0

```

### `apk` package: `apr-util`

```console
apr-util-1.6.1-r7 description:
The Apache Portable Runtime Utility Library

apr-util-1.6.1-r7 webpage:
http://apr.apache.org/

apr-util-1.6.1-r7 installed size:
200 KiB

apr-util-1.6.1-r7 license:
Apache-2.0

```

### `apk` package: `argon2-libs`

```console
argon2-libs-20190702-r1 description:
The password hash Argon2, winner of PHC (libraries)

argon2-libs-20190702-r1 webpage:
https://github.com/P-H-C/phc-winner-argon2

argon2-libs-20190702-r1 installed size:
52 KiB

argon2-libs-20190702-r1 license:
Apache-2.0 CC0-1.0

```

### `apk` package: `bash`

```console
bash-5.1.4-r0 description:
The GNU Bourne Again shell

bash-5.1.4-r0 webpage:
https://www.gnu.org/software/bash/bash.html

bash-5.1.4-r0 installed size:
1296 KiB

bash-5.1.4-r0 license:
GPL-3.0-or-later

```

### `apk` package: `brotli-libs`

```console
brotli-libs-1.0.9-r5 description:
Generic lossless compressor (libraries)

brotli-libs-1.0.9-r5 webpage:
https://github.com/google/brotli

brotli-libs-1.0.9-r5 installed size:
720 KiB

brotli-libs-1.0.9-r5 license:
MIT

```

### `apk` package: `busybox`

```console
busybox-1.33.1-r3 description:
Size optimized toolbox of many common UNIX utilities

busybox-1.33.1-r3 webpage:
https://busybox.net/

busybox-1.33.1-r3 installed size:
928 KiB

busybox-1.33.1-r3 license:
GPL-2.0-only

```

### `apk` package: `ca-certificates`

```console
ca-certificates-20191127-r5 description:
Common CA certificates PEM files from Mozilla

ca-certificates-20191127-r5 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-20191127-r5 installed size:
672 KiB

ca-certificates-20191127-r5 license:
MPL-2.0 AND MIT

```

### `apk` package: `ca-certificates-bundle`

```console
ca-certificates-bundle-20191127-r5 description:
Pre generated bundle of Mozilla certificates

ca-certificates-bundle-20191127-r5 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-bundle-20191127-r5 installed size:
228 KiB

ca-certificates-bundle-20191127-r5 license:
MPL-2.0 AND MIT

```

### `apk` package: `coreutils`

```console
coreutils-8.32-r2 description:
The basic file, shell and text manipulation utilities

coreutils-8.32-r2 webpage:
https://www.gnu.org/software/coreutils/

coreutils-8.32-r2 installed size:
1100 KiB

coreutils-8.32-r2 license:
GPL-3.0-or-later

```

### `apk` package: `curl`

```console
curl-7.79.1-r0 description:
URL retrival utility and library

curl-7.79.1-r0 webpage:
https://curl.se/

curl-7.79.1-r0 installed size:
248 KiB

curl-7.79.1-r0 license:
MIT

```

### `apk` package: `expat`

```console
expat-2.4.1-r0 description:
XML Parser library written in C

expat-2.4.1-r0 webpage:
http://www.libexpat.org/

expat-2.4.1-r0 installed size:
188 KiB

expat-2.4.1-r0 license:
MIT

```

### `apk` package: `gdbm`

```console
gdbm-1.19-r0 description:
GNU dbm is a set of database routines that use extensible hashing

gdbm-1.19-r0 webpage:
https://www.gnu.org/software/gdbm/

gdbm-1.19-r0 installed size:
224 KiB

gdbm-1.19-r0 license:
GPL-3.0-or-later

```

### `apk` package: `git`

```console
git-2.32.0-r0 description:
Distributed version control system

git-2.32.0-r0 webpage:
https://www.git-scm.com/

git-2.32.0-r0 installed size:
10 MiB

git-2.32.0-r0 license:
GPL-2.0-or-later

```

### `apk` package: `libacl`

```console
libacl-2.2.53-r0 description:
Dynamic library for access control list support

libacl-2.2.53-r0 webpage:
https://savannah.nongnu.org/projects/acl

libacl-2.2.53-r0 installed size:
44 KiB

libacl-2.2.53-r0 license:
LGPL-2.1-or-later AND GPL-2.0-or-later

```

### `apk` package: `libattr`

```console
libattr-2.5.1-r0 description:
utilities for managing filesystem extended attributes (libraries)

libattr-2.5.1-r0 webpage:
https://savannah.nongnu.org/projects/attr

libattr-2.5.1-r0 installed size:
32 KiB

libattr-2.5.1-r0 license:
LGPL-2.1-or-later

```

### `apk` package: `libbz2`

```console
libbz2-1.0.8-r1 description:
Shared library for bz2

libbz2-1.0.8-r1 webpage:
http://sources.redhat.com/bzip2

libbz2-1.0.8-r1 installed size:
72 KiB

libbz2-1.0.8-r1 license:
bzip2-1.0.6

```

### `apk` package: `libc-utils`

```console
libc-utils-0.7.2-r3 description:
Meta package to pull in correct libc

libc-utils-0.7.2-r3 webpage:
https://alpinelinux.org

libc-utils-0.7.2-r3 installed size:
4096 B

libc-utils-0.7.2-r3 license:
BSD-2-Clause AND BSD-3-Clause

```

### `apk` package: `libcrypto1.1`

```console
libcrypto1.1-1.1.1l-r0 description:
Crypto library from openssl

libcrypto1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libcrypto1.1-1.1.1l-r0 installed size:
2704 KiB

libcrypto1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libcurl`

```console
libcurl-7.79.1-r0 description:
The multiprotocol file transfer library

libcurl-7.79.1-r0 webpage:
https://curl.se/

libcurl-7.79.1-r0 installed size:
500 KiB

libcurl-7.79.1-r0 license:
MIT

```

### `apk` package: `libedit`

```console
libedit-20210216.3.1-r0 description:
BSD line editing library

libedit-20210216.3.1-r0 webpage:
https://www.thrysoee.dk/editline

libedit-20210216.3.1-r0 installed size:
196 KiB

libedit-20210216.3.1-r0 license:
BSD-3-Clause

```

### `apk` package: `libffi`

```console
libffi-3.3-r2 description:
A portable, high level programming interface to various calling conventions.

libffi-3.3-r2 webpage:
https://sourceware.org/libffi

libffi-3.3-r2 installed size:
52 KiB

libffi-3.3-r2 license:
MIT

```

### `apk` package: `libgcc`

```console
libgcc-10.3.1_git20210424-r2 description:
GNU C compiler runtime libraries

libgcc-10.3.1_git20210424-r2 webpage:
https://gcc.gnu.org

libgcc-10.3.1_git20210424-r2 installed size:
112 KiB

libgcc-10.3.1_git20210424-r2 license:
GPL-2.0-or-later LGPL-2.1-or-later

```

### `apk` package: `libretls`

```console
libretls-3.3.3p1-r2 description:
port of libtls from libressl to openssl

libretls-3.3.3p1-r2 webpage:
https://git.causal.agency/libretls/

libretls-3.3.3p1-r2 installed size:
84 KiB

libretls-3.3.3p1-r2 license:
ISC AND (BSD-3-Clause OR MIT)

```

### `apk` package: `libsasl`

```console
libsasl-2.1.27-r12 description:
Cyrus Simple Authentication and Security Layer (SASL) library

libsasl-2.1.27-r12 webpage:
https://www.cyrusimap.org/sasl/

libsasl-2.1.27-r12 installed size:
192 KiB

libsasl-2.1.27-r12 license:
custom

```

### `apk` package: `libsodium`

```console
libsodium-1.0.18-r0 description:
P(ortable|ackageable) NaCl-based crypto library

libsodium-1.0.18-r0 webpage:
https://github.com/jedisct1/libsodium

libsodium-1.0.18-r0 installed size:
340 KiB

libsodium-1.0.18-r0 license:
ISC

```

### `apk` package: `libssl1.1`

```console
libssl1.1-1.1.1l-r0 description:
SSL shared libraries

libssl1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libssl1.1-1.1.1l-r0 installed size:
528 KiB

libssl1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libstdc++`

```console
libstdc++-10.3.1_git20210424-r2 description:
GNU C++ standard runtime library

libstdc++-10.3.1_git20210424-r2 webpage:
https://gcc.gnu.org

libstdc++-10.3.1_git20210424-r2 installed size:
1664 KiB

libstdc++-10.3.1_git20210424-r2 license:
GPL-2.0-or-later LGPL-2.1-or-later

```

### `apk` package: `libuuid`

```console
libuuid-2.37-r0 description:
DCE compatible Universally Unique Identifier library

libuuid-2.37-r0 webpage:
https://git.kernel.org/cgit/utils/util-linux/util-linux.git

libuuid-2.37-r0 installed size:
40 KiB

libuuid-2.37-r0 license:
GPL-3.0-or-later AND GPL-2.0-or-later AND GPL-2.0-only AND

```

### `apk` package: `libxml2`

```console
libxml2-2.9.12-r1 description:
XML parsing library, version 2

libxml2-2.9.12-r1 webpage:
http://www.xmlsoft.org/

libxml2-2.9.12-r1 installed size:
1200 KiB

libxml2-2.9.12-r1 license:
MIT

```

### `apk` package: `libzip`

```console
libzip-1.7.3-r2 description:
C library for manipulating zip archives

libzip-1.7.3-r2 webpage:
http://www.nih.at/libzip/index.html

libzip-1.7.3-r2 installed size:
108 KiB

libzip-1.7.3-r2 license:
BSD-3-Clause

```

### `apk` package: `lz4-libs`

```console
lz4-libs-1.9.3-r0 description:
LZ4 is lossless compression algorithm with fast decoder @ multiple GB/s per core. (libraries)

lz4-libs-1.9.3-r0 webpage:
https://github.com/lz4/lz4

lz4-libs-1.9.3-r0 installed size:
216 KiB

lz4-libs-1.9.3-r0 license:
BSD-2-Clause GPL-2.0-only

```

### `apk` package: `make`

```console
make-4.3-r0 description:
GNU make utility to maintain groups of programs

make-4.3-r0 webpage:
https://www.gnu.org/software/make

make-4.3-r0 installed size:
232 KiB

make-4.3-r0 license:
GPL-3.0-or-later

```

### `apk` package: `mercurial`

```console
mercurial-5.7-r0 description:
Scalable distributed SCM tool

mercurial-5.7-r0 webpage:
https://www.mercurial-scm.org/

mercurial-5.7-r0 installed size:
21 MiB

mercurial-5.7-r0 license:
GPL-2.0-or-later

```

### `apk` package: `mpdecimal`

```console
mpdecimal-2.5.1-r1 description:
complete implementation of the General Decimal Arithmetic Specification

mpdecimal-2.5.1-r1 webpage:
https://www.bytereef.org/mpdecimal/index.html

mpdecimal-2.5.1-r1 installed size:
212 KiB

mpdecimal-2.5.1-r1 license:
BSD-2-Clause

```

### `apk` package: `musl`

```console
musl-1.2.2-r3 description:
the musl c library (libc) implementation

musl-1.2.2-r3 webpage:
https://musl.libc.org/

musl-1.2.2-r3 installed size:
608 KiB

musl-1.2.2-r3 license:
MIT

```

### `apk` package: `musl-utils`

```console
musl-utils-1.2.2-r3 description:
the musl c library (libc) implementation

musl-utils-1.2.2-r3 webpage:
https://musl.libc.org/

musl-utils-1.2.2-r3 installed size:
144 KiB

musl-utils-1.2.2-r3 license:
MIT BSD GPL2+

```

### `apk` package: `ncurses-libs`

```console
ncurses-libs-6.2_p20210612-r0 description:
Ncurses libraries

ncurses-libs-6.2_p20210612-r0 webpage:
https://invisible-island.net/ncurses/

ncurses-libs-6.2_p20210612-r0 installed size:
500 KiB

ncurses-libs-6.2_p20210612-r0 license:
MIT

```

### `apk` package: `ncurses-terminfo-base`

```console
ncurses-terminfo-base-6.2_p20210612-r0 description:
Descriptions of common terminals

ncurses-terminfo-base-6.2_p20210612-r0 webpage:
https://invisible-island.net/ncurses/

ncurses-terminfo-base-6.2_p20210612-r0 installed size:
216 KiB

ncurses-terminfo-base-6.2_p20210612-r0 license:
MIT

```

### `apk` package: `nghttp2-libs`

```console
nghttp2-libs-1.43.0-r0 description:
Experimental HTTP/2 client, server and proxy (libraries)

nghttp2-libs-1.43.0-r0 webpage:
https://nghttp2.org

nghttp2-libs-1.43.0-r0 installed size:
168 KiB

nghttp2-libs-1.43.0-r0 license:
MIT

```

### `apk` package: `oniguruma`

```console
oniguruma-6.9.7.1-r0 description:
a regular expressions library

oniguruma-6.9.7.1-r0 webpage:
https://github.com/kkos/oniguruma

oniguruma-6.9.7.1-r0 installed size:
560 KiB

oniguruma-6.9.7.1-r0 license:
BSD-2-Clause

```

### `apk` package: `openssh-client-common`

```console
openssh-client-common-8.6_p1-r3 description:
OpenBSD's SSH client common files

openssh-client-common-8.6_p1-r3 webpage:
https://www.openssh.com/portable.html

openssh-client-common-8.6_p1-r3 installed size:
2312 KiB

openssh-client-common-8.6_p1-r3 license:
BSD

```

### `apk` package: `openssh-client-default`

```console
openssh-client-default-8.6_p1-r3 description:
OpenBSD's SSH client

openssh-client-default-8.6_p1-r3 webpage:
https://www.openssh.com/portable.html

openssh-client-default-8.6_p1-r3 installed size:
760 KiB

openssh-client-default-8.6_p1-r3 license:
BSD

```

### `apk` package: `openssh-keygen`

```console
openssh-keygen-8.6_p1-r3 description:
ssh helper program for generating keys

openssh-keygen-8.6_p1-r3 webpage:
https://www.openssh.com/portable.html

openssh-keygen-8.6_p1-r3 installed size:
460 KiB

openssh-keygen-8.6_p1-r3 license:
BSD

```

### `apk` package: `openssl`

```console
openssl-1.1.1l-r0 description:
Toolkit for Transport Layer Security (TLS)

openssl-1.1.1l-r0 webpage:
https://www.openssl.org/

openssl-1.1.1l-r0 installed size:
660 KiB

openssl-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `p7zip`

```console
p7zip-16.02-r4 description:
command-line port of the 7zip compression utility

p7zip-16.02-r4 webpage:
http://p7zip.sourceforge.net

p7zip-16.02-r4 installed size:
4568 KiB

p7zip-16.02-r4 license:
LGPL-2.0-or-later

```

### `apk` package: `patch`

```console
patch-2.7.6-r7 description:
Utility to apply diffs to files

patch-2.7.6-r7 webpage:
https://www.gnu.org/software/patch/patch.html

patch-2.7.6-r7 installed size:
172 KiB

patch-2.7.6-r7 license:
GPL-3.0-or-later

```

### `apk` package: `pcre2`

```console
pcre2-10.36-r0 description:
Perl-compatible regular expression library

pcre2-10.36-r0 webpage:
https://pcre.org/

pcre2-10.36-r0 installed size:
652 KiB

pcre2-10.36-r0 license:
BSD-3-Clause

```

### `apk` package: `python3`

```console
python3-3.9.5-r1 description:
A high-level scripting language

python3-3.9.5-r1 webpage:
https://www.python.org/

python3-3.9.5-r1 installed size:
45 MiB

python3-3.9.5-r1 license:
PSF-2.0

```

### `apk` package: `readline`

```console
readline-8.1.0-r0 description:
GNU readline library

readline-8.1.0-r0 webpage:
https://tiswww.cwru.edu/php/chet/readline/rltop.html

readline-8.1.0-r0 installed size:
308 KiB

readline-8.1.0-r0 license:
GPL-2.0-or-later

```

### `apk` package: `s6-ipcserver`

```console
s6-ipcserver-2.10.0.3-r0 description:
skarnet.org's small & secure supervision software suite.

s6-ipcserver-2.10.0.3-r0 webpage:
https://skarnet.org/software/s6/

s6-ipcserver-2.10.0.3-r0 installed size:
76 KiB

s6-ipcserver-2.10.0.3-r0 license:
ISC

```

### `apk` package: `scanelf`

```console
scanelf-1.3.2-r0 description:
Scan ELF binaries for stuff

scanelf-1.3.2-r0 webpage:
https://wiki.gentoo.org/wiki/Hardened/PaX_Utilities

scanelf-1.3.2-r0 installed size:
92 KiB

scanelf-1.3.2-r0 license:
GPL-2.0-only

```

### `apk` package: `serf`

```console
serf-1.3.9-r5 description:
High-Performance Asynchronous HTTP Client Library

serf-1.3.9-r5 webpage:
http://serf.apache.org/

serf-1.3.9-r5 installed size:
120 KiB

serf-1.3.9-r5 license:
Apache-2.0

```

### `apk` package: `skalibs`

```console
skalibs-2.10.0.3-r0 description:
Set of general-purpose C programming libraries for skarnet.org software.

skalibs-2.10.0.3-r0 webpage:
https://skarnet.org/software/skalibs/

skalibs-2.10.0.3-r0 installed size:
204 KiB

skalibs-2.10.0.3-r0 license:
ISC

```

### `apk` package: `sqlite-libs`

```console
sqlite-libs-3.35.5-r0 description:
Sqlite3 library

sqlite-libs-3.35.5-r0 webpage:
https://www.sqlite.org/

sqlite-libs-3.35.5-r0 installed size:
964 KiB

sqlite-libs-3.35.5-r0 license:
Public-Domain

```

### `apk` package: `ssl_client`

```console
ssl_client-1.33.1-r3 description:
EXternal ssl_client for busybox wget

ssl_client-1.33.1-r3 webpage:
https://busybox.net/

ssl_client-1.33.1-r3 installed size:
28 KiB

ssl_client-1.33.1-r3 license:
GPL-2.0-only

```

### `apk` package: `subversion`

```console
subversion-1.14.1-r3 description:
Replacement for CVS, another versioning system (svn)

subversion-1.14.1-r3 webpage:
https://subversion.apache.org/

subversion-1.14.1-r3 installed size:
1040 KiB

subversion-1.14.1-r3 license:
Apache-2.0 BSD

```

### `apk` package: `subversion-libs`

```console
subversion-libs-1.14.1-r3 description:
Replacement for CVS, another versioning system (svn) (libraries)

subversion-libs-1.14.1-r3 webpage:
https://subversion.apache.org/

subversion-libs-1.14.1-r3 installed size:
3624 KiB

subversion-libs-1.14.1-r3 license:
Apache-2.0 BSD

```

### `apk` package: `tar`

```console
tar-1.34-r0 description:
Utility used to store, backup, and transport files

tar-1.34-r0 webpage:
https://www.gnu.org/software/tar/

tar-1.34-r0 installed size:
488 KiB

tar-1.34-r0 license:
GPL-3.0-or-later

```

### `apk` package: `tini`

```console
tini-0.19.0-r0 description:
A tiny but valid init for containers

tini-0.19.0-r0 webpage:
https://github.com/krallin/tini

tini-0.19.0-r0 installed size:
36 KiB

tini-0.19.0-r0 license:
MIT

```

### `apk` package: `unzip`

```console
unzip-6.0-r9 description:
Extract PKZIP-compatible .zip files

unzip-6.0-r9 webpage:
http://www.info-zip.org/UnZip.html

unzip-6.0-r9 installed size:
316 KiB

unzip-6.0-r9 license:
custom

```

### `apk` package: `utmps`

```console
utmps-0.1.0.2-r0 description:
A secure utmp/wtmp implementation

utmps-0.1.0.2-r0 webpage:
https://skarnet.org/software/utmps/

utmps-0.1.0.2-r0 installed size:
64 KiB

utmps-0.1.0.2-r0 license:
ISC

```

### `apk` package: `xz`

```console
xz-5.2.5-r0 description:
Library and CLI tools for XZ and LZMA compressed files

xz-5.2.5-r0 webpage:
https://tukaani.org/xz

xz-5.2.5-r0 installed size:
160 KiB

xz-5.2.5-r0 license:
GPL-2.0-or-later AND Public-Domain AND LGPL-2.1-or-later

```

### `apk` package: `xz-libs`

```console
xz-libs-5.2.5-r0 description:
Library and CLI tools for XZ and LZMA compressed files (libraries)

xz-libs-5.2.5-r0 webpage:
https://tukaani.org/xz

xz-libs-5.2.5-r0 installed size:
148 KiB

xz-libs-5.2.5-r0 license:
GPL-2.0-or-later AND Public-Domain AND LGPL-2.1-or-later

```

### `apk` package: `zip`

```console
zip-3.0-r9 description:
Creates PKZIP-compatible .zip files

zip-3.0-r9 webpage:
http://www.info-zip.org/pub/infozip/Zip.html

zip-3.0-r9 installed size:
412 KiB

zip-3.0-r9 license:
Info-ZIP

```

### `apk` package: `zlib`

```console
zlib-1.2.11-r3 description:
A compression/decompression Library

zlib-1.2.11-r3 webpage:
https://zlib.net/

zlib-1.2.11-r3 installed size:
108 KiB

zlib-1.2.11-r3 license:
Zlib

```
