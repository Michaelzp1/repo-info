# `docker:20.10.10-dind-rootless`

## Docker Metadata

- Image ID: `sha256:ebcfe9dea8276ca16e54836a93936cfc08f6cb0c92332a509a11af82cf6279dd`
- Created: `2021-10-25T22:19:48.788252887Z`
- Virtual Size: ~ 286.95 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["dockerd-entrypoint.sh"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `DOCKER_VERSION=20.10.10`
  - `DOCKER_TLS_CERTDIR=/certs`
  - `DIND_COMMIT=42b1175eda071c0e9121e1d64345928384a93df1`

## `apk` (`.apk`-based packages)

### `apk` package: `alpine-baselayout`

```console
alpine-baselayout-3.2.0-r16 description:
Alpine base dir structure and init scripts

alpine-baselayout-3.2.0-r16 webpage:
https://git.alpinelinux.org/cgit/aports/tree/main/alpine-baselayout

alpine-baselayout-3.2.0-r16 installed size:
404 KiB

alpine-baselayout-3.2.0-r16 license:
GPL-2.0-only

```

### `apk` package: `alpine-keys`

```console
alpine-keys-2.3-r1 description:
Public keys for Alpine Linux packages

alpine-keys-2.3-r1 webpage:
https://alpinelinux.org

alpine-keys-2.3-r1 installed size:
116 KiB

alpine-keys-2.3-r1 license:
MIT

```

### `apk` package: `apk-tools`

```console
apk-tools-2.12.7-r0 description:
Alpine Package Keeper - package manager for alpine

apk-tools-2.12.7-r0 webpage:
https://gitlab.alpinelinux.org/alpine/apk-tools

apk-tools-2.12.7-r0 installed size:
304 KiB

apk-tools-2.12.7-r0 license:
GPL-2.0-only

```

### `apk` package: `btrfs-progs`

```console
btrfs-progs-5.12.1-r0 description:
BTRFS filesystem utilities

btrfs-progs-5.12.1-r0 webpage:
https://btrfs.wiki.kernel.org

btrfs-progs-5.12.1-r0 installed size:
1292 KiB

btrfs-progs-5.12.1-r0 license:
GPL-2.0-or-later LGPL-3.0-or-later

```

### `apk` package: `busybox`

```console
busybox-1.33.1-r3 description:
Size optimized toolbox of many common UNIX utilities

busybox-1.33.1-r3 webpage:
https://busybox.net/

busybox-1.33.1-r3 installed size:
928 KiB

busybox-1.33.1-r3 license:
GPL-2.0-only

```

### `apk` package: `ca-certificates`

```console
ca-certificates-20191127-r5 description:
Common CA certificates PEM files from Mozilla

ca-certificates-20191127-r5 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-20191127-r5 installed size:
672 KiB

ca-certificates-20191127-r5 license:
MPL-2.0 AND MIT

```

### `apk` package: `ca-certificates-bundle`

```console
ca-certificates-bundle-20191127-r5 description:
Pre generated bundle of Mozilla certificates

ca-certificates-bundle-20191127-r5 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-bundle-20191127-r5 installed size:
228 KiB

ca-certificates-bundle-20191127-r5 license:
MPL-2.0 AND MIT

```

### `apk` package: `e2fsprogs`

```console
e2fsprogs-1.46.2-r0 description:
Standard Ext2/3/4 filesystem utilities

e2fsprogs-1.46.2-r0 webpage:
http://e2fsprogs.sourceforge.net

e2fsprogs-1.46.2-r0 installed size:
432 KiB

e2fsprogs-1.46.2-r0 license:
GPL-2.0-or-later AND LGPL-2.0-or-later AND BSD-3-Clause AND MIT

```

### `apk` package: `e2fsprogs-extra`

```console
e2fsprogs-extra-1.46.2-r0 description:
Ext2/3/4 filesystem extra utilities

e2fsprogs-extra-1.46.2-r0 webpage:
http://e2fsprogs.sourceforge.net

e2fsprogs-extra-1.46.2-r0 installed size:
688 KiB

e2fsprogs-extra-1.46.2-r0 license:
GPL-2.0-or-later AND LGPL-2.0-or-later AND BSD-3-Clause AND MIT

```

### `apk` package: `e2fsprogs-libs`

```console
e2fsprogs-libs-1.46.2-r0 description:
Standard Ext2/3/4 filesystem utilities (libraries)

e2fsprogs-libs-1.46.2-r0 webpage:
http://e2fsprogs.sourceforge.net

e2fsprogs-libs-1.46.2-r0 installed size:
468 KiB

e2fsprogs-libs-1.46.2-r0 license:
GPL-2.0-or-later AND LGPL-2.0-or-later AND BSD-3-Clause AND MIT

```

### `apk` package: `fts`

```console
fts-1.2.7-r1 description:
Provides the fts(3) functions, which are missing in musl libc

fts-1.2.7-r1 webpage:
https://github.com/pullmoll/musl-fts/

fts-1.2.7-r1 installed size:
32 KiB

fts-1.2.7-r1 license:
BSD-3-Clause

```

### `apk` package: `inih`

```console
inih-53-r1 description:
Simple .INI file parser for embedded systems

inih-53-r1 webpage:
https://github.com/benhoyt/inih

inih-53-r1 installed size:
28 KiB

inih-53-r1 license:
BSD-3-Clause

```

### `apk` package: `ip6tables`

```console
ip6tables-1.8.7-r1 description:
Linux kernel firewall, NAT and packet mangling tools

ip6tables-1.8.7-r1 webpage:
https://www.netfilter.org/projects/iptables/index.html

ip6tables-1.8.7-r1 installed size:
372 KiB

ip6tables-1.8.7-r1 license:
GPL-2.0-or-later

```

### `apk` package: `iproute2`

```console
iproute2-5.12.0-r0 description:
IP Routing Utilities

iproute2-5.12.0-r0 webpage:
https://www.linuxfoundation.org/collaborate/workgroups/networking/iproute2

iproute2-5.12.0-r0 installed size:
520 KiB

iproute2-5.12.0-r0 license:
GPL-2.0-or-later

```

### `apk` package: `iproute2-minimal`

```console
iproute2-minimal-5.12.0-r0 description:
IP Routing Utilities (/sbin/ip only)

iproute2-minimal-5.12.0-r0 webpage:
https://www.linuxfoundation.org/collaborate/workgroups/networking/iproute2

iproute2-minimal-5.12.0-r0 installed size:
592 KiB

iproute2-minimal-5.12.0-r0 license:
GPL-2.0-or-later

```

### `apk` package: `iproute2-ss`

```console
iproute2-ss-5.12.0-r0 description:
IP Routing Utilities (socket statistics tool)

iproute2-ss-5.12.0-r0 webpage:
https://www.linuxfoundation.org/collaborate/workgroups/networking/iproute2

iproute2-ss-5.12.0-r0 installed size:
168 KiB

iproute2-ss-5.12.0-r0 license:
GPL-2.0-or-later

```

### `apk` package: `iproute2-tc`

```console
iproute2-tc-5.12.0-r0 description:
IP Routing Utilities (traffic control and XDP support)

iproute2-tc-5.12.0-r0 webpage:
https://www.linuxfoundation.org/collaborate/workgroups/networking/iproute2

iproute2-tc-5.12.0-r0 installed size:
628 KiB

iproute2-tc-5.12.0-r0 license:
GPL-2.0-or-later

```

### `apk` package: `iptables`

```console
iptables-1.8.7-r1 description:
Linux kernel firewall, NAT and packet mangling tools

iptables-1.8.7-r1 webpage:
https://www.netfilter.org/projects/iptables/index.html

iptables-1.8.7-r1 installed size:
2172 KiB

iptables-1.8.7-r1 license:
GPL-2.0-or-later

```

### `apk` package: `keyutils-libs`

```console
keyutils-libs-1.6.3-r0 description:
Key utilities library

keyutils-libs-1.6.3-r0 webpage:
http://people.redhat.com/~dhowells/keyutils/

keyutils-libs-1.6.3-r0 installed size:
36 KiB

keyutils-libs-1.6.3-r0 license:
GPL-2.0-or-later LGPL-2.0-or-later

```

### `apk` package: `krb5-conf`

```console
krb5-conf-1.0-r2 description:
Shared krb5.conf for both MIT krb5 and heimdal

krb5-conf-1.0-r2 webpage:
https://web.mit.edu/kerberos/www/

krb5-conf-1.0-r2 installed size:
12 KiB

krb5-conf-1.0-r2 license:
MIT

```

### `apk` package: `krb5-libs`

```console
krb5-libs-1.18.4-r0 description:
The shared libraries used by Kerberos 5

krb5-libs-1.18.4-r0 webpage:
https://web.mit.edu/kerberos/www/

krb5-libs-1.18.4-r0 installed size:
1960 KiB

krb5-libs-1.18.4-r0 license:
MIT

```

### `apk` package: `libblkid`

```console
libblkid-2.37-r0 description:
Block device identification library from util-linux

libblkid-2.37-r0 webpage:
https://git.kernel.org/cgit/utils/util-linux/util-linux.git

libblkid-2.37-r0 installed size:
300 KiB

libblkid-2.37-r0 license:
GPL-3.0-or-later AND GPL-2.0-or-later AND GPL-2.0-only AND

```

### `apk` package: `libbz2`

```console
libbz2-1.0.8-r1 description:
Shared library for bz2

libbz2-1.0.8-r1 webpage:
http://sources.redhat.com/bzip2

libbz2-1.0.8-r1 installed size:
72 KiB

libbz2-1.0.8-r1 license:
bzip2-1.0.6

```

### `apk` package: `libc-utils`

```console
libc-utils-0.7.2-r3 description:
Meta package to pull in correct libc

libc-utils-0.7.2-r3 webpage:
https://alpinelinux.org

libc-utils-0.7.2-r3 installed size:
4096 B

libc-utils-0.7.2-r3 license:
BSD-2-Clause AND BSD-3-Clause

```

### `apk` package: `libc6-compat`

```console
libc6-compat-1.2.2-r3 description:
compatibility libraries for glibc

libc6-compat-1.2.2-r3 webpage:
https://musl.libc.org/

libc6-compat-1.2.2-r3 installed size:
12 KiB

libc6-compat-1.2.2-r3 license:
MIT

```

### `apk` package: `libcom_err`

```console
libcom_err-1.46.2-r0 description:
Common error description library

libcom_err-1.46.2-r0 webpage:
http://e2fsprogs.sourceforge.net

libcom_err-1.46.2-r0 installed size:
24 KiB

libcom_err-1.46.2-r0 license:
GPL-2.0-or-later AND LGPL-2.0-or-later AND BSD-3-Clause AND MIT

```

### `apk` package: `libcrypto1.1`

```console
libcrypto1.1-1.1.1l-r0 description:
Crypto library from openssl

libcrypto1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libcrypto1.1-1.1.1l-r0 installed size:
2704 KiB

libcrypto1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libedit`

```console
libedit-20210216.3.1-r0 description:
BSD line editing library

libedit-20210216.3.1-r0 webpage:
https://www.thrysoee.dk/editline

libedit-20210216.3.1-r0 installed size:
196 KiB

libedit-20210216.3.1-r0 license:
BSD-3-Clause

```

### `apk` package: `libelf`

```console
libelf-0.182-r1 description:
Utilities and DSOs to handle ELF files and DWARF data - runtime libraries

libelf-0.182-r1 webpage:
http://elfutils.org/

libelf-0.182-r1 installed size:
684 KiB

libelf-0.182-r1 license:
GPL-3.0-or-later AND (GPL-2.0-or-later OR LGPL-3.0-or-later)

```

### `apk` package: `libintl`

```console
libintl-0.21-r0 description:
GNU gettext runtime library

libintl-0.21-r0 webpage:
https://www.gnu.org/software/gettext/gettext.html

libintl-0.21-r0 installed size:
56 KiB

libintl-0.21-r0 license:
LGPL-2.1-or-later

```

### `apk` package: `libmnl`

```console
libmnl-1.0.4-r1 description:
Library for minimalistic netlink

libmnl-1.0.4-r1 webpage:
http://www.netfilter.org/projects/libmnl/

libmnl-1.0.4-r1 installed size:
40 KiB

libmnl-1.0.4-r1 license:
GPL

```

### `apk` package: `libnftnl-libs`

```console
libnftnl-libs-1.2.0-r0 description:
Netfilter library providing interface to the nf_tables subsystem (libraries)

libnftnl-libs-1.2.0-r0 webpage:
https://netfilter.org/projects/libnftnl

libnftnl-libs-1.2.0-r0 installed size:
200 KiB

libnftnl-libs-1.2.0-r0 license:
GPL-2.0-or-later

```

### `apk` package: `libretls`

```console
libretls-3.3.3p1-r2 description:
port of libtls from libressl to openssl

libretls-3.3.3p1-r2 webpage:
https://git.causal.agency/libretls/

libretls-3.3.3p1-r2 installed size:
84 KiB

libretls-3.3.3p1-r2 license:
ISC AND (BSD-3-Clause OR MIT)

```

### `apk` package: `libssl1.1`

```console
libssl1.1-1.1.1l-r0 description:
SSL shared libraries

libssl1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libssl1.1-1.1.1l-r0 installed size:
528 KiB

libssl1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libtirpc`

```console
libtirpc-1.3.2-r0 description:
Transport Independent RPC library (SunRPC replacement)

libtirpc-1.3.2-r0 webpage:
https://sourceforge.net/projects/libtirpc

libtirpc-1.3.2-r0 installed size:
180 KiB

libtirpc-1.3.2-r0 license:
BSD-3-Clause

```

### `apk` package: `libtirpc-conf`

```console
libtirpc-conf-1.3.2-r0 description:
Configuration files for TI-RPC

libtirpc-conf-1.3.2-r0 webpage:
https://sourceforge.net/projects/libtirpc

libtirpc-conf-1.3.2-r0 installed size:
16 KiB

libtirpc-conf-1.3.2-r0 license:
BSD-3-Clause

```

### `apk` package: `libuuid`

```console
libuuid-2.37-r0 description:
DCE compatible Universally Unique Identifier library

libuuid-2.37-r0 webpage:
https://git.kernel.org/cgit/utils/util-linux/util-linux.git

libuuid-2.37-r0 installed size:
40 KiB

libuuid-2.37-r0 license:
GPL-3.0-or-later AND GPL-2.0-or-later AND GPL-2.0-only AND

```

### `apk` package: `libverto`

```console
libverto-0.3.2-r0 description:
Main loop abstraction library

libverto-0.3.2-r0 webpage:
https://github.com/npmccallum/libverto

libverto-0.3.2-r0 installed size:
36 KiB

libverto-0.3.2-r0 license:
MIT

```

### `apk` package: `lzo`

```console
lzo-2.10-r2 description:
LZO -- a real-time data compression library

lzo-2.10-r2 webpage:
http://www.oberhumer.com/opensource/lzo

lzo-2.10-r2 installed size:
128 KiB

lzo-2.10-r2 license:
GPL

```

### `apk` package: `musl`

```console
musl-1.2.2-r3 description:
the musl c library (libc) implementation

musl-1.2.2-r3 webpage:
https://musl.libc.org/

musl-1.2.2-r3 installed size:
608 KiB

musl-1.2.2-r3 license:
MIT

```

### `apk` package: `musl-utils`

```console
musl-utils-1.2.2-r3 description:
the musl c library (libc) implementation

musl-utils-1.2.2-r3 webpage:
https://musl.libc.org/

musl-utils-1.2.2-r3 installed size:
144 KiB

musl-utils-1.2.2-r3 license:
MIT BSD GPL2+

```

### `apk` package: `ncurses-libs`

```console
ncurses-libs-6.2_p20210612-r0 description:
Ncurses libraries

ncurses-libs-6.2_p20210612-r0 webpage:
https://invisible-island.net/ncurses/

ncurses-libs-6.2_p20210612-r0 installed size:
500 KiB

ncurses-libs-6.2_p20210612-r0 license:
MIT

```

### `apk` package: `ncurses-terminfo-base`

```console
ncurses-terminfo-base-6.2_p20210612-r0 description:
Descriptions of common terminals

ncurses-terminfo-base-6.2_p20210612-r0 webpage:
https://invisible-island.net/ncurses/

ncurses-terminfo-base-6.2_p20210612-r0 installed size:
216 KiB

ncurses-terminfo-base-6.2_p20210612-r0 license:
MIT

```

### `apk` package: `openssh-client-common`

```console
openssh-client-common-8.6_p1-r2 description:
OpenBSD's SSH client common files

openssh-client-common-8.6_p1-r2 webpage:
https://www.openssh.com/portable.html

openssh-client-common-8.6_p1-r2 installed size:
2312 KiB

openssh-client-common-8.6_p1-r2 license:
BSD

```

### `apk` package: `openssh-client-default`

```console
openssh-client-default-8.6_p1-r2 description:
OpenBSD's SSH client

openssh-client-default-8.6_p1-r2 webpage:
https://www.openssh.com/portable.html

openssh-client-default-8.6_p1-r2 installed size:
760 KiB

openssh-client-default-8.6_p1-r2 license:
BSD

```

### `apk` package: `openssh-keygen`

```console
openssh-keygen-8.6_p1-r2 description:
ssh helper program for generating keys

openssh-keygen-8.6_p1-r2 webpage:
https://www.openssh.com/portable.html

openssh-keygen-8.6_p1-r2 installed size:
460 KiB

openssh-keygen-8.6_p1-r2 license:
BSD

```

### `apk` package: `openssl`

```console
openssl-1.1.1l-r0 description:
Toolkit for Transport Layer Security (TLS)

openssl-1.1.1l-r0 webpage:
https://www.openssl.org/

openssl-1.1.1l-r0 installed size:
660 KiB

openssl-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `pigz`

```console
pigz-2.6-r0 description:
Parallel implementation of gzip

pigz-2.6-r0 webpage:
https://zlib.net/pigz/

pigz-2.6-r0 installed size:
268 KiB

pigz-2.6-r0 license:
Zlib

```

### `apk` package: `scanelf`

```console
scanelf-1.3.2-r0 description:
Scan ELF binaries for stuff

scanelf-1.3.2-r0 webpage:
https://wiki.gentoo.org/wiki/Hardened/PaX_Utilities

scanelf-1.3.2-r0 installed size:
92 KiB

scanelf-1.3.2-r0 license:
GPL-2.0-only

```

### `apk` package: `shadow-uidmap`

```console
shadow-uidmap-4.8.1-r0 description:
Utilities for using subordinate UIDs and GIDs

shadow-uidmap-4.8.1-r0 webpage:
http://pkg-shadow.alioth.debian.org/

shadow-uidmap-4.8.1-r0 installed size:
96 KiB

shadow-uidmap-4.8.1-r0 license:
BSD-3-Clause

```

### `apk` package: `ssl_client`

```console
ssl_client-1.33.1-r3 description:
EXternal ssl_client for busybox wget

ssl_client-1.33.1-r3 webpage:
https://busybox.net/

ssl_client-1.33.1-r3 installed size:
28 KiB

ssl_client-1.33.1-r3 license:
GPL-2.0-only

```

### `apk` package: `xfsprogs`

```console
xfsprogs-5.12.0-r0 description:
XFS filesystem utilities

xfsprogs-5.12.0-r0 webpage:
https://xfs.org/index.php/Main_Page

xfsprogs-5.12.0-r0 installed size:
1156 KiB

xfsprogs-5.12.0-r0 license:
LGPL-2.1-or-later

```

### `apk` package: `xz`

```console
xz-5.2.5-r0 description:
Library and CLI tools for XZ and LZMA compressed files

xz-5.2.5-r0 webpage:
https://tukaani.org/xz

xz-5.2.5-r0 installed size:
160 KiB

xz-5.2.5-r0 license:
GPL-2.0-or-later AND Public-Domain AND LGPL-2.1-or-later

```

### `apk` package: `xz-libs`

```console
xz-libs-5.2.5-r0 description:
Library and CLI tools for XZ and LZMA compressed files (libraries)

xz-libs-5.2.5-r0 webpage:
https://tukaani.org/xz

xz-libs-5.2.5-r0 installed size:
148 KiB

xz-libs-5.2.5-r0 license:
GPL-2.0-or-later AND Public-Domain AND LGPL-2.1-or-later

```

### `apk` package: `zfs`

```console
zfs-2.0.3-r1 description:
ZFS for Linux

zfs-2.0.3-r1 webpage:
https://zfsonlinux.org

zfs-2.0.3-r1 installed size:
1316 KiB

zfs-2.0.3-r1 license:
CDDL-1.0

```

### `apk` package: `zfs-libs`

```console
zfs-libs-2.0.3-r1 description:
ZFS for Linux (libraries)

zfs-libs-2.0.3-r1 webpage:
https://zfsonlinux.org

zfs-libs-2.0.3-r1 installed size:
4004 KiB

zfs-libs-2.0.3-r1 license:
CDDL-1.0

```

### `apk` package: `zlib`

```console
zlib-1.2.11-r3 description:
A compression/decompression Library

zlib-1.2.11-r3 webpage:
https://zlib.net/

zlib-1.2.11-r3 installed size:
108 KiB

zlib-1.2.11-r3 license:
Zlib

```

### `apk` package: `zstd-libs`

```console
zstd-libs-1.4.9-r1 description:
Zstandard - Fast real-time compression algorithm (libraries)

zstd-libs-1.4.9-r1 webpage:
https://www.zstd.net

zstd-libs-1.4.9-r1 installed size:
900 KiB

zstd-libs-1.4.9-r1 license:
BSD-3-Clause GPL-2.0-or-later

```
