# `docker:20.10.10-git`

## Docker Metadata

- Image ID: `sha256:f36edeffa553a75eeae015925e97cfa57bc864d9a952792ce45adcc9f9195b0d`
- Created: `2021-10-25T22:19:52.656792651Z`
- Virtual Size: ~ 229.08 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["docker-entrypoint.sh"]`
- Command: `["sh"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `DOCKER_VERSION=20.10.10`
  - `DOCKER_TLS_CERTDIR=/certs`

## `apk` (`.apk`-based packages)

### `apk` package: `alpine-baselayout`

```console
alpine-baselayout-3.2.0-r16 description:
Alpine base dir structure and init scripts

alpine-baselayout-3.2.0-r16 webpage:
https://git.alpinelinux.org/cgit/aports/tree/main/alpine-baselayout

alpine-baselayout-3.2.0-r16 installed size:
404 KiB

alpine-baselayout-3.2.0-r16 license:
GPL-2.0-only

```

### `apk` package: `alpine-keys`

```console
alpine-keys-2.3-r1 description:
Public keys for Alpine Linux packages

alpine-keys-2.3-r1 webpage:
https://alpinelinux.org

alpine-keys-2.3-r1 installed size:
116 KiB

alpine-keys-2.3-r1 license:
MIT

```

### `apk` package: `apk-tools`

```console
apk-tools-2.12.7-r0 description:
Alpine Package Keeper - package manager for alpine

apk-tools-2.12.7-r0 webpage:
https://gitlab.alpinelinux.org/alpine/apk-tools

apk-tools-2.12.7-r0 installed size:
304 KiB

apk-tools-2.12.7-r0 license:
GPL-2.0-only

```

### `apk` package: `brotli-libs`

```console
brotli-libs-1.0.9-r5 description:
Generic lossless compressor (libraries)

brotli-libs-1.0.9-r5 webpage:
https://github.com/google/brotli

brotli-libs-1.0.9-r5 installed size:
720 KiB

brotli-libs-1.0.9-r5 license:
MIT

```

### `apk` package: `busybox`

```console
busybox-1.33.1-r3 description:
Size optimized toolbox of many common UNIX utilities

busybox-1.33.1-r3 webpage:
https://busybox.net/

busybox-1.33.1-r3 installed size:
928 KiB

busybox-1.33.1-r3 license:
GPL-2.0-only

```

### `apk` package: `ca-certificates`

```console
ca-certificates-20191127-r5 description:
Common CA certificates PEM files from Mozilla

ca-certificates-20191127-r5 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-20191127-r5 installed size:
672 KiB

ca-certificates-20191127-r5 license:
MPL-2.0 AND MIT

```

### `apk` package: `ca-certificates-bundle`

```console
ca-certificates-bundle-20191127-r5 description:
Pre generated bundle of Mozilla certificates

ca-certificates-bundle-20191127-r5 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-bundle-20191127-r5 installed size:
228 KiB

ca-certificates-bundle-20191127-r5 license:
MPL-2.0 AND MIT

```

### `apk` package: `expat`

```console
expat-2.4.1-r0 description:
XML Parser library written in C

expat-2.4.1-r0 webpage:
http://www.libexpat.org/

expat-2.4.1-r0 installed size:
188 KiB

expat-2.4.1-r0 license:
MIT

```

### `apk` package: `git`

```console
git-2.32.0-r0 description:
Distributed version control system

git-2.32.0-r0 webpage:
https://www.git-scm.com/

git-2.32.0-r0 installed size:
10 MiB

git-2.32.0-r0 license:
GPL-2.0-or-later

```

### `apk` package: `libc-utils`

```console
libc-utils-0.7.2-r3 description:
Meta package to pull in correct libc

libc-utils-0.7.2-r3 webpage:
https://alpinelinux.org

libc-utils-0.7.2-r3 installed size:
4096 B

libc-utils-0.7.2-r3 license:
BSD-2-Clause AND BSD-3-Clause

```

### `apk` package: `libc6-compat`

```console
libc6-compat-1.2.2-r3 description:
compatibility libraries for glibc

libc6-compat-1.2.2-r3 webpage:
https://musl.libc.org/

libc6-compat-1.2.2-r3 installed size:
12 KiB

libc6-compat-1.2.2-r3 license:
MIT

```

### `apk` package: `libcrypto1.1`

```console
libcrypto1.1-1.1.1l-r0 description:
Crypto library from openssl

libcrypto1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libcrypto1.1-1.1.1l-r0 installed size:
2704 KiB

libcrypto1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libcurl`

```console
libcurl-7.79.1-r0 description:
The multiprotocol file transfer library

libcurl-7.79.1-r0 webpage:
https://curl.se/

libcurl-7.79.1-r0 installed size:
500 KiB

libcurl-7.79.1-r0 license:
MIT

```

### `apk` package: `libedit`

```console
libedit-20210216.3.1-r0 description:
BSD line editing library

libedit-20210216.3.1-r0 webpage:
https://www.thrysoee.dk/editline

libedit-20210216.3.1-r0 installed size:
196 KiB

libedit-20210216.3.1-r0 license:
BSD-3-Clause

```

### `apk` package: `libretls`

```console
libretls-3.3.3p1-r2 description:
port of libtls from libressl to openssl

libretls-3.3.3p1-r2 webpage:
https://git.causal.agency/libretls/

libretls-3.3.3p1-r2 installed size:
84 KiB

libretls-3.3.3p1-r2 license:
ISC AND (BSD-3-Clause OR MIT)

```

### `apk` package: `libssl1.1`

```console
libssl1.1-1.1.1l-r0 description:
SSL shared libraries

libssl1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libssl1.1-1.1.1l-r0 installed size:
528 KiB

libssl1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `musl`

```console
musl-1.2.2-r3 description:
the musl c library (libc) implementation

musl-1.2.2-r3 webpage:
https://musl.libc.org/

musl-1.2.2-r3 installed size:
608 KiB

musl-1.2.2-r3 license:
MIT

```

### `apk` package: `musl-utils`

```console
musl-utils-1.2.2-r3 description:
the musl c library (libc) implementation

musl-utils-1.2.2-r3 webpage:
https://musl.libc.org/

musl-utils-1.2.2-r3 installed size:
144 KiB

musl-utils-1.2.2-r3 license:
MIT BSD GPL2+

```

### `apk` package: `ncurses-libs`

```console
ncurses-libs-6.2_p20210612-r0 description:
Ncurses libraries

ncurses-libs-6.2_p20210612-r0 webpage:
https://invisible-island.net/ncurses/

ncurses-libs-6.2_p20210612-r0 installed size:
500 KiB

ncurses-libs-6.2_p20210612-r0 license:
MIT

```

### `apk` package: `ncurses-terminfo-base`

```console
ncurses-terminfo-base-6.2_p20210612-r0 description:
Descriptions of common terminals

ncurses-terminfo-base-6.2_p20210612-r0 webpage:
https://invisible-island.net/ncurses/

ncurses-terminfo-base-6.2_p20210612-r0 installed size:
216 KiB

ncurses-terminfo-base-6.2_p20210612-r0 license:
MIT

```

### `apk` package: `nghttp2-libs`

```console
nghttp2-libs-1.43.0-r0 description:
Experimental HTTP/2 client, server and proxy (libraries)

nghttp2-libs-1.43.0-r0 webpage:
https://nghttp2.org

nghttp2-libs-1.43.0-r0 installed size:
168 KiB

nghttp2-libs-1.43.0-r0 license:
MIT

```

### `apk` package: `openssh-client-common`

```console
openssh-client-common-8.6_p1-r2 description:
OpenBSD's SSH client common files

openssh-client-common-8.6_p1-r2 webpage:
https://www.openssh.com/portable.html

openssh-client-common-8.6_p1-r2 installed size:
2312 KiB

openssh-client-common-8.6_p1-r2 license:
BSD

```

### `apk` package: `openssh-client-default`

```console
openssh-client-default-8.6_p1-r2 description:
OpenBSD's SSH client

openssh-client-default-8.6_p1-r2 webpage:
https://www.openssh.com/portable.html

openssh-client-default-8.6_p1-r2 installed size:
760 KiB

openssh-client-default-8.6_p1-r2 license:
BSD

```

### `apk` package: `openssh-keygen`

```console
openssh-keygen-8.6_p1-r2 description:
ssh helper program for generating keys

openssh-keygen-8.6_p1-r2 webpage:
https://www.openssh.com/portable.html

openssh-keygen-8.6_p1-r2 installed size:
460 KiB

openssh-keygen-8.6_p1-r2 license:
BSD

```

### `apk` package: `pcre2`

```console
pcre2-10.36-r0 description:
Perl-compatible regular expression library

pcre2-10.36-r0 webpage:
https://pcre.org/

pcre2-10.36-r0 installed size:
652 KiB

pcre2-10.36-r0 license:
BSD-3-Clause

```

### `apk` package: `scanelf`

```console
scanelf-1.3.2-r0 description:
Scan ELF binaries for stuff

scanelf-1.3.2-r0 webpage:
https://wiki.gentoo.org/wiki/Hardened/PaX_Utilities

scanelf-1.3.2-r0 installed size:
92 KiB

scanelf-1.3.2-r0 license:
GPL-2.0-only

```

### `apk` package: `ssl_client`

```console
ssl_client-1.33.1-r3 description:
EXternal ssl_client for busybox wget

ssl_client-1.33.1-r3 webpage:
https://busybox.net/

ssl_client-1.33.1-r3 installed size:
28 KiB

ssl_client-1.33.1-r3 license:
GPL-2.0-only

```

### `apk` package: `zlib`

```console
zlib-1.2.11-r3 description:
A compression/decompression Library

zlib-1.2.11-r3 webpage:
https://zlib.net/

zlib-1.2.11-r3 installed size:
108 KiB

zlib-1.2.11-r3 license:
Zlib

```
