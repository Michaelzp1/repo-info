## `traefik:maroilles-windowsservercore-1809`

```console
$ docker pull traefik@sha256:1c03b2731c2b5f09e4eac696dd825536511773948573299c833bba7422fe22f9
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	windows version 10.0.17763.2237; amd64

### `traefik:maroilles-windowsservercore-1809` - windows version 10.0.17763.2237; amd64

```console
$ docker pull traefik@sha256:9915d0c046c4582e014e87b30039125ccdefe3b3a47c4bea64351de481ae709c
```

-	Docker Version: 20.10.8
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.7 GB (2709163106 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4ff9e00e621f8151ee41e3ebc49cb89707437ddf3ef565e036c30210dfdb16bb`
-	Entrypoint: `["\/traefik"]`
-	`SHELL`: `["powershell","-Command","$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]`

```dockerfile
# Thu, 07 May 2020 05:09:25 GMT
RUN Apply image 1809-RTM-amd64
# Thu, 07 Oct 2021 08:25:51 GMT
RUN Install update 1809-amd64
# Wed, 13 Oct 2021 12:02:56 GMT
SHELL [powershell -Command $ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';]
# Thu, 14 Oct 2021 02:48:38 GMT
RUN Invoke-WebRequest     -Uri "https://github.com/traefik/traefik/releases/download/v1.7.33/traefik_windows-amd64.exe"     -OutFile "/traefik.exe"
# Thu, 14 Oct 2021 02:48:39 GMT
EXPOSE 80
# Thu, 14 Oct 2021 02:48:40 GMT
ENTRYPOINT ["/traefik"]
# Thu, 14 Oct 2021 02:48:41 GMT
LABEL org.opencontainers.image.vendor=Traefik Labs org.opencontainers.image.url=https://traefik.io org.opencontainers.image.title=Traefik org.opencontainers.image.description=A modern reverse-proxy org.opencontainers.image.version=v1.7.33 org.opencontainers.image.documentation=https://docs.traefik.io
```

-	Layers:
	-	`sha256:4612f6d0b889cad0ed0292fae3a0b0c8a9e49aff6dea8eb049b2386d9b07986f`  
		Size: 1.7 GB (1718332879 bytes)  
		MIME: application/vnd.docker.image.rootfs.foreign.diff.tar.gzip
	-	`sha256:c0698cf91ebd6bcfb319be6a50421b356d6a3dbbd213d9b2b9dca0f837d7a999`  
		Size: 968.0 MB (967985917 bytes)  
		MIME: application/vnd.docker.image.rootfs.foreign.diff.tar.gzip
	-	`sha256:cc0c4e719f418d49c6a0fb87abd2e0e480c5b6fec1bacc3077cacfad9b4ab3e0`  
		Last Modified: Wed, 13 Oct 2021 12:18:09 GMT  
		Size: 1.4 KB (1399 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:8940656a944acb2501adec03e3c36d660e5050edd6f6ff3a9d62b05d990d7f9b`  
		Last Modified: Thu, 14 Oct 2021 02:49:46 GMT  
		Size: 22.8 MB (22838634 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:9125ed12b11bea1b24eb93d9bb458c938eb3ab801aeabaec734e5ee4ecf7cb92`  
		Last Modified: Thu, 14 Oct 2021 02:49:39 GMT  
		Size: 1.4 KB (1423 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:43e0c0d41de43b6446dd3388773bb8d262ec341ce63381488382457c566f026c`  
		Last Modified: Thu, 14 Oct 2021 02:49:39 GMT  
		Size: 1.4 KB (1441 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:47708fb7fab8eaab26cd32cd0f60c92420d58adf28682203a0c4bd34be7fffc5`  
		Last Modified: Thu, 14 Oct 2021 02:49:39 GMT  
		Size: 1.4 KB (1413 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
