# `traefik:v1.7.33`

## Docker Metadata

- Image ID: `sha256:85d1b251ae417bc825b2b8d1473d5f4e6451f6cfbadf53fb892e65975a6de1aa`
- Created: `2021-10-07T20:05:00.156270038Z`
- Virtual Size: ~ 82.92 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/traefik"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
- Labels:
  - `org.opencontainers.image.description=A modern reverse-proxy`
  - `org.opencontainers.image.documentation=https://docs.traefik.io`
  - `org.opencontainers.image.title=Traefik`
  - `org.opencontainers.image.url=https://traefik.io`
  - `org.opencontainers.image.vendor=Traefik Labs`
  - `org.opencontainers.image.version=v1.7.33`
