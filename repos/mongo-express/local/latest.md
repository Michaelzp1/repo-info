# `mongo-express:1.0.0-alpha.4`

## Docker Metadata

- Image ID: `sha256:2d2fb2cabc8fa6b894bd7c82158dd7a9dd5815d240f75a93db0d853f008cfd20`
- Created: `2021-10-22T21:11:30.919438897Z`
- Virtual Size: ~ 135.66 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["tini","--","/docker-entrypoint.sh"]`
- Command: `["mongo-express"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `NODE_VERSION=12.22.7`
  - `YARN_VERSION=1.22.15`
  - `ME_CONFIG_EDITORTHEME=default`
  - `ME_CONFIG_MONGODB_URL=mongodb://mongo:27017`
  - `ME_CONFIG_MONGODB_ENABLE_ADMIN=true`
  - `ME_CONFIG_BASICAUTH_USERNAME=`
  - `ME_CONFIG_BASICAUTH_PASSWORD=`
  - `VCAP_APP_HOST=0.0.0.0`
  - `MONGO_EXPRESS=1.0.0-alpha.4`

## `apk` (`.apk`-based packages)

### `apk` package: `alpine-baselayout`

```console
alpine-baselayout-3.2.0-r3 description:
Alpine base dir structure and init scripts

alpine-baselayout-3.2.0-r3 webpage:
https://git.alpinelinux.org/cgit/aports/tree/main/alpine-baselayout

alpine-baselayout-3.2.0-r3 installed size:
404 KiB

alpine-baselayout-3.2.0-r3 license:
GPL-2.0-only

```

### `apk` package: `alpine-keys`

```console
alpine-keys-2.1-r2 description:
Public keys for Alpine Linux packages

alpine-keys-2.1-r2 webpage:
https://alpinelinux.org

alpine-keys-2.1-r2 installed size:
96 KiB

alpine-keys-2.1-r2 license:
MIT

```

### `apk` package: `apk-tools`

```console
apk-tools-2.10.8-r0 description:
Alpine Package Keeper - package manager for alpine

apk-tools-2.10.8-r0 webpage:
https://gitlab.alpinelinux.org/alpine/apk-tools

apk-tools-2.10.8-r0 installed size:
260 KiB

apk-tools-2.10.8-r0 license:
GPL2

```

### `apk` package: `bash`

```console
bash-5.0.11-r1 description:
The GNU Bourne Again shell

bash-5.0.11-r1 webpage:
https://www.gnu.org/software/bash/bash.html

bash-5.0.11-r1 installed size:
1172 KiB

bash-5.0.11-r1 license:
GPL-3.0-or-later

```

### `apk` package: `busybox`

```console
busybox-1.31.1-r10 description:
Size optimized toolbox of many common UNIX utilities

busybox-1.31.1-r10 webpage:
https://busybox.net/

busybox-1.31.1-r10 installed size:
940 KiB

busybox-1.31.1-r10 license:
GPL-2.0-only

```

### `apk` package: `ca-certificates-cacert`

```console
ca-certificates-cacert-20191127-r2 description:
Mozilla bundled certificates

ca-certificates-cacert-20191127-r2 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-cacert-20191127-r2 installed size:
240 KiB

ca-certificates-cacert-20191127-r2 license:
MPL-2.0 GPL-2.0-or-later

```

### `apk` package: `libc-utils`

```console
libc-utils-0.7.2-r0 description:
Meta package to pull in correct libc

libc-utils-0.7.2-r0 webpage:
http://alpinelinux.org

libc-utils-0.7.2-r0 installed size:
4096 B

libc-utils-0.7.2-r0 license:
BSD

```

### `apk` package: `libcrypto1.1`

```console
libcrypto1.1-1.1.1l-r0 description:
Crypto library from openssl

libcrypto1.1-1.1.1l-r0 webpage:
https://www.openssl.org

libcrypto1.1-1.1.1l-r0 installed size:
2700 KiB

libcrypto1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libgcc`

```console
libgcc-9.3.0-r0 description:
GNU C compiler runtime libraries

libgcc-9.3.0-r0 webpage:
http://gcc.gnu.org

libgcc-9.3.0-r0 installed size:
88 KiB

libgcc-9.3.0-r0 license:
GPL LGPL

```

### `apk` package: `libssl1.1`

```console
libssl1.1-1.1.1l-r0 description:
SSL shared libraries

libssl1.1-1.1.1l-r0 webpage:
https://www.openssl.org

libssl1.1-1.1.1l-r0 installed size:
528 KiB

libssl1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libstdc++`

```console
libstdc++-9.3.0-r0 description:
GNU C++ standard runtime library

libstdc++-9.3.0-r0 webpage:
http://gcc.gnu.org

libstdc++-9.3.0-r0 installed size:
1632 KiB

libstdc++-9.3.0-r0 license:
GPL LGPL

```

### `apk` package: `libtls-standalone`

```console
libtls-standalone-2.9.1-r0 description:
libtls extricated from libressl sources

libtls-standalone-2.9.1-r0 webpage:
https://www.libressl.org/

libtls-standalone-2.9.1-r0 installed size:
108 KiB

libtls-standalone-2.9.1-r0 license:
ISC

```

### `apk` package: `musl`

```console
musl-1.1.24-r3 description:
the musl c library (libc) implementation

musl-1.1.24-r3 webpage:
https://musl.libc.org/

musl-1.1.24-r3 installed size:
600 KiB

musl-1.1.24-r3 license:
MIT

```

### `apk` package: `musl-utils`

```console
musl-utils-1.1.24-r3 description:
the musl c library (libc) implementation

musl-utils-1.1.24-r3 webpage:
https://musl.libc.org/

musl-utils-1.1.24-r3 installed size:
148 KiB

musl-utils-1.1.24-r3 license:
MIT BSD GPL2+

```

### `apk` package: `ncurses-libs`

```console
ncurses-libs-6.1_p20200118-r4 description:
Ncurses libraries

ncurses-libs-6.1_p20200118-r4 webpage:
https://invisible-island.net/ncurses/

ncurses-libs-6.1_p20200118-r4 installed size:
496 KiB

ncurses-libs-6.1_p20200118-r4 license:
MIT

```

### `apk` package: `ncurses-terminfo-base`

```console
ncurses-terminfo-base-6.1_p20200118-r4 description:
Descriptions of common terminals

ncurses-terminfo-base-6.1_p20200118-r4 webpage:
https://invisible-island.net/ncurses/

ncurses-terminfo-base-6.1_p20200118-r4 installed size:
212 KiB

ncurses-terminfo-base-6.1_p20200118-r4 license:
MIT

```

### `apk` package: `readline`

```console
readline-8.0.1-r0 description:
GNU readline library

readline-8.0.1-r0 webpage:
https://tiswww.cwru.edu/php/chet/readline/rltop.html

readline-8.0.1-r0 installed size:
292 KiB

readline-8.0.1-r0 license:
GPL-2.0-or-later

```

### `apk` package: `scanelf`

```console
scanelf-1.2.4-r0 description:
Scan ELF binaries for stuff

scanelf-1.2.4-r0 webpage:
https://wiki.gentoo.org/wiki/Hardened/PaX_Utilities

scanelf-1.2.4-r0 installed size:
92 KiB

scanelf-1.2.4-r0 license:
GPL-2.0-only

```

### `apk` package: `ssl_client`

```console
ssl_client-1.31.1-r10 description:
EXternal ssl_client for busybox wget

ssl_client-1.31.1-r10 webpage:
https://busybox.net/

ssl_client-1.31.1-r10 installed size:
28 KiB

ssl_client-1.31.1-r10 license:
GPL-2.0-only

```

### `apk` package: `tini`

```console
tini-0.18.0-r0 description:
A tiny but valid init for containers

tini-0.18.0-r0 webpage:
https://github.com/krallin/tini

tini-0.18.0-r0 installed size:
36 KiB

tini-0.18.0-r0 license:
MIT

```

### `apk` package: `zlib`

```console
zlib-1.2.11-r3 description:
A compression/decompression Library

zlib-1.2.11-r3 webpage:
https://zlib.net/

zlib-1.2.11-r3 installed size:
108 KiB

zlib-1.2.11-r3 license:
Zlib

```
