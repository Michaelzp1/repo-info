# `maven:3.8.3-openjdk-17`

## Docker Metadata

- Image ID: `sha256:c16deef870ba375659c737865cc0259ee668ba8571fafefd4f24e6302508264d`
- Created: `2021-10-28T02:51:10.275470254Z`
- Virtual Size: ~ 772.09 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/usr/local/bin/mvn-entrypoint.sh"]`
- Command: `["mvn"]`
- Environment:
  - `PATH=/usr/java/openjdk-17/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `JAVA_HOME=/usr/java/openjdk-17`
  - `LANG=C.UTF-8`
  - `JAVA_VERSION=17.0.1`
  - `MAVEN_HOME=/usr/share/maven`
  - `MAVEN_CONFIG=/root/.m2`
