# `maven:3.8.3-ibmjava-8-alpine`

## Docker Metadata

- Image ID: `sha256:e2b4967df8a2a4e625a460d843933e0ab3a917aecffccbf19fea76000fc2bf02`
- Created: `2021-10-06T21:15:42.795118891Z`
- Virtual Size: ~ 271.88 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/usr/local/bin/mvn-entrypoint.sh"]`
- Command: `["mvn"]`
- Environment:
  - `PATH=/opt/ibm/java/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `JAVA_VERSION=8.0.6.36`
  - `JAVA_HOME=/opt/ibm/java/jre`
  - `IBM_JAVA_OPTIONS=-XX:+UseContainerSupport`
  - `MAVEN_HOME=/usr/share/maven`
  - `MAVEN_CONFIG=/root/.m2`

## `apk` (`.apk`-based packages)

### `apk` package: `alpine-baselayout`

```console
alpine-baselayout-3.2.0-r7 description:
Alpine base dir structure and init scripts

alpine-baselayout-3.2.0-r7 webpage:
https://git.alpinelinux.org/cgit/aports/tree/main/alpine-baselayout

alpine-baselayout-3.2.0-r7 installed size:
400 KiB

alpine-baselayout-3.2.0-r7 license:
GPL-2.0-only

```

### `apk` package: `alpine-keys`

```console
alpine-keys-2.2-r0 description:
Public keys for Alpine Linux packages

alpine-keys-2.2-r0 webpage:
https://alpinelinux.org

alpine-keys-2.2-r0 installed size:
104 KiB

alpine-keys-2.2-r0 license:
MIT

```

### `apk` package: `apk-tools`

```console
apk-tools-2.10.8-r0 description:
Alpine Package Keeper - package manager for alpine

apk-tools-2.10.8-r0 webpage:
https://gitlab.alpinelinux.org/alpine/apk-tools

apk-tools-2.10.8-r0 installed size:
260 KiB

apk-tools-2.10.8-r0 license:
GPL-2.0-only

```

### `apk` package: `bash`

```console
bash-5.0.17-r0 description:
The GNU Bourne Again shell

bash-5.0.17-r0 webpage:
https://www.gnu.org/software/bash/bash.html

bash-5.0.17-r0 installed size:
1172 KiB

bash-5.0.17-r0 license:
GPL-3.0-or-later

```

### `apk` package: `busybox`

```console
busybox-1.31.1-r20 description:
Size optimized toolbox of many common UNIX utilities

busybox-1.31.1-r20 webpage:
https://busybox.net/

busybox-1.31.1-r20 installed size:
940 KiB

busybox-1.31.1-r20 license:
GPL-2.0-only

```

### `apk` package: `ca-certificates`

```console
ca-certificates-20191127-r4 description:
Common CA certificates PEM files from Mozilla

ca-certificates-20191127-r4 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-20191127-r4 installed size:
672 KiB

ca-certificates-20191127-r4 license:
MPL-2.0 GPL-2.0-or-later

```

### `apk` package: `ca-certificates-bundle`

```console
ca-certificates-bundle-20191127-r4 description:
Pre generated bundle of Mozilla certificates

ca-certificates-bundle-20191127-r4 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-bundle-20191127-r4 installed size:
228 KiB

ca-certificates-bundle-20191127-r4 license:
MPL-2.0 GPL-2.0-or-later

```

### `apk` package: `curl`

```console
curl-7.79.0-r0 description:
URL retrival utility and library

curl-7.79.0-r0 webpage:
https://curl.se/

curl-7.79.0-r0 installed size:
248 KiB

curl-7.79.0-r0 license:
MIT

```

### `apk` package: `glibc`

```console
glibc-2.30-r0 description:
GNU C Library compatibility layer

glibc-2.30-r0 webpage:
https://github.com/sgerrand/alpine-pkg-glibc

glibc-2.30-r0 installed size:
9028 KiB

glibc-2.30-r0 license:
LGPL

```

### `apk` package: `libacl`

```console
libacl-2.2.53-r0 description:
Dynamic library for access control list support

libacl-2.2.53-r0 webpage:
https://savannah.nongnu.org/projects/acl

libacl-2.2.53-r0 installed size:
44 KiB

libacl-2.2.53-r0 license:
LGPL-2.1-or-later AND GPL-2.0-or-later

```

### `apk` package: `libc-utils`

```console
libc-utils-0.7.2-r3 description:
Meta package to pull in correct libc

libc-utils-0.7.2-r3 webpage:
https://alpinelinux.org

libc-utils-0.7.2-r3 installed size:
4096 B

libc-utils-0.7.2-r3 license:
BSD-2-Clause AND BSD-3-Clause

```

### `apk` package: `libcrypto1.1`

```console
libcrypto1.1-1.1.1l-r0 description:
Crypto library from openssl

libcrypto1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libcrypto1.1-1.1.1l-r0 installed size:
2700 KiB

libcrypto1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libcurl`

```console
libcurl-7.79.0-r0 description:
The multiprotocol file transfer library

libcurl-7.79.0-r0 webpage:
https://curl.se/

libcurl-7.79.0-r0 installed size:
500 KiB

libcurl-7.79.0-r0 license:
MIT

```

### `apk` package: `libintl`

```console
libintl-0.20.2-r0 description:
GNU gettext runtime library

libintl-0.20.2-r0 webpage:
https://www.gnu.org/software/gettext/gettext.html

libintl-0.20.2-r0 installed size:
56 KiB

libintl-0.20.2-r0 license:
LGPL-2.1-or-later

```

### `apk` package: `libproc`

```console
libproc-3.3.16-r0 description:
Library for monitoring system and processes

libproc-3.3.16-r0 webpage:
https://gitlab.com/procps-ng/procps

libproc-3.3.16-r0 installed size:
84 KiB

libproc-3.3.16-r0 license:
GPL-2.0 LGPL-2.1+

```

### `apk` package: `libssl1.1`

```console
libssl1.1-1.1.1l-r0 description:
SSL shared libraries

libssl1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libssl1.1-1.1.1l-r0 installed size:
528 KiB

libssl1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libtls-standalone`

```console
libtls-standalone-2.9.1-r1 description:
libtls extricated from libressl sources

libtls-standalone-2.9.1-r1 webpage:
https://www.libressl.org/

libtls-standalone-2.9.1-r1 installed size:
108 KiB

libtls-standalone-2.9.1-r1 license:
ISC

```

### `apk` package: `musl`

```console
musl-1.1.24-r10 description:
the musl c library (libc) implementation

musl-1.1.24-r10 webpage:
https://musl.libc.org/

musl-1.1.24-r10 installed size:
600 KiB

musl-1.1.24-r10 license:
MIT

```

### `apk` package: `musl-utils`

```console
musl-utils-1.1.24-r10 description:
the musl c library (libc) implementation

musl-utils-1.1.24-r10 webpage:
https://musl.libc.org/

musl-utils-1.1.24-r10 installed size:
148 KiB

musl-utils-1.1.24-r10 license:
MIT BSD GPL2+

```

### `apk` package: `ncurses-libs`

```console
ncurses-libs-6.2_p20200523-r0 description:
Ncurses libraries

ncurses-libs-6.2_p20200523-r0 webpage:
https://invisible-island.net/ncurses/

ncurses-libs-6.2_p20200523-r0 installed size:
496 KiB

ncurses-libs-6.2_p20200523-r0 license:
MIT

```

### `apk` package: `ncurses-terminfo-base`

```console
ncurses-terminfo-base-6.2_p20200523-r0 description:
Descriptions of common terminals

ncurses-terminfo-base-6.2_p20200523-r0 webpage:
https://invisible-island.net/ncurses/

ncurses-terminfo-base-6.2_p20200523-r0 installed size:
212 KiB

ncurses-terminfo-base-6.2_p20200523-r0 license:
MIT

```

### `apk` package: `nghttp2-libs`

```console
nghttp2-libs-1.41.0-r0 description:
Experimental HTTP/2 client, server and proxy (libraries)

nghttp2-libs-1.41.0-r0 webpage:
https://nghttp2.org

nghttp2-libs-1.41.0-r0 installed size:
156 KiB

nghttp2-libs-1.41.0-r0 license:
MIT

```

### `apk` package: `openssl`

```console
openssl-1.1.1l-r0 description:
Toolkit for Transport Layer Security (TLS)

openssl-1.1.1l-r0 webpage:
https://www.openssl.org/

openssl-1.1.1l-r0 installed size:
660 KiB

openssl-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `procps`

```console
procps-3.3.16-r0 description:
Utilities for monitoring your system and processes on your system

procps-3.3.16-r0 webpage:
https://gitlab.com/procps-ng/procps

procps-3.3.16-r0 installed size:
544 KiB

procps-3.3.16-r0 license:
GPL-2.0 LGPL-2.1+

```

### `apk` package: `readline`

```console
readline-8.0.4-r0 description:
GNU readline library

readline-8.0.4-r0 webpage:
https://tiswww.cwru.edu/php/chet/readline/rltop.html

readline-8.0.4-r0 installed size:
292 KiB

readline-8.0.4-r0 license:
GPL-2.0-or-later

```

### `apk` package: `scanelf`

```console
scanelf-1.2.6-r0 description:
Scan ELF binaries for stuff

scanelf-1.2.6-r0 webpage:
https://wiki.gentoo.org/wiki/Hardened/PaX_Utilities

scanelf-1.2.6-r0 installed size:
92 KiB

scanelf-1.2.6-r0 license:
GPL-2.0-only

```

### `apk` package: `ssl_client`

```console
ssl_client-1.31.1-r20 description:
EXternal ssl_client for busybox wget

ssl_client-1.31.1-r20 webpage:
https://busybox.net/

ssl_client-1.31.1-r20 installed size:
28 KiB

ssl_client-1.31.1-r20 license:
GPL-2.0-only

```

### `apk` package: `tar`

```console
tar-1.32-r2 description:
Utility used to store, backup, and transport files

tar-1.32-r2 webpage:
https://www.gnu.org/software/tar/

tar-1.32-r2 installed size:
488 KiB

tar-1.32-r2 license:
GPL-3.0-or-later

```

### `apk` package: `zlib`

```console
zlib-1.2.11-r3 description:
A compression/decompression Library

zlib-1.2.11-r3 webpage:
https://zlib.net/

zlib-1.2.11-r3 installed size:
108 KiB

zlib-1.2.11-r3 license:
Zlib

```
