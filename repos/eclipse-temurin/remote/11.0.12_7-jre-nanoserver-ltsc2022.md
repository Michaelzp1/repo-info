## `eclipse-temurin:11.0.12_7-jre-nanoserver-ltsc2022`

```console
$ docker pull eclipse-temurin@sha256:73655405bf02063b0240906b1bb894f3a79e126536db31b99c29d6ed8cdbd0b4
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	windows version 10.0.20348.288; amd64

### `eclipse-temurin:11.0.12_7-jre-nanoserver-ltsc2022` - windows version 10.0.20348.288; amd64

```console
$ docker pull eclipse-temurin@sha256:6c7816d7cb91585f44ed89ed406c0e3563d722618bd3ef439a16dda66715bfb1
```

-	Docker Version: 20.10.8
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **159.8 MB (159793532 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e077bcc8897e8957a574391826e0d811bd53946d00c41038d7f145d46dc13b29`
-	Default Command: `["c:\\windows\\system32\\cmd.exe"]`
-	`SHELL`: `["cmd","\/s","\/c"]`

```dockerfile
# Thu, 07 Oct 2021 11:15:04 GMT
RUN Apply image ltsc2022-amd64
# Wed, 13 Oct 2021 19:02:30 GMT
SHELL [cmd /s /c]
# Wed, 13 Oct 2021 19:03:51 GMT
ENV JAVA_VERSION=jdk-11.0.12+7
# Wed, 13 Oct 2021 19:03:51 GMT
ENV JAVA_HOME=C:\openjdk-11
# Wed, 13 Oct 2021 19:03:52 GMT
USER ContainerAdministrator
# Wed, 13 Oct 2021 19:04:06 GMT
RUN echo Updating PATH: %JAVA_HOME%\bin;%PATH%     && setx /M PATH %JAVA_HOME%\bin;%PATH%     && echo Complete.
# Wed, 13 Oct 2021 19:04:07 GMT
USER ContainerUser
# Wed, 13 Oct 2021 19:05:12 GMT
COPY dir:37196d51db7ddeb5d9f84ac7a3abd0b4eaec3c6dcffe01db917f682d6ae15f0d in C:\openjdk-11 
# Wed, 13 Oct 2021 19:05:30 GMT
RUN echo Verifying install ...     && echo java --version && java --version     && echo Complete.
```

-	Layers:
	-	`sha256:91284e7e8fd4bd7ebcfa98544a3e4f59639f38281225c81c34b6fe22e0dba4e5`  
		Size: 116.9 MB (116939483 bytes)  
		MIME: application/vnd.docker.image.rootfs.foreign.diff.tar.gzip
	-	`sha256:1ba797e8f93823c3d71c52fcae413f3a33ca28ff2711c09ba5141432948f8298`  
		Last Modified: Wed, 13 Oct 2021 19:43:54 GMT  
		Size: 1.2 KB (1171 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:fb99973d4bc63fc57fe80728eb9284d6c30c79247c995760d90694a2e2c475ec`  
		Last Modified: Wed, 13 Oct 2021 19:45:10 GMT  
		Size: 1.1 KB (1133 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:95f4e3ec7a41b6157ede35d40a643ae557e91ef37b5271db4c11a05d21cff390`  
		Last Modified: Wed, 13 Oct 2021 19:45:10 GMT  
		Size: 1.2 KB (1160 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:d0a57907d6771ce405ca54b1de43cce670178bbe268fe4b6afb93e3c4b9872df`  
		Last Modified: Wed, 13 Oct 2021 19:45:10 GMT  
		Size: 1.2 KB (1154 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:43d25c4bee418e5626e46146febfe6b1e78bab8cdf6bbf29147c380a1aa963da`  
		Last Modified: Wed, 13 Oct 2021 19:45:08 GMT  
		Size: 85.4 KB (85362 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:379d093b1025356b9815670b65ceb49f3d0e72fe900d7964c86f9b83f7a4d478`  
		Last Modified: Wed, 13 Oct 2021 19:45:07 GMT  
		Size: 1.2 KB (1162 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:4b739de942f3d858d641bbba30d0481b420497f65459a764d56f9900bffbd38e`  
		Last Modified: Wed, 13 Oct 2021 19:45:47 GMT  
		Size: 42.7 MB (42702032 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:bf8f8a2f0b5b0a88c184e25a27c3744639362b8b3112b3cfc71cca78f1b1e6c3`  
		Last Modified: Wed, 13 Oct 2021 19:45:39 GMT  
		Size: 60.9 KB (60875 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
