## `eclipse-temurin:11-jre-nanoserver-1809`

```console
$ docker pull eclipse-temurin@sha256:ed4c2e48a8a77930689cc65cb7870fef23bd91857abc0e02b206c6b28e1f3138
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	windows version 10.0.17763.2237; amd64

### `eclipse-temurin:11-jre-nanoserver-1809` - windows version 10.0.17763.2237; amd64

```console
$ docker pull eclipse-temurin@sha256:4a87d729fbd762abbb765a1943728a31f74bb5c29014f7e900d4cd19134abee0
```

-	Docker Version: 20.10.8
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **145.5 MB (145522267 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9adeebfbb61ee0c3adf4e9d7450826bcb2442f5b47a4e768ae9d03b77c45b3e5`
-	Default Command: `["c:\\windows\\system32\\cmd.exe"]`
-	`SHELL`: `["cmd","\/s","\/c"]`

```dockerfile
# Thu, 07 Oct 2021 08:01:14 GMT
RUN Apply image 1809-amd64
# Wed, 13 Oct 2021 18:17:55 GMT
SHELL [cmd /s /c]
# Wed, 13 Oct 2021 18:33:29 GMT
ENV JAVA_VERSION=jdk-11.0.12+7
# Wed, 13 Oct 2021 18:33:30 GMT
ENV JAVA_HOME=C:\openjdk-11
# Wed, 13 Oct 2021 18:33:31 GMT
USER ContainerAdministrator
# Wed, 13 Oct 2021 18:33:44 GMT
RUN echo Updating PATH: %JAVA_HOME%\bin;%PATH%     && setx /M PATH %JAVA_HOME%\bin;%PATH%     && echo Complete.
# Wed, 13 Oct 2021 18:33:45 GMT
USER ContainerUser
# Wed, 13 Oct 2021 18:42:00 GMT
COPY dir:37196d51db7ddeb5d9f84ac7a3abd0b4eaec3c6dcffe01db917f682d6ae15f0d in C:\openjdk-11 
# Wed, 13 Oct 2021 18:42:15 GMT
RUN echo Verifying install ...     && echo java --version && java --version     && echo Complete.
```

-	Layers:
	-	`sha256:934e212983f208dc2bebc5de38259a6a62f1761868aacfee2cb3585a13b1e24b`  
		Size: 102.7 MB (102661372 bytes)  
		MIME: application/vnd.docker.image.rootfs.foreign.diff.tar.gzip
	-	`sha256:ceff24c3c0c2c360cd3788fcde5c41e1d6601339f4944623c732558a6030e631`  
		Last Modified: Wed, 13 Oct 2021 19:12:58 GMT  
		Size: 1.1 KB (1058 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:de63d1537f04b56e93b0b7487dbb45775e91fc6634697cf3fdda8a82ed83f651`  
		Last Modified: Wed, 13 Oct 2021 19:22:31 GMT  
		Size: 1.0 KB (1029 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:9359d885179b20c5f8871cc2c856c7b5d609b5c3b024b53c3e4bc7f237ae838e`  
		Last Modified: Wed, 13 Oct 2021 19:22:31 GMT  
		Size: 1.0 KB (1038 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:7397577754d9a766800b16d38d25df303d1d40880a656a87d8f1ec5de0075279`  
		Last Modified: Wed, 13 Oct 2021 19:22:30 GMT  
		Size: 1.1 KB (1051 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:814b69b71c6d3e48a4e089a84fa44459dd64a2734f0ec64623708ac87e844cf0`  
		Last Modified: Wed, 13 Oct 2021 19:22:28 GMT  
		Size: 71.2 KB (71249 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:155046e430dd658eb2e17c42b4dda04125a578e099f10d260da1811806b76549`  
		Last Modified: Wed, 13 Oct 2021 19:22:29 GMT  
		Size: 1.0 KB (1035 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:5be6f77825e3f5f5be5880a27240bf2657a1a796b3b27e00e55b729bf5e617ad`  
		Last Modified: Wed, 13 Oct 2021 19:35:35 GMT  
		Size: 42.7 MB (42702088 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:305a9a69b96e749db8d8a7b917f49be74b32dee86336ad9cad1c33a015fa2713`  
		Last Modified: Wed, 13 Oct 2021 19:34:48 GMT  
		Size: 82.3 KB (82347 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
