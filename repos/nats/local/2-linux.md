# `nats:2.6.3-scratch`

## Docker Metadata

- Image ID: `sha256:ac63f03657f633c1b7e4dcfa469f560c160b8b8ac5b71c6267553ebe540bcf6f`
- Created: `2021-10-28T22:20:00.138430979Z`
- Virtual Size: ~ 12.16 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/nats-server"]`
- Command: `["--config","nats-server.conf"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
