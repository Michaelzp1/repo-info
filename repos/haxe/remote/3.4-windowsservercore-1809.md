## `haxe:3.4-windowsservercore-1809`

```console
$ docker pull haxe@sha256:7f74c5ec3500ad500d8c4da817b5fc2b2d202584f1060330ed3f6a524e6842cb
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	windows version 10.0.17763.2183; amd64

### `haxe:3.4-windowsservercore-1809` - windows version 10.0.17763.2183; amd64

```console
$ docker pull haxe@sha256:20e8c0cc520fb318c97e80646fb85934e0991777f6db9e00b23dd0695aa2a48f
```

-	Docker Version: 20.10.8
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.7 GB (2709340523 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1a1879befbde68da7b60f6fd60d5e2380d923fb5dfab0fa998e7d80059420268`
-	Default Command: `["haxe"]`
-	`SHELL`: `["powershell","-Command","$ErrorActionPreference = 'Stop';"]`

```dockerfile
# Thu, 07 May 2020 05:09:25 GMT
RUN Apply image 1809-RTM-amd64
# Mon, 13 Sep 2021 04:08:33 GMT
RUN Install update 1809-amd64
# Wed, 15 Sep 2021 13:14:12 GMT
SHELL [powershell -Command $ErrorActionPreference = 'Stop';]
# Wed, 15 Sep 2021 13:14:13 GMT
ENV HAXETOOLKIT_PATH=C:\HaxeToolkit
# Wed, 15 Sep 2021 13:14:14 GMT
ENV NEKOPATH=C:\HaxeToolkit\neko
# Wed, 15 Sep 2021 13:14:15 GMT
ENV HAXEPATH=C:\HaxeToolkit\haxe
# Wed, 15 Sep 2021 13:14:15 GMT
ENV HAXE_STD_PATH=C:\HaxeToolkit\haxe\std
# Wed, 15 Sep 2021 13:14:16 GMT
ENV HAXELIB_PATH=C:\HaxeToolkit\haxe\lib
# Wed, 15 Sep 2021 13:15:12 GMT
RUN $newPath = ('{0};{1};{2}' -f $env:HAXEPATH, $env:NEKOPATH, $env:PATH); 	Write-Host ('Updating PATH: {0}' -f $newPath); 	[Environment]::SetEnvironmentVariable('PATH', $newPath, [EnvironmentVariableTarget]::Machine);
# Wed, 15 Sep 2021 13:16:32 GMT
RUN $url = 'https://download.microsoft.com/download/0/5/6/056dcda9-d667-4e27-8001-8a0c6971d6b1/vcredist_x86.exe'; 	Write-Host ('Downloading {0} ...' -f $url); 	[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; 	Invoke-WebRequest -Uri $url -OutFile 'vcredist_x86.exe'; 		Write-Host 'Verifying sha256 (89f4e593ea5541d1c53f983923124f9fd061a1c0c967339109e375c661573c17) ...'; 	if ((Get-FileHash vcredist_x86.exe -Algorithm sha256).Hash -ne '89f4e593ea5541d1c53f983923124f9fd061a1c0c967339109e375c661573c17') { 		Write-Host 'FAILED!'; 		exit 1; 	}; 		Write-Host 'Installing ...'; 	Start-Process -FilePath "vcredist_x86.exe" -ArgumentList "/Q" -Wait; 		Write-Host 'Removing installer...'; 	Remove-Item .\vcredist_x86.exe; 		Write-Host 'Complete.';
# Wed, 15 Sep 2021 13:17:22 GMT
RUN New-Item -ItemType directory -Path $env:HAXETOOLKIT_PATH;
# Wed, 15 Sep 2021 13:17:23 GMT
ENV NEKO_VERSION=2.3.0
# Wed, 15 Sep 2021 14:10:05 GMT
RUN $url = 'https://github.com/HaxeFoundation/neko/releases/download/v2-3-0/neko-2.3.0-win.zip'; 	Write-Host ('Downloading {0} ...' -f $url); 	[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; 	Invoke-WebRequest -Uri $url -OutFile 'neko.zip'; 		Write-Host 'Verifying sha256 (fe5a11350d2dd74338f971d62115f2bd21ec6912f193db04c5d28eb987a50485) ...'; 	if ((Get-FileHash neko.zip -Algorithm sha256).Hash -ne 'fe5a11350d2dd74338f971d62115f2bd21ec6912f193db04c5d28eb987a50485') { 		Write-Host 'FAILED!'; 		exit 1; 	}; 		Write-Host 'Expanding ...'; 	New-Item -ItemType directory -Path tmp; 	Expand-Archive -Path neko.zip -DestinationPath tmp; 	if (Test-Path tmp\neko.exe) { Move-Item tmp $env:NEKOPATH } 	else { Move-Item (Resolve-Path tmp\neko* | Select -ExpandProperty Path) $env:NEKOPATH }; 		Write-Host 'Removing ...'; 	Remove-Item -Path neko.zip, tmp -Force -Recurse -ErrorAction Ignore; 		Write-Host 'Verifying install ...'; 	Write-Host '  neko -version'; neko -version; 		Write-Host 'Complete.';
# Wed, 15 Sep 2021 14:10:06 GMT
ENV HAXE_VERSION=3.4.7
# Wed, 15 Sep 2021 14:13:17 GMT
RUN $url = 'https://github.com/HaxeFoundation/haxe/releases/download/3.4.7/haxe-3.4.7-win64.zip'; 	Write-Host ('Downloading {0} ...' -f $url); 	[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; 	Invoke-WebRequest -Uri $url -OutFile haxe.zip; 		Write-Host 'Verifying sha256 (609acdcb58a2253e357487d495ffe19e9034165f3102f8716ca968afbee8f1b2) ...'; 	if ((Get-FileHash haxe.zip -Algorithm sha256).Hash -ne '609acdcb58a2253e357487d495ffe19e9034165f3102f8716ca968afbee8f1b2') { 		Write-Host 'FAILED!'; 		exit 1; 	}; 		Write-Host 'Expanding ...'; 	New-Item -ItemType directory -Path tmp; 	Expand-Archive -Path haxe.zip -DestinationPath tmp; 	if (Test-Path tmp\haxe.exe) { Move-Item tmp $env:HAXEPATH } 	else { Move-Item (Resolve-Path tmp\haxe* | Select -ExpandProperty Path) $env:HAXEPATH }; 		Write-Host 'Removing ...'; 	Remove-Item -Path haxe.zip, tmp -Force -Recurse -ErrorAction Ignore; 		Write-Host 'Verifying install ...'; 	Write-Host '  haxe -version'; haxe -version; 	Write-Host '  haxelib version'; haxelib version; 		Write-Host 'Complete.';
# Wed, 15 Sep 2021 14:14:09 GMT
RUN New-Item -ItemType directory -Path $env:HAXELIB_PATH;
# Wed, 15 Sep 2021 14:14:10 GMT
ENV HOMEDRIVE=C:
# Wed, 15 Sep 2021 14:14:52 GMT
RUN $newPath = ('{0}\Users\{1}' -f $env:HOMEDRIVE, $env:USERNAME); 	Write-Host ('Updating HOMEPATH: {0}' -f $newPath); 	[Environment]::SetEnvironmentVariable('HOMEPATH', $newPath, [EnvironmentVariableTarget]::Machine);
# Wed, 15 Sep 2021 14:15:38 GMT
RUN (New-Object System.Net.WebClient).DownloadString('https://lib.haxe.org') >$null
# Wed, 15 Sep 2021 14:16:25 GMT
RUN (New-Object System.Net.WebClient).DownloadString('https://d1smpvufia21az.cloudfront.net') >$null
# Wed, 15 Sep 2021 14:16:25 GMT
CMD ["haxe"]
```

-	Layers:
	-	`sha256:4612f6d0b889cad0ed0292fae3a0b0c8a9e49aff6dea8eb049b2386d9b07986f`  
		Size: 1.7 GB (1718332879 bytes)  
		MIME: application/vnd.docker.image.rootfs.foreign.diff.tar.gzip
	-	`sha256:a0ddf88812f10c7d6edc858aa9784ff5ca9de4a7bb631909c090090343abd059`  
		Size: 968.4 MB (968365008 bytes)  
		MIME: application/vnd.docker.image.rootfs.foreign.diff.tar.gzip
	-	`sha256:6c4091e033b8590db7b89fded6d31ac2224162744daa4d7a7a66cbebd4b8c228`  
		Last Modified: Wed, 15 Sep 2021 15:04:44 GMT  
		Size: 1.4 KB (1435 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:af4bf2f9de6d0690ec307aa7ae3e887dfa6e4c098acf2d2cbb1d2c7500b57318`  
		Last Modified: Wed, 15 Sep 2021 15:04:43 GMT  
		Size: 1.4 KB (1435 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:2aa57ef34506aac4c09acc76c7ccd5520b96922bbcce0a89921827c79a7b81d5`  
		Last Modified: Wed, 15 Sep 2021 15:04:42 GMT  
		Size: 1.4 KB (1415 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:dba824f569a6227c52a7f2268028fc72f454669fc3cf56f6fd3a447dc1294d5d`  
		Last Modified: Wed, 15 Sep 2021 15:04:41 GMT  
		Size: 1.5 KB (1456 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:124c38214ca903c190b5bd5b0b303be1ae81bc8656b4f9309548ec9bf91fb4ce`  
		Last Modified: Wed, 15 Sep 2021 15:04:41 GMT  
		Size: 1.4 KB (1417 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:76c399cb6bf356814c0a040600ae5c65bedb10faea4e19c47e16bd365c8f7674`  
		Last Modified: Wed, 15 Sep 2021 15:04:40 GMT  
		Size: 1.4 KB (1384 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:c2a26194f6a2e4d4c3380182dc390fb75f129711a4ded824d6079127f5840769`  
		Last Modified: Wed, 15 Sep 2021 15:04:39 GMT  
		Size: 353.9 KB (353948 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:ecd5a7f61c2e061040b957d8b19b148b536d4d8da99d6349b5fadfa69dd8bde2`  
		Last Modified: Wed, 15 Sep 2021 15:04:53 GMT  
		Size: 12.9 MB (12940205 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:d57266b37b24f40b6627684cc653ecb7ca25ecf0f5dd7ad2a4900d591b6a8e4a`  
		Last Modified: Wed, 15 Sep 2021 15:04:38 GMT  
		Size: 326.7 KB (326661 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:aa382991530ae2bb237a1d8e17bb8de32687807e31f1f4d577e95d0e2558b350`  
		Last Modified: Wed, 15 Sep 2021 15:04:36 GMT  
		Size: 1.4 KB (1376 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:7599ad0bfaa0c90f601dd121166974b2eb91b6539e3f7ebabf9c5740c983f940`  
		Last Modified: Wed, 15 Sep 2021 15:07:03 GMT  
		Size: 1.9 MB (1947623 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:545b2b8c637be810dd3a063664e9fca28dae16744cb57c07cdf9b97ffd02dd44`  
		Last Modified: Wed, 15 Sep 2021 15:07:01 GMT  
		Size: 1.4 KB (1386 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:409f975b12b68f2357bf6aa58de6e8cef230db2706eac15d97b37c68e5a7f062`  
		Last Modified: Wed, 15 Sep 2021 15:07:07 GMT  
		Size: 5.6 MB (5579359 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:51e4c271409ef0041bd774fd483b1fb21d5d59912605a0c113d4d7c35d5b0034`  
		Last Modified: Wed, 15 Sep 2021 15:07:01 GMT  
		Size: 347.5 KB (347518 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:afb8450d6f555a41b46f7e6ec64c472a4b69b51623eaf38c878b419c78484049`  
		Last Modified: Wed, 15 Sep 2021 15:06:58 GMT  
		Size: 1.3 KB (1291 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:3a94834f60a28f463301e86e9f6a73869347c12007ba126095183d635ad96726`  
		Last Modified: Wed, 15 Sep 2021 15:06:58 GMT  
		Size: 358.7 KB (358712 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:bcfaa7681544b0bb47091a801d4a734da158f3b1e057db84a285165e4445cf98`  
		Last Modified: Wed, 15 Sep 2021 15:06:58 GMT  
		Size: 382.2 KB (382202 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:7091d9735fe3ea3ec7eec8e0bf794493efedd737ebf37a4cf63b07596ae36b24`  
		Last Modified: Wed, 15 Sep 2021 15:06:58 GMT  
		Size: 392.4 KB (392421 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:5a8befe4817f857882d752e7f56cf596423eff0d0c97073c9ec14d73881a980f`  
		Last Modified: Wed, 15 Sep 2021 15:06:58 GMT  
		Size: 1.4 KB (1392 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
