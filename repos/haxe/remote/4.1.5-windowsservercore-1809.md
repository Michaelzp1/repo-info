## `haxe:4.1.5-windowsservercore-1809`

```console
$ docker pull haxe@sha256:10379c2228e803c338bba1f7f32cb58af09ac04a79f55124dbdb288f91352ce3
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	windows version 10.0.17763.2183; amd64

### `haxe:4.1.5-windowsservercore-1809` - windows version 10.0.17763.2183; amd64

```console
$ docker pull haxe@sha256:bb05664095501c532c0f185082a6b190d3fe115a4697b7b5ff268c0d448e878d
```

-	Docker Version: 20.10.8
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.7 GB (2711651679 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:54338bb4c6e48d5c889b4e227afa97d6b52bafab96c035437d9896e7eb0761c4`
-	Default Command: `["haxe"]`
-	`SHELL`: `["powershell","-Command","$ErrorActionPreference = 'Stop';"]`

```dockerfile
# Thu, 07 May 2020 05:09:25 GMT
RUN Apply image 1809-RTM-amd64
# Mon, 13 Sep 2021 04:08:33 GMT
RUN Install update 1809-amd64
# Wed, 15 Sep 2021 13:14:12 GMT
SHELL [powershell -Command $ErrorActionPreference = 'Stop';]
# Wed, 15 Sep 2021 13:14:13 GMT
ENV HAXETOOLKIT_PATH=C:\HaxeToolkit
# Wed, 15 Sep 2021 13:14:14 GMT
ENV NEKOPATH=C:\HaxeToolkit\neko
# Wed, 15 Sep 2021 13:14:15 GMT
ENV HAXEPATH=C:\HaxeToolkit\haxe
# Wed, 15 Sep 2021 13:14:15 GMT
ENV HAXE_STD_PATH=C:\HaxeToolkit\haxe\std
# Wed, 15 Sep 2021 13:14:16 GMT
ENV HAXELIB_PATH=C:\HaxeToolkit\haxe\lib
# Wed, 15 Sep 2021 13:15:12 GMT
RUN $newPath = ('{0};{1};{2}' -f $env:HAXEPATH, $env:NEKOPATH, $env:PATH); 	Write-Host ('Updating PATH: {0}' -f $newPath); 	[Environment]::SetEnvironmentVariable('PATH', $newPath, [EnvironmentVariableTarget]::Machine);
# Wed, 15 Sep 2021 13:16:32 GMT
RUN $url = 'https://download.microsoft.com/download/0/5/6/056dcda9-d667-4e27-8001-8a0c6971d6b1/vcredist_x86.exe'; 	Write-Host ('Downloading {0} ...' -f $url); 	[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; 	Invoke-WebRequest -Uri $url -OutFile 'vcredist_x86.exe'; 		Write-Host 'Verifying sha256 (89f4e593ea5541d1c53f983923124f9fd061a1c0c967339109e375c661573c17) ...'; 	if ((Get-FileHash vcredist_x86.exe -Algorithm sha256).Hash -ne '89f4e593ea5541d1c53f983923124f9fd061a1c0c967339109e375c661573c17') { 		Write-Host 'FAILED!'; 		exit 1; 	}; 		Write-Host 'Installing ...'; 	Start-Process -FilePath "vcredist_x86.exe" -ArgumentList "/Q" -Wait; 		Write-Host 'Removing installer...'; 	Remove-Item .\vcredist_x86.exe; 		Write-Host 'Complete.';
# Wed, 15 Sep 2021 13:17:22 GMT
RUN New-Item -ItemType directory -Path $env:HAXETOOLKIT_PATH;
# Wed, 15 Sep 2021 13:17:23 GMT
ENV NEKO_VERSION=2.3.0
# Wed, 15 Sep 2021 13:18:25 GMT
RUN $url = 'https://github.com/HaxeFoundation/neko/releases/download/v2-3-0/neko-2.3.0-win64.zip'; 	Write-Host ('Downloading {0} ...' -f $url); 	[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; 	Invoke-WebRequest -Uri $url -OutFile 'neko.zip'; 		Write-Host 'Verifying sha256 (d09fdf362cd2e3274f6c8528be7211663260c3a5323ce893b7637c2818995f0b) ...'; 	if ((Get-FileHash neko.zip -Algorithm sha256).Hash -ne 'd09fdf362cd2e3274f6c8528be7211663260c3a5323ce893b7637c2818995f0b') { 		Write-Host 'FAILED!'; 		exit 1; 	}; 		Write-Host 'Expanding ...'; 	New-Item -ItemType directory -Path tmp; 	Expand-Archive -Path neko.zip -DestinationPath tmp; 	if (Test-Path tmp\neko.exe) { Move-Item tmp $env:NEKOPATH } 	else { Move-Item (Resolve-Path tmp\neko* | Select -ExpandProperty Path) $env:NEKOPATH }; 		Write-Host 'Removing ...'; 	Remove-Item -Path neko.zip, tmp -Force -Recurse -ErrorAction Ignore; 		Write-Host 'Verifying install ...'; 	Write-Host '  neko -version'; neko -version; 		Write-Host 'Complete.';
# Wed, 15 Sep 2021 13:38:50 GMT
ENV HAXE_VERSION=4.1.5
# Wed, 15 Sep 2021 13:42:32 GMT
RUN $url = 'https://github.com/HaxeFoundation/haxe/releases/download/4.1.5/haxe-4.1.5-win64.zip'; 	Write-Host ('Downloading {0} ...' -f $url); 	[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; 	Invoke-WebRequest -Uri $url -OutFile haxe.zip; 		Write-Host 'Verifying sha256 (ce4134cdf49814f8f8694648408d006116bd171b957a37be74c79cf403db9633) ...'; 	if ((Get-FileHash haxe.zip -Algorithm sha256).Hash -ne 'ce4134cdf49814f8f8694648408d006116bd171b957a37be74c79cf403db9633') { 		Write-Host 'FAILED!'; 		exit 1; 	}; 		Write-Host 'Expanding ...'; 	New-Item -ItemType directory -Path tmp; 	Expand-Archive -Path haxe.zip -DestinationPath tmp; 	if (Test-Path tmp\haxe.exe) { Move-Item tmp $env:HAXEPATH } 	else { Move-Item (Resolve-Path tmp\haxe* | Select -ExpandProperty Path) $env:HAXEPATH }; 		Write-Host 'Removing ...'; 	Remove-Item -Path haxe.zip, tmp -Force -Recurse -ErrorAction Ignore; 		Write-Host 'Verifying install ...'; 	Write-Host '  haxe -version'; haxe -version; 	Write-Host '  haxelib version'; haxelib version; 		Write-Host 'Complete.';
# Wed, 15 Sep 2021 13:43:19 GMT
RUN New-Item -ItemType directory -Path $env:HAXELIB_PATH;
# Wed, 15 Sep 2021 13:43:20 GMT
ENV HOMEDRIVE=C:
# Wed, 15 Sep 2021 13:44:10 GMT
RUN $newPath = ('{0}\Users\{1}' -f $env:HOMEDRIVE, $env:USERNAME); 	Write-Host ('Updating HOMEPATH: {0}' -f $newPath); 	[Environment]::SetEnvironmentVariable('HOMEPATH', $newPath, [EnvironmentVariableTarget]::Machine);
# Wed, 15 Sep 2021 13:44:58 GMT
RUN (New-Object System.Net.WebClient).DownloadString('https://lib.haxe.org') >$null
# Wed, 15 Sep 2021 13:45:41 GMT
RUN (New-Object System.Net.WebClient).DownloadString('https://d1smpvufia21az.cloudfront.net') >$null
# Wed, 15 Sep 2021 13:45:42 GMT
CMD ["haxe"]
```

-	Layers:
	-	`sha256:4612f6d0b889cad0ed0292fae3a0b0c8a9e49aff6dea8eb049b2386d9b07986f`  
		Size: 1.7 GB (1718332879 bytes)  
		MIME: application/vnd.docker.image.rootfs.foreign.diff.tar.gzip
	-	`sha256:a0ddf88812f10c7d6edc858aa9784ff5ca9de4a7bb631909c090090343abd059`  
		Size: 968.4 MB (968365008 bytes)  
		MIME: application/vnd.docker.image.rootfs.foreign.diff.tar.gzip
	-	`sha256:6c4091e033b8590db7b89fded6d31ac2224162744daa4d7a7a66cbebd4b8c228`  
		Last Modified: Wed, 15 Sep 2021 15:04:44 GMT  
		Size: 1.4 KB (1435 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:af4bf2f9de6d0690ec307aa7ae3e887dfa6e4c098acf2d2cbb1d2c7500b57318`  
		Last Modified: Wed, 15 Sep 2021 15:04:43 GMT  
		Size: 1.4 KB (1435 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:2aa57ef34506aac4c09acc76c7ccd5520b96922bbcce0a89921827c79a7b81d5`  
		Last Modified: Wed, 15 Sep 2021 15:04:42 GMT  
		Size: 1.4 KB (1415 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:dba824f569a6227c52a7f2268028fc72f454669fc3cf56f6fd3a447dc1294d5d`  
		Last Modified: Wed, 15 Sep 2021 15:04:41 GMT  
		Size: 1.5 KB (1456 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:124c38214ca903c190b5bd5b0b303be1ae81bc8656b4f9309548ec9bf91fb4ce`  
		Last Modified: Wed, 15 Sep 2021 15:04:41 GMT  
		Size: 1.4 KB (1417 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:76c399cb6bf356814c0a040600ae5c65bedb10faea4e19c47e16bd365c8f7674`  
		Last Modified: Wed, 15 Sep 2021 15:04:40 GMT  
		Size: 1.4 KB (1384 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:c2a26194f6a2e4d4c3380182dc390fb75f129711a4ded824d6079127f5840769`  
		Last Modified: Wed, 15 Sep 2021 15:04:39 GMT  
		Size: 353.9 KB (353948 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:ecd5a7f61c2e061040b957d8b19b148b536d4d8da99d6349b5fadfa69dd8bde2`  
		Last Modified: Wed, 15 Sep 2021 15:04:53 GMT  
		Size: 12.9 MB (12940205 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:d57266b37b24f40b6627684cc653ecb7ca25ecf0f5dd7ad2a4900d591b6a8e4a`  
		Last Modified: Wed, 15 Sep 2021 15:04:38 GMT  
		Size: 326.7 KB (326661 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:aa382991530ae2bb237a1d8e17bb8de32687807e31f1f4d577e95d0e2558b350`  
		Last Modified: Wed, 15 Sep 2021 15:04:36 GMT  
		Size: 1.4 KB (1376 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:e30c22063a403723a85bff67c91b52962211c95d2da8655fa470509d38920aba`  
		Last Modified: Wed, 15 Sep 2021 15:04:38 GMT  
		Size: 2.2 MB (2155568 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:eeb3895db185748c186eae1bc73f7881ad3d3135cbf1e2ab83fc16498e0b5717`  
		Last Modified: Wed, 15 Sep 2021 15:05:37 GMT  
		Size: 1.3 KB (1284 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:ac524f5717c5eea564085c5523b87a34f9015d44d60e7745848915204958506d`  
		Last Modified: Wed, 15 Sep 2021 15:05:40 GMT  
		Size: 7.7 MB (7680331 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:22ab1ab537b6d0cd1ed6b830181a54a8dd9dffe2d733caa2a776765348ddc29c`  
		Last Modified: Wed, 15 Sep 2021 15:05:37 GMT  
		Size: 347.9 KB (347919 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:8fc0ee99fb7dae36935a1cfde4224c6e0134d318b9b236ce3175d9dd233030bd`  
		Last Modified: Wed, 15 Sep 2021 15:05:34 GMT  
		Size: 1.3 KB (1275 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:05dc8416f3417b8edc62cc85ac053b3dd4544f987f3297f4810a56e071feb308`  
		Last Modified: Wed, 15 Sep 2021 15:05:34 GMT  
		Size: 359.4 KB (359375 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:c8543b31e9c90aa32131283a4955a964ca206e6ce8dced0cd72d3854467a1b39`  
		Last Modified: Wed, 15 Sep 2021 15:05:34 GMT  
		Size: 382.6 KB (382625 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:1d7b56843851c4e1846e2d220e5cedfab739126656549013d76271b1d5dc5105`  
		Last Modified: Wed, 15 Sep 2021 15:05:34 GMT  
		Size: 393.4 KB (393394 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:7681bea8c9abba3b93f8c6acaba8a301cc30d06e378ec7756b0b96a1f0b1fe8f`  
		Last Modified: Wed, 15 Sep 2021 15:05:34 GMT  
		Size: 1.3 KB (1289 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
