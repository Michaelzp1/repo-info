## `haxe:3.3-alpine3.14`

```console
$ docker pull haxe@sha256:8a1ac661ab4397ca5845ad6175fb044c7c1fcdc536c404b3448fcbbb5410f976
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 2
	-	linux; amd64
	-	linux; arm64 variant v8

### `haxe:3.3-alpine3.14` - linux; amd64

```console
$ docker pull haxe@sha256:bb2d0467d379ce822d50df36364ec2de620f23374dc6bbe8d2e48b928da14649
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **27.4 MB (27415920 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ad3b9d1158f7b870c1e2695b3d76bded30edf301e9869ab2dea11d639f9835e8`
-	Default Command: `["haxe"]`

```dockerfile
# Fri, 27 Aug 2021 17:19:45 GMT
ADD file:aad4290d27580cc1a094ffaf98c3ca2fc5d699fe695dfb8e6e9fac20f1129450 in / 
# Fri, 27 Aug 2021 17:19:45 GMT
CMD ["/bin/sh"]
# Fri, 27 Aug 2021 19:27:41 GMT
ENV PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
# Fri, 27 Aug 2021 19:27:43 GMT
RUN apk add --no-cache ca-certificates
# Fri, 27 Aug 2021 19:27:43 GMT
ENV NEKO_VERSION=2.3.0
# Fri, 27 Aug 2021 20:00:08 GMT
ENV HAXE_VERSION=3.3.0-rc.1
# Fri, 27 Aug 2021 20:00:08 GMT
ENV HAXE_STD_PATH=/usr/local/share/haxe/std
# Fri, 27 Aug 2021 20:11:33 GMT
RUN set -ex 	&& apk add --no-cache --virtual .fetch-deps 		tar 		git 		&& wget -O neko.tar.gz "https://github.com/HaxeFoundation/neko/archive/v2-3-0/neko-2.3.0.tar.gz" 	&& echo "850e7e317bdaf24ed652efeff89c1cb21380ca19f20e68a296c84f6bad4ee995 *neko.tar.gz" | sha256sum -c - 	&& mkdir -p /usr/src/neko 	&& tar -xC /usr/src/neko --strip-components=1 -f neko.tar.gz 	&& rm neko.tar.gz 	&& apk add --no-cache --virtual .neko-build-deps 		apache2-dev 		cmake 		gc-dev 		gcc 		gtk+2.0-dev 		libc-dev 		linux-headers 		mariadb-dev 		mbedtls-dev 		ninja 		sqlite-dev 	&& cd /usr/src/neko 	&& cmake -GNinja -DNEKO_JIT_DISABLE=ON -DRELOCATABLE=OFF -DRUN_LDCONFIG=OFF . 	&& ninja 	&& ninja install 		&& git clone --recursive --depth 1 --branch 3.3.0-rc1 "https://github.com/HaxeFoundation/haxe.git" /usr/src/haxe 	&& cd /usr/src/haxe 	&& mkdir -p $HAXE_STD_PATH 	&& cp -r std/* $HAXE_STD_PATH 	&& apk add --no-cache --virtual .haxe-build-deps 		pcre-dev 		zlib-dev 		mbedtls-dev 		make 		opam 		aspcud 		m4 		unzip 		patch 		musl-dev 		perl-string-shellquote 		perl-ipc-system-simple 		ocaml-compiler-libs 		ocaml-ocamldoc 	&& opam init --compiler=4.09.1 --disable-sandboxing 	&& eval $(opam env) 	&& opam pin add haxe . --no-action 		&& opam install haxe --deps-only --yes 	&& opam install camlp4 	&& env OCAMLPARAM=safe-string=0,_ make all tools 		&& eval $(opam env --revert) 	&& mkdir -p /usr/local/bin 	&& cp haxe haxelib /usr/local/bin 	&& mkdir -p /haxelib 	&& cd / && haxelib setup /haxelib 		&& runDeps="$( 		scanelf --needed --nobanner --recursive /usr/local 			| awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' 			| sort -u 			| xargs -r apk info --installed 			| sort -u 	)" 	&& apk add --virtual .haxe-rundeps $runDeps 	&& apk del .fetch-deps .neko-build-deps .haxe-build-deps 	&& rm -rf ~/.opam 	&& rm -rf /usr/src/neko /usr/src/haxe
# Fri, 27 Aug 2021 20:11:33 GMT
CMD ["haxe"]
```

-	Layers:
	-	`sha256:a0d0a0d46f8b52473982a3c466318f479767577551a53ffc9074c9fa7035982e`  
		Last Modified: Fri, 27 Aug 2021 17:20:13 GMT  
		Size: 2.8 MB (2814446 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:f78d44baa25c170276b4726fb07f56a94abe669fafd7415e5d7137f7a84b4467`  
		Last Modified: Fri, 27 Aug 2021 20:25:28 GMT  
		Size: 281.5 KB (281493 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:5205117396370ecf4f1c4a88a5b3355273ba70c990412316325393b967db2ac2`  
		Last Modified: Fri, 27 Aug 2021 20:27:25 GMT  
		Size: 24.3 MB (24319981 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `haxe:3.3-alpine3.14` - linux; arm64 variant v8

```console
$ docker pull haxe@sha256:a00279e38b982bfd99c78270c648649935bba05e85f5397583aae8841401560c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **28.4 MB (28414960 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:693e0e051556159ff9524e2b4cb8508287f019eb51bb537c7224fabe5ffa3567`
-	Default Command: `["haxe"]`

```dockerfile
# Fri, 27 Aug 2021 17:39:33 GMT
ADD file:dc8af9c7bfe9f9541e1db38dea01c4201609f3075b31e108f2073ffed8c5e4b9 in / 
# Fri, 27 Aug 2021 17:39:33 GMT
CMD ["/bin/sh"]
# Tue, 12 Oct 2021 20:18:52 GMT
ENV PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
# Tue, 12 Oct 2021 20:18:54 GMT
RUN apk add --no-cache ca-certificates
# Tue, 12 Oct 2021 20:18:54 GMT
ENV NEKO_VERSION=2.3.0
# Tue, 12 Oct 2021 21:26:29 GMT
ENV HAXE_VERSION=3.3.0-rc.1
# Tue, 12 Oct 2021 21:26:30 GMT
ENV HAXE_STD_PATH=/usr/local/share/haxe/std
# Tue, 12 Oct 2021 21:32:36 GMT
RUN set -ex 	&& apk add --no-cache --virtual .fetch-deps 		tar 		git 		&& wget -O neko.tar.gz "https://github.com/HaxeFoundation/neko/archive/v2-3-0/neko-2.3.0.tar.gz" 	&& echo "850e7e317bdaf24ed652efeff89c1cb21380ca19f20e68a296c84f6bad4ee995 *neko.tar.gz" | sha256sum -c - 	&& mkdir -p /usr/src/neko 	&& tar -xC /usr/src/neko --strip-components=1 -f neko.tar.gz 	&& rm neko.tar.gz 	&& apk add --no-cache --virtual .neko-build-deps 		apache2-dev 		cmake 		gc-dev 		gcc 		gtk+2.0-dev 		libc-dev 		linux-headers 		mariadb-dev 		mbedtls-dev 		ninja 		sqlite-dev 	&& cd /usr/src/neko 	&& cmake -GNinja -DNEKO_JIT_DISABLE=ON -DRELOCATABLE=OFF -DRUN_LDCONFIG=OFF . 	&& ninja 	&& ninja install 		&& git clone --recursive --depth 1 --branch 3.3.0-rc1 "https://github.com/HaxeFoundation/haxe.git" /usr/src/haxe 	&& cd /usr/src/haxe 	&& mkdir -p $HAXE_STD_PATH 	&& cp -r std/* $HAXE_STD_PATH 	&& apk add --no-cache --virtual .haxe-build-deps 		pcre-dev 		zlib-dev 		mbedtls-dev 		make 		opam 		aspcud 		m4 		unzip 		patch 		musl-dev 		perl-string-shellquote 		perl-ipc-system-simple 		ocaml-compiler-libs 		ocaml-ocamldoc 	&& opam init --compiler=4.09.1 --disable-sandboxing 	&& eval $(opam env) 	&& opam pin add haxe . --no-action 		&& opam install haxe --deps-only --yes 	&& opam install camlp4 	&& env OCAMLPARAM=safe-string=0,_ make all tools 		&& eval $(opam env --revert) 	&& mkdir -p /usr/local/bin 	&& cp haxe haxelib /usr/local/bin 	&& mkdir -p /haxelib 	&& cd / && haxelib setup /haxelib 		&& runDeps="$( 		scanelf --needed --nobanner --recursive /usr/local 			| awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' 			| sort -u 			| xargs -r apk info --installed 			| sort -u 	)" 	&& apk add --virtual .haxe-rundeps $runDeps 	&& apk del .fetch-deps .neko-build-deps .haxe-build-deps 	&& rm -rf ~/.opam 	&& rm -rf /usr/src/neko /usr/src/haxe
# Tue, 12 Oct 2021 21:32:37 GMT
CMD ["haxe"]
```

-	Layers:
	-	`sha256:552d1f2373af9bfe12033568ebbfb0ccbb0de11279f9a415a29207e264d7f4d9`  
		Last Modified: Fri, 27 Aug 2021 17:40:18 GMT  
		Size: 2.7 MB (2711827 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:da80617bb116448de3547356f10e326a4f6983e3afd13a46f0e7bd803d96c7a3`  
		Last Modified: Tue, 12 Oct 2021 21:50:55 GMT  
		Size: 281.5 KB (281474 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:2e20fb08546702d4f8f4dcfdb7db181132d07e49238578149875d585774af3ef`  
		Last Modified: Tue, 12 Oct 2021 21:57:02 GMT  
		Size: 25.4 MB (25421659 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
