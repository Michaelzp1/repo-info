## `haxe:3.3-alpine3.11`

```console
$ docker pull haxe@sha256:051c7da4cc96719d19e3f352adf52871865375582d5ac6fc954f0d3ad05423ce
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 2
	-	linux; amd64
	-	linux; arm64 variant v8

### `haxe:3.3-alpine3.11` - linux; amd64

```console
$ docker pull haxe@sha256:b73e526cd586b1993d4bb865b26e6f67356b4600f4273bfe0c518bf97e27b338
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **25.6 MB (25569462 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:56876e9649745dfab814e3cb3dca987bc48267fff2bd79e7b3b28991b26812d4`
-	Default Command: `["haxe"]`

```dockerfile
# Tue, 31 Aug 2021 23:18:31 GMT
ADD file:9d14b11183983923090d9e6d15cc51ee210466296e913bfefbfd580b3de59c95 in / 
# Tue, 31 Aug 2021 23:18:31 GMT
CMD ["/bin/sh"]
# Wed, 01 Sep 2021 01:55:04 GMT
ENV PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
# Wed, 01 Sep 2021 01:55:05 GMT
RUN apk add --no-cache ca-certificates
# Wed, 01 Sep 2021 01:55:05 GMT
ENV NEKO_VERSION=2.3.0
# Wed, 01 Sep 2021 02:36:56 GMT
ENV HAXE_VERSION=3.3.0-rc.1
# Wed, 01 Sep 2021 02:36:57 GMT
ENV HAXE_STD_PATH=/usr/local/share/haxe/std
# Wed, 01 Sep 2021 02:37:45 GMT
RUN set -ex 	&& apk add --no-cache --virtual .fetch-deps 		libressl 		tar 		git 		&& wget -O neko.tar.gz "https://github.com/HaxeFoundation/neko/archive/v2-3-0/neko-2.3.0.tar.gz" 	&& echo "850e7e317bdaf24ed652efeff89c1cb21380ca19f20e68a296c84f6bad4ee995 *neko.tar.gz" | sha256sum -c - 	&& mkdir -p /usr/src/neko 	&& tar -xC /usr/src/neko --strip-components=1 -f neko.tar.gz 	&& rm neko.tar.gz 	&& apk add --no-cache --virtual .neko-build-deps 		apache2-dev 		cmake 		gc-dev 		gcc 		gtk+2.0-dev 		libc-dev 		linux-headers 		mariadb-dev 		mbedtls-dev 		ninja 		sqlite-dev 	&& cd /usr/src/neko 	&& cmake -GNinja -DNEKO_JIT_DISABLE=ON -DRELOCATABLE=OFF -DRUN_LDCONFIG=OFF . 	&& ninja 	&& ninja install 		&& git clone --recursive --depth 1 --branch 3.3.0-rc1 "https://github.com/HaxeFoundation/haxe.git" /usr/src/haxe 	&& cd /usr/src/haxe 	&& mkdir -p $HAXE_STD_PATH 	&& cp -r std/* $HAXE_STD_PATH 	&& apk add --no-cache --virtual .haxe-build-deps 		pcre-dev 		zlib-dev 		mbedtls-dev 		make 				ocaml 		camlp4 		ocaml-camlp4-dev 				&& OCAMLPARAM=safe-string=0,_ make all tools 		&& mkdir -p /usr/local/bin 	&& cp haxe haxelib /usr/local/bin 	&& mkdir -p /haxelib 	&& cd / && haxelib setup /haxelib 		&& runDeps="$( 		scanelf --needed --nobanner --recursive /usr/local 			| awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' 			| sort -u 			| xargs -r apk info --installed 			| sort -u 	)" 	&& apk add --virtual .haxe-rundeps $runDeps 	&& apk del .fetch-deps .neko-build-deps .haxe-build-deps 		&& rm -rf /usr/src/neko /usr/src/haxe
# Wed, 01 Sep 2021 02:37:45 GMT
CMD ["haxe"]
```

-	Layers:
	-	`sha256:6a428f9f83b0a29f1fdd2ccccca19a9bab805a925b8eddf432a5a3d3da04afbc`  
		Last Modified: Tue, 31 Aug 2021 23:19:15 GMT  
		Size: 2.8 MB (2817307 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:d98308738eedbe094727accbfd928def5ca3f9b9e91de4c373b9395d7c4021a1`  
		Last Modified: Wed, 01 Sep 2021 02:44:50 GMT  
		Size: 299.7 KB (299656 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:5e66544c7a09c46398cf551183b98a0b0d63516d50b02b9f525734119cfed902`  
		Last Modified: Wed, 01 Sep 2021 02:48:18 GMT  
		Size: 22.5 MB (22452499 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `haxe:3.3-alpine3.11` - linux; arm64 variant v8

```console
$ docker pull haxe@sha256:5c49facdd6a95f715a1b82c1243f9e6080ecf7c189424d750a4aa7ee3f25091a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **26.6 MB (26638925 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:c21191e699c3ce4c9ace9abd67ea476d9f39a224b1ce9c8fa9836474c82e804f`
-	Default Command: `["haxe"]`

```dockerfile
# Wed, 01 Sep 2021 02:50:59 GMT
ADD file:da6c0ac7cb9f819998546d88fb489b746004eb2ad6da64a39210696ef0e66e54 in / 
# Wed, 01 Sep 2021 02:50:59 GMT
CMD ["/bin/sh"]
# Tue, 12 Oct 2021 20:29:44 GMT
ENV PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
# Tue, 12 Oct 2021 20:29:46 GMT
RUN apk add --no-cache ca-certificates
# Tue, 12 Oct 2021 20:29:47 GMT
ENV NEKO_VERSION=2.3.0
# Tue, 12 Oct 2021 21:34:57 GMT
ENV HAXE_VERSION=3.3.0-rc.1
# Tue, 12 Oct 2021 21:34:58 GMT
ENV HAXE_STD_PATH=/usr/local/share/haxe/std
# Tue, 12 Oct 2021 21:35:43 GMT
RUN set -ex 	&& apk add --no-cache --virtual .fetch-deps 		libressl 		tar 		git 		&& wget -O neko.tar.gz "https://github.com/HaxeFoundation/neko/archive/v2-3-0/neko-2.3.0.tar.gz" 	&& echo "850e7e317bdaf24ed652efeff89c1cb21380ca19f20e68a296c84f6bad4ee995 *neko.tar.gz" | sha256sum -c - 	&& mkdir -p /usr/src/neko 	&& tar -xC /usr/src/neko --strip-components=1 -f neko.tar.gz 	&& rm neko.tar.gz 	&& apk add --no-cache --virtual .neko-build-deps 		apache2-dev 		cmake 		gc-dev 		gcc 		gtk+2.0-dev 		libc-dev 		linux-headers 		mariadb-dev 		mbedtls-dev 		ninja 		sqlite-dev 	&& cd /usr/src/neko 	&& cmake -GNinja -DNEKO_JIT_DISABLE=ON -DRELOCATABLE=OFF -DRUN_LDCONFIG=OFF . 	&& ninja 	&& ninja install 		&& git clone --recursive --depth 1 --branch 3.3.0-rc1 "https://github.com/HaxeFoundation/haxe.git" /usr/src/haxe 	&& cd /usr/src/haxe 	&& mkdir -p $HAXE_STD_PATH 	&& cp -r std/* $HAXE_STD_PATH 	&& apk add --no-cache --virtual .haxe-build-deps 		pcre-dev 		zlib-dev 		mbedtls-dev 		make 				ocaml 		camlp4 		ocaml-camlp4-dev 				&& OCAMLPARAM=safe-string=0,_ make all tools 		&& mkdir -p /usr/local/bin 	&& cp haxe haxelib /usr/local/bin 	&& mkdir -p /haxelib 	&& cd / && haxelib setup /haxelib 		&& runDeps="$( 		scanelf --needed --nobanner --recursive /usr/local 			| awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' 			| sort -u 			| xargs -r apk info --installed 			| sort -u 	)" 	&& apk add --virtual .haxe-rundeps $runDeps 	&& apk del .fetch-deps .neko-build-deps .haxe-build-deps 		&& rm -rf /usr/src/neko /usr/src/haxe
# Tue, 12 Oct 2021 21:35:43 GMT
CMD ["haxe"]
```

-	Layers:
	-	`sha256:07d756952c5cd45726cf9e8a292a3e05ca67eee5da176df7d632be8c5bb0ad04`  
		Last Modified: Wed, 01 Sep 2021 02:52:00 GMT  
		Size: 2.7 MB (2728407 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:1144c1f7d844694f5f0b7925b133be22af5840f7f6ac5ec68a782bc70b4e5acd`  
		Last Modified: Tue, 12 Oct 2021 21:51:47 GMT  
		Size: 299.9 KB (299872 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:dbddac5dd1097800a90bf1338d54c80ce134efb26c3b7096e3a2c867a231ae95`  
		Last Modified: Tue, 12 Oct 2021 21:58:06 GMT  
		Size: 23.6 MB (23610646 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
