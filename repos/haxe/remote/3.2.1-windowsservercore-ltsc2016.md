## `haxe:3.2.1-windowsservercore-ltsc2016`

```console
$ docker pull haxe@sha256:9356a3cc92f54b6783bec00c2ad4fa76b604c18e031939b00748c9f04413ea93
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	windows version 10.0.14393.4651; amd64

### `haxe:3.2.1-windowsservercore-ltsc2016` - windows version 10.0.14393.4651; amd64

```console
$ docker pull haxe@sha256:ff023236a4adf3c6685692e3370afed147a5d6208cb7a00e0b69e59be2452558
```

-	Docker Version: 20.10.8
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **6.3 GB (6296816030 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a01599e8642c5f029e38dbb0c7b02c52dbdda8440332a64dee4c826d57c1bec0`
-	Default Command: `["haxe"]`
-	`SHELL`: `["powershell","-Command","$ErrorActionPreference = 'Stop';"]`

```dockerfile
# Sat, 19 Nov 2016 17:05:00 GMT
RUN Apply image 1607-RTM-amd64
# Mon, 13 Sep 2021 01:03:00 GMT
RUN Install update ltsc2016-amd64
# Wed, 15 Sep 2021 13:26:09 GMT
SHELL [powershell -Command $ErrorActionPreference = 'Stop';]
# Wed, 15 Sep 2021 13:26:09 GMT
ENV HAXETOOLKIT_PATH=C:\HaxeToolkit
# Wed, 15 Sep 2021 13:26:11 GMT
ENV NEKOPATH=C:\HaxeToolkit\neko
# Wed, 15 Sep 2021 13:26:12 GMT
ENV HAXEPATH=C:\HaxeToolkit\haxe
# Wed, 15 Sep 2021 13:26:13 GMT
ENV HAXE_STD_PATH=C:\HaxeToolkit\haxe\std
# Wed, 15 Sep 2021 13:26:14 GMT
ENV HAXELIB_PATH=C:\HaxeToolkit\haxe\lib
# Wed, 15 Sep 2021 13:27:20 GMT
RUN $newPath = ('{0};{1};{2}' -f $env:HAXEPATH, $env:NEKOPATH, $env:PATH); 	Write-Host ('Updating PATH: {0}' -f $newPath); 	[Environment]::SetEnvironmentVariable('PATH', $newPath, [EnvironmentVariableTarget]::Machine);
# Wed, 15 Sep 2021 13:28:55 GMT
RUN $url = 'https://download.microsoft.com/download/0/5/6/056dcda9-d667-4e27-8001-8a0c6971d6b1/vcredist_x86.exe'; 	Write-Host ('Downloading {0} ...' -f $url); 	[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; 	Invoke-WebRequest -Uri $url -OutFile 'vcredist_x86.exe'; 		Write-Host 'Verifying sha256 (89f4e593ea5541d1c53f983923124f9fd061a1c0c967339109e375c661573c17) ...'; 	if ((Get-FileHash vcredist_x86.exe -Algorithm sha256).Hash -ne '89f4e593ea5541d1c53f983923124f9fd061a1c0c967339109e375c661573c17') { 		Write-Host 'FAILED!'; 		exit 1; 	}; 		Write-Host 'Installing ...'; 	Start-Process -FilePath "vcredist_x86.exe" -ArgumentList "/Q" -Wait; 		Write-Host 'Removing installer...'; 	Remove-Item .\vcredist_x86.exe; 		Write-Host 'Complete.';
# Wed, 15 Sep 2021 13:29:46 GMT
RUN New-Item -ItemType directory -Path $env:HAXETOOLKIT_PATH;
# Wed, 15 Sep 2021 13:29:47 GMT
ENV NEKO_VERSION=2.3.0
# Wed, 15 Sep 2021 14:17:42 GMT
RUN $url = 'https://github.com/HaxeFoundation/neko/releases/download/v2-3-0/neko-2.3.0-win.zip'; 	Write-Host ('Downloading {0} ...' -f $url); 	[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; 	Invoke-WebRequest -Uri $url -OutFile 'neko.zip'; 		Write-Host 'Verifying sha256 (fe5a11350d2dd74338f971d62115f2bd21ec6912f193db04c5d28eb987a50485) ...'; 	if ((Get-FileHash neko.zip -Algorithm sha256).Hash -ne 'fe5a11350d2dd74338f971d62115f2bd21ec6912f193db04c5d28eb987a50485') { 		Write-Host 'FAILED!'; 		exit 1; 	}; 		Write-Host 'Expanding ...'; 	New-Item -ItemType directory -Path tmp; 	Expand-Archive -Path neko.zip -DestinationPath tmp; 	if (Test-Path tmp\neko.exe) { Move-Item tmp $env:NEKOPATH } 	else { Move-Item (Resolve-Path tmp\neko* | Select -ExpandProperty Path) $env:NEKOPATH }; 		Write-Host 'Removing ...'; 	Remove-Item -Path neko.zip, tmp -Force -Recurse -ErrorAction Ignore; 		Write-Host 'Verifying install ...'; 	Write-Host '  neko -version'; neko -version; 		Write-Host 'Complete.';
# Wed, 15 Sep 2021 14:44:30 GMT
ENV HAXE_VERSION=3.2.1
# Wed, 15 Sep 2021 14:47:26 GMT
RUN $url = 'https://github.com/HaxeFoundation/haxe/releases/download/3.2.1/haxe-3.2.1-win.zip'; 	Write-Host ('Downloading {0} ...' -f $url); 	[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; 	Invoke-WebRequest -Uri $url -OutFile haxe.zip; 		Write-Host 'Verifying sha256 (af57d42ca474bba826426e9403b2cb21c210d56addc8bbc0e8fafa88b3660db3) ...'; 	if ((Get-FileHash haxe.zip -Algorithm sha256).Hash -ne 'af57d42ca474bba826426e9403b2cb21c210d56addc8bbc0e8fafa88b3660db3') { 		Write-Host 'FAILED!'; 		exit 1; 	}; 		Write-Host 'Expanding ...'; 	New-Item -ItemType directory -Path tmp; 	Expand-Archive -Path haxe.zip -DestinationPath tmp; 	if (Test-Path tmp\haxe.exe) { Move-Item tmp $env:HAXEPATH } 	else { Move-Item (Resolve-Path tmp\haxe* | Select -ExpandProperty Path) $env:HAXEPATH }; 		Write-Host 'Removing ...'; 	Remove-Item -Path haxe.zip, tmp -Force -Recurse -ErrorAction Ignore; 		Write-Host 'Verifying install ...'; 	Write-Host '  haxe -version'; haxe -version; 	Write-Host '  haxelib version'; haxelib version; 		Write-Host 'Complete.';
# Wed, 15 Sep 2021 14:48:22 GMT
RUN New-Item -ItemType directory -Path $env:HAXELIB_PATH;
# Wed, 15 Sep 2021 14:48:23 GMT
ENV HOMEDRIVE=C:
# Wed, 15 Sep 2021 14:49:12 GMT
RUN $newPath = ('{0}\Users\{1}' -f $env:HOMEDRIVE, $env:USERNAME); 	Write-Host ('Updating HOMEPATH: {0}' -f $newPath); 	[Environment]::SetEnvironmentVariable('HOMEPATH', $newPath, [EnvironmentVariableTarget]::Machine);
# Wed, 15 Sep 2021 14:50:10 GMT
RUN (New-Object System.Net.WebClient).DownloadString('https://lib.haxe.org') >$null
# Wed, 15 Sep 2021 14:50:59 GMT
RUN (New-Object System.Net.WebClient).DownloadString('https://d1smpvufia21az.cloudfront.net') >$null
# Wed, 15 Sep 2021 14:51:00 GMT
CMD ["haxe"]
```

-	Layers:
	-	`sha256:3889bb8d808bbae6fa5a33e07093e65c31371bcf9e4c38c21be6b9af52ad1548`  
		Size: 4.1 GB (4069985900 bytes)  
		MIME: application/vnd.docker.image.rootfs.foreign.diff.tar.gzip
	-	`sha256:e9b8281bf21e46c781fb54e4f15f5728e2c44dea4219c9e6deeb732f1d909d3b`  
		Size: 2.2 GB (2201342322 bytes)  
		MIME: application/vnd.docker.image.rootfs.foreign.diff.tar.gzip
	-	`sha256:5796468f91731000e9a76e836156826a1cd4ed963bcd80e6558c538a12c2132d`  
		Last Modified: Wed, 15 Sep 2021 15:05:15 GMT  
		Size: 1.4 KB (1408 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:cb81cf9111761ddfe646e14b3995aeb31cc62dfdd52e54bc224970ba4eba8fb3`  
		Last Modified: Wed, 15 Sep 2021 15:05:14 GMT  
		Size: 1.4 KB (1389 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:aa94e80ecf939b785e7cff1a96d5ea8dba0c7ef90c11cdc973895e5c5d6973b5`  
		Last Modified: Wed, 15 Sep 2021 15:05:14 GMT  
		Size: 1.4 KB (1434 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:e3cbb5076880a04d79b3f4bfedd245af485d5c68b239477df5344707a1ff8608`  
		Last Modified: Wed, 15 Sep 2021 15:05:14 GMT  
		Size: 1.4 KB (1416 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:338090c51a577c50769f47c0591d8743479d61d3a73b5368efc88b0abe3ee29a`  
		Last Modified: Wed, 15 Sep 2021 15:05:13 GMT  
		Size: 1.4 KB (1386 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:178a93c88e06aecb15e50d3916f8bf334090343631079254cd294fc520490c1d`  
		Last Modified: Wed, 15 Sep 2021 15:05:11 GMT  
		Size: 1.4 KB (1435 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:ad2375ea7a4a97e136359a714deebf459d9074a80c577b796050c0799829022a`  
		Last Modified: Wed, 15 Sep 2021 15:05:11 GMT  
		Size: 343.1 KB (343060 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:65edc0426667f07b34281a4ac33bbd8435c1048ac15561ee397b356f42f88852`  
		Last Modified: Wed, 15 Sep 2021 15:05:12 GMT  
		Size: 12.9 MB (12887941 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:25818b3c69036482575c269a2afbe8a4df05b24898f1af31a8295e9848aae663`  
		Last Modified: Wed, 15 Sep 2021 15:05:10 GMT  
		Size: 289.0 KB (289035 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:3a5aef79e155774c5b3a2bd63ff1045491fb5952220cb3b63dd8181f54942083`  
		Last Modified: Wed, 15 Sep 2021 15:05:07 GMT  
		Size: 1.4 KB (1395 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:4b10a411f2e54c61517807f148da280bd5c6e5a5335d068fc6adb2cbff84a829`  
		Last Modified: Wed, 15 Sep 2021 15:07:22 GMT  
		Size: 1.9 MB (1902582 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:55b033e4e7a1bbbc8b8b392a3b7761a9cab45e0b1991dd4c3505137f1b6469d1`  
		Last Modified: Wed, 15 Sep 2021 15:08:33 GMT  
		Size: 1.3 KB (1323 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:a6bd7f07b67cbbcf94689d3d8a2108c13263b7a07eeefa0ad5492916dfdf99c4`  
		Last Modified: Wed, 15 Sep 2021 15:08:42 GMT  
		Size: 8.8 MB (8803539 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:4ee2ec0e0adfddc4b23dcabcea41e5393708c22f8f5c442b6137549931191395`  
		Last Modified: Wed, 15 Sep 2021 15:08:33 GMT  
		Size: 308.5 KB (308518 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:080fe4f0828db7b04f44c7918d97cc7dabc80843812064a3198f295bfb143249`  
		Last Modified: Wed, 15 Sep 2021 15:08:30 GMT  
		Size: 1.4 KB (1380 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:0e23be8142dec1af17f36fb74bcb8856e16c2fea38e8e91b27238963f74bd2d4`  
		Last Modified: Wed, 15 Sep 2021 15:08:30 GMT  
		Size: 309.6 KB (309585 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:0da02f9ee7002fe5f3ad4ec7fde1b44f6aa24fdde7068cd9f34b8f34f9a7cd66`  
		Last Modified: Wed, 15 Sep 2021 15:08:30 GMT  
		Size: 314.5 KB (314516 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:33ca963f7f829456ebfccb7050f0897d449031ce3752a58a6097eda7a1bc45c7`  
		Last Modified: Wed, 15 Sep 2021 15:08:31 GMT  
		Size: 315.2 KB (315179 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:02495d79ec0d4935609cd0a5640f82e4987210c555c32e13a0757e40cd67b4dc`  
		Last Modified: Wed, 15 Sep 2021 15:08:30 GMT  
		Size: 1.3 KB (1287 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
