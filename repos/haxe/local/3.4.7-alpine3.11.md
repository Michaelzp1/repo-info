# `haxe:3.4.7-alpine3.11`

## Docker Metadata

- Image ID: `sha256:fececaee61ce836773dff3487270bf5551bb347bb9f08359e05de1a99ed8726f`
- Created: `2021-09-01T02:34:05.152042189Z`
- Virtual Size: ~ 71.35 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["haxe"]`
- Environment:
  - `PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `NEKO_VERSION=2.3.0`
  - `HAXE_VERSION=3.4.7`
  - `HAXE_STD_PATH=/usr/local/share/haxe/std`

## `apk` (`.apk`-based packages)

### `apk` package: `alpine-baselayout`

```console
alpine-baselayout-3.2.0-r3 description:
Alpine base dir structure and init scripts

alpine-baselayout-3.2.0-r3 webpage:
https://git.alpinelinux.org/cgit/aports/tree/main/alpine-baselayout

alpine-baselayout-3.2.0-r3 installed size:
404 KiB

alpine-baselayout-3.2.0-r3 license:
GPL-2.0-only

```

### `apk` package: `alpine-keys`

```console
alpine-keys-2.1-r2 description:
Public keys for Alpine Linux packages

alpine-keys-2.1-r2 webpage:
https://alpinelinux.org

alpine-keys-2.1-r2 installed size:
96 KiB

alpine-keys-2.1-r2 license:
MIT

```

### `apk` package: `apk-tools`

```console
apk-tools-2.10.8-r0 description:
Alpine Package Keeper - package manager for alpine

apk-tools-2.10.8-r0 webpage:
https://gitlab.alpinelinux.org/alpine/apk-tools

apk-tools-2.10.8-r0 installed size:
260 KiB

apk-tools-2.10.8-r0 license:
GPL2

```

### `apk` package: `apr`

```console
apr-1.7.0-r0 description:
The Apache Portable Runtime

apr-1.7.0-r0 webpage:
http://apr.apache.org/

apr-1.7.0-r0 installed size:
220 KiB

apr-1.7.0-r0 license:
Apache-2.0

```

### `apk` package: `atk`

```console
atk-2.34.1-r0 description:
A library providing a set of interfaces for accessibility

atk-2.34.1-r0 webpage:
https://www.gtk.org/

atk-2.34.1-r0 installed size:
248 KiB

atk-2.34.1-r0 license:
LGPL-2.0-or-later

```

### `apk` package: `avahi-libs`

```console
avahi-libs-0.7-r5 description:
Libraries for avahi run-time use

avahi-libs-0.7-r5 webpage:
https://www.avahi.org/

avahi-libs-0.7-r5 installed size:
128 KiB

avahi-libs-0.7-r5 license:
LGPL-2.0-or-later

```

### `apk` package: `busybox`

```console
busybox-1.31.1-r10 description:
Size optimized toolbox of many common UNIX utilities

busybox-1.31.1-r10 webpage:
https://busybox.net/

busybox-1.31.1-r10 installed size:
940 KiB

busybox-1.31.1-r10 license:
GPL-2.0-only

```

### `apk` package: `ca-certificates`

```console
ca-certificates-20191127-r2 description:
Common CA certificates PEM files from Mozilla

ca-certificates-20191127-r2 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-20191127-r2 installed size:
720 KiB

ca-certificates-20191127-r2 license:
MPL-2.0 GPL-2.0-or-later

```

### `apk` package: `ca-certificates-cacert`

```console
ca-certificates-cacert-20191127-r2 description:
Mozilla bundled certificates

ca-certificates-cacert-20191127-r2 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-cacert-20191127-r2 installed size:
240 KiB

ca-certificates-cacert-20191127-r2 license:
MPL-2.0 GPL-2.0-or-later

```

### `apk` package: `cairo`

```console
cairo-1.16.0-r3 description:
A vector graphics library

cairo-1.16.0-r3 webpage:
https://cairographics.org/

cairo-1.16.0-r3 installed size:
1124 KiB

cairo-1.16.0-r3 license:
LGPL-2.0-or-later MPL-1.1

```

### `apk` package: `cups-libs`

```console
cups-libs-2.2.12-r2 description:
CUPS libraries

cups-libs-2.2.12-r2 webpage:
https://www.cups.org/

cups-libs-2.2.12-r2 installed size:
556 KiB

cups-libs-2.2.12-r2 license:
GPL-2.0-only

```

### `apk` package: `dbus-libs`

```console
dbus-libs-1.12.16-r3 description:
D-BUS access libraries

dbus-libs-1.12.16-r3 webpage:
https://www.freedesktop.org/Software/dbus

dbus-libs-1.12.16-r3 installed size:
304 KiB

dbus-libs-1.12.16-r3 license:
AFL-2.1 OR GPL-2.0-or-later

```

### `apk` package: `expat`

```console
expat-2.2.9-r1 description:
An XML Parser library written in C

expat-2.2.9-r1 webpage:
http://www.libexpat.org/

expat-2.2.9-r1 installed size:
184 KiB

expat-2.2.9-r1 license:
MIT

```

### `apk` package: `fontconfig`

```console
fontconfig-2.13.1-r2 description:
Library for configuring and customizing font access

fontconfig-2.13.1-r2 webpage:
https://www.freedesktop.org/wiki/Software/fontconfig

fontconfig-2.13.1-r2 installed size:
632 KiB

fontconfig-2.13.1-r2 license:
MIT

```

### `apk` package: `freetype`

```console
freetype-2.10.1-r1 description:
TrueType font rendering library

freetype-2.10.1-r1 webpage:
https://www.freetype.org/

freetype-2.10.1-r1 installed size:
720 KiB

freetype-2.10.1-r1 license:
FTL GPL-2.0-or-later

```

### `apk` package: `fribidi`

```console
fribidi-1.0.8-r0 description:
Free Implementation of the Unicode Bidirectional Algorithm

fribidi-1.0.8-r0 webpage:
https://github.com/fribidi/fribidi

fribidi-1.0.8-r0 installed size:
156 KiB

fribidi-1.0.8-r0 license:
LGPL-2.0-or-later

```

### `apk` package: `gc`

```console
gc-8.0.4-r0 description:
A garbage collector for C and C++

gc-8.0.4-r0 webpage:
http://hboehm.info/gc/

gc-8.0.4-r0 installed size:
228 KiB

gc-8.0.4-r0 license:
GPL

```

### `apk` package: `gdk-pixbuf`

```console
gdk-pixbuf-2.40.0-r2 description:
GTK+ image loading library

gdk-pixbuf-2.40.0-r2 webpage:
https://wiki.gnome.org/Projects/GdkPixbuf

gdk-pixbuf-2.40.0-r2 installed size:
620 KiB

gdk-pixbuf-2.40.0-r2 license:
LGPL-2.0-or-later

```

### `apk` package: `glib`

```console
glib-2.62.6-r0 description:
Common C routines used by Gtk+ and other libs

glib-2.62.6-r0 webpage:
https://developer.gnome.org/glib/

glib-2.62.6-r0 installed size:
3196 KiB

glib-2.62.6-r0 license:
LGPL-2.1-or-later

```

### `apk` package: `gmp`

```console
gmp-6.1.2-r1 description:
A free library for arbitrary precision arithmetic

gmp-6.1.2-r1 webpage:
https://gmplib.org/

gmp-6.1.2-r1 installed size:
412 KiB

gmp-6.1.2-r1 license:
LGPL-3.0

```

### `apk` package: `gnutls`

```console
gnutls-3.6.15-r1 description:
A TLS protocol implementation

gnutls-3.6.15-r1 webpage:
https://www.gnutls.org/

gnutls-3.6.15-r1 installed size:
1728 KiB

gnutls-3.6.15-r1 license:
GPL-3.0-or-later

```

### `apk` package: `graphite2`

```console
graphite2-1.3.13-r1 description:
reimplementation of the SIL Graphite text processing engine

graphite2-1.3.13-r1 webpage:
http://graphite.sil.org/

graphite2-1.3.13-r1 installed size:
168 KiB

graphite2-1.3.13-r1 license:
LGPL-2.1-or-later OR MPL-2.0 OR GPL-2.0-or-later

```

### `apk` package: `gtk+2.0`

```console
gtk+2.0-2.24.32-r1 description:
The GTK+ Toolkit (v2)

gtk+2.0-2.24.32-r1 webpage:
https://www.gtk.org/

gtk+2.0-2.24.32-r1 installed size:
6260 KiB

gtk+2.0-2.24.32-r1 license:
LGPL-2.0+

```

### `apk` package: `gtk-update-icon-cache`

```console
gtk-update-icon-cache-2.24.32-r1 description:
The GTK+ Toolkit (v2)

gtk-update-icon-cache-2.24.32-r1 webpage:
https://www.gtk.org/

gtk-update-icon-cache-2.24.32-r1 installed size:
44 KiB

gtk-update-icon-cache-2.24.32-r1 license:
LGPL-2.0+

```

### `apk` package: `harfbuzz`

```console
harfbuzz-2.6.4-r0 description:
Text shaping library

harfbuzz-2.6.4-r0 webpage:
https://freedesktop.org/wiki/Software/HarfBuzz

harfbuzz-2.6.4-r0 installed size:
1156 KiB

harfbuzz-2.6.4-r0 license:
MIT

```

### `apk` package: `hicolor-icon-theme`

```console
hicolor-icon-theme-0.17-r1 description:
Freedesktop.org Hicolor icon theme

hicolor-icon-theme-0.17-r1 webpage:
https://www.freedesktop.org/wiki/Software/icon-theme/

hicolor-icon-theme-0.17-r1 installed size:
1540 KiB

hicolor-icon-theme-0.17-r1 license:
GPL-2.0-only

```

### `apk` package: `libblkid`

```console
libblkid-2.34-r1 description:
Block device identification library from util-linux

libblkid-2.34-r1 webpage:
https://git.kernel.org/cgit/utils/util-linux/util-linux.git

libblkid-2.34-r1 installed size:
304 KiB

libblkid-2.34-r1 license:
GPL-2.0 GPL-2.0-or-later LGPL-2.0-or-later BSD Public-Domain

```

### `apk` package: `libbsd`

```console
libbsd-0.10.0-r0 description:
commonly-used BSD functions not implemented by all libcs

libbsd-0.10.0-r0 webpage:
https://libbsd.freedesktop.org/

libbsd-0.10.0-r0 installed size:
92 KiB

libbsd-0.10.0-r0 license:
BSD

```

### `apk` package: `libbz2`

```console
libbz2-1.0.8-r1 description:
Shared library for bz2

libbz2-1.0.8-r1 webpage:
http://sources.redhat.com/bzip2

libbz2-1.0.8-r1 installed size:
72 KiB

libbz2-1.0.8-r1 license:
bzip2-1.0.6

```

### `apk` package: `libc-utils`

```console
libc-utils-0.7.2-r0 description:
Meta package to pull in correct libc

libc-utils-0.7.2-r0 webpage:
http://alpinelinux.org

libc-utils-0.7.2-r0 installed size:
4096 B

libc-utils-0.7.2-r0 license:
BSD

```

### `apk` package: `libcrypto1.1`

```console
libcrypto1.1-1.1.1l-r0 description:
Crypto library from openssl

libcrypto1.1-1.1.1l-r0 webpage:
https://www.openssl.org

libcrypto1.1-1.1.1l-r0 installed size:
2700 KiB

libcrypto1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libffi`

```console
libffi-3.2.1-r6 description:
A portable, high level programming interface to various calling conventions.

libffi-3.2.1-r6 webpage:
http://sourceware.org/libffi

libffi-3.2.1-r6 installed size:
48 KiB

libffi-3.2.1-r6 license:
MIT

```

### `apk` package: `libgcc`

```console
libgcc-9.3.0-r0 description:
GNU C compiler runtime libraries

libgcc-9.3.0-r0 webpage:
http://gcc.gnu.org

libgcc-9.3.0-r0 installed size:
88 KiB

libgcc-9.3.0-r0 license:
GPL LGPL

```

### `apk` package: `libintl`

```console
libintl-0.20.1-r2 description:
GNU gettext runtime library

libintl-0.20.1-r2 webpage:
https://www.gnu.org/software/gettext/gettext.html

libintl-0.20.1-r2 installed size:
56 KiB

libintl-0.20.1-r2 license:
LGPL-2.1-or-later

```

### `apk` package: `libjpeg-turbo`

```console
libjpeg-turbo-2.0.4-r1 description:
Accelerated baseline JPEG compression and decompression library

libjpeg-turbo-2.0.4-r1 webpage:
https://libjpeg-turbo.org/

libjpeg-turbo-2.0.4-r1 installed size:
1324 KiB

libjpeg-turbo-2.0.4-r1 license:
BSD-3-Clause IJG Zlib

```

### `apk` package: `libmount`

```console
libmount-2.34-r1 description:
Block device identification library from util-linux

libmount-2.34-r1 webpage:
https://git.kernel.org/cgit/utils/util-linux/util-linux.git

libmount-2.34-r1 installed size:
336 KiB

libmount-2.34-r1 license:
GPL-2.0 GPL-2.0-or-later LGPL-2.0-or-later BSD Public-Domain

```

### `apk` package: `libpng`

```console
libpng-1.6.37-r1 description:
Portable Network Graphics library

libpng-1.6.37-r1 webpage:
http://www.libpng.org

libpng-1.6.37-r1 installed size:
200 KiB

libpng-1.6.37-r1 license:
Libpng

```

### `apk` package: `libssl1.1`

```console
libssl1.1-1.1.1l-r0 description:
SSL shared libraries

libssl1.1-1.1.1l-r0 webpage:
https://www.openssl.org

libssl1.1-1.1.1l-r0 installed size:
528 KiB

libssl1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libtasn1`

```console
libtasn1-4.15.0-r0 description:
The ASN.1 library used in GNUTLS

libtasn1-4.15.0-r0 webpage:
https://www.gnu.org/software/gnutls/

libtasn1-4.15.0-r0 installed size:
148 KiB

libtasn1-4.15.0-r0 license:
GPL-3.0 LGPL

```

### `apk` package: `libtls-standalone`

```console
libtls-standalone-2.9.1-r0 description:
libtls extricated from libressl sources

libtls-standalone-2.9.1-r0 webpage:
https://www.libressl.org/

libtls-standalone-2.9.1-r0 installed size:
108 KiB

libtls-standalone-2.9.1-r0 license:
ISC

```

### `apk` package: `libunistring`

```console
libunistring-0.9.10-r0 description:
Library for manipulating Unicode strings and C strings

libunistring-0.9.10-r0 webpage:
https://www.gnu.org/software/libunistring/

libunistring-0.9.10-r0 installed size:
1504 KiB

libunistring-0.9.10-r0 license:
GPL-2.0+ OR LGPL-3.0+

```

### `apk` package: `libuuid`

```console
libuuid-2.34-r1 description:
DCE compatible Universally Unique Identifier library

libuuid-2.34-r1 webpage:
https://git.kernel.org/cgit/utils/util-linux/util-linux.git

libuuid-2.34-r1 installed size:
40 KiB

libuuid-2.34-r1 license:
GPL-2.0 GPL-2.0-or-later LGPL-2.0-or-later BSD Public-Domain

```

### `apk` package: `libx11`

```console
libx11-1.6.12-r1 description:
X11 client-side library

libx11-1.6.12-r1 webpage:
http://xorg.freedesktop.org/

libx11-1.6.12-r1 installed size:
3312 KiB

libx11-1.6.12-r1 license:
custom:XFREE86

```

### `apk` package: `libxau`

```console
libxau-1.0.9-r0 description:
X11 authorisation library

libxau-1.0.9-r0 webpage:
http://xorg.freedesktop.org/

libxau-1.0.9-r0 installed size:
28 KiB

libxau-1.0.9-r0 license:
MIT

```

### `apk` package: `libxcb`

```console
libxcb-1.13.1-r0 description:
X11 client-side library

libxcb-1.13.1-r0 webpage:
https://xcb.freedesktop.org

libxcb-1.13.1-r0 installed size:
996 KiB

libxcb-1.13.1-r0 license:
MIT

```

### `apk` package: `libxcomposite`

```console
libxcomposite-0.4.5-r0 description:
X11 Composite extension library

libxcomposite-0.4.5-r0 webpage:
http://xorg.freedesktop.org/

libxcomposite-0.4.5-r0 installed size:
28 KiB

libxcomposite-0.4.5-r0 license:
custom

```

### `apk` package: `libxcursor`

```console
libxcursor-1.2.0-r0 description:
X cursor management library

libxcursor-1.2.0-r0 webpage:
http://xorg.freedesktop.org/

libxcursor-1.2.0-r0 installed size:
56 KiB

libxcursor-1.2.0-r0 license:
MIT

```

### `apk` package: `libxdamage`

```console
libxdamage-1.1.5-r0 description:
X11 damaged region extension library

libxdamage-1.1.5-r0 webpage:
http://xorg.freedesktop.org/

libxdamage-1.1.5-r0 installed size:
28 KiB

libxdamage-1.1.5-r0 license:
MIT

```

### `apk` package: `libxdmcp`

```console
libxdmcp-1.1.3-r0 description:
X11 Display Manager Control Protocol library

libxdmcp-1.1.3-r0 webpage:
http://xorg.freedesktop.org/

libxdmcp-1.1.3-r0 installed size:
40 KiB

libxdmcp-1.1.3-r0 license:
MIT

```

### `apk` package: `libxext`

```console
libxext-1.3.4-r0 description:
X11 miscellaneous extensions library

libxext-1.3.4-r0 webpage:
http://xorg.freedesktop.org/

libxext-1.3.4-r0 installed size:
84 KiB

libxext-1.3.4-r0 license:
MIT

```

### `apk` package: `libxfixes`

```console
libxfixes-5.0.3-r2 description:
X11 miscellaneous 'fixes' extension library

libxfixes-5.0.3-r2 webpage:
http://xorg.freedesktop.org/

libxfixes-5.0.3-r2 installed size:
40 KiB

libxfixes-5.0.3-r2 license:
custom

```

### `apk` package: `libxft`

```console
libxft-2.3.3-r0 description:
FreeType-based font drawing library for X

libxft-2.3.3-r0 webpage:
http://xorg.freedesktop.org/

libxft-2.3.3-r0 installed size:
92 KiB

libxft-2.3.3-r0 license:
MIT

```

### `apk` package: `libxi`

```console
libxi-1.7.10-r0 description:
X11 Input extension library

libxi-1.7.10-r0 webpage:
https://www.x.org

libxi-1.7.10-r0 installed size:
76 KiB

libxi-1.7.10-r0 license:
MIT AND X11

```

### `apk` package: `libxml2`

```console
libxml2-2.9.12-r0 description:
XML parsing library, version 2

libxml2-2.9.12-r0 webpage:
http://www.xmlsoft.org/

libxml2-2.9.12-r0 installed size:
1196 KiB

libxml2-2.9.12-r0 license:
MIT

```

### `apk` package: `libxrandr`

```console
libxrandr-1.5.2-r0 description:
X11 RandR extension library

libxrandr-1.5.2-r0 webpage:
http://xorg.freedesktop.org/

libxrandr-1.5.2-r0 installed size:
56 KiB

libxrandr-1.5.2-r0 license:
MIT

```

### `apk` package: `libxrender`

```console
libxrender-0.9.10-r3 description:
X Rendering Extension client library

libxrender-0.9.10-r3 webpage:
http://xorg.freedesktop.org/

libxrender-0.9.10-r3 installed size:
56 KiB

libxrender-0.9.10-r3 license:
custom

```

### `apk` package: `mariadb-connector-c`

```console
mariadb-connector-c-3.1.6-r1 description:
The MariaDB Native Client library (C driver)

mariadb-connector-c-3.1.6-r1 webpage:
https://mariadb.org/

mariadb-connector-c-3.1.6-r1 installed size:
524 KiB

mariadb-connector-c-3.1.6-r1 license:
LGPL-2.1-or-later

```

### `apk` package: `mbedtls`

```console
mbedtls-2.16.9-r0 description:
Light-weight cryptographic and SSL/TLS library

mbedtls-2.16.9-r0 webpage:
https://tls.mbed.org

mbedtls-2.16.9-r0 installed size:
644 KiB

mbedtls-2.16.9-r0 license:
Apache-2.0

```

### `apk` package: `musl`

```console
musl-1.1.24-r3 description:
the musl c library (libc) implementation

musl-1.1.24-r3 webpage:
https://musl.libc.org/

musl-1.1.24-r3 installed size:
600 KiB

musl-1.1.24-r3 license:
MIT

```

### `apk` package: `musl-utils`

```console
musl-utils-1.1.24-r3 description:
the musl c library (libc) implementation

musl-utils-1.1.24-r3 webpage:
https://musl.libc.org/

musl-utils-1.1.24-r3 installed size:
148 KiB

musl-utils-1.1.24-r3 license:
MIT BSD GPL2+

```

### `apk` package: `nettle`

```console
nettle-3.5.1-r0 description:
A low-level cryptographic library

nettle-3.5.1-r0 webpage:
https://www.lysator.liu.se/~nisse/nettle/

nettle-3.5.1-r0 installed size:
452 KiB

nettle-3.5.1-r0 license:
LGPL-2.0-or-later

```

### `apk` package: `p11-kit`

```console
p11-kit-0.23.18.1-r1 description:
Library for loading and sharing PKCS#11 modules

p11-kit-0.23.18.1-r1 webpage:
https://p11-glue.freedesktop.org/

p11-kit-0.23.18.1-r1 installed size:
1216 KiB

p11-kit-0.23.18.1-r1 license:
BSD-3-Clause

```

### `apk` package: `pango`

```console
pango-1.44.7-r0 description:
A library for layout and rendering of text

pango-1.44.7-r0 webpage:
https://www.pango.org/

pango-1.44.7-r0 installed size:
572 KiB

pango-1.44.7-r0 license:
LGPL-2.1-or-later

```

### `apk` package: `pcre`

```console
pcre-8.43-r1 description:
Perl-compatible regular expression library

pcre-8.43-r1 webpage:
http://pcre.sourceforge.net

pcre-8.43-r1 installed size:
392 KiB

pcre-8.43-r1 license:
BSD-3-Clause

```

### `apk` package: `pixman`

```console
pixman-0.38.4-r0 description:
Low-level pixel manipulation library

pixman-0.38.4-r0 webpage:
http://xorg.freedesktop.org

pixman-0.38.4-r0 installed size:
580 KiB

pixman-0.38.4-r0 license:
MIT

```

### `apk` package: `pkgconf`

```console
pkgconf-1.6.3-r0 description:
development framework configuration tools

pkgconf-1.6.3-r0 webpage:
https://git.dereferenced.org/pkgconf/pkgconf

pkgconf-1.6.3-r0 installed size:
140 KiB

pkgconf-1.6.3-r0 license:
ISC

```

### `apk` package: `scanelf`

```console
scanelf-1.2.4-r0 description:
Scan ELF binaries for stuff

scanelf-1.2.4-r0 webpage:
https://wiki.gentoo.org/wiki/Hardened/PaX_Utilities

scanelf-1.2.4-r0 installed size:
92 KiB

scanelf-1.2.4-r0 license:
GPL-2.0-only

```

### `apk` package: `shared-mime-info`

```console
shared-mime-info-1.15-r0 description:
Freedesktop.org Shared MIME Info

shared-mime-info-1.15-r0 webpage:
http://freedesktop.org/Software/shared-mime-info

shared-mime-info-1.15-r0 installed size:
2340 KiB

shared-mime-info-1.15-r0 license:
GPL-2.0-or-later

```

### `apk` package: `sqlite-libs`

```console
sqlite-libs-3.30.1-r2 description:
Sqlite3 library

sqlite-libs-3.30.1-r2 webpage:
https://www.sqlite.org/

sqlite-libs-3.30.1-r2 installed size:
916 KiB

sqlite-libs-3.30.1-r2 license:
Public-Domain

```

### `apk` package: `ssl_client`

```console
ssl_client-1.31.1-r10 description:
EXternal ssl_client for busybox wget

ssl_client-1.31.1-r10 webpage:
https://busybox.net/

ssl_client-1.31.1-r10 installed size:
28 KiB

ssl_client-1.31.1-r10 license:
GPL-2.0-only

```

### `apk` package: `tiff`

```console
tiff-4.1.0-r0 description:
Provides support for the Tag Image File Format or TIFF

tiff-4.1.0-r0 webpage:
http://www.libtiff.org

tiff-4.1.0-r0 installed size:
440 KiB

tiff-4.1.0-r0 license:
libtiff

```

### `apk` package: `xz-libs`

```console
xz-libs-5.2.4-r0 description:
Library and CLI tools for XZ and LZMA compressed files (libraries)

xz-libs-5.2.4-r0 webpage:
https://tukaani.org/xz

xz-libs-5.2.4-r0 installed size:
148 KiB

xz-libs-5.2.4-r0 license:
GPL-2.0-or-later Public-Domain

```

### `apk` package: `zlib`

```console
zlib-1.2.11-r3 description:
A compression/decompression Library

zlib-1.2.11-r3 webpage:
https://zlib.net/

zlib-1.2.11-r3 installed size:
108 KiB

zlib-1.2.11-r3 license:
Zlib

```
