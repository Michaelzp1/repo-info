# `hello-world:linux`

## Docker Metadata

- Image ID: `sha256:feb5d9fea6a5e9606aa995e879d862b825965ba48de054caab5ef356dc6b3412`
- Created: `2021-09-23T23:47:57.442225064Z`
- Virtual Size: ~ 13.26 Kb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["/hello"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
