## `hello-world:latest`

```console
$ docker pull hello-world@sha256:37a0b92b08d4919615c3ee023f7ddb068d12b8387475d64c622ac30f45c29c51
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 11
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; riscv64
	-	linux; s390x
	-	windows version 10.0.20348.288; amd64
	-	windows version 10.0.17763.2237; amd64

### `hello-world:latest` - linux; amd64

```console
$ docker pull hello-world@sha256:f54a58bc1aac5ea1a25d796ae155dc228b3f0e11d046ae276b39c4bf2f13d8c4
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.5 KB (2479 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:feb5d9fea6a5e9606aa995e879d862b825965ba48de054caab5ef356dc6b3412`
-	Default Command: `["\/hello"]`

```dockerfile
# Thu, 23 Sep 2021 23:47:57 GMT
COPY file:50563a97010fd7ce1ceebd1fa4f4891ac3decdf428333fb2683696f4358af6c2 in / 
# Thu, 23 Sep 2021 23:47:57 GMT
CMD ["/hello"]
```

-	Layers:
	-	`sha256:2db29710123e3e53a794f2694094b9b4338aa9ee5c40b930cb8063a1be392c54`  
		Last Modified: Thu, 23 Sep 2021 23:48:07 GMT  
		Size: 2.5 KB (2479 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `hello-world:latest` - linux; arm variant v5

```console
$ docker pull hello-world@sha256:7b8b7289d0536a08eabdf71c20246e23f7116641db7e1d278592236ea4dcb30c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **3.7 KB (3684 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:c0218de6585df06a66d67b25237bdda42137c727c367373a32639710c7a9fa94`
-	Default Command: `["\/hello"]`

```dockerfile
# Thu, 23 Sep 2021 22:09:55 GMT
COPY file:2b28186c8eb8bb7ee57e0b02055b51cb4023eac9161feab0338311b5a9ff6665 in / 
# Thu, 23 Sep 2021 22:09:56 GMT
CMD ["/hello"]
```

-	Layers:
	-	`sha256:b921b04d0447ddcd82a9220d887cd146f6ef39e20a938ee5e19a90fc3323e030`  
		Last Modified: Thu, 23 Sep 2021 22:10:16 GMT  
		Size: 3.7 KB (3684 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `hello-world:latest` - linux; arm variant v7

```console
$ docker pull hello-world@sha256:f130bd2d67e6e9280ac6d0a6c83857bfaf70234e8ef4236876eccfbd30973b1c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **3.0 KB (2993 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1ec996c686eb87d8f091080ec29dd1862b39b5822ddfd8f9a1e2c9288fad89fe`
-	Default Command: `["\/hello"]`

```dockerfile
# Thu, 30 Sep 2021 19:27:27 GMT
COPY file:064e3842b2c067a42e34f354951e44e733d8077cfb791d6847f4e653677d2f7e in / 
# Thu, 30 Sep 2021 19:27:27 GMT
CMD ["/hello"]
```

-	Layers:
	-	`sha256:9b157615502ddff86482f7fe2fa7a074db74a62fce12b4e8507827ac8f08d0ce`  
		Last Modified: Thu, 30 Sep 2021 19:27:48 GMT  
		Size: 3.0 KB (2993 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `hello-world:latest` - linux; arm64 variant v8

```console
$ docker pull hello-world@sha256:01433e86a06b752f228e3c17394169a5e21a0995f153268a9b36a16d4f2b2184
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **3.2 KB (3208 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:18e5af7904737ba5ef7fbbd7d59de5ebe6c4437907bd7fc436bf9b3ef3149ea9`
-	Default Command: `["\/hello"]`

```dockerfile
# Fri, 24 Sep 2021 01:21:00 GMT
COPY file:a79dd5bda1e77203401956a93401d3aef45221fc750295a4291896f3386f4f54 in / 
# Fri, 24 Sep 2021 01:21:00 GMT
CMD ["/hello"]
```

-	Layers:
	-	`sha256:93288797bd35d114f2d788e5abf4fae518a5bd299647daf4ede47acc029d66c5`  
		Last Modified: Fri, 24 Sep 2021 01:21:15 GMT  
		Size: 3.2 KB (3208 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `hello-world:latest` - linux; 386

```console
$ docker pull hello-world@sha256:251bb7a536c7cce3437758971aab3a31c6da52fb43ff0654cff5b167c4486409
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.7 KB (2735 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:7c2f8335a1a3faa411d45fc31419e2fbb519dd9c41ece8ebc350e6fe7af6b017`
-	Default Command: `["\/hello"]`

```dockerfile
# Thu, 23 Sep 2021 22:55:43 GMT
COPY file:1babbe3b37eb1154a832c225a92090691ababcfb0ed16f027eb3fe3c2e4ed27b in / 
# Thu, 23 Sep 2021 22:55:43 GMT
CMD ["/hello"]
```

-	Layers:
	-	`sha256:cdfa10f2a818a2f4fcb49da487dc42795690a7fb04524bdc3decfa9fe16b2c2a`  
		Last Modified: Thu, 23 Sep 2021 22:55:56 GMT  
		Size: 2.7 KB (2735 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `hello-world:latest` - linux; mips64le

```console
$ docker pull hello-world@sha256:c2f204d26b4ea353651385001bb6bc371d8c4edcd9daf61d00ad365d927e00c0
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **4.1 KB (4091 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4d278d00d38a32c6096213745a79ff5c1f6ec12b575ed0f5e5a780a3478c1b21`
-	Default Command: `["\/hello"]`

```dockerfile
# Thu, 23 Sep 2021 22:48:30 GMT
COPY file:9acd5b5706455a6fe3d36fc638bf68bead9cf9f0ac938fc68ec9e669f0d2f8c7 in / 
# Thu, 23 Sep 2021 22:48:30 GMT
CMD ["/hello"]
```

-	Layers:
	-	`sha256:3e4037a0f36c739a8a3f2184b76b57f6a1d56a36fe3c3f966657ad1db828e1aa`  
		Last Modified: Thu, 23 Sep 2021 22:48:41 GMT  
		Size: 4.1 KB (4091 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `hello-world:latest` - linux; ppc64le

```console
$ docker pull hello-world@sha256:b836bb24a270b9cc935962d8228517fde0f16990e88893d935efcb1b14c0017a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **3.9 KB (3929 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:61fff98d5ca765a4351964c8f4b5fb1a0d2c48458026f5452a389eb52d146fe8`
-	Default Command: `["\/hello"]`

```dockerfile
# Fri, 24 Sep 2021 06:38:41 GMT
COPY file:3f80414426e41becdbb214f7a6d2e211f255be120a2409e43668203eafd069af in / 
# Fri, 24 Sep 2021 06:38:43 GMT
CMD ["/hello"]
```

-	Layers:
	-	`sha256:33450689bfb495ed64ead935c9933f1d6b3e42fe369b8de9680cf4ff9d89ce5c`  
		Last Modified: Fri, 24 Sep 2021 06:38:57 GMT  
		Size: 3.9 KB (3929 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `hello-world:latest` - linux; riscv64

```console
$ docker pull hello-world@sha256:98c9722322be649df94780d3fbe594fce7996234b259f27eac9428b84050c849
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **3.0 KB (3000 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b3593dab05491cdf5ee88c29bee36603c0df0bc34798eed5067f6e1335a9d391`
-	Default Command: `["\/hello"]`

```dockerfile
# Thu, 23 Sep 2021 21:24:49 GMT
COPY file:84444ad88bd7fb1ac993a256408f10ce0befd4338385371c4b1943d8b219e0b4 in / 
# Thu, 23 Sep 2021 21:24:50 GMT
CMD ["/hello"]
```

-	Layers:
	-	`sha256:3caa6dc69d0b73f21d29bfa75356395f2695a7abad34f010656740e90ddce399`  
		Last Modified: Thu, 23 Sep 2021 21:27:20 GMT  
		Size: 3.0 KB (3000 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `hello-world:latest` - linux; s390x

```console
$ docker pull hello-world@sha256:c7b6944911848ce39b44ed660d95fb54d69bbd531de724c7ce6fc9f743c0b861
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **3.3 KB (3276 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:df5477cea5582b0ae6a31de2d1c9bbacb506091f42a3b0fe77a209006f409fd8`
-	Default Command: `["\/hello"]`

```dockerfile
# Thu, 23 Sep 2021 22:39:53 GMT
COPY file:600d8ab1c71de7b59c96ed558afbed478d81ebf3ef7517c4c4f3757cc136475f in / 
# Thu, 23 Sep 2021 22:39:54 GMT
CMD ["/hello"]
```

-	Layers:
	-	`sha256:abc70fcc95b2f52b325d69cc5c259dd9babb40a9df152e88b286fada1d3248bd`  
		Last Modified: Thu, 23 Sep 2021 22:40:07 GMT  
		Size: 3.3 KB (3276 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `hello-world:latest` - windows version 10.0.20348.288; amd64

```console
$ docker pull hello-world@sha256:31801872aacfc6245ba5277e07dc2c9a482a473c87d625f25c3e6d5de930b35d
```

-	Docker Version: 20.10.8
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **116.9 MB (116942543 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e564d7360cfa5451bc1d616d8871f6a17f1b2f977a68462a7259bce110f8a754`
-	Default Command: `["cmd","\/C","type C:\\hello.txt"]`

```dockerfile
# Thu, 07 Oct 2021 11:15:04 GMT
RUN Apply image ltsc2022-amd64
# Wed, 13 Oct 2021 12:20:37 GMT
RUN cmd /S /C #(nop) COPY file:55c009fa8b983e38026b41064e5367bc779dae76c0d404a11886c3cb19ec4509 in C: 
# Wed, 13 Oct 2021 12:20:37 GMT
RUN cmd /S /C #(nop)  CMD ["cmd" "/C" "type C:\\hello.txt"]
```

-	Layers:
	-	`sha256:91284e7e8fd4bd7ebcfa98544a3e4f59639f38281225c81c34b6fe22e0dba4e5`  
		Size: 116.9 MB (116939483 bytes)  
		MIME: application/vnd.docker.image.rootfs.foreign.diff.tar.gzip
	-	`sha256:84f480fc2b679166f7d1340f9ee5ffc1ce7f860cea8e850365fef57631f1908f`  
		Last Modified: Wed, 13 Oct 2021 12:21:01 GMT  
		Size: 1.9 KB (1889 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:e9f41bfcf238a5a974261c1f9ce6c039568585a03e2d8966083e273d7448d275`  
		Last Modified: Wed, 13 Oct 2021 12:21:02 GMT  
		Size: 1.2 KB (1171 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `hello-world:latest` - windows version 10.0.17763.2237; amd64

```console
$ docker pull hello-world@sha256:563c31a6b24347d3f367df5dc33890ab1aec20e9470e5d998f3b6a8fc6eb5763
```

-	Docker Version: 20.10.8
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **102.7 MB (102664370 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b7acc395cb5190d3f6eca8330eadfa3d2666becbb9960a2dfa7bbbef1f72d21a`
-	Default Command: `["cmd","\/C","type C:\\hello.txt"]`

```dockerfile
# Thu, 07 Oct 2021 08:01:14 GMT
RUN Apply image 1809-amd64
# Wed, 13 Oct 2021 12:20:43 GMT
RUN cmd /S /C #(nop) COPY file:dbb4e437ca342a79d5980fcb71c065abfe00353f696b1b54084e7c09d32ec085 in C: 
# Wed, 13 Oct 2021 12:20:44 GMT
RUN cmd /S /C #(nop)  CMD ["cmd" "/C" "type C:\\hello.txt"]
```

-	Layers:
	-	`sha256:934e212983f208dc2bebc5de38259a6a62f1761868aacfee2cb3585a13b1e24b`  
		Size: 102.7 MB (102661372 bytes)  
		MIME: application/vnd.docker.image.rootfs.foreign.diff.tar.gzip
	-	`sha256:105a5ce4ce14779701660c1c93917420b43117535c32a71b22690f4c27a53204`  
		Last Modified: Wed, 13 Oct 2021 12:21:08 GMT  
		Size: 1.9 KB (1865 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:6512b9624c09c870e109a0bac0e917a666ce1ce895f30b5c0c21c8d7e8e11d6e`  
		Last Modified: Wed, 13 Oct 2021 12:21:08 GMT  
		Size: 1.1 KB (1133 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
