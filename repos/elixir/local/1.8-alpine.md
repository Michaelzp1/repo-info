# `elixir:1.8.2-alpine`

## Docker Metadata

- Image ID: `sha256:a4a39b94b037b42ebcbbd90dc2d27e189d3db631cb1bd6bff69f50c58b0e7e2a`
- Created: `2021-09-01T06:51:43.827129257Z`
- Virtual Size: ~ 81.92 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["iex"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `OTP_VERSION=21.3.8.24`
  - `REBAR3_VERSION=3.15.2`
  - `ELIXIR_VERSION=v1.8.2`
  - `LANG=C.UTF-8`
- Labels:
  - `org.opencontainers.image.version=21.3.8.24`

## `apk` (`.apk`-based packages)

### `apk` package: `alpine-baselayout`

```console
alpine-baselayout-3.2.0-r8 description:
Alpine base dir structure and init scripts

alpine-baselayout-3.2.0-r8 webpage:
https://git.alpinelinux.org/cgit/aports/tree/main/alpine-baselayout

alpine-baselayout-3.2.0-r8 installed size:
400 KiB

alpine-baselayout-3.2.0-r8 license:
GPL-2.0-only

```

### `apk` package: `alpine-keys`

```console
alpine-keys-2.2-r0 description:
Public keys for Alpine Linux packages

alpine-keys-2.2-r0 webpage:
https://alpinelinux.org

alpine-keys-2.2-r0 installed size:
104 KiB

alpine-keys-2.2-r0 license:
MIT

```

### `apk` package: `apk-tools`

```console
apk-tools-2.12.7-r0 description:
Alpine Package Keeper - package manager for alpine

apk-tools-2.12.7-r0 webpage:
https://gitlab.alpinelinux.org/alpine/apk-tools

apk-tools-2.12.7-r0 installed size:
304 KiB

apk-tools-2.12.7-r0 license:
GPL-2.0-only

```

### `apk` package: `busybox`

```console
busybox-1.32.1-r6 description:
Size optimized toolbox of many common UNIX utilities

busybox-1.32.1-r6 webpage:
https://busybox.net/

busybox-1.32.1-r6 installed size:
924 KiB

busybox-1.32.1-r6 license:
GPL-2.0-only

```

### `apk` package: `ca-certificates`

```console
ca-certificates-20191127-r5 description:
Common CA certificates PEM files from Mozilla

ca-certificates-20191127-r5 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-20191127-r5 installed size:
672 KiB

ca-certificates-20191127-r5 license:
MPL-2.0 AND MIT

```

### `apk` package: `ca-certificates-bundle`

```console
ca-certificates-bundle-20191127-r5 description:
Pre generated bundle of Mozilla certificates

ca-certificates-bundle-20191127-r5 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-bundle-20191127-r5 installed size:
228 KiB

ca-certificates-bundle-20191127-r5 license:
MPL-2.0 AND MIT

```

### `apk` package: `libc-utils`

```console
libc-utils-0.7.2-r3 description:
Meta package to pull in correct libc

libc-utils-0.7.2-r3 webpage:
https://alpinelinux.org

libc-utils-0.7.2-r3 installed size:
4096 B

libc-utils-0.7.2-r3 license:
BSD-2-Clause AND BSD-3-Clause

```

### `apk` package: `libcrypto1.1`

```console
libcrypto1.1-1.1.1l-r0 description:
Crypto library from openssl

libcrypto1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libcrypto1.1-1.1.1l-r0 installed size:
2704 KiB

libcrypto1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `liblksctp`

```console
liblksctp-1.0.18-r1 description:
User-space access to Linux Kernel SCTP (libraries)

liblksctp-1.0.18-r1 webpage:
http://lksctp.sourceforge.net

liblksctp-1.0.18-r1 installed size:
52 KiB

liblksctp-1.0.18-r1 license:
GPL-2.0-only AND LGPL-2.1-or-later

```

### `apk` package: `libssl1.1`

```console
libssl1.1-1.1.1l-r0 description:
SSL shared libraries

libssl1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libssl1.1-1.1.1l-r0 installed size:
528 KiB

libssl1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libtls-standalone`

```console
libtls-standalone-2.9.1-r1 description:
libtls extricated from libressl sources

libtls-standalone-2.9.1-r1 webpage:
https://www.libressl.org/

libtls-standalone-2.9.1-r1 installed size:
108 KiB

libtls-standalone-2.9.1-r1 license:
ISC

```

### `apk` package: `lksctp-tools`

```console
lksctp-tools-1.0.18-r1 description:
User-space access to Linux Kernel SCTP

lksctp-tools-1.0.18-r1 webpage:
http://lksctp.sourceforge.net

lksctp-tools-1.0.18-r1 installed size:
140 KiB

lksctp-tools-1.0.18-r1 license:
GPL-2.0-only AND LGPL-2.1-or-later

```

### `apk` package: `musl`

```console
musl-1.2.2-r1 description:
the musl c library (libc) implementation

musl-1.2.2-r1 webpage:
https://musl.libc.org/

musl-1.2.2-r1 installed size:
608 KiB

musl-1.2.2-r1 license:
MIT

```

### `apk` package: `musl-utils`

```console
musl-utils-1.2.2-r1 description:
the musl c library (libc) implementation

musl-utils-1.2.2-r1 webpage:
https://musl.libc.org/

musl-utils-1.2.2-r1 installed size:
140 KiB

musl-utils-1.2.2-r1 license:
MIT BSD GPL2+

```

### `apk` package: `ncurses-libs`

```console
ncurses-libs-6.2_p20210109-r0 description:
Ncurses libraries

ncurses-libs-6.2_p20210109-r0 webpage:
https://invisible-island.net/ncurses/

ncurses-libs-6.2_p20210109-r0 installed size:
496 KiB

ncurses-libs-6.2_p20210109-r0 license:
MIT

```

### `apk` package: `ncurses-terminfo-base`

```console
ncurses-terminfo-base-6.2_p20210109-r0 description:
Descriptions of common terminals

ncurses-terminfo-base-6.2_p20210109-r0 webpage:
https://invisible-island.net/ncurses/

ncurses-terminfo-base-6.2_p20210109-r0 installed size:
216 KiB

ncurses-terminfo-base-6.2_p20210109-r0 license:
MIT

```

### `apk` package: `readline`

```console
readline-8.1.0-r0 description:
GNU readline library

readline-8.1.0-r0 webpage:
https://tiswww.cwru.edu/php/chet/readline/rltop.html

readline-8.1.0-r0 installed size:
308 KiB

readline-8.1.0-r0 license:
GPL-2.0-or-later

```

### `apk` package: `scanelf`

```console
scanelf-1.2.8-r0 description:
Scan ELF binaries for stuff

scanelf-1.2.8-r0 webpage:
https://wiki.gentoo.org/wiki/Hardened/PaX_Utilities

scanelf-1.2.8-r0 installed size:
92 KiB

scanelf-1.2.8-r0 license:
GPL-2.0-only

```

### `apk` package: `ssl_client`

```console
ssl_client-1.32.1-r6 description:
EXternal ssl_client for busybox wget

ssl_client-1.32.1-r6 webpage:
https://busybox.net/

ssl_client-1.32.1-r6 installed size:
28 KiB

ssl_client-1.32.1-r6 license:
GPL-2.0-only

```

### `apk` package: `unixodbc`

```console
unixodbc-2.3.9-r1 description:
ODBC is an open specification to access Data Sources

unixodbc-2.3.9-r1 webpage:
http://www.unixodbc.org/

unixodbc-2.3.9-r1 installed size:
700 KiB

unixodbc-2.3.9-r1 license:
LGPL-2.0-or-later

```

### `apk` package: `zlib`

```console
zlib-1.2.11-r3 description:
A compression/decompression Library

zlib-1.2.11-r3 webpage:
https://zlib.net/

zlib-1.2.11-r3 installed size:
108 KiB

zlib-1.2.11-r3 license:
Zlib

```
