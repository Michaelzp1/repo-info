## `mageia:cauldron`

```console
$ docker pull mageia@sha256:c1013793a986cdbb6ff91e1e5db9a264537c34094253c000b6c212f2afaae8b0
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 3
	-	linux; amd64
	-	linux; arm variant v7
	-	linux; arm64 variant v8

### `mageia:cauldron` - linux; amd64

```console
$ docker pull mageia@sha256:81f37116368e9bdbc05a2948459caf05975cdc86b75b2353c0654ba582eea65e
```

-	Docker Version: 19.03.12
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **111.5 MB (111489373 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b87a6a1118c2bfae4e6cc27e74101537aaad22888734908bab43268554d5b780`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 08 Apr 2021 19:22:32 GMT
MAINTAINER "Juan Luis Baptiste" <juancho@mageia.org>
# Thu, 08 Apr 2021 19:23:13 GMT
ADD file:a6b8fb6bc1c30fbe7491bf66f6b79646db247a55e47e89ce995601db9bde99de in / 
# Thu, 08 Apr 2021 19:23:14 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:30ceb7200a194fede71e0162cef3070c1146b150f26a111c8ba1518faf10515f`  
		Last Modified: Thu, 08 Apr 2021 19:24:35 GMT  
		Size: 111.5 MB (111489373 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `mageia:cauldron` - linux; arm variant v7

```console
$ docker pull mageia@sha256:5d0b02c7042da09eefe17487369d72620530b21c50bc2df1d4da8744670f57ff
```

-	Docker Version: 19.03.12
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **99.6 MB (99644274 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3b2c9ab030502da023b8a5fabc60478c9b41eb69d2f52bce8cdd3ed68aa2d6a8`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 10 May 2019 23:52:32 GMT
MAINTAINER "Juan Luis Baptiste" <juancho@mageia.org>
# Thu, 08 Apr 2021 19:01:09 GMT
ADD file:07cf8e6fdfdac3035e1563ad16a17dc72386a58276c36b20367ae2241d848165 in / 
# Thu, 08 Apr 2021 19:01:13 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:7146c64058582eda58cd7025bcf2d86a943da07d48c146f907996dc572f56aa5`  
		Last Modified: Thu, 08 Apr 2021 19:03:20 GMT  
		Size: 99.6 MB (99644274 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `mageia:cauldron` - linux; arm64 variant v8

```console
$ docker pull mageia@sha256:f40b41f9f596ecd44d54fcfafbbdbb5305add13d4852b9f2940c1129c4bf4744
```

-	Docker Version: 19.03.12
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **104.5 MB (104465402 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d221e3dde9630fb02792f897de7001480dcdb9cfa0f0c1a82d07c077ab940d2f`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Mon, 20 May 2019 22:51:25 GMT
MAINTAINER "Juan Luis Baptiste" <juancho@mageia.org>
# Thu, 08 Apr 2021 18:42:28 GMT
ADD file:4709aa1eede2eb1dd0efcfb6e52d88d545518dace9adf61534e97c029a19ad73 in / 
# Thu, 08 Apr 2021 18:42:33 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:42ce7cb655f14bc074fb885ffc2d007561a8755bc7888a9fa023b6c989260b51`  
		Last Modified: Thu, 08 Apr 2021 18:44:21 GMT  
		Size: 104.5 MB (104465402 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
