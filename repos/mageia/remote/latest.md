## `mageia:latest`

```console
$ docker pull mageia@sha256:ee8deeb5ab22773a38ee147c98127b2faa5edc72272beef5d497db44c4fda658
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 3
	-	linux; amd64
	-	linux; arm variant v7
	-	linux; arm64 variant v8

### `mageia:latest` - linux; amd64

```console
$ docker pull mageia@sha256:8493cf3c971e7fb11901489d309c36b2f4a6ea09a9a199d4b2f606e23b4fd030
```

-	Docker Version: 19.03.12
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **110.5 MB (110539840 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5d2f474d2628e13f1dbad79463a27c5c226f03eef3b4a5f6f5c9c92a83707cbd`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 08 Apr 2021 19:22:32 GMT
MAINTAINER "Juan Luis Baptiste" <juancho@mageia.org>
# Thu, 08 Apr 2021 19:22:41 GMT
ADD file:bc20b285107532db99fb447f9cd37ebc4f30146125fe644df86523900d8c5eea in / 
# Thu, 08 Apr 2021 19:22:41 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:2b7a6260b5e1024ee3e3aaea14424ae322182becf6d1593b6542c7e711e2c6bc`  
		Last Modified: Thu, 08 Apr 2021 19:23:40 GMT  
		Size: 110.5 MB (110539840 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `mageia:latest` - linux; arm variant v7

```console
$ docker pull mageia@sha256:3c7af3f6e7c0ff13a5bed95893d83a70670ea82496df8a7031330c2a0e7f3548
```

-	Docker Version: 19.03.12
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **98.7 MB (98722394 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:007291ab74dba35b5326ba43f66ef87f64361ef9e505157615185ffac2d5fe0a`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 10 May 2019 23:52:32 GMT
MAINTAINER "Juan Luis Baptiste" <juancho@mageia.org>
# Thu, 08 Apr 2021 18:59:48 GMT
ADD file:1d85a62b5307305b3ac2a98bbe876a81bcf28d9535186ac0e9d1d3be9427591b in / 
# Thu, 08 Apr 2021 18:59:50 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:c95c4479265a26554c2c1e0a3014d17b2c7dd17051a554e256a13b1952e4194f`  
		Last Modified: Thu, 08 Apr 2021 19:02:03 GMT  
		Size: 98.7 MB (98722394 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `mageia:latest` - linux; arm64 variant v8

```console
$ docker pull mageia@sha256:3442482718e8d83c62d2b5514cf48be4272b306e745ab811a0d22e1e3356310a
```

-	Docker Version: 19.03.12
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **103.5 MB (103482098 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cdf768b606028b7dd80471a9662249702d04d531898c40e260bc94c7677990cf`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Mon, 20 May 2019 22:51:25 GMT
MAINTAINER "Juan Luis Baptiste" <juancho@mageia.org>
# Thu, 08 Apr 2021 18:41:35 GMT
ADD file:3a94d4705390ce0e7d4d99f9b43300dc1fe0c2e96d30ef6ccd2a220eec1cdf2f in / 
# Thu, 08 Apr 2021 18:41:38 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:308fdbbff63b950c0be1bc073fd6f1bf89c717e65c299ad0f90990d6b07357cd`  
		Last Modified: Thu, 08 Apr 2021 18:43:14 GMT  
		Size: 103.5 MB (103482098 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
