<!-- THIS FILE IS GENERATED VIA './update-remote.sh' -->

# Tags of `mageia`

-	[`mageia:7`](#mageia7)
-	[`mageia:8`](#mageia8)
-	[`mageia:cauldron`](#mageiacauldron)
-	[`mageia:latest`](#mageialatest)

## `mageia:7`

```console
$ docker pull mageia@sha256:30a0be429b4c987bd57697382562aed95a33fc2ed6927239709334ce3a3f5f95
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 3
	-	linux; amd64
	-	linux; arm variant v7
	-	linux; arm64 variant v8

### `mageia:7` - linux; amd64

```console
$ docker pull mageia@sha256:f5a26c57f5f60a817cad323c1eeeba305ea5f7828c49ecd284abdb5ba851dc86
```

-	Docker Version: 19.03.12
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **101.8 MB (101790982 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d4f79c43e24562ed859d373e53f598955bb91c727d41cbb2d8667d9327043797`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 08 Apr 2021 19:22:32 GMT
MAINTAINER "Juan Luis Baptiste" <juancho@mageia.org>
# Thu, 08 Apr 2021 19:22:57 GMT
ADD file:7816e97f0bcaf09a3e3a94e88111cb6e188a681b3113e5eb2edf13c50d1ca06f in / 
# Thu, 08 Apr 2021 19:22:58 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:d515b204ceac4cd76c6293d199258b871820a4a6843a6745384b2fb22af44440`  
		Last Modified: Thu, 08 Apr 2021 19:24:09 GMT  
		Size: 101.8 MB (101790982 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `mageia:7` - linux; arm variant v7

```console
$ docker pull mageia@sha256:1611bdd9c1ecc6d337009690bb836c09d2e835ae29fddf1b1b561b58f1d1177a
```

-	Docker Version: 19.03.12
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **91.2 MB (91222991 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:89342603512f4fc7ae0af1571b8d2e24a5f157fd6a7fb565b5ef7ad7a830cc7e`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 10 May 2019 23:52:32 GMT
MAINTAINER "Juan Luis Baptiste" <juancho@mageia.org>
# Thu, 08 Apr 2021 19:00:22 GMT
ADD file:88915bd3cb5c5f5f8ecc3d520c3da2243baa431ed5528239735303282103aa7a in / 
# Thu, 08 Apr 2021 19:00:28 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:7666fe2a0a4161a052cbfe34be01c545fd1a0b49cb7069dd1a21c0376dccc03c`  
		Last Modified: Thu, 08 Apr 2021 19:02:40 GMT  
		Size: 91.2 MB (91222991 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `mageia:7` - linux; arm64 variant v8

```console
$ docker pull mageia@sha256:f8c58ebb2c946647465b524166677b7e36774b7fb35b9df6547950beaa8f973e
```

-	Docker Version: 19.03.12
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **97.6 MB (97593342 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:60eee2243cdfe171f664a986e8b4092b08c33601e1d1560a063e24eb3182b7f1`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Mon, 20 May 2019 22:51:25 GMT
MAINTAINER "Juan Luis Baptiste" <juancho@mageia.org>
# Thu, 08 Apr 2021 18:41:59 GMT
ADD file:1646caa9d073a61893d13fc9a537f6185ec4ef26b9b5dd116cb1a6bd30eba192 in / 
# Thu, 08 Apr 2021 18:42:05 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:618e086e95ae1e71fc76185eb9ad3d53ef6e3c466b65bf4b94a8692205192a4e`  
		Last Modified: Thu, 08 Apr 2021 18:43:46 GMT  
		Size: 97.6 MB (97593342 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `mageia:8`

```console
$ docker pull mageia@sha256:ee8deeb5ab22773a38ee147c98127b2faa5edc72272beef5d497db44c4fda658
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 3
	-	linux; amd64
	-	linux; arm variant v7
	-	linux; arm64 variant v8

### `mageia:8` - linux; amd64

```console
$ docker pull mageia@sha256:8493cf3c971e7fb11901489d309c36b2f4a6ea09a9a199d4b2f606e23b4fd030
```

-	Docker Version: 19.03.12
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **110.5 MB (110539840 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5d2f474d2628e13f1dbad79463a27c5c226f03eef3b4a5f6f5c9c92a83707cbd`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 08 Apr 2021 19:22:32 GMT
MAINTAINER "Juan Luis Baptiste" <juancho@mageia.org>
# Thu, 08 Apr 2021 19:22:41 GMT
ADD file:bc20b285107532db99fb447f9cd37ebc4f30146125fe644df86523900d8c5eea in / 
# Thu, 08 Apr 2021 19:22:41 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:2b7a6260b5e1024ee3e3aaea14424ae322182becf6d1593b6542c7e711e2c6bc`  
		Last Modified: Thu, 08 Apr 2021 19:23:40 GMT  
		Size: 110.5 MB (110539840 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `mageia:8` - linux; arm variant v7

```console
$ docker pull mageia@sha256:3c7af3f6e7c0ff13a5bed95893d83a70670ea82496df8a7031330c2a0e7f3548
```

-	Docker Version: 19.03.12
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **98.7 MB (98722394 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:007291ab74dba35b5326ba43f66ef87f64361ef9e505157615185ffac2d5fe0a`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 10 May 2019 23:52:32 GMT
MAINTAINER "Juan Luis Baptiste" <juancho@mageia.org>
# Thu, 08 Apr 2021 18:59:48 GMT
ADD file:1d85a62b5307305b3ac2a98bbe876a81bcf28d9535186ac0e9d1d3be9427591b in / 
# Thu, 08 Apr 2021 18:59:50 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:c95c4479265a26554c2c1e0a3014d17b2c7dd17051a554e256a13b1952e4194f`  
		Last Modified: Thu, 08 Apr 2021 19:02:03 GMT  
		Size: 98.7 MB (98722394 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `mageia:8` - linux; arm64 variant v8

```console
$ docker pull mageia@sha256:3442482718e8d83c62d2b5514cf48be4272b306e745ab811a0d22e1e3356310a
```

-	Docker Version: 19.03.12
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **103.5 MB (103482098 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cdf768b606028b7dd80471a9662249702d04d531898c40e260bc94c7677990cf`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Mon, 20 May 2019 22:51:25 GMT
MAINTAINER "Juan Luis Baptiste" <juancho@mageia.org>
# Thu, 08 Apr 2021 18:41:35 GMT
ADD file:3a94d4705390ce0e7d4d99f9b43300dc1fe0c2e96d30ef6ccd2a220eec1cdf2f in / 
# Thu, 08 Apr 2021 18:41:38 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:308fdbbff63b950c0be1bc073fd6f1bf89c717e65c299ad0f90990d6b07357cd`  
		Last Modified: Thu, 08 Apr 2021 18:43:14 GMT  
		Size: 103.5 MB (103482098 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `mageia:cauldron`

```console
$ docker pull mageia@sha256:c1013793a986cdbb6ff91e1e5db9a264537c34094253c000b6c212f2afaae8b0
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 3
	-	linux; amd64
	-	linux; arm variant v7
	-	linux; arm64 variant v8

### `mageia:cauldron` - linux; amd64

```console
$ docker pull mageia@sha256:81f37116368e9bdbc05a2948459caf05975cdc86b75b2353c0654ba582eea65e
```

-	Docker Version: 19.03.12
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **111.5 MB (111489373 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b87a6a1118c2bfae4e6cc27e74101537aaad22888734908bab43268554d5b780`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 08 Apr 2021 19:22:32 GMT
MAINTAINER "Juan Luis Baptiste" <juancho@mageia.org>
# Thu, 08 Apr 2021 19:23:13 GMT
ADD file:a6b8fb6bc1c30fbe7491bf66f6b79646db247a55e47e89ce995601db9bde99de in / 
# Thu, 08 Apr 2021 19:23:14 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:30ceb7200a194fede71e0162cef3070c1146b150f26a111c8ba1518faf10515f`  
		Last Modified: Thu, 08 Apr 2021 19:24:35 GMT  
		Size: 111.5 MB (111489373 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `mageia:cauldron` - linux; arm variant v7

```console
$ docker pull mageia@sha256:5d0b02c7042da09eefe17487369d72620530b21c50bc2df1d4da8744670f57ff
```

-	Docker Version: 19.03.12
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **99.6 MB (99644274 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3b2c9ab030502da023b8a5fabc60478c9b41eb69d2f52bce8cdd3ed68aa2d6a8`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 10 May 2019 23:52:32 GMT
MAINTAINER "Juan Luis Baptiste" <juancho@mageia.org>
# Thu, 08 Apr 2021 19:01:09 GMT
ADD file:07cf8e6fdfdac3035e1563ad16a17dc72386a58276c36b20367ae2241d848165 in / 
# Thu, 08 Apr 2021 19:01:13 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:7146c64058582eda58cd7025bcf2d86a943da07d48c146f907996dc572f56aa5`  
		Last Modified: Thu, 08 Apr 2021 19:03:20 GMT  
		Size: 99.6 MB (99644274 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `mageia:cauldron` - linux; arm64 variant v8

```console
$ docker pull mageia@sha256:f40b41f9f596ecd44d54fcfafbbdbb5305add13d4852b9f2940c1129c4bf4744
```

-	Docker Version: 19.03.12
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **104.5 MB (104465402 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d221e3dde9630fb02792f897de7001480dcdb9cfa0f0c1a82d07c077ab940d2f`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Mon, 20 May 2019 22:51:25 GMT
MAINTAINER "Juan Luis Baptiste" <juancho@mageia.org>
# Thu, 08 Apr 2021 18:42:28 GMT
ADD file:4709aa1eede2eb1dd0efcfb6e52d88d545518dace9adf61534e97c029a19ad73 in / 
# Thu, 08 Apr 2021 18:42:33 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:42ce7cb655f14bc074fb885ffc2d007561a8755bc7888a9fa023b6c989260b51`  
		Last Modified: Thu, 08 Apr 2021 18:44:21 GMT  
		Size: 104.5 MB (104465402 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `mageia:latest`

```console
$ docker pull mageia@sha256:ee8deeb5ab22773a38ee147c98127b2faa5edc72272beef5d497db44c4fda658
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 3
	-	linux; amd64
	-	linux; arm variant v7
	-	linux; arm64 variant v8

### `mageia:latest` - linux; amd64

```console
$ docker pull mageia@sha256:8493cf3c971e7fb11901489d309c36b2f4a6ea09a9a199d4b2f606e23b4fd030
```

-	Docker Version: 19.03.12
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **110.5 MB (110539840 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5d2f474d2628e13f1dbad79463a27c5c226f03eef3b4a5f6f5c9c92a83707cbd`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 08 Apr 2021 19:22:32 GMT
MAINTAINER "Juan Luis Baptiste" <juancho@mageia.org>
# Thu, 08 Apr 2021 19:22:41 GMT
ADD file:bc20b285107532db99fb447f9cd37ebc4f30146125fe644df86523900d8c5eea in / 
# Thu, 08 Apr 2021 19:22:41 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:2b7a6260b5e1024ee3e3aaea14424ae322182becf6d1593b6542c7e711e2c6bc`  
		Last Modified: Thu, 08 Apr 2021 19:23:40 GMT  
		Size: 110.5 MB (110539840 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `mageia:latest` - linux; arm variant v7

```console
$ docker pull mageia@sha256:3c7af3f6e7c0ff13a5bed95893d83a70670ea82496df8a7031330c2a0e7f3548
```

-	Docker Version: 19.03.12
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **98.7 MB (98722394 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:007291ab74dba35b5326ba43f66ef87f64361ef9e505157615185ffac2d5fe0a`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Fri, 10 May 2019 23:52:32 GMT
MAINTAINER "Juan Luis Baptiste" <juancho@mageia.org>
# Thu, 08 Apr 2021 18:59:48 GMT
ADD file:1d85a62b5307305b3ac2a98bbe876a81bcf28d9535186ac0e9d1d3be9427591b in / 
# Thu, 08 Apr 2021 18:59:50 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:c95c4479265a26554c2c1e0a3014d17b2c7dd17051a554e256a13b1952e4194f`  
		Last Modified: Thu, 08 Apr 2021 19:02:03 GMT  
		Size: 98.7 MB (98722394 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `mageia:latest` - linux; arm64 variant v8

```console
$ docker pull mageia@sha256:3442482718e8d83c62d2b5514cf48be4272b306e745ab811a0d22e1e3356310a
```

-	Docker Version: 19.03.12
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **103.5 MB (103482098 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cdf768b606028b7dd80471a9662249702d04d531898c40e260bc94c7677990cf`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Mon, 20 May 2019 22:51:25 GMT
MAINTAINER "Juan Luis Baptiste" <juancho@mageia.org>
# Thu, 08 Apr 2021 18:41:35 GMT
ADD file:3a94d4705390ce0e7d4d99f9b43300dc1fe0c2e96d30ef6ccd2a220eec1cdf2f in / 
# Thu, 08 Apr 2021 18:41:38 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:308fdbbff63b950c0be1bc073fd6f1bf89c717e65c299ad0f90990d6b07357cd`  
		Last Modified: Thu, 08 Apr 2021 18:43:14 GMT  
		Size: 103.5 MB (103482098 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
