# `mageia:8`

## Docker Metadata

- Image ID: `sha256:5d2f474d2628e13f1dbad79463a27c5c226f03eef3b4a5f6f5c9c92a83707cbd`
- Created: `2021-04-08T19:22:41.937991859Z`
- Virtual Size: ~ 313.29 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["/bin/bash"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
