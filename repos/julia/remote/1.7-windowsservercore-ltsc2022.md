## `julia:1.7-windowsservercore-ltsc2022`

```console
$ docker pull julia@sha256:356ea9e770390b94092fe62b971adcc8a5f2c59b06bfa7423d454e2f2d0e3f3c
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	windows version 10.0.20348.288; amd64

### `julia:1.7-windowsservercore-ltsc2022` - windows version 10.0.20348.288; amd64

```console
$ docker pull julia@sha256:4cdac4eb0e86c232d524c65ad817ef5ff35e32c52e5ad4fdfbfbedc1c7b693ce
```

-	Docker Version: 20.10.8
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.3 GB (2285838520 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:dcc3cdf2118e6dbf5d9e91322c7550fc07cf9213b2940c8153154be6e462ea1d`
-	Default Command: `["julia"]`
-	`SHELL`: `["powershell","-Command","$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]`

```dockerfile
# Sat, 08 May 2021 09:40:24 GMT
RUN Apply image 2022-RTM-amd64
# Thu, 07 Oct 2021 11:33:56 GMT
RUN Install update ltsc2022-amd64
# Wed, 13 Oct 2021 12:26:42 GMT
SHELL [powershell -Command $ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';]
# Mon, 25 Oct 2021 18:15:03 GMT
ENV JULIA_VERSION=1.7.0-rc2
# Mon, 25 Oct 2021 18:15:04 GMT
ENV JULIA_SHA256=e17314113d70b64f1fe8aa5ee9d7e0cd96ac884af786c7ee58c5b032225c6d23
# Mon, 25 Oct 2021 18:16:42 GMT
RUN $url = ('https://julialang-s3.julialang.org/bin/winnt/x64/{1}/julia-{0}-win64.exe' -f $env:JULIA_VERSION, ($env:JULIA_VERSION.Split('.')[0..1] -Join '.')); 	Write-Host ('Downloading {0} ...' -f $url); 	[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; 	Invoke-WebRequest -Uri $url -OutFile 'julia.exe'; 		Write-Host ('Verifying sha256 ({0}) ...' -f $env:JULIA_SHA256); 	if ((Get-FileHash julia.exe -Algorithm sha256).Hash -ne $env:JULIA_SHA256) { 		Write-Host 'FAILED!'; 		exit 1; 	}; 		Write-Host 'Installing ...'; 	Start-Process -Wait -NoNewWindow 		-FilePath '.\julia.exe' 		-ArgumentList @( 			'/SILENT', 			'/DIR=C:\julia' 		); 		Write-Host 'Updating PATH ...'; 	$env:PATH = 'C:\julia\bin;' + $env:PATH; 	[Environment]::SetEnvironmentVariable('PATH', $env:PATH, [EnvironmentVariableTarget]::Machine); 		Write-Host 'Verifying install ("julia --version") ...'; 	julia --version; 		Write-Host 'Removing ...'; 	Remove-Item julia.exe -Force; 		Write-Host 'Complete.'
# Mon, 25 Oct 2021 18:16:43 GMT
CMD ["julia"]
```

-	Layers:
	-	`sha256:8f616e6e9eec767c425fd9346648807d1b658d20ff6097be1d955aac69c26642`  
		Size: 1.3 GB (1251699055 bytes)  
		MIME: application/vnd.docker.image.rootfs.foreign.diff.tar.gzip
	-	`sha256:b03bbc71f9254a4ad2fba472595c859655b9d0cfefa638928416e277e0f0d497`  
		Size: 889.8 MB (889767519 bytes)  
		MIME: application/vnd.docker.image.rootfs.foreign.diff.tar.gzip
	-	`sha256:b201e45e5b11128e36517715f5b6ae98e5782737c1b112a5fae2aa83206f57bf`  
		Last Modified: Wed, 13 Oct 2021 13:23:57 GMT  
		Size: 1.4 KB (1433 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:242d3a055515a28185bfc984f657cdc51b77d5eaccd9a1dda113f01261aa7279`  
		Last Modified: Mon, 25 Oct 2021 18:24:31 GMT  
		Size: 1.4 KB (1398 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:4be83ad11ffe05d3136289bade37a9b8391526778e3bbec5aea98418b387ac78`  
		Last Modified: Mon, 25 Oct 2021 18:24:31 GMT  
		Size: 1.4 KB (1424 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:65e49a69399f883f108ec0331e1aa093bd7b98f42e80fc6316b504b9346f0833`  
		Last Modified: Mon, 25 Oct 2021 18:25:06 GMT  
		Size: 144.4 MB (144366272 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:9cf3f5dfcee36718b89f39876f19928d39059c34575683a6c257bfa3792026a9`  
		Last Modified: Mon, 25 Oct 2021 18:24:31 GMT  
		Size: 1.4 KB (1419 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
