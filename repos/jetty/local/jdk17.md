# `jetty:9.4.44`

## Docker Metadata

- Image ID: `sha256:9743ea05fbc65088f991f86d91be1fa6068ffd3cd1e8fa6aa9b30c5a24b2ad2d`
- Created: `2021-10-28T02:46:12.34295633Z`
- Virtual Size: ~ 482.03 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/docker-entrypoint.sh"]`
- Command: `["java","-jar","/usr/local/jetty/start.jar"]`
- Environment:
  - `PATH=/usr/local/jetty/bin:/usr/java/openjdk-17/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `JAVA_HOME=/usr/java/openjdk-17`
  - `LANG=C.UTF-8`
  - `JAVA_VERSION=17.0.1`
  - `JETTY_VERSION=9.4.44.v20210927`
  - `JETTY_HOME=/usr/local/jetty`
  - `JETTY_BASE=/var/lib/jetty`
  - `TMPDIR=/tmp/jetty`
  - `JETTY_TGZ_URL=https://repo1.maven.org/maven2/org/eclipse/jetty/jetty-home/9.4.44.v20210927/jetty-home-9.4.44.v20210927.tar.gz`
  - `JETTY_GPG_KEYS=AED5EE6C45D0FE8D5D1B164F27DED4BF6216DB8F 	2A684B57436A81FA8706B53C61C3351A438A3B7D 	5989BAF76217B843D66BE55B2D0E1FB8FE4B68B4 	B59B67FD7904984367F931800818D9D68FB67BAC 	BFBB21C246D7776836287A48A04E0C74ABB35FEA 	8B096546B1A8F02656B15D3B1677D141BCF3584D 	FBA2B18D238AB852DF95745C76157BDF03D0DCD6 	5C9579B3DB2E506429319AAEF33B071B29559E1E 	F254B35617DC255D9344BCFA873A8E86B4372146`
