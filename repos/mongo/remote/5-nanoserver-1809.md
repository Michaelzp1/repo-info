## `mongo:5-nanoserver-1809`

```console
$ docker pull mongo@sha256:6da9de11e7bba713edd3e67c4343ded7896f6faac63d90173488090db95bae6d
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	windows version 10.0.17763.2237; amd64

### `mongo:5-nanoserver-1809` - windows version 10.0.17763.2237; amd64

```console
$ docker pull mongo@sha256:777f593fb06706122e5ac52aa5b8f01fb806522762c05566c43b12274068f8ae
```

-	Docker Version: 20.10.8
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **395.2 MB (395210990 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:96c8afcbb432d55f15a6369eb7ffdcdac2be537c79833ea6df7342f400671c61`
-	Default Command: `["mongod","--bind_ip_all"]`
-	`SHELL`: `["cmd","\/S","\/C"]`

```dockerfile
# Thu, 07 Oct 2021 08:01:14 GMT
RUN Apply image 1809-amd64
# Wed, 13 Oct 2021 12:53:34 GMT
SHELL [cmd /S /C]
# Thu, 14 Oct 2021 01:40:20 GMT
USER ContainerAdministrator
# Thu, 14 Oct 2021 01:40:33 GMT
RUN setx /m PATH "C:\mongodb\bin;%PATH%"
# Thu, 14 Oct 2021 01:40:34 GMT
USER ContainerUser
# Thu, 14 Oct 2021 01:40:36 GMT
COPY multi:0196f9e96f60ad3e2b92fd0773f8d0fe3a8235e1eefbb9ef1a1f0d672e6a711c in C:\Windows\System32\ 
# Thu, 14 Oct 2021 01:40:37 GMT
ENV MONGO_VERSION=5.0.3
# Thu, 14 Oct 2021 01:41:09 GMT
COPY dir:baac5d5282a412454e91eae7dd7233043d96306f42a1848f6024087e0035bcb7 in C:\mongodb 
# Thu, 14 Oct 2021 01:41:23 GMT
RUN mongo --version && mongod --version
# Thu, 14 Oct 2021 01:41:24 GMT
VOLUME [C:\data\db C:\data\configdb]
# Thu, 14 Oct 2021 01:41:25 GMT
EXPOSE 27017
# Thu, 14 Oct 2021 01:41:26 GMT
CMD ["mongod" "--bind_ip_all"]
```

-	Layers:
	-	`sha256:934e212983f208dc2bebc5de38259a6a62f1761868aacfee2cb3585a13b1e24b`  
		Size: 102.7 MB (102661372 bytes)  
		MIME: application/vnd.docker.image.rootfs.foreign.diff.tar.gzip
	-	`sha256:372f5dac265602794d3a4ddc20bd9845caf2acda0cec410704843c213a880da9`  
		Last Modified: Wed, 13 Oct 2021 13:29:46 GMT  
		Size: 1.2 KB (1168 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:bfae37dcc2021e3d10496776687736e4d2565f4b93492b8286aa9bd4a663324a`  
		Last Modified: Thu, 14 Oct 2021 02:19:23 GMT  
		Size: 1.2 KB (1180 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:e7b099be52eef9112df652bed33f01c48930744f308329472c54e09e00e77e79`  
		Last Modified: Thu, 14 Oct 2021 02:19:21 GMT  
		Size: 70.2 KB (70170 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:7102d479d05ed3a9336a5c5e5829142fb9489c43e80f3bcbcc9f22c71f1042f6`  
		Last Modified: Thu, 14 Oct 2021 02:19:21 GMT  
		Size: 1.1 KB (1059 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:455297daaaa79153573901a8796f77c2dd232e2709aeb8638e0b766abbc6875c`  
		Last Modified: Thu, 14 Oct 2021 02:19:21 GMT  
		Size: 274.0 KB (273992 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:7521c73f7ba728d36b4f9a85b3c00010629480c4aa2076310f9eb0ab8521082b`  
		Last Modified: Thu, 14 Oct 2021 02:19:20 GMT  
		Size: 1.0 KB (1032 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:6ba0bc6625058eec7c5043ec6d54d8009956d582b5b9103b9b8d19722314b506`  
		Last Modified: Thu, 14 Oct 2021 02:20:17 GMT  
		Size: 292.1 MB (292116373 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:1a4bc7aa2a6d273e373e50935048aa0d1bff549457dcef93e8f309a79dc2e902`  
		Last Modified: Thu, 14 Oct 2021 02:19:18 GMT  
		Size: 81.4 KB (81438 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:abaa00e2a434ace17319d31c51c5e6ea5a1b54ad1f253443ef2c6f5adf72c7e9`  
		Last Modified: Thu, 14 Oct 2021 02:19:18 GMT  
		Size: 1.1 KB (1062 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:a2df6ada6a26e43ba5e5d0ecb452e92118899c4ed1bcdf6899171cfa4dd10523`  
		Last Modified: Thu, 14 Oct 2021 02:19:18 GMT  
		Size: 1.0 KB (1031 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:d1b6cee23c95b7071b2bb89135e6cffc9851e0cf9a652ed8ddf6d2982cdfd8b6`  
		Last Modified: Thu, 14 Oct 2021 02:19:18 GMT  
		Size: 1.1 KB (1113 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
