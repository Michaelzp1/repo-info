# `consul:1.9.10`

## Docker Metadata

- Image ID: `sha256:90b86a727b9cd5edfe2bb01952b7d03112f5591448f86841679a9b8d9a1de48b`
- Created: `2021-09-29T16:25:07.027990756Z`
- Virtual Size: ~ 122.46 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["docker-entrypoint.sh"]`
- Command: `["agent","-dev","-client","0.0.0.0"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `HASHICORP_RELEASES=https://releases.hashicorp.com`
- Labels:
  - `org.opencontainers.image.authors=Consul Team <consul@hashicorp.com>`
  - `org.opencontainers.image.description=Consul is a datacenter runtime that provides service discovery, configuration, and orchestration.`
  - `org.opencontainers.image.documentation=https://www.consul.io/docs`
  - `org.opencontainers.image.source=https://github.com/hashicorp/consul`
  - `org.opencontainers.image.title=consul`
  - `org.opencontainers.image.url=https://www.consul.io/`
  - `org.opencontainers.image.vendor=HashiCorp`
  - `org.opencontainers.image.version=1.9.10`

## `apk` (`.apk`-based packages)

### `apk` package: `alpine-baselayout`

```console
alpine-baselayout-3.2.0-r8 description:
Alpine base dir structure and init scripts

alpine-baselayout-3.2.0-r8 webpage:
https://git.alpinelinux.org/cgit/aports/tree/main/alpine-baselayout

alpine-baselayout-3.2.0-r8 installed size:
400 KiB

alpine-baselayout-3.2.0-r8 license:
GPL-2.0-only

```

### `apk` package: `alpine-keys`

```console
alpine-keys-2.2-r0 description:
Public keys for Alpine Linux packages

alpine-keys-2.2-r0 webpage:
https://alpinelinux.org

alpine-keys-2.2-r0 installed size:
104 KiB

alpine-keys-2.2-r0 license:
MIT

```

### `apk` package: `apk-tools`

```console
apk-tools-2.12.7-r0 description:
Alpine Package Keeper - package manager for alpine

apk-tools-2.12.7-r0 webpage:
https://gitlab.alpinelinux.org/alpine/apk-tools

apk-tools-2.12.7-r0 installed size:
304 KiB

apk-tools-2.12.7-r0 license:
GPL-2.0-only

```

### `apk` package: `brotli-libs`

```console
brotli-libs-1.0.9-r3 description:
Generic lossless compressor (libraries)

brotli-libs-1.0.9-r3 webpage:
https://github.com/google/brotli

brotli-libs-1.0.9-r3 installed size:
720 KiB

brotli-libs-1.0.9-r3 license:
MIT

```

### `apk` package: `busybox`

```console
busybox-1.32.1-r6 description:
Size optimized toolbox of many common UNIX utilities

busybox-1.32.1-r6 webpage:
https://busybox.net/

busybox-1.32.1-r6 installed size:
924 KiB

busybox-1.32.1-r6 license:
GPL-2.0-only

```

### `apk` package: `ca-certificates`

```console
ca-certificates-20191127-r5 description:
Common CA certificates PEM files from Mozilla

ca-certificates-20191127-r5 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-20191127-r5 installed size:
672 KiB

ca-certificates-20191127-r5 license:
MPL-2.0 AND MIT

```

### `apk` package: `ca-certificates-bundle`

```console
ca-certificates-bundle-20191127-r5 description:
Pre generated bundle of Mozilla certificates

ca-certificates-bundle-20191127-r5 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-bundle-20191127-r5 installed size:
228 KiB

ca-certificates-bundle-20191127-r5 license:
MPL-2.0 AND MIT

```

### `apk` package: `curl`

```console
curl-7.79.1-r0 description:
URL retrival utility and library

curl-7.79.1-r0 webpage:
https://curl.se/

curl-7.79.1-r0 installed size:
248 KiB

curl-7.79.1-r0 license:
MIT

```

### `apk` package: `dumb-init`

```console
dumb-init-1.2.5-r0 description:
minimal init system for Linux containers

dumb-init-1.2.5-r0 webpage:
https://github.com/Yelp/dumb-init

dumb-init-1.2.5-r0 installed size:
64 KiB

dumb-init-1.2.5-r0 license:
MIT

```

### `apk` package: `iptables`

```console
iptables-1.8.6-r0 description:
Linux kernel firewall, NAT and packet mangling tools

iptables-1.8.6-r0 webpage:
https://www.netfilter.org/projects/iptables/index.html

iptables-1.8.6-r0 installed size:
2172 KiB

iptables-1.8.6-r0 license:
GPL-2.0-or-later

```

### `apk` package: `iputils`

```console
iputils-20190709-r1 description:
IP Configuration Utilities (and Ping)

iputils-20190709-r1 webpage:
https://github.com/iputils/iputils/

iputils-20190709-r1 installed size:
288 KiB

iputils-20190709-r1 license:
BSD-3-Clause AND GPL-2.0-or-later AND MIT

```

### `apk` package: `jq`

```console
jq-1.6-r1 description:
A lightweight and flexible command-line JSON processor

jq-1.6-r1 webpage:
https://stedolan.github.io/jq/

jq-1.6-r1 installed size:
564 KiB

jq-1.6-r1 license:
MIT

```

### `apk` package: `libc-utils`

```console
libc-utils-0.7.2-r3 description:
Meta package to pull in correct libc

libc-utils-0.7.2-r3 webpage:
https://alpinelinux.org

libc-utils-0.7.2-r3 installed size:
4096 B

libc-utils-0.7.2-r3 license:
BSD-2-Clause AND BSD-3-Clause

```

### `apk` package: `libc6-compat`

```console
libc6-compat-1.2.2-r1 description:
compatibility libraries for glibc

libc6-compat-1.2.2-r1 webpage:
https://musl.libc.org/

libc6-compat-1.2.2-r1 installed size:
12 KiB

libc6-compat-1.2.2-r1 license:
MIT

```

### `apk` package: `libcap`

```console
libcap-2.46-r0 description:
POSIX 1003.1e capabilities

libcap-2.46-r0 webpage:
https://sites.google.com/site/fullycapable/

libcap-2.46-r0 installed size:
140 KiB

libcap-2.46-r0 license:
BSD-3-Clause OR GPL-2.0-only

```

### `apk` package: `libcrypto1.1`

```console
libcrypto1.1-1.1.1l-r0 description:
Crypto library from openssl

libcrypto1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libcrypto1.1-1.1.1l-r0 installed size:
2704 KiB

libcrypto1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libcurl`

```console
libcurl-7.79.1-r0 description:
The multiprotocol file transfer library

libcurl-7.79.1-r0 webpage:
https://curl.se/

libcurl-7.79.1-r0 installed size:
500 KiB

libcurl-7.79.1-r0 license:
MIT

```

### `apk` package: `libmnl`

```console
libmnl-1.0.4-r1 description:
Library for minimalistic netlink

libmnl-1.0.4-r1 webpage:
http://www.netfilter.org/projects/libmnl/

libmnl-1.0.4-r1 installed size:
40 KiB

libmnl-1.0.4-r1 license:
GPL

```

### `apk` package: `libnftnl-libs`

```console
libnftnl-libs-1.1.8-r0 description:
Netfilter library providing interface to the nf_tables subsystem (libraries)

libnftnl-libs-1.1.8-r0 webpage:
https://netfilter.org/projects/libnftnl

libnftnl-libs-1.1.8-r0 installed size:
196 KiB

libnftnl-libs-1.1.8-r0 license:
GPL-2.0-or-later

```

### `apk` package: `libssl1.1`

```console
libssl1.1-1.1.1l-r0 description:
SSL shared libraries

libssl1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libssl1.1-1.1.1l-r0 installed size:
528 KiB

libssl1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libtls-standalone`

```console
libtls-standalone-2.9.1-r1 description:
libtls extricated from libressl sources

libtls-standalone-2.9.1-r1 webpage:
https://www.libressl.org/

libtls-standalone-2.9.1-r1 installed size:
108 KiB

libtls-standalone-2.9.1-r1 license:
ISC

```

### `apk` package: `musl`

```console
musl-1.2.2-r1 description:
the musl c library (libc) implementation

musl-1.2.2-r1 webpage:
https://musl.libc.org/

musl-1.2.2-r1 installed size:
608 KiB

musl-1.2.2-r1 license:
MIT

```

### `apk` package: `musl-utils`

```console
musl-utils-1.2.2-r1 description:
the musl c library (libc) implementation

musl-utils-1.2.2-r1 webpage:
https://musl.libc.org/

musl-utils-1.2.2-r1 installed size:
140 KiB

musl-utils-1.2.2-r1 license:
MIT BSD GPL2+

```

### `apk` package: `nghttp2-libs`

```console
nghttp2-libs-1.42.0-r1 description:
Experimental HTTP/2 client, server and proxy (libraries)

nghttp2-libs-1.42.0-r1 webpage:
https://nghttp2.org

nghttp2-libs-1.42.0-r1 installed size:
168 KiB

nghttp2-libs-1.42.0-r1 license:
MIT

```

### `apk` package: `oniguruma`

```console
oniguruma-6.9.6-r0 description:
a regular expressions library

oniguruma-6.9.6-r0 webpage:
https://github.com/kkos/oniguruma

oniguruma-6.9.6-r0 installed size:
556 KiB

oniguruma-6.9.6-r0 license:
BSD-2-Clause

```

### `apk` package: `scanelf`

```console
scanelf-1.2.8-r0 description:
Scan ELF binaries for stuff

scanelf-1.2.8-r0 webpage:
https://wiki.gentoo.org/wiki/Hardened/PaX_Utilities

scanelf-1.2.8-r0 installed size:
92 KiB

scanelf-1.2.8-r0 license:
GPL-2.0-only

```

### `apk` package: `ssl_client`

```console
ssl_client-1.32.1-r6 description:
EXternal ssl_client for busybox wget

ssl_client-1.32.1-r6 webpage:
https://busybox.net/

ssl_client-1.32.1-r6 installed size:
28 KiB

ssl_client-1.32.1-r6 license:
GPL-2.0-only

```

### `apk` package: `su-exec`

```console
su-exec-0.2-r1 description:
switch user and group id, setgroups and exec

su-exec-0.2-r1 webpage:
https://github.com/ncopa/su-exec

su-exec-0.2-r1 installed size:
24 KiB

su-exec-0.2-r1 license:
MIT

```

### `apk` package: `zlib`

```console
zlib-1.2.11-r3 description:
A compression/decompression Library

zlib-1.2.11-r3 webpage:
https://zlib.net/

zlib-1.2.11-r3 installed size:
108 KiB

zlib-1.2.11-r3 license:
Zlib

```
