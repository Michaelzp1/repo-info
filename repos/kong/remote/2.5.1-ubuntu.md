## `kong:2.5.1-ubuntu`

```console
$ docker pull kong@sha256:673c148c94846a72e041f9f1ec0b04d113786cec6712799b4209eda2af4ffb03
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `kong:2.5.1-ubuntu` - linux; amd64

```console
$ docker pull kong@sha256:5992659176defe3a2cb08d82fade385d1bea3b88148e5a940cf3af678250c610
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **128.0 MB (128036481 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:fc48a633e5485b161e995eeefeb395dc3e2a1cc7fc08684d3ac70fb07ee5e5f3`
-	Entrypoint: `["\/docker-entrypoint.sh"]`
-	Default Command: `["kong","docker-start"]`

```dockerfile
# Sat, 16 Oct 2021 00:37:47 GMT
ADD file:5d68d27cc15a80653c93d3a0b262a28112d47a46326ff5fc2dfbf7fa3b9a0ce8 in / 
# Sat, 16 Oct 2021 00:37:47 GMT
CMD ["bash"]
# Sat, 16 Oct 2021 02:50:17 GMT
ARG ASSET=ce
# Sat, 16 Oct 2021 02:50:17 GMT
ENV ASSET=ce
# Sat, 16 Oct 2021 02:50:18 GMT
ARG EE_PORTS
# Sat, 16 Oct 2021 02:50:18 GMT
COPY file:5da22ad111df95d5c0f9c17c60cd4123a51ad46a41d3f442fca7b2bcc8d7d11b in /tmp/kong.deb 
# Sat, 16 Oct 2021 02:50:54 GMT
ARG KONG_VERSION=2.5.1
# Sat, 16 Oct 2021 02:50:54 GMT
ENV KONG_VERSION=2.5.1
# Sat, 16 Oct 2021 02:51:15 GMT
RUN set -ex     && apt-get update     && if [ "$ASSET" = "ce" ] ; then       apt-get install -y curl       && curl -fL https://download.konghq.com/gateway-${KONG_VERSION%%.*}.x-ubuntu-$(cat /etc/os-release | grep UBUNTU_CODENAME | cut -d = -f 2)/pool/all/k/kong/kong_${KONG_VERSION}_$(dpkg --print-architecture).deb -o /tmp/kong.deb       && apt-get purge -y curl;     fi;     apt-get install -y --no-install-recommends unzip git     && apt install --yes /tmp/kong.deb     && rm -rf /var/lib/apt/lists/*     && rm -rf /tmp/kong.deb     && chown kong:0 /usr/local/bin/kong     && chown -R kong:0 /usr/local/kong     && ln -s /usr/local/openresty/bin/resty /usr/local/bin/resty     && ln -s /usr/local/openresty/luajit/bin/luajit /usr/local/bin/luajit     && ln -s /usr/local/openresty/luajit/bin/luajit /usr/local/bin/lua     && ln -s /usr/local/openresty/nginx/sbin/nginx /usr/local/bin/nginx     && if [ "$ASSET" = "ce" ] ; then       kong version ;     fi
# Sat, 16 Oct 2021 02:51:16 GMT
COPY file:df7f26941e26fd034e43646906785ecba3877cf078fa891fd1d304925f70408e in /docker-entrypoint.sh 
# Sat, 16 Oct 2021 02:51:16 GMT
USER kong
# Sat, 16 Oct 2021 02:51:16 GMT
ENTRYPOINT ["/docker-entrypoint.sh"]
# Sat, 16 Oct 2021 02:51:16 GMT
EXPOSE 8000 8001 8443 8444
# Sat, 16 Oct 2021 02:51:16 GMT
STOPSIGNAL SIGQUIT
# Sat, 16 Oct 2021 02:51:16 GMT
HEALTHCHECK &{["CMD-SHELL" "kong health"] "10s" "10s" "0s" '\n'}
# Sat, 16 Oct 2021 02:51:17 GMT
CMD ["kong" "docker-start"]
```

-	Layers:
	-	`sha256:7b1a6ab2e44dbac178598dabe7cff59bd67233dba0b27e4fbd1f9d4b3c877a54`  
		Last Modified: Thu, 07 Oct 2021 23:44:23 GMT  
		Size: 28.6 MB (28567101 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:a161c721a16c61e7d874293e0cdd47f98a8f82b390bdf961d621a52dbdc1ac41`  
		Last Modified: Sat, 16 Oct 2021 02:51:59 GMT  
		Size: 25.1 MB (25081963 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:a3eed63b5a7b3d59da5743acef3a98fafd449e690ba488ac5dbc375b1828fb39`  
		Last Modified: Sat, 16 Oct 2021 02:52:36 GMT  
		Size: 74.4 MB (74386536 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:a5309cf88284208ec6c93ef9bf3450ce5b34e97c60de0f576f3139dd1c99d472`  
		Last Modified: Sat, 16 Oct 2021 02:52:24 GMT  
		Size: 881.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
