# `kong:2.4.1-alpine`

## Docker Metadata

- Image ID: `sha256:d7cf1525dfb883b0ba6a6a34740124f69b9c300aea711c255acb20f35dab1668`
- Created: `2021-09-01T03:09:13.510323882Z`
- Virtual Size: ~ 143.53 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/docker-entrypoint.sh"]`
- Command: `["kong","docker-start"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `ASSET=ce`
  - `KONG_VERSION=2.4.1`
  - `KONG_AMD64_SHA=582cbcf23eb4dcdf9873105ac7d8428a4022ec61bcc68642ad9dd8a5c03e2a57`
  - `KONG_ARM64_SHA=5ec35d1b19dd4e6592ad2c6586e68bfd1549c6a22840ce7d5654677b94e5028a`
- Labels:
  - `maintainer=Kong <support@konghq.com>`

## `apk` (`.apk`-based packages)

### `apk` package: `alpine-baselayout`

```console
alpine-baselayout-3.2.0-r3 description:
Alpine base dir structure and init scripts

alpine-baselayout-3.2.0-r3 webpage:
https://git.alpinelinux.org/cgit/aports/tree/main/alpine-baselayout

alpine-baselayout-3.2.0-r3 installed size:
404 KiB

alpine-baselayout-3.2.0-r3 license:
GPL-2.0-only

```

### `apk` package: `alpine-keys`

```console
alpine-keys-2.1-r2 description:
Public keys for Alpine Linux packages

alpine-keys-2.1-r2 webpage:
https://alpinelinux.org

alpine-keys-2.1-r2 installed size:
96 KiB

alpine-keys-2.1-r2 license:
MIT

```

### `apk` package: `apk-tools`

```console
apk-tools-2.10.8-r0 description:
Alpine Package Keeper - package manager for alpine

apk-tools-2.10.8-r0 webpage:
https://gitlab.alpinelinux.org/alpine/apk-tools

apk-tools-2.10.8-r0 installed size:
260 KiB

apk-tools-2.10.8-r0 license:
GPL2

```

### `apk` package: `bash`

```console
bash-5.0.11-r1 description:
The GNU Bourne Again shell

bash-5.0.11-r1 webpage:
https://www.gnu.org/software/bash/bash.html

bash-5.0.11-r1 installed size:
1172 KiB

bash-5.0.11-r1 license:
GPL-3.0-or-later

```

### `apk` package: `busybox`

```console
busybox-1.31.1-r10 description:
Size optimized toolbox of many common UNIX utilities

busybox-1.31.1-r10 webpage:
https://busybox.net/

busybox-1.31.1-r10 installed size:
940 KiB

busybox-1.31.1-r10 license:
GPL-2.0-only

```

### `apk` package: `ca-certificates`

```console
ca-certificates-20191127-r2 description:
Common CA certificates PEM files from Mozilla

ca-certificates-20191127-r2 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-20191127-r2 installed size:
720 KiB

ca-certificates-20191127-r2 license:
MPL-2.0 GPL-2.0-or-later

```

### `apk` package: `ca-certificates-cacert`

```console
ca-certificates-cacert-20191127-r2 description:
Mozilla bundled certificates

ca-certificates-cacert-20191127-r2 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-cacert-20191127-r2 installed size:
240 KiB

ca-certificates-cacert-20191127-r2 license:
MPL-2.0 GPL-2.0-or-later

```

### `apk` package: `expat`

```console
expat-2.2.9-r1 description:
An XML Parser library written in C

expat-2.2.9-r1 webpage:
http://www.libexpat.org/

expat-2.2.9-r1 installed size:
184 KiB

expat-2.2.9-r1 license:
MIT

```

### `apk` package: `git`

```console
git-2.24.4-r0 description:
Distributed version control system

git-2.24.4-r0 webpage:
https://www.git-scm.com/

git-2.24.4-r0 installed size:
13 MiB

git-2.24.4-r0 license:
GPL-2.0-or-later

```

### `apk` package: `git-perl`

```console
git-perl-2.24.4-r0 description:
Additional Git commands that requires perl

git-perl-2.24.4-r0 webpage:
https://www.git-scm.com/

git-perl-2.24.4-r0 installed size:
128 KiB

git-perl-2.24.4-r0 license:
GPL-2.0-or-later

```

### `apk` package: `libbz2`

```console
libbz2-1.0.8-r1 description:
Shared library for bz2

libbz2-1.0.8-r1 webpage:
http://sources.redhat.com/bzip2

libbz2-1.0.8-r1 installed size:
72 KiB

libbz2-1.0.8-r1 license:
bzip2-1.0.6

```

### `apk` package: `libc-utils`

```console
libc-utils-0.7.2-r0 description:
Meta package to pull in correct libc

libc-utils-0.7.2-r0 webpage:
http://alpinelinux.org

libc-utils-0.7.2-r0 installed size:
4096 B

libc-utils-0.7.2-r0 license:
BSD

```

### `apk` package: `libcap`

```console
libcap-2.27-r0 description:
POSIX 1003.1e capabilities

libcap-2.27-r0 webpage:
http://www.friedhoff.org/posixfilecaps.html

libcap-2.27-r0 installed size:
116 KiB

libcap-2.27-r0 license:
BSD-3-Clause OR GPL-2.0-only

```

### `apk` package: `libcrypto1.1`

```console
libcrypto1.1-1.1.1l-r0 description:
Crypto library from openssl

libcrypto1.1-1.1.1l-r0 webpage:
https://www.openssl.org

libcrypto1.1-1.1.1l-r0 installed size:
2700 KiB

libcrypto1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libcurl`

```console
libcurl-7.67.0-r5 description:
The multiprotocol file transfer library

libcurl-7.67.0-r5 webpage:
https://curl.haxx.se/

libcurl-7.67.0-r5 installed size:
448 KiB

libcurl-7.67.0-r5 license:
MIT

```

### `apk` package: `libgcc`

```console
libgcc-9.3.0-r0 description:
GNU C compiler runtime libraries

libgcc-9.3.0-r0 webpage:
http://gcc.gnu.org

libgcc-9.3.0-r0 installed size:
88 KiB

libgcc-9.3.0-r0 license:
GPL LGPL

```

### `apk` package: `libssl1.1`

```console
libssl1.1-1.1.1l-r0 description:
SSL shared libraries

libssl1.1-1.1.1l-r0 webpage:
https://www.openssl.org

libssl1.1-1.1.1l-r0 installed size:
528 KiB

libssl1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libstdc++`

```console
libstdc++-9.3.0-r0 description:
GNU C++ standard runtime library

libstdc++-9.3.0-r0 webpage:
http://gcc.gnu.org

libstdc++-9.3.0-r0 installed size:
1632 KiB

libstdc++-9.3.0-r0 license:
GPL LGPL

```

### `apk` package: `libtls-standalone`

```console
libtls-standalone-2.9.1-r0 description:
libtls extricated from libressl sources

libtls-standalone-2.9.1-r0 webpage:
https://www.libressl.org/

libtls-standalone-2.9.1-r0 installed size:
108 KiB

libtls-standalone-2.9.1-r0 license:
ISC

```

### `apk` package: `musl`

```console
musl-1.1.24-r3 description:
the musl c library (libc) implementation

musl-1.1.24-r3 webpage:
https://musl.libc.org/

musl-1.1.24-r3 installed size:
600 KiB

musl-1.1.24-r3 license:
MIT

```

### `apk` package: `musl-utils`

```console
musl-utils-1.1.24-r3 description:
the musl c library (libc) implementation

musl-utils-1.1.24-r3 webpage:
https://musl.libc.org/

musl-utils-1.1.24-r3 installed size:
148 KiB

musl-utils-1.1.24-r3 license:
MIT BSD GPL2+

```

### `apk` package: `ncurses-libs`

```console
ncurses-libs-6.1_p20200118-r4 description:
Ncurses libraries

ncurses-libs-6.1_p20200118-r4 webpage:
https://invisible-island.net/ncurses/

ncurses-libs-6.1_p20200118-r4 installed size:
496 KiB

ncurses-libs-6.1_p20200118-r4 license:
MIT

```

### `apk` package: `ncurses-terminfo-base`

```console
ncurses-terminfo-base-6.1_p20200118-r4 description:
Descriptions of common terminals

ncurses-terminfo-base-6.1_p20200118-r4 webpage:
https://invisible-island.net/ncurses/

ncurses-terminfo-base-6.1_p20200118-r4 installed size:
212 KiB

ncurses-terminfo-base-6.1_p20200118-r4 license:
MIT

```

### `apk` package: `nghttp2-libs`

```console
nghttp2-libs-1.40.0-r1 description:
Experimental HTTP/2 client, server and proxy (libraries)

nghttp2-libs-1.40.0-r1 webpage:
https://nghttp2.org

nghttp2-libs-1.40.0-r1 installed size:
156 KiB

nghttp2-libs-1.40.0-r1 license:
MIT

```

### `apk` package: `openssl`

```console
openssl-1.1.1l-r0 description:
Toolkit for Transport Layer Security (TLS)

openssl-1.1.1l-r0 webpage:
https://www.openssl.org

openssl-1.1.1l-r0 installed size:
660 KiB

openssl-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `pcre`

```console
pcre-8.43-r1 description:
Perl-compatible regular expression library

pcre-8.43-r1 webpage:
http://pcre.sourceforge.net

pcre-8.43-r1 installed size:
392 KiB

pcre-8.43-r1 license:
BSD-3-Clause

```

### `apk` package: `pcre2`

```console
pcre2-10.34-r1 description:
Perl-compatible regular expression library

pcre2-10.34-r1 webpage:
https://pcre.org/

pcre2-10.34-r1 installed size:
664 KiB

pcre2-10.34-r1 license:
BSD-3-Clause

```

### `apk` package: `perl`

```console
perl-5.30.3-r0 description:
Larry Wall's Practical Extraction and Report Language

perl-5.30.3-r0 webpage:
https://www.perl.org/

perl-5.30.3-r0 installed size:
36 MiB

perl-5.30.3-r0 license:
Artistic GPL-2.0

```

### `apk` package: `perl-error`

```console
perl-error-0.17028-r0 description:
Perl module error/exception handling in an OO-ish way

perl-error-0.17028-r0 webpage:
https://metacpan.org/release/Error

perl-error-0.17028-r0 installed size:
88 KiB

perl-error-0.17028-r0 license:
GPL-1.0-or-later OR Artistic-1.0-Perl

```

### `apk` package: `perl-git`

```console
perl-git-2.24.4-r0 description:
Perl interface to Git

perl-git-2.24.4-r0 webpage:
https://www.git-scm.com/

perl-git-2.24.4-r0 installed size:
152 KiB

perl-git-2.24.4-r0 license:
GPL-2.0-or-later

```

### `apk` package: `pkgconf`

```console
pkgconf-1.6.3-r0 description:
development framework configuration tools

pkgconf-1.6.3-r0 webpage:
https://git.dereferenced.org/pkgconf/pkgconf

pkgconf-1.6.3-r0 installed size:
140 KiB

pkgconf-1.6.3-r0 license:
ISC

```

### `apk` package: `readline`

```console
readline-8.0.1-r0 description:
GNU readline library

readline-8.0.1-r0 webpage:
https://tiswww.cwru.edu/php/chet/readline/rltop.html

readline-8.0.1-r0 installed size:
292 KiB

readline-8.0.1-r0 license:
GPL-2.0-or-later

```

### `apk` package: `scanelf`

```console
scanelf-1.2.4-r0 description:
Scan ELF binaries for stuff

scanelf-1.2.4-r0 webpage:
https://wiki.gentoo.org/wiki/Hardened/PaX_Utilities

scanelf-1.2.4-r0 installed size:
92 KiB

scanelf-1.2.4-r0 license:
GPL-2.0-only

```

### `apk` package: `ssl_client`

```console
ssl_client-1.31.1-r10 description:
EXternal ssl_client for busybox wget

ssl_client-1.31.1-r10 webpage:
https://busybox.net/

ssl_client-1.31.1-r10 installed size:
28 KiB

ssl_client-1.31.1-r10 license:
GPL-2.0-only

```

### `apk` package: `tzdata`

```console
tzdata-2021a-r0 description:
Timezone data

tzdata-2021a-r0 webpage:
https://www.iana.org/time-zones

tzdata-2021a-r0 installed size:
3436 KiB

tzdata-2021a-r0 license:
Public-Domain

```

### `apk` package: `unzip`

```console
unzip-6.0-r7 description:
Extract PKZIP-compatible .zip files

unzip-6.0-r7 webpage:
http://www.info-zip.org/UnZip.html

unzip-6.0-r7 installed size:
316 KiB

unzip-6.0-r7 license:
custom

```

### `apk` package: `zip`

```console
zip-3.0-r7 description:
Creates PKZIP-compatible .zip files

zip-3.0-r7 webpage:
http://www.info-zip.org/pub/infozip/Zip.html

zip-3.0-r7 installed size:
420 KiB

zip-3.0-r7 license:
Info-ZIP

```

### `apk` package: `zlib`

```console
zlib-1.2.11-r3 description:
A compression/decompression Library

zlib-1.2.11-r3 webpage:
https://zlib.net/

zlib-1.2.11-r3 installed size:
108 KiB

zlib-1.2.11-r3 license:
Zlib

```

### `apk` package: `zlib-dev`

```console
zlib-dev-1.2.11-r3 description:
A compression/decompression Library (development files)

zlib-dev-1.2.11-r3 webpage:
https://zlib.net/

zlib-dev-1.2.11-r3 installed size:
140 KiB

zlib-dev-1.2.11-r3 license:
Zlib

```
