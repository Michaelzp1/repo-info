# `kong:2.6.0-centos`

## Docker Metadata

- Image ID: `sha256:ee5d952d7b8eb428c80083082cfea96234e5a263dea8f0a1ef32882fbcec5920`
- Created: `2021-10-05T17:43:42.694456091Z`
- Virtual Size: ~ 455.16 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/docker-entrypoint.sh"]`
- Command: `["kong","docker-start"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `ASSET=ce`
  - `KONG_VERSION=2.6.0`
- Labels:
  - `maintainer=Kong <support@konghq.com>`
  - `org.label-schema.build-date=20210915`
  - `org.label-schema.license=GPLv2`
  - `org.label-schema.name=CentOS Base Image`
  - `org.label-schema.schema-version=1.0`
  - `org.label-schema.vendor=CentOS`
