# `kong:2.6.0-alpine`

## Docker Metadata

- Image ID: `sha256:d5ef5ae48a052818b2567e65ab0e2e45332dea662fd20af66a49a04c6a5ab54e`
- Created: `2021-10-05T17:41:55.635039605Z`
- Virtual Size: ~ 141.61 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/docker-entrypoint.sh"]`
- Command: `["kong","docker-start"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `ASSET=ce`
  - `KONG_VERSION=2.6.0`
  - `KONG_AMD64_SHA=43fb5f27185e274e22b4a36b93b1b7e27afe94b9fd2efbe4ec69b8ed8a9e5902`
  - `KONG_ARM64_SHA=a057eaa6d10ecf49443ec0cac6e1b70a62ee357a777e0e169c780e18fd5c5544`
- Labels:
  - `maintainer=Kong <support@konghq.com>`

## `apk` (`.apk`-based packages)

### `apk` package: `alpine-baselayout`

```console
alpine-baselayout-3.2.0-r16 description:
Alpine base dir structure and init scripts

alpine-baselayout-3.2.0-r16 webpage:
https://git.alpinelinux.org/cgit/aports/tree/main/alpine-baselayout

alpine-baselayout-3.2.0-r16 installed size:
404 KiB

alpine-baselayout-3.2.0-r16 license:
GPL-2.0-only

```

### `apk` package: `alpine-keys`

```console
alpine-keys-2.3-r1 description:
Public keys for Alpine Linux packages

alpine-keys-2.3-r1 webpage:
https://alpinelinux.org

alpine-keys-2.3-r1 installed size:
116 KiB

alpine-keys-2.3-r1 license:
MIT

```

### `apk` package: `apk-tools`

```console
apk-tools-2.12.7-r0 description:
Alpine Package Keeper - package manager for alpine

apk-tools-2.12.7-r0 webpage:
https://gitlab.alpinelinux.org/alpine/apk-tools

apk-tools-2.12.7-r0 installed size:
304 KiB

apk-tools-2.12.7-r0 license:
GPL-2.0-only

```

### `apk` package: `bash`

```console
bash-5.1.4-r0 description:
The GNU Bourne Again shell

bash-5.1.4-r0 webpage:
https://www.gnu.org/software/bash/bash.html

bash-5.1.4-r0 installed size:
1296 KiB

bash-5.1.4-r0 license:
GPL-3.0-or-later

```

### `apk` package: `brotli-libs`

```console
brotli-libs-1.0.9-r5 description:
Generic lossless compressor (libraries)

brotli-libs-1.0.9-r5 webpage:
https://github.com/google/brotli

brotli-libs-1.0.9-r5 installed size:
720 KiB

brotli-libs-1.0.9-r5 license:
MIT

```

### `apk` package: `busybox`

```console
busybox-1.33.1-r3 description:
Size optimized toolbox of many common UNIX utilities

busybox-1.33.1-r3 webpage:
https://busybox.net/

busybox-1.33.1-r3 installed size:
928 KiB

busybox-1.33.1-r3 license:
GPL-2.0-only

```

### `apk` package: `ca-certificates`

```console
ca-certificates-20191127-r5 description:
Common CA certificates PEM files from Mozilla

ca-certificates-20191127-r5 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-20191127-r5 installed size:
672 KiB

ca-certificates-20191127-r5 license:
MPL-2.0 AND MIT

```

### `apk` package: `ca-certificates-bundle`

```console
ca-certificates-bundle-20191127-r5 description:
Pre generated bundle of Mozilla certificates

ca-certificates-bundle-20191127-r5 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-bundle-20191127-r5 installed size:
228 KiB

ca-certificates-bundle-20191127-r5 license:
MPL-2.0 AND MIT

```

### `apk` package: `expat`

```console
expat-2.4.1-r0 description:
XML Parser library written in C

expat-2.4.1-r0 webpage:
http://www.libexpat.org/

expat-2.4.1-r0 installed size:
188 KiB

expat-2.4.1-r0 license:
MIT

```

### `apk` package: `git`

```console
git-2.32.0-r0 description:
Distributed version control system

git-2.32.0-r0 webpage:
https://www.git-scm.com/

git-2.32.0-r0 installed size:
10 MiB

git-2.32.0-r0 license:
GPL-2.0-or-later

```

### `apk` package: `git-perl`

```console
git-perl-2.32.0-r0 description:
Additional Git commands that requires perl

git-perl-2.32.0-r0 webpage:
https://www.git-scm.com/

git-perl-2.32.0-r0 installed size:
132 KiB

git-perl-2.32.0-r0 license:
GPL-2.0-or-later

```

### `apk` package: `libbz2`

```console
libbz2-1.0.8-r1 description:
Shared library for bz2

libbz2-1.0.8-r1 webpage:
http://sources.redhat.com/bzip2

libbz2-1.0.8-r1 installed size:
72 KiB

libbz2-1.0.8-r1 license:
bzip2-1.0.6

```

### `apk` package: `libc-utils`

```console
libc-utils-0.7.2-r3 description:
Meta package to pull in correct libc

libc-utils-0.7.2-r3 webpage:
https://alpinelinux.org

libc-utils-0.7.2-r3 installed size:
4096 B

libc-utils-0.7.2-r3 license:
BSD-2-Clause AND BSD-3-Clause

```

### `apk` package: `libcap`

```console
libcap-2.50-r0 description:
POSIX 1003.1e capabilities

libcap-2.50-r0 webpage:
https://sites.google.com/site/fullycapable/

libcap-2.50-r0 installed size:
168 KiB

libcap-2.50-r0 license:
BSD-3-Clause OR GPL-2.0-only

```

### `apk` package: `libcrypto1.1`

```console
libcrypto1.1-1.1.1l-r0 description:
Crypto library from openssl

libcrypto1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libcrypto1.1-1.1.1l-r0 installed size:
2704 KiB

libcrypto1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libcurl`

```console
libcurl-7.79.1-r0 description:
The multiprotocol file transfer library

libcurl-7.79.1-r0 webpage:
https://curl.se/

libcurl-7.79.1-r0 installed size:
500 KiB

libcurl-7.79.1-r0 license:
MIT

```

### `apk` package: `libgcc`

```console
libgcc-10.3.1_git20210424-r2 description:
GNU C compiler runtime libraries

libgcc-10.3.1_git20210424-r2 webpage:
https://gcc.gnu.org

libgcc-10.3.1_git20210424-r2 installed size:
112 KiB

libgcc-10.3.1_git20210424-r2 license:
GPL-2.0-or-later LGPL-2.1-or-later

```

### `apk` package: `libretls`

```console
libretls-3.3.3p1-r2 description:
port of libtls from libressl to openssl

libretls-3.3.3p1-r2 webpage:
https://git.causal.agency/libretls/

libretls-3.3.3p1-r2 installed size:
84 KiB

libretls-3.3.3p1-r2 license:
ISC AND (BSD-3-Clause OR MIT)

```

### `apk` package: `libssl1.1`

```console
libssl1.1-1.1.1l-r0 description:
SSL shared libraries

libssl1.1-1.1.1l-r0 webpage:
https://www.openssl.org/

libssl1.1-1.1.1l-r0 installed size:
528 KiB

libssl1.1-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `libstdc++`

```console
libstdc++-10.3.1_git20210424-r2 description:
GNU C++ standard runtime library

libstdc++-10.3.1_git20210424-r2 webpage:
https://gcc.gnu.org

libstdc++-10.3.1_git20210424-r2 installed size:
1664 KiB

libstdc++-10.3.1_git20210424-r2 license:
GPL-2.0-or-later LGPL-2.1-or-later

```

### `apk` package: `musl`

```console
musl-1.2.2-r3 description:
the musl c library (libc) implementation

musl-1.2.2-r3 webpage:
https://musl.libc.org/

musl-1.2.2-r3 installed size:
608 KiB

musl-1.2.2-r3 license:
MIT

```

### `apk` package: `musl-utils`

```console
musl-utils-1.2.2-r3 description:
the musl c library (libc) implementation

musl-utils-1.2.2-r3 webpage:
https://musl.libc.org/

musl-utils-1.2.2-r3 installed size:
144 KiB

musl-utils-1.2.2-r3 license:
MIT BSD GPL2+

```

### `apk` package: `ncurses-libs`

```console
ncurses-libs-6.2_p20210612-r0 description:
Ncurses libraries

ncurses-libs-6.2_p20210612-r0 webpage:
https://invisible-island.net/ncurses/

ncurses-libs-6.2_p20210612-r0 installed size:
500 KiB

ncurses-libs-6.2_p20210612-r0 license:
MIT

```

### `apk` package: `ncurses-terminfo-base`

```console
ncurses-terminfo-base-6.2_p20210612-r0 description:
Descriptions of common terminals

ncurses-terminfo-base-6.2_p20210612-r0 webpage:
https://invisible-island.net/ncurses/

ncurses-terminfo-base-6.2_p20210612-r0 installed size:
216 KiB

ncurses-terminfo-base-6.2_p20210612-r0 license:
MIT

```

### `apk` package: `nghttp2-libs`

```console
nghttp2-libs-1.43.0-r0 description:
Experimental HTTP/2 client, server and proxy (libraries)

nghttp2-libs-1.43.0-r0 webpage:
https://nghttp2.org

nghttp2-libs-1.43.0-r0 installed size:
168 KiB

nghttp2-libs-1.43.0-r0 license:
MIT

```

### `apk` package: `openssl`

```console
openssl-1.1.1l-r0 description:
Toolkit for Transport Layer Security (TLS)

openssl-1.1.1l-r0 webpage:
https://www.openssl.org/

openssl-1.1.1l-r0 installed size:
660 KiB

openssl-1.1.1l-r0 license:
OpenSSL

```

### `apk` package: `pcre`

```console
pcre-8.44-r0 description:
Perl-compatible regular expression library

pcre-8.44-r0 webpage:
http://pcre.sourceforge.net

pcre-8.44-r0 installed size:
392 KiB

pcre-8.44-r0 license:
BSD-3-Clause

```

### `apk` package: `pcre2`

```console
pcre2-10.36-r0 description:
Perl-compatible regular expression library

pcre2-10.36-r0 webpage:
https://pcre.org/

pcre2-10.36-r0 installed size:
652 KiB

pcre2-10.36-r0 license:
BSD-3-Clause

```

### `apk` package: `perl`

```console
perl-5.32.1-r0 description:
Larry Wall's Practical Extraction and Report Language

perl-5.32.1-r0 webpage:
https://www.perl.org/

perl-5.32.1-r0 installed size:
37 MiB

perl-5.32.1-r0 license:
Artistic-Perl OR GPL-1.0-or-later

```

### `apk` package: `perl-error`

```console
perl-error-0.17029-r1 description:
Perl module error/exception handling in an OO-ish way

perl-error-0.17029-r1 webpage:
https://metacpan.org/release/Error

perl-error-0.17029-r1 installed size:
84 KiB

perl-error-0.17029-r1 license:
GPL-1.0-or-later OR Artistic-1.0-Perl

```

### `apk` package: `perl-git`

```console
perl-git-2.32.0-r0 description:
Perl interface to Git

perl-git-2.32.0-r0 webpage:
https://www.git-scm.com/

perl-git-2.32.0-r0 installed size:
152 KiB

perl-git-2.32.0-r0 license:
GPL-2.0-or-later

```

### `apk` package: `pkgconf`

```console
pkgconf-1.7.4-r0 description:
development framework configuration tools

pkgconf-1.7.4-r0 webpage:
https://git.sr.ht/~kaniini/pkgconf

pkgconf-1.7.4-r0 installed size:
140 KiB

pkgconf-1.7.4-r0 license:
ISC

```

### `apk` package: `readline`

```console
readline-8.1.0-r0 description:
GNU readline library

readline-8.1.0-r0 webpage:
https://tiswww.cwru.edu/php/chet/readline/rltop.html

readline-8.1.0-r0 installed size:
308 KiB

readline-8.1.0-r0 license:
GPL-2.0-or-later

```

### `apk` package: `scanelf`

```console
scanelf-1.3.2-r0 description:
Scan ELF binaries for stuff

scanelf-1.3.2-r0 webpage:
https://wiki.gentoo.org/wiki/Hardened/PaX_Utilities

scanelf-1.3.2-r0 installed size:
92 KiB

scanelf-1.3.2-r0 license:
GPL-2.0-only

```

### `apk` package: `ssl_client`

```console
ssl_client-1.33.1-r3 description:
EXternal ssl_client for busybox wget

ssl_client-1.33.1-r3 webpage:
https://busybox.net/

ssl_client-1.33.1-r3 installed size:
28 KiB

ssl_client-1.33.1-r3 license:
GPL-2.0-only

```

### `apk` package: `tzdata`

```console
tzdata-2021c-r0 description:
Timezone data

tzdata-2021c-r0 webpage:
https://www.iana.org/time-zones

tzdata-2021c-r0 installed size:
3376 KiB

tzdata-2021c-r0 license:
Public-Domain

```

### `apk` package: `unzip`

```console
unzip-6.0-r9 description:
Extract PKZIP-compatible .zip files

unzip-6.0-r9 webpage:
http://www.info-zip.org/UnZip.html

unzip-6.0-r9 installed size:
316 KiB

unzip-6.0-r9 license:
custom

```

### `apk` package: `zip`

```console
zip-3.0-r9 description:
Creates PKZIP-compatible .zip files

zip-3.0-r9 webpage:
http://www.info-zip.org/pub/infozip/Zip.html

zip-3.0-r9 installed size:
412 KiB

zip-3.0-r9 license:
Info-ZIP

```

### `apk` package: `zlib`

```console
zlib-1.2.11-r3 description:
A compression/decompression Library

zlib-1.2.11-r3 webpage:
https://zlib.net/

zlib-1.2.11-r3 installed size:
108 KiB

zlib-1.2.11-r3 license:
Zlib

```

### `apk` package: `zlib-dev`

```console
zlib-dev-1.2.11-r3 description:
A compression/decompression Library (development files)

zlib-dev-1.2.11-r3 webpage:
https://zlib.net/

zlib-dev-1.2.11-r3 installed size:
140 KiB

zlib-dev-1.2.11-r3 license:
Zlib

```
