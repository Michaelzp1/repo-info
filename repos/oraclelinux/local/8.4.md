# `oraclelinux:8.4`

## Docker Metadata

- Image ID: `sha256:1f6254c916d3c49c136886602a3c83b911b56b6ee4a25ca0c3a86be15d18c040`
- Created: `2021-10-28T01:22:38.430675121Z`
- Virtual Size: ~ 246.16 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["/bin/bash"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
