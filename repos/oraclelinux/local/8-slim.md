# `oraclelinux:8-slim`

## Docker Metadata

- Image ID: `sha256:7ef73d4e130c2ebd597b1507bfd08b48c060fe9e9100f7c778935ddb0fe1d49e`
- Created: `2021-10-28T01:22:47.619138655Z`
- Virtual Size: ~ 110.36 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["/bin/bash"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
