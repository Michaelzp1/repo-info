## `oraclelinux:6-slim`

```console
$ docker pull oraclelinux@sha256:dbae3e4779d5b412d13d8b06e5ed8bf2a6a9a5a82414646ee76505b10f2fb173
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `oraclelinux:6-slim` - linux; amd64

```console
$ docker pull oraclelinux@sha256:bc77589bd852d5a6030f388d02390a8d2f4eaf89902ddb8160233835eee824fc
```

-	Docker Version: 19.03.12
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **43.8 MB (43786118 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:140aefca7c505ab8020f616bfc1966500969a6a9c9b4b82719b6753244aa26b0`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Tue, 09 Mar 2021 20:37:14 GMT
ADD file:0462345d2d9eac4b2067b8ecab2c69f4acbf1cb083c6f06c2111ceea9ccca161 in / 
# Tue, 09 Mar 2021 20:37:14 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:ca8686401c21efb411d51f1ae65c0feb2293d10c50afa11cdaba415cf3f944ee`  
		Last Modified: Mon, 01 Feb 2021 20:35:14 GMT  
		Size: 43.8 MB (43786118 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
