<!-- THIS FILE IS GENERATED VIA './update-remote.sh' -->

# Tags of `oraclelinux`

-	[`oraclelinux:6`](#oraclelinux6)
-	[`oraclelinux:6-slim`](#oraclelinux6-slim)
-	[`oraclelinux:6.10`](#oraclelinux610)
-	[`oraclelinux:7`](#oraclelinux7)
-	[`oraclelinux:7-slim`](#oraclelinux7-slim)
-	[`oraclelinux:7.9`](#oraclelinux79)
-	[`oraclelinux:8`](#oraclelinux8)
-	[`oraclelinux:8-slim`](#oraclelinux8-slim)
-	[`oraclelinux:8.4`](#oraclelinux84)

## `oraclelinux:6`

```console
$ docker pull oraclelinux@sha256:f4f7375d3a220de1158f57719eb1df7a7438cad9e33c3a8b8ce88907684b656b
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `oraclelinux:6` - linux; amd64

```console
$ docker pull oraclelinux@sha256:a06327c0f1d18d753f2a60bb17864c84a850bb6dcbcf5946dd1a8123f6e75495
```

-	Docker Version: 19.03.12
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **70.2 MB (70202056 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:c187fee7ec1b25f3114cbe9639cdc1296ee0d43fb117fc557b953b5c362de979`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Tue, 09 Mar 2021 20:37:05 GMT
ADD file:7549e8f7e8408ec32fdcec283dbd4958393edff0fe21754c7fb2aa934c9da691 in / 
# Tue, 09 Mar 2021 20:37:05 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:20eaa36af3affb45a7366fb8419cb8256fd13b89d18cc699bc9f0f3a95b0e455`  
		Last Modified: Mon, 01 Feb 2021 20:35:00 GMT  
		Size: 70.2 MB (70202056 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `oraclelinux:6-slim`

```console
$ docker pull oraclelinux@sha256:dbae3e4779d5b412d13d8b06e5ed8bf2a6a9a5a82414646ee76505b10f2fb173
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `oraclelinux:6-slim` - linux; amd64

```console
$ docker pull oraclelinux@sha256:bc77589bd852d5a6030f388d02390a8d2f4eaf89902ddb8160233835eee824fc
```

-	Docker Version: 19.03.12
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **43.8 MB (43786118 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:140aefca7c505ab8020f616bfc1966500969a6a9c9b4b82719b6753244aa26b0`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Tue, 09 Mar 2021 20:37:14 GMT
ADD file:0462345d2d9eac4b2067b8ecab2c69f4acbf1cb083c6f06c2111ceea9ccca161 in / 
# Tue, 09 Mar 2021 20:37:14 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:ca8686401c21efb411d51f1ae65c0feb2293d10c50afa11cdaba415cf3f944ee`  
		Last Modified: Mon, 01 Feb 2021 20:35:14 GMT  
		Size: 43.8 MB (43786118 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `oraclelinux:6.10`

```console
$ docker pull oraclelinux@sha256:f4f7375d3a220de1158f57719eb1df7a7438cad9e33c3a8b8ce88907684b656b
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `oraclelinux:6.10` - linux; amd64

```console
$ docker pull oraclelinux@sha256:a06327c0f1d18d753f2a60bb17864c84a850bb6dcbcf5946dd1a8123f6e75495
```

-	Docker Version: 19.03.12
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **70.2 MB (70202056 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:c187fee7ec1b25f3114cbe9639cdc1296ee0d43fb117fc557b953b5c362de979`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Tue, 09 Mar 2021 20:37:05 GMT
ADD file:7549e8f7e8408ec32fdcec283dbd4958393edff0fe21754c7fb2aa934c9da691 in / 
# Tue, 09 Mar 2021 20:37:05 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:20eaa36af3affb45a7366fb8419cb8256fd13b89d18cc699bc9f0f3a95b0e455`  
		Last Modified: Mon, 01 Feb 2021 20:35:00 GMT  
		Size: 70.2 MB (70202056 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `oraclelinux:7`

```console
$ docker pull oraclelinux@sha256:cac0abe031f6e16723dd29a938f56be580425c911f08baf66ba27c476fbafac2
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 2
	-	linux; amd64
	-	linux; arm64 variant v8

### `oraclelinux:7` - linux; amd64

```console
$ docker pull oraclelinux@sha256:df65e2272a740ae8c2ef332eac8c89e119b58a7a66b3c78329d248ed206bb6c2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **93.4 MB (93377687 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f81087a7fe82adc5448998ab2584e80d32c6da5d53fa1b577df3cbef173f1657`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 03 Nov 2021 00:20:07 GMT
ADD file:8a8ac480a4f9d27fff7bca8bf39f55cba123d511a3cbc515e6b26cb63c68e1e9 in / 
# Wed, 03 Nov 2021 00:20:08 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:3405b13884e35cd21aab8d897677f670249eaba300a6fe90214fc58fb731fc13`  
		Last Modified: Wed, 03 Nov 2021 00:20:57 GMT  
		Size: 93.4 MB (93377687 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `oraclelinux:7` - linux; arm64 variant v8

```console
$ docker pull oraclelinux@sha256:8b9e629939647159a457da6321ed0808e439c4cf05e3155be68de405a3d1dc3e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **95.3 MB (95302262 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:fde0714d0fe344da227dc2eb7a69ef73e2d8aa0edbeeb46e8bc0b347c2ba4f87`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 03 Nov 2021 00:43:55 GMT
ADD file:b55daa2532dd757fc185d4ecb5a97f0d33e91976432a4405d5e917240a37353b in / 
# Wed, 03 Nov 2021 00:43:57 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:5507e3191aacc8487157a8b1a1dfb16f8c70148b3f13ebd0db0e919e5b3dd770`  
		Last Modified: Wed, 03 Nov 2021 00:44:37 GMT  
		Size: 95.3 MB (95302262 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `oraclelinux:7-slim`

```console
$ docker pull oraclelinux@sha256:b834f3a63409b2bdabf676a7d27b128fc0f5c8daa66c3b34404c177e26543a37
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 2
	-	linux; amd64
	-	linux; arm64 variant v8

### `oraclelinux:7-slim` - linux; amd64

```console
$ docker pull oraclelinux@sha256:49cf6273163857f0aba4de7f9acc2741787ded076763604063dc5649eb2c652b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **48.3 MB (48328962 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:078d6e3ae75e90fe80b1993e78a67bd4c81f459d5e609ca006b901ee0e5cbdbe`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 28 Oct 2021 01:23:11 GMT
ADD file:1ba19f36d1d89d348cc182f0c7feb2c27bcca7bd084032525deaba8822462091 in / 
# Thu, 28 Oct 2021 01:23:11 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:a755c2d4aa362701b6bb477a4b1cb2594cb0dca6d0e6839e07b0636fb824c7f7`  
		Last Modified: Thu, 28 Oct 2021 01:24:42 GMT  
		Size: 48.3 MB (48328962 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `oraclelinux:7-slim` - linux; arm64 variant v8

```console
$ docker pull oraclelinux@sha256:30a3f914155907526d4296d05d15af6da1c659ae62d92733fbdcdfce5a2923a0
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **48.9 MB (48905370 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:bc0a178f7496cf09fb64c74156503addae49173647cfef5461cd1745d51ff087`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 28 Oct 2021 01:44:12 GMT
ADD file:f997c704e003e7bef1e40d457d005aa07a60cb4c37255cc2711119deb3e6df7d in / 
# Thu, 28 Oct 2021 01:44:13 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:9e1221f69ca9b4fd1daae21b9a20e7225f6ca7d0e34e9e998bc6600de1a3836a`  
		Last Modified: Thu, 28 Oct 2021 01:45:43 GMT  
		Size: 48.9 MB (48905370 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `oraclelinux:7.9`

```console
$ docker pull oraclelinux@sha256:cac0abe031f6e16723dd29a938f56be580425c911f08baf66ba27c476fbafac2
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 2
	-	linux; amd64
	-	linux; arm64 variant v8

### `oraclelinux:7.9` - linux; amd64

```console
$ docker pull oraclelinux@sha256:df65e2272a740ae8c2ef332eac8c89e119b58a7a66b3c78329d248ed206bb6c2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **93.4 MB (93377687 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f81087a7fe82adc5448998ab2584e80d32c6da5d53fa1b577df3cbef173f1657`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 03 Nov 2021 00:20:07 GMT
ADD file:8a8ac480a4f9d27fff7bca8bf39f55cba123d511a3cbc515e6b26cb63c68e1e9 in / 
# Wed, 03 Nov 2021 00:20:08 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:3405b13884e35cd21aab8d897677f670249eaba300a6fe90214fc58fb731fc13`  
		Last Modified: Wed, 03 Nov 2021 00:20:57 GMT  
		Size: 93.4 MB (93377687 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `oraclelinux:7.9` - linux; arm64 variant v8

```console
$ docker pull oraclelinux@sha256:8b9e629939647159a457da6321ed0808e439c4cf05e3155be68de405a3d1dc3e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **95.3 MB (95302262 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:fde0714d0fe344da227dc2eb7a69ef73e2d8aa0edbeeb46e8bc0b347c2ba4f87`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 03 Nov 2021 00:43:55 GMT
ADD file:b55daa2532dd757fc185d4ecb5a97f0d33e91976432a4405d5e917240a37353b in / 
# Wed, 03 Nov 2021 00:43:57 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:5507e3191aacc8487157a8b1a1dfb16f8c70148b3f13ebd0db0e919e5b3dd770`  
		Last Modified: Wed, 03 Nov 2021 00:44:37 GMT  
		Size: 95.3 MB (95302262 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `oraclelinux:8`

```console
$ docker pull oraclelinux@sha256:b81d5b0638bb67030b207d28586d0e714a811cc612396dbe3410db406998b3ad
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 2
	-	linux; amd64
	-	linux; arm64 variant v8

### `oraclelinux:8` - linux; amd64

```console
$ docker pull oraclelinux@sha256:ef0327c1a51e3471e9c2966b26b6245bd1f4c3f7c86d7edfb47a39adb446ceb5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **90.4 MB (90362360 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:97e22ab49eea70a5d500e00980537605d56f30f9614b3a6d6c4ae9ddbd642489`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 03 Nov 2021 22:19:59 GMT
ADD file:4ca8a2fd095830917f08d0d0d3f08103d722d2bd4fdbb975ed0ad1f8bb1e1a6c in / 
# Wed, 03 Nov 2021 22:20:00 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:a4df6f21af842935f0b80f5f255a88caf5f16b86e2642b468f83b8976666c3d7`  
		Last Modified: Wed, 03 Nov 2021 22:21:00 GMT  
		Size: 90.4 MB (90362360 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `oraclelinux:8` - linux; arm64 variant v8

```console
$ docker pull oraclelinux@sha256:55c4a95f5719f4145e410994e723dc2540e2ac331d6aaa620badd0a8cea8398a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **90.8 MB (90841443 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:034c11d3a5025875f8c916090306a77f37c9511f79ad8612b78db9b409a884cd`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 03 Nov 2021 22:43:59 GMT
ADD file:3fcd242a812c4453a85683f4b246623f31bb267cee4886b55a6d0167fd1de9aa in / 
# Wed, 03 Nov 2021 22:44:01 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:3525c9711691f9a6d450d6071231ecd3921b391dbff682498401473072552856`  
		Last Modified: Wed, 03 Nov 2021 22:44:54 GMT  
		Size: 90.8 MB (90841443 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `oraclelinux:8-slim`

```console
$ docker pull oraclelinux@sha256:67767ec8b1e55300c76aab633c9dad8bfcba939a5666750dc32fc9602a589c2d
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 2
	-	linux; amd64
	-	linux; arm64 variant v8

### `oraclelinux:8-slim` - linux; amd64

```console
$ docker pull oraclelinux@sha256:9a6a89a7a336b8f4c9ba5e0c66115626601eea7b371d65aeda7ea9182193b71e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **42.0 MB (41968115 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:87ea2daf719e9cff7c84a2d2cacd8144e05e67c4dd99271ba94c9f5b50eb985a`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 03 Nov 2021 22:20:09 GMT
ADD file:ee2c184d933cfe1848f54f94d92f5c14d073160b62ec403259b18392d4ec6e1b in / 
# Wed, 03 Nov 2021 22:20:10 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:0a6167eaa66c8f2dc1c8dbed1a335f3d4052409454c9f7fbcd8fc35d8576a7e2`  
		Last Modified: Wed, 03 Nov 2021 22:21:16 GMT  
		Size: 42.0 MB (41968115 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `oraclelinux:8-slim` - linux; arm64 variant v8

```console
$ docker pull oraclelinux@sha256:3ede3ab64e05a5cf474aaef64ffa910239fa464a22f968f10401ec9734931f4b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **41.9 MB (41879080 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:959feb05f35e66bc6244762bd29ebd4eacd7ae2bd73051e15fa89322049d3ee7`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 03 Nov 2021 22:44:10 GMT
ADD file:42d3c96053f453ca6f7155adc565cd822cd2663bd2a3862ccaabb9886c191116 in / 
# Wed, 03 Nov 2021 22:44:11 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:f6fe8bd1b591af20f7f1bf6cabe92846a2b5f1ba33ca5fd165cf03624558cd5c`  
		Last Modified: Wed, 03 Nov 2021 22:45:11 GMT  
		Size: 41.9 MB (41879080 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `oraclelinux:8.4`

```console
$ docker pull oraclelinux@sha256:b81d5b0638bb67030b207d28586d0e714a811cc612396dbe3410db406998b3ad
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 2
	-	linux; amd64
	-	linux; arm64 variant v8

### `oraclelinux:8.4` - linux; amd64

```console
$ docker pull oraclelinux@sha256:ef0327c1a51e3471e9c2966b26b6245bd1f4c3f7c86d7edfb47a39adb446ceb5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **90.4 MB (90362360 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:97e22ab49eea70a5d500e00980537605d56f30f9614b3a6d6c4ae9ddbd642489`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 03 Nov 2021 22:19:59 GMT
ADD file:4ca8a2fd095830917f08d0d0d3f08103d722d2bd4fdbb975ed0ad1f8bb1e1a6c in / 
# Wed, 03 Nov 2021 22:20:00 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:a4df6f21af842935f0b80f5f255a88caf5f16b86e2642b468f83b8976666c3d7`  
		Last Modified: Wed, 03 Nov 2021 22:21:00 GMT  
		Size: 90.4 MB (90362360 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `oraclelinux:8.4` - linux; arm64 variant v8

```console
$ docker pull oraclelinux@sha256:55c4a95f5719f4145e410994e723dc2540e2ac331d6aaa620badd0a8cea8398a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **90.8 MB (90841443 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:034c11d3a5025875f8c916090306a77f37c9511f79ad8612b78db9b409a884cd`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 03 Nov 2021 22:43:59 GMT
ADD file:3fcd242a812c4453a85683f4b246623f31bb267cee4886b55a6d0167fd1de9aa in / 
# Wed, 03 Nov 2021 22:44:01 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:3525c9711691f9a6d450d6071231ecd3921b391dbff682498401473072552856`  
		Last Modified: Wed, 03 Nov 2021 22:44:54 GMT  
		Size: 90.8 MB (90841443 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
