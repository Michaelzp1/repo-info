## `ubuntu:devel`

```console
$ docker pull ubuntu@sha256:203d16099ec9f0529f1fd6e7f33cef02fc637bfae1d9d5721f733e59d4d90bad
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 6
	-	linux; amd64
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; ppc64le
	-	linux; riscv64
	-	linux; s390x

### `ubuntu:devel` - linux; amd64

```console
$ docker pull ubuntu@sha256:b284173fbabcefd6cd4ef871e408d3252b24d762a997323377eaa208240fb72a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.4 MB (30399219 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6cd54db809ce7c0c4a5519431f18e583fc4571e433345ca7deceaa5a1f9fb6ec`
-	Default Command: `["bash"]`

```dockerfile
# Fri, 01 Oct 2021 02:24:06 GMT
ADD file:14abde46780e9aa8c8c5c54d89baf3f1913aa6fef0d44ecc320cf962c4ba9e4d in / 
# Fri, 01 Oct 2021 02:24:06 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6122078cc60fecb720ddc58a9f5be50b4112ba9f366d21eccaf834489eafdda5`  
		Last Modified: Fri, 01 Oct 2021 02:25:54 GMT  
		Size: 30.4 MB (30399219 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `ubuntu:devel` - linux; arm variant v7

```console
$ docker pull ubuntu@sha256:4f92d734b1b459411bf08e3d372425f9fe32778c5379be9b871dcb36445573c6
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **26.9 MB (26916992 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ce570a3a124ab71b3f2a4b517021b2537b380b0eb4cef867bb9d5db03478906f`
-	Default Command: `["bash"]`

```dockerfile
# Sat, 02 Oct 2021 05:59:55 GMT
ADD file:6ac61c944ca4bc7b903b037816e21005acf3338af57e5732f38b396acbbefef6 in / 
# Sat, 02 Oct 2021 05:59:56 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:85bc3e6b05f0f487b1f8903c51a8bf098a931ac723c2ff30726c9b4f42e7d8be`  
		Last Modified: Sat, 02 Oct 2021 06:04:07 GMT  
		Size: 26.9 MB (26916992 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `ubuntu:devel` - linux; arm64 variant v8

```console
$ docker pull ubuntu@sha256:55bce25aa903e56698b8ab74c96a9d4de4b152254bf766d2be6f48a02cd886f5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.0 MB (29047978 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:68655ee9aa431faa0081c84edc93a175c59b8f897440c69bd30e69087b91e9cc`
-	Default Command: `["bash"]`

```dockerfile
# Fri, 01 Oct 2021 02:44:16 GMT
ADD file:55c9fc8bcadd5ae136c0fd0e7bc6b97490ad9f02d9fe707ffda0ab5ba6208a63 in / 
# Fri, 01 Oct 2021 02:44:16 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:d6840eb3b14e719af003eb60b9616929459d959673d9a13237691e5be0928545`  
		Last Modified: Fri, 01 Oct 2021 02:46:30 GMT  
		Size: 29.0 MB (29047978 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `ubuntu:devel` - linux; ppc64le

```console
$ docker pull ubuntu@sha256:8642e5dd16db2fed5d428e9ff72e2b7d40f79731d5f5808eae35304bd8ddcbf3
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **36.2 MB (36159197 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:47e845699b1584965d538319e04a43dfa37b156826f125e50a6f3849557ddc0c`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 05 Oct 2021 11:08:44 GMT
ADD file:336f6c173990c33ed817c24e2640c594320911a456647acdd356ff4dfd6c2d3e in / 
# Tue, 05 Oct 2021 11:08:51 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:364d4546a8bf6fc0df00b576bfd4c074f52c0bd19474259ab96c4c6a58140d0f`  
		Last Modified: Tue, 05 Oct 2021 11:11:40 GMT  
		Size: 36.2 MB (36159197 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `ubuntu:devel` - linux; riscv64

```console
$ docker pull ubuntu@sha256:6d43cbf9a191da6a33efc5bdc327185ea8363d0ead8a07fbe8590d5a3bdde25c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **27.2 MB (27210170 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a3b7f6a349055717cf10ba89991eeded03ccaf3a92888ca35b7a84f56a798688`
-	Default Command: `["bash"]`

```dockerfile
# Fri, 01 Oct 2021 01:18:45 GMT
ADD file:e18a9f741d10c7c0a74f1cb7527ca233ce7d1b16b4b8ce47c97acf8d6a561bc6 in / 
# Fri, 01 Oct 2021 01:18:47 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:834bf9f8a8e81cd7ddae19382f75e69f5f13259008681b25aa70aca6da903e07`  
		Last Modified: Fri, 01 Oct 2021 01:42:59 GMT  
		Size: 27.2 MB (27210170 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `ubuntu:devel` - linux; s390x

```console
$ docker pull ubuntu@sha256:8370ebd1ad75d56999ebb386c9a3c9f3bc52e3e38a6096566a867d92bb6ebdf0
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.8 MB (30770041 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e4811400563f83422d383472f6ce188a30359e1e4b9534306ebf243f461cd2df`
-	Default Command: `["bash"]`

```dockerfile
# Fri, 01 Oct 2021 01:42:51 GMT
ADD file:d4436e6a424f51bbe486f3f4614b547c16e11f0b9e876ac7a4196bba85f65dff in / 
# Fri, 01 Oct 2021 01:42:53 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:51f9fa2a618c34c6f055c5337e5484b46ec926bf3b8ed93cbe89399bf17c4938`  
		Last Modified: Fri, 01 Oct 2021 01:44:30 GMT  
		Size: 30.8 MB (30770041 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
