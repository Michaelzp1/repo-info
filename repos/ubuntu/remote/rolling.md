## `ubuntu:rolling`

```console
$ docker pull ubuntu@sha256:f53c26896aaebc7727f3255c24e261b1b6f630a848a2e67c8cc5848d7c33f93f
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 6
	-	linux; amd64
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; ppc64le
	-	linux; riscv64
	-	linux; s390x

### `ubuntu:rolling` - linux; amd64

```console
$ docker pull ubuntu@sha256:36a55018c9a07cb08de538986a6d6e262216e4392a3884cce9231f4a168d473c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.4 MB (30391190 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f062f6f5a34de0cb5b2cec7709aa42fbb970f5846b9ff9d30e65d86f1d01f54e`
-	Default Command: `["bash"]`

```dockerfile
# Sat, 16 Oct 2021 00:37:58 GMT
ADD file:41d75787395224d025bcc0f7feca7c56fd4c1adea7cce667e2472fad282054fc in / 
# Sat, 16 Oct 2021 00:37:59 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:3910f09893e52b84e55ef361892610a5a78e9df154e23da9bd7e317a4f3518d5`  
		Last Modified: Sat, 16 Oct 2021 00:38:57 GMT  
		Size: 30.4 MB (30391190 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `ubuntu:rolling` - linux; arm variant v7

```console
$ docker pull ubuntu@sha256:b1f0161fd248d48bfb8c2d6b42666179d69d1c65cb9d8a5b3fefbe2078f4ff07
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **26.9 MB (26919116 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ef0cb1d7b0beecf47bf327824429f9e7ec4f97831c4738f1eba3b98758ee94fa`
-	Default Command: `["bash"]`

```dockerfile
# Sat, 16 Oct 2021 01:12:36 GMT
ADD file:200fc5e8340bed54c8a1f2d364d7eecb37212e03a3c7c5f8b9fe94a5689b34fc in / 
# Sat, 16 Oct 2021 01:12:37 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:47d179bbc8c38e5833d8d05d251749ab4997c8e937e8913717f1124bdd9f3c75`  
		Last Modified: Sat, 16 Oct 2021 01:15:40 GMT  
		Size: 26.9 MB (26919116 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `ubuntu:rolling` - linux; arm64 variant v8

```console
$ docker pull ubuntu@sha256:9e537eb3bea013df5644452cce045d23340caf16fe1e1f06249e1dfc5528266f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.0 MB (29042311 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f99cc3098237072c591e03bdbc6b1bb39cdc89133020953e760da5fc2971427b`
-	Default Command: `["bash"]`

```dockerfile
# Sat, 16 Oct 2021 01:48:00 GMT
ADD file:12f0ca9f9259de0f0a05250c3c108e96125ae9e13b868f0d2361add6c231bba4 in / 
# Sat, 16 Oct 2021 01:48:00 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:87227608fa0c8780400c9e08e962fb29c6c6326768ea08e296ee1df1fd96e922`  
		Last Modified: Sat, 16 Oct 2021 01:49:59 GMT  
		Size: 29.0 MB (29042311 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `ubuntu:rolling` - linux; ppc64le

```console
$ docker pull ubuntu@sha256:cc5b456b9e9dbb2fa062bb34e5c391516d26bdd7f4176dac74c7d23621651633
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **36.2 MB (36177739 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:999e63299fc5386d4b194dc03bd03df7d1aa727ff92c282031b678a2a8ac5371`
-	Default Command: `["bash"]`

```dockerfile
# Sat, 16 Oct 2021 00:37:08 GMT
ADD file:2882612be3480d0a26c565cfff723f6f7c913ecc1e3b982def388f5c29ad10e6 in / 
# Sat, 16 Oct 2021 00:37:12 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:4cfd5cbebb9f6bac4f6c4e8b0ed1450129f245085d7131094975207c3091fb66`  
		Last Modified: Sat, 16 Oct 2021 00:39:02 GMT  
		Size: 36.2 MB (36177739 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `ubuntu:rolling` - linux; riscv64

```console
$ docker pull ubuntu@sha256:c70ed25bff9d1b56eca8972150b85d3879092d56480ab5f5bc5f204e5bec703c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **27.2 MB (27208455 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6be87bfc5d9252e429ccb2c6e3ab5049cbf51e45488a904f5a3916f511265a87`
-	Default Command: `["bash"]`

```dockerfile
# Sat, 16 Oct 2021 00:17:48 GMT
ADD file:8cf59b4b38ccab14d48f2740546784966d4b20690dc57f9018241a62adac259e in / 
# Sat, 16 Oct 2021 00:17:50 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:19a9653dc9c1bf288e8c03d2027a0edd6609e7bc4c852b05e2ba35e947868701`  
		Last Modified: Sat, 16 Oct 2021 00:31:12 GMT  
		Size: 27.2 MB (27208455 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `ubuntu:rolling` - linux; s390x

```console
$ docker pull ubuntu@sha256:d2dfc9195b811280b3f1f74a321c8774f64dd5b78793a15909a3c32297025883
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.5 MB (30527974 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3c51644c19aadd7d203a74fa5658a4716f45be90493af6463caf5777c6df66af`
-	Default Command: `["bash"]`

```dockerfile
# Sat, 16 Oct 2021 00:42:02 GMT
ADD file:e3ac42c4b4650e7d57adf242fb9b7137898397121142c4fd47afb661024e0b00 in / 
# Sat, 16 Oct 2021 00:42:03 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:7101024e2e65688584ff7a7aa5f503fe9e8165ebcc5fb924b1304bbdd0d4256e`  
		Last Modified: Sat, 16 Oct 2021 00:43:14 GMT  
		Size: 30.5 MB (30527974 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
