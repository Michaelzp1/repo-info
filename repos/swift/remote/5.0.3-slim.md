## `swift:5.0.3-slim`

```console
$ docker pull swift@sha256:18ced509261f080d01f72774255a923a4b0a55597d31826558981b18f024bd0a
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `swift:5.0.3-slim` - linux; amd64

```console
$ docker pull swift@sha256:265f6eeaa47a8b7873e025f8eb822ce78349fa824135fef0f0dae376125619f4
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **75.1 MB (75148856 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8c90aef2cf5ed0ce256eee8370ff122dd8d7e4e435466c2fc068ec56d0d970ad`
-	Default Command: `["bash"]`

```dockerfile
# Fri, 01 Oct 2021 02:23:23 GMT
ADD file:0d82cd095966e8ee78b593cb47a352eec842edb7bd9d9468e8a70154522447d1 in / 
# Fri, 01 Oct 2021 02:23:24 GMT
CMD ["bash"]
# Fri, 01 Oct 2021 03:43:13 GMT
LABEL maintainer=Swift Infrastructure <swift-infrastructure@swift.org>
# Fri, 01 Oct 2021 03:43:13 GMT
LABEL Description=Docker Container for the Swift programming language
# Tue, 05 Oct 2021 20:35:05 GMT
RUN export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true && apt-get -q update &&     apt-get -q install -y     libatomic1     libbsd0     libcurl4     libxml2     tzdata     && rm -r /var/lib/apt/lists/*
# Tue, 05 Oct 2021 20:35:06 GMT
ARG SWIFT_PLATFORM=ubuntu18.04
# Tue, 05 Oct 2021 20:35:06 GMT
ARG SWIFT_BRANCH=swift-5.0.3-release
# Tue, 05 Oct 2021 20:35:06 GMT
ARG SWIFT_VERSION=swift-5.0.3-RELEASE
# Tue, 05 Oct 2021 20:35:06 GMT
ENV SWIFT_PLATFORM=ubuntu18.04 SWIFT_BRANCH=swift-5.0.3-release SWIFT_VERSION=swift-5.0.3-RELEASE
# Tue, 05 Oct 2021 20:38:35 GMT
RUN SWIFT_URL=https://swift.org/builds/$SWIFT_BRANCH/$(echo "$SWIFT_PLATFORM" | tr -d .)/$SWIFT_VERSION/$SWIFT_VERSION-$SWIFT_PLATFORM.tar.gz     && apt-get update     && apt-get install -y curl gpg     && curl -fSsL $SWIFT_URL -o swift.tar.gz     && curl -fSsL $SWIFT_URL.sig -o swift.tar.gz.sig     && export GNUPGHOME="$(mktemp -d)"     && set -e;         for key in           A62AE125BBBFBB96A6E042EC925CC1CCED3D1561         ; do           gpg --quiet --keyserver ha.pool.sks-keyservers.net --recv-keys "$key";         done     && gpg --batch --verify --quiet swift.tar.gz.sig swift.tar.gz     && tar -xzf swift.tar.gz --directory / --strip-components=1 $SWIFT_VERSION-$SWIFT_PLATFORM/usr/lib/swift/linux     && apt-get purge -y curl gpg     && apt-get -y autoremove     && rm -r /var/lib/apt/lists/*     && rm -r "$GNUPGHOME" swift.tar.gz.sig swift.tar.gz     && chmod -R o+r /usr/lib/swift
```

-	Layers:
	-	`sha256:284055322776031bac33723839acb0db2d063a525ba4fa1fd268a831c7553b26`  
		Last Modified: Fri, 01 Oct 2021 02:25:02 GMT  
		Size: 26.7 MB (26705075 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:5717e3c640c1a150f2c78980371a0e94de07191b12300fc1c467ff64189319bc`  
		Last Modified: Tue, 05 Oct 2021 20:42:06 GMT  
		Size: 20.5 MB (20544175 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:b3c5768bee8ceff27647fa5e99854c57d5af61b589d3cc13d37899cbc2326c53`  
		Last Modified: Tue, 05 Oct 2021 20:42:07 GMT  
		Size: 27.9 MB (27899606 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
