## `swift:5.3.3-focal`

```console
$ docker pull swift@sha256:4fb03befa49d34ed0e3bb351ceb895901ec3db87a8ff40aee4857a5d05f6f795
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `swift:5.3.3-focal` - linux; amd64

```console
$ docker pull swift@sha256:675ae5a9c7523d633f9b09185a04cd2f66f6d6bcfac035cfd23d61ddf385816a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **548.6 MB (548605177 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:7e26f50d554a97c92f23197b4cebf1a9b7c272da6889c5b7e269ca309a5c3d98`
-	Default Command: `["bash"]`

```dockerfile
# Sat, 16 Oct 2021 00:37:47 GMT
ADD file:5d68d27cc15a80653c93d3a0b262a28112d47a46326ff5fc2dfbf7fa3b9a0ce8 in / 
# Sat, 16 Oct 2021 00:37:47 GMT
CMD ["bash"]
# Thu, 28 Oct 2021 23:48:36 GMT
LABEL maintainer=Swift Infrastructure <swift-infrastructure@forums.swift.org>
# Thu, 28 Oct 2021 23:48:36 GMT
LABEL description=Docker Container for the Swift programming language
# Fri, 29 Oct 2021 00:14:30 GMT
RUN export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true && apt-get -q update &&     apt-get -q install -y     binutils     git     gnupg2     libc6-dev     libcurl4     libedit2     libgcc-9-dev     libpython2.7     libsqlite3-0     libstdc++-9-dev     libxml2     libz3-dev     pkg-config     tzdata     zlib1g-dev     && rm -r /var/lib/apt/lists/*
# Fri, 29 Oct 2021 00:14:32 GMT
ARG SWIFT_SIGNING_KEY=A62AE125BBBFBB96A6E042EC925CC1CCED3D1561
# Fri, 29 Oct 2021 00:14:32 GMT
ARG SWIFT_PLATFORM=ubuntu20.04
# Fri, 29 Oct 2021 00:14:32 GMT
ARG SWIFT_BRANCH=swift-5.3.3-release
# Fri, 29 Oct 2021 00:14:32 GMT
ARG SWIFT_VERSION=swift-5.3.3-RELEASE
# Fri, 29 Oct 2021 00:14:32 GMT
ARG SWIFT_WEBROOT=https://download.swift.org
# Fri, 29 Oct 2021 00:14:32 GMT
ENV SWIFT_SIGNING_KEY=A62AE125BBBFBB96A6E042EC925CC1CCED3D1561 SWIFT_PLATFORM=ubuntu20.04 SWIFT_BRANCH=swift-5.3.3-release SWIFT_VERSION=swift-5.3.3-RELEASE SWIFT_WEBROOT=https://download.swift.org
# Fri, 29 Oct 2021 00:15:07 GMT
RUN set -e;     SWIFT_WEBDIR="$SWIFT_WEBROOT/$SWIFT_BRANCH/$(echo $SWIFT_PLATFORM | tr -d .)"     && SWIFT_BIN_URL="$SWIFT_WEBDIR/$SWIFT_VERSION/$SWIFT_VERSION-$SWIFT_PLATFORM.tar.gz"     && SWIFT_SIG_URL="$SWIFT_BIN_URL.sig"     && export DEBIAN_FRONTEND=noninteractive     && apt-get -q update && apt-get -q install -y curl && rm -rf /var/lib/apt/lists/*     && export GNUPGHOME="$(mktemp -d)"     && curl -fsSL "$SWIFT_BIN_URL" -o swift.tar.gz "$SWIFT_SIG_URL" -o swift.tar.gz.sig     && gpg --batch --quiet --keyserver keyserver.ubuntu.com --recv-keys "$SWIFT_SIGNING_KEY"     && gpg --batch --verify swift.tar.gz.sig swift.tar.gz     && tar -xzf swift.tar.gz --directory / --strip-components=1     && chmod -R o+r /usr/lib/swift     && rm -rf "$GNUPGHOME" swift.tar.gz.sig swift.tar.gz     && apt-get purge --auto-remove -y curl
# Fri, 29 Oct 2021 00:15:12 GMT
RUN swift --version
```

-	Layers:
	-	`sha256:7b1a6ab2e44dbac178598dabe7cff59bd67233dba0b27e4fbd1f9d4b3c877a54`  
		Last Modified: Thu, 07 Oct 2021 23:44:23 GMT  
		Size: 28.6 MB (28567101 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:911c785b1c1eeabe11121a0b7e314483fa33d1aed5ccb3b262f882c1743d5b3d`  
		Last Modified: Fri, 29 Oct 2021 01:08:07 GMT  
		Size: 97.8 MB (97844457 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:278efc5c0a48118e58de3d1732bca45b83c21b582e7744db387007d57f255bee`  
		Last Modified: Fri, 29 Oct 2021 01:08:50 GMT  
		Size: 422.2 MB (422193619 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
