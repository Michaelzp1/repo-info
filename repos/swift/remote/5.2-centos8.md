## `swift:5.2-centos8`

```console
$ docker pull swift@sha256:a680729c4319e102717369d8767e325078dd324740aaca78f79ed41a1471a761
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `swift:5.2-centos8` - linux; amd64

```console
$ docker pull swift@sha256:8995008b1b9a01e34604f2372764f3279cbb2fbf811e1f9212e0a82bf434761c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **672.6 MB (672550276 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d27c724c0acba3966227239009e7357a0376f5029b8839789438f21b837f4df1`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:20:04 GMT
ADD file:805cb5e15fb6e0bb0326ca33fd2942e068863ce2a8491bb71522c652f31fb466 in / 
# Wed, 15 Sep 2021 18:20:04 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20210915
# Wed, 15 Sep 2021 18:20:05 GMT
CMD ["/bin/bash"]
# Thu, 28 Oct 2021 23:54:32 GMT
LABEL maintainer=Swift Infrastructure <swift-infrastructure@forums.swift.org>
# Thu, 28 Oct 2021 23:54:33 GMT
LABEL description=Docker Container for the Swift programming language
# Thu, 28 Oct 2021 23:54:42 GMT
RUN yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
# Fri, 29 Oct 2021 00:17:26 GMT
RUN yum install --enablerepo=powertools -y   binutils   gcc   git   glibc-static   libbsd-devel   libedit   libedit-devel   libicu-devel   libstdc++-static   pkg-config   python2   sqlite   zlib-devel
# Fri, 29 Oct 2021 00:17:28 GMT
RUN ln -s /usr/bin/python2 /usr/bin/python
# Fri, 29 Oct 2021 00:17:28 GMT
ARG SWIFT_SIGNING_KEY=A62AE125BBBFBB96A6E042EC925CC1CCED3D1561
# Fri, 29 Oct 2021 00:17:28 GMT
ARG SWIFT_PLATFORM=centos8
# Fri, 29 Oct 2021 00:26:30 GMT
ARG SWIFT_BRANCH=swift-5.2.5-release
# Fri, 29 Oct 2021 00:26:30 GMT
ARG SWIFT_VERSION=swift-5.2.5-RELEASE
# Fri, 29 Oct 2021 00:26:30 GMT
ARG SWIFT_WEBROOT=https://download.swift.org
# Fri, 29 Oct 2021 00:26:30 GMT
ENV SWIFT_SIGNING_KEY=A62AE125BBBFBB96A6E042EC925CC1CCED3D1561 SWIFT_PLATFORM=centos8 SWIFT_BRANCH=swift-5.2.5-release SWIFT_VERSION=swift-5.2.5-RELEASE SWIFT_WEBROOT=https://download.swift.org
# Fri, 29 Oct 2021 00:26:59 GMT
RUN set -e;     SWIFT_WEBDIR="$SWIFT_WEBROOT/$SWIFT_BRANCH/$(echo $SWIFT_PLATFORM | tr -d .)"     && SWIFT_BIN_URL="$SWIFT_WEBDIR/$SWIFT_VERSION/$SWIFT_VERSION-$SWIFT_PLATFORM.tar.gz"     && SWIFT_SIG_URL="$SWIFT_BIN_URL.sig"     && export GNUPGHOME="$(mktemp -d)"     && curl -fsSL "$SWIFT_BIN_URL" -o swift.tar.gz "$SWIFT_SIG_URL" -o swift.tar.gz.sig     && curl -fSsL https://swift.org/keys/release-key-swift-5.x.asc | gpg --import -     && gpg --batch --verify swift.tar.gz.sig swift.tar.gz     && tar -xzf swift.tar.gz --directory / --strip-components=1     && chmod -R o+r /usr/lib/swift     && rm -rf "$GNUPGHOME" swift.tar.gz.sig swift.tar.gz
# Fri, 29 Oct 2021 00:27:04 GMT
RUN swift --version
```

-	Layers:
	-	`sha256:a1d0c75327776413fa0db9ed3adcdbadedc95a662eb1d360dad82bb913f8a1d1`  
		Last Modified: Wed, 15 Sep 2021 18:21:25 GMT  
		Size: 83.5 MB (83518086 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:97208e04a9180f5341d539388dd884c661bf09323b947a392221c30229ec28a7`  
		Last Modified: Fri, 29 Oct 2021 00:50:05 GMT  
		Size: 31.5 MB (31496052 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:671aa14f1982cc61139cee899e526a1003a6a3c9a1f8f2bc8601952041815ba1`  
		Last Modified: Fri, 29 Oct 2021 01:10:59 GMT  
		Size: 169.2 MB (169206682 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:7570f2505a82b07ee29cb9b22c7b19a59fef027970ea924cb690223512bc654e`  
		Last Modified: Fri, 29 Oct 2021 01:10:36 GMT  
		Size: 153.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:aec4dd98fd96d2ef9cf893bd10166740daeaee2da3d6105702feb190dccc5661`  
		Last Modified: Fri, 29 Oct 2021 01:20:06 GMT  
		Size: 388.3 MB (388329303 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
