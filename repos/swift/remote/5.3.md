## `swift:5.3`

```console
$ docker pull swift@sha256:ecb6bfbe687db07a2f0f8a1355bcb8b3849ec9aab14b53212f6b913a440debba
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `swift:5.3` - linux; amd64

```console
$ docker pull swift@sha256:89ae4c378e710a5b8ce0d1b0a244713e3cc56b0b4ebdcb7ea7feae3b0decaf1f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **573.3 MB (573280558 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4a0d361299bdf1f9adaaad131702e8b8632b3989404ea639a848aeaff6bb4a02`
-	Default Command: `["bash"]`

```dockerfile
# Fri, 01 Oct 2021 02:23:23 GMT
ADD file:0d82cd095966e8ee78b593cb47a352eec842edb7bd9d9468e8a70154522447d1 in / 
# Fri, 01 Oct 2021 02:23:24 GMT
CMD ["bash"]
# Thu, 28 Oct 2021 23:39:56 GMT
LABEL maintainer=Swift Infrastructure <swift-infrastructure@forums.swift.org>
# Thu, 28 Oct 2021 23:39:57 GMT
LABEL Description=Docker Container for the Swift programming language
# Fri, 29 Oct 2021 00:10:03 GMT
RUN export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true && apt-get -q update &&     apt-get -q install -y     libatomic1     libcurl4     libxml2     libedit2     libsqlite3-0     libc6-dev     binutils     libgcc-5-dev     libstdc++-5-dev     zlib1g-dev     libpython2.7     tzdata     git     pkg-config     && rm -r /var/lib/apt/lists/*
# Fri, 29 Oct 2021 00:10:04 GMT
ARG SWIFT_SIGNING_KEY=A62AE125BBBFBB96A6E042EC925CC1CCED3D1561
# Fri, 29 Oct 2021 00:10:04 GMT
ARG SWIFT_PLATFORM=ubuntu18.04
# Fri, 29 Oct 2021 00:10:05 GMT
ARG SWIFT_BRANCH=swift-5.3.3-release
# Fri, 29 Oct 2021 00:10:05 GMT
ARG SWIFT_VERSION=swift-5.3.3-RELEASE
# Fri, 29 Oct 2021 00:10:05 GMT
ARG SWIFT_WEBROOT=https://download.swift.org
# Fri, 29 Oct 2021 00:10:05 GMT
ENV SWIFT_SIGNING_KEY=A62AE125BBBFBB96A6E042EC925CC1CCED3D1561 SWIFT_PLATFORM=ubuntu18.04 SWIFT_BRANCH=swift-5.3.3-release SWIFT_VERSION=swift-5.3.3-RELEASE SWIFT_WEBROOT=https://download.swift.org
# Fri, 29 Oct 2021 00:10:43 GMT
RUN set -e;     SWIFT_WEBDIR="$SWIFT_WEBROOT/$SWIFT_BRANCH/$(echo $SWIFT_PLATFORM | tr -d .)"     && SWIFT_BIN_URL="$SWIFT_WEBDIR/$SWIFT_VERSION/$SWIFT_VERSION-$SWIFT_PLATFORM.tar.gz"     && SWIFT_SIG_URL="$SWIFT_BIN_URL.sig"     && export DEBIAN_FRONTEND=noninteractive     && apt-get -q update && apt-get -q install -y curl && rm -rf /var/lib/apt/lists/*     && export GNUPGHOME="$(mktemp -d)"     && curl -fsSL "$SWIFT_BIN_URL" -o swift.tar.gz "$SWIFT_SIG_URL" -o swift.tar.gz.sig     && gpg --batch --quiet --keyserver keyserver.ubuntu.com --recv-keys "$SWIFT_SIGNING_KEY"     && gpg --batch --verify swift.tar.gz.sig swift.tar.gz     && tar -xzf swift.tar.gz --directory / --strip-components=1     && chmod -R o+r /usr/lib/swift     && rm -rf "$GNUPGHOME" swift.tar.gz.sig swift.tar.gz     && apt-get purge --auto-remove -y curl
# Fri, 29 Oct 2021 00:10:48 GMT
RUN swift --version
```

-	Layers:
	-	`sha256:284055322776031bac33723839acb0db2d063a525ba4fa1fd268a831c7553b26`  
		Last Modified: Fri, 01 Oct 2021 02:25:02 GMT  
		Size: 26.7 MB (26705075 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:7c59e8486899e2fe33ab2b41a3e12fb0daad39383514b00b4bdb870531d385fd`  
		Last Modified: Fri, 29 Oct 2021 01:04:55 GMT  
		Size: 124.1 MB (124073968 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:f64341b2671ccdf945a1612927caeb07fec715b218d43e58351a227be8392148`  
		Last Modified: Fri, 29 Oct 2021 01:05:34 GMT  
		Size: 422.5 MB (422501515 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
