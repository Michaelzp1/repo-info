## `swift:5.2.5-centos7`

```console
$ docker pull swift@sha256:e2c48523badb9b3e1810e7e836994104922e7222befac3cad1d7719322bcbfe7
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `swift:5.2.5-centos7` - linux; amd64

```console
$ docker pull swift@sha256:b9a5b212e3579e3b487a1529050e25b7b23c54f14ee92ce075d470c0255b5aae
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **584.1 MB (584101555 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cc86437c9a647fcf86dd61fa5254d9f3db70877cfa164101b03be537c94dc17c`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Wed, 15 Sep 2021 18:20:23 GMT
ADD file:b3ebbe8bd304723d43b7b44a6d990cd657b63d93d6a2a9293983a30bfc1dfa53 in / 
# Wed, 15 Sep 2021 18:20:23 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20201113 org.opencontainers.image.title=CentOS Base Image org.opencontainers.image.vendor=CentOS org.opencontainers.image.licenses=GPL-2.0-only org.opencontainers.image.created=2020-11-13 00:00:00+00:00
# Wed, 15 Sep 2021 18:20:23 GMT
CMD ["/bin/bash"]
# Thu, 28 Oct 2021 23:56:50 GMT
LABEL maintainer=Swift Infrastructure <swift-infrastructure@forums.swift.org>
# Thu, 28 Oct 2021 23:56:50 GMT
LABEL description=Docker Container for the Swift programming language
# Fri, 29 Oct 2021 00:19:46 GMT
RUN yum install shadow-utils.x86_64 -y   binutils   gcc   git   glibc-static   libbsd-devel   libedit   libedit-devel   libicu-devel   libstdc++-static   pkg-config   python2   sqlite   zlib-devel
# Fri, 29 Oct 2021 00:19:47 GMT
RUN sed -i -e 's/\*__block/\*__libc_block/g' /usr/include/unistd.h
# Fri, 29 Oct 2021 00:19:47 GMT
ARG SWIFT_SIGNING_KEY=A62AE125BBBFBB96A6E042EC925CC1CCED3D1561
# Fri, 29 Oct 2021 00:19:47 GMT
ARG SWIFT_PLATFORM=centos7
# Fri, 29 Oct 2021 00:27:33 GMT
ARG SWIFT_BRANCH=swift-5.2.5-release
# Fri, 29 Oct 2021 00:27:33 GMT
ARG SWIFT_VERSION=swift-5.2.5-RELEASE
# Fri, 29 Oct 2021 00:27:34 GMT
ARG SWIFT_WEBROOT=https://download.swift.org
# Fri, 29 Oct 2021 00:27:34 GMT
ENV SWIFT_SIGNING_KEY=A62AE125BBBFBB96A6E042EC925CC1CCED3D1561 SWIFT_PLATFORM=centos7 SWIFT_BRANCH=swift-5.2.5-release SWIFT_VERSION=swift-5.2.5-RELEASE SWIFT_WEBROOT=https://download.swift.org
# Fri, 29 Oct 2021 00:28:05 GMT
RUN set -e;     SWIFT_WEBDIR="$SWIFT_WEBROOT/$SWIFT_BRANCH/$(echo $SWIFT_PLATFORM | tr -d .)"     && SWIFT_BIN_URL="$SWIFT_WEBDIR/$SWIFT_VERSION/$SWIFT_VERSION-$SWIFT_PLATFORM.tar.gz"     && SWIFT_SIG_URL="$SWIFT_BIN_URL.sig"     && export GNUPGHOME="$(mktemp -d)"     && curl -fsSL "$SWIFT_BIN_URL" -o swift.tar.gz "$SWIFT_SIG_URL" -o swift.tar.gz.sig     && curl -fSsL https://swift.org/keys/release-key-swift-5.x.asc | gpg --import -     && gpg --batch --verify swift.tar.gz.sig swift.tar.gz     && tar -xzf swift.tar.gz --directory / --strip-components=1     && chmod -R o+r /usr/lib/swift     && rm -rf "$GNUPGHOME" swift.tar.gz.sig swift.tar.gz
# Fri, 29 Oct 2021 00:28:09 GMT
RUN swift --version
```

-	Layers:
	-	`sha256:2d473b07cdd5f0912cd6f1a703352c82b512407db6b05b43f2553732b55df3bc`  
		Last Modified: Sat, 14 Nov 2020 00:21:39 GMT  
		Size: 76.1 MB (76097157 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:21f363b60ba8cb1acb5d7e8a778514ff460702ed46300bc4e6ea30f734622ece`  
		Last Modified: Fri, 29 Oct 2021 01:12:25 GMT  
		Size: 125.4 MB (125384290 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:d8c0c1caa2327d4aba744b6cb676a30e944243c680e64c642711a00d3b1657a0`  
		Last Modified: Fri, 29 Oct 2021 01:12:06 GMT  
		Size: 11.5 KB (11502 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:16ea46e27d0bf83812c9a875d7ace9783fd6e263801c06708d352f3bf1d1dbb5`  
		Last Modified: Fri, 29 Oct 2021 01:21:25 GMT  
		Size: 382.6 MB (382608606 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
