# `swift:5.4.3-centos8`

## Docker Metadata

- Image ID: `sha256:73aab7a36bb6f1601b97043e11542eb3b0bd5b1c2bb086262bd8b9c0235f1a43`
- Created: `2021-10-29T00:07:05.466527686Z`
- Virtual Size: ~ 2.32 Gb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["/bin/bash"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `SWIFT_SIGNING_KEY=A62AE125BBBFBB96A6E042EC925CC1CCED3D1561`
  - `SWIFT_PLATFORM=centos8`
  - `SWIFT_BRANCH=swift-5.4.3-release`
  - `SWIFT_VERSION=swift-5.4.3-RELEASE`
  - `SWIFT_WEBROOT=https://download.swift.org`
- Labels:
  - `description=Docker Container for the Swift programming language`
  - `maintainer=Swift Infrastructure <swift-infrastructure@forums.swift.org>`
  - `org.label-schema.build-date=20210915`
  - `org.label-schema.license=GPLv2`
  - `org.label-schema.name=CentOS Base Image`
  - `org.label-schema.schema-version=1.0`
  - `org.label-schema.vendor=CentOS`
