# `swift:5.3.3-centos8`

## Docker Metadata

- Image ID: `sha256:f8a8428bd164bed916b3c6befe00ab164957e009dcf513f9b32fe91ab092cfa4`
- Created: `2021-10-29T00:18:13.161065311Z`
- Virtual Size: ~ 2.02 Gb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["/bin/bash"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `SWIFT_SIGNING_KEY=A62AE125BBBFBB96A6E042EC925CC1CCED3D1561`
  - `SWIFT_PLATFORM=centos8`
  - `SWIFT_BRANCH=swift-5.3.3-release`
  - `SWIFT_VERSION=swift-5.3.3-RELEASE`
  - `SWIFT_WEBROOT=https://download.swift.org`
- Labels:
  - `description=Docker Container for the Swift programming language`
  - `maintainer=Swift Infrastructure <swift-infrastructure@forums.swift.org>`
  - `org.label-schema.build-date=20210915`
  - `org.label-schema.license=GPLv2`
  - `org.label-schema.name=CentOS Base Image`
  - `org.label-schema.schema-version=1.0`
  - `org.label-schema.vendor=CentOS`
