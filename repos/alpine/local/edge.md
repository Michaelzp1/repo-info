# `alpine:20210804`

## Docker Metadata

- Image ID: `sha256:4e873038b87b454ce65890597a96fa12772164123b4d266f1278366241eca60d`
- Created: `2021-08-04T21:19:39.905977401Z`
- Virtual Size: ~ 5.59 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["/bin/sh"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`

## `apk` (`.apk`-based packages)

### `apk` package: `alpine-baselayout`

```console
alpine-baselayout-3.2.0-r17 description:
Alpine base dir structure and init scripts

alpine-baselayout-3.2.0-r17 webpage:
https://git.alpinelinux.org/cgit/aports/tree/main/alpine-baselayout

alpine-baselayout-3.2.0-r17 installed size:
404 KiB

alpine-baselayout-3.2.0-r17 license:
GPL-2.0-only

```

### `apk` package: `alpine-keys`

```console
alpine-keys-2.3-r1 description:
Public keys for Alpine Linux packages

alpine-keys-2.3-r1 webpage:
https://alpinelinux.org

alpine-keys-2.3-r1 installed size:
116 KiB

alpine-keys-2.3-r1 license:
MIT

```

### `apk` package: `apk-tools`

```console
apk-tools-2.12.7-r0 description:
Alpine Package Keeper - package manager for alpine

apk-tools-2.12.7-r0 webpage:
https://gitlab.alpinelinux.org/alpine/apk-tools

apk-tools-2.12.7-r0 installed size:
304 KiB

apk-tools-2.12.7-r0 license:
GPL-2.0-only

```

### `apk` package: `busybox`

```console
busybox-1.33.1-r4 description:
Size optimized toolbox of many common UNIX utilities

busybox-1.33.1-r4 webpage:
https://busybox.net/

busybox-1.33.1-r4 installed size:
928 KiB

busybox-1.33.1-r4 license:
GPL-2.0-only

```

### `apk` package: `ca-certificates-bundle`

```console
ca-certificates-bundle-20191127-r5 description:
Pre generated bundle of Mozilla certificates

ca-certificates-bundle-20191127-r5 webpage:
https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/

ca-certificates-bundle-20191127-r5 installed size:
228 KiB

ca-certificates-bundle-20191127-r5 license:
MPL-2.0 AND MIT

```

### `apk` package: `libc-utils`

```console
libc-utils-0.7.2-r3 description:
Meta package to pull in correct libc

libc-utils-0.7.2-r3 webpage:
https://alpinelinux.org

libc-utils-0.7.2-r3 installed size:
4096 B

libc-utils-0.7.2-r3 license:
BSD-2-Clause AND BSD-3-Clause

```

### `apk` package: `libcrypto1.1`

```console
libcrypto1.1-1.1.1k-r1 description:
Crypto library from openssl

libcrypto1.1-1.1.1k-r1 webpage:
https://www.openssl.org/

libcrypto1.1-1.1.1k-r1 installed size:
2696 KiB

libcrypto1.1-1.1.1k-r1 license:
OpenSSL

```

### `apk` package: `libretls`

```console
libretls-3.3.3p1-r0 description:
port of libtls from libressl to openssl

libretls-3.3.3p1-r0 webpage:
https://git.causal.agency/libretls/

libretls-3.3.3p1-r0 installed size:
84 KiB

libretls-3.3.3p1-r0 license:
ISC AND (BSD-3-Clause OR MIT)

```

### `apk` package: `libssl1.1`

```console
libssl1.1-1.1.1k-r1 description:
SSL shared libraries

libssl1.1-1.1.1k-r1 webpage:
https://www.openssl.org/

libssl1.1-1.1.1k-r1 installed size:
532 KiB

libssl1.1-1.1.1k-r1 license:
OpenSSL

```

### `apk` package: `musl`

```console
musl-1.2.2-r5 description:
the musl c library (libc) implementation

musl-1.2.2-r5 webpage:
https://musl.libc.org/

musl-1.2.2-r5 installed size:
608 KiB

musl-1.2.2-r5 license:
MIT

```

### `apk` package: `musl-utils`

```console
musl-utils-1.2.2-r5 description:
the musl c library (libc) implementation

musl-utils-1.2.2-r5 webpage:
https://musl.libc.org/

musl-utils-1.2.2-r5 installed size:
140 KiB

musl-utils-1.2.2-r5 license:
MIT BSD GPL2+

```

### `apk` package: `scanelf`

```console
scanelf-1.3.3-r0 description:
Scan ELF binaries for stuff

scanelf-1.3.3-r0 webpage:
https://wiki.gentoo.org/wiki/Hardened/PaX_Utilities

scanelf-1.3.3-r0 installed size:
92 KiB

scanelf-1.3.3-r0 license:
GPL-2.0-only

```

### `apk` package: `ssl_client`

```console
ssl_client-1.33.1-r4 description:
EXternal ssl_client for busybox wget

ssl_client-1.33.1-r4 webpage:
https://busybox.net/

ssl_client-1.33.1-r4 installed size:
28 KiB

ssl_client-1.33.1-r4 license:
GPL-2.0-only

```

### `apk` package: `zlib`

```console
zlib-1.2.11-r3 description:
A compression/decompression Library

zlib-1.2.11-r3 webpage:
https://zlib.net/

zlib-1.2.11-r3 installed size:
108 KiB

zlib-1.2.11-r3 license:
Zlib

```
