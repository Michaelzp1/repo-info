<!-- THIS FILE IS GENERATED VIA './update-remote.sh' -->

# Tags of `alpine`

-	[`alpine:20210804`](#alpine20210804)
-	[`alpine:3`](#alpine3)
-	[`alpine:3.11`](#alpine311)
-	[`alpine:3.11.12`](#alpine31112)
-	[`alpine:3.12`](#alpine312)
-	[`alpine:3.12.8`](#alpine3128)
-	[`alpine:3.13`](#alpine313)
-	[`alpine:3.13.6`](#alpine3136)
-	[`alpine:3.14`](#alpine314)
-	[`alpine:3.14.2`](#alpine3142)
-	[`alpine:edge`](#alpineedge)
-	[`alpine:latest`](#alpinelatest)

## `alpine:20210804`

```console
$ docker pull alpine@sha256:e64a0b2fc7ff870c2b22506009288daa5134da2b8c541440694b629fc22d792e
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v6
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; ppc64le
	-	linux; riscv64
	-	linux; s390x

### `alpine:20210804` - linux; amd64

```console
$ docker pull alpine@sha256:0119f88f395766eb52f9b817c3d23576bf31935dc8e94abe14bae9a083ce4639
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2809935 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4e873038b87b454ce65890597a96fa12772164123b4d266f1278366241eca60d`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 04 Aug 2021 21:19:39 GMT
ADD file:9d64c3b3b8afed5f8c49f3fd84c6a4d5f244a48c3a6e714f0f9e3f188e36dfec in / 
# Wed, 04 Aug 2021 21:19:39 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:41dcc117e123342ad8ade3073ce46b505baa04b6a19e2fed7758f086a102fa7b`  
		Last Modified: Wed, 04 Aug 2021 21:20:10 GMT  
		Size: 2.8 MB (2809935 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:20210804` - linux; arm variant v6

```console
$ docker pull alpine@sha256:fe9c17d22bcf0f1042f87b9a8f0dc4f0f83a0183a9d418d74175091fc57f0b48
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2624432 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:2b32c399cd9e6d58d593d4a08638e845c83f51ae68b40b763b3d4cce84e5c473`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Mon, 30 Aug 2021 18:37:05 GMT
ADD file:a040ef45d7cfe61fabcf624a7b533e28d8417884dab9987d30ffda8dbdb19ed9 in / 
# Mon, 30 Aug 2021 18:37:06 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:af2daa8728d7e61dcf122793a25e9635fc0d31604ea5435fd249266af31d85e6`  
		Last Modified: Wed, 04 Aug 2021 21:51:01 GMT  
		Size: 2.6 MB (2624432 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:20210804` - linux; arm variant v7

```console
$ docker pull alpine@sha256:7e18acf944b0654583187a683ad5d0ac64495850600ea7db40137f4e33d34983
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.4 MB (2427357 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:bc81d48a660bb2243625e40bf35b7516c17fe6868ce7bd5e695c9a98f5ba2c07`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 04 Aug 2021 21:57:24 GMT
ADD file:c2215bda0db2fb3720aa9795bf5a1346e9d873d8016b50770b0963e7fce527f9 in / 
# Wed, 04 Aug 2021 21:57:25 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:1094e2da180bc33d5acca46b8b96b7d5d6db6a5e9943441c3cd3187ac2d37d34`  
		Last Modified: Wed, 04 Aug 2021 21:58:56 GMT  
		Size: 2.4 MB (2427357 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:20210804` - linux; arm64 variant v8

```console
$ docker pull alpine@sha256:e69f011cd0c679fb71015c1276f49cd474ed8992b9d724d448250fdcd7501bc5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.7 MB (2710192 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1e093ce69acfe14d25fc33c1fc4026f78a292770ff22fba01b5bda8068634605`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 04 Aug 2021 21:39:21 GMT
ADD file:7081d91e1ac988e9b96ab189dbd20209b0ad9ee550372591ddbb733600e94f42 in / 
# Wed, 04 Aug 2021 21:39:21 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:863239114e4bd9eef9cdf736b57a7f9f349e18280a9d771cf0780288000cb0f9`  
		Last Modified: Wed, 04 Aug 2021 21:40:08 GMT  
		Size: 2.7 MB (2710192 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:20210804` - linux; 386

```console
$ docker pull alpine@sha256:8fb23b62707b60d1872c7bb1516bc16a7f9f261ace57255b46675093b25b264a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2819706 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:99765b8e6b425318173f8adfad6fe274a9991d11547cf6a84a988fed398a1cff`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 04 Aug 2021 21:38:23 GMT
ADD file:b807a944822e2b1f892ca140b74b3e0e97e14fdb03fcc8e5c2c3fecf96606422 in / 
# Wed, 04 Aug 2021 21:38:23 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:0ad37d1d34cd8b97a849f75b389b29058dd85bd8be125ffbdc4dc2cd4ebd9ab2`  
		Last Modified: Wed, 04 Aug 2021 21:39:13 GMT  
		Size: 2.8 MB (2819706 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:20210804` - linux; ppc64le

```console
$ docker pull alpine@sha256:6029416056a6546f890e6d023228b971e593ecae86be1cae9ab593eb11c7937a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2810679 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:39f926c7bfaf9bf8cf3e4d1d114f91584c18b8e207dd0daa1b427d1cf299691e`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 04 Aug 2021 21:23:04 GMT
ADD file:a367f58774ad9846b4cf7a1cf3f9ee298ff3d91cd8e98861548bd4ad00b088d0 in / 
# Wed, 04 Aug 2021 21:23:07 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:7d12250a58c05855b73036bfab1a9fdfe22d460ed011b345f447619421f63b0e`  
		Last Modified: Wed, 04 Aug 2021 21:24:00 GMT  
		Size: 2.8 MB (2810679 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:20210804` - linux; riscv64

```console
$ docker pull alpine@sha256:2387fa34569bac5a2745bbe536105834be0c5a72be9d0fa1c793403672c220a2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.4 MB (2396116 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6bb9c1ea8da2626a9292ec9e6fffb5246e26c766aeda2656754eb13b47a54e65`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Thu, 05 Aug 2021 00:14:52 GMT
ADD file:ff7ce713e31a4e63191a849a00344bcf4cbe6a815579022c77da801e253ee79b in / 
# Thu, 05 Aug 2021 00:14:53 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:9fc107b906f212bfd9a75969f6d44bba2db3d0e8cdb53cf311470edc5e66b825`  
		Last Modified: Thu, 05 Aug 2021 00:18:00 GMT  
		Size: 2.4 MB (2396116 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:20210804` - linux; s390x

```console
$ docker pull alpine@sha256:0b21b0bf11bcd22f3ef496f0eb95cdd8b05a1489a3ed67e3d0225fe5b2462e25
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2598383 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:bfdf0d35838c8f10bd529ba8150eb8b3d7c19b3e0a5ed064fd3645c2e68b29aa`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 04 Aug 2021 21:41:28 GMT
ADD file:f5fe47ec03c849b6bc8e0a5768cb7a93ffe09dba1b2a30d98525c80a9565ae94 in / 
# Wed, 04 Aug 2021 21:41:29 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:bb9c72f8d440b8ccd27b4a03966b11ce4bc833a1dcda38fe38e3cacc701554ac`  
		Last Modified: Wed, 04 Aug 2021 21:42:39 GMT  
		Size: 2.6 MB (2598383 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `alpine:3`

```console
$ docker pull alpine@sha256:e1c082e3d3c45cccac829840a25941e679c25d438cc8412c2fa221cf1a824e6a
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 7
	-	linux; amd64
	-	linux; arm variant v6
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; ppc64le
	-	linux; s390x

### `alpine:3` - linux; amd64

```console
$ docker pull alpine@sha256:69704ef328d05a9f806b6b8502915e6a0a4faa4d72018dc42343f511490daf8a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2814446 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:14119a10abf4669e8cdbdff324a9f9605d99697215a0d21c360fe8dfa8471bab`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 17:19:45 GMT
ADD file:aad4290d27580cc1a094ffaf98c3ca2fc5d699fe695dfb8e6e9fac20f1129450 in / 
# Fri, 27 Aug 2021 17:19:45 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:a0d0a0d46f8b52473982a3c466318f479767577551a53ffc9074c9fa7035982e`  
		Last Modified: Fri, 27 Aug 2021 17:20:13 GMT  
		Size: 2.8 MB (2814446 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3` - linux; arm variant v6

```console
$ docker pull alpine@sha256:18c29393a090ba5cde8a5f00926e9e419f47cfcfd206cc3f7f590e91b19adfe9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2627447 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:651b237e41d5a4f13081d1def5aee149bfe130e33cc4834bca7c8554b7f4a21a`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 17:49:29 GMT
ADD file:1c1c4520d49cb6e8f795f3b953d1ed3c3c77868b98b53a455169c254fcec5acd in / 
# Fri, 27 Aug 2021 17:49:30 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:2e78c0f86ba9a1fed30df20cb48c9cc73e9626399f12749d36b892ff99c0ecf5`  
		Last Modified: Fri, 27 Aug 2021 17:50:55 GMT  
		Size: 2.6 MB (2627447 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3` - linux; arm variant v7

```console
$ docker pull alpine@sha256:e12ff876f0075740ed3d7bdf788107ae84c1b3dd6dc98b3baea41088aba5236f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.4 MB (2430419 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3e8172af00ceeca661953e4f3ce408bc705028e4e673db4332e9e8587614e6c2`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 17:57:31 GMT
ADD file:a7da7992ccea54d3295231027614f138aa45c4d4a25ea6ec9bf7b316b9f67d95 in / 
# Fri, 27 Aug 2021 17:57:32 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:a14774a5a62e0f454febaec7cb603e18a6a8e25ef9da4a4da85b155bdd5e5a7a`  
		Last Modified: Fri, 27 Aug 2021 17:59:00 GMT  
		Size: 2.4 MB (2430419 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3` - linux; arm64 variant v8

```console
$ docker pull alpine@sha256:b06a5cf61b2956088722c4f1b9a6f71dfe95f0b1fe285d44195452b8a1627de7
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.7 MB (2711827 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:bb3de5531c18f185667b0be0e400ab24aa40f4440093de82baf4072e14af3b84`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 17:39:33 GMT
ADD file:dc8af9c7bfe9f9541e1db38dea01c4201609f3075b31e108f2073ffed8c5e4b9 in / 
# Fri, 27 Aug 2021 17:39:33 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:552d1f2373af9bfe12033568ebbfb0ccbb0de11279f9a415a29207e264d7f4d9`  
		Last Modified: Fri, 27 Aug 2021 17:40:18 GMT  
		Size: 2.7 MB (2711827 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3` - linux; 386

```console
$ docker pull alpine@sha256:a77adef9f69751add61080617e15e67aba9aa7a5fd5414b9fae84143210ee0ad
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2822857 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:85f8acd1af44479b7ed08b5e2528c45f6fe1e7ac64b111ba8723548e1566ff57`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 17:38:29 GMT
ADD file:42a7bc5a08b546b47049dd0f89ae4e7a8c86342f87000f39ade3ff52916a6c2e in / 
# Fri, 27 Aug 2021 17:38:30 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:b11ae9fc5d8a106cfed3a6f6c75e5b5b5797c566fac4411622b4055df6541286`  
		Last Modified: Fri, 27 Aug 2021 17:39:18 GMT  
		Size: 2.8 MB (2822857 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3` - linux; ppc64le

```console
$ docker pull alpine@sha256:9bea59997a84beb47a8cc7ddb11abc957b141e8160852aa93b4cf60659016b53
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2812284 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6bc661e735b72b62d45b189445b5eca79b8d149fe5ae01b0985dc8edce3e49c0`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 19:39:54 GMT
ADD file:d213c56ffc24a5051e8060fd0fec1a0520367c10d88ab16321c36336b6c66098 in / 
# Fri, 27 Aug 2021 19:39:59 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:63da8ca98f7b4b94381aed56862a60aecf355d9428b9aeb7c61d5bd017100c18`  
		Last Modified: Fri, 27 Aug 2021 19:41:06 GMT  
		Size: 2.8 MB (2812284 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3` - linux; s390x

```console
$ docker pull alpine@sha256:e841d0f0881ea22080e84088337646ada15871abbc3ce19b3219e8fc2cb0cc22
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2603464 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:90ee2af91bcf25a281ce4f0513d1828671b37e7c1eea1a42f5626a54087df9fd`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 17:41:29 GMT
ADD file:9b40ee281e8797067fb2ae207c406084cb81593090338a8b7cb09ade52168daa in / 
# Fri, 27 Aug 2021 17:41:30 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:da14cb6b6dc946dbb2d84386bcaca84e2d46f650767cd11bdb3331ec9d623988`  
		Last Modified: Fri, 27 Aug 2021 17:42:25 GMT  
		Size: 2.6 MB (2603464 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `alpine:3.11`

```console
$ docker pull alpine@sha256:e457c945f4be075a9e5365c6628e552fcd15551f0bc1c1fc2ea0f2227a524210
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 7
	-	linux; amd64
	-	linux; arm variant v6
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; ppc64le
	-	linux; s390x

### `alpine:3.11` - linux; amd64

```console
$ docker pull alpine@sha256:c3d45491770c51da4ef58318e3714da686bc7165338b7ab5ac758e75c7455efb
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2817307 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d2bcc698e2fc63aec63bd1593bb8b4d8d0cbd5be76e3224b87678bab23d50265`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Tue, 31 Aug 2021 23:18:31 GMT
ADD file:9d14b11183983923090d9e6d15cc51ee210466296e913bfefbfd580b3de59c95 in / 
# Tue, 31 Aug 2021 23:18:31 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:6a428f9f83b0a29f1fdd2ccccca19a9bab805a925b8eddf432a5a3d3da04afbc`  
		Last Modified: Tue, 31 Aug 2021 23:19:15 GMT  
		Size: 2.8 MB (2817307 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.11` - linux; arm variant v6

```console
$ docker pull alpine@sha256:86d55ea0abdcc98ed0a3652510ff7fda8f36aefcd49aa067a9cc2c817d05f1d8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2623044 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d7818f0f8acfe973ae22435e06884cd6219d34252487eec50b4b42387acac64f`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Tue, 31 Aug 2021 22:30:57 GMT
ADD file:3e83d6b5df3a951968e475c7326baf5ef90a22f04163693db34f3b4fc5812434 in / 
# Tue, 31 Aug 2021 22:30:57 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:7fe987b00bcb1f14c5b65f89813475143c021e2f5c156705ac3525abe1b397a1`  
		Last Modified: Tue, 31 Aug 2021 22:32:38 GMT  
		Size: 2.6 MB (2623044 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.11` - linux; arm variant v7

```console
$ docker pull alpine@sha256:bc3a56cf7fea030fbf230540ce724c2e5f81434eb2903910c31e7be3918f3890
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.4 MB (2426002 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:20eab2c45d3b11729b17dd4a9c95604c4f5e4cf2bf93d4f373394b41906d5ce9`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 01:27:25 GMT
ADD file:8c75cb63bbe2eaaa2143faab71b60e4fca1773613a67dd81fbad93eaf133e208 in / 
# Wed, 01 Sep 2021 01:27:25 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:083a2a44b72d61c367ed39325938c3520170b91cdda058a00b72d8323618aabd`  
		Last Modified: Wed, 01 Sep 2021 01:29:05 GMT  
		Size: 2.4 MB (2426002 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.11` - linux; arm64 variant v8

```console
$ docker pull alpine@sha256:0a3b174523eafed2b1276cf957020850fc5305ed4605750d0b88f79d4900465d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.7 MB (2728407 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:282ca20f6f11befc379c9c312eba8fa622ff2a246b22b63dc7a9bf234ff830d6`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 02:50:59 GMT
ADD file:da6c0ac7cb9f819998546d88fb489b746004eb2ad6da64a39210696ef0e66e54 in / 
# Wed, 01 Sep 2021 02:50:59 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:07d756952c5cd45726cf9e8a292a3e05ca67eee5da176df7d632be8c5bb0ad04`  
		Last Modified: Wed, 01 Sep 2021 02:52:00 GMT  
		Size: 2.7 MB (2728407 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.11` - linux; 386

```console
$ docker pull alpine@sha256:8a6f8667a8eb87453662f3799ea3768b4c3653a66486ee7c1dd1bed0dfe8c5e7
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2813860 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:dffa084855e3c4dfa859eb62c26e208a5bbab235b47feab2859bbb730858a181`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Tue, 31 Aug 2021 21:23:45 GMT
ADD file:cb3b0c8081ccb41fa220b40134af176a4f81554954edd43158061fc1759f0ffd in / 
# Tue, 31 Aug 2021 21:23:45 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:a668cd93fc6ddc8cef33c9cd91e0ebd2d97f164977839d5ebbd25812f63b164f`  
		Last Modified: Tue, 31 Aug 2021 21:24:52 GMT  
		Size: 2.8 MB (2813860 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.11` - linux; ppc64le

```console
$ docker pull alpine@sha256:024cea07734079c8314b158b62d95aee704fad739f90cc9a591219a07df45241
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2824904 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8e8a9c591b6e4cfb81c674148973d61329c69a45113429a2fc2d3e331e3699b9`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 02:43:12 GMT
ADD file:17477eee6878a950f2f16286dba9067b4f27ab5d90c29157e30d9bcaee1d9418 in / 
# Wed, 01 Sep 2021 02:43:15 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:6ff762d8ea8ca25e11609614eeba7af16b71db5d8ebea8bc7b802c275174f2ee`  
		Last Modified: Wed, 01 Sep 2021 02:44:15 GMT  
		Size: 2.8 MB (2824904 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.11` - linux; s390x

```console
$ docker pull alpine@sha256:5f82c1806d60592b747d6a53d35164b0c8f40e7ccf5425c1d59ae86ec8b3c0bc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2585711 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:45fcff441288bb31228e19a6cad26e26a454e5d5484f1f7115660517bd48cb29`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 01:15:38 GMT
ADD file:c1038db2fb356dc0acf4108117c20ba1da2a1546023e16a0905f49a968d7a1c7 in / 
# Wed, 01 Sep 2021 01:15:39 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:518c147bd44d1caf10e5031a373df0ed439dcb012b636012aa159436f07e1ade`  
		Last Modified: Wed, 01 Sep 2021 01:16:37 GMT  
		Size: 2.6 MB (2585711 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `alpine:3.11.12`

```console
$ docker pull alpine@sha256:e457c945f4be075a9e5365c6628e552fcd15551f0bc1c1fc2ea0f2227a524210
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 7
	-	linux; amd64
	-	linux; arm variant v6
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; ppc64le
	-	linux; s390x

### `alpine:3.11.12` - linux; amd64

```console
$ docker pull alpine@sha256:c3d45491770c51da4ef58318e3714da686bc7165338b7ab5ac758e75c7455efb
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2817307 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d2bcc698e2fc63aec63bd1593bb8b4d8d0cbd5be76e3224b87678bab23d50265`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Tue, 31 Aug 2021 23:18:31 GMT
ADD file:9d14b11183983923090d9e6d15cc51ee210466296e913bfefbfd580b3de59c95 in / 
# Tue, 31 Aug 2021 23:18:31 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:6a428f9f83b0a29f1fdd2ccccca19a9bab805a925b8eddf432a5a3d3da04afbc`  
		Last Modified: Tue, 31 Aug 2021 23:19:15 GMT  
		Size: 2.8 MB (2817307 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.11.12` - linux; arm variant v6

```console
$ docker pull alpine@sha256:86d55ea0abdcc98ed0a3652510ff7fda8f36aefcd49aa067a9cc2c817d05f1d8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2623044 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d7818f0f8acfe973ae22435e06884cd6219d34252487eec50b4b42387acac64f`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Tue, 31 Aug 2021 22:30:57 GMT
ADD file:3e83d6b5df3a951968e475c7326baf5ef90a22f04163693db34f3b4fc5812434 in / 
# Tue, 31 Aug 2021 22:30:57 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:7fe987b00bcb1f14c5b65f89813475143c021e2f5c156705ac3525abe1b397a1`  
		Last Modified: Tue, 31 Aug 2021 22:32:38 GMT  
		Size: 2.6 MB (2623044 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.11.12` - linux; arm variant v7

```console
$ docker pull alpine@sha256:bc3a56cf7fea030fbf230540ce724c2e5f81434eb2903910c31e7be3918f3890
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.4 MB (2426002 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:20eab2c45d3b11729b17dd4a9c95604c4f5e4cf2bf93d4f373394b41906d5ce9`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 01:27:25 GMT
ADD file:8c75cb63bbe2eaaa2143faab71b60e4fca1773613a67dd81fbad93eaf133e208 in / 
# Wed, 01 Sep 2021 01:27:25 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:083a2a44b72d61c367ed39325938c3520170b91cdda058a00b72d8323618aabd`  
		Last Modified: Wed, 01 Sep 2021 01:29:05 GMT  
		Size: 2.4 MB (2426002 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.11.12` - linux; arm64 variant v8

```console
$ docker pull alpine@sha256:0a3b174523eafed2b1276cf957020850fc5305ed4605750d0b88f79d4900465d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.7 MB (2728407 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:282ca20f6f11befc379c9c312eba8fa622ff2a246b22b63dc7a9bf234ff830d6`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 02:50:59 GMT
ADD file:da6c0ac7cb9f819998546d88fb489b746004eb2ad6da64a39210696ef0e66e54 in / 
# Wed, 01 Sep 2021 02:50:59 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:07d756952c5cd45726cf9e8a292a3e05ca67eee5da176df7d632be8c5bb0ad04`  
		Last Modified: Wed, 01 Sep 2021 02:52:00 GMT  
		Size: 2.7 MB (2728407 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.11.12` - linux; 386

```console
$ docker pull alpine@sha256:8a6f8667a8eb87453662f3799ea3768b4c3653a66486ee7c1dd1bed0dfe8c5e7
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2813860 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:dffa084855e3c4dfa859eb62c26e208a5bbab235b47feab2859bbb730858a181`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Tue, 31 Aug 2021 21:23:45 GMT
ADD file:cb3b0c8081ccb41fa220b40134af176a4f81554954edd43158061fc1759f0ffd in / 
# Tue, 31 Aug 2021 21:23:45 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:a668cd93fc6ddc8cef33c9cd91e0ebd2d97f164977839d5ebbd25812f63b164f`  
		Last Modified: Tue, 31 Aug 2021 21:24:52 GMT  
		Size: 2.8 MB (2813860 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.11.12` - linux; ppc64le

```console
$ docker pull alpine@sha256:024cea07734079c8314b158b62d95aee704fad739f90cc9a591219a07df45241
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2824904 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8e8a9c591b6e4cfb81c674148973d61329c69a45113429a2fc2d3e331e3699b9`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 02:43:12 GMT
ADD file:17477eee6878a950f2f16286dba9067b4f27ab5d90c29157e30d9bcaee1d9418 in / 
# Wed, 01 Sep 2021 02:43:15 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:6ff762d8ea8ca25e11609614eeba7af16b71db5d8ebea8bc7b802c275174f2ee`  
		Last Modified: Wed, 01 Sep 2021 02:44:15 GMT  
		Size: 2.8 MB (2824904 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.11.12` - linux; s390x

```console
$ docker pull alpine@sha256:5f82c1806d60592b747d6a53d35164b0c8f40e7ccf5425c1d59ae86ec8b3c0bc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2585711 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:45fcff441288bb31228e19a6cad26e26a454e5d5484f1f7115660517bd48cb29`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 01:15:38 GMT
ADD file:c1038db2fb356dc0acf4108117c20ba1da2a1546023e16a0905f49a968d7a1c7 in / 
# Wed, 01 Sep 2021 01:15:39 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:518c147bd44d1caf10e5031a373df0ed439dcb012b636012aa159436f07e1ade`  
		Last Modified: Wed, 01 Sep 2021 01:16:37 GMT  
		Size: 2.6 MB (2585711 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `alpine:3.12`

```console
$ docker pull alpine@sha256:a296b4c6f6ee2b88f095b61e95c7ef4f51ba25598835b4978c9256d8c8ace48a
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 7
	-	linux; amd64
	-	linux; arm variant v6
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; ppc64le
	-	linux; s390x

### `alpine:3.12` - linux; amd64

```console
$ docker pull alpine@sha256:a721d672be3cdaaf1285c2b60b76d81ab4a8a3cf5a7180a966f2c545305ad6de
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2801707 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:48b8ec4ed9ebc29a5c6178a5267c3a9e12fb5e9923fc7bad3b494b9a6a491b68`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Tue, 31 Aug 2021 23:18:23 GMT
ADD file:e3d2013df9d58cd9255c749dbd62e7b1b1bdf1c2ee644c17bb93e67d859f0815 in / 
# Tue, 31 Aug 2021 23:18:24 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:e519532ddf75bafbbb0ad01d3fb678ef9395cd8554fa25bef4695bb6e11f39f1`  
		Last Modified: Tue, 31 Aug 2021 23:19:05 GMT  
		Size: 2.8 MB (2801707 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.12` - linux; arm variant v6

```console
$ docker pull alpine@sha256:b7b182ccc0811af3fc00754c3cc744e45bc3d7d4f6ef9ee47071173f893dbfb0
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2606586 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:74c3eed34b5a9eaf8fe3d01051c1bc6cd015ad5d8e69ca9ce5c08aad64159fd9`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Tue, 31 Aug 2021 22:30:45 GMT
ADD file:6b70601c3d32558ee0cefe87b44ce4c4c8e85db5211f940538f9acf03f6ba6be in / 
# Tue, 31 Aug 2021 22:30:45 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:b3e69bfc382676dcc8e89f289815a72b60fdf61e66e21d90b8851cbb4b5e3aeb`  
		Last Modified: Tue, 31 Aug 2021 22:32:25 GMT  
		Size: 2.6 MB (2606586 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.12` - linux; arm variant v7

```console
$ docker pull alpine@sha256:16aea3fc0648559c47f974d7f31096e71b9ec7013224bb29e35cf3db375af976
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.4 MB (2410387 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:32d927dff55fad51102e58bd75ff1c2d5b59afb8c40a9ebd3e3d63e5e73e2e28`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 01:27:09 GMT
ADD file:fb3ea1e5df00e24345fc4f1adf07702f4ec457a6f7249f6ad06c72e2570955a0 in / 
# Wed, 01 Sep 2021 01:27:10 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:6b3b5e25c4f507c4fe7952504ebbc679023d8699d9c6b2b61376ed676d1ab572`  
		Last Modified: Wed, 01 Sep 2021 01:28:52 GMT  
		Size: 2.4 MB (2410387 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.12` - linux; arm64 variant v8

```console
$ docker pull alpine@sha256:df5a62c11342b503e692ac719f9569da3518c8f1d5a7f072ad76f1b5ef8d3d43
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.7 MB (2712295 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:afc2cc120ca295ae8c973d16f2e6d479c70a8a20c8ce6989a44fae5e717dcf29`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 02:50:52 GMT
ADD file:065608b23c19f03e2753bacf8f548d77f05519f94648c6872303aa1d1fc9254b in / 
# Wed, 01 Sep 2021 02:50:52 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:ccdebe76d52933c5fff4a8db8870e8454f3dc3c92a88e59acc5eb30c6c1178af`  
		Last Modified: Wed, 01 Sep 2021 02:51:48 GMT  
		Size: 2.7 MB (2712295 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.12` - linux; 386

```console
$ docker pull alpine@sha256:38f483c3c603557ed5f9a287d13dd9ca8daea07435ad3f06b038621a2f9fc1d9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2797354 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a60407fbde44776feff052df5f3b69c3e44139efa01f95ca534ee9b796ebb52f`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Tue, 31 Aug 2021 21:23:37 GMT
ADD file:68d4b86192561e85ef9468b3c3e67f604f6788e279c0eeeb28153a30ca5ef846 in / 
# Tue, 31 Aug 2021 21:23:37 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:e34d176e9c153bdf92130c870a1f5ceaf0a97af4243bcbdc67787ed97492313f`  
		Last Modified: Tue, 31 Aug 2021 21:24:40 GMT  
		Size: 2.8 MB (2797354 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.12` - linux; ppc64le

```console
$ docker pull alpine@sha256:372de563e36c3989cee334c3cedf72d0e80879ed989ebd8dee42bdbff5fe4184
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2808136 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:123ae0b982e9321867c0c082de7d207c13d93dcb00dffbe3f0c364df160390f4`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 02:42:58 GMT
ADD file:d72ee9007869e1d3d4b9a35335a1822924b271dbf1b6be5f58504530d124b75e in / 
# Wed, 01 Sep 2021 02:43:01 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:5792a244f55c184baa6ec391092a2266274f93abcadc6d6a3cd8efec5f34cc77`  
		Last Modified: Wed, 01 Sep 2021 02:44:04 GMT  
		Size: 2.8 MB (2808136 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.12` - linux; s390x

```console
$ docker pull alpine@sha256:5391bba453c1474d620fd092c96342e09e0a8ee7f1c2e95ae2dc6e3ed44408c9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2569482 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b73fb63e6e5c139380bc11ac916c07133b820a10f90fd0e5d4db606146b6c1c8`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 01:15:30 GMT
ADD file:f92e0acaaacda169771c62b02df222807a11332100cdfb74af2d2780fd063607 in / 
# Wed, 01 Sep 2021 01:15:30 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:a2902e219e4f4be61566030dec339cd91d4d9abc64cb1d13b5e3125aabcd2823`  
		Last Modified: Wed, 01 Sep 2021 01:16:29 GMT  
		Size: 2.6 MB (2569482 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `alpine:3.12.8`

```console
$ docker pull alpine@sha256:a296b4c6f6ee2b88f095b61e95c7ef4f51ba25598835b4978c9256d8c8ace48a
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 7
	-	linux; amd64
	-	linux; arm variant v6
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; ppc64le
	-	linux; s390x

### `alpine:3.12.8` - linux; amd64

```console
$ docker pull alpine@sha256:a721d672be3cdaaf1285c2b60b76d81ab4a8a3cf5a7180a966f2c545305ad6de
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2801707 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:48b8ec4ed9ebc29a5c6178a5267c3a9e12fb5e9923fc7bad3b494b9a6a491b68`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Tue, 31 Aug 2021 23:18:23 GMT
ADD file:e3d2013df9d58cd9255c749dbd62e7b1b1bdf1c2ee644c17bb93e67d859f0815 in / 
# Tue, 31 Aug 2021 23:18:24 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:e519532ddf75bafbbb0ad01d3fb678ef9395cd8554fa25bef4695bb6e11f39f1`  
		Last Modified: Tue, 31 Aug 2021 23:19:05 GMT  
		Size: 2.8 MB (2801707 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.12.8` - linux; arm variant v6

```console
$ docker pull alpine@sha256:b7b182ccc0811af3fc00754c3cc744e45bc3d7d4f6ef9ee47071173f893dbfb0
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2606586 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:74c3eed34b5a9eaf8fe3d01051c1bc6cd015ad5d8e69ca9ce5c08aad64159fd9`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Tue, 31 Aug 2021 22:30:45 GMT
ADD file:6b70601c3d32558ee0cefe87b44ce4c4c8e85db5211f940538f9acf03f6ba6be in / 
# Tue, 31 Aug 2021 22:30:45 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:b3e69bfc382676dcc8e89f289815a72b60fdf61e66e21d90b8851cbb4b5e3aeb`  
		Last Modified: Tue, 31 Aug 2021 22:32:25 GMT  
		Size: 2.6 MB (2606586 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.12.8` - linux; arm variant v7

```console
$ docker pull alpine@sha256:16aea3fc0648559c47f974d7f31096e71b9ec7013224bb29e35cf3db375af976
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.4 MB (2410387 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:32d927dff55fad51102e58bd75ff1c2d5b59afb8c40a9ebd3e3d63e5e73e2e28`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 01:27:09 GMT
ADD file:fb3ea1e5df00e24345fc4f1adf07702f4ec457a6f7249f6ad06c72e2570955a0 in / 
# Wed, 01 Sep 2021 01:27:10 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:6b3b5e25c4f507c4fe7952504ebbc679023d8699d9c6b2b61376ed676d1ab572`  
		Last Modified: Wed, 01 Sep 2021 01:28:52 GMT  
		Size: 2.4 MB (2410387 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.12.8` - linux; arm64 variant v8

```console
$ docker pull alpine@sha256:df5a62c11342b503e692ac719f9569da3518c8f1d5a7f072ad76f1b5ef8d3d43
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.7 MB (2712295 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:afc2cc120ca295ae8c973d16f2e6d479c70a8a20c8ce6989a44fae5e717dcf29`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 02:50:52 GMT
ADD file:065608b23c19f03e2753bacf8f548d77f05519f94648c6872303aa1d1fc9254b in / 
# Wed, 01 Sep 2021 02:50:52 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:ccdebe76d52933c5fff4a8db8870e8454f3dc3c92a88e59acc5eb30c6c1178af`  
		Last Modified: Wed, 01 Sep 2021 02:51:48 GMT  
		Size: 2.7 MB (2712295 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.12.8` - linux; 386

```console
$ docker pull alpine@sha256:38f483c3c603557ed5f9a287d13dd9ca8daea07435ad3f06b038621a2f9fc1d9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2797354 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a60407fbde44776feff052df5f3b69c3e44139efa01f95ca534ee9b796ebb52f`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Tue, 31 Aug 2021 21:23:37 GMT
ADD file:68d4b86192561e85ef9468b3c3e67f604f6788e279c0eeeb28153a30ca5ef846 in / 
# Tue, 31 Aug 2021 21:23:37 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:e34d176e9c153bdf92130c870a1f5ceaf0a97af4243bcbdc67787ed97492313f`  
		Last Modified: Tue, 31 Aug 2021 21:24:40 GMT  
		Size: 2.8 MB (2797354 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.12.8` - linux; ppc64le

```console
$ docker pull alpine@sha256:372de563e36c3989cee334c3cedf72d0e80879ed989ebd8dee42bdbff5fe4184
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2808136 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:123ae0b982e9321867c0c082de7d207c13d93dcb00dffbe3f0c364df160390f4`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 02:42:58 GMT
ADD file:d72ee9007869e1d3d4b9a35335a1822924b271dbf1b6be5f58504530d124b75e in / 
# Wed, 01 Sep 2021 02:43:01 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:5792a244f55c184baa6ec391092a2266274f93abcadc6d6a3cd8efec5f34cc77`  
		Last Modified: Wed, 01 Sep 2021 02:44:04 GMT  
		Size: 2.8 MB (2808136 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.12.8` - linux; s390x

```console
$ docker pull alpine@sha256:5391bba453c1474d620fd092c96342e09e0a8ee7f1c2e95ae2dc6e3ed44408c9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2569482 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b73fb63e6e5c139380bc11ac916c07133b820a10f90fd0e5d4db606146b6c1c8`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 01:15:30 GMT
ADD file:f92e0acaaacda169771c62b02df222807a11332100cdfb74af2d2780fd063607 in / 
# Wed, 01 Sep 2021 01:15:30 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:a2902e219e4f4be61566030dec339cd91d4d9abc64cb1d13b5e3125aabcd2823`  
		Last Modified: Wed, 01 Sep 2021 01:16:29 GMT  
		Size: 2.6 MB (2569482 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `alpine:3.13`

```console
$ docker pull alpine@sha256:2582893dec6f12fd499d3a709477f2c0c0c1dfcd28024c93f1f0626b9e3540c8
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 7
	-	linux; amd64
	-	linux; arm variant v6
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; ppc64le
	-	linux; s390x

### `alpine:3.13` - linux; amd64

```console
$ docker pull alpine@sha256:e15947432b813e8ffa90165da919953e2ce850bef511a0ad1287d7cb86de84b5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2814079 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:12adea71a33bcce0925f5b2e951992cc2d8b69f4051122e93d5c35000e9b9e28`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Tue, 31 Aug 2021 23:18:16 GMT
ADD file:ecdfb91a737d6c292265c1b77ffd3d82ba810dd43ea4ef79714b66b1da74a5aa in / 
# Tue, 31 Aug 2021 23:18:16 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:4e9f2cdf438714c2c4533e28c6c41a89cc6c1b46cf77e54c488db30ca4f5b6f3`  
		Last Modified: Tue, 31 Aug 2021 23:18:55 GMT  
		Size: 2.8 MB (2814079 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.13` - linux; arm variant v6

```console
$ docker pull alpine@sha256:fb28db57eff4a87917c5c83325a1ca62adce670a3f6e1da50a3387211d72ba96
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2623761 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:69b7a149c06ac4c7a78dee41db13d9e66206b1af1ac676e5b0f432c9b01e0508`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Tue, 31 Aug 2021 22:30:33 GMT
ADD file:ed2b5e0fbd1e7ae37ab8f808c827d23c6841ce1edd7427552d5bf741d67ebcc0 in / 
# Tue, 31 Aug 2021 22:30:33 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:840d18d84f6afdc3231d126fdd3f84f23f0335b61cbfa9cb8808b888a4308919`  
		Last Modified: Tue, 31 Aug 2021 22:32:11 GMT  
		Size: 2.6 MB (2623761 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.13` - linux; arm variant v7

```console
$ docker pull alpine@sha256:7bf024556a224584c0fff680d650b4be2ad560b17f6f627b11e0e2d5beb4b597
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.4 MB (2425373 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:057ad4ee621986edcaa3c070eb2c7671504d67b29e8b55a077a457a5d60de603`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 01:26:54 GMT
ADD file:4a3cd5b6e6a9e76edf236ec86eb493ae8b09bf3220a8c0fdcaa474b9d6135ad3 in / 
# Wed, 01 Sep 2021 01:26:55 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:48fad15491f9799a77d01e4a4a3b0e201ca2aba6f0849c39afa1160d6f3d905a`  
		Last Modified: Wed, 01 Sep 2021 01:28:39 GMT  
		Size: 2.4 MB (2425373 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.13` - linux; arm64 variant v8

```console
$ docker pull alpine@sha256:17d432b0c16d46b7d978dd82a14084f7bcebe497532baea3e3cd9864eb044406
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.7 MB (2713008 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6de88656b047d1aa36eb2721c9e63609cf6dd5341268adfefe4e949dbd49eafb`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 02:50:45 GMT
ADD file:924de68748d5d710724ceb45b3bff9d38eedcad50d5744be4ce74f8f731a791f in / 
# Wed, 01 Sep 2021 02:50:45 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:bbf911997326f5b56d515142e8dbdbe01d2f308276938ddbce3ab347584ed8ce`  
		Last Modified: Wed, 01 Sep 2021 02:51:37 GMT  
		Size: 2.7 MB (2713008 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.13` - linux; 386

```console
$ docker pull alpine@sha256:4ed1812024ed78962a34727137627e8854a3b414d19e2c35a1dc727a47e16fba
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2821095 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:2d1d6881767e3e1c194b061b3422aa76bf076aefd51d1d27c679ff998ead3104`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Tue, 31 Aug 2021 21:23:28 GMT
ADD file:fb9d541cffc3a5660d23426ba847aa696b59a9d7f14e00ba0a63b28c9ec272c0 in / 
# Tue, 31 Aug 2021 21:23:29 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:4ed7d06bd90bc8d13b87220ccc6204a7d235ec943be9fb4289d856f9ff0a5b7b`  
		Last Modified: Tue, 31 Aug 2021 21:24:28 GMT  
		Size: 2.8 MB (2821095 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.13` - linux; ppc64le

```console
$ docker pull alpine@sha256:a243de15b52a44b19c2a02647b11fcf598007ff7f2b07cb8195a0979eff30a0d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2814813 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9c3181779a868e09698b567a3c42f3744584ddb1398efe2c4ba569a99b823f7a`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 02:42:40 GMT
ADD file:07a51f1a2f818bd1c1651832ce63cb1e0046a57994724cda6a20ff1a2a685295 in / 
# Wed, 01 Sep 2021 02:42:41 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:39d9bf63205258fe1d085fd596101e6fc46ff796cda8d3ba2983e166a25b74db`  
		Last Modified: Wed, 01 Sep 2021 02:43:53 GMT  
		Size: 2.8 MB (2814813 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.13` - linux; s390x

```console
$ docker pull alpine@sha256:d04f568d1401e6bcc2c34bd81ac1035ba1d8fc3a03b92c78e84cec61e028d6ea
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2604390 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:71c6dbab0948c6d4931d217ddb197fc78b8fecebde4872267308dc4399cd5059`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 01:15:21 GMT
ADD file:def74c9e73d87d3c8b94cc0200f2723aea3a7462f8d2e0852db9da25c19855ac in / 
# Wed, 01 Sep 2021 01:15:22 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:c1d78e8a87395f597d24b8eb78423ccdcfd404846906154e15aea8be9541c3ae`  
		Last Modified: Wed, 01 Sep 2021 01:16:19 GMT  
		Size: 2.6 MB (2604390 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `alpine:3.13.6`

```console
$ docker pull alpine@sha256:2582893dec6f12fd499d3a709477f2c0c0c1dfcd28024c93f1f0626b9e3540c8
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 7
	-	linux; amd64
	-	linux; arm variant v6
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; ppc64le
	-	linux; s390x

### `alpine:3.13.6` - linux; amd64

```console
$ docker pull alpine@sha256:e15947432b813e8ffa90165da919953e2ce850bef511a0ad1287d7cb86de84b5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2814079 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:12adea71a33bcce0925f5b2e951992cc2d8b69f4051122e93d5c35000e9b9e28`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Tue, 31 Aug 2021 23:18:16 GMT
ADD file:ecdfb91a737d6c292265c1b77ffd3d82ba810dd43ea4ef79714b66b1da74a5aa in / 
# Tue, 31 Aug 2021 23:18:16 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:4e9f2cdf438714c2c4533e28c6c41a89cc6c1b46cf77e54c488db30ca4f5b6f3`  
		Last Modified: Tue, 31 Aug 2021 23:18:55 GMT  
		Size: 2.8 MB (2814079 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.13.6` - linux; arm variant v6

```console
$ docker pull alpine@sha256:fb28db57eff4a87917c5c83325a1ca62adce670a3f6e1da50a3387211d72ba96
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2623761 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:69b7a149c06ac4c7a78dee41db13d9e66206b1af1ac676e5b0f432c9b01e0508`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Tue, 31 Aug 2021 22:30:33 GMT
ADD file:ed2b5e0fbd1e7ae37ab8f808c827d23c6841ce1edd7427552d5bf741d67ebcc0 in / 
# Tue, 31 Aug 2021 22:30:33 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:840d18d84f6afdc3231d126fdd3f84f23f0335b61cbfa9cb8808b888a4308919`  
		Last Modified: Tue, 31 Aug 2021 22:32:11 GMT  
		Size: 2.6 MB (2623761 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.13.6` - linux; arm variant v7

```console
$ docker pull alpine@sha256:7bf024556a224584c0fff680d650b4be2ad560b17f6f627b11e0e2d5beb4b597
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.4 MB (2425373 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:057ad4ee621986edcaa3c070eb2c7671504d67b29e8b55a077a457a5d60de603`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 01:26:54 GMT
ADD file:4a3cd5b6e6a9e76edf236ec86eb493ae8b09bf3220a8c0fdcaa474b9d6135ad3 in / 
# Wed, 01 Sep 2021 01:26:55 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:48fad15491f9799a77d01e4a4a3b0e201ca2aba6f0849c39afa1160d6f3d905a`  
		Last Modified: Wed, 01 Sep 2021 01:28:39 GMT  
		Size: 2.4 MB (2425373 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.13.6` - linux; arm64 variant v8

```console
$ docker pull alpine@sha256:17d432b0c16d46b7d978dd82a14084f7bcebe497532baea3e3cd9864eb044406
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.7 MB (2713008 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6de88656b047d1aa36eb2721c9e63609cf6dd5341268adfefe4e949dbd49eafb`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 02:50:45 GMT
ADD file:924de68748d5d710724ceb45b3bff9d38eedcad50d5744be4ce74f8f731a791f in / 
# Wed, 01 Sep 2021 02:50:45 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:bbf911997326f5b56d515142e8dbdbe01d2f308276938ddbce3ab347584ed8ce`  
		Last Modified: Wed, 01 Sep 2021 02:51:37 GMT  
		Size: 2.7 MB (2713008 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.13.6` - linux; 386

```console
$ docker pull alpine@sha256:4ed1812024ed78962a34727137627e8854a3b414d19e2c35a1dc727a47e16fba
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2821095 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:2d1d6881767e3e1c194b061b3422aa76bf076aefd51d1d27c679ff998ead3104`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Tue, 31 Aug 2021 21:23:28 GMT
ADD file:fb9d541cffc3a5660d23426ba847aa696b59a9d7f14e00ba0a63b28c9ec272c0 in / 
# Tue, 31 Aug 2021 21:23:29 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:4ed7d06bd90bc8d13b87220ccc6204a7d235ec943be9fb4289d856f9ff0a5b7b`  
		Last Modified: Tue, 31 Aug 2021 21:24:28 GMT  
		Size: 2.8 MB (2821095 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.13.6` - linux; ppc64le

```console
$ docker pull alpine@sha256:a243de15b52a44b19c2a02647b11fcf598007ff7f2b07cb8195a0979eff30a0d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2814813 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9c3181779a868e09698b567a3c42f3744584ddb1398efe2c4ba569a99b823f7a`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 02:42:40 GMT
ADD file:07a51f1a2f818bd1c1651832ce63cb1e0046a57994724cda6a20ff1a2a685295 in / 
# Wed, 01 Sep 2021 02:42:41 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:39d9bf63205258fe1d085fd596101e6fc46ff796cda8d3ba2983e166a25b74db`  
		Last Modified: Wed, 01 Sep 2021 02:43:53 GMT  
		Size: 2.8 MB (2814813 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.13.6` - linux; s390x

```console
$ docker pull alpine@sha256:d04f568d1401e6bcc2c34bd81ac1035ba1d8fc3a03b92c78e84cec61e028d6ea
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2604390 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:71c6dbab0948c6d4931d217ddb197fc78b8fecebde4872267308dc4399cd5059`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 01:15:21 GMT
ADD file:def74c9e73d87d3c8b94cc0200f2723aea3a7462f8d2e0852db9da25c19855ac in / 
# Wed, 01 Sep 2021 01:15:22 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:c1d78e8a87395f597d24b8eb78423ccdcfd404846906154e15aea8be9541c3ae`  
		Last Modified: Wed, 01 Sep 2021 01:16:19 GMT  
		Size: 2.6 MB (2604390 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `alpine:3.14`

```console
$ docker pull alpine@sha256:e1c082e3d3c45cccac829840a25941e679c25d438cc8412c2fa221cf1a824e6a
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 7
	-	linux; amd64
	-	linux; arm variant v6
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; ppc64le
	-	linux; s390x

### `alpine:3.14` - linux; amd64

```console
$ docker pull alpine@sha256:69704ef328d05a9f806b6b8502915e6a0a4faa4d72018dc42343f511490daf8a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2814446 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:14119a10abf4669e8cdbdff324a9f9605d99697215a0d21c360fe8dfa8471bab`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 17:19:45 GMT
ADD file:aad4290d27580cc1a094ffaf98c3ca2fc5d699fe695dfb8e6e9fac20f1129450 in / 
# Fri, 27 Aug 2021 17:19:45 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:a0d0a0d46f8b52473982a3c466318f479767577551a53ffc9074c9fa7035982e`  
		Last Modified: Fri, 27 Aug 2021 17:20:13 GMT  
		Size: 2.8 MB (2814446 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.14` - linux; arm variant v6

```console
$ docker pull alpine@sha256:18c29393a090ba5cde8a5f00926e9e419f47cfcfd206cc3f7f590e91b19adfe9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2627447 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:651b237e41d5a4f13081d1def5aee149bfe130e33cc4834bca7c8554b7f4a21a`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 17:49:29 GMT
ADD file:1c1c4520d49cb6e8f795f3b953d1ed3c3c77868b98b53a455169c254fcec5acd in / 
# Fri, 27 Aug 2021 17:49:30 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:2e78c0f86ba9a1fed30df20cb48c9cc73e9626399f12749d36b892ff99c0ecf5`  
		Last Modified: Fri, 27 Aug 2021 17:50:55 GMT  
		Size: 2.6 MB (2627447 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.14` - linux; arm variant v7

```console
$ docker pull alpine@sha256:e12ff876f0075740ed3d7bdf788107ae84c1b3dd6dc98b3baea41088aba5236f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.4 MB (2430419 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3e8172af00ceeca661953e4f3ce408bc705028e4e673db4332e9e8587614e6c2`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 17:57:31 GMT
ADD file:a7da7992ccea54d3295231027614f138aa45c4d4a25ea6ec9bf7b316b9f67d95 in / 
# Fri, 27 Aug 2021 17:57:32 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:a14774a5a62e0f454febaec7cb603e18a6a8e25ef9da4a4da85b155bdd5e5a7a`  
		Last Modified: Fri, 27 Aug 2021 17:59:00 GMT  
		Size: 2.4 MB (2430419 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.14` - linux; arm64 variant v8

```console
$ docker pull alpine@sha256:b06a5cf61b2956088722c4f1b9a6f71dfe95f0b1fe285d44195452b8a1627de7
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.7 MB (2711827 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:bb3de5531c18f185667b0be0e400ab24aa40f4440093de82baf4072e14af3b84`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 17:39:33 GMT
ADD file:dc8af9c7bfe9f9541e1db38dea01c4201609f3075b31e108f2073ffed8c5e4b9 in / 
# Fri, 27 Aug 2021 17:39:33 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:552d1f2373af9bfe12033568ebbfb0ccbb0de11279f9a415a29207e264d7f4d9`  
		Last Modified: Fri, 27 Aug 2021 17:40:18 GMT  
		Size: 2.7 MB (2711827 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.14` - linux; 386

```console
$ docker pull alpine@sha256:a77adef9f69751add61080617e15e67aba9aa7a5fd5414b9fae84143210ee0ad
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2822857 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:85f8acd1af44479b7ed08b5e2528c45f6fe1e7ac64b111ba8723548e1566ff57`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 17:38:29 GMT
ADD file:42a7bc5a08b546b47049dd0f89ae4e7a8c86342f87000f39ade3ff52916a6c2e in / 
# Fri, 27 Aug 2021 17:38:30 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:b11ae9fc5d8a106cfed3a6f6c75e5b5b5797c566fac4411622b4055df6541286`  
		Last Modified: Fri, 27 Aug 2021 17:39:18 GMT  
		Size: 2.8 MB (2822857 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.14` - linux; ppc64le

```console
$ docker pull alpine@sha256:9bea59997a84beb47a8cc7ddb11abc957b141e8160852aa93b4cf60659016b53
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2812284 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6bc661e735b72b62d45b189445b5eca79b8d149fe5ae01b0985dc8edce3e49c0`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 19:39:54 GMT
ADD file:d213c56ffc24a5051e8060fd0fec1a0520367c10d88ab16321c36336b6c66098 in / 
# Fri, 27 Aug 2021 19:39:59 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:63da8ca98f7b4b94381aed56862a60aecf355d9428b9aeb7c61d5bd017100c18`  
		Last Modified: Fri, 27 Aug 2021 19:41:06 GMT  
		Size: 2.8 MB (2812284 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.14` - linux; s390x

```console
$ docker pull alpine@sha256:e841d0f0881ea22080e84088337646ada15871abbc3ce19b3219e8fc2cb0cc22
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2603464 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:90ee2af91bcf25a281ce4f0513d1828671b37e7c1eea1a42f5626a54087df9fd`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 17:41:29 GMT
ADD file:9b40ee281e8797067fb2ae207c406084cb81593090338a8b7cb09ade52168daa in / 
# Fri, 27 Aug 2021 17:41:30 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:da14cb6b6dc946dbb2d84386bcaca84e2d46f650767cd11bdb3331ec9d623988`  
		Last Modified: Fri, 27 Aug 2021 17:42:25 GMT  
		Size: 2.6 MB (2603464 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `alpine:3.14.2`

```console
$ docker pull alpine@sha256:e1c082e3d3c45cccac829840a25941e679c25d438cc8412c2fa221cf1a824e6a
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 7
	-	linux; amd64
	-	linux; arm variant v6
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; ppc64le
	-	linux; s390x

### `alpine:3.14.2` - linux; amd64

```console
$ docker pull alpine@sha256:69704ef328d05a9f806b6b8502915e6a0a4faa4d72018dc42343f511490daf8a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2814446 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:14119a10abf4669e8cdbdff324a9f9605d99697215a0d21c360fe8dfa8471bab`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 17:19:45 GMT
ADD file:aad4290d27580cc1a094ffaf98c3ca2fc5d699fe695dfb8e6e9fac20f1129450 in / 
# Fri, 27 Aug 2021 17:19:45 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:a0d0a0d46f8b52473982a3c466318f479767577551a53ffc9074c9fa7035982e`  
		Last Modified: Fri, 27 Aug 2021 17:20:13 GMT  
		Size: 2.8 MB (2814446 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.14.2` - linux; arm variant v6

```console
$ docker pull alpine@sha256:18c29393a090ba5cde8a5f00926e9e419f47cfcfd206cc3f7f590e91b19adfe9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2627447 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:651b237e41d5a4f13081d1def5aee149bfe130e33cc4834bca7c8554b7f4a21a`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 17:49:29 GMT
ADD file:1c1c4520d49cb6e8f795f3b953d1ed3c3c77868b98b53a455169c254fcec5acd in / 
# Fri, 27 Aug 2021 17:49:30 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:2e78c0f86ba9a1fed30df20cb48c9cc73e9626399f12749d36b892ff99c0ecf5`  
		Last Modified: Fri, 27 Aug 2021 17:50:55 GMT  
		Size: 2.6 MB (2627447 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.14.2` - linux; arm variant v7

```console
$ docker pull alpine@sha256:e12ff876f0075740ed3d7bdf788107ae84c1b3dd6dc98b3baea41088aba5236f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.4 MB (2430419 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3e8172af00ceeca661953e4f3ce408bc705028e4e673db4332e9e8587614e6c2`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 17:57:31 GMT
ADD file:a7da7992ccea54d3295231027614f138aa45c4d4a25ea6ec9bf7b316b9f67d95 in / 
# Fri, 27 Aug 2021 17:57:32 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:a14774a5a62e0f454febaec7cb603e18a6a8e25ef9da4a4da85b155bdd5e5a7a`  
		Last Modified: Fri, 27 Aug 2021 17:59:00 GMT  
		Size: 2.4 MB (2430419 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.14.2` - linux; arm64 variant v8

```console
$ docker pull alpine@sha256:b06a5cf61b2956088722c4f1b9a6f71dfe95f0b1fe285d44195452b8a1627de7
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.7 MB (2711827 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:bb3de5531c18f185667b0be0e400ab24aa40f4440093de82baf4072e14af3b84`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 17:39:33 GMT
ADD file:dc8af9c7bfe9f9541e1db38dea01c4201609f3075b31e108f2073ffed8c5e4b9 in / 
# Fri, 27 Aug 2021 17:39:33 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:552d1f2373af9bfe12033568ebbfb0ccbb0de11279f9a415a29207e264d7f4d9`  
		Last Modified: Fri, 27 Aug 2021 17:40:18 GMT  
		Size: 2.7 MB (2711827 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.14.2` - linux; 386

```console
$ docker pull alpine@sha256:a77adef9f69751add61080617e15e67aba9aa7a5fd5414b9fae84143210ee0ad
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2822857 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:85f8acd1af44479b7ed08b5e2528c45f6fe1e7ac64b111ba8723548e1566ff57`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 17:38:29 GMT
ADD file:42a7bc5a08b546b47049dd0f89ae4e7a8c86342f87000f39ade3ff52916a6c2e in / 
# Fri, 27 Aug 2021 17:38:30 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:b11ae9fc5d8a106cfed3a6f6c75e5b5b5797c566fac4411622b4055df6541286`  
		Last Modified: Fri, 27 Aug 2021 17:39:18 GMT  
		Size: 2.8 MB (2822857 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.14.2` - linux; ppc64le

```console
$ docker pull alpine@sha256:9bea59997a84beb47a8cc7ddb11abc957b141e8160852aa93b4cf60659016b53
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2812284 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6bc661e735b72b62d45b189445b5eca79b8d149fe5ae01b0985dc8edce3e49c0`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 19:39:54 GMT
ADD file:d213c56ffc24a5051e8060fd0fec1a0520367c10d88ab16321c36336b6c66098 in / 
# Fri, 27 Aug 2021 19:39:59 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:63da8ca98f7b4b94381aed56862a60aecf355d9428b9aeb7c61d5bd017100c18`  
		Last Modified: Fri, 27 Aug 2021 19:41:06 GMT  
		Size: 2.8 MB (2812284 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.14.2` - linux; s390x

```console
$ docker pull alpine@sha256:e841d0f0881ea22080e84088337646ada15871abbc3ce19b3219e8fc2cb0cc22
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2603464 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:90ee2af91bcf25a281ce4f0513d1828671b37e7c1eea1a42f5626a54087df9fd`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 17:41:29 GMT
ADD file:9b40ee281e8797067fb2ae207c406084cb81593090338a8b7cb09ade52168daa in / 
# Fri, 27 Aug 2021 17:41:30 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:da14cb6b6dc946dbb2d84386bcaca84e2d46f650767cd11bdb3331ec9d623988`  
		Last Modified: Fri, 27 Aug 2021 17:42:25 GMT  
		Size: 2.6 MB (2603464 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `alpine:edge`

```console
$ docker pull alpine@sha256:e64a0b2fc7ff870c2b22506009288daa5134da2b8c541440694b629fc22d792e
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v6
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; ppc64le
	-	linux; riscv64
	-	linux; s390x

### `alpine:edge` - linux; amd64

```console
$ docker pull alpine@sha256:0119f88f395766eb52f9b817c3d23576bf31935dc8e94abe14bae9a083ce4639
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2809935 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4e873038b87b454ce65890597a96fa12772164123b4d266f1278366241eca60d`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 04 Aug 2021 21:19:39 GMT
ADD file:9d64c3b3b8afed5f8c49f3fd84c6a4d5f244a48c3a6e714f0f9e3f188e36dfec in / 
# Wed, 04 Aug 2021 21:19:39 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:41dcc117e123342ad8ade3073ce46b505baa04b6a19e2fed7758f086a102fa7b`  
		Last Modified: Wed, 04 Aug 2021 21:20:10 GMT  
		Size: 2.8 MB (2809935 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:edge` - linux; arm variant v6

```console
$ docker pull alpine@sha256:fe9c17d22bcf0f1042f87b9a8f0dc4f0f83a0183a9d418d74175091fc57f0b48
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2624432 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:2b32c399cd9e6d58d593d4a08638e845c83f51ae68b40b763b3d4cce84e5c473`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Mon, 30 Aug 2021 18:37:05 GMT
ADD file:a040ef45d7cfe61fabcf624a7b533e28d8417884dab9987d30ffda8dbdb19ed9 in / 
# Mon, 30 Aug 2021 18:37:06 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:af2daa8728d7e61dcf122793a25e9635fc0d31604ea5435fd249266af31d85e6`  
		Last Modified: Wed, 04 Aug 2021 21:51:01 GMT  
		Size: 2.6 MB (2624432 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:edge` - linux; arm variant v7

```console
$ docker pull alpine@sha256:7e18acf944b0654583187a683ad5d0ac64495850600ea7db40137f4e33d34983
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.4 MB (2427357 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:bc81d48a660bb2243625e40bf35b7516c17fe6868ce7bd5e695c9a98f5ba2c07`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 04 Aug 2021 21:57:24 GMT
ADD file:c2215bda0db2fb3720aa9795bf5a1346e9d873d8016b50770b0963e7fce527f9 in / 
# Wed, 04 Aug 2021 21:57:25 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:1094e2da180bc33d5acca46b8b96b7d5d6db6a5e9943441c3cd3187ac2d37d34`  
		Last Modified: Wed, 04 Aug 2021 21:58:56 GMT  
		Size: 2.4 MB (2427357 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:edge` - linux; arm64 variant v8

```console
$ docker pull alpine@sha256:e69f011cd0c679fb71015c1276f49cd474ed8992b9d724d448250fdcd7501bc5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.7 MB (2710192 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1e093ce69acfe14d25fc33c1fc4026f78a292770ff22fba01b5bda8068634605`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 04 Aug 2021 21:39:21 GMT
ADD file:7081d91e1ac988e9b96ab189dbd20209b0ad9ee550372591ddbb733600e94f42 in / 
# Wed, 04 Aug 2021 21:39:21 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:863239114e4bd9eef9cdf736b57a7f9f349e18280a9d771cf0780288000cb0f9`  
		Last Modified: Wed, 04 Aug 2021 21:40:08 GMT  
		Size: 2.7 MB (2710192 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:edge` - linux; 386

```console
$ docker pull alpine@sha256:8fb23b62707b60d1872c7bb1516bc16a7f9f261ace57255b46675093b25b264a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2819706 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:99765b8e6b425318173f8adfad6fe274a9991d11547cf6a84a988fed398a1cff`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 04 Aug 2021 21:38:23 GMT
ADD file:b807a944822e2b1f892ca140b74b3e0e97e14fdb03fcc8e5c2c3fecf96606422 in / 
# Wed, 04 Aug 2021 21:38:23 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:0ad37d1d34cd8b97a849f75b389b29058dd85bd8be125ffbdc4dc2cd4ebd9ab2`  
		Last Modified: Wed, 04 Aug 2021 21:39:13 GMT  
		Size: 2.8 MB (2819706 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:edge` - linux; ppc64le

```console
$ docker pull alpine@sha256:6029416056a6546f890e6d023228b971e593ecae86be1cae9ab593eb11c7937a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2810679 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:39f926c7bfaf9bf8cf3e4d1d114f91584c18b8e207dd0daa1b427d1cf299691e`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 04 Aug 2021 21:23:04 GMT
ADD file:a367f58774ad9846b4cf7a1cf3f9ee298ff3d91cd8e98861548bd4ad00b088d0 in / 
# Wed, 04 Aug 2021 21:23:07 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:7d12250a58c05855b73036bfab1a9fdfe22d460ed011b345f447619421f63b0e`  
		Last Modified: Wed, 04 Aug 2021 21:24:00 GMT  
		Size: 2.8 MB (2810679 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:edge` - linux; riscv64

```console
$ docker pull alpine@sha256:2387fa34569bac5a2745bbe536105834be0c5a72be9d0fa1c793403672c220a2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.4 MB (2396116 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6bb9c1ea8da2626a9292ec9e6fffb5246e26c766aeda2656754eb13b47a54e65`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Thu, 05 Aug 2021 00:14:52 GMT
ADD file:ff7ce713e31a4e63191a849a00344bcf4cbe6a815579022c77da801e253ee79b in / 
# Thu, 05 Aug 2021 00:14:53 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:9fc107b906f212bfd9a75969f6d44bba2db3d0e8cdb53cf311470edc5e66b825`  
		Last Modified: Thu, 05 Aug 2021 00:18:00 GMT  
		Size: 2.4 MB (2396116 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:edge` - linux; s390x

```console
$ docker pull alpine@sha256:0b21b0bf11bcd22f3ef496f0eb95cdd8b05a1489a3ed67e3d0225fe5b2462e25
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2598383 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:bfdf0d35838c8f10bd529ba8150eb8b3d7c19b3e0a5ed064fd3645c2e68b29aa`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 04 Aug 2021 21:41:28 GMT
ADD file:f5fe47ec03c849b6bc8e0a5768cb7a93ffe09dba1b2a30d98525c80a9565ae94 in / 
# Wed, 04 Aug 2021 21:41:29 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:bb9c72f8d440b8ccd27b4a03966b11ce4bc833a1dcda38fe38e3cacc701554ac`  
		Last Modified: Wed, 04 Aug 2021 21:42:39 GMT  
		Size: 2.6 MB (2598383 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `alpine:latest`

```console
$ docker pull alpine@sha256:e1c082e3d3c45cccac829840a25941e679c25d438cc8412c2fa221cf1a824e6a
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 7
	-	linux; amd64
	-	linux; arm variant v6
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; ppc64le
	-	linux; s390x

### `alpine:latest` - linux; amd64

```console
$ docker pull alpine@sha256:69704ef328d05a9f806b6b8502915e6a0a4faa4d72018dc42343f511490daf8a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2814446 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:14119a10abf4669e8cdbdff324a9f9605d99697215a0d21c360fe8dfa8471bab`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 17:19:45 GMT
ADD file:aad4290d27580cc1a094ffaf98c3ca2fc5d699fe695dfb8e6e9fac20f1129450 in / 
# Fri, 27 Aug 2021 17:19:45 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:a0d0a0d46f8b52473982a3c466318f479767577551a53ffc9074c9fa7035982e`  
		Last Modified: Fri, 27 Aug 2021 17:20:13 GMT  
		Size: 2.8 MB (2814446 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:latest` - linux; arm variant v6

```console
$ docker pull alpine@sha256:18c29393a090ba5cde8a5f00926e9e419f47cfcfd206cc3f7f590e91b19adfe9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2627447 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:651b237e41d5a4f13081d1def5aee149bfe130e33cc4834bca7c8554b7f4a21a`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 17:49:29 GMT
ADD file:1c1c4520d49cb6e8f795f3b953d1ed3c3c77868b98b53a455169c254fcec5acd in / 
# Fri, 27 Aug 2021 17:49:30 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:2e78c0f86ba9a1fed30df20cb48c9cc73e9626399f12749d36b892ff99c0ecf5`  
		Last Modified: Fri, 27 Aug 2021 17:50:55 GMT  
		Size: 2.6 MB (2627447 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:latest` - linux; arm variant v7

```console
$ docker pull alpine@sha256:e12ff876f0075740ed3d7bdf788107ae84c1b3dd6dc98b3baea41088aba5236f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.4 MB (2430419 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3e8172af00ceeca661953e4f3ce408bc705028e4e673db4332e9e8587614e6c2`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 17:57:31 GMT
ADD file:a7da7992ccea54d3295231027614f138aa45c4d4a25ea6ec9bf7b316b9f67d95 in / 
# Fri, 27 Aug 2021 17:57:32 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:a14774a5a62e0f454febaec7cb603e18a6a8e25ef9da4a4da85b155bdd5e5a7a`  
		Last Modified: Fri, 27 Aug 2021 17:59:00 GMT  
		Size: 2.4 MB (2430419 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:latest` - linux; arm64 variant v8

```console
$ docker pull alpine@sha256:b06a5cf61b2956088722c4f1b9a6f71dfe95f0b1fe285d44195452b8a1627de7
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.7 MB (2711827 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:bb3de5531c18f185667b0be0e400ab24aa40f4440093de82baf4072e14af3b84`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 17:39:33 GMT
ADD file:dc8af9c7bfe9f9541e1db38dea01c4201609f3075b31e108f2073ffed8c5e4b9 in / 
# Fri, 27 Aug 2021 17:39:33 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:552d1f2373af9bfe12033568ebbfb0ccbb0de11279f9a415a29207e264d7f4d9`  
		Last Modified: Fri, 27 Aug 2021 17:40:18 GMT  
		Size: 2.7 MB (2711827 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:latest` - linux; 386

```console
$ docker pull alpine@sha256:a77adef9f69751add61080617e15e67aba9aa7a5fd5414b9fae84143210ee0ad
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2822857 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:85f8acd1af44479b7ed08b5e2528c45f6fe1e7ac64b111ba8723548e1566ff57`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 17:38:29 GMT
ADD file:42a7bc5a08b546b47049dd0f89ae4e7a8c86342f87000f39ade3ff52916a6c2e in / 
# Fri, 27 Aug 2021 17:38:30 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:b11ae9fc5d8a106cfed3a6f6c75e5b5b5797c566fac4411622b4055df6541286`  
		Last Modified: Fri, 27 Aug 2021 17:39:18 GMT  
		Size: 2.8 MB (2822857 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:latest` - linux; ppc64le

```console
$ docker pull alpine@sha256:9bea59997a84beb47a8cc7ddb11abc957b141e8160852aa93b4cf60659016b53
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2812284 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6bc661e735b72b62d45b189445b5eca79b8d149fe5ae01b0985dc8edce3e49c0`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 19:39:54 GMT
ADD file:d213c56ffc24a5051e8060fd0fec1a0520367c10d88ab16321c36336b6c66098 in / 
# Fri, 27 Aug 2021 19:39:59 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:63da8ca98f7b4b94381aed56862a60aecf355d9428b9aeb7c61d5bd017100c18`  
		Last Modified: Fri, 27 Aug 2021 19:41:06 GMT  
		Size: 2.8 MB (2812284 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:latest` - linux; s390x

```console
$ docker pull alpine@sha256:e841d0f0881ea22080e84088337646ada15871abbc3ce19b3219e8fc2cb0cc22
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2603464 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:90ee2af91bcf25a281ce4f0513d1828671b37e7c1eea1a42f5626a54087df9fd`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Fri, 27 Aug 2021 17:41:29 GMT
ADD file:9b40ee281e8797067fb2ae207c406084cb81593090338a8b7cb09ade52168daa in / 
# Fri, 27 Aug 2021 17:41:30 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:da14cb6b6dc946dbb2d84386bcaca84e2d46f650767cd11bdb3331ec9d623988`  
		Last Modified: Fri, 27 Aug 2021 17:42:25 GMT  
		Size: 2.6 MB (2603464 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
