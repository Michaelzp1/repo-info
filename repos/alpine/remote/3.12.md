## `alpine:3.12`

```console
$ docker pull alpine@sha256:a296b4c6f6ee2b88f095b61e95c7ef4f51ba25598835b4978c9256d8c8ace48a
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 7
	-	linux; amd64
	-	linux; arm variant v6
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; ppc64le
	-	linux; s390x

### `alpine:3.12` - linux; amd64

```console
$ docker pull alpine@sha256:a721d672be3cdaaf1285c2b60b76d81ab4a8a3cf5a7180a966f2c545305ad6de
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2801707 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:48b8ec4ed9ebc29a5c6178a5267c3a9e12fb5e9923fc7bad3b494b9a6a491b68`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Tue, 31 Aug 2021 23:18:23 GMT
ADD file:e3d2013df9d58cd9255c749dbd62e7b1b1bdf1c2ee644c17bb93e67d859f0815 in / 
# Tue, 31 Aug 2021 23:18:24 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:e519532ddf75bafbbb0ad01d3fb678ef9395cd8554fa25bef4695bb6e11f39f1`  
		Last Modified: Tue, 31 Aug 2021 23:19:05 GMT  
		Size: 2.8 MB (2801707 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.12` - linux; arm variant v6

```console
$ docker pull alpine@sha256:b7b182ccc0811af3fc00754c3cc744e45bc3d7d4f6ef9ee47071173f893dbfb0
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2606586 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:74c3eed34b5a9eaf8fe3d01051c1bc6cd015ad5d8e69ca9ce5c08aad64159fd9`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Tue, 31 Aug 2021 22:30:45 GMT
ADD file:6b70601c3d32558ee0cefe87b44ce4c4c8e85db5211f940538f9acf03f6ba6be in / 
# Tue, 31 Aug 2021 22:30:45 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:b3e69bfc382676dcc8e89f289815a72b60fdf61e66e21d90b8851cbb4b5e3aeb`  
		Last Modified: Tue, 31 Aug 2021 22:32:25 GMT  
		Size: 2.6 MB (2606586 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.12` - linux; arm variant v7

```console
$ docker pull alpine@sha256:16aea3fc0648559c47f974d7f31096e71b9ec7013224bb29e35cf3db375af976
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.4 MB (2410387 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:32d927dff55fad51102e58bd75ff1c2d5b59afb8c40a9ebd3e3d63e5e73e2e28`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 01:27:09 GMT
ADD file:fb3ea1e5df00e24345fc4f1adf07702f4ec457a6f7249f6ad06c72e2570955a0 in / 
# Wed, 01 Sep 2021 01:27:10 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:6b3b5e25c4f507c4fe7952504ebbc679023d8699d9c6b2b61376ed676d1ab572`  
		Last Modified: Wed, 01 Sep 2021 01:28:52 GMT  
		Size: 2.4 MB (2410387 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.12` - linux; arm64 variant v8

```console
$ docker pull alpine@sha256:df5a62c11342b503e692ac719f9569da3518c8f1d5a7f072ad76f1b5ef8d3d43
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.7 MB (2712295 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:afc2cc120ca295ae8c973d16f2e6d479c70a8a20c8ce6989a44fae5e717dcf29`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 02:50:52 GMT
ADD file:065608b23c19f03e2753bacf8f548d77f05519f94648c6872303aa1d1fc9254b in / 
# Wed, 01 Sep 2021 02:50:52 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:ccdebe76d52933c5fff4a8db8870e8454f3dc3c92a88e59acc5eb30c6c1178af`  
		Last Modified: Wed, 01 Sep 2021 02:51:48 GMT  
		Size: 2.7 MB (2712295 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.12` - linux; 386

```console
$ docker pull alpine@sha256:38f483c3c603557ed5f9a287d13dd9ca8daea07435ad3f06b038621a2f9fc1d9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2797354 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a60407fbde44776feff052df5f3b69c3e44139efa01f95ca534ee9b796ebb52f`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Tue, 31 Aug 2021 21:23:37 GMT
ADD file:68d4b86192561e85ef9468b3c3e67f604f6788e279c0eeeb28153a30ca5ef846 in / 
# Tue, 31 Aug 2021 21:23:37 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:e34d176e9c153bdf92130c870a1f5ceaf0a97af4243bcbdc67787ed97492313f`  
		Last Modified: Tue, 31 Aug 2021 21:24:40 GMT  
		Size: 2.8 MB (2797354 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.12` - linux; ppc64le

```console
$ docker pull alpine@sha256:372de563e36c3989cee334c3cedf72d0e80879ed989ebd8dee42bdbff5fe4184
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2808136 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:123ae0b982e9321867c0c082de7d207c13d93dcb00dffbe3f0c364df160390f4`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 02:42:58 GMT
ADD file:d72ee9007869e1d3d4b9a35335a1822924b271dbf1b6be5f58504530d124b75e in / 
# Wed, 01 Sep 2021 02:43:01 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:5792a244f55c184baa6ec391092a2266274f93abcadc6d6a3cd8efec5f34cc77`  
		Last Modified: Wed, 01 Sep 2021 02:44:04 GMT  
		Size: 2.8 MB (2808136 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.12` - linux; s390x

```console
$ docker pull alpine@sha256:5391bba453c1474d620fd092c96342e09e0a8ee7f1c2e95ae2dc6e3ed44408c9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2569482 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b73fb63e6e5c139380bc11ac916c07133b820a10f90fd0e5d4db606146b6c1c8`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 01:15:30 GMT
ADD file:f92e0acaaacda169771c62b02df222807a11332100cdfb74af2d2780fd063607 in / 
# Wed, 01 Sep 2021 01:15:30 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:a2902e219e4f4be61566030dec339cd91d4d9abc64cb1d13b5e3125aabcd2823`  
		Last Modified: Wed, 01 Sep 2021 01:16:29 GMT  
		Size: 2.6 MB (2569482 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
