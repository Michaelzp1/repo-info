## `alpine:3.11.12`

```console
$ docker pull alpine@sha256:e457c945f4be075a9e5365c6628e552fcd15551f0bc1c1fc2ea0f2227a524210
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 7
	-	linux; amd64
	-	linux; arm variant v6
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; ppc64le
	-	linux; s390x

### `alpine:3.11.12` - linux; amd64

```console
$ docker pull alpine@sha256:c3d45491770c51da4ef58318e3714da686bc7165338b7ab5ac758e75c7455efb
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2817307 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d2bcc698e2fc63aec63bd1593bb8b4d8d0cbd5be76e3224b87678bab23d50265`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Tue, 31 Aug 2021 23:18:31 GMT
ADD file:9d14b11183983923090d9e6d15cc51ee210466296e913bfefbfd580b3de59c95 in / 
# Tue, 31 Aug 2021 23:18:31 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:6a428f9f83b0a29f1fdd2ccccca19a9bab805a925b8eddf432a5a3d3da04afbc`  
		Last Modified: Tue, 31 Aug 2021 23:19:15 GMT  
		Size: 2.8 MB (2817307 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.11.12` - linux; arm variant v6

```console
$ docker pull alpine@sha256:86d55ea0abdcc98ed0a3652510ff7fda8f36aefcd49aa067a9cc2c817d05f1d8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2623044 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d7818f0f8acfe973ae22435e06884cd6219d34252487eec50b4b42387acac64f`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Tue, 31 Aug 2021 22:30:57 GMT
ADD file:3e83d6b5df3a951968e475c7326baf5ef90a22f04163693db34f3b4fc5812434 in / 
# Tue, 31 Aug 2021 22:30:57 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:7fe987b00bcb1f14c5b65f89813475143c021e2f5c156705ac3525abe1b397a1`  
		Last Modified: Tue, 31 Aug 2021 22:32:38 GMT  
		Size: 2.6 MB (2623044 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.11.12` - linux; arm variant v7

```console
$ docker pull alpine@sha256:bc3a56cf7fea030fbf230540ce724c2e5f81434eb2903910c31e7be3918f3890
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.4 MB (2426002 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:20eab2c45d3b11729b17dd4a9c95604c4f5e4cf2bf93d4f373394b41906d5ce9`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 01:27:25 GMT
ADD file:8c75cb63bbe2eaaa2143faab71b60e4fca1773613a67dd81fbad93eaf133e208 in / 
# Wed, 01 Sep 2021 01:27:25 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:083a2a44b72d61c367ed39325938c3520170b91cdda058a00b72d8323618aabd`  
		Last Modified: Wed, 01 Sep 2021 01:29:05 GMT  
		Size: 2.4 MB (2426002 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.11.12` - linux; arm64 variant v8

```console
$ docker pull alpine@sha256:0a3b174523eafed2b1276cf957020850fc5305ed4605750d0b88f79d4900465d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.7 MB (2728407 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:282ca20f6f11befc379c9c312eba8fa622ff2a246b22b63dc7a9bf234ff830d6`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 02:50:59 GMT
ADD file:da6c0ac7cb9f819998546d88fb489b746004eb2ad6da64a39210696ef0e66e54 in / 
# Wed, 01 Sep 2021 02:50:59 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:07d756952c5cd45726cf9e8a292a3e05ca67eee5da176df7d632be8c5bb0ad04`  
		Last Modified: Wed, 01 Sep 2021 02:52:00 GMT  
		Size: 2.7 MB (2728407 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.11.12` - linux; 386

```console
$ docker pull alpine@sha256:8a6f8667a8eb87453662f3799ea3768b4c3653a66486ee7c1dd1bed0dfe8c5e7
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2813860 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:dffa084855e3c4dfa859eb62c26e208a5bbab235b47feab2859bbb730858a181`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Tue, 31 Aug 2021 21:23:45 GMT
ADD file:cb3b0c8081ccb41fa220b40134af176a4f81554954edd43158061fc1759f0ffd in / 
# Tue, 31 Aug 2021 21:23:45 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:a668cd93fc6ddc8cef33c9cd91e0ebd2d97f164977839d5ebbd25812f63b164f`  
		Last Modified: Tue, 31 Aug 2021 21:24:52 GMT  
		Size: 2.8 MB (2813860 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.11.12` - linux; ppc64le

```console
$ docker pull alpine@sha256:024cea07734079c8314b158b62d95aee704fad739f90cc9a591219a07df45241
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.8 MB (2824904 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8e8a9c591b6e4cfb81c674148973d61329c69a45113429a2fc2d3e331e3699b9`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 02:43:12 GMT
ADD file:17477eee6878a950f2f16286dba9067b4f27ab5d90c29157e30d9bcaee1d9418 in / 
# Wed, 01 Sep 2021 02:43:15 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:6ff762d8ea8ca25e11609614eeba7af16b71db5d8ebea8bc7b802c275174f2ee`  
		Last Modified: Wed, 01 Sep 2021 02:44:15 GMT  
		Size: 2.8 MB (2824904 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `alpine:3.11.12` - linux; s390x

```console
$ docker pull alpine@sha256:5f82c1806d60592b747d6a53d35164b0c8f40e7ccf5425c1d59ae86ec8b3c0bc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **2.6 MB (2585711 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:45fcff441288bb31228e19a6cad26e26a454e5d5484f1f7115660517bd48cb29`
-	Default Command: `["\/bin\/sh"]`

```dockerfile
# Wed, 01 Sep 2021 01:15:38 GMT
ADD file:c1038db2fb356dc0acf4108117c20ba1da2a1546023e16a0905f49a968d7a1c7 in / 
# Wed, 01 Sep 2021 01:15:39 GMT
CMD ["/bin/sh"]
```

-	Layers:
	-	`sha256:518c147bd44d1caf10e5031a373df0ed439dcb012b636012aa159436f07e1ade`  
		Last Modified: Wed, 01 Sep 2021 01:16:37 GMT  
		Size: 2.6 MB (2585711 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
