## `photon:2.0`

```console
$ docker pull photon@sha256:ff8bea46f22762de5b24f1387e0dfaf31e2bdf77a6426c6440c70e88de8967c2
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `photon:2.0` - linux; amd64

```console
$ docker pull photon@sha256:d74dd5d821e639923da9fa5a338c26760c70ba5829f49e853cb92885e3a18675
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **14.8 MB (14791573 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8a91ced49815d3e08eddd5ba499e7d7fce1e50a6378509c25b37bba5679af4a4`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Mon, 01 Nov 2021 19:02:12 GMT
ADD file:687647177ab76904abb994c90ab28b95f3f7acad9b87715a11ed4e06e12576fe in / 
# Mon, 01 Nov 2021 19:02:12 GMT
LABEL name=Photon OS 2.0 Base Image vendor=VMware build-date=20211029
# Mon, 01 Nov 2021 19:02:12 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:b93b70a4922e50c5d452688d644cd9386ffa7e3758115de941dc94ac2ef56311`  
		Last Modified: Mon, 01 Nov 2021 19:03:13 GMT  
		Size: 14.8 MB (14791573 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
