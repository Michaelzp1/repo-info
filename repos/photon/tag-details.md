<!-- THIS FILE IS GENERATED VIA './update-remote.sh' -->

# Tags of `photon`

-	[`photon:1.0`](#photon10)
-	[`photon:1.0-20211029`](#photon10-20211029)
-	[`photon:2.0`](#photon20)
-	[`photon:2.0-20211029`](#photon20-20211029)
-	[`photon:3.0`](#photon30)
-	[`photon:3.0-20211029`](#photon30-20211029)
-	[`photon:4.0`](#photon40)
-	[`photon:4.0-20211029`](#photon40-20211029)
-	[`photon:latest`](#photonlatest)

## `photon:1.0`

```console
$ docker pull photon@sha256:062db13edbd5ac9e9c7fedad1b7ebd0c6a0faf21492eb4fda76720e86124f31a
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `photon:1.0` - linux; amd64

```console
$ docker pull photon@sha256:3b36e4332a4323c939d3e8f289457326a1902627be2eba925e490c07a317303d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.4 MB (49447247 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:7fa5cf952ad5672d8901d417dbd4cdeec05759a52e1ee63cc5a584726e5b4362`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Mon, 01 Nov 2021 19:02:02 GMT
ADD file:4c5954b610d3a60c2e80e08b0a6ce927d1fcd50848056392acf30a4086f828f9 in / 
# Mon, 01 Nov 2021 19:02:03 GMT
LABEL name=Photon OS 1.0 Base Image vendor=VMware build-date=20211029
# Mon, 01 Nov 2021 19:02:03 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:cf94aa5b9ed6bb9820b202777fc07fed165aafd8f0115dde32bb709d27a2806e`  
		Last Modified: Mon, 01 Nov 2021 19:03:01 GMT  
		Size: 49.4 MB (49447247 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `photon:1.0-20211029`

```console
$ docker pull photon@sha256:062db13edbd5ac9e9c7fedad1b7ebd0c6a0faf21492eb4fda76720e86124f31a
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `photon:1.0-20211029` - linux; amd64

```console
$ docker pull photon@sha256:3b36e4332a4323c939d3e8f289457326a1902627be2eba925e490c07a317303d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.4 MB (49447247 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:7fa5cf952ad5672d8901d417dbd4cdeec05759a52e1ee63cc5a584726e5b4362`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Mon, 01 Nov 2021 19:02:02 GMT
ADD file:4c5954b610d3a60c2e80e08b0a6ce927d1fcd50848056392acf30a4086f828f9 in / 
# Mon, 01 Nov 2021 19:02:03 GMT
LABEL name=Photon OS 1.0 Base Image vendor=VMware build-date=20211029
# Mon, 01 Nov 2021 19:02:03 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:cf94aa5b9ed6bb9820b202777fc07fed165aafd8f0115dde32bb709d27a2806e`  
		Last Modified: Mon, 01 Nov 2021 19:03:01 GMT  
		Size: 49.4 MB (49447247 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `photon:2.0`

```console
$ docker pull photon@sha256:ff8bea46f22762de5b24f1387e0dfaf31e2bdf77a6426c6440c70e88de8967c2
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `photon:2.0` - linux; amd64

```console
$ docker pull photon@sha256:d74dd5d821e639923da9fa5a338c26760c70ba5829f49e853cb92885e3a18675
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **14.8 MB (14791573 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8a91ced49815d3e08eddd5ba499e7d7fce1e50a6378509c25b37bba5679af4a4`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Mon, 01 Nov 2021 19:02:12 GMT
ADD file:687647177ab76904abb994c90ab28b95f3f7acad9b87715a11ed4e06e12576fe in / 
# Mon, 01 Nov 2021 19:02:12 GMT
LABEL name=Photon OS 2.0 Base Image vendor=VMware build-date=20211029
# Mon, 01 Nov 2021 19:02:12 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:b93b70a4922e50c5d452688d644cd9386ffa7e3758115de941dc94ac2ef56311`  
		Last Modified: Mon, 01 Nov 2021 19:03:13 GMT  
		Size: 14.8 MB (14791573 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `photon:2.0-20211029`

```console
$ docker pull photon@sha256:ff8bea46f22762de5b24f1387e0dfaf31e2bdf77a6426c6440c70e88de8967c2
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `photon:2.0-20211029` - linux; amd64

```console
$ docker pull photon@sha256:d74dd5d821e639923da9fa5a338c26760c70ba5829f49e853cb92885e3a18675
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **14.8 MB (14791573 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8a91ced49815d3e08eddd5ba499e7d7fce1e50a6378509c25b37bba5679af4a4`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Mon, 01 Nov 2021 19:02:12 GMT
ADD file:687647177ab76904abb994c90ab28b95f3f7acad9b87715a11ed4e06e12576fe in / 
# Mon, 01 Nov 2021 19:02:12 GMT
LABEL name=Photon OS 2.0 Base Image vendor=VMware build-date=20211029
# Mon, 01 Nov 2021 19:02:12 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:b93b70a4922e50c5d452688d644cd9386ffa7e3758115de941dc94ac2ef56311`  
		Last Modified: Mon, 01 Nov 2021 19:03:13 GMT  
		Size: 14.8 MB (14791573 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `photon:3.0`

```console
$ docker pull photon@sha256:65ad52cdd6591b0e29089a33f00a9b1ae905c4945dadb0078b55deca06fbbe61
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 2
	-	linux; amd64
	-	linux; arm64 variant v8

### `photon:3.0` - linux; amd64

```console
$ docker pull photon@sha256:ccc82048a71dfc101b65a77e5c31d3552bee6ea9f02376bc4512719a1ebc2727
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **15.8 MB (15779562 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1c8fd7be337bb4f51986baae74ca1a8e681deff0452f5650f4345ebbb20d7033`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Mon, 01 Nov 2021 19:01:47 GMT
ADD file:ca5a99c98e466c42efd02c38473d43d519af1a0d015477a2d93b61efebb5504c in / 
# Mon, 01 Nov 2021 19:01:48 GMT
LABEL name=Photon OS x86_64/3.0 Base Image vendor=VMware build-date=20211029
# Mon, 01 Nov 2021 19:01:48 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:41648b5883b869765cb52de8bace5610a6318afb5452137b5165c378a0d908cb`  
		Last Modified: Mon, 01 Nov 2021 19:02:45 GMT  
		Size: 15.8 MB (15779562 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `photon:3.0` - linux; arm64 variant v8

```console
$ docker pull photon@sha256:eea4338e046bb75f493acaa65999f99a72ebf0e702032c67189f25c087d98413
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **13.5 MB (13536593 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b065cffae0c5a9c9866a4e0b76ec0f358bd90aee8eee26709294c9910bbeef9c`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Mon, 01 Nov 2021 19:36:07 GMT
ADD file:73affef6951133b69ffe5af5f17d79fca39a907762b660fb8a3836ca33da2bee in / 
# Mon, 01 Nov 2021 19:36:07 GMT
LABEL name=Photon OS aarch64/3.0 Base Image vendor=VMware build-date=20211029
# Mon, 01 Nov 2021 19:36:08 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:cac2ff8c889e872d8f2474aedeadedff8f241bd9f2ad828c0c74d19c13f3e889`  
		Last Modified: Mon, 01 Nov 2021 19:36:43 GMT  
		Size: 13.5 MB (13536593 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `photon:3.0-20211029`

```console
$ docker pull photon@sha256:65ad52cdd6591b0e29089a33f00a9b1ae905c4945dadb0078b55deca06fbbe61
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 2
	-	linux; amd64
	-	linux; arm64 variant v8

### `photon:3.0-20211029` - linux; amd64

```console
$ docker pull photon@sha256:ccc82048a71dfc101b65a77e5c31d3552bee6ea9f02376bc4512719a1ebc2727
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **15.8 MB (15779562 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1c8fd7be337bb4f51986baae74ca1a8e681deff0452f5650f4345ebbb20d7033`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Mon, 01 Nov 2021 19:01:47 GMT
ADD file:ca5a99c98e466c42efd02c38473d43d519af1a0d015477a2d93b61efebb5504c in / 
# Mon, 01 Nov 2021 19:01:48 GMT
LABEL name=Photon OS x86_64/3.0 Base Image vendor=VMware build-date=20211029
# Mon, 01 Nov 2021 19:01:48 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:41648b5883b869765cb52de8bace5610a6318afb5452137b5165c378a0d908cb`  
		Last Modified: Mon, 01 Nov 2021 19:02:45 GMT  
		Size: 15.8 MB (15779562 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `photon:3.0-20211029` - linux; arm64 variant v8

```console
$ docker pull photon@sha256:eea4338e046bb75f493acaa65999f99a72ebf0e702032c67189f25c087d98413
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **13.5 MB (13536593 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b065cffae0c5a9c9866a4e0b76ec0f358bd90aee8eee26709294c9910bbeef9c`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Mon, 01 Nov 2021 19:36:07 GMT
ADD file:73affef6951133b69ffe5af5f17d79fca39a907762b660fb8a3836ca33da2bee in / 
# Mon, 01 Nov 2021 19:36:07 GMT
LABEL name=Photon OS aarch64/3.0 Base Image vendor=VMware build-date=20211029
# Mon, 01 Nov 2021 19:36:08 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:cac2ff8c889e872d8f2474aedeadedff8f241bd9f2ad828c0c74d19c13f3e889`  
		Last Modified: Mon, 01 Nov 2021 19:36:43 GMT  
		Size: 13.5 MB (13536593 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `photon:4.0`

```console
$ docker pull photon@sha256:e895242ac472a83d1adc8576267818c5701bf844a784e13cd80627a12e8dd708
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 2
	-	linux; amd64
	-	linux; arm64 variant v8

### `photon:4.0` - linux; amd64

```console
$ docker pull photon@sha256:955c8907a76f84e656b698913eab9a96a08eb23743dcd328518475639dd9500e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **16.0 MB (15971814 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a1297f6546accd6725f4501b8ec101b0c615cb51e7be8ec9bf140da9f050c205`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Mon, 01 Nov 2021 19:01:41 GMT
ADD file:b7e352405c142091d518c44b65a3932ec47ccf2b0568027be048a580695f151e in / 
# Mon, 01 Nov 2021 19:01:41 GMT
LABEL name=Photon OS x86_64/4.0 Base Image vendor=VMware build-date=20211029
# Mon, 01 Nov 2021 19:01:42 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:53cfa89c520c131f17bc57b64c3b3d190f1dc8666010ce2ffd9b09011665bb1b`  
		Last Modified: Mon, 01 Nov 2021 19:02:31 GMT  
		Size: 16.0 MB (15971814 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `photon:4.0` - linux; arm64 variant v8

```console
$ docker pull photon@sha256:a51a7f16d9173af3cff51de6036bfd28270cd6edce00c90238828c568d33b707
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **15.0 MB (15006227 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:869ac28def796e4a9906633a78a77624d018d7ad9498ef8ad2bc7b581f04c80c`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Mon, 01 Nov 2021 19:35:59 GMT
ADD file:dea9e85d20ecc6a5e64c8ba7a1f27d98946706d3418082cf76bd522ce6d71c74 in / 
# Mon, 01 Nov 2021 19:35:59 GMT
LABEL name=Photon OS aarch64/4.0 Base Image vendor=VMware build-date=20211029
# Mon, 01 Nov 2021 19:36:00 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:93159618a592dc6987362a88048bd615c37890bfc561618c5d9ca98f86bfe8bf`  
		Last Modified: Mon, 01 Nov 2021 19:36:28 GMT  
		Size: 15.0 MB (15006227 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `photon:4.0-20211029`

```console
$ docker pull photon@sha256:e895242ac472a83d1adc8576267818c5701bf844a784e13cd80627a12e8dd708
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 2
	-	linux; amd64
	-	linux; arm64 variant v8

### `photon:4.0-20211029` - linux; amd64

```console
$ docker pull photon@sha256:955c8907a76f84e656b698913eab9a96a08eb23743dcd328518475639dd9500e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **16.0 MB (15971814 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a1297f6546accd6725f4501b8ec101b0c615cb51e7be8ec9bf140da9f050c205`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Mon, 01 Nov 2021 19:01:41 GMT
ADD file:b7e352405c142091d518c44b65a3932ec47ccf2b0568027be048a580695f151e in / 
# Mon, 01 Nov 2021 19:01:41 GMT
LABEL name=Photon OS x86_64/4.0 Base Image vendor=VMware build-date=20211029
# Mon, 01 Nov 2021 19:01:42 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:53cfa89c520c131f17bc57b64c3b3d190f1dc8666010ce2ffd9b09011665bb1b`  
		Last Modified: Mon, 01 Nov 2021 19:02:31 GMT  
		Size: 16.0 MB (15971814 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `photon:4.0-20211029` - linux; arm64 variant v8

```console
$ docker pull photon@sha256:a51a7f16d9173af3cff51de6036bfd28270cd6edce00c90238828c568d33b707
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **15.0 MB (15006227 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:869ac28def796e4a9906633a78a77624d018d7ad9498ef8ad2bc7b581f04c80c`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Mon, 01 Nov 2021 19:35:59 GMT
ADD file:dea9e85d20ecc6a5e64c8ba7a1f27d98946706d3418082cf76bd522ce6d71c74 in / 
# Mon, 01 Nov 2021 19:35:59 GMT
LABEL name=Photon OS aarch64/4.0 Base Image vendor=VMware build-date=20211029
# Mon, 01 Nov 2021 19:36:00 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:93159618a592dc6987362a88048bd615c37890bfc561618c5d9ca98f86bfe8bf`  
		Last Modified: Mon, 01 Nov 2021 19:36:28 GMT  
		Size: 15.0 MB (15006227 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `photon:latest`

```console
$ docker pull photon@sha256:e895242ac472a83d1adc8576267818c5701bf844a784e13cd80627a12e8dd708
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 2
	-	linux; amd64
	-	linux; arm64 variant v8

### `photon:latest` - linux; amd64

```console
$ docker pull photon@sha256:955c8907a76f84e656b698913eab9a96a08eb23743dcd328518475639dd9500e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **16.0 MB (15971814 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a1297f6546accd6725f4501b8ec101b0c615cb51e7be8ec9bf140da9f050c205`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Mon, 01 Nov 2021 19:01:41 GMT
ADD file:b7e352405c142091d518c44b65a3932ec47ccf2b0568027be048a580695f151e in / 
# Mon, 01 Nov 2021 19:01:41 GMT
LABEL name=Photon OS x86_64/4.0 Base Image vendor=VMware build-date=20211029
# Mon, 01 Nov 2021 19:01:42 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:53cfa89c520c131f17bc57b64c3b3d190f1dc8666010ce2ffd9b09011665bb1b`  
		Last Modified: Mon, 01 Nov 2021 19:02:31 GMT  
		Size: 16.0 MB (15971814 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `photon:latest` - linux; arm64 variant v8

```console
$ docker pull photon@sha256:a51a7f16d9173af3cff51de6036bfd28270cd6edce00c90238828c568d33b707
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **15.0 MB (15006227 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:869ac28def796e4a9906633a78a77624d018d7ad9498ef8ad2bc7b581f04c80c`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Mon, 01 Nov 2021 19:35:59 GMT
ADD file:dea9e85d20ecc6a5e64c8ba7a1f27d98946706d3418082cf76bd522ce6d71c74 in / 
# Mon, 01 Nov 2021 19:35:59 GMT
LABEL name=Photon OS aarch64/4.0 Base Image vendor=VMware build-date=20211029
# Mon, 01 Nov 2021 19:36:00 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:93159618a592dc6987362a88048bd615c37890bfc561618c5d9ca98f86bfe8bf`  
		Last Modified: Mon, 01 Nov 2021 19:36:28 GMT  
		Size: 15.0 MB (15006227 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
