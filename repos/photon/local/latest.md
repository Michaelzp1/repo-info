# `photon:4.0`

## Docker Metadata

- Image ID: `sha256:a1297f6546accd6725f4501b8ec101b0c615cb51e7be8ec9bf140da9f050c205`
- Created: `2021-11-01T19:01:42.143540206Z`
- Virtual Size: ~ 36.58 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["/bin/bash"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
- Labels:
  - `build-date=20211029`
  - `name=Photon OS x86_64/4.0 Base Image`
  - `vendor=VMware`

## `rpm` (`.rpm`-based packages)

### `rpm` package: `bash-5.0-2.ph4.x86_64`

Licenses (from `rpm --query`): GPLv3

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `bzip2-libs-1.0.8-4.ph4.x86_64`

Licenses (from `rpm --query`): BSD

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `ca-certificates-20201001-2.ph4.x86_64`

Licenses (from `rpm --query`): Custom

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `ca-certificates-pki-20201001-2.ph4.x86_64`

Licenses (from `rpm --query`): Custom

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `curl-7.78.0-2.ph4.x86_64`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `curl-libs-7.78.0-2.ph4.x86_64`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `e2fsprogs-libs-1.45.6-2.ph4.x86_64`

Licenses (from `rpm --query`): GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `elfutils-libelf-0.181-4.ph4.x86_64`

Licenses (from `rpm --query`): GPLv2+ or LGPLv3+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `expat-2.2.9-3.ph4.x86_64`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `expat-libs-2.2.9-3.ph4.x86_64`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `filesystem-1.1-4.ph4.x86_64`

Licenses (from `rpm --query`): GPLv3

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `glibc-2.32-10.ph4.x86_64`

Licenses (from `rpm --query`): LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `gpg-pubkey-66fd4949-4803fe57`

Licenses (from `rpm --query`): pubkey

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `krb5-1.17-5.ph4.x86_64`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libcap-2.43-1.ph4.x86_64`

Licenses (from `rpm --query`): GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libdb-5.3.28-2.ph4.x86_64`

Licenses (from `rpm --query`): BSD and LGPLv2 and Sleepycat

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libgcc-10.2.0-1.ph4.x86_64`

Licenses (from `rpm --query`): GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libgcrypt-1.9.3-1.ph4.x86_64`

Licenses (from `rpm --query`): GPLv2+ and LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libgpg-error-1.39-1.ph4.x86_64`

Licenses (from `rpm --query`): GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libmetalink-0.1.3-3.ph4.x86_64`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libsolv-0.7.19-1.ph4.x86_64`

Licenses (from `rpm --query`): BSD

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `libssh2-1.9.0-3.ph4.x86_64`

Licenses (from `rpm --query`): BSD

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `lua-5.4.3-1.ph4.x86_64`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `ncurses-libs-6.2-3.ph4.x86_64`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `nspr-4.30-1.ph4.x86_64`

Licenses (from `rpm --query`): MPLv2.0

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `nss-libs-3.66-1.ph4.x86_64`

Licenses (from `rpm --query`): MPLv2.0

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `openssl-1.1.1l-1.ph4.x86_64`

Licenses (from `rpm --query`): OpenSSL

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `photon-release-4.0-1.ph4.noarch`

Licenses (from `rpm --query`): Apache License

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `photon-repos-4.0-2.ph4.noarch`

Licenses (from `rpm --query`): Apache License

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `popt-1.16-5.ph4.x86_64`

Licenses (from `rpm --query`): MIT

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `readline-7.0-3.ph4.x86_64`

Licenses (from `rpm --query`): GPLv3+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `rpm-libs-4.16.1.2-8.ph4.x86_64`

Licenses (from `rpm --query`): GPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `sqlite-libs-3.33.0-1.ph4.x86_64`

Licenses (from `rpm --query`): Public Domain

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `tdnf-3.1.5-1.ph4.x86_64`

Licenses (from `rpm --query`): LGPLv2.1,GPLv2

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `tdnf-cli-libs-3.1.5-1.ph4.x86_64`

Licenses (from `rpm --query`): LGPLv2.1,GPLv2

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `toybox-0.8.3-2.ph4.x86_64`

Licenses (from `rpm --query`): BSD

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `xz-libs-5.2.5-1.ph4.x86_64`

Licenses (from `rpm --query`): GPLv2+ and GPLv3+ and LGPLv2+

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `zlib-1.2.11-2.ph4.x86_64`

Licenses (from `rpm --query`): zlib

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!

### `rpm` package: `zstd-libs-1.4.5-2.ph4.x86_64`

Licenses (from `rpm --query`): BSD and GPLv2

**WARNING:** unable to find source (`yumdownloader` failed or returned no results)!
