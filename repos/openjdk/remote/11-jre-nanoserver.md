## `openjdk:11-jre-nanoserver`

```console
$ docker pull openjdk@sha256:5c673379d30489fcb59d5e6450b0e9f65023379ab5acd34392ed7fc6e4e3d9d9
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	windows version 10.0.17763.2237; amd64

### `openjdk:11-jre-nanoserver` - windows version 10.0.17763.2237; amd64

```console
$ docker pull openjdk@sha256:471826a1bfc12fed4261a210f6971ef08439d8f556816d0b076d71360902da2b
```

-	Docker Version: 20.10.8
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **142.2 MB (142207346 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:079aab64602732b35816113477467df9ca1b0729909e9135ebd8c7359ea4d819`
-	Default Command: `["c:\\windows\\system32\\cmd.exe"]`
-	`SHELL`: `["cmd","\/s","\/c"]`

```dockerfile
# Thu, 07 Oct 2021 08:01:14 GMT
RUN Apply image 1809-amd64
# Wed, 13 Oct 2021 18:17:55 GMT
SHELL [cmd /s /c]
# Thu, 14 Oct 2021 00:59:21 GMT
ENV JAVA_HOME=C:\openjdk-11
# Thu, 14 Oct 2021 00:59:22 GMT
USER ContainerAdministrator
# Thu, 14 Oct 2021 00:59:32 GMT
RUN echo Updating PATH: %JAVA_HOME%\bin;%PATH% 	&& setx /M PATH %JAVA_HOME%\bin;%PATH% 	&& echo Complete.
# Thu, 14 Oct 2021 00:59:32 GMT
USER ContainerUser
# Thu, 21 Oct 2021 23:27:05 GMT
ENV JAVA_VERSION=11.0.13
# Thu, 21 Oct 2021 23:32:00 GMT
COPY dir:63916d6bee2220e36f2a9872b4f6dbefd913ce14199f5f87aa18e7a5987717fa in C:\openjdk-11 
# Thu, 21 Oct 2021 23:32:19 GMT
RUN echo Verifying install ... 	&& echo   java --version && java --version 	&& echo Complete.
```

-	Layers:
	-	`sha256:934e212983f208dc2bebc5de38259a6a62f1761868aacfee2cb3585a13b1e24b`  
		Size: 102.7 MB (102661372 bytes)  
		MIME: application/vnd.docker.image.rootfs.foreign.diff.tar.gzip
	-	`sha256:ceff24c3c0c2c360cd3788fcde5c41e1d6601339f4944623c732558a6030e631`  
		Last Modified: Wed, 13 Oct 2021 19:12:58 GMT  
		Size: 1.1 KB (1058 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:43342c8994cfe8e109320781e376005858ed6af7b5e15090c692a48ddee1c9d1`  
		Last Modified: Sat, 16 Oct 2021 00:48:14 GMT  
		Size: 1.2 KB (1175 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:1cf3a714ac5c06b6ff3556dd1e4f015c50841a0819a87a526dcbdc6dbf295478`  
		Last Modified: Sat, 16 Oct 2021 00:48:14 GMT  
		Size: 1.2 KB (1155 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:53d5e43d0b80f79ad39e7b8a9d5f3b829595776b1abdf47c07fe87891d489583`  
		Last Modified: Sat, 16 Oct 2021 00:48:14 GMT  
		Size: 75.1 KB (75090 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:20aa72adc10eb6d0803d15b38f310dd050e3662bf077f683fea6ff85482f1a69`  
		Last Modified: Sat, 16 Oct 2021 00:48:12 GMT  
		Size: 1.2 KB (1162 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:37e6b5288ff0d6fb76adfaa52452f0fba92ab92936e6793b14e0264de7152a12`  
		Last Modified: Thu, 21 Oct 2021 23:48:54 GMT  
		Size: 1.1 KB (1137 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:338af1dfed56fbae81de1b7a5bde3efd6a5434ad47d710a03dc3f69bef3e2e92`  
		Last Modified: Thu, 21 Oct 2021 23:54:10 GMT  
		Size: 39.4 MB (39417941 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:448bf2ffa1f2f3769bc79aa24d0402bd4076dee01ff2acb51d422b86aa91cc67`  
		Last Modified: Thu, 21 Oct 2021 23:53:26 GMT  
		Size: 47.3 KB (47256 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
