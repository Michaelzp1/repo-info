# `openjdk:17.0.1-jdk-oraclelinux8`

## Docker Metadata

- Image ID: `sha256:b002702b7d015a0cfe17b6d7679c903fc5fecb65c906a7ad1a19aa98d51c2d1b`
- Created: `2021-10-28T01:46:11.060375715Z`
- Virtual Size: ~ 470.59 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["jshell"]`
- Environment:
  - `PATH=/usr/java/openjdk-17/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `JAVA_HOME=/usr/java/openjdk-17`
  - `LANG=C.UTF-8`
  - `JAVA_VERSION=17.0.1`
