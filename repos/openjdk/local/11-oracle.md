# `openjdk:11.0.13-jdk-oraclelinux8`

## Docker Metadata

- Image ID: `sha256:f49a94dd812700750252e3c8315dd060fbf65fd8bcf5ffefd5653f78f1138032`
- Created: `2021-10-28T01:47:09.176885008Z`
- Virtual Size: ~ 492.53 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["jshell"]`
- Environment:
  - `PATH=/usr/java/openjdk-11/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `JAVA_HOME=/usr/java/openjdk-11`
  - `LANG=C.UTF-8`
  - `JAVA_VERSION=11.0.13`
