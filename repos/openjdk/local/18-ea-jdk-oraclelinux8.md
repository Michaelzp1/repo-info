# `openjdk:18-ea-21-jdk-oraclelinux8`

## Docker Metadata

- Image ID: `sha256:22ff2325c1eedb17c158c957f2231200134cd5d9f9569fcce932b4a99a8f5cb2`
- Created: `2021-11-01T18:49:16.40859708Z`
- Virtual Size: ~ 471.97 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["jshell"]`
- Environment:
  - `PATH=/usr/java/openjdk-18/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `JAVA_HOME=/usr/java/openjdk-18`
  - `LANG=C.UTF-8`
  - `JAVA_VERSION=18-ea+21`
