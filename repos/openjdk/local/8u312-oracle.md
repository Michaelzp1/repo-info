# `openjdk:8u312-jdk-oraclelinux8`

## Docker Metadata

- Image ID: `sha256:6d0f88085ff5f62611f6cbd12a4854c2e97af260d393237a24c105ec30ad49d2`
- Created: `2021-10-28T01:48:11.243602827Z`
- Virtual Size: ~ 359.07 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["/bin/bash"]`
- Environment:
  - `PATH=/usr/java/openjdk-8/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `JAVA_HOME=/usr/java/openjdk-8`
  - `LANG=C.UTF-8`
  - `JAVA_VERSION=8u312`
