## `perl:5.30-slim-stretch`

```console
$ docker pull perl@sha256:3fc8cb14500b05ab938cc7343252ce9411c58e9ce7d78ee69e39f8629ef0e67d
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 4
	-	linux; amd64
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386

### `perl:5.30-slim-stretch` - linux; amd64

```console
$ docker pull perl@sha256:a6000de5cb6b55e1e44a84e0ba2c46f6d4edecb2e70bb4548d83de9b8790d9ed
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **36.9 MB (36867688 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b2a658b278d3fdd99f341dcc50634e35ee81c58deabce0e393320977f4a0d1e4`
-	Default Command: `["perl5.30.3","-de0"]`

```dockerfile
# Tue, 12 Oct 2021 01:22:52 GMT
ADD file:1220579e31585dec45ca8e35874eb689018ed026a1f23b7906f791c0279671e0 in / 
# Tue, 12 Oct 2021 01:22:53 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 13:16:09 GMT
LABEL maintainer=Peter Martini <PeterCMartini@GMail.com>, Zak B. Elep <zakame@cpan.org>
# Tue, 12 Oct 2021 13:55:55 GMT
COPY file:afd976b0034f036eb2b1d47802c3178315416ae93c0fe19174e7315afc1a8cc6 in /usr/src/perl/ 
# Tue, 12 Oct 2021 13:55:56 GMT
WORKDIR /usr/src/perl
# Tue, 12 Oct 2021 14:04:17 GMT
RUN apt-get update     && apt-get install -y --no-install-recommends        bzip2        ca-certificates        curl        dpkg-dev        gcc        libc6-dev        make        netbase        patch        xz-utils     && curl -SL https://www.cpan.org/src/5.0/perl-5.30.3.tar.xz -o perl-5.30.3.tar.xz     && echo '6967595f2e3f3a94544c35152f9a25e0cb8ea24ae45f4bf1882f2e33f4a400f4 *perl-5.30.3.tar.xz' | sha256sum -c -     && tar --strip-components=1 -xaf perl-5.30.3.tar.xz -C /usr/src/perl     && rm perl-5.30.3.tar.xz     && cat *.patch | patch -p1     && gnuArch="$(dpkg-architecture --query DEB_BUILD_GNU_TYPE)"     && archBits="$(dpkg-architecture --query DEB_BUILD_ARCH_BITS)"     && archFlag="$([ "$archBits" = '64' ] && echo '-Duse64bitall' || echo '-Duse64bitint')"     && ./Configure -Darchname="$gnuArch" "$archFlag" -Duseshrplib -Dvendorprefix=/usr/local  -des     && make -j$(nproc)     && TEST_JOBS=$(nproc) make test_harness     && make install     && cd /usr/src     && curl -LO https://www.cpan.org/authors/id/M/MI/MIYAGAWA/App-cpanminus-1.7044.tar.gz     && echo '9b60767fe40752ef7a9d3f13f19060a63389a5c23acc3e9827e19b75500f81f3 *App-cpanminus-1.7044.tar.gz' | sha256sum -c -     && tar -xzf App-cpanminus-1.7044.tar.gz && cd App-cpanminus-1.7044 && perl bin/cpanm . && cd /root     && savedPackages="make netbase"     && apt-mark auto '.*' > /dev/null     && apt-mark manual $savedPackages     && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false     && rm -fr /var/cache/apt/* /var/lib/apt/lists/*     && rm -fr ./cpanm /root/.cpanm /usr/src/perl /usr/src/App-cpanminus-1.7044* /tmp/*
# Tue, 12 Oct 2021 14:04:17 GMT
WORKDIR /
# Tue, 12 Oct 2021 14:04:17 GMT
CMD ["perl5.30.3" "-de0"]
```

-	Layers:
	-	`sha256:eec53b8a5053c739b5b685cb372b38eea3286ab6626532bad963291f76357c5f`  
		Last Modified: Tue, 12 Oct 2021 01:29:50 GMT  
		Size: 22.5 MB (22527572 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:ac3eb20a9f64112f3add85a44215bae22c545771a920b4773fd896c1fc2abe53`  
		Last Modified: Tue, 12 Oct 2021 14:28:01 GMT  
		Size: 616.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:03a49db037b92a6fdc1554c62c174c5fad747d150423542edcdb53ad6cd37ece`  
		Last Modified: Tue, 12 Oct 2021 14:28:04 GMT  
		Size: 14.3 MB (14339500 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `perl:5.30-slim-stretch` - linux; arm variant v7

```console
$ docker pull perl@sha256:b38cbec8d55a6dd67c72bb720b40c6a322e5b6f82a29b57beb0d9e7d5c304dad
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **32.8 MB (32793034 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8007b776a7c5585815ef0f6105efacb216400eec9574369d38a268a7771e7b66`
-	Default Command: `["perl5.30.3","-de0"]`

```dockerfile
# Tue, 12 Oct 2021 01:34:42 GMT
ADD file:9bfcfd0aaac802b902b0e842e040a6599c461c90b73579bcacc2fbdda7ec39cb in / 
# Tue, 12 Oct 2021 01:34:42 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 08:32:26 GMT
LABEL maintainer=Peter Martini <PeterCMartini@GMail.com>, Zak B. Elep <zakame@cpan.org>
# Tue, 12 Oct 2021 09:19:57 GMT
COPY file:afd976b0034f036eb2b1d47802c3178315416ae93c0fe19174e7315afc1a8cc6 in /usr/src/perl/ 
# Tue, 12 Oct 2021 09:19:57 GMT
WORKDIR /usr/src/perl
# Tue, 12 Oct 2021 09:32:01 GMT
RUN apt-get update     && apt-get install -y --no-install-recommends        bzip2        ca-certificates        curl        dpkg-dev        gcc        libc6-dev        make        netbase        patch        xz-utils     && curl -SL https://www.cpan.org/src/5.0/perl-5.30.3.tar.xz -o perl-5.30.3.tar.xz     && echo '6967595f2e3f3a94544c35152f9a25e0cb8ea24ae45f4bf1882f2e33f4a400f4 *perl-5.30.3.tar.xz' | sha256sum -c -     && tar --strip-components=1 -xaf perl-5.30.3.tar.xz -C /usr/src/perl     && rm perl-5.30.3.tar.xz     && cat *.patch | patch -p1     && gnuArch="$(dpkg-architecture --query DEB_BUILD_GNU_TYPE)"     && archBits="$(dpkg-architecture --query DEB_BUILD_ARCH_BITS)"     && archFlag="$([ "$archBits" = '64' ] && echo '-Duse64bitall' || echo '-Duse64bitint')"     && ./Configure -Darchname="$gnuArch" "$archFlag" -Duseshrplib -Dvendorprefix=/usr/local  -des     && make -j$(nproc)     && TEST_JOBS=$(nproc) make test_harness     && make install     && cd /usr/src     && curl -LO https://www.cpan.org/authors/id/M/MI/MIYAGAWA/App-cpanminus-1.7044.tar.gz     && echo '9b60767fe40752ef7a9d3f13f19060a63389a5c23acc3e9827e19b75500f81f3 *App-cpanminus-1.7044.tar.gz' | sha256sum -c -     && tar -xzf App-cpanminus-1.7044.tar.gz && cd App-cpanminus-1.7044 && perl bin/cpanm . && cd /root     && savedPackages="make netbase"     && apt-mark auto '.*' > /dev/null     && apt-mark manual $savedPackages     && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false     && rm -fr /var/cache/apt/* /var/lib/apt/lists/*     && rm -fr ./cpanm /root/.cpanm /usr/src/perl /usr/src/App-cpanminus-1.7044* /tmp/*
# Tue, 12 Oct 2021 09:32:02 GMT
WORKDIR /
# Tue, 12 Oct 2021 09:32:02 GMT
CMD ["perl5.30.3" "-de0"]
```

-	Layers:
	-	`sha256:a1a3620b17011bd36d6f64dfcc8fd7c4cb3da78d19a59efb1b35afcadaf3f6a8`  
		Last Modified: Tue, 12 Oct 2021 01:51:59 GMT  
		Size: 19.3 MB (19316474 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:4f3183db22d7b09f78a5cc52a057913b41308c94a54952776ef1748bb84eda59`  
		Last Modified: Tue, 12 Oct 2021 10:08:07 GMT  
		Size: 620.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:4c4e344e400cefb67bfcd96b78e06b70bdbc1c61d42d64cabc047b45e7d2a9d1`  
		Last Modified: Tue, 12 Oct 2021 10:08:24 GMT  
		Size: 13.5 MB (13475940 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `perl:5.30-slim-stretch` - linux; arm64 variant v8

```console
$ docker pull perl@sha256:1e4c501cb246a6f2f425e5be882d3275a587e94c4907f771b6bff46da87c9e3f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **34.6 MB (34554575 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:92b72c36d1a8ae93805c6a58d2e6e9b0c73482a374939634d2bd4a879dea1eb7`
-	Default Command: `["perl5.30.3","-de0"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:29 GMT
ADD file:ebfc214a3da7b706f0759fd22fa991c905976d2c970b2d59d134753f7cbd5e06 in / 
# Tue, 12 Oct 2021 01:43:29 GMT
CMD ["bash"]
# Wed, 13 Oct 2021 12:18:30 GMT
LABEL maintainer=Peter Martini <PeterCMartini@GMail.com>, Zak B. Elep <zakame@cpan.org>
# Wed, 13 Oct 2021 12:51:30 GMT
COPY file:afd976b0034f036eb2b1d47802c3178315416ae93c0fe19174e7315afc1a8cc6 in /usr/src/perl/ 
# Wed, 13 Oct 2021 12:51:30 GMT
WORKDIR /usr/src/perl
# Wed, 13 Oct 2021 12:55:20 GMT
RUN apt-get update     && apt-get install -y --no-install-recommends        bzip2        ca-certificates        curl        dpkg-dev        gcc        libc6-dev        make        netbase        patch        xz-utils     && curl -SL https://www.cpan.org/src/5.0/perl-5.30.3.tar.xz -o perl-5.30.3.tar.xz     && echo '6967595f2e3f3a94544c35152f9a25e0cb8ea24ae45f4bf1882f2e33f4a400f4 *perl-5.30.3.tar.xz' | sha256sum -c -     && tar --strip-components=1 -xaf perl-5.30.3.tar.xz -C /usr/src/perl     && rm perl-5.30.3.tar.xz     && cat *.patch | patch -p1     && gnuArch="$(dpkg-architecture --query DEB_BUILD_GNU_TYPE)"     && archBits="$(dpkg-architecture --query DEB_BUILD_ARCH_BITS)"     && archFlag="$([ "$archBits" = '64' ] && echo '-Duse64bitall' || echo '-Duse64bitint')"     && ./Configure -Darchname="$gnuArch" "$archFlag" -Duseshrplib -Dvendorprefix=/usr/local  -des     && make -j$(nproc)     && TEST_JOBS=$(nproc) make test_harness     && make install     && cd /usr/src     && curl -LO https://www.cpan.org/authors/id/M/MI/MIYAGAWA/App-cpanminus-1.7044.tar.gz     && echo '9b60767fe40752ef7a9d3f13f19060a63389a5c23acc3e9827e19b75500f81f3 *App-cpanminus-1.7044.tar.gz' | sha256sum -c -     && tar -xzf App-cpanminus-1.7044.tar.gz && cd App-cpanminus-1.7044 && perl bin/cpanm . && cd /root     && savedPackages="make netbase"     && apt-mark auto '.*' > /dev/null     && apt-mark manual $savedPackages     && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false     && rm -fr /var/cache/apt/* /var/lib/apt/lists/*     && rm -fr ./cpanm /root/.cpanm /usr/src/perl /usr/src/App-cpanminus-1.7044* /tmp/*
# Wed, 13 Oct 2021 12:55:20 GMT
WORKDIR /
# Wed, 13 Oct 2021 12:55:21 GMT
CMD ["perl5.30.3" "-de0"]
```

-	Layers:
	-	`sha256:0b0a22641ad2aad782c696887c59bf05ec0b1e7aa1e07902ee51949bab802657`  
		Last Modified: Tue, 12 Oct 2021 01:52:33 GMT  
		Size: 20.4 MB (20389450 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:89669e290c19e3b449740230e5e2a27c7a209781aedccd2ad6b896743556f4d8`  
		Last Modified: Wed, 13 Oct 2021 13:20:10 GMT  
		Size: 588.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:83e87b04aabfecb4bd22888d78992946cb2ec95f7e0df3976011da4bfb2115bc`  
		Last Modified: Wed, 13 Oct 2021 13:20:15 GMT  
		Size: 14.2 MB (14164537 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `perl:5.30-slim-stretch` - linux; 386

```console
$ docker pull perl@sha256:4180259644163e3fc6550d868ac2b2ebec8783c65868ed65444fa59ad59baf94
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **37.0 MB (36983165 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cd5d62c0daeee221309fa477e69e6ce2b61803b2999ac1737de855f99dfba5b3`
-	Default Command: `["perl5.30.3","-de0"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:34 GMT
ADD file:4420d4cd2022d6bad7f61fbfaf16f0759ca6c30424974c6076dc7d5c09910d66 in / 
# Tue, 12 Oct 2021 01:42:34 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 03:17:09 GMT
LABEL maintainer=Peter Martini <PeterCMartini@GMail.com>, Zak B. Elep <zakame@cpan.org>
# Tue, 12 Oct 2021 03:58:59 GMT
COPY file:afd976b0034f036eb2b1d47802c3178315416ae93c0fe19174e7315afc1a8cc6 in /usr/src/perl/ 
# Tue, 12 Oct 2021 03:58:59 GMT
WORKDIR /usr/src/perl
# Tue, 12 Oct 2021 04:07:22 GMT
RUN apt-get update     && apt-get install -y --no-install-recommends        bzip2        ca-certificates        curl        dpkg-dev        gcc        libc6-dev        make        netbase        patch        xz-utils     && curl -SL https://www.cpan.org/src/5.0/perl-5.30.3.tar.xz -o perl-5.30.3.tar.xz     && echo '6967595f2e3f3a94544c35152f9a25e0cb8ea24ae45f4bf1882f2e33f4a400f4 *perl-5.30.3.tar.xz' | sha256sum -c -     && tar --strip-components=1 -xaf perl-5.30.3.tar.xz -C /usr/src/perl     && rm perl-5.30.3.tar.xz     && cat *.patch | patch -p1     && gnuArch="$(dpkg-architecture --query DEB_BUILD_GNU_TYPE)"     && archBits="$(dpkg-architecture --query DEB_BUILD_ARCH_BITS)"     && archFlag="$([ "$archBits" = '64' ] && echo '-Duse64bitall' || echo '-Duse64bitint')"     && ./Configure -Darchname="$gnuArch" "$archFlag" -Duseshrplib -Dvendorprefix=/usr/local  -des     && make -j$(nproc)     && TEST_JOBS=$(nproc) make test_harness     && make install     && cd /usr/src     && curl -LO https://www.cpan.org/authors/id/M/MI/MIYAGAWA/App-cpanminus-1.7044.tar.gz     && echo '9b60767fe40752ef7a9d3f13f19060a63389a5c23acc3e9827e19b75500f81f3 *App-cpanminus-1.7044.tar.gz' | sha256sum -c -     && tar -xzf App-cpanminus-1.7044.tar.gz && cd App-cpanminus-1.7044 && perl bin/cpanm . && cd /root     && savedPackages="make netbase"     && apt-mark auto '.*' > /dev/null     && apt-mark manual $savedPackages     && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false     && rm -fr /var/cache/apt/* /var/lib/apt/lists/*     && rm -fr ./cpanm /root/.cpanm /usr/src/perl /usr/src/App-cpanminus-1.7044* /tmp/*
# Tue, 12 Oct 2021 04:07:22 GMT
WORKDIR /
# Tue, 12 Oct 2021 04:07:23 GMT
CMD ["perl5.30.3" "-de0"]
```

-	Layers:
	-	`sha256:3d6d74524a87c2d3253aea0ad3bc52520036015f508d5b113e4c8c6627e92bc2`  
		Last Modified: Tue, 12 Oct 2021 01:52:28 GMT  
		Size: 23.2 MB (23156692 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:927e017581311785f980e91bd5f056aac19aa71976214e9858dbdc26acd3319a`  
		Last Modified: Tue, 12 Oct 2021 04:34:39 GMT  
		Size: 619.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:7076469ff48061d8a9117ddab9729ec2e95825ff51b6ae4f3612b6efeab59e9d`  
		Last Modified: Tue, 12 Oct 2021 04:34:43 GMT  
		Size: 13.8 MB (13825854 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
