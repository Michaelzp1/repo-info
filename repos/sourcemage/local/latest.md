# `sourcemage:latest`

## Docker Metadata

- Image ID: `sha256:b63a9ff41eb2e2fd772b91722ca329842c384bf1a8d252c927746a151ce73ab0`
- Created: `2021-05-22T09:21:47.53406815Z`
- Virtual Size: ~ 722.47 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["/bin/bash"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
