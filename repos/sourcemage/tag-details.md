<!-- THIS FILE IS GENERATED VIA './update-remote.sh' -->

# Tags of `sourcemage`

-	[`sourcemage:0.62`](#sourcemage062)
-	[`sourcemage:latest`](#sourcemagelatest)

## `sourcemage:0.62`

```console
$ docker pull sourcemage@sha256:f39c8eeadfc81182aaeeb142b196641f7e3dcca5ef278070e0753bd6b784d0c8
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `sourcemage:0.62` - linux; amd64

```console
$ docker pull sourcemage@sha256:07ffe294f2dab74a3a6828626f2aa3b4387a4b0c9deabfb46d1600c13fe54a7e
```

-	Docker Version: 19.03.12
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **263.2 MB (263237242 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b63a9ff41eb2e2fd772b91722ca329842c384bf1a8d252c927746a151ce73ab0`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Sat, 22 May 2021 09:21:20 GMT
MAINTAINER Vlad Glagolev <stealth@sourcemage.org>
# Sat, 22 May 2021 09:21:44 GMT
ADD file:363a47d47d444d4a17aaf596e459c5459589f365987a7698c6d4d6062ecee13c in / 
# Sat, 22 May 2021 09:21:47 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:e233679f61ac795c13ff91167b8e62de1760acd3d6c9e64223d58d79216d7e6d`  
		Last Modified: Fri, 11 Sep 2020 18:23:32 GMT  
		Size: 263.2 MB (263237242 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `sourcemage:latest`

```console
$ docker pull sourcemage@sha256:f39c8eeadfc81182aaeeb142b196641f7e3dcca5ef278070e0753bd6b784d0c8
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `sourcemage:latest` - linux; amd64

```console
$ docker pull sourcemage@sha256:07ffe294f2dab74a3a6828626f2aa3b4387a4b0c9deabfb46d1600c13fe54a7e
```

-	Docker Version: 19.03.12
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **263.2 MB (263237242 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b63a9ff41eb2e2fd772b91722ca329842c384bf1a8d252c927746a151ce73ab0`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Sat, 22 May 2021 09:21:20 GMT
MAINTAINER Vlad Glagolev <stealth@sourcemage.org>
# Sat, 22 May 2021 09:21:44 GMT
ADD file:363a47d47d444d4a17aaf596e459c5459589f365987a7698c6d4d6062ecee13c in / 
# Sat, 22 May 2021 09:21:47 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:e233679f61ac795c13ff91167b8e62de1760acd3d6c9e64223d58d79216d7e6d`  
		Last Modified: Fri, 11 Sep 2020 18:23:32 GMT  
		Size: 263.2 MB (263237242 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
