## `debian:stretch-slim`

```console
$ docker pull debian@sha256:5bb84147ba0e80fe348af4bc2368148de74721444a2109a8625c98ea04f709f2
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386

### `debian:stretch-slim` - linux; amd64

```console
$ docker pull debian@sha256:51b5ea69ad41c56e2a79c874a7be9cfec4dfd3a3da3d30036625778f3074f8e8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **22.5 MB (22527572 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:22816b63493638811cfd75f71aa60e8801bbf8461e9c8c1b4974cb0487c9eaec`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:22:52 GMT
ADD file:1220579e31585dec45ca8e35874eb689018ed026a1f23b7906f791c0279671e0 in / 
# Tue, 12 Oct 2021 01:22:53 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:eec53b8a5053c739b5b685cb372b38eea3286ab6626532bad963291f76357c5f`  
		Last Modified: Tue, 12 Oct 2021 01:29:50 GMT  
		Size: 22.5 MB (22527572 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:a9ecf4dbe81b23312dfcf0e748b5cd18f248d83ea8ce39930ef8c63182f03a70
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **21.2 MB (21204312 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ee231f2410bd399183d952d81cf4a7e4244adb8dbdfecc61796f1ca35e186e4b`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:56:09 GMT
ADD file:7d9fa668f5eefa7aa63b53766fa8274d9a23658bf6e4e07fbbbad36c489ad80b in / 
# Tue, 12 Oct 2021 00:56:10 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:79e3fe0e4dd5ba1fd349b975b6ae002bcf61c48327bc735bbaeb2964c857701b`  
		Last Modified: Tue, 12 Oct 2021 01:14:12 GMT  
		Size: 21.2 MB (21204312 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:1963485172787c41bd94382890c4bfbd2d5f154ec526fe895edf392e26f7c2a8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **19.3 MB (19316474 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d3cc824b240e1bf2567d85b0d69a734e5e7242c8c1e35ae7b16b911c53cdd3d2`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:34:42 GMT
ADD file:9bfcfd0aaac802b902b0e842e040a6599c461c90b73579bcacc2fbdda7ec39cb in / 
# Tue, 12 Oct 2021 01:34:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:a1a3620b17011bd36d6f64dfcc8fd7c4cb3da78d19a59efb1b35afcadaf3f6a8`  
		Last Modified: Tue, 12 Oct 2021 01:51:59 GMT  
		Size: 19.3 MB (19316474 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:fce43bd1a1fb7829efe2424037e93bf46a4b8551d0101998f1461d6a44065ec4
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **20.4 MB (20389450 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:94481b52b5464aa62213c5766e1c738caa9d5fedd95c9da1047847cb9284820b`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:29 GMT
ADD file:ebfc214a3da7b706f0759fd22fa991c905976d2c970b2d59d134753f7cbd5e06 in / 
# Tue, 12 Oct 2021 01:43:29 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:0b0a22641ad2aad782c696887c59bf05ec0b1e7aa1e07902ee51949bab802657`  
		Last Modified: Tue, 12 Oct 2021 01:52:33 GMT  
		Size: 20.4 MB (20389450 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch-slim` - linux; 386

```console
$ docker pull debian@sha256:3e6f082b2511516bb3f7f216d9611b837e1ce9891ef145c85cd5570b60170ed6
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **23.2 MB (23156692 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5b1775b4f91bd2929fc4e6ab2bde382fa03e1449eb5e29e0e3a032e677a7a882`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:34 GMT
ADD file:4420d4cd2022d6bad7f61fbfaf16f0759ca6c30424974c6076dc7d5c09910d66 in / 
# Tue, 12 Oct 2021 01:42:34 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:3d6d74524a87c2d3253aea0ad3bc52520036015f508d5b113e4c8c6627e92bc2`  
		Last Modified: Tue, 12 Oct 2021 01:52:28 GMT  
		Size: 23.2 MB (23156692 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
