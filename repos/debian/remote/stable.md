## `debian:stable`

```console
$ docker pull debian@sha256:9de81807d160cd87c476ffe90215d85478d1d2e2c1d4973b54d993a34d3986de
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:stable` - linux; amd64

```console
$ docker pull debian@sha256:6a8bad8d20e1ca5ecbb7a314e51df6fca73fcce19af2778550671bdd1cbe7b43
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.9 MB (54917542 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5ef9fdf9b7e2c2341463a3c80366fe01c7c9e4c5942ecf612aeb0b5cb318c03a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:22:19 GMT
ADD file:5b20fa1d06a2543173c0349037f3cc0daf6865433bd0078294f56a1ad74eab99 in / 
# Tue, 12 Oct 2021 01:22:19 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:55cb80d281932941c15ecf1f22a0a0d3aeaf0cd54b119a2c866dc63f1a19d34b`  
		Last Modified: Tue, 12 Oct 2021 01:28:51 GMT  
		Size: 54.9 MB (54917542 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable` - linux; arm variant v5

```console
$ docker pull debian@sha256:f1a787f061d49cec1b933b3d657b38e4b79c311b8cfcf59073409af07fc99193
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **52.5 MB (52452247 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:54806557b8d01619c0f13258f4afebce0b74058d8ce5e1de4b46c4f194e52c0f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:54:40 GMT
ADD file:cf95c47a44c6429c9b78a21a77a25de617d7044ceac8f34076e0e4fda02a1d9a in / 
# Tue, 12 Oct 2021 00:54:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:cafc721d7e171ea8a3201aee9d49e7706edf5e50c628667efd8de159f25a2be5`  
		Last Modified: Tue, 12 Oct 2021 01:12:13 GMT  
		Size: 52.5 MB (52452247 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable` - linux; arm variant v7

```console
$ docker pull debian@sha256:485b88fde08409af5f105cf0bed2fb02ddd450d609412e1e4541b19cce050835
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.1 MB (50118660 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:c078eb0e6e57818dc23241a8945379944507c8f2411f3b3242b06cbc285cb85d`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:33:06 GMT
ADD file:49cf5edfc79e4e3ee46774cffd5252769c7920d2dc27fa3913bad6687620d64c in / 
# Tue, 12 Oct 2021 01:33:07 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:440069d8324183357e1261418d3a9a960b29f433ad2a633e70d73a72fef163a8`  
		Last Modified: Tue, 12 Oct 2021 01:50:11 GMT  
		Size: 50.1 MB (50118660 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:58e0daf62681d24cfcd64f5ab8f1a91e84b93332117393d510042e8634d31b69
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.6 MB (53602996 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:979017a0e0c87fa419e46d2a806d77d12367061a2803f7d16d19bf4e39e5aa52`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:54 GMT
ADD file:804ef9b2fc9c5b697d59a7a917482886a55807bbd36490892c70b3b629517d5b in / 
# Tue, 12 Oct 2021 01:42:54 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:ea317fc754dd760ba835f4b130dfe44ad680eb7b1fa5e5a46c133b48946f0dff`  
		Last Modified: Tue, 12 Oct 2021 01:51:25 GMT  
		Size: 53.6 MB (53602996 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable` - linux; 386

```console
$ docker pull debian@sha256:120621ece1c05c79de79dc03258b4258ec9eaab619cc7356e410eaca0cce7e9c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.9 MB (55923414 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:fba7987c39e47b72e5d8783fdfc47609726ae89bfa2ae54ef6b231d014d9651f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:51 GMT
ADD file:24d67f7b5641ca3ed5b97d965f8ce4f5f4c0387342825f51e77004fbd64dc739 in / 
# Tue, 12 Oct 2021 01:41:51 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6dc2525540e90f643ce691e74944f7fb377c97243f1d150666146756bba0ec27`  
		Last Modified: Tue, 12 Oct 2021 01:51:13 GMT  
		Size: 55.9 MB (55923414 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable` - linux; mips64le

```console
$ docker pull debian@sha256:d6f9c88fd7a0550bcc35cff245eef9cb6fed51ad3c79c949b3f800f5d6191459
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53169812 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a6b56c395f2d39368a76a931611989c4ff9d9212783a24d9f9944fe2697105f5`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:14:15 GMT
ADD file:0e49a2741eab1dd61adf85a42a5f7b1e7b8a801d17e5925ce71a962dd9436843 in / 
# Tue, 12 Oct 2021 01:14:16 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c643a33bbcb2443773c1a3a92228130d1cc6d19d74fe3ed68787c4024751ff66`  
		Last Modified: Tue, 12 Oct 2021 01:24:52 GMT  
		Size: 53.2 MB (53169812 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable` - linux; ppc64le

```console
$ docker pull debian@sha256:cbfd6decaf005166ebfbaba55c25c28a19b52b1f3fe019bd02c15877cbd1d93e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **58.8 MB (58808876 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:48fbf095386a152cddeea622bd2ec7bc98d434cac3b83ebba5552848d26f3d22`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:28:41 GMT
ADD file:50a64f2cbd267a73c297febc1aac2d0a0ff0966d105e37c199477a326aa29e76 in / 
# Tue, 12 Oct 2021 01:28:47 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:f2a063c91861196790fc45eaacac95fcbb278654c161c3397bc453aafb71bc96`  
		Last Modified: Tue, 12 Oct 2021 01:42:23 GMT  
		Size: 58.8 MB (58808876 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable` - linux; s390x

```console
$ docker pull debian@sha256:9920697169fc2f3464f2104978957748669190704e445613fea6707fd57075a5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53192883 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:dd8dad94e40fd66520452a5f35effb6d8aceb1b911e1ebf50cc103d5d8638d61`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:43:57 GMT
ADD file:edc453c86c89f128b79c20c971b4b63e822f652a01b62cba93042f5ab3a41c22 in / 
# Tue, 12 Oct 2021 00:44:00 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2f41532c0354f964b59239357dc1211cd22974215e0e805b74bb664ecd5eb1de`  
		Last Modified: Tue, 12 Oct 2021 00:49:54 GMT  
		Size: 53.2 MB (53192883 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
