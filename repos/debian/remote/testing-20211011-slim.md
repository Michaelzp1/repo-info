## `debian:testing-20211011-slim`

```console
$ docker pull debian@sha256:c2d4cc4508746a3dcdb3238330d885b97a906c390352fc73144415663faaa967
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:testing-20211011-slim` - linux; amd64

```console
$ docker pull debian@sha256:381a7544474095e0bd9737d30b4327d285133cfe3b62e72a7d9b02f42001a29e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **31.7 MB (31692026 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e254ab466936be7fd30aad2dc653e1ffca975964521d4b6bb08ec0c4591f15dd`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:23:15 GMT
ADD file:f6b80ed3e804ddf5f782ae51a8bc04fa09bf52f65efb39cadb0857b781d08cca in / 
# Tue, 12 Oct 2021 01:23:16 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:e41a2e3d0b0a7e79e8b869b888c3cd874a287dbcb0195b0f58dbf77d330eed8f`  
		Last Modified: Tue, 12 Oct 2021 01:30:31 GMT  
		Size: 31.7 MB (31692026 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-20211011-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:9b6c35071bd5949a788d14f40b144f5bdbe2d4ac3c288dc91813ace08959f46f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.2 MB (29211602 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e4e729458c110bd7bc8247cdb7896c14a7f1858b04950bbb091ca76b122df943`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:57:12 GMT
ADD file:73fe64bf1b943046619e7c49d38fae01f1227cd1c4ecf479af518541a8fc4660 in / 
# Tue, 12 Oct 2021 00:57:13 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c2ad6bea8aeedf02a9bedf4bf8669b4b006f5ba7b01ec4080c655f359c7ea3a0`  
		Last Modified: Tue, 12 Oct 2021 01:15:41 GMT  
		Size: 29.2 MB (29211602 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-20211011-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:acea487aa87b458ee9a92873202de8843a9ea81bda72f3487b7a0ae1daf5863c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **26.8 MB (26813077 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:12950c4415d27eade99c05fd2e1c2d7b4c749bc5ff482230ff6e6d5f7c1bfd91`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:35:46 GMT
ADD file:1180a916a9a7bdc2cb4681f64bedd4b12596b91d59902e7e7a14436005b2912a in / 
# Tue, 12 Oct 2021 01:35:47 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:54ae0de37f782991c9999cb61b8b8aee1d3c5ad1abb13ae2ddc42269c5bb83de`  
		Last Modified: Tue, 12 Oct 2021 01:53:20 GMT  
		Size: 26.8 MB (26813077 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-20211011-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:505bc16b681a41a36df96c64aaa8193a0ce5ecbd6e383326adb3bd4d4b8d64c4
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.7 MB (30711434 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5fe2789664c10528575d52af305a871bd4b0eb6952651d7f7b96f0f41fd0305f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:52 GMT
ADD file:8259d7f861d8a2631d9f4acd401206fede662319db3e6898bd0627b5c8a80c62 in / 
# Tue, 12 Oct 2021 01:43:53 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2a967de72364dc4d247bcea2ffa3a7e32a4cb6747a8b71527b74ac7372e526fc`  
		Last Modified: Tue, 12 Oct 2021 01:53:19 GMT  
		Size: 30.7 MB (30711434 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-20211011-slim` - linux; 386

```console
$ docker pull debian@sha256:e27aba3e78139fda2484dabb945b26cad4762fc1ef92c33803ebaf1ff880428a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **32.7 MB (32726667 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:45ab4b5740a25d600f1d34bf99ecb10abc25be93708311fb451d738f1f448e6d`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:04 GMT
ADD file:d7185a52026638c9fa18bed919a0632cd93fa9ea97fd039d4b38a9dbeefd568a in / 
# Tue, 12 Oct 2021 01:43:04 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:ecce2aec504529229dcba1809cc5b268de6b494a1f67b17a591dac6acca40782`  
		Last Modified: Tue, 12 Oct 2021 01:53:22 GMT  
		Size: 32.7 MB (32726667 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-20211011-slim` - linux; mips64le

```console
$ docker pull debian@sha256:fae74ed361dd00626cf69cf15ad23c1586873d4038ddc173fd5c32603bb60902
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.3 MB (30313260 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:de3bc24fbd39908f83d30cbcb08bc963585fb47be21144ab86f17c24f3b5ce80`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:15:37 GMT
ADD file:b4260b6560d51c7575580c9e51965b6509ff23a7deeae32c9909adc859d39714 in / 
# Tue, 12 Oct 2021 01:15:38 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:d2bdfda2b58edd29fba5352889110e56882c383fa034dd5ce9bf197a0ca6f05a`  
		Last Modified: Tue, 12 Oct 2021 01:26:55 GMT  
		Size: 30.3 MB (30313260 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-20211011-slim` - linux; ppc64le

```console
$ docker pull debian@sha256:46c2b4ba2d4315cd5f632bd4725a580f0086932a48f9c0a97f54657667b4f7e1
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **35.9 MB (35900669 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3ba35589f777934d19d5bc639cea24ccac552b26beff6ad1a73a9bda6fc1adbf`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:30:38 GMT
ADD file:e2cddfa5844a7b6a1df121b58d7df96d27401bb7eaa57311dbf8f2d1fbefbdbc in / 
# Tue, 12 Oct 2021 01:30:41 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2463691f6014433729769cd612d2e4f913196d353b3e2b03669140872fa429a1`  
		Last Modified: Tue, 12 Oct 2021 01:43:51 GMT  
		Size: 35.9 MB (35900669 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-20211011-slim` - linux; s390x

```console
$ docker pull debian@sha256:2ff0a952d712916606876bef59e82c7a69481bf269c40e8b66974469cce0a9ca
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.9 MB (29947092 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:47200e6b0dfa4498f50d81360bcb8a590ed8a22b35dde1909161f98942e8093f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:44:41 GMT
ADD file:8a1f9a5b713b56f35ccf4b9cbec121b39d4657f61daff35687bcc7d5e057a772 in / 
# Tue, 12 Oct 2021 00:44:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:82c50934191a866dd0513b11f069a868b988df942169c6f1b2aa5fe440f96a9c`  
		Last Modified: Tue, 12 Oct 2021 00:50:41 GMT  
		Size: 29.9 MB (29947092 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
