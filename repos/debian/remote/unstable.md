## `debian:unstable`

```console
$ docker pull debian@sha256:911e49bbc76754c139b48cd2111d5ddec1338ae495cd624b7eb871fee24ee4fa
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 9
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; riscv64
	-	linux; s390x

### `debian:unstable` - linux; amd64

```console
$ docker pull debian@sha256:8433e61815f23a9fd11923709b74eac55e0b31952a3f372248f4b6d9da536716
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.7 MB (55687490 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:95a5ee88d43cc7bd4dbf637921c5e0527193b7d9bde8732986f04a1c3e68de85`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:23:26 GMT
ADD file:768176ccdb1053418db077c8d4e7f86c7d238d14b4d6298282277ed9167f5103 in / 
# Tue, 12 Oct 2021 01:23:27 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:68135aa259351f7843ab8a4668eac588b9c494bae3a339713817d0f4bd9e6572`  
		Last Modified: Tue, 12 Oct 2021 01:30:47 GMT  
		Size: 55.7 MB (55687490 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable` - linux; arm variant v5

```console
$ docker pull debian@sha256:7a63ba04b0026bfffb2842f8735914848c27ebded3c4ae18f2eaa0471c5d7b4b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53186307 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:63cf150c14c6cab9c7354db59d4f469bf883250c6336d79d218f36ee7c288810`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:57:38 GMT
ADD file:142ff5a3e7c083dbe37f25d418562ca69fb261ae8301ac6704d16389cac353ff in / 
# Tue, 12 Oct 2021 00:57:40 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6d15c84744a3fd9ea8041501d65ab1bd82ba58fb2d2a5efae10506df977440fb`  
		Last Modified: Tue, 12 Oct 2021 01:16:31 GMT  
		Size: 53.2 MB (53186307 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable` - linux; arm variant v7

```console
$ docker pull debian@sha256:58ba27af65ec4ebe418167c7e86e92f02c4cda9f00d1815c22f0638159aeb1ec
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.8 MB (50797060 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8a48f9648a6cb865b41d35ad12cf9482f2b3e9b32b99ccb1779a22bb8e69268e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:36:14 GMT
ADD file:be4b9a2b55c955b91060dc950fbb28c53858174da0e76833e50d3a818c01eec0 in / 
# Tue, 12 Oct 2021 01:36:15 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c5c2a3ca9a7b031872447f9db1cf1ff22cd0711b3d2db5d88595c25ed411d976`  
		Last Modified: Tue, 12 Oct 2021 01:53:58 GMT  
		Size: 50.8 MB (50797060 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:27d1fbaa3c41efb372bf56aeee62773e769a2c4053762d61637e14f8374b3b7d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.7 MB (54702889 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8eea3e5564dee6f4daa835f9600b64724d2fb6ff12ecb0df387ecbd9fb3d8274`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:44:02 GMT
ADD file:894de0be09c3231a5faac7ccddbc80c3b60408789649645b6ee1f495fa9b9658 in / 
# Tue, 12 Oct 2021 01:44:02 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:f7d44b65f406086d3f6c33e702e62ccb453a15e440f7a20e605c14d292b461e3`  
		Last Modified: Tue, 12 Oct 2021 01:53:38 GMT  
		Size: 54.7 MB (54702889 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable` - linux; 386

```console
$ docker pull debian@sha256:bc343c4a530fca5bc02a01afe34a457887c2181c07300a916b0dc59cf07d20bc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **56.7 MB (56716144 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ff2f64fca99c88e738b282653da3c64106af7073e3afdd8a1bf106acdda3d727`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:17 GMT
ADD file:3509f6fbe74f127b3a47c4cb6323c6e8e8fb46e38fe742921d35fcd0ce76d276 in / 
# Tue, 12 Oct 2021 01:43:18 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:1665cd0d0f387adf8fec7c5686c084e64edc02778c6f6244db411ddc55a1f4c1`  
		Last Modified: Tue, 12 Oct 2021 01:53:43 GMT  
		Size: 56.7 MB (56716144 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable` - linux; mips64le

```console
$ docker pull debian@sha256:c33597ce0a00d9ece2cf7ac9de46fa223b4984d09c49078286d76a8023f3e346
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.3 MB (54313458 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:988c9c93af6d5bf80a0c09f9febc14b93cb1bc9a3d6702d27bef179229f3e621`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:16:04 GMT
ADD file:2cc438bb6fb8a3a9b0818598818b7854a43d5393ac7e53bdf871b61d3357f09e in / 
# Tue, 12 Oct 2021 01:16:05 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:4f27b1c33880effa793ac87419f57ce87673a42cd455a5eccb5b14568e55ce22`  
		Last Modified: Tue, 12 Oct 2021 01:27:38 GMT  
		Size: 54.3 MB (54313458 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable` - linux; ppc64le

```console
$ docker pull debian@sha256:9223d6eca312fe2cf854702184cd57de1528bd7f09ffc49f6b039da5f1d3ab43
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **59.9 MB (59889202 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ba9c394f27ca08b92becd800aebba35ae8befe30da03cf7fcf6e71073dd5ee85`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:30:57 GMT
ADD file:52b1e51a9aeb65e63eb0c1a42cef5a4bc15ead2428cb8ece49b10c4fe8464216 in / 
# Tue, 12 Oct 2021 01:31:02 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:5339ee5d539a0754933340b57bd16312bc7f834914e59717e20c1b83eaa8072a`  
		Last Modified: Tue, 12 Oct 2021 01:44:12 GMT  
		Size: 59.9 MB (59889202 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable` - linux; riscv64

```console
$ docker pull debian@sha256:22fd02a51c09a8cd28b60aa0002da6684ebe636f3a6af9254b504b5f4ee60bfb
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **51.5 MB (51516616 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:7001c623dd7e7170ed515c5eb57e3050e9f8ce74245306e4a5ee85b1254ff3f0`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:17:56 GMT
ADD file:fa84e1dde40e56bc549344dc0d3f2bf2400e5009c42f96c262489c33aaa81455 in / 
# Tue, 12 Oct 2021 01:17:59 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:e70c24b27e8c6ebee7852a52b1fdd44e247e6ae2e2ce6e18bed96cc9408cff2d`  
		Last Modified: Tue, 12 Oct 2021 01:33:55 GMT  
		Size: 51.5 MB (51516616 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable` - linux; s390x

```console
$ docker pull debian@sha256:b462d85e8d3b3fa5a0a5cf7bd7c19c203f96b30ed03d07ce77902b248a59c8dd
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.9 MB (53940695 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:81653f9e5bcc1363f51436ac7f2f766cce507ad1ae71f2d09b6d162ebeb57e7c`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:44:52 GMT
ADD file:8e72740308f6ae0d026697be186652e326c16f3f37e6ab6cc86de966dfa8cab4 in / 
# Tue, 12 Oct 2021 00:44:55 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:7d3fc62e9c56880d86636919f36507d68aacc14f85a6c05acb3ecfcfedcafb8a`  
		Last Modified: Tue, 12 Oct 2021 00:50:54 GMT  
		Size: 53.9 MB (53940695 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
