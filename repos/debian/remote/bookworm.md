## `debian:bookworm`

```console
$ docker pull debian@sha256:27b2d97cf281cc6cd20ba5c99961315954cb5e83e03707686584bfeecbff71ad
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:bookworm` - linux; amd64

```console
$ docker pull debian@sha256:db7c986f30367ddc5c3a090c2617ef4da058512141720f71d0dd9866c18dfadc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.4 MB (55446041 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:674f99e735974b7d2ab9ed4a9c5efde15906f413e885a24d5b80dc8f6902787a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:20:05 GMT
ADD file:27dfce899c847022174d5caa95af127a948c63fcd5a23b6cbecc8167f4b2de21 in / 
# Tue, 12 Oct 2021 01:20:06 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:618a27eae568875e1902bc8979ae01edf1e0116fea44ca04d1d7c67d590cd708`  
		Last Modified: Tue, 12 Oct 2021 01:24:59 GMT  
		Size: 55.4 MB (55446041 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm` - linux; arm variant v5

```console
$ docker pull debian@sha256:1e8a7aa2d3bda4300789ee97c93fa766f71efa9ae3ab025260ec659221eac814
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.0 MB (52964940 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ca5160232bb477662a4acf9baf77a51af25617bda8f99566e476f45cea976b5b`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:48:55 GMT
ADD file:ab43782dfcd6f5ad4a656cf43f0385f45ef4a3c26e23153f4898190cf9b704c5 in / 
# Tue, 12 Oct 2021 00:48:56 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:13544a9b0148e44e1e5fa2a9d5e9cd0ecec08898da21d7ebf79b6a6b34d6b67a`  
		Last Modified: Tue, 12 Oct 2021 01:03:53 GMT  
		Size: 53.0 MB (52964940 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm` - linux; arm variant v7

```console
$ docker pull debian@sha256:1612d79bcee557bf56c6cf3be17e6ae1891f9dbaf5637671708c5aa469ff9c20
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.6 MB (50566236 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:c1f65878563c46c4f65696a9602c64d6e707af379d841d8f047ac44ea73c6f10`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:26:56 GMT
ADD file:3b73f2accf5cf51a65b062fd02e1840ba8413191e44707e035ba6819a8eba1be in / 
# Tue, 12 Oct 2021 01:26:57 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:96084cd63f96d5395a465b5034a4392747af562af1269011d400540dc5b85a5d`  
		Last Modified: Tue, 12 Oct 2021 01:42:27 GMT  
		Size: 50.6 MB (50566236 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:9769434fb677d3dbe5f04b473d671b32e724553a7e93fb2eab52e0819baf14a9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.5 MB (54465168 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5897f90de5dff9070f18c81e5e9b6a9a36ae055872803771df6a173bae036f66`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:41 GMT
ADD file:4f13e43ce08281cef17adbdc736c615c592b9a55db9dcea6106e9355805d8eb3 in / 
# Tue, 12 Oct 2021 01:40:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2cbe7f3c2d37f6e753ada76cd01b0ecb8c8dc3a63791174a7bc8f7b2c95aa4eb`  
		Last Modified: Tue, 12 Oct 2021 01:46:59 GMT  
		Size: 54.5 MB (54465168 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm` - linux; 386

```console
$ docker pull debian@sha256:420dac847dd9fc23619b4490b5b3826408001d7a45585ee4612c1018955139fe
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **56.5 MB (56480795 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e39f774888b00cd708625cf03feebeb72322a7c404b565cc68474394ae7ec705`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:39:00 GMT
ADD file:5c67fb048ea543a4bda0c18d5b1f13f9ed438f0c2da79f7710603bb8072f06e5 in / 
# Tue, 12 Oct 2021 01:39:01 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:5b6ea382495037522d99f596d06e6a07243f5c9a81406a71b1867d7b81c2d245`  
		Last Modified: Tue, 12 Oct 2021 01:46:18 GMT  
		Size: 56.5 MB (56480795 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm` - linux; mips64le

```console
$ docker pull debian@sha256:4a0bb29a8e82741de4a47712c9975fbd4364fb1c6eac1fdf333ceccda4f8aaec
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.1 MB (54069055 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4dc26e5f1694179e8b84307074a831a2cb51187f382ad10b88386ca0ab337c62`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:09:56 GMT
ADD file:8f2e72c6d8bc47bf31947b2f916b90c94c62fd9a3fd11deef021971c079ff665 in / 
# Tue, 12 Oct 2021 01:09:57 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:fad81077a6466f0231de8750a5f41773b60638f37eeb5ec052d93dd340d095de`  
		Last Modified: Tue, 12 Oct 2021 01:17:59 GMT  
		Size: 54.1 MB (54069055 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm` - linux; ppc64le

```console
$ docker pull debian@sha256:e1611ee0974084e3addfe03b5a2905cb3964d65156b176be4149be0227a6761e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **59.7 MB (59659978 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b770b8a29c1a84b3e0d7e9dba1c48a06b7f0ae524df43fec3f4844d2ca9a5aaf`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:24:06 GMT
ADD file:2208d7bf60cb752ee46dc1a2c107f0ffefb15b83324ef516b1b93f03c5d0c249 in / 
# Tue, 12 Oct 2021 01:24:11 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:3cb87628128232319961676d93044c80b66d2fba881080d5a34c710d8a09207d`  
		Last Modified: Tue, 12 Oct 2021 01:34:29 GMT  
		Size: 59.7 MB (59659978 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm` - linux; s390x

```console
$ docker pull debian@sha256:862c7efde050551594c757f68b44e46100f62d49b3fbb679d72083f7921794d4
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.7 MB (53700141 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:fff27f662c407237cf9388d0ea2a608eb9558b6fbae278f0d41f4761d9f4f959`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:41:38 GMT
ADD file:c1434e9b848c2f01f3fbd798a3e6ea6f1806b144dbc3f1b0d5bb8e1f51339b7d in / 
# Tue, 12 Oct 2021 00:41:44 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:68fb91bf36b385ffeb27bd029bade1171ff1a60e865f85a09ee539cbdb35b1e7`  
		Last Modified: Tue, 12 Oct 2021 00:47:15 GMT  
		Size: 53.7 MB (53700141 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
