## `debian:oldoldstable`

```console
$ docker pull debian@sha256:c07e3fd22cde46953b2a1070bf64f7284f93d5f318bb37209d4560e92453ae30
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386

### `debian:oldoldstable` - linux; amd64

```console
$ docker pull debian@sha256:f00f6414af2edd4bad1b71932a57246d4e6757899d8cb44dd4269918fa6929e3
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **45.4 MB (45379650 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:389bf269f1c37e68845217ff046c6242037fa0ad2128ffe343c765db350c3608`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:21:15 GMT
ADD file:2dad8aa47b7c0abdcaccbf1c501793fd733f172a30177cc5a5906a7e8c94163d in / 
# Tue, 12 Oct 2021 01:21:15 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:a8b1a171ce712e7503b1f72abb7b9751bff88e01e4413539244c27b04b96af58`  
		Last Modified: Tue, 12 Oct 2021 01:27:11 GMT  
		Size: 45.4 MB (45379650 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable` - linux; arm variant v5

```console
$ docker pull debian@sha256:1aa499985f779e93df7634f379878fe5576ad3178529a00cd6ae38b4b4877b0a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **44.1 MB (44091934 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:061ddcb939a4b79e2dd4143ee731737459f9a4536fdd99bac41a0ba30a9b849d`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:51:59 GMT
ADD file:562359b7dd2e2aded9f637c664877f9d2a7e1a5ca24b5b0f87032c0c6f63ff05 in / 
# Tue, 12 Oct 2021 00:52:00 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:65b249c69eb3aa72e4d5fc5c9152f2138252bc14d2e8245381d9860d13f3e4fe`  
		Last Modified: Tue, 12 Oct 2021 01:08:32 GMT  
		Size: 44.1 MB (44091934 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable` - linux; arm variant v7

```console
$ docker pull debian@sha256:b7412ba79e69ab9e0ea3d0e1db866a274626cc4572d71f5d42758b5dc5004824
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **42.1 MB (42119365 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1f419b7c1027b9396d13d7390a62b30cbb6a60e33ce0f4427f9378648651e686`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:30:12 GMT
ADD file:5ea5714223f1698d41da52f17d7b6936e82f8e1f41f19e00b5e300b8f329d58b in / 
# Tue, 12 Oct 2021 01:30:13 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:ee5cec6429f90405fa16db25cfe4f7bf7d03099e0372790dc8f0a457d9d0a244`  
		Last Modified: Tue, 12 Oct 2021 01:46:47 GMT  
		Size: 42.1 MB (42119365 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:650a826b7c505f481cf2a727a2df43abaadf0888c7a8d8ea923b968147b29978
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **43.2 MB (43176728 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4ccb9b25c45fd91b23aa7b3f6af466bc9653807a9fb5d5aa7dca358630049503`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:52 GMT
ADD file:82529472573ae109efa01a1b422a0c016903bc71b937668f7c4ee238c356afe3 in / 
# Tue, 12 Oct 2021 01:41:53 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:1682a7bcf61dffda8aa42e61da61e3c02fbfdd938351384f3ff623d8d6465b64`  
		Last Modified: Tue, 12 Oct 2021 01:49:30 GMT  
		Size: 43.2 MB (43176728 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable` - linux; 386

```console
$ docker pull debian@sha256:87fe9e37f9f64ba508836c9a147faed99a465780fa80a20e9f3244120c83c3c3
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **46.1 MB (46097211 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e6f0b084c62b69ca5987572d23cdb6f4353fef1f59fce9405c1520a8e3737b75`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:30 GMT
ADD file:4fcadf4f144c0e7b2f9e6cd377b183799f017ea7ec585a314d6deaec3ca13a6c in / 
# Tue, 12 Oct 2021 01:40:31 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:5fcdd168800cca93cd361730f88c052b1ec6ddfced4c88f830d939fb2d6c8f3b`  
		Last Modified: Tue, 12 Oct 2021 01:49:04 GMT  
		Size: 46.1 MB (46097211 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
