## `debian:experimental-20211011`

```console
$ docker pull debian@sha256:4f1a654e8faa6d79b2bddc387086689eb723e54790043d33f368529a42db8bd3
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 9
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; riscv64
	-	linux; s390x

### `debian:experimental-20211011` - linux; amd64

```console
$ docker pull debian@sha256:45b05b03d8dcb55a91f192f2b1ba939f557cba4855126df4c0a1913ed09f300c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.7 MB (55687710 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:41362f447867d0fc309d288a43d40dfdec369d00a051a47b87eb73da692ad37c`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:23:26 GMT
ADD file:768176ccdb1053418db077c8d4e7f86c7d238d14b4d6298282277ed9167f5103 in / 
# Tue, 12 Oct 2021 01:23:27 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:23:40 GMT
RUN echo 'deb http://deb.debian.org/debian experimental main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:68135aa259351f7843ab8a4668eac588b9c494bae3a339713817d0f4bd9e6572`  
		Last Modified: Tue, 12 Oct 2021 01:30:47 GMT  
		Size: 55.7 MB (55687490 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:6f78521d0a2b6454a78768f886ba8690e18795ceed1ce1d4552e364ef901c431`  
		Last Modified: Tue, 12 Oct 2021 01:31:11 GMT  
		Size: 220.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:experimental-20211011` - linux; arm variant v5

```console
$ docker pull debian@sha256:890e93ced7aa94f08f25318df45cb109ba1c56316f25f98cf303bf5956f350b7
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53186529 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:644ccdd631618cc306856129692f304c1244ad881fb4ba1b65904920cd8d9e9f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:57:38 GMT
ADD file:142ff5a3e7c083dbe37f25d418562ca69fb261ae8301ac6704d16389cac353ff in / 
# Tue, 12 Oct 2021 00:57:40 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 00:58:13 GMT
RUN echo 'deb http://deb.debian.org/debian experimental main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:6d15c84744a3fd9ea8041501d65ab1bd82ba58fb2d2a5efae10506df977440fb`  
		Last Modified: Tue, 12 Oct 2021 01:16:31 GMT  
		Size: 53.2 MB (53186307 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:2ed5e1c306d5761f0b0feb2e6678975fef169295197aeb91fd2fa392cfebc95c`  
		Last Modified: Tue, 12 Oct 2021 01:17:14 GMT  
		Size: 222.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:experimental-20211011` - linux; arm variant v7

```console
$ docker pull debian@sha256:2cb69e8a78c323c61599a58d5d5873bf5b8960a64cc325073c1043f7219b2885
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.8 MB (50797283 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5ca5708262eda55b6cad949d42b44fdfa4d911ea1948a27d77aa519fa9f3caf3`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:36:14 GMT
ADD file:be4b9a2b55c955b91060dc950fbb28c53858174da0e76833e50d3a818c01eec0 in / 
# Tue, 12 Oct 2021 01:36:15 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:36:50 GMT
RUN echo 'deb http://deb.debian.org/debian experimental main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:c5c2a3ca9a7b031872447f9db1cf1ff22cd0711b3d2db5d88595c25ed411d976`  
		Last Modified: Tue, 12 Oct 2021 01:53:58 GMT  
		Size: 50.8 MB (50797060 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:ecf20fff3bbe8438b10b6cb41c8084d34ecca24bedc6d9358a4ffba7924b2613`  
		Last Modified: Tue, 12 Oct 2021 01:54:36 GMT  
		Size: 223.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:experimental-20211011` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:9e39f2c9a51c1d6df1daa9a6e3dc0db2ddb8384de41ab5e331e618cb2254df8b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.7 MB (54703113 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1bb92dd3759aefbb612ce3141728641dd5672ee2fc75ce0445b12c1e6c27bec4`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:44:02 GMT
ADD file:894de0be09c3231a5faac7ccddbc80c3b60408789649645b6ee1f495fa9b9658 in / 
# Tue, 12 Oct 2021 01:44:02 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:44:16 GMT
RUN echo 'deb http://deb.debian.org/debian experimental main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:f7d44b65f406086d3f6c33e702e62ccb453a15e440f7a20e605c14d292b461e3`  
		Last Modified: Tue, 12 Oct 2021 01:53:38 GMT  
		Size: 54.7 MB (54702889 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:475af9fc2fe2608137d618cfead6cca5a53d40ea0eb4a3ea3f98f8162dfb0413`  
		Last Modified: Tue, 12 Oct 2021 01:54:03 GMT  
		Size: 224.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:experimental-20211011` - linux; 386

```console
$ docker pull debian@sha256:f61d1e3e2a66d096e99db36fd490cae635487f38f405649e981fef8501cca7ba
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **56.7 MB (56716365 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:dc94beb02f59f62ea35428287c0a095aa8b57d62120d5b1069dbbd5ca00fe2ee`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:17 GMT
ADD file:3509f6fbe74f127b3a47c4cb6323c6e8e8fb46e38fe742921d35fcd0ce76d276 in / 
# Tue, 12 Oct 2021 01:43:18 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:43:34 GMT
RUN echo 'deb http://deb.debian.org/debian experimental main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:1665cd0d0f387adf8fec7c5686c084e64edc02778c6f6244db411ddc55a1f4c1`  
		Last Modified: Tue, 12 Oct 2021 01:53:43 GMT  
		Size: 56.7 MB (56716144 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:001705f4d6e77993e3e9b15bfd6e40efcc270260e3c8d997df6b1fc2f8d95aaf`  
		Last Modified: Tue, 12 Oct 2021 01:54:11 GMT  
		Size: 221.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:experimental-20211011` - linux; mips64le

```console
$ docker pull debian@sha256:be3e2a66e000738453f854a5606740eaa43e25f12ead9e3af8e64b910aefb7d7
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.3 MB (54313680 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:7ea96ff5f92d9473503a6c5ed74d1ab0dc1cc09e625c894a4129ed5b1b08635a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:16:04 GMT
ADD file:2cc438bb6fb8a3a9b0818598818b7854a43d5393ac7e53bdf871b61d3357f09e in / 
# Tue, 12 Oct 2021 01:16:05 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:16:33 GMT
RUN echo 'deb http://deb.debian.org/debian experimental main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:4f27b1c33880effa793ac87419f57ce87673a42cd455a5eccb5b14568e55ce22`  
		Last Modified: Tue, 12 Oct 2021 01:27:38 GMT  
		Size: 54.3 MB (54313458 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:ffa954ac487aed0297c5d2daa45223a07a7dd7a36a69f238c6a12578b258a88c`  
		Last Modified: Tue, 12 Oct 2021 01:28:17 GMT  
		Size: 222.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:experimental-20211011` - linux; ppc64le

```console
$ docker pull debian@sha256:4e1c40d06c866035bcfc8f015b61e52ac9ebcdeb3dec44d94589ae27b9062b90
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **59.9 MB (59889424 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e685f3cca876eaf0c3ba43bc676f3ef498127a64057841ad168063e06217da01`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:30:57 GMT
ADD file:52b1e51a9aeb65e63eb0c1a42cef5a4bc15ead2428cb8ece49b10c4fe8464216 in / 
# Tue, 12 Oct 2021 01:31:02 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:31:33 GMT
RUN echo 'deb http://deb.debian.org/debian experimental main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:5339ee5d539a0754933340b57bd16312bc7f834914e59717e20c1b83eaa8072a`  
		Last Modified: Tue, 12 Oct 2021 01:44:12 GMT  
		Size: 59.9 MB (59889202 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:5c43ea5c91b12462cce1ef6b54ea15df8f50f27a259dd443a0c54ab8d8de75ce`  
		Last Modified: Tue, 12 Oct 2021 01:44:42 GMT  
		Size: 222.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:experimental-20211011` - linux; riscv64

```console
$ docker pull debian@sha256:6fa46d48943121066c5fe9142a297eba9e9a37a34251618fe938af616b8b487f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **51.5 MB (51516846 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:2cba3e98c3bcb33cc6d4e4fedf1501cd52fe2e856ec94081c67350535a2f24b5`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:17:56 GMT
ADD file:fa84e1dde40e56bc549344dc0d3f2bf2400e5009c42f96c262489c33aaa81455 in / 
# Tue, 12 Oct 2021 01:17:59 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:21:06 GMT
RUN echo 'deb http://deb.debian.org/debian-ports experimental main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:e70c24b27e8c6ebee7852a52b1fdd44e247e6ae2e2ce6e18bed96cc9408cff2d`  
		Last Modified: Tue, 12 Oct 2021 01:33:55 GMT  
		Size: 51.5 MB (51516616 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:4be7420055369178beaa1d7bdb785056a2cd5b3b1b420f78b3064101c302b572`  
		Last Modified: Tue, 12 Oct 2021 01:36:32 GMT  
		Size: 230.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:experimental-20211011` - linux; s390x

```console
$ docker pull debian@sha256:06c4e0c44afe02e648cdd5531d0280df7023967e12e41ee3272563fa22278ab6
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.9 MB (53940915 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a8619faf1bee4a51e2100d5f39b95cbdaf1adcbe14fa14ca91b1b00a83665d4e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:44:52 GMT
ADD file:8e72740308f6ae0d026697be186652e326c16f3f37e6ab6cc86de966dfa8cab4 in / 
# Tue, 12 Oct 2021 00:44:55 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 00:45:11 GMT
RUN echo 'deb http://deb.debian.org/debian experimental main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:7d3fc62e9c56880d86636919f36507d68aacc14f85a6c05acb3ecfcfedcafb8a`  
		Last Modified: Tue, 12 Oct 2021 00:50:54 GMT  
		Size: 53.9 MB (53940695 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:64db1ba033328d40f3acc0855295bc14ce0380c1aa79808f1ecbb1dcf9b4bb90`  
		Last Modified: Tue, 12 Oct 2021 00:51:13 GMT  
		Size: 220.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
