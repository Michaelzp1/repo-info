## `debian:oldstable-20211011-slim`

```console
$ docker pull debian@sha256:de685da52b9eda22b01498dfa7c62f5fe65b9d3eb52a7b4c77a23e33d8f3e6d9
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:oldstable-20211011-slim` - linux; amd64

```console
$ docker pull debian@sha256:39600b2f54313fb62f6109dadc36d62f1418c51dde871f7c97e9d6be9b9d8f59
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **27.1 MB (27139517 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:dfc5f8e95722e645dba909ea600a7ed02e899e4fb1c89513514ebe3b6c05ea2a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:21:48 GMT
ADD file:0087281285401f9572dc89dc688004e8d163d8a6d0f7d06d6b9e49182048d7f8 in / 
# Tue, 12 Oct 2021 01:21:48 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:fb1a97c1b19b06e11a10ec57653655b9eba0af39ba825e6bd53860c5d2004314`  
		Last Modified: Tue, 12 Oct 2021 01:28:05 GMT  
		Size: 27.1 MB (27139517 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:eca169327cf548858a07543d8e4267a37c30110f1abfbef21fac4bab1576cfb2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **24.9 MB (24872664 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f7e3c01c91b41008236f284ce976e0003653e2710454bfdd5e3851a01c8c979e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:53:27 GMT
ADD file:185c1c313855b2b561257e51db5d2f5391b483cc432f07e7514b60cd2286b22d in / 
# Tue, 12 Oct 2021 00:53:28 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:a6064fa7bf9cd05b7bc06c090d0c55c7d418fdf9d765054d4400566d4f0b0964`  
		Last Modified: Tue, 12 Oct 2021 01:10:18 GMT  
		Size: 24.9 MB (24872664 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:3459e7a1e45d8bfdebb9db507870ec2eab28569331636788ee970bf632ba3099
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **22.7 MB (22739684 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9b99ca577fedc45ac5ec1abfac64a8e85145d21ae23905c753c5927fc33edc96`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:31:44 GMT
ADD file:6518c6d487ce10fc0225f82202f45f76b27b8c2c730f9dc5d8684a2fe1abe052 in / 
# Tue, 12 Oct 2021 01:31:45 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6264a2b2178995c852d7382a07beb4b2180a91d897ea06ff0f85e8980b848ce2`  
		Last Modified: Tue, 12 Oct 2021 01:48:26 GMT  
		Size: 22.7 MB (22739684 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:4ab893492695e7b5d720f8c4e73a7e324c7031a197a10a9e6dc99bdc38311500
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **25.9 MB (25908531 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:38f9be39c6f8b4d85099dc2c53a77a435a09e3d341efc11bed012cb2754ff92a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:26 GMT
ADD file:d84316b679e908b43e684d1016a6717082e6f204251fa4073c9a2ef33fbf0f75 in / 
# Tue, 12 Oct 2021 01:42:27 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2868439da93c0a0bde3f7b871b0d731222d132234cbdb2c306bcee9b248f0230`  
		Last Modified: Tue, 12 Oct 2021 01:50:33 GMT  
		Size: 25.9 MB (25908531 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011-slim` - linux; 386

```console
$ docker pull debian@sha256:b3b09bba602445d783770407cf2f5cad522f30664cef4ad304fc6e9ec0d6b6dc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **27.8 MB (27791432 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a7be7b77864d8dfefa7cf5b430ed6b45eabbdd4d553e7fa373ec347ad2a9f115`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:13 GMT
ADD file:2dd30cffeab47a7a23ffe4360316e7c6dea896e3b1a2b09c2920ff6c365f95d1 in / 
# Tue, 12 Oct 2021 01:41:13 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:8a077bff6f39437082dd48e8f72a9f9e283de24e59f11abbd0ae1724e025858a`  
		Last Modified: Tue, 12 Oct 2021 01:50:12 GMT  
		Size: 27.8 MB (27791432 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011-slim` - linux; mips64le

```console
$ docker pull debian@sha256:e3cc551053ea193ecbf7dd470a4912fe80a56f2880681c95e77b56dda0241009
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **25.8 MB (25806507 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3daf9661d78d9210c3599942e82d0e8157ae3cae3de8169c0f5cb60a6c6ed018`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:13:00 GMT
ADD file:802804164a1553ad9a96fb3a18bd3b946bb8f13ff708ba1ee602833d5eeee223 in / 
# Tue, 12 Oct 2021 01:13:01 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:00d53dc1ef6d0acda8a927a222fcf676c7f6e815046421c750df808bec08249f`  
		Last Modified: Tue, 12 Oct 2021 01:22:55 GMT  
		Size: 25.8 MB (25806507 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011-slim` - linux; ppc64le

```console
$ docker pull debian@sha256:4e11cefcdb3b921eb7679001f0e265d31cc2c3cf1de1b293127177331ce79c23
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.5 MB (30547194 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a318b65276d8fee17e69fb358b56710039bfb33a95a46d644c8e3c847a7694c9`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:27:38 GMT
ADD file:f3be27a2f7c4b00219c330c9134f72512cb3c0e27748e8139f36ded07e864d69 in / 
# Tue, 12 Oct 2021 01:27:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:dc9eab1fc04bbc5273373b8b688f6153bb57283aabfa43a541656a5cbc07c382`  
		Last Modified: Tue, 12 Oct 2021 01:40:10 GMT  
		Size: 30.5 MB (30547194 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011-slim` - linux; s390x

```console
$ docker pull debian@sha256:b38ac6dbb3e27812ca70b5a40a54dbad7c1fc95e97e8572315dc5bcf4e76fbd1
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **25.8 MB (25754237 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:7404699e67c5d79b5883165de65d63077045b31e72389b0a22bce55c83ecbfcc`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:43:24 GMT
ADD file:6ec6e214c53947bf14459791523b57a631aa100c905c746bdba061913e5b1347 in / 
# Tue, 12 Oct 2021 00:43:26 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:092d82f61df2a5e21e13d548ff1e09c69cb982e320d870d7b38ca7017cc46574`  
		Last Modified: Tue, 12 Oct 2021 00:49:14 GMT  
		Size: 25.8 MB (25754237 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
