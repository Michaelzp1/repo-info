## `debian:stretch`

```console
$ docker pull debian@sha256:86269e614274db90a1d71dac258c74ed0a867a1d05f67dea6263b0f216ec7724
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386

### `debian:stretch` - linux; amd64

```console
$ docker pull debian@sha256:856ba93f897fb7d86ce4363461fe04b6997ce64a79e8198747e472464d58e0fa
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **45.4 MB (45379651 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5ddf6ebdcdb43d0b4b87d5c50996a322dbfd87aa2482febce521079422ba8b1f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:22:41 GMT
ADD file:084c8b3d38d578aa3910f3786b67b058962dbfdfd4a49d6e0201f2e91670873b in / 
# Tue, 12 Oct 2021 01:22:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2f0ef4316716c8b8925ba3665932f8bf2cef3b072d82de1aeb269d6b8b61b84c`  
		Last Modified: Tue, 12 Oct 2021 01:29:27 GMT  
		Size: 45.4 MB (45379651 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch` - linux; arm variant v5

```console
$ docker pull debian@sha256:80a932816893c940a3478402b574173be5fb7b1be76f35515fe8e1de61341f01
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **44.1 MB (44091932 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:634bcfa0a09aaabd232b426140bfbb01e8cd4fd43562b9698fbb7040133643fd`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:55:37 GMT
ADD file:eb65cc31f82e76c4ea5a8f21e4cad1372399517ee41a759740ff91d1d23b9e44 in / 
# Tue, 12 Oct 2021 00:55:38 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:d2f67fd5897d122073fc9ad347c1fb9faec31555c47b3ee6ff64ca2b553034ed`  
		Last Modified: Tue, 12 Oct 2021 01:13:31 GMT  
		Size: 44.1 MB (44091932 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch` - linux; arm variant v7

```console
$ docker pull debian@sha256:9434c91665ec2a24d73df1cea6956045563ee0d22fbf34946b8f1f3d69b865b8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **42.1 MB (42119423 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3c34ebc14ce655c7307d49d1b2be353ab021b23689a18bd48eb0e75b48b8b092`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:34:07 GMT
ADD file:eb5955ccd54a486767e85aeb1c03ffc6d8667b5476d0cf17b892ca407f1c8853 in / 
# Tue, 12 Oct 2021 01:34:08 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:8e9db3616ec4491d75e6c28254ff2376f24900797e7c4cc464f36a0c502b111f`  
		Last Modified: Tue, 12 Oct 2021 01:51:20 GMT  
		Size: 42.1 MB (42119423 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:fcfa90396a4e6d64fb2a721beefc43faa2e0b706b223581f6a1da33bc4d5cb76
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **43.2 MB (43176697 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:57945afea1d021f3fb89b42983c1ba68716bfc327205f96310c7088b02a848f3`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:16 GMT
ADD file:33c2886dff8a69edd550eb69499bf01658d58e2eff405d4b5d4cd5d5be535faa in / 
# Tue, 12 Oct 2021 01:43:16 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:cd2a530f50c2591d61de58d0e9cf724f6c5a2e1112456f38562e289732464c0c`  
		Last Modified: Tue, 12 Oct 2021 01:52:05 GMT  
		Size: 43.2 MB (43176697 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch` - linux; 386

```console
$ docker pull debian@sha256:8a71adf557086b1f0379142a24cbea502d9bc864b890f48819992b009118c481
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **46.1 MB (46097164 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:596fe040a7233ce2db4046c7bd5ebf3d528c3b7891acb28ee1a7720e7337e5e5`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:19 GMT
ADD file:5a6f08cc731f24357c3702231256fb05eb0a97733779b2ace406ba3c9bf64baa in / 
# Tue, 12 Oct 2021 01:42:19 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:d2ef4e90cca7406b4907a34e3571153fc56e3ecc9b2b5fdbb7787f22877cfec5`  
		Last Modified: Tue, 12 Oct 2021 01:51:58 GMT  
		Size: 46.1 MB (46097164 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
