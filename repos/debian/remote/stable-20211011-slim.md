## `debian:stable-20211011-slim`

```console
$ docker pull debian@sha256:6d6db11a446181868d088e4638f1392ea1489ce24a77d352d0d75d8e56307909
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:stable-20211011-slim` - linux; amd64

```console
$ docker pull debian@sha256:edb0a5915350ee6e2fedd8f9d0fe2e7f956f7a58f7f41b5e836e0bb7543e48a1
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **31.4 MB (31357347 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:11ed9f070876f7382b66e9ce1bcad0f5e40233f7f08a464bfde4477052500d0f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:22:31 GMT
ADD file:4f362433f6080825bee86a9f1c682bf4570f4057808bca78dd96338dca29c311 in / 
# Tue, 12 Oct 2021 01:22:31 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:adce49e07b069b00a50d0d61d9ee54a9b59b4b2cb77409ae75d28a1b976448db`  
		Last Modified: Tue, 12 Oct 2021 01:29:11 GMT  
		Size: 31.4 MB (31357347 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-20211011-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:b4350ec26fea1073d7498e82a410581c0b24e767c5d08a74e9cd7e1c1144a889
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **28.9 MB (28899694 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9c659a92c5d2e8754b8cd5879edbe847f5615b0cbfd13764b5bd3b8d02a1be51`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:55:12 GMT
ADD file:cbab9e3cfa5e8db683241e2f7954282bbbeba8f4c1c416d433e2f50ad37db705 in / 
# Tue, 12 Oct 2021 00:55:13 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:652ea08cf58402bb1137be61a410591890b542fa2fc82c6cb7bda2de1ae042b7`  
		Last Modified: Tue, 12 Oct 2021 01:12:51 GMT  
		Size: 28.9 MB (28899694 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-20211011-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:2ed3381dc520b76100dd6231c4d6cdbd76acec0d60a1532b9ec816f28ec17517
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **26.6 MB (26561055 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e48abc070ffd5f65a81e8c01f5901e0999abadebdfbde49ef50f959f4937d07d`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:33:39 GMT
ADD file:306bee95e9c083b8c53c224f1c95c4e0916066846898cc85fce2c4144a8f0cd6 in / 
# Tue, 12 Oct 2021 01:33:40 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:1a64e7c91f064d51d2fd3f2423bc75933ea608328a78574aad7a9c88c3875eb8`  
		Last Modified: Tue, 12 Oct 2021 01:50:44 GMT  
		Size: 26.6 MB (26561055 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-20211011-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:00bdd2cd7e6bba43444604b453d7895d5bd5fc52ec2a7b0afd2c390a9900b58b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.0 MB (30043947 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6b276e5b87dc34f09ad96d9eda412c63b5ceaa8d4333a3eeffe2633ed444236d`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:07 GMT
ADD file:a141cb3e243a0f0855631b8079045d039a85e867695553bc78439b4bf01c8616 in / 
# Tue, 12 Oct 2021 01:43:07 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:51da15902d5edd2c3ceaeb3d5051c3f5d0a1fb504bc463de8066e605a3f15cac`  
		Last Modified: Tue, 12 Oct 2021 01:51:48 GMT  
		Size: 30.0 MB (30043947 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-20211011-slim` - linux; 386

```console
$ docker pull debian@sha256:a10944bbb2cc079633dcbd85be26a633814cce8fe453e8130017d8f749873c70
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **32.4 MB (32370374 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:182cb73a54fc2611fdb8b3def1deb66c530eb51d7ab6f71d4ef23a40bf0773a6`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:07 GMT
ADD file:1142f2f5c40a76101ddf0224dad6d4f7524c7eedd06db2bc1912e9ea5eea4fed in / 
# Tue, 12 Oct 2021 01:42:07 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:1e6173dfff3ac1867162f6a4f7b2ce356942e122d4b6143ef90e6cb0fbd2bb52`  
		Last Modified: Tue, 12 Oct 2021 01:51:38 GMT  
		Size: 32.4 MB (32370374 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-20211011-slim` - linux; mips64le

```console
$ docker pull debian@sha256:a20313a0acb3000f827f9f8aaae7f3c33d246e130538a5b47225649b68e78ee9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.6 MB (29618729 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8cb8fea9bfeed77cf7ba0c072c921d0dfaf147a61b6e2a8b7e55221daeb76feb`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:14:42 GMT
ADD file:46e35382664a6e3726c33a3c7c76be2e48446e27eb64681767f23a9fd291bc85 in / 
# Tue, 12 Oct 2021 01:14:43 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6d182e03917a80138113af4d5d173014a12ee3690c9c029772d1a7bf22e15465`  
		Last Modified: Tue, 12 Oct 2021 01:25:34 GMT  
		Size: 29.6 MB (29618729 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-20211011-slim` - linux; ppc64le

```console
$ docker pull debian@sha256:8db5684ffe7ed1237db13c32675ab52e1fa17019cf44079725da72f2f4890330
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **35.3 MB (35258684 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e9771c04353c16a770e1e9a59ccf81f9d52934d37534ed284adedbc6c938ea60`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:29:20 GMT
ADD file:d79079de0552b16f985e2090473a38a7fb64d93f17af19025f98a0b8c630abf9 in / 
# Tue, 12 Oct 2021 01:29:24 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:0c92e5a5aea29b9e3de17e7228e4bca42228fcf6e447a558db18ce34a67eeb0c`  
		Last Modified: Tue, 12 Oct 2021 01:43:05 GMT  
		Size: 35.3 MB (35258684 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-20211011-slim` - linux; s390x

```console
$ docker pull debian@sha256:bdd6e283a992bbd2ecf2574d28a3e8a510be65bbea0c5e80aa49b410f2bae3fc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.6 MB (29641207 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4b4f4978f72e042065c09f6a961858efb6c75867f8ecb828c56356acc558108d`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:44:13 GMT
ADD file:477ffbcd999d81cc8e830ca9f8ae9991eb21cd3968dd5e50fcf817792bdb30c0 in / 
# Tue, 12 Oct 2021 00:44:15 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:e39be091b20ca683565fcae56fc2ba6848a4f4b424bb79407099ece6fae9b821`  
		Last Modified: Tue, 12 Oct 2021 00:50:11 GMT  
		Size: 29.6 MB (29641207 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
