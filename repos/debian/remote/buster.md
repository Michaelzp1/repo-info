## `debian:buster`

```console
$ docker pull debian@sha256:f9182ead292f45165f4a851e5ff98ea0800e172ccedce7d17764ffaae5ed4d6e
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:buster` - linux; amd64

```console
$ docker pull debian@sha256:2f4b2c4b44ec6d9f4afc9578d8475f7272dc50783268dc672d19b0db6098b89d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.4 MB (50436692 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:2b6f409b1d24285cbae8f2c6baa6969741151bed561a9b47c57b7ff1ee33829a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:20:53 GMT
ADD file:98c256057b79b141aea9a806a4538cf6c3f340d7e3b0d6e8c363699333f3406b in / 
# Tue, 12 Oct 2021 01:20:53 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:07471e81507f7cf1100827f10c60c3c0422d1222430e34e527d97ec72b14a193`  
		Last Modified: Tue, 12 Oct 2021 01:26:26 GMT  
		Size: 50.4 MB (50436692 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster` - linux; arm variant v5

```console
$ docker pull debian@sha256:9e9fbcf7fb8d2709289dac41865ef04aa00d313e88f87671f03d543a178f2243
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **48.2 MB (48154085 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:59313f9a9838cbb93bbac0a7840af610a5f4157a02dd303f3d18a7ca58220dcd`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:50:58 GMT
ADD file:f9432d6da0faddeaa117c57c0c007cd56738ef01549baf71e5c59939b7e69b6c in / 
# Tue, 12 Oct 2021 00:50:59 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:ae00de77d9c27fe3ba9ea06833b273e4fc0aa128a3a61d3992632e6e3941a78c`  
		Last Modified: Tue, 12 Oct 2021 01:07:01 GMT  
		Size: 48.2 MB (48154085 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster` - linux; arm variant v7

```console
$ docker pull debian@sha256:502cc09268a941c89fd917694127bee670e25885303627116fe30039bc01115b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **45.9 MB (45917899 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:03a2b8db893e04a8f924f52abea53a5f813cbea9ce26d34ff99c50e78d5ecbf2`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:29:06 GMT
ADD file:b1857dc4fe4fbc4af220829cc6af7169c4555d3f63a3a304db6fa0146b1d5539 in / 
# Tue, 12 Oct 2021 01:29:08 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:139a906e9407e96c50669130c30fe8fba2681b14aa838a3e52f8753b566ef1c8`  
		Last Modified: Tue, 12 Oct 2021 01:45:13 GMT  
		Size: 45.9 MB (45917899 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:e10bf96e3d994bb35989db5acd0bc38e689fa7e88d44acaea92188a1cbb62efe
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.2 MB (49222756 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:714cbbc292e7a7c0d9607c589230e6069290bd286c1ffa66e74d3e03008de43e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:28 GMT
ADD file:aed1709ccba6a81b9726b228fad7b81bcf4c16bafe723981ad37076322d78986 in / 
# Tue, 12 Oct 2021 01:41:29 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2ff6d7a9e7d73e4a01b9417518d18c001728c45fa8109ed8f55aaa50e7981482`  
		Last Modified: Tue, 12 Oct 2021 01:48:38 GMT  
		Size: 49.2 MB (49222756 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster` - linux; 386

```console
$ docker pull debian@sha256:d607eb89ee478cf01e2e664fcfd15788c88096a12c356855766a461c0d247397
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **51.2 MB (51207606 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:292a353f30cfc06a30e9044c17083c5cb39c4194ee653e82ef6fe3dd91ae0ef6`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:01 GMT
ADD file:1461fa0362c70b5b7a677c57dd48633827fd9635a9cb136730aa7581cc523b46 in / 
# Tue, 12 Oct 2021 01:40:02 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:f4b233498baa64e956a5f70979351e206bd085bd547a3cf25c08b154348df726`  
		Last Modified: Tue, 12 Oct 2021 01:48:07 GMT  
		Size: 51.2 MB (51207606 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster` - linux; mips64le

```console
$ docker pull debian@sha256:c44701e5d78443bfaf970f5fad82b90cd44b438f855c790667d3037eb4cac7e6
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.1 MB (49079545 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e09b392b225ac4e6c8dba0e912d0a360db896a7426b4317f436e5fecef0a9876`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:11:41 GMT
ADD file:37326203d7d5c7764007ca3a26d61a2da861011dddf8992bc1a0b70ba659c0c1 in / 
# Tue, 12 Oct 2021 01:11:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:f086aaa10cbe2dfccc93401fe3cce55b2f5eed2ba1a2fe3e34a4501644f9c8fa`  
		Last Modified: Tue, 12 Oct 2021 01:20:49 GMT  
		Size: 49.1 MB (49079545 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster` - linux; ppc64le

```console
$ docker pull debian@sha256:96c855e6aac97b7fee0b8c94e6dd1279d763b5e0e6e33ce10984cf658e14608c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.2 MB (54183476 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ab49c44234888d80796a160c0c11bf72ba4efa6e3f68cdee24313275ac4121d2`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:26:10 GMT
ADD file:94a7157f0c578810dcc73fd2dbdcb4ce021626d9858288c970e007a590c71d44 in / 
# Tue, 12 Oct 2021 01:26:18 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:77e7cc3fe486cc9a5ddc4cee43979cbebb5e7c4f36b82ccaa61dbda5dd37dac8`  
		Last Modified: Tue, 12 Oct 2021 01:37:52 GMT  
		Size: 54.2 MB (54183476 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster` - linux; s390x

```console
$ docker pull debian@sha256:94950a0190738d1fca34b5038075f702536c73be4ce2f6ecc69b0123507856aa
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.0 MB (49004847 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3ba87fd7547978ad82059f9a3be11170d199a614928b3f1a0f7f6346eb292b6a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:42:39 GMT
ADD file:91e4bb81a5308737580259a9213b02933901431aa2ea23f3f4f59321a6ccc301 in / 
# Tue, 12 Oct 2021 00:42:41 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:9df790508568720a3b71c02b057e4a119d9d2e8ed003ccba18d600e1ea44fa8a`  
		Last Modified: Tue, 12 Oct 2021 00:48:22 GMT  
		Size: 49.0 MB (49004847 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
