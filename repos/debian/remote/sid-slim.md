## `debian:sid-slim`

```console
$ docker pull debian@sha256:c8fafcd3e291857ebb5aaa15e86fbb5503edf73f5124cb6e02e30fd1b7e4cf12
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 9
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; riscv64
	-	linux; s390x

### `debian:sid-slim` - linux; amd64

```console
$ docker pull debian@sha256:025254bbfc82262031a971f63960d24ecf012eaf972e608b61756c631a34e6e1
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **31.7 MB (31731935 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9757525a4c83cdfed2a25cf6dd35a58ca447be791107d4424016205a37094286`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:22:08 GMT
ADD file:0f445d032ec45ac68f4ab9aec63d072544f2f037acfc934d6ad83c05e7a1c8f2 in / 
# Tue, 12 Oct 2021 01:22:08 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:fe4bf86b3222e855d7fbd5d536cbfd58a86d97b22ac9d818009064b8f0c9f70d`  
		Last Modified: Tue, 12 Oct 2021 01:28:35 GMT  
		Size: 31.7 MB (31731935 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:f943984a7e68c7a598c43182ac2100be6ab756828fe6d7b50d42ae4fea6c28bd
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.2 MB (29236158 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:abdb111ae5b6d0cadcf99ec5c556436f6e90ee20ffc4e18cd48bc36289230cdb`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:54:14 GMT
ADD file:0d62475df50475f297b93f15ebd9558d7d394da8162d787571a086573db86208 in / 
# Tue, 12 Oct 2021 00:54:15 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:267bc1ddf25b0a46bca086d5c858756088941d3e37d3b111768b17b9ef785f52`  
		Last Modified: Tue, 12 Oct 2021 01:11:31 GMT  
		Size: 29.2 MB (29236158 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:4056d12160b7dff77ff4b9fb55d961c338c316ae15958ff018299cb27c4d0016
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **26.8 MB (26844535 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:80b1e2ed697631a802b29456df07790656c3ab6ecf1e341926d19d5b88538210`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:32:37 GMT
ADD file:d9ca247784f1577677312109520a55abbf4398cf0945a671e4f6f06d138e457b in / 
# Tue, 12 Oct 2021 01:32:38 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:51a59c6e8056cfa5147497aa653cc2009174167aaf96d66180c3076399265071`  
		Last Modified: Tue, 12 Oct 2021 01:49:33 GMT  
		Size: 26.8 MB (26844535 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:c414ac6ff2f42868ab9d6a9b02d90690c598400306bb9bfe5e4a2a6461fcee8a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.7 MB (30748029 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:354990caee2d357d7107faff17cacd1d5f4fecb4bee72bf626e14a3b2e3667d2`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:44 GMT
ADD file:4fa890043902fb37f641132c1f05eb54a6426bc5acf42168b1325997d27630dd in / 
# Tue, 12 Oct 2021 01:42:45 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:72e9d9bbff420a8b05de6a0fca437727849fcd865412156e74a3ed3c2b31acc1`  
		Last Modified: Tue, 12 Oct 2021 01:51:06 GMT  
		Size: 30.7 MB (30748029 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-slim` - linux; 386

```console
$ docker pull debian@sha256:32b2f8d196a9f1747445653374060c6238a55b50c7214b8672b6b44a9b25a799
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **32.8 MB (32763456 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f74dcbe232aebc14a4ba21d9b45a955b2072340486ab485b9558d7279774b7bd`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:37 GMT
ADD file:aa104a082fed2b6420fcb08a1b08b7a35c9d1cba634e3870a61651e0c4cf2121 in / 
# Tue, 12 Oct 2021 01:41:38 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:080c5fa94c94c2285e3a5531cf014af5b9de902062387455579db76e75dc4e20`  
		Last Modified: Tue, 12 Oct 2021 01:50:51 GMT  
		Size: 32.8 MB (32763456 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-slim` - linux; mips64le

```console
$ docker pull debian@sha256:2941665bafbe82217b7b3d668a7ab38a63cda4893e1ae110f69bfd992c9e6e37
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.4 MB (30360295 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cd11b1196359a14617f6dd3ca909dca5163962510f3aba0cbfaefa4692b566ff`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:13:48 GMT
ADD file:714ca4b115daadc65209eb1d33c550e0471f06cb6c18d7a1fe6f696346fa9eaf in / 
# Tue, 12 Oct 2021 01:13:49 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:9f5541d536fc3aa4428caca822e0eef70cd3eb2f7b23664698d4885cc1314dff`  
		Last Modified: Tue, 12 Oct 2021 01:24:12 GMT  
		Size: 30.4 MB (30360295 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-slim` - linux; ppc64le

```console
$ docker pull debian@sha256:1db113adc6fa7fef63c252718f6b7d9524fcc696267215cc1c7797bb077a3853
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **35.9 MB (35935037 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a4c0bebab42996c2f005f3c5b4e3a30b11b7bf236da7876dc5658a42359de7b4`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:28:21 GMT
ADD file:e21a675ddadf39c3bb49dd0416d283aa3d9d566b255843cf2ca1cb7c4daf1f03 in / 
# Tue, 12 Oct 2021 01:28:26 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:7b0d2761efc65ea068376f8b2415c02777aed2d1e2dc740230380f851fd81948`  
		Last Modified: Tue, 12 Oct 2021 01:41:36 GMT  
		Size: 35.9 MB (35935037 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-slim` - linux; riscv64

```console
$ docker pull debian@sha256:b7858e5b27d186b682918e8011a529b0ebc618d704b3ef45d5235929bb5d751b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **27.6 MB (27563282 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b44b4afb292f9c55cca5cccda82c0a4feebb92f7f307daaf9e90f23b3d584c32`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:15:58 GMT
ADD file:f208b8fbff4c817dbc6db3f55963b389fe47f3c48e7f64b85471d9d125d7875f in / 
# Tue, 12 Oct 2021 01:15:59 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:4a0a5284a0463692c510b467e923e38595722dff8de4e8e5bc7aa2dae33c4337`  
		Last Modified: Tue, 12 Oct 2021 01:32:01 GMT  
		Size: 27.6 MB (27563282 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-slim` - linux; s390x

```console
$ docker pull debian@sha256:42a3541da7b01a4bce2c8b7822a7083b890d7a264b9cf95d86883e1bf1b2d022
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.0 MB (29984176 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:aa7d655906ea245de10ad3c9019a2d68a96f89951b43d8a1f1b9fc7b9bb46268`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:43:46 GMT
ADD file:6665e978a6ce5d437da63343803505d8c4635f430d97517512e64159e3b6ef7c in / 
# Tue, 12 Oct 2021 00:43:48 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c1a10e05e4a1ae95d84297c58d62e4ab685c5e256be0d8395d81ad943a8524db`  
		Last Modified: Tue, 12 Oct 2021 00:49:40 GMT  
		Size: 30.0 MB (29984176 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
