## `debian:oldoldstable-backports`

```console
$ docker pull debian@sha256:2bf0e1838199188582790c3db41c7a5c282546b209ffae0cc8a507f6ed6ac93d
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386

### `debian:oldoldstable-backports` - linux; amd64

```console
$ docker pull debian@sha256:d7bbc327d978d1fbdb680cace784a5af91f829381e2ceaa8b122fbb2d9a3404c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **45.4 MB (45379877 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9367c4a8363f79fac39d0630280e18ed848a3b149b404cc8402a2aed909efc43`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:21:15 GMT
ADD file:2dad8aa47b7c0abdcaccbf1c501793fd733f172a30177cc5a5906a7e8c94163d in / 
# Tue, 12 Oct 2021 01:21:15 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:21:18 GMT
RUN echo 'deb http://deb.debian.org/debian oldoldstable-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:a8b1a171ce712e7503b1f72abb7b9751bff88e01e4413539244c27b04b96af58`  
		Last Modified: Tue, 12 Oct 2021 01:27:11 GMT  
		Size: 45.4 MB (45379650 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:6d7255bd0b84533d575c620d180348c49ca21fc2e1a349eae841a239993cd4a4`  
		Last Modified: Tue, 12 Oct 2021 01:27:20 GMT  
		Size: 227.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable-backports` - linux; arm variant v5

```console
$ docker pull debian@sha256:44d7e3e3784429c105ccd6c64f9229c7c86840f2b150c303f9a5cf5ef7af7d9b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **44.1 MB (44092162 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:bf26e4cceede1e07c759dd5b9762e61798ff82db0e24d58e238d70d9a69b4d73`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:51:59 GMT
ADD file:562359b7dd2e2aded9f637c664877f9d2a7e1a5ca24b5b0f87032c0c6f63ff05 in / 
# Tue, 12 Oct 2021 00:52:00 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 00:52:13 GMT
RUN echo 'deb http://deb.debian.org/debian oldoldstable-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:65b249c69eb3aa72e4d5fc5c9152f2138252bc14d2e8245381d9860d13f3e4fe`  
		Last Modified: Tue, 12 Oct 2021 01:08:32 GMT  
		Size: 44.1 MB (44091934 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:796f25118818d6c99e7a3375e61f0f3d3c2d6db97af4b4dcce6bfa2cdfb526d2`  
		Last Modified: Tue, 12 Oct 2021 01:08:44 GMT  
		Size: 228.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable-backports` - linux; arm variant v7

```console
$ docker pull debian@sha256:634d7edda7c9c0538ba9fcbfb9a149befe77d39a48b86300f799ed0f33de10ab
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **42.1 MB (42119594 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:2c4c1426e5f97be9161dd9e3e3bed7079f9f7f4d41b6ca168573c42dd518a691`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:30:12 GMT
ADD file:5ea5714223f1698d41da52f17d7b6936e82f8e1f41f19e00b5e300b8f329d58b in / 
# Tue, 12 Oct 2021 01:30:13 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:30:25 GMT
RUN echo 'deb http://deb.debian.org/debian oldoldstable-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:ee5cec6429f90405fa16db25cfe4f7bf7d03099e0372790dc8f0a457d9d0a244`  
		Last Modified: Tue, 12 Oct 2021 01:46:47 GMT  
		Size: 42.1 MB (42119365 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:5eb86b790eca2e063d7cbaa9e5c9c1023e66ea32b0b7b5201679667260ae4ff9`  
		Last Modified: Tue, 12 Oct 2021 01:46:58 GMT  
		Size: 229.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable-backports` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:ca7a14702c0a5f89e06b3dc64904dd27a835fa2311945fad9dd2cb1f7ba87da9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **43.2 MB (43176955 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:bd1ead9ec72fa33e82cceabd5eff8953614a80ba297eb3d2b7f92985d37be182`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:52 GMT
ADD file:82529472573ae109efa01a1b422a0c016903bc71b937668f7c4ee238c356afe3 in / 
# Tue, 12 Oct 2021 01:41:53 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:41:58 GMT
RUN echo 'deb http://deb.debian.org/debian oldoldstable-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:1682a7bcf61dffda8aa42e61da61e3c02fbfdd938351384f3ff623d8d6465b64`  
		Last Modified: Tue, 12 Oct 2021 01:49:30 GMT  
		Size: 43.2 MB (43176728 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:b8ddaf2b98c1bd4bd5d39d4021a096879c08483dcbf1d16525b549f4b945b160`  
		Last Modified: Tue, 12 Oct 2021 01:49:41 GMT  
		Size: 227.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable-backports` - linux; 386

```console
$ docker pull debian@sha256:77345914a052130fc478b10e5258a752cab94ffc309abdb913d3f8eea59da565
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **46.1 MB (46097437 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:0810587cba56246e8d02c52acc2082f8aae1e2df5ae11f20bcbc8f039fdc949c`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:30 GMT
ADD file:4fcadf4f144c0e7b2f9e6cd377b183799f017ea7ec585a314d6deaec3ca13a6c in / 
# Tue, 12 Oct 2021 01:40:31 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:40:36 GMT
RUN echo 'deb http://deb.debian.org/debian oldoldstable-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:5fcdd168800cca93cd361730f88c052b1ec6ddfced4c88f830d939fb2d6c8f3b`  
		Last Modified: Tue, 12 Oct 2021 01:49:04 GMT  
		Size: 46.1 MB (46097211 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:1b0b98772c895bb9d7bf4ce6953ba50207c7d202b427517cf09efb4c78376c45`  
		Last Modified: Tue, 12 Oct 2021 01:49:15 GMT  
		Size: 226.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
