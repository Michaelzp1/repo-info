## `debian:bookworm-slim`

```console
$ docker pull debian@sha256:ae9bdb5571feb6badec4eaaff9e44e1e6d696ce67ee6a398284f511d859f3730
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:bookworm-slim` - linux; amd64

```console
$ docker pull debian@sha256:92d67639b39672a47c94e8e586d86116edb8d52870b0a37e896a80f304fc1d1a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **31.7 MB (31692052 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6927ab6f448b8c84f7554aae52d50c9a24cc5fc81292eb2de8a8cf3158479caa`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:20:19 GMT
ADD file:b19679c81e3d98d3cbd0b6ec72d2a362c09962e0ce831a2e38ac17ef87ddabd2 in / 
# Tue, 12 Oct 2021 01:20:19 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:545b369240c1b746a743d71bbe363b94bdb88045ff39f617813addb017a9db38`  
		Last Modified: Tue, 12 Oct 2021 01:25:20 GMT  
		Size: 31.7 MB (31692052 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:4c429af93819092d97ccd126171dcd6cbf797684d421265f86bc977a564bd371
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.2 MB (29211548 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9733ad9c58755ab9df6e8ee94ec2665f029d264fd8015f5b1ac0df91afbbae9f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:49:27 GMT
ADD file:1cc1654614b2d094a458e8a8551dd8176e62f3195bbb89459e45510fc11960de in / 
# Tue, 12 Oct 2021 00:49:28 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:9f46836bdbba00fc5266132714febc7f811c9a8359e7d244cc2c49f6107e8382`  
		Last Modified: Tue, 12 Oct 2021 01:04:31 GMT  
		Size: 29.2 MB (29211548 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:b5304ad3e82f2ab7071253d354e021ccaabe40b759d3038751e529408883b611
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **26.8 MB (26813217 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:86b6db36bd9dfd99fdf162784510d9f86d31a20918c7cef886d3e1f5a36396df`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:27:31 GMT
ADD file:c440250bfbd5321f6c02335a37021debd8b16c1cf17995cc724890c57bfee83e in / 
# Tue, 12 Oct 2021 01:27:31 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:50027ff2d1b31d50bb37b1d5e30f980285612ecaba26d55a99bc567ecfd51f2b`  
		Last Modified: Tue, 12 Oct 2021 01:43:02 GMT  
		Size: 26.8 MB (26813217 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:28f38a1292e397c8f95f93f8fe75e21416dbef17444e9d9ad780f425622d7f48
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.7 MB (30711467 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1a7cb47ea1a5c1bc4199d0fff286400dcdff083f52d892ff58a36c25d6211142`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:54 GMT
ADD file:40ef674c8f82aba05c38db6e5d27ff51c37bb7c7ac1198caa88fc063cb0e60b8 in / 
# Tue, 12 Oct 2021 01:40:54 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:00ac796bd8c532aef4ad77ab8175a39e0bca984f27287b38605b53b3f8190fd2`  
		Last Modified: Tue, 12 Oct 2021 01:47:22 GMT  
		Size: 30.7 MB (30711467 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-slim` - linux; 386

```console
$ docker pull debian@sha256:de0155830775f43a6753ca34c2ab24293902c52ed3df1966b026ce796daa8f99
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **32.7 MB (32726559 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:96f9f2758d836d3df24920a85b7b3c29fd57a0473eeb9ff38e04923715656ddf`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:39:16 GMT
ADD file:6d87e7e36b81c240e58aa7316772916c99b8423eacee000d2f6956dd9356f0e1 in / 
# Tue, 12 Oct 2021 01:39:17 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c91ec16b79b47c46caca08a0fa27f06df0e4f4c8c5d1e489dff2c0e4af8495e1`  
		Last Modified: Tue, 12 Oct 2021 01:46:43 GMT  
		Size: 32.7 MB (32726559 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-slim` - linux; mips64le

```console
$ docker pull debian@sha256:fd7fd4a53285c3aade29f5fb63fe4fe34ab8ad924b1328943204634aac690bd2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.3 MB (30313364 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:760bced86d5035543c1b4ec51a6df5e0c3ee4f3a3f23bdff74f70a6c8d19b5ba`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:10:22 GMT
ADD file:87aba135412a9bdf9f8da22d5a6eb90259bd6e54a71e7946443abca33b58867b in / 
# Tue, 12 Oct 2021 01:10:23 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:8e21180f99b22caf985c06275e081e2fcb5cfebaad7874eea3bec5af3f36f639`  
		Last Modified: Tue, 12 Oct 2021 01:18:36 GMT  
		Size: 30.3 MB (30313364 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-slim` - linux; ppc64le

```console
$ docker pull debian@sha256:e4ccc66ce1c20510bb0f3037745e765507832b91cc77fe8ec675fe4ca47585b8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **35.9 MB (35900678 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3f736f04688ed59050ee8d4848f17d26577a1cfee0d3739ced37f4abbaee3674`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:24:44 GMT
ADD file:5c750a1cf418050da8164b02e79d89893775c8cb82cba89ce6d2f820f4e43139 in / 
# Tue, 12 Oct 2021 01:24:49 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6281a0df95aaa56f35d9c470a0b63d5dcf2bab02587999175c26f131fc898534`  
		Last Modified: Tue, 12 Oct 2021 01:35:18 GMT  
		Size: 35.9 MB (35900678 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-slim` - linux; s390x

```console
$ docker pull debian@sha256:98a9cc4b4aeb8eb0d8f81f60155e1c2ed1a2c228678d2aca4047082f04b582c9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.9 MB (29947073 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:287ecd72602d334147536fbaa8c09a03503bd0d35ba2812d6e35b7774b7a8411`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:41:58 GMT
ADD file:fa2051ff320c558a9da82f6d05b5bc8144b1bbd925e103978a8493e9341ea7da in / 
# Tue, 12 Oct 2021 00:42:00 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:e44f3cf4e8b7f77baa7d2ac41c1a985b13c519dcfcb0667fb483f72860f50fca`  
		Last Modified: Tue, 12 Oct 2021 00:47:30 GMT  
		Size: 29.9 MB (29947073 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
