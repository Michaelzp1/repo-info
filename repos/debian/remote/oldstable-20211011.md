## `debian:oldstable-20211011`

```console
$ docker pull debian@sha256:843fdcc419eba12e43deff8b955d3b06f7a4296ddc8916b1fed048446c0356d2
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:oldstable-20211011` - linux; amd64

```console
$ docker pull debian@sha256:66b1a790407e21eab0f1c3cd8a5aacadcc2b70c0c865f87976cd6221eb64db2b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.4 MB (50436730 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:dc4441f19678c8bf67f5183b81e95491594f4154a51a84638eb939d799ec40b6`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:21:36 GMT
ADD file:1254fe58a557c9bd7a44702af996bc01a041007fa31c4bfd0674a7437f83e226 in / 
# Tue, 12 Oct 2021 01:21:36 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:486fdf79d93406106977e297ffa5f755ab39b223d63b7cf84a14c0ff8f62a9d2`  
		Last Modified: Tue, 12 Oct 2021 01:27:45 GMT  
		Size: 50.4 MB (50436730 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011` - linux; arm variant v5

```console
$ docker pull debian@sha256:173b47ac2d6db7999ce60877b7487600566ae307f2ab38d9953cdfd74590d27a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **48.2 MB (48154060 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:88ece4f04cfb212a3f78cac2c0cbd34357ebea607d6260d6c7c1daec40c25d44`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:52:56 GMT
ADD file:a042a1b4d39d508b4725145149caf6615d44e43427b4566e3839840e9dec939e in / 
# Tue, 12 Oct 2021 00:52:57 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:e187af41423780e5cc95f89de4b56a3c18a2c5636f1919be23b07449d54381a7`  
		Last Modified: Tue, 12 Oct 2021 01:09:44 GMT  
		Size: 48.2 MB (48154060 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011` - linux; arm variant v7

```console
$ docker pull debian@sha256:f81dd238be9034087264afc0dead1422c67347b03c4aa730e2e86337f6e18ce9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **45.9 MB (45917917 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:297db7d35883781b4abf5c1c520ad049e705249fba2d259ded490c9692c2a54b`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:31:12 GMT
ADD file:075763af19fd2f62d5199e9f9ece478e2602aee33c939ccd258f599d88469046 in / 
# Tue, 12 Oct 2021 01:31:13 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:e1e8a087bec2ff439032f91ace1191030ed684bf5807987913cb1d995565a032`  
		Last Modified: Tue, 12 Oct 2021 01:47:54 GMT  
		Size: 45.9 MB (45917917 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:ed3651fcf95b5fc5e1cba1b3de53e0501d2682a0978e0b800ec489ae6dd565aa
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.2 MB (49222718 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a6dcb8339dd72e9a03e5a41eb75f960e6bc1ab1e07146e53c2be2aa92e35892a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:14 GMT
ADD file:10280ad72673a7baf3b2455f9bc16ba1c50a1d0c5a8276d89e2a786b1c84af75 in / 
# Tue, 12 Oct 2021 01:42:14 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2d37bb723b5cb5b3b65952b63d77d16450a085709a53d71e1e3251bd609ea570`  
		Last Modified: Tue, 12 Oct 2021 01:50:11 GMT  
		Size: 49.2 MB (49222718 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011` - linux; 386

```console
$ docker pull debian@sha256:5cc593a0418586ee8d8063e1febdc5418e742e47fd3bbd36f4cfd90b83f7b0a1
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **51.2 MB (51207558 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:db65077c2e16181149a43424ec2c9edaf084b70bfcac9b5cb0ee4c54f136feb3`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:58 GMT
ADD file:53dbb1fd8cfb15946c2df51556975d015470c45ae4ca8033fba41c5f31356d52 in / 
# Tue, 12 Oct 2021 01:40:58 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:1f657a8783f7ec563f233d088f92dcd9bf9e3a39b1d449cb9fab27caed5aadb0`  
		Last Modified: Tue, 12 Oct 2021 01:49:47 GMT  
		Size: 51.2 MB (51207558 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011` - linux; mips64le

```console
$ docker pull debian@sha256:9178b403e2489c89d302a4275208d85f89916ff7170498745b738bb7dd4a7b74
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.1 MB (49079493 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:07c603e98ef5819b15fddd70fca5fa1093b1c81c3db58f07364d2bd5da5a2b1f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:12:34 GMT
ADD file:e8a4c8f46e0308d588aabdfa8cef54cbc9e873569fc4f06b1fb6942cb106c5b0 in / 
# Tue, 12 Oct 2021 01:12:35 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:771cec173f62208002de95d47a62059a81d70a4b0c86620fcc0f9dd6ff5deee2`  
		Last Modified: Tue, 12 Oct 2021 01:22:21 GMT  
		Size: 49.1 MB (49079493 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011` - linux; ppc64le

```console
$ docker pull debian@sha256:184f45d671a31ebc88fd5c7894fe8a38061f42d4cba5ffe1331af2034fb75d1b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.2 MB (54183530 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9b9071633d8c700d6759209257667ba09ac2910bdc81c32a65ce9891317fdacd`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:27:07 GMT
ADD file:1685b474962ea6fe3945c29bc49da37313319ebb22343899681a2e98b188f7a7 in / 
# Tue, 12 Oct 2021 01:27:12 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:f80c6b99f8408b2bde68df0b7885d92c0fcd5531033f895599a7d09df37fe5e2`  
		Last Modified: Tue, 12 Oct 2021 01:39:27 GMT  
		Size: 54.2 MB (54183530 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011` - linux; s390x

```console
$ docker pull debian@sha256:99e6214d374d8dd98b2d9feb31b61c8b1a7953a8d6034f3555d3138d9c78194f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.0 MB (49004893 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:c893c71789973beb7ae24997a12fd8adb6337a63a79205985622acb81fd21e9e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:43:08 GMT
ADD file:131966fcf68790a9e9a3c79c1be0f33ee52522f57e0d286fee2f2441daba4718 in / 
# Tue, 12 Oct 2021 00:43:10 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:e95a5b27ccd32e7e7d080f127e6d02afb66719bec59086cf85da5f76bf785fdf`  
		Last Modified: Tue, 12 Oct 2021 00:48:58 GMT  
		Size: 49.0 MB (49004893 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
