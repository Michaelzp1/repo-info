## `debian:bullseye-20211011-slim`

```console
$ docker pull debian@sha256:dddc0f5f01db7ca3599fd8cf9821ffc4d09ec9d7d15e49019e73228ac1eee7f9
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:bullseye-20211011-slim` - linux; amd64

```console
$ docker pull debian@sha256:312218c8dae688bae4e9d12926704fa9af6f7307a6edb4f66e479702a9af5a0c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **31.4 MB (31357311 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:dd984c2cf05c58c61026b0bd2298b30aa87bca6f234db507396371137c891a6c`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:20:42 GMT
ADD file:16dc2c6d1932194edec28d730b004fd6deca3d0f0e1a07bc5b8b6e8a1662f7af in / 
# Tue, 12 Oct 2021 01:20:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:7d63c13d9b9b6ec5f05a2b07daadacaa9c610d01102a662ae9b1d082105f1ffa`  
		Last Modified: Tue, 12 Oct 2021 01:26:05 GMT  
		Size: 31.4 MB (31357311 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-20211011-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:6b8e1746e462cd13b1d954f55a6275a7eeed331a633cf0d62b17e5c6fe4983a2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **28.9 MB (28899715 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:83ed6052a5a4c461efabe92d02b3067de1d84e9f619dbb0f973eaf1bcc7872ac`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:50:30 GMT
ADD file:fb1afabdc1f93b4866b4c4d696f272b4888519cb5bf35eb7ed1a4c021d1a04c8 in / 
# Tue, 12 Oct 2021 00:50:30 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:5c9bab66abc5c53391435f6fd66e0ff3571295d568f45ac7d2dc480bcbfd56e8`  
		Last Modified: Tue, 12 Oct 2021 01:06:07 GMT  
		Size: 28.9 MB (28899715 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-20211011-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:c61311dc78459c6c94b87c3ae3ad070449e532d276459d395ee01645c2208676
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **26.6 MB (26561058 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b45f159c2abea86ab575dff875e962b1b64cabd5787d919253aab1db083a4d91`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:28:36 GMT
ADD file:89eac94007ac04f9168737686fa0a6c737f2c28fc9e5918d4d512924fe1973be in / 
# Tue, 12 Oct 2021 01:28:37 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:4e5300249f84466df4dc73ea0ce09938ca00c1718bb12619c4f26bd936162331`  
		Last Modified: Tue, 12 Oct 2021 01:44:28 GMT  
		Size: 26.6 MB (26561058 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-20211011-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:6b5a9ba2bf7d60c09ddee95638a8421a4ad182706052882f24f7069542891b88
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.0 MB (30043906 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:2b66020322207f385b8d57efb4be7222fa200473d803a7bcabf7c11c03f95839`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:18 GMT
ADD file:d84144ad575672cd6cc8a00c8e5a88ab0545348f040fc249ea5fcf8193b2bce6 in / 
# Tue, 12 Oct 2021 01:41:18 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:a9eb63951c1c2ee8efcc12b696928fee3741a2d4eae76f2da3e161a5d90548bf`  
		Last Modified: Tue, 12 Oct 2021 01:48:13 GMT  
		Size: 30.0 MB (30043906 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-20211011-slim` - linux; 386

```console
$ docker pull debian@sha256:5e70f265871e466baa10bf4ab07c62ca56b88d72d8504feacb466b2367e4a22a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **32.4 MB (32370344 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6f31b9786954c6a7ca8b1ed9c5da3397cef6ecea93626779c64675e2052b1d3f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:39:47 GMT
ADD file:42196ffa4c70af8d9f1e7b74edb3bb92d47296acf989adf615e8208230f8bd0c in / 
# Tue, 12 Oct 2021 01:39:47 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:87318d165b5c0fdf05c8ccf97d83084f56b4608075a3335b1a46c76295b82753`  
		Last Modified: Tue, 12 Oct 2021 01:47:39 GMT  
		Size: 32.4 MB (32370344 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-20211011-slim` - linux; mips64le

```console
$ docker pull debian@sha256:1fb2dc326cae06c54451f67ade73c85a23fe49a84de793146069a58c374cf833
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.6 MB (29618732 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:91f0d7e87aa154d1593aad58bebf939b9bb22590172455e65978a9b81bb858ee`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:11:15 GMT
ADD file:f90ca8957172106f42a9096c9a21082d51f3201aa78e6f64621a73117e2b7b6a in / 
# Tue, 12 Oct 2021 01:11:16 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:59ddca50ce05605e6234c1e7213c39cdedabc48ef11637e12156b7bd69afe161`  
		Last Modified: Tue, 12 Oct 2021 01:20:03 GMT  
		Size: 29.6 MB (29618732 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-20211011-slim` - linux; ppc64le

```console
$ docker pull debian@sha256:222db950b210833c502a932f306b728f6fd711035b2470facf689ed4286df69c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **35.3 MB (35258729 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8452d145c52c5b9d0ebed595d8909727e790af2b9b4a2d31c52918018c608fde`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:25:45 GMT
ADD file:d8794fadf16c9948c3574ba28740d08393eca57766340333ad91f9c2a33eb719 in / 
# Tue, 12 Oct 2021 01:25:53 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6c28c1405c677dd809094d06190225b38cee3d24f18da38d287ef5bcc67884d6`  
		Last Modified: Tue, 12 Oct 2021 01:37:00 GMT  
		Size: 35.3 MB (35258729 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-20211011-slim` - linux; s390x

```console
$ docker pull debian@sha256:6b06aa7d0d589aaeb2d76e1488bd1d1984a2f8524eef19e66592b80a66308489
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.6 MB (29641215 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:077e43ea2b6a64971755f52a382c17917c68ab24bb5eb5ee848be6ba18f4f675`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:42:27 GMT
ADD file:6038dd6db57fb05c3d39c02c3379667ccd2989e7667ff773a8020fe6a69a760c in / 
# Tue, 12 Oct 2021 00:42:29 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:ded751c48f72503973be01be2794cc039490f22b039b8ac106e9f17de4980742`  
		Last Modified: Tue, 12 Oct 2021 00:48:05 GMT  
		Size: 29.6 MB (29641215 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
