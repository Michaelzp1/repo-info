## `debian:unstable-slim`

```console
$ docker pull debian@sha256:da81f85e40b587612c1b9366acdd9236c86dbcd93d1294295309c3cf8e386611
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 9
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; riscv64
	-	linux; s390x

### `debian:unstable-slim` - linux; amd64

```console
$ docker pull debian@sha256:8e09fb9dd5cc5aab6acfecce9a2eef3afb8d5514ecd1d4d2c9f18d5c4f8ca88b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **31.7 MB (31731974 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5218a74f4da567f6569712bf6b318713a19792e439d3503f9c1bef0490575e10`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:23:36 GMT
ADD file:017f678793e737c4eb26779b05dc1ff7542751e024938abaee2d3077c1822fa5 in / 
# Tue, 12 Oct 2021 01:23:36 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:f1c9427fc1e1e92049b6f5a6e7384a4507ae9ec93d75e72cd2530cccbd680310`  
		Last Modified: Tue, 12 Oct 2021 01:31:02 GMT  
		Size: 31.7 MB (31731974 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:2877f16f3620d7d2678e374a952de4e87ed2e4db3f37fd32fcb61b58b511b9bd
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.2 MB (29236155 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:07ce4fb8c7619d8fa5045731f280cb88a7f870681fc786414ffaaf7a42d059fd`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:57:59 GMT
ADD file:0a16c1d34be07dd282f59442792c1a3c13be01192aadb601d17937b5cfc7014a in / 
# Tue, 12 Oct 2021 00:58:01 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2667e161c1f445181b931350f0e137a01480e8b7aee7cd443260c58639223801`  
		Last Modified: Tue, 12 Oct 2021 01:17:03 GMT  
		Size: 29.2 MB (29236155 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:a16615d23bb3ae70642414f20f5a1f37166e6b8f4e89a9304ed4b0bbcbe037a0
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **26.8 MB (26844545 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:23e8689c159aa7100dcd0eaa27e838b42342bb048bbdb30a17abd725913dd2b0`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:36:36 GMT
ADD file:1aa4f2f1a9b196606788d4ef8971354311546ec87785e728eda6dd28dc8f0703 in / 
# Tue, 12 Oct 2021 01:36:37 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:af234ad4bc1a66f08150bb3e47a8655217a5881489b3d16455925299896aeb48`  
		Last Modified: Tue, 12 Oct 2021 01:54:25 GMT  
		Size: 26.8 MB (26844545 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:af707b5c78af806aaf2d1d6ec331c1b762b58cb93f95a3d8eaabe5d247dca739
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.7 MB (30748056 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f4a5cb9dbdc7c7eb2093d86aa39f75c8dd017a5d7400842f01613d6a907a2fed`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:44:10 GMT
ADD file:75c40743ce1d29b5bfad592cdd0e7bfc50a4294cd2fc2ec8a843f5a547afbfb9 in / 
# Tue, 12 Oct 2021 01:44:11 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:41402d960b89f43a20d3b0c4116e3623db2ae2dd0cf72e13625714e0dec92fe5`  
		Last Modified: Tue, 12 Oct 2021 01:53:53 GMT  
		Size: 30.7 MB (30748056 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-slim` - linux; 386

```console
$ docker pull debian@sha256:02867a14ce4a996d6f184ae4abef54c066546c567f664bf7728f1f196d6c10ba
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **32.8 MB (32763493 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:482f53a8414dd8375bd522122b3c96c56094ec2fb3f0c6f7baac24925f503c9c`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:28 GMT
ADD file:1f6c67031a134cc1488a3dd5b1afc48aa91f1fec0ef43163ec45e701d7fb5749 in / 
# Tue, 12 Oct 2021 01:43:28 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:8cc6f6761bfcf3ebcef675e4caf6fbec3b60204771c0bf05086986885db488be`  
		Last Modified: Tue, 12 Oct 2021 01:54:00 GMT  
		Size: 32.8 MB (32763493 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-slim` - linux; mips64le

```console
$ docker pull debian@sha256:38fdd1f36c850b19a74240cad66459dd0bf602a55165a1259ef90aaa33a5c9cf
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.4 MB (30360328 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:99b7fec1197a157131a21239056ab2a719be0e0ada47605224848da0043a588f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:16:25 GMT
ADD file:6993b092f03b469e7400686851bcfcebca618af437e056b1b666cef37b5b6153 in / 
# Tue, 12 Oct 2021 01:16:26 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:64a2ff80f178e33cf773999a407067f90dd9d3949b2dc79b39737ac25cf811c4`  
		Last Modified: Tue, 12 Oct 2021 01:28:08 GMT  
		Size: 30.4 MB (30360328 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-slim` - linux; ppc64le

```console
$ docker pull debian@sha256:e8381228ea8e6a2a603580be529c3cc8110b24d4d824d2adea3e77ba1a3ad802
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **35.9 MB (35935015 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d40481c899bc8e69850cc571fa027ae297f0d3fcdf5d02515f4ba43bbd5761af`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:31:16 GMT
ADD file:1e32ff7653c6a38b7fc55329c42957dd45ef1084a910cbd6219c735493d864b6 in / 
# Tue, 12 Oct 2021 01:31:22 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:49b65047f6921d4626cbf4019e684e789b9fee599da822a3305cb2cdccabb92c`  
		Last Modified: Tue, 12 Oct 2021 01:44:31 GMT  
		Size: 35.9 MB (35935015 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-slim` - linux; riscv64

```console
$ docker pull debian@sha256:105f953c74ec0c00abd27a3c2aa0bc61f2d37e16c3d99a707db829c9b74a03fe
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **27.6 MB (27563313 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8c08e4d8abe9c035035af444293cd7f20ffa362541bfa2b71b9def71e975174b`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:19:32 GMT
ADD file:d023bb4e594d57c08428052992601ba913854065f3d8481129223f986c2eabb8 in / 
# Tue, 12 Oct 2021 01:19:34 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:5dad889f11fd648e2e18c46f30ea00d750cf9c392dbabfcc4f72c2e09e3d0af0`  
		Last Modified: Tue, 12 Oct 2021 01:35:28 GMT  
		Size: 27.6 MB (27563313 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-slim` - linux; s390x

```console
$ docker pull debian@sha256:a989409c6363abb33a1a3d379073d3fddcf10d5e00cdd9b2f06c36f228f2aaa8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.0 MB (29984209 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:7787b1607637a684546f1ccc6a546b31c16edae7b06dda2232c4756f4bdba847`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:45:04 GMT
ADD file:9cf952a889ba9dc2c4e86a75aca595d35e0df14dec61963f64769d55df851e40 in / 
# Tue, 12 Oct 2021 00:45:05 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:0c3ff48a9ed28edd72e1e14389926020dc484e092e7db31244fc783a52859923`  
		Last Modified: Tue, 12 Oct 2021 00:51:07 GMT  
		Size: 30.0 MB (29984209 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
