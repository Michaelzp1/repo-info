## `debian:oldoldstable-slim`

```console
$ docker pull debian@sha256:26e0a9d420a9e06ab03cc7a69743fbfd888fc2af159f1837a0e94729d9d329e2
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386

### `debian:oldoldstable-slim` - linux; amd64

```console
$ docker pull debian@sha256:5d1e4df796f8ee15cbfd5e46932af78c0d351c6053e44c17c0c9f95acdc67f68
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **22.5 MB (22527518 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d852065264ac8a67bdb251ec797a82b7c9fac214c893ee302aced55997ef22ce`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:21:26 GMT
ADD file:6f0d65d34e99acc0a838582bb0c52c8b12eadb39798d231caf702a8f3a9f4de2 in / 
# Tue, 12 Oct 2021 01:21:26 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:1599ff3b20b129431eddbefc2dc08a6a0aa8ef68faf34d44eea8c9cfdf5eb921`  
		Last Modified: Tue, 12 Oct 2021 01:27:30 GMT  
		Size: 22.5 MB (22527518 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:1c1201bdfd440e4c7f9ae2c594825f3f62e4f4485eae81b122f22424d745006f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **21.2 MB (21204338 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9138868e8021053dbf3833b593e719e5132d5539ea38441c9c7fabad87a2bbc3`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:52:30 GMT
ADD file:287e595e03955546f25eb897aa7553ee626ae6c6b64b8164d5d3b42297641edc in / 
# Tue, 12 Oct 2021 00:52:31 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:cee58a8ba33bca4d50453537ed50c73bed23501940fc2a62da2ac84e166732e7`  
		Last Modified: Tue, 12 Oct 2021 01:09:04 GMT  
		Size: 21.2 MB (21204338 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:cba44244e91c243cd754845d3b910882c83f8c6324c8d4c4a3505840f5f8145d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **19.3 MB (19316443 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:825f907336d8205e7b0130eabb99133cde40dbaf9ca278d973318836dda78f46`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:30:44 GMT
ADD file:dfe14f5069e960939daf937d576bddf08c2dd9386730c01bc365ac610b035f71 in / 
# Tue, 12 Oct 2021 01:30:45 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:dc641641f7cb36d80035d98c2c9fe584419cdca7cf5ded32111b25f4945753fe`  
		Last Modified: Tue, 12 Oct 2021 01:47:18 GMT  
		Size: 19.3 MB (19316443 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:07f91a5d79ab9d7d492307d76b77db6e241d5f830bd0d6323f7a03cf2add0608
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **20.4 MB (20389428 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e9e8adb4c56e2f66a90d67602595080738aeb7f949a6ed4881f2fd101fc69e5e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:04 GMT
ADD file:5d456d87f1a04e598f5e60aff2dfe22e2c5e218cc255dd5383e4a29659c30471 in / 
# Tue, 12 Oct 2021 01:42:05 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:51a573003c28d47d7b57a5fef8f423cf0a6d3c7f1f7fd8b538b4ae893fa860d4`  
		Last Modified: Tue, 12 Oct 2021 01:49:52 GMT  
		Size: 20.4 MB (20389428 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable-slim` - linux; 386

```console
$ docker pull debian@sha256:4f4f5898707a9955c71cb072fa46effc42556fb4829ed1f2ff71758df4d8213e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **23.2 MB (23156644 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e2078108904ce9e8978355229f5e5c709403d783d7cf4c34adbc74354722e644`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:45 GMT
ADD file:0c0934e7f8d30895fa74e47b73ad9064299cbf54de898ee430dbcde8b3a2d130 in / 
# Tue, 12 Oct 2021 01:40:45 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:0ff128fa75c485cac31b556b81d71c790608f3404c66504c40ac9940109223bb`  
		Last Modified: Tue, 12 Oct 2021 01:49:26 GMT  
		Size: 23.2 MB (23156644 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
