## `debian:testing`

```console
$ docker pull debian@sha256:e3f8186278fd321c0b3460d6d423c1bc81557d09a8974036bf24de55b90d708f
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:testing` - linux; amd64

```console
$ docker pull debian@sha256:76025017aeddaa14bcfcc3f53f3432dc73a2c8977df9350d13d2cc28a591a96c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.4 MB (55445865 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4022f69526eecf543e97b259c48e368c0ac69f539f1f006ac2c9f69fecdc0530`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:23:03 GMT
ADD file:7a2d92b4684fdb24b1c954a390700dbb0a50ce8cc8774b959e562a3652fb0456 in / 
# Tue, 12 Oct 2021 01:23:03 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:91c31f9cd4fd949265f5532465cddce98935dcfa86015a5348b5f47c344d67e0`  
		Last Modified: Tue, 12 Oct 2021 01:30:11 GMT  
		Size: 55.4 MB (55445865 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing` - linux; arm variant v5

```console
$ docker pull debian@sha256:9d678ad4d51a5275fccddf0a00c6733bec3bbc732ec0bdbef5699f5b45e5146b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.0 MB (52964860 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:75bee44fbb8531b937b8fc6677c583c4dfe9dbe3cd74f918877122bd3e295f35`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:56:39 GMT
ADD file:a0ded502d0b8dfe5e897243e08b3778f3adc2f2f41c32568ca6681c428d2896c in / 
# Tue, 12 Oct 2021 00:56:40 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:10d62d8715d4ae164047d47200167e5ce725ffbc04256c778e0aba73de1fd27b`  
		Last Modified: Tue, 12 Oct 2021 01:15:02 GMT  
		Size: 53.0 MB (52964860 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing` - linux; arm variant v7

```console
$ docker pull debian@sha256:dda29e24c653c90ad3b5b4303590c97ac5ba98be2e19b217c58c95539418e0b2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.6 MB (50566085 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b551a927d2971e313f53905918a67117a4e269f9b04623f2b56c9ff128febe9e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:35:12 GMT
ADD file:4927232089e749d84b21863f4f2612c8a799f3039adcc70d971cbea78ed6a7af in / 
# Tue, 12 Oct 2021 01:35:13 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:bec27cf3f9ae45321e058c161bc06cb3640cc32d6d8cc5cd5119b254da7ecfca`  
		Last Modified: Tue, 12 Oct 2021 01:52:46 GMT  
		Size: 50.6 MB (50566085 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:0d97fee927f66da36b469718694d9c2384887faa39b4e510046ba2662763f09d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.5 MB (54465025 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:37726bb7e5af704bc1fabf9de18e72f40569e9d7fd364082fde69118ef50c863`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:39 GMT
ADD file:0d2781f09dc7fd32dad3f41e34a91046910847a56bf128bb53a7cad6c06c1f26 in / 
# Tue, 12 Oct 2021 01:43:40 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:0a75495c66c5fc986e1fa178fa94fe24ac603e1ee8a61ff4b344624e0b8b030e`  
		Last Modified: Tue, 12 Oct 2021 01:52:57 GMT  
		Size: 54.5 MB (54465025 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing` - linux; 386

```console
$ docker pull debian@sha256:47c810fa726452b53ce4e980d4ce1ecdf09c788439b41e2136f09e6157728c7d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **56.5 MB (56480852 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:85200d26145b3631fd03e8a3487999608f30534862eb4ddcb5599cf90b2bf0cd`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:48 GMT
ADD file:15469ad0ddbee66d623cee7627f9ae7a0e09baf6095d23599c5cb2ae7493abae in / 
# Tue, 12 Oct 2021 01:42:49 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:8508f9607417ad80810488921b0ada480e2ce5d8a1cb8f2303e5d0f3a1d2174e`  
		Last Modified: Tue, 12 Oct 2021 01:52:56 GMT  
		Size: 56.5 MB (56480852 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing` - linux; mips64le

```console
$ docker pull debian@sha256:3a8f5e0fbfd8d20e6c718d488d40d2b67bde613080d549f04ead60ccdc2f484c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.1 MB (54069071 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b705bc479a321116963aaaf14b8b38e2fd963bd8b430edea5d866fcd28c8efc1`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:15:09 GMT
ADD file:3d46a1b0da2f717a6fcd5a81011ddf94c751043fb103b98d488647a531fed650 in / 
# Tue, 12 Oct 2021 01:15:10 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:4216a1c7f16a580bb4ab02a3c570e7f8ccf383c93ed4aebda567c184fedb4fc6`  
		Last Modified: Tue, 12 Oct 2021 01:26:17 GMT  
		Size: 54.1 MB (54069071 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing` - linux; ppc64le

```console
$ docker pull debian@sha256:7f17ba887c708c29105ed16fb1142fc78e5bc1c2a7d529bed58ead7c83507768
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **59.7 MB (59659961 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:0f7a4e81d010e0501a02b9389b55eb09c038b2de245805d91479bd80d5c92ea2`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:29:42 GMT
ADD file:2920b1ef5c61978464fc969befdade3714d84884adee006fb93d4d89bb412093 in / 
# Tue, 12 Oct 2021 01:29:48 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:7695f94c27f2511cd3e23671d32882f166a2fc1b8124ec1f9d3f769e88536556`  
		Last Modified: Tue, 12 Oct 2021 01:43:25 GMT  
		Size: 59.7 MB (59659961 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing` - linux; s390x

```console
$ docker pull debian@sha256:a50a383979142b1b2425e3bb5861275308f801c038984641aa41da4e01551344
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.7 MB (53700056 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:48e41d87d85e9e7a6b313ccf471ba63bff3fe1ef999525c94f979d240c227428`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:44:24 GMT
ADD file:6bdd28da982bbaaa3e5fd43949b430a741f7441a3423aea45476b602884003fb in / 
# Tue, 12 Oct 2021 00:44:27 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:16455b9c3f308b090e540412543381f2981a94842cb89496c8f0d1636c7ad1da`  
		Last Modified: Tue, 12 Oct 2021 00:50:24 GMT  
		Size: 53.7 MB (53700056 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
