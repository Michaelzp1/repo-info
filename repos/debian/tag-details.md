<!-- THIS FILE IS GENERATED VIA './update-remote.sh' -->

# Tags of `debian`

-	[`debian:10`](#debian10)
-	[`debian:10-slim`](#debian10-slim)
-	[`debian:10.11`](#debian1011)
-	[`debian:10.11-slim`](#debian1011-slim)
-	[`debian:11`](#debian11)
-	[`debian:11-slim`](#debian11-slim)
-	[`debian:11.1`](#debian111)
-	[`debian:11.1-slim`](#debian111-slim)
-	[`debian:9`](#debian9)
-	[`debian:9-slim`](#debian9-slim)
-	[`debian:9.13`](#debian913)
-	[`debian:9.13-slim`](#debian913-slim)
-	[`debian:bookworm`](#debianbookworm)
-	[`debian:bookworm-20211011`](#debianbookworm-20211011)
-	[`debian:bookworm-20211011-slim`](#debianbookworm-20211011-slim)
-	[`debian:bookworm-backports`](#debianbookworm-backports)
-	[`debian:bookworm-slim`](#debianbookworm-slim)
-	[`debian:bullseye`](#debianbullseye)
-	[`debian:bullseye-20211011`](#debianbullseye-20211011)
-	[`debian:bullseye-20211011-slim`](#debianbullseye-20211011-slim)
-	[`debian:bullseye-backports`](#debianbullseye-backports)
-	[`debian:bullseye-slim`](#debianbullseye-slim)
-	[`debian:buster`](#debianbuster)
-	[`debian:buster-20211011`](#debianbuster-20211011)
-	[`debian:buster-20211011-slim`](#debianbuster-20211011-slim)
-	[`debian:buster-backports`](#debianbuster-backports)
-	[`debian:buster-slim`](#debianbuster-slim)
-	[`debian:experimental`](#debianexperimental)
-	[`debian:experimental-20211011`](#debianexperimental-20211011)
-	[`debian:latest`](#debianlatest)
-	[`debian:oldoldstable`](#debianoldoldstable)
-	[`debian:oldoldstable-20211011`](#debianoldoldstable-20211011)
-	[`debian:oldoldstable-20211011-slim`](#debianoldoldstable-20211011-slim)
-	[`debian:oldoldstable-backports`](#debianoldoldstable-backports)
-	[`debian:oldoldstable-slim`](#debianoldoldstable-slim)
-	[`debian:oldstable`](#debianoldstable)
-	[`debian:oldstable-20211011`](#debianoldstable-20211011)
-	[`debian:oldstable-20211011-slim`](#debianoldstable-20211011-slim)
-	[`debian:oldstable-backports`](#debianoldstable-backports)
-	[`debian:oldstable-slim`](#debianoldstable-slim)
-	[`debian:rc-buggy`](#debianrc-buggy)
-	[`debian:rc-buggy-20211011`](#debianrc-buggy-20211011)
-	[`debian:sid`](#debiansid)
-	[`debian:sid-20211011`](#debiansid-20211011)
-	[`debian:sid-20211011-slim`](#debiansid-20211011-slim)
-	[`debian:sid-slim`](#debiansid-slim)
-	[`debian:stable`](#debianstable)
-	[`debian:stable-20211011`](#debianstable-20211011)
-	[`debian:stable-20211011-slim`](#debianstable-20211011-slim)
-	[`debian:stable-backports`](#debianstable-backports)
-	[`debian:stable-slim`](#debianstable-slim)
-	[`debian:stretch`](#debianstretch)
-	[`debian:stretch-20211011`](#debianstretch-20211011)
-	[`debian:stretch-20211011-slim`](#debianstretch-20211011-slim)
-	[`debian:stretch-backports`](#debianstretch-backports)
-	[`debian:stretch-slim`](#debianstretch-slim)
-	[`debian:testing`](#debiantesting)
-	[`debian:testing-20211011`](#debiantesting-20211011)
-	[`debian:testing-20211011-slim`](#debiantesting-20211011-slim)
-	[`debian:testing-backports`](#debiantesting-backports)
-	[`debian:testing-slim`](#debiantesting-slim)
-	[`debian:unstable`](#debianunstable)
-	[`debian:unstable-20211011`](#debianunstable-20211011)
-	[`debian:unstable-20211011-slim`](#debianunstable-20211011-slim)
-	[`debian:unstable-slim`](#debianunstable-slim)

## `debian:10`

```console
$ docker pull debian@sha256:f9182ead292f45165f4a851e5ff98ea0800e172ccedce7d17764ffaae5ed4d6e
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:10` - linux; amd64

```console
$ docker pull debian@sha256:2f4b2c4b44ec6d9f4afc9578d8475f7272dc50783268dc672d19b0db6098b89d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.4 MB (50436692 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:2b6f409b1d24285cbae8f2c6baa6969741151bed561a9b47c57b7ff1ee33829a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:20:53 GMT
ADD file:98c256057b79b141aea9a806a4538cf6c3f340d7e3b0d6e8c363699333f3406b in / 
# Tue, 12 Oct 2021 01:20:53 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:07471e81507f7cf1100827f10c60c3c0422d1222430e34e527d97ec72b14a193`  
		Last Modified: Tue, 12 Oct 2021 01:26:26 GMT  
		Size: 50.4 MB (50436692 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10` - linux; arm variant v5

```console
$ docker pull debian@sha256:9e9fbcf7fb8d2709289dac41865ef04aa00d313e88f87671f03d543a178f2243
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **48.2 MB (48154085 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:59313f9a9838cbb93bbac0a7840af610a5f4157a02dd303f3d18a7ca58220dcd`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:50:58 GMT
ADD file:f9432d6da0faddeaa117c57c0c007cd56738ef01549baf71e5c59939b7e69b6c in / 
# Tue, 12 Oct 2021 00:50:59 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:ae00de77d9c27fe3ba9ea06833b273e4fc0aa128a3a61d3992632e6e3941a78c`  
		Last Modified: Tue, 12 Oct 2021 01:07:01 GMT  
		Size: 48.2 MB (48154085 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10` - linux; arm variant v7

```console
$ docker pull debian@sha256:502cc09268a941c89fd917694127bee670e25885303627116fe30039bc01115b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **45.9 MB (45917899 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:03a2b8db893e04a8f924f52abea53a5f813cbea9ce26d34ff99c50e78d5ecbf2`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:29:06 GMT
ADD file:b1857dc4fe4fbc4af220829cc6af7169c4555d3f63a3a304db6fa0146b1d5539 in / 
# Tue, 12 Oct 2021 01:29:08 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:139a906e9407e96c50669130c30fe8fba2681b14aa838a3e52f8753b566ef1c8`  
		Last Modified: Tue, 12 Oct 2021 01:45:13 GMT  
		Size: 45.9 MB (45917899 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:e10bf96e3d994bb35989db5acd0bc38e689fa7e88d44acaea92188a1cbb62efe
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.2 MB (49222756 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:714cbbc292e7a7c0d9607c589230e6069290bd286c1ffa66e74d3e03008de43e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:28 GMT
ADD file:aed1709ccba6a81b9726b228fad7b81bcf4c16bafe723981ad37076322d78986 in / 
# Tue, 12 Oct 2021 01:41:29 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2ff6d7a9e7d73e4a01b9417518d18c001728c45fa8109ed8f55aaa50e7981482`  
		Last Modified: Tue, 12 Oct 2021 01:48:38 GMT  
		Size: 49.2 MB (49222756 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10` - linux; 386

```console
$ docker pull debian@sha256:d607eb89ee478cf01e2e664fcfd15788c88096a12c356855766a461c0d247397
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **51.2 MB (51207606 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:292a353f30cfc06a30e9044c17083c5cb39c4194ee653e82ef6fe3dd91ae0ef6`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:01 GMT
ADD file:1461fa0362c70b5b7a677c57dd48633827fd9635a9cb136730aa7581cc523b46 in / 
# Tue, 12 Oct 2021 01:40:02 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:f4b233498baa64e956a5f70979351e206bd085bd547a3cf25c08b154348df726`  
		Last Modified: Tue, 12 Oct 2021 01:48:07 GMT  
		Size: 51.2 MB (51207606 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10` - linux; mips64le

```console
$ docker pull debian@sha256:c44701e5d78443bfaf970f5fad82b90cd44b438f855c790667d3037eb4cac7e6
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.1 MB (49079545 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e09b392b225ac4e6c8dba0e912d0a360db896a7426b4317f436e5fecef0a9876`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:11:41 GMT
ADD file:37326203d7d5c7764007ca3a26d61a2da861011dddf8992bc1a0b70ba659c0c1 in / 
# Tue, 12 Oct 2021 01:11:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:f086aaa10cbe2dfccc93401fe3cce55b2f5eed2ba1a2fe3e34a4501644f9c8fa`  
		Last Modified: Tue, 12 Oct 2021 01:20:49 GMT  
		Size: 49.1 MB (49079545 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10` - linux; ppc64le

```console
$ docker pull debian@sha256:96c855e6aac97b7fee0b8c94e6dd1279d763b5e0e6e33ce10984cf658e14608c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.2 MB (54183476 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ab49c44234888d80796a160c0c11bf72ba4efa6e3f68cdee24313275ac4121d2`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:26:10 GMT
ADD file:94a7157f0c578810dcc73fd2dbdcb4ce021626d9858288c970e007a590c71d44 in / 
# Tue, 12 Oct 2021 01:26:18 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:77e7cc3fe486cc9a5ddc4cee43979cbebb5e7c4f36b82ccaa61dbda5dd37dac8`  
		Last Modified: Tue, 12 Oct 2021 01:37:52 GMT  
		Size: 54.2 MB (54183476 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10` - linux; s390x

```console
$ docker pull debian@sha256:94950a0190738d1fca34b5038075f702536c73be4ce2f6ecc69b0123507856aa
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.0 MB (49004847 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3ba87fd7547978ad82059f9a3be11170d199a614928b3f1a0f7f6346eb292b6a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:42:39 GMT
ADD file:91e4bb81a5308737580259a9213b02933901431aa2ea23f3f4f59321a6ccc301 in / 
# Tue, 12 Oct 2021 00:42:41 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:9df790508568720a3b71c02b057e4a119d9d2e8ed003ccba18d600e1ea44fa8a`  
		Last Modified: Tue, 12 Oct 2021 00:48:22 GMT  
		Size: 49.0 MB (49004847 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:10-slim`

```console
$ docker pull debian@sha256:544c93597c784cf68dbe492ef35c00de7f4f6a990955c7144a40b20d86a3475f
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:10-slim` - linux; amd64

```console
$ docker pull debian@sha256:f6ed7ce6e3264649e1d4f40585247c50e32faaf268984c5c5cbf0e67cf7f0ec7
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **27.1 MB (27139510 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8ae0cebc834a83dc033065409120379a215e64d4d717449a56f211c10c8f1c95`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:21:05 GMT
ADD file:910392427fdf089bc26b64d6dc450ff3d020c7c1a474d85b2f9298134d0007bd in / 
# Tue, 12 Oct 2021 01:21:05 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:b380bbd43752f83945df8b5d1074fef8dd044820e7d3aef33b655a2483e030c7`  
		Last Modified: Tue, 12 Oct 2021 01:26:51 GMT  
		Size: 27.1 MB (27139510 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:5cf40d31707d8371b9f7657b08fe5757f66b7541f0ba60a6884e3a228f3826e2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **24.9 MB (24872704 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f5327b97db05809d81919bb3751bf09f5ec0862d38c8f871f47d088bb4c13b8e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:51:32 GMT
ADD file:0d95eee31ffe3f4260263e4fac2a61123a4b98d2e7a67efcc7bfea338c54f41d in / 
# Tue, 12 Oct 2021 00:51:32 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:175b083b22fb061f7d1894fe4916a2967ce636d47376da526d90d57c8f7efc60`  
		Last Modified: Tue, 12 Oct 2021 01:07:45 GMT  
		Size: 24.9 MB (24872704 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:f865df940558a112dfac67eef50714dbd4bc2a82f6d302cd2c133649c2b4288e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **22.7 MB (22739698 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:c97f71b3175eb8ee139a3abbb2090e87dedf50b0199d0b2baa920a1e11293556`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:29:42 GMT
ADD file:fe1eaa1b1d760b3d250cb4ac331d26a261fc569f1694bd34e146f00874dc87e5 in / 
# Tue, 12 Oct 2021 01:29:43 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c0b5ba470cac8cfdae4633f5cddb3ba9350fe1d1b1507c4965a8224eb40e52c5`  
		Last Modified: Tue, 12 Oct 2021 01:45:52 GMT  
		Size: 22.7 MB (22739698 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:9f6e6f1f5a4665a552f46028808b28ab19788d28db470de6822febf710f47914
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **25.9 MB (25908479 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d5dbc8809dc0af0cdd5b012ad95108771f6957859cfe7e4ccf0695de3242db94`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:42 GMT
ADD file:f0d53027a7ba594477674127971aa477af3a2bc6bcddef6c0aa174953a5f2db0 in / 
# Tue, 12 Oct 2021 01:41:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:02b45931a436a68cadb742684148ed6aacbbf9aa21447b467c11bac97f92eb46`  
		Last Modified: Tue, 12 Oct 2021 01:49:07 GMT  
		Size: 25.9 MB (25908479 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10-slim` - linux; 386

```console
$ docker pull debian@sha256:870b187824ecb90dbb968ff713e30ad950c42c6b2ddbae30559a48694b87fd77
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **27.8 MB (27791445 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d7232f6968596f6edce31c8b1a83faff4770cafddcf44639813b6ec911d827ee`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:17 GMT
ADD file:1b432d2306b487c59442b30c65ddabd08b45484c6ce09b0c25112c29f4a98f17 in / 
# Tue, 12 Oct 2021 01:40:17 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:860d012fe46ce39c3737ace24d10d8ad65ae9c78abde6891ca6e97b0d7f271dd`  
		Last Modified: Tue, 12 Oct 2021 01:48:37 GMT  
		Size: 27.8 MB (27791445 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10-slim` - linux; mips64le

```console
$ docker pull debian@sha256:b642ad734ac5033873def85d8471bee90fc194a27105eb358e92f25c58c155d7
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **25.8 MB (25806535 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d6273f44d21d6b75bfcf57caf8935a94139bfd43f9fdcc44e4287d185150a495`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:12:07 GMT
ADD file:a467fbf2015350da455cad98b5f5f247d88749cf317062ca09c466c27d53f87b in / 
# Tue, 12 Oct 2021 01:12:07 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:4d8cdee9d607e77b6f8c2203cc9cc02b38004def2b7fe162538c1538da10c870`  
		Last Modified: Tue, 12 Oct 2021 01:21:36 GMT  
		Size: 25.8 MB (25806535 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10-slim` - linux; ppc64le

```console
$ docker pull debian@sha256:ac88814d59305f903079944b7b647c813beceeca3e7ad7920604cd90b9dbd130
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.5 MB (30547197 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6e05e4d22ac1078c5c920a3d1b28da35d0e0c4770f9dc7fb505394229f218b43`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:26:46 GMT
ADD file:5526880c6f19a4b4231a8b2bd7cfc625d116764c4918eff6f0b55f8c1eb38e1a in / 
# Tue, 12 Oct 2021 01:26:50 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:d69973af32573c07c8789c0f9c144a5c0b822a1e85b59db3ce9f8ebc489ce85d`  
		Last Modified: Tue, 12 Oct 2021 01:38:40 GMT  
		Size: 30.5 MB (30547197 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10-slim` - linux; s390x

```console
$ docker pull debian@sha256:007b10cc082c948b5a45f7298e8776c5b0df77331ab3e2bf00b7ed98d85f9d8e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **25.8 MB (25754252 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4340acf96cb3e2fec8d744a74977a94566b488869b7b467c9c1571a45cc40b95`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:42:56 GMT
ADD file:39da9acd4d06d69f3ca8d25ef5c097ae741972d6b15a6a057bc7487380157b61 in / 
# Tue, 12 Oct 2021 00:42:57 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:78a640c6bb4733e5156ce97c5ec773cf97b357c3a07244348384da5060788a61`  
		Last Modified: Tue, 12 Oct 2021 00:48:41 GMT  
		Size: 25.8 MB (25754252 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:10.11`

```console
$ docker pull debian@sha256:f9182ead292f45165f4a851e5ff98ea0800e172ccedce7d17764ffaae5ed4d6e
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:10.11` - linux; amd64

```console
$ docker pull debian@sha256:2f4b2c4b44ec6d9f4afc9578d8475f7272dc50783268dc672d19b0db6098b89d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.4 MB (50436692 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:2b6f409b1d24285cbae8f2c6baa6969741151bed561a9b47c57b7ff1ee33829a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:20:53 GMT
ADD file:98c256057b79b141aea9a806a4538cf6c3f340d7e3b0d6e8c363699333f3406b in / 
# Tue, 12 Oct 2021 01:20:53 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:07471e81507f7cf1100827f10c60c3c0422d1222430e34e527d97ec72b14a193`  
		Last Modified: Tue, 12 Oct 2021 01:26:26 GMT  
		Size: 50.4 MB (50436692 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10.11` - linux; arm variant v5

```console
$ docker pull debian@sha256:9e9fbcf7fb8d2709289dac41865ef04aa00d313e88f87671f03d543a178f2243
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **48.2 MB (48154085 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:59313f9a9838cbb93bbac0a7840af610a5f4157a02dd303f3d18a7ca58220dcd`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:50:58 GMT
ADD file:f9432d6da0faddeaa117c57c0c007cd56738ef01549baf71e5c59939b7e69b6c in / 
# Tue, 12 Oct 2021 00:50:59 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:ae00de77d9c27fe3ba9ea06833b273e4fc0aa128a3a61d3992632e6e3941a78c`  
		Last Modified: Tue, 12 Oct 2021 01:07:01 GMT  
		Size: 48.2 MB (48154085 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10.11` - linux; arm variant v7

```console
$ docker pull debian@sha256:502cc09268a941c89fd917694127bee670e25885303627116fe30039bc01115b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **45.9 MB (45917899 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:03a2b8db893e04a8f924f52abea53a5f813cbea9ce26d34ff99c50e78d5ecbf2`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:29:06 GMT
ADD file:b1857dc4fe4fbc4af220829cc6af7169c4555d3f63a3a304db6fa0146b1d5539 in / 
# Tue, 12 Oct 2021 01:29:08 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:139a906e9407e96c50669130c30fe8fba2681b14aa838a3e52f8753b566ef1c8`  
		Last Modified: Tue, 12 Oct 2021 01:45:13 GMT  
		Size: 45.9 MB (45917899 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10.11` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:e10bf96e3d994bb35989db5acd0bc38e689fa7e88d44acaea92188a1cbb62efe
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.2 MB (49222756 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:714cbbc292e7a7c0d9607c589230e6069290bd286c1ffa66e74d3e03008de43e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:28 GMT
ADD file:aed1709ccba6a81b9726b228fad7b81bcf4c16bafe723981ad37076322d78986 in / 
# Tue, 12 Oct 2021 01:41:29 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2ff6d7a9e7d73e4a01b9417518d18c001728c45fa8109ed8f55aaa50e7981482`  
		Last Modified: Tue, 12 Oct 2021 01:48:38 GMT  
		Size: 49.2 MB (49222756 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10.11` - linux; 386

```console
$ docker pull debian@sha256:d607eb89ee478cf01e2e664fcfd15788c88096a12c356855766a461c0d247397
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **51.2 MB (51207606 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:292a353f30cfc06a30e9044c17083c5cb39c4194ee653e82ef6fe3dd91ae0ef6`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:01 GMT
ADD file:1461fa0362c70b5b7a677c57dd48633827fd9635a9cb136730aa7581cc523b46 in / 
# Tue, 12 Oct 2021 01:40:02 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:f4b233498baa64e956a5f70979351e206bd085bd547a3cf25c08b154348df726`  
		Last Modified: Tue, 12 Oct 2021 01:48:07 GMT  
		Size: 51.2 MB (51207606 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10.11` - linux; mips64le

```console
$ docker pull debian@sha256:c44701e5d78443bfaf970f5fad82b90cd44b438f855c790667d3037eb4cac7e6
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.1 MB (49079545 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e09b392b225ac4e6c8dba0e912d0a360db896a7426b4317f436e5fecef0a9876`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:11:41 GMT
ADD file:37326203d7d5c7764007ca3a26d61a2da861011dddf8992bc1a0b70ba659c0c1 in / 
# Tue, 12 Oct 2021 01:11:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:f086aaa10cbe2dfccc93401fe3cce55b2f5eed2ba1a2fe3e34a4501644f9c8fa`  
		Last Modified: Tue, 12 Oct 2021 01:20:49 GMT  
		Size: 49.1 MB (49079545 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10.11` - linux; ppc64le

```console
$ docker pull debian@sha256:96c855e6aac97b7fee0b8c94e6dd1279d763b5e0e6e33ce10984cf658e14608c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.2 MB (54183476 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ab49c44234888d80796a160c0c11bf72ba4efa6e3f68cdee24313275ac4121d2`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:26:10 GMT
ADD file:94a7157f0c578810dcc73fd2dbdcb4ce021626d9858288c970e007a590c71d44 in / 
# Tue, 12 Oct 2021 01:26:18 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:77e7cc3fe486cc9a5ddc4cee43979cbebb5e7c4f36b82ccaa61dbda5dd37dac8`  
		Last Modified: Tue, 12 Oct 2021 01:37:52 GMT  
		Size: 54.2 MB (54183476 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10.11` - linux; s390x

```console
$ docker pull debian@sha256:94950a0190738d1fca34b5038075f702536c73be4ce2f6ecc69b0123507856aa
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.0 MB (49004847 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3ba87fd7547978ad82059f9a3be11170d199a614928b3f1a0f7f6346eb292b6a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:42:39 GMT
ADD file:91e4bb81a5308737580259a9213b02933901431aa2ea23f3f4f59321a6ccc301 in / 
# Tue, 12 Oct 2021 00:42:41 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:9df790508568720a3b71c02b057e4a119d9d2e8ed003ccba18d600e1ea44fa8a`  
		Last Modified: Tue, 12 Oct 2021 00:48:22 GMT  
		Size: 49.0 MB (49004847 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:10.11-slim`

```console
$ docker pull debian@sha256:544c93597c784cf68dbe492ef35c00de7f4f6a990955c7144a40b20d86a3475f
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:10.11-slim` - linux; amd64

```console
$ docker pull debian@sha256:f6ed7ce6e3264649e1d4f40585247c50e32faaf268984c5c5cbf0e67cf7f0ec7
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **27.1 MB (27139510 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8ae0cebc834a83dc033065409120379a215e64d4d717449a56f211c10c8f1c95`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:21:05 GMT
ADD file:910392427fdf089bc26b64d6dc450ff3d020c7c1a474d85b2f9298134d0007bd in / 
# Tue, 12 Oct 2021 01:21:05 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:b380bbd43752f83945df8b5d1074fef8dd044820e7d3aef33b655a2483e030c7`  
		Last Modified: Tue, 12 Oct 2021 01:26:51 GMT  
		Size: 27.1 MB (27139510 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10.11-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:5cf40d31707d8371b9f7657b08fe5757f66b7541f0ba60a6884e3a228f3826e2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **24.9 MB (24872704 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f5327b97db05809d81919bb3751bf09f5ec0862d38c8f871f47d088bb4c13b8e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:51:32 GMT
ADD file:0d95eee31ffe3f4260263e4fac2a61123a4b98d2e7a67efcc7bfea338c54f41d in / 
# Tue, 12 Oct 2021 00:51:32 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:175b083b22fb061f7d1894fe4916a2967ce636d47376da526d90d57c8f7efc60`  
		Last Modified: Tue, 12 Oct 2021 01:07:45 GMT  
		Size: 24.9 MB (24872704 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10.11-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:f865df940558a112dfac67eef50714dbd4bc2a82f6d302cd2c133649c2b4288e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **22.7 MB (22739698 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:c97f71b3175eb8ee139a3abbb2090e87dedf50b0199d0b2baa920a1e11293556`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:29:42 GMT
ADD file:fe1eaa1b1d760b3d250cb4ac331d26a261fc569f1694bd34e146f00874dc87e5 in / 
# Tue, 12 Oct 2021 01:29:43 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c0b5ba470cac8cfdae4633f5cddb3ba9350fe1d1b1507c4965a8224eb40e52c5`  
		Last Modified: Tue, 12 Oct 2021 01:45:52 GMT  
		Size: 22.7 MB (22739698 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10.11-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:9f6e6f1f5a4665a552f46028808b28ab19788d28db470de6822febf710f47914
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **25.9 MB (25908479 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d5dbc8809dc0af0cdd5b012ad95108771f6957859cfe7e4ccf0695de3242db94`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:42 GMT
ADD file:f0d53027a7ba594477674127971aa477af3a2bc6bcddef6c0aa174953a5f2db0 in / 
# Tue, 12 Oct 2021 01:41:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:02b45931a436a68cadb742684148ed6aacbbf9aa21447b467c11bac97f92eb46`  
		Last Modified: Tue, 12 Oct 2021 01:49:07 GMT  
		Size: 25.9 MB (25908479 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10.11-slim` - linux; 386

```console
$ docker pull debian@sha256:870b187824ecb90dbb968ff713e30ad950c42c6b2ddbae30559a48694b87fd77
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **27.8 MB (27791445 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d7232f6968596f6edce31c8b1a83faff4770cafddcf44639813b6ec911d827ee`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:17 GMT
ADD file:1b432d2306b487c59442b30c65ddabd08b45484c6ce09b0c25112c29f4a98f17 in / 
# Tue, 12 Oct 2021 01:40:17 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:860d012fe46ce39c3737ace24d10d8ad65ae9c78abde6891ca6e97b0d7f271dd`  
		Last Modified: Tue, 12 Oct 2021 01:48:37 GMT  
		Size: 27.8 MB (27791445 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10.11-slim` - linux; mips64le

```console
$ docker pull debian@sha256:b642ad734ac5033873def85d8471bee90fc194a27105eb358e92f25c58c155d7
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **25.8 MB (25806535 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d6273f44d21d6b75bfcf57caf8935a94139bfd43f9fdcc44e4287d185150a495`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:12:07 GMT
ADD file:a467fbf2015350da455cad98b5f5f247d88749cf317062ca09c466c27d53f87b in / 
# Tue, 12 Oct 2021 01:12:07 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:4d8cdee9d607e77b6f8c2203cc9cc02b38004def2b7fe162538c1538da10c870`  
		Last Modified: Tue, 12 Oct 2021 01:21:36 GMT  
		Size: 25.8 MB (25806535 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10.11-slim` - linux; ppc64le

```console
$ docker pull debian@sha256:ac88814d59305f903079944b7b647c813beceeca3e7ad7920604cd90b9dbd130
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.5 MB (30547197 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6e05e4d22ac1078c5c920a3d1b28da35d0e0c4770f9dc7fb505394229f218b43`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:26:46 GMT
ADD file:5526880c6f19a4b4231a8b2bd7cfc625d116764c4918eff6f0b55f8c1eb38e1a in / 
# Tue, 12 Oct 2021 01:26:50 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:d69973af32573c07c8789c0f9c144a5c0b822a1e85b59db3ce9f8ebc489ce85d`  
		Last Modified: Tue, 12 Oct 2021 01:38:40 GMT  
		Size: 30.5 MB (30547197 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:10.11-slim` - linux; s390x

```console
$ docker pull debian@sha256:007b10cc082c948b5a45f7298e8776c5b0df77331ab3e2bf00b7ed98d85f9d8e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **25.8 MB (25754252 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4340acf96cb3e2fec8d744a74977a94566b488869b7b467c9c1571a45cc40b95`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:42:56 GMT
ADD file:39da9acd4d06d69f3ca8d25ef5c097ae741972d6b15a6a057bc7487380157b61 in / 
# Tue, 12 Oct 2021 00:42:57 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:78a640c6bb4733e5156ce97c5ec773cf97b357c3a07244348384da5060788a61`  
		Last Modified: Tue, 12 Oct 2021 00:48:41 GMT  
		Size: 25.8 MB (25754252 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:11`

```console
$ docker pull debian@sha256:4d6ab716de467aad58e91b1b720f0badd7478847ec7a18f66027d0f8a329a43c
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:11` - linux; amd64

```console
$ docker pull debian@sha256:826d8850098b954c3d67c2e45ea7a08fa681c128126ba01b35bd2689b1bb2e04
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.9 MB (54917520 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f776cfb21b5e06bb5b4883eb15c09ab928a411476b8275293c7f96d09b90f7f9`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:20:30 GMT
ADD file:aea313ae50ce6474a3df142b34d4dcba4e7e0186ea6fe55389cb2ea903b9ebbb in / 
# Tue, 12 Oct 2021 01:20:30 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:bb7d5a84853b217ac05783963f12b034243070c1c9c8d2e60ada47444f3cce04`  
		Last Modified: Tue, 12 Oct 2021 01:25:37 GMT  
		Size: 54.9 MB (54917520 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11` - linux; arm variant v5

```console
$ docker pull debian@sha256:4b164f1a9efee849da43191cf8316ac35e7ea1bc9c1b9c73f6c4dbcfa1296c8b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **52.5 MB (52452198 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ba60f6fec7f0faafeb0679c8e0a1df7c55eda14bed43d9e5f561d1fa50a0db76`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:49:54 GMT
ADD file:b9969604315238de271e1769a4b8dd85cdf0d33f9fac387e940d5195ac3030a1 in / 
# Tue, 12 Oct 2021 00:49:55 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:a97c782f2538791984ce12c859cada96e395e3950d4cb7d1238e08acd2eb74ec`  
		Last Modified: Tue, 12 Oct 2021 01:05:12 GMT  
		Size: 52.5 MB (52452198 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11` - linux; arm variant v7

```console
$ docker pull debian@sha256:580d9d9b46eb13e047b78c71a43d9f61521995def78d31586f6ab770c34d47cd
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.1 MB (50118673 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5aeb2ea51c3d45f85b7b225916cea627d677cad4371093aaea94f9a251e6cafb`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:27:59 GMT
ADD file:d9f40ca54fb79741d1e76c7bca9250f78453d0b39fb6c921337f3071a79a55a9 in / 
# Tue, 12 Oct 2021 01:28:00 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:88ce59c3081c4041f41eb407032db40c207cc2d806ba0eb12e170b697db91e52`  
		Last Modified: Tue, 12 Oct 2021 01:43:42 GMT  
		Size: 50.1 MB (50118673 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:21e79e173393a5c03e2dd910354affc1e6ea995aa3c88a18d40bae41760b3d4e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.6 MB (53603015 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:bf33d9a9dfb709a9a24218f6ea55bab3965d0029cdc2ce01214ad1a87040e530`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:04 GMT
ADD file:1529ae12e334fd992892d3fb97c103297cff7e0115b0475bec4c093939a2bff7 in / 
# Tue, 12 Oct 2021 01:41:04 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:1c47a423366578e5ce665d03788914bf0459485a627a27896fa9c5663ce55cdf`  
		Last Modified: Tue, 12 Oct 2021 01:47:41 GMT  
		Size: 53.6 MB (53603015 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11` - linux; 386

```console
$ docker pull debian@sha256:7448716003c940a5adbf4ddedf0486ecdfeb18fbcd8f13c700886ba0d9b82318
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.9 MB (55923419 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b76357513741b0e14fe9ca5c1aa46fe6f4c5e853ff0b0fc03ac27e798ca40c59`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:39:30 GMT
ADD file:e007d6eff02dbc696e1166db315b97a77ae8aa65f2e66ca8074765dde9c70a59 in / 
# Tue, 12 Oct 2021 01:39:31 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6911c8f66691266d86e2cafa67ca005d919f357436fac971c694d37625b5ba90`  
		Last Modified: Tue, 12 Oct 2021 01:47:06 GMT  
		Size: 55.9 MB (55923419 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11` - linux; mips64le

```console
$ docker pull debian@sha256:9d1807f24103e235abbc8df2d946332daad7f82f1a650b5abe0ffcd4af864aec
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53169836 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:18cdbedcbd6db4f83257f4056d6486eb20919f55f29ceb40d247c93b050903ec`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:10:49 GMT
ADD file:856952232716a2e2d5069387a06a537937a1dec1bb75bc9519c60d6ad226bddb in / 
# Tue, 12 Oct 2021 01:10:50 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:3d2cd78b9c44ce4eadbb0cc35b884bfd0dac51123f3c8c6645cb8dc9dc9c1512`  
		Last Modified: Tue, 12 Oct 2021 01:19:18 GMT  
		Size: 53.2 MB (53169836 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11` - linux; ppc64le

```console
$ docker pull debian@sha256:6e37f5053e41a23ff8c716c87bd1ba185e732979f0f4f26581842bafe5ae2790
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **58.8 MB (58808876 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ec0ee09301b7955b4289ddd8912742ea5c8473147ff2f55955cade3d9962c343`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:25:04 GMT
ADD file:127b21494e1c790bdbb4c277638b3e16b25fbbcd1e0cdb52e60a4f55a57cd0f2 in / 
# Tue, 12 Oct 2021 01:25:10 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c7e7ddcf3b3abfed2b7aab50c1b68bfdfc6fcffed2f4b93ebff4d6050b3d5e72`  
		Last Modified: Tue, 12 Oct 2021 01:36:12 GMT  
		Size: 58.8 MB (58808876 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11` - linux; s390x

```console
$ docker pull debian@sha256:853435582c5e3dd9695a0748116a6dc3bb09a6d8ead73bfc70d80a8aacc686f5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53192895 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:0ab94af22e7333d274f0aae62c0d3bcd833fa3f70ed1618bdb62eca5b57e7495`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:42:09 GMT
ADD file:a7a74f6be757ceb5e84c440e739019782df1a118264eef70aa49886a976c43f6 in / 
# Tue, 12 Oct 2021 00:42:12 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:081299fd984cd296112766e1cc55f44fffbf898b2900b01c0333962494a2dd80`  
		Last Modified: Tue, 12 Oct 2021 00:47:43 GMT  
		Size: 53.2 MB (53192895 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:11-slim`

```console
$ docker pull debian@sha256:dddc0f5f01db7ca3599fd8cf9821ffc4d09ec9d7d15e49019e73228ac1eee7f9
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:11-slim` - linux; amd64

```console
$ docker pull debian@sha256:312218c8dae688bae4e9d12926704fa9af6f7307a6edb4f66e479702a9af5a0c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **31.4 MB (31357311 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:dd984c2cf05c58c61026b0bd2298b30aa87bca6f234db507396371137c891a6c`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:20:42 GMT
ADD file:16dc2c6d1932194edec28d730b004fd6deca3d0f0e1a07bc5b8b6e8a1662f7af in / 
# Tue, 12 Oct 2021 01:20:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:7d63c13d9b9b6ec5f05a2b07daadacaa9c610d01102a662ae9b1d082105f1ffa`  
		Last Modified: Tue, 12 Oct 2021 01:26:05 GMT  
		Size: 31.4 MB (31357311 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:6b8e1746e462cd13b1d954f55a6275a7eeed331a633cf0d62b17e5c6fe4983a2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **28.9 MB (28899715 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:83ed6052a5a4c461efabe92d02b3067de1d84e9f619dbb0f973eaf1bcc7872ac`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:50:30 GMT
ADD file:fb1afabdc1f93b4866b4c4d696f272b4888519cb5bf35eb7ed1a4c021d1a04c8 in / 
# Tue, 12 Oct 2021 00:50:30 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:5c9bab66abc5c53391435f6fd66e0ff3571295d568f45ac7d2dc480bcbfd56e8`  
		Last Modified: Tue, 12 Oct 2021 01:06:07 GMT  
		Size: 28.9 MB (28899715 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:c61311dc78459c6c94b87c3ae3ad070449e532d276459d395ee01645c2208676
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **26.6 MB (26561058 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b45f159c2abea86ab575dff875e962b1b64cabd5787d919253aab1db083a4d91`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:28:36 GMT
ADD file:89eac94007ac04f9168737686fa0a6c737f2c28fc9e5918d4d512924fe1973be in / 
# Tue, 12 Oct 2021 01:28:37 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:4e5300249f84466df4dc73ea0ce09938ca00c1718bb12619c4f26bd936162331`  
		Last Modified: Tue, 12 Oct 2021 01:44:28 GMT  
		Size: 26.6 MB (26561058 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:6b5a9ba2bf7d60c09ddee95638a8421a4ad182706052882f24f7069542891b88
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.0 MB (30043906 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:2b66020322207f385b8d57efb4be7222fa200473d803a7bcabf7c11c03f95839`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:18 GMT
ADD file:d84144ad575672cd6cc8a00c8e5a88ab0545348f040fc249ea5fcf8193b2bce6 in / 
# Tue, 12 Oct 2021 01:41:18 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:a9eb63951c1c2ee8efcc12b696928fee3741a2d4eae76f2da3e161a5d90548bf`  
		Last Modified: Tue, 12 Oct 2021 01:48:13 GMT  
		Size: 30.0 MB (30043906 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11-slim` - linux; 386

```console
$ docker pull debian@sha256:5e70f265871e466baa10bf4ab07c62ca56b88d72d8504feacb466b2367e4a22a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **32.4 MB (32370344 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6f31b9786954c6a7ca8b1ed9c5da3397cef6ecea93626779c64675e2052b1d3f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:39:47 GMT
ADD file:42196ffa4c70af8d9f1e7b74edb3bb92d47296acf989adf615e8208230f8bd0c in / 
# Tue, 12 Oct 2021 01:39:47 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:87318d165b5c0fdf05c8ccf97d83084f56b4608075a3335b1a46c76295b82753`  
		Last Modified: Tue, 12 Oct 2021 01:47:39 GMT  
		Size: 32.4 MB (32370344 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11-slim` - linux; mips64le

```console
$ docker pull debian@sha256:1fb2dc326cae06c54451f67ade73c85a23fe49a84de793146069a58c374cf833
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.6 MB (29618732 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:91f0d7e87aa154d1593aad58bebf939b9bb22590172455e65978a9b81bb858ee`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:11:15 GMT
ADD file:f90ca8957172106f42a9096c9a21082d51f3201aa78e6f64621a73117e2b7b6a in / 
# Tue, 12 Oct 2021 01:11:16 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:59ddca50ce05605e6234c1e7213c39cdedabc48ef11637e12156b7bd69afe161`  
		Last Modified: Tue, 12 Oct 2021 01:20:03 GMT  
		Size: 29.6 MB (29618732 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11-slim` - linux; ppc64le

```console
$ docker pull debian@sha256:222db950b210833c502a932f306b728f6fd711035b2470facf689ed4286df69c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **35.3 MB (35258729 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8452d145c52c5b9d0ebed595d8909727e790af2b9b4a2d31c52918018c608fde`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:25:45 GMT
ADD file:d8794fadf16c9948c3574ba28740d08393eca57766340333ad91f9c2a33eb719 in / 
# Tue, 12 Oct 2021 01:25:53 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6c28c1405c677dd809094d06190225b38cee3d24f18da38d287ef5bcc67884d6`  
		Last Modified: Tue, 12 Oct 2021 01:37:00 GMT  
		Size: 35.3 MB (35258729 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11-slim` - linux; s390x

```console
$ docker pull debian@sha256:6b06aa7d0d589aaeb2d76e1488bd1d1984a2f8524eef19e66592b80a66308489
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.6 MB (29641215 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:077e43ea2b6a64971755f52a382c17917c68ab24bb5eb5ee848be6ba18f4f675`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:42:27 GMT
ADD file:6038dd6db57fb05c3d39c02c3379667ccd2989e7667ff773a8020fe6a69a760c in / 
# Tue, 12 Oct 2021 00:42:29 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:ded751c48f72503973be01be2794cc039490f22b039b8ac106e9f17de4980742`  
		Last Modified: Tue, 12 Oct 2021 00:48:05 GMT  
		Size: 29.6 MB (29641215 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:11.1`

```console
$ docker pull debian@sha256:4d6ab716de467aad58e91b1b720f0badd7478847ec7a18f66027d0f8a329a43c
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:11.1` - linux; amd64

```console
$ docker pull debian@sha256:826d8850098b954c3d67c2e45ea7a08fa681c128126ba01b35bd2689b1bb2e04
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.9 MB (54917520 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f776cfb21b5e06bb5b4883eb15c09ab928a411476b8275293c7f96d09b90f7f9`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:20:30 GMT
ADD file:aea313ae50ce6474a3df142b34d4dcba4e7e0186ea6fe55389cb2ea903b9ebbb in / 
# Tue, 12 Oct 2021 01:20:30 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:bb7d5a84853b217ac05783963f12b034243070c1c9c8d2e60ada47444f3cce04`  
		Last Modified: Tue, 12 Oct 2021 01:25:37 GMT  
		Size: 54.9 MB (54917520 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11.1` - linux; arm variant v5

```console
$ docker pull debian@sha256:4b164f1a9efee849da43191cf8316ac35e7ea1bc9c1b9c73f6c4dbcfa1296c8b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **52.5 MB (52452198 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ba60f6fec7f0faafeb0679c8e0a1df7c55eda14bed43d9e5f561d1fa50a0db76`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:49:54 GMT
ADD file:b9969604315238de271e1769a4b8dd85cdf0d33f9fac387e940d5195ac3030a1 in / 
# Tue, 12 Oct 2021 00:49:55 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:a97c782f2538791984ce12c859cada96e395e3950d4cb7d1238e08acd2eb74ec`  
		Last Modified: Tue, 12 Oct 2021 01:05:12 GMT  
		Size: 52.5 MB (52452198 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11.1` - linux; arm variant v7

```console
$ docker pull debian@sha256:580d9d9b46eb13e047b78c71a43d9f61521995def78d31586f6ab770c34d47cd
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.1 MB (50118673 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5aeb2ea51c3d45f85b7b225916cea627d677cad4371093aaea94f9a251e6cafb`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:27:59 GMT
ADD file:d9f40ca54fb79741d1e76c7bca9250f78453d0b39fb6c921337f3071a79a55a9 in / 
# Tue, 12 Oct 2021 01:28:00 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:88ce59c3081c4041f41eb407032db40c207cc2d806ba0eb12e170b697db91e52`  
		Last Modified: Tue, 12 Oct 2021 01:43:42 GMT  
		Size: 50.1 MB (50118673 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11.1` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:21e79e173393a5c03e2dd910354affc1e6ea995aa3c88a18d40bae41760b3d4e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.6 MB (53603015 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:bf33d9a9dfb709a9a24218f6ea55bab3965d0029cdc2ce01214ad1a87040e530`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:04 GMT
ADD file:1529ae12e334fd992892d3fb97c103297cff7e0115b0475bec4c093939a2bff7 in / 
# Tue, 12 Oct 2021 01:41:04 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:1c47a423366578e5ce665d03788914bf0459485a627a27896fa9c5663ce55cdf`  
		Last Modified: Tue, 12 Oct 2021 01:47:41 GMT  
		Size: 53.6 MB (53603015 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11.1` - linux; 386

```console
$ docker pull debian@sha256:7448716003c940a5adbf4ddedf0486ecdfeb18fbcd8f13c700886ba0d9b82318
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.9 MB (55923419 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b76357513741b0e14fe9ca5c1aa46fe6f4c5e853ff0b0fc03ac27e798ca40c59`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:39:30 GMT
ADD file:e007d6eff02dbc696e1166db315b97a77ae8aa65f2e66ca8074765dde9c70a59 in / 
# Tue, 12 Oct 2021 01:39:31 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6911c8f66691266d86e2cafa67ca005d919f357436fac971c694d37625b5ba90`  
		Last Modified: Tue, 12 Oct 2021 01:47:06 GMT  
		Size: 55.9 MB (55923419 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11.1` - linux; mips64le

```console
$ docker pull debian@sha256:9d1807f24103e235abbc8df2d946332daad7f82f1a650b5abe0ffcd4af864aec
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53169836 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:18cdbedcbd6db4f83257f4056d6486eb20919f55f29ceb40d247c93b050903ec`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:10:49 GMT
ADD file:856952232716a2e2d5069387a06a537937a1dec1bb75bc9519c60d6ad226bddb in / 
# Tue, 12 Oct 2021 01:10:50 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:3d2cd78b9c44ce4eadbb0cc35b884bfd0dac51123f3c8c6645cb8dc9dc9c1512`  
		Last Modified: Tue, 12 Oct 2021 01:19:18 GMT  
		Size: 53.2 MB (53169836 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11.1` - linux; ppc64le

```console
$ docker pull debian@sha256:6e37f5053e41a23ff8c716c87bd1ba185e732979f0f4f26581842bafe5ae2790
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **58.8 MB (58808876 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ec0ee09301b7955b4289ddd8912742ea5c8473147ff2f55955cade3d9962c343`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:25:04 GMT
ADD file:127b21494e1c790bdbb4c277638b3e16b25fbbcd1e0cdb52e60a4f55a57cd0f2 in / 
# Tue, 12 Oct 2021 01:25:10 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c7e7ddcf3b3abfed2b7aab50c1b68bfdfc6fcffed2f4b93ebff4d6050b3d5e72`  
		Last Modified: Tue, 12 Oct 2021 01:36:12 GMT  
		Size: 58.8 MB (58808876 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11.1` - linux; s390x

```console
$ docker pull debian@sha256:853435582c5e3dd9695a0748116a6dc3bb09a6d8ead73bfc70d80a8aacc686f5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53192895 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:0ab94af22e7333d274f0aae62c0d3bcd833fa3f70ed1618bdb62eca5b57e7495`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:42:09 GMT
ADD file:a7a74f6be757ceb5e84c440e739019782df1a118264eef70aa49886a976c43f6 in / 
# Tue, 12 Oct 2021 00:42:12 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:081299fd984cd296112766e1cc55f44fffbf898b2900b01c0333962494a2dd80`  
		Last Modified: Tue, 12 Oct 2021 00:47:43 GMT  
		Size: 53.2 MB (53192895 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:11.1-slim`

```console
$ docker pull debian@sha256:dddc0f5f01db7ca3599fd8cf9821ffc4d09ec9d7d15e49019e73228ac1eee7f9
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:11.1-slim` - linux; amd64

```console
$ docker pull debian@sha256:312218c8dae688bae4e9d12926704fa9af6f7307a6edb4f66e479702a9af5a0c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **31.4 MB (31357311 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:dd984c2cf05c58c61026b0bd2298b30aa87bca6f234db507396371137c891a6c`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:20:42 GMT
ADD file:16dc2c6d1932194edec28d730b004fd6deca3d0f0e1a07bc5b8b6e8a1662f7af in / 
# Tue, 12 Oct 2021 01:20:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:7d63c13d9b9b6ec5f05a2b07daadacaa9c610d01102a662ae9b1d082105f1ffa`  
		Last Modified: Tue, 12 Oct 2021 01:26:05 GMT  
		Size: 31.4 MB (31357311 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11.1-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:6b8e1746e462cd13b1d954f55a6275a7eeed331a633cf0d62b17e5c6fe4983a2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **28.9 MB (28899715 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:83ed6052a5a4c461efabe92d02b3067de1d84e9f619dbb0f973eaf1bcc7872ac`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:50:30 GMT
ADD file:fb1afabdc1f93b4866b4c4d696f272b4888519cb5bf35eb7ed1a4c021d1a04c8 in / 
# Tue, 12 Oct 2021 00:50:30 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:5c9bab66abc5c53391435f6fd66e0ff3571295d568f45ac7d2dc480bcbfd56e8`  
		Last Modified: Tue, 12 Oct 2021 01:06:07 GMT  
		Size: 28.9 MB (28899715 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11.1-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:c61311dc78459c6c94b87c3ae3ad070449e532d276459d395ee01645c2208676
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **26.6 MB (26561058 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b45f159c2abea86ab575dff875e962b1b64cabd5787d919253aab1db083a4d91`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:28:36 GMT
ADD file:89eac94007ac04f9168737686fa0a6c737f2c28fc9e5918d4d512924fe1973be in / 
# Tue, 12 Oct 2021 01:28:37 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:4e5300249f84466df4dc73ea0ce09938ca00c1718bb12619c4f26bd936162331`  
		Last Modified: Tue, 12 Oct 2021 01:44:28 GMT  
		Size: 26.6 MB (26561058 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11.1-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:6b5a9ba2bf7d60c09ddee95638a8421a4ad182706052882f24f7069542891b88
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.0 MB (30043906 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:2b66020322207f385b8d57efb4be7222fa200473d803a7bcabf7c11c03f95839`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:18 GMT
ADD file:d84144ad575672cd6cc8a00c8e5a88ab0545348f040fc249ea5fcf8193b2bce6 in / 
# Tue, 12 Oct 2021 01:41:18 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:a9eb63951c1c2ee8efcc12b696928fee3741a2d4eae76f2da3e161a5d90548bf`  
		Last Modified: Tue, 12 Oct 2021 01:48:13 GMT  
		Size: 30.0 MB (30043906 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11.1-slim` - linux; 386

```console
$ docker pull debian@sha256:5e70f265871e466baa10bf4ab07c62ca56b88d72d8504feacb466b2367e4a22a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **32.4 MB (32370344 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6f31b9786954c6a7ca8b1ed9c5da3397cef6ecea93626779c64675e2052b1d3f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:39:47 GMT
ADD file:42196ffa4c70af8d9f1e7b74edb3bb92d47296acf989adf615e8208230f8bd0c in / 
# Tue, 12 Oct 2021 01:39:47 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:87318d165b5c0fdf05c8ccf97d83084f56b4608075a3335b1a46c76295b82753`  
		Last Modified: Tue, 12 Oct 2021 01:47:39 GMT  
		Size: 32.4 MB (32370344 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11.1-slim` - linux; mips64le

```console
$ docker pull debian@sha256:1fb2dc326cae06c54451f67ade73c85a23fe49a84de793146069a58c374cf833
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.6 MB (29618732 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:91f0d7e87aa154d1593aad58bebf939b9bb22590172455e65978a9b81bb858ee`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:11:15 GMT
ADD file:f90ca8957172106f42a9096c9a21082d51f3201aa78e6f64621a73117e2b7b6a in / 
# Tue, 12 Oct 2021 01:11:16 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:59ddca50ce05605e6234c1e7213c39cdedabc48ef11637e12156b7bd69afe161`  
		Last Modified: Tue, 12 Oct 2021 01:20:03 GMT  
		Size: 29.6 MB (29618732 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11.1-slim` - linux; ppc64le

```console
$ docker pull debian@sha256:222db950b210833c502a932f306b728f6fd711035b2470facf689ed4286df69c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **35.3 MB (35258729 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8452d145c52c5b9d0ebed595d8909727e790af2b9b4a2d31c52918018c608fde`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:25:45 GMT
ADD file:d8794fadf16c9948c3574ba28740d08393eca57766340333ad91f9c2a33eb719 in / 
# Tue, 12 Oct 2021 01:25:53 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6c28c1405c677dd809094d06190225b38cee3d24f18da38d287ef5bcc67884d6`  
		Last Modified: Tue, 12 Oct 2021 01:37:00 GMT  
		Size: 35.3 MB (35258729 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:11.1-slim` - linux; s390x

```console
$ docker pull debian@sha256:6b06aa7d0d589aaeb2d76e1488bd1d1984a2f8524eef19e66592b80a66308489
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.6 MB (29641215 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:077e43ea2b6a64971755f52a382c17917c68ab24bb5eb5ee848be6ba18f4f675`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:42:27 GMT
ADD file:6038dd6db57fb05c3d39c02c3379667ccd2989e7667ff773a8020fe6a69a760c in / 
# Tue, 12 Oct 2021 00:42:29 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:ded751c48f72503973be01be2794cc039490f22b039b8ac106e9f17de4980742`  
		Last Modified: Tue, 12 Oct 2021 00:48:05 GMT  
		Size: 29.6 MB (29641215 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:9`

```console
$ docker pull debian@sha256:86269e614274db90a1d71dac258c74ed0a867a1d05f67dea6263b0f216ec7724
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386

### `debian:9` - linux; amd64

```console
$ docker pull debian@sha256:856ba93f897fb7d86ce4363461fe04b6997ce64a79e8198747e472464d58e0fa
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **45.4 MB (45379651 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5ddf6ebdcdb43d0b4b87d5c50996a322dbfd87aa2482febce521079422ba8b1f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:22:41 GMT
ADD file:084c8b3d38d578aa3910f3786b67b058962dbfdfd4a49d6e0201f2e91670873b in / 
# Tue, 12 Oct 2021 01:22:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2f0ef4316716c8b8925ba3665932f8bf2cef3b072d82de1aeb269d6b8b61b84c`  
		Last Modified: Tue, 12 Oct 2021 01:29:27 GMT  
		Size: 45.4 MB (45379651 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:9` - linux; arm variant v5

```console
$ docker pull debian@sha256:80a932816893c940a3478402b574173be5fb7b1be76f35515fe8e1de61341f01
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **44.1 MB (44091932 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:634bcfa0a09aaabd232b426140bfbb01e8cd4fd43562b9698fbb7040133643fd`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:55:37 GMT
ADD file:eb65cc31f82e76c4ea5a8f21e4cad1372399517ee41a759740ff91d1d23b9e44 in / 
# Tue, 12 Oct 2021 00:55:38 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:d2f67fd5897d122073fc9ad347c1fb9faec31555c47b3ee6ff64ca2b553034ed`  
		Last Modified: Tue, 12 Oct 2021 01:13:31 GMT  
		Size: 44.1 MB (44091932 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:9` - linux; arm variant v7

```console
$ docker pull debian@sha256:9434c91665ec2a24d73df1cea6956045563ee0d22fbf34946b8f1f3d69b865b8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **42.1 MB (42119423 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3c34ebc14ce655c7307d49d1b2be353ab021b23689a18bd48eb0e75b48b8b092`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:34:07 GMT
ADD file:eb5955ccd54a486767e85aeb1c03ffc6d8667b5476d0cf17b892ca407f1c8853 in / 
# Tue, 12 Oct 2021 01:34:08 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:8e9db3616ec4491d75e6c28254ff2376f24900797e7c4cc464f36a0c502b111f`  
		Last Modified: Tue, 12 Oct 2021 01:51:20 GMT  
		Size: 42.1 MB (42119423 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:9` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:fcfa90396a4e6d64fb2a721beefc43faa2e0b706b223581f6a1da33bc4d5cb76
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **43.2 MB (43176697 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:57945afea1d021f3fb89b42983c1ba68716bfc327205f96310c7088b02a848f3`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:16 GMT
ADD file:33c2886dff8a69edd550eb69499bf01658d58e2eff405d4b5d4cd5d5be535faa in / 
# Tue, 12 Oct 2021 01:43:16 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:cd2a530f50c2591d61de58d0e9cf724f6c5a2e1112456f38562e289732464c0c`  
		Last Modified: Tue, 12 Oct 2021 01:52:05 GMT  
		Size: 43.2 MB (43176697 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:9` - linux; 386

```console
$ docker pull debian@sha256:8a71adf557086b1f0379142a24cbea502d9bc864b890f48819992b009118c481
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **46.1 MB (46097164 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:596fe040a7233ce2db4046c7bd5ebf3d528c3b7891acb28ee1a7720e7337e5e5`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:19 GMT
ADD file:5a6f08cc731f24357c3702231256fb05eb0a97733779b2ace406ba3c9bf64baa in / 
# Tue, 12 Oct 2021 01:42:19 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:d2ef4e90cca7406b4907a34e3571153fc56e3ecc9b2b5fdbb7787f22877cfec5`  
		Last Modified: Tue, 12 Oct 2021 01:51:58 GMT  
		Size: 46.1 MB (46097164 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:9-slim`

```console
$ docker pull debian@sha256:5bb84147ba0e80fe348af4bc2368148de74721444a2109a8625c98ea04f709f2
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386

### `debian:9-slim` - linux; amd64

```console
$ docker pull debian@sha256:51b5ea69ad41c56e2a79c874a7be9cfec4dfd3a3da3d30036625778f3074f8e8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **22.5 MB (22527572 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:22816b63493638811cfd75f71aa60e8801bbf8461e9c8c1b4974cb0487c9eaec`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:22:52 GMT
ADD file:1220579e31585dec45ca8e35874eb689018ed026a1f23b7906f791c0279671e0 in / 
# Tue, 12 Oct 2021 01:22:53 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:eec53b8a5053c739b5b685cb372b38eea3286ab6626532bad963291f76357c5f`  
		Last Modified: Tue, 12 Oct 2021 01:29:50 GMT  
		Size: 22.5 MB (22527572 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:9-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:a9ecf4dbe81b23312dfcf0e748b5cd18f248d83ea8ce39930ef8c63182f03a70
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **21.2 MB (21204312 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ee231f2410bd399183d952d81cf4a7e4244adb8dbdfecc61796f1ca35e186e4b`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:56:09 GMT
ADD file:7d9fa668f5eefa7aa63b53766fa8274d9a23658bf6e4e07fbbbad36c489ad80b in / 
# Tue, 12 Oct 2021 00:56:10 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:79e3fe0e4dd5ba1fd349b975b6ae002bcf61c48327bc735bbaeb2964c857701b`  
		Last Modified: Tue, 12 Oct 2021 01:14:12 GMT  
		Size: 21.2 MB (21204312 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:9-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:1963485172787c41bd94382890c4bfbd2d5f154ec526fe895edf392e26f7c2a8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **19.3 MB (19316474 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d3cc824b240e1bf2567d85b0d69a734e5e7242c8c1e35ae7b16b911c53cdd3d2`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:34:42 GMT
ADD file:9bfcfd0aaac802b902b0e842e040a6599c461c90b73579bcacc2fbdda7ec39cb in / 
# Tue, 12 Oct 2021 01:34:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:a1a3620b17011bd36d6f64dfcc8fd7c4cb3da78d19a59efb1b35afcadaf3f6a8`  
		Last Modified: Tue, 12 Oct 2021 01:51:59 GMT  
		Size: 19.3 MB (19316474 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:9-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:fce43bd1a1fb7829efe2424037e93bf46a4b8551d0101998f1461d6a44065ec4
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **20.4 MB (20389450 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:94481b52b5464aa62213c5766e1c738caa9d5fedd95c9da1047847cb9284820b`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:29 GMT
ADD file:ebfc214a3da7b706f0759fd22fa991c905976d2c970b2d59d134753f7cbd5e06 in / 
# Tue, 12 Oct 2021 01:43:29 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:0b0a22641ad2aad782c696887c59bf05ec0b1e7aa1e07902ee51949bab802657`  
		Last Modified: Tue, 12 Oct 2021 01:52:33 GMT  
		Size: 20.4 MB (20389450 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:9-slim` - linux; 386

```console
$ docker pull debian@sha256:3e6f082b2511516bb3f7f216d9611b837e1ce9891ef145c85cd5570b60170ed6
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **23.2 MB (23156692 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5b1775b4f91bd2929fc4e6ab2bde382fa03e1449eb5e29e0e3a032e677a7a882`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:34 GMT
ADD file:4420d4cd2022d6bad7f61fbfaf16f0759ca6c30424974c6076dc7d5c09910d66 in / 
# Tue, 12 Oct 2021 01:42:34 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:3d6d74524a87c2d3253aea0ad3bc52520036015f508d5b113e4c8c6627e92bc2`  
		Last Modified: Tue, 12 Oct 2021 01:52:28 GMT  
		Size: 23.2 MB (23156692 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:9.13`

```console
$ docker pull debian@sha256:86269e614274db90a1d71dac258c74ed0a867a1d05f67dea6263b0f216ec7724
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386

### `debian:9.13` - linux; amd64

```console
$ docker pull debian@sha256:856ba93f897fb7d86ce4363461fe04b6997ce64a79e8198747e472464d58e0fa
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **45.4 MB (45379651 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5ddf6ebdcdb43d0b4b87d5c50996a322dbfd87aa2482febce521079422ba8b1f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:22:41 GMT
ADD file:084c8b3d38d578aa3910f3786b67b058962dbfdfd4a49d6e0201f2e91670873b in / 
# Tue, 12 Oct 2021 01:22:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2f0ef4316716c8b8925ba3665932f8bf2cef3b072d82de1aeb269d6b8b61b84c`  
		Last Modified: Tue, 12 Oct 2021 01:29:27 GMT  
		Size: 45.4 MB (45379651 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:9.13` - linux; arm variant v5

```console
$ docker pull debian@sha256:80a932816893c940a3478402b574173be5fb7b1be76f35515fe8e1de61341f01
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **44.1 MB (44091932 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:634bcfa0a09aaabd232b426140bfbb01e8cd4fd43562b9698fbb7040133643fd`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:55:37 GMT
ADD file:eb65cc31f82e76c4ea5a8f21e4cad1372399517ee41a759740ff91d1d23b9e44 in / 
# Tue, 12 Oct 2021 00:55:38 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:d2f67fd5897d122073fc9ad347c1fb9faec31555c47b3ee6ff64ca2b553034ed`  
		Last Modified: Tue, 12 Oct 2021 01:13:31 GMT  
		Size: 44.1 MB (44091932 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:9.13` - linux; arm variant v7

```console
$ docker pull debian@sha256:9434c91665ec2a24d73df1cea6956045563ee0d22fbf34946b8f1f3d69b865b8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **42.1 MB (42119423 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3c34ebc14ce655c7307d49d1b2be353ab021b23689a18bd48eb0e75b48b8b092`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:34:07 GMT
ADD file:eb5955ccd54a486767e85aeb1c03ffc6d8667b5476d0cf17b892ca407f1c8853 in / 
# Tue, 12 Oct 2021 01:34:08 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:8e9db3616ec4491d75e6c28254ff2376f24900797e7c4cc464f36a0c502b111f`  
		Last Modified: Tue, 12 Oct 2021 01:51:20 GMT  
		Size: 42.1 MB (42119423 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:9.13` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:fcfa90396a4e6d64fb2a721beefc43faa2e0b706b223581f6a1da33bc4d5cb76
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **43.2 MB (43176697 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:57945afea1d021f3fb89b42983c1ba68716bfc327205f96310c7088b02a848f3`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:16 GMT
ADD file:33c2886dff8a69edd550eb69499bf01658d58e2eff405d4b5d4cd5d5be535faa in / 
# Tue, 12 Oct 2021 01:43:16 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:cd2a530f50c2591d61de58d0e9cf724f6c5a2e1112456f38562e289732464c0c`  
		Last Modified: Tue, 12 Oct 2021 01:52:05 GMT  
		Size: 43.2 MB (43176697 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:9.13` - linux; 386

```console
$ docker pull debian@sha256:8a71adf557086b1f0379142a24cbea502d9bc864b890f48819992b009118c481
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **46.1 MB (46097164 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:596fe040a7233ce2db4046c7bd5ebf3d528c3b7891acb28ee1a7720e7337e5e5`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:19 GMT
ADD file:5a6f08cc731f24357c3702231256fb05eb0a97733779b2ace406ba3c9bf64baa in / 
# Tue, 12 Oct 2021 01:42:19 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:d2ef4e90cca7406b4907a34e3571153fc56e3ecc9b2b5fdbb7787f22877cfec5`  
		Last Modified: Tue, 12 Oct 2021 01:51:58 GMT  
		Size: 46.1 MB (46097164 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:9.13-slim`

```console
$ docker pull debian@sha256:5bb84147ba0e80fe348af4bc2368148de74721444a2109a8625c98ea04f709f2
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386

### `debian:9.13-slim` - linux; amd64

```console
$ docker pull debian@sha256:51b5ea69ad41c56e2a79c874a7be9cfec4dfd3a3da3d30036625778f3074f8e8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **22.5 MB (22527572 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:22816b63493638811cfd75f71aa60e8801bbf8461e9c8c1b4974cb0487c9eaec`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:22:52 GMT
ADD file:1220579e31585dec45ca8e35874eb689018ed026a1f23b7906f791c0279671e0 in / 
# Tue, 12 Oct 2021 01:22:53 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:eec53b8a5053c739b5b685cb372b38eea3286ab6626532bad963291f76357c5f`  
		Last Modified: Tue, 12 Oct 2021 01:29:50 GMT  
		Size: 22.5 MB (22527572 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:9.13-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:a9ecf4dbe81b23312dfcf0e748b5cd18f248d83ea8ce39930ef8c63182f03a70
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **21.2 MB (21204312 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ee231f2410bd399183d952d81cf4a7e4244adb8dbdfecc61796f1ca35e186e4b`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:56:09 GMT
ADD file:7d9fa668f5eefa7aa63b53766fa8274d9a23658bf6e4e07fbbbad36c489ad80b in / 
# Tue, 12 Oct 2021 00:56:10 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:79e3fe0e4dd5ba1fd349b975b6ae002bcf61c48327bc735bbaeb2964c857701b`  
		Last Modified: Tue, 12 Oct 2021 01:14:12 GMT  
		Size: 21.2 MB (21204312 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:9.13-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:1963485172787c41bd94382890c4bfbd2d5f154ec526fe895edf392e26f7c2a8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **19.3 MB (19316474 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d3cc824b240e1bf2567d85b0d69a734e5e7242c8c1e35ae7b16b911c53cdd3d2`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:34:42 GMT
ADD file:9bfcfd0aaac802b902b0e842e040a6599c461c90b73579bcacc2fbdda7ec39cb in / 
# Tue, 12 Oct 2021 01:34:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:a1a3620b17011bd36d6f64dfcc8fd7c4cb3da78d19a59efb1b35afcadaf3f6a8`  
		Last Modified: Tue, 12 Oct 2021 01:51:59 GMT  
		Size: 19.3 MB (19316474 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:9.13-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:fce43bd1a1fb7829efe2424037e93bf46a4b8551d0101998f1461d6a44065ec4
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **20.4 MB (20389450 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:94481b52b5464aa62213c5766e1c738caa9d5fedd95c9da1047847cb9284820b`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:29 GMT
ADD file:ebfc214a3da7b706f0759fd22fa991c905976d2c970b2d59d134753f7cbd5e06 in / 
# Tue, 12 Oct 2021 01:43:29 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:0b0a22641ad2aad782c696887c59bf05ec0b1e7aa1e07902ee51949bab802657`  
		Last Modified: Tue, 12 Oct 2021 01:52:33 GMT  
		Size: 20.4 MB (20389450 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:9.13-slim` - linux; 386

```console
$ docker pull debian@sha256:3e6f082b2511516bb3f7f216d9611b837e1ce9891ef145c85cd5570b60170ed6
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **23.2 MB (23156692 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5b1775b4f91bd2929fc4e6ab2bde382fa03e1449eb5e29e0e3a032e677a7a882`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:34 GMT
ADD file:4420d4cd2022d6bad7f61fbfaf16f0759ca6c30424974c6076dc7d5c09910d66 in / 
# Tue, 12 Oct 2021 01:42:34 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:3d6d74524a87c2d3253aea0ad3bc52520036015f508d5b113e4c8c6627e92bc2`  
		Last Modified: Tue, 12 Oct 2021 01:52:28 GMT  
		Size: 23.2 MB (23156692 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:bookworm`

```console
$ docker pull debian@sha256:27b2d97cf281cc6cd20ba5c99961315954cb5e83e03707686584bfeecbff71ad
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:bookworm` - linux; amd64

```console
$ docker pull debian@sha256:db7c986f30367ddc5c3a090c2617ef4da058512141720f71d0dd9866c18dfadc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.4 MB (55446041 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:674f99e735974b7d2ab9ed4a9c5efde15906f413e885a24d5b80dc8f6902787a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:20:05 GMT
ADD file:27dfce899c847022174d5caa95af127a948c63fcd5a23b6cbecc8167f4b2de21 in / 
# Tue, 12 Oct 2021 01:20:06 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:618a27eae568875e1902bc8979ae01edf1e0116fea44ca04d1d7c67d590cd708`  
		Last Modified: Tue, 12 Oct 2021 01:24:59 GMT  
		Size: 55.4 MB (55446041 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm` - linux; arm variant v5

```console
$ docker pull debian@sha256:1e8a7aa2d3bda4300789ee97c93fa766f71efa9ae3ab025260ec659221eac814
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.0 MB (52964940 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ca5160232bb477662a4acf9baf77a51af25617bda8f99566e476f45cea976b5b`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:48:55 GMT
ADD file:ab43782dfcd6f5ad4a656cf43f0385f45ef4a3c26e23153f4898190cf9b704c5 in / 
# Tue, 12 Oct 2021 00:48:56 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:13544a9b0148e44e1e5fa2a9d5e9cd0ecec08898da21d7ebf79b6a6b34d6b67a`  
		Last Modified: Tue, 12 Oct 2021 01:03:53 GMT  
		Size: 53.0 MB (52964940 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm` - linux; arm variant v7

```console
$ docker pull debian@sha256:1612d79bcee557bf56c6cf3be17e6ae1891f9dbaf5637671708c5aa469ff9c20
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.6 MB (50566236 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:c1f65878563c46c4f65696a9602c64d6e707af379d841d8f047ac44ea73c6f10`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:26:56 GMT
ADD file:3b73f2accf5cf51a65b062fd02e1840ba8413191e44707e035ba6819a8eba1be in / 
# Tue, 12 Oct 2021 01:26:57 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:96084cd63f96d5395a465b5034a4392747af562af1269011d400540dc5b85a5d`  
		Last Modified: Tue, 12 Oct 2021 01:42:27 GMT  
		Size: 50.6 MB (50566236 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:9769434fb677d3dbe5f04b473d671b32e724553a7e93fb2eab52e0819baf14a9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.5 MB (54465168 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5897f90de5dff9070f18c81e5e9b6a9a36ae055872803771df6a173bae036f66`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:41 GMT
ADD file:4f13e43ce08281cef17adbdc736c615c592b9a55db9dcea6106e9355805d8eb3 in / 
# Tue, 12 Oct 2021 01:40:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2cbe7f3c2d37f6e753ada76cd01b0ecb8c8dc3a63791174a7bc8f7b2c95aa4eb`  
		Last Modified: Tue, 12 Oct 2021 01:46:59 GMT  
		Size: 54.5 MB (54465168 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm` - linux; 386

```console
$ docker pull debian@sha256:420dac847dd9fc23619b4490b5b3826408001d7a45585ee4612c1018955139fe
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **56.5 MB (56480795 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e39f774888b00cd708625cf03feebeb72322a7c404b565cc68474394ae7ec705`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:39:00 GMT
ADD file:5c67fb048ea543a4bda0c18d5b1f13f9ed438f0c2da79f7710603bb8072f06e5 in / 
# Tue, 12 Oct 2021 01:39:01 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:5b6ea382495037522d99f596d06e6a07243f5c9a81406a71b1867d7b81c2d245`  
		Last Modified: Tue, 12 Oct 2021 01:46:18 GMT  
		Size: 56.5 MB (56480795 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm` - linux; mips64le

```console
$ docker pull debian@sha256:4a0bb29a8e82741de4a47712c9975fbd4364fb1c6eac1fdf333ceccda4f8aaec
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.1 MB (54069055 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4dc26e5f1694179e8b84307074a831a2cb51187f382ad10b88386ca0ab337c62`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:09:56 GMT
ADD file:8f2e72c6d8bc47bf31947b2f916b90c94c62fd9a3fd11deef021971c079ff665 in / 
# Tue, 12 Oct 2021 01:09:57 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:fad81077a6466f0231de8750a5f41773b60638f37eeb5ec052d93dd340d095de`  
		Last Modified: Tue, 12 Oct 2021 01:17:59 GMT  
		Size: 54.1 MB (54069055 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm` - linux; ppc64le

```console
$ docker pull debian@sha256:e1611ee0974084e3addfe03b5a2905cb3964d65156b176be4149be0227a6761e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **59.7 MB (59659978 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b770b8a29c1a84b3e0d7e9dba1c48a06b7f0ae524df43fec3f4844d2ca9a5aaf`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:24:06 GMT
ADD file:2208d7bf60cb752ee46dc1a2c107f0ffefb15b83324ef516b1b93f03c5d0c249 in / 
# Tue, 12 Oct 2021 01:24:11 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:3cb87628128232319961676d93044c80b66d2fba881080d5a34c710d8a09207d`  
		Last Modified: Tue, 12 Oct 2021 01:34:29 GMT  
		Size: 59.7 MB (59659978 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm` - linux; s390x

```console
$ docker pull debian@sha256:862c7efde050551594c757f68b44e46100f62d49b3fbb679d72083f7921794d4
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.7 MB (53700141 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:fff27f662c407237cf9388d0ea2a608eb9558b6fbae278f0d41f4761d9f4f959`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:41:38 GMT
ADD file:c1434e9b848c2f01f3fbd798a3e6ea6f1806b144dbc3f1b0d5bb8e1f51339b7d in / 
# Tue, 12 Oct 2021 00:41:44 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:68fb91bf36b385ffeb27bd029bade1171ff1a60e865f85a09ee539cbdb35b1e7`  
		Last Modified: Tue, 12 Oct 2021 00:47:15 GMT  
		Size: 53.7 MB (53700141 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:bookworm-20211011`

```console
$ docker pull debian@sha256:27b2d97cf281cc6cd20ba5c99961315954cb5e83e03707686584bfeecbff71ad
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:bookworm-20211011` - linux; amd64

```console
$ docker pull debian@sha256:db7c986f30367ddc5c3a090c2617ef4da058512141720f71d0dd9866c18dfadc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.4 MB (55446041 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:674f99e735974b7d2ab9ed4a9c5efde15906f413e885a24d5b80dc8f6902787a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:20:05 GMT
ADD file:27dfce899c847022174d5caa95af127a948c63fcd5a23b6cbecc8167f4b2de21 in / 
# Tue, 12 Oct 2021 01:20:06 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:618a27eae568875e1902bc8979ae01edf1e0116fea44ca04d1d7c67d590cd708`  
		Last Modified: Tue, 12 Oct 2021 01:24:59 GMT  
		Size: 55.4 MB (55446041 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-20211011` - linux; arm variant v5

```console
$ docker pull debian@sha256:1e8a7aa2d3bda4300789ee97c93fa766f71efa9ae3ab025260ec659221eac814
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.0 MB (52964940 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ca5160232bb477662a4acf9baf77a51af25617bda8f99566e476f45cea976b5b`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:48:55 GMT
ADD file:ab43782dfcd6f5ad4a656cf43f0385f45ef4a3c26e23153f4898190cf9b704c5 in / 
# Tue, 12 Oct 2021 00:48:56 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:13544a9b0148e44e1e5fa2a9d5e9cd0ecec08898da21d7ebf79b6a6b34d6b67a`  
		Last Modified: Tue, 12 Oct 2021 01:03:53 GMT  
		Size: 53.0 MB (52964940 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-20211011` - linux; arm variant v7

```console
$ docker pull debian@sha256:1612d79bcee557bf56c6cf3be17e6ae1891f9dbaf5637671708c5aa469ff9c20
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.6 MB (50566236 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:c1f65878563c46c4f65696a9602c64d6e707af379d841d8f047ac44ea73c6f10`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:26:56 GMT
ADD file:3b73f2accf5cf51a65b062fd02e1840ba8413191e44707e035ba6819a8eba1be in / 
# Tue, 12 Oct 2021 01:26:57 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:96084cd63f96d5395a465b5034a4392747af562af1269011d400540dc5b85a5d`  
		Last Modified: Tue, 12 Oct 2021 01:42:27 GMT  
		Size: 50.6 MB (50566236 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-20211011` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:9769434fb677d3dbe5f04b473d671b32e724553a7e93fb2eab52e0819baf14a9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.5 MB (54465168 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5897f90de5dff9070f18c81e5e9b6a9a36ae055872803771df6a173bae036f66`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:41 GMT
ADD file:4f13e43ce08281cef17adbdc736c615c592b9a55db9dcea6106e9355805d8eb3 in / 
# Tue, 12 Oct 2021 01:40:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2cbe7f3c2d37f6e753ada76cd01b0ecb8c8dc3a63791174a7bc8f7b2c95aa4eb`  
		Last Modified: Tue, 12 Oct 2021 01:46:59 GMT  
		Size: 54.5 MB (54465168 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-20211011` - linux; 386

```console
$ docker pull debian@sha256:420dac847dd9fc23619b4490b5b3826408001d7a45585ee4612c1018955139fe
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **56.5 MB (56480795 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e39f774888b00cd708625cf03feebeb72322a7c404b565cc68474394ae7ec705`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:39:00 GMT
ADD file:5c67fb048ea543a4bda0c18d5b1f13f9ed438f0c2da79f7710603bb8072f06e5 in / 
# Tue, 12 Oct 2021 01:39:01 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:5b6ea382495037522d99f596d06e6a07243f5c9a81406a71b1867d7b81c2d245`  
		Last Modified: Tue, 12 Oct 2021 01:46:18 GMT  
		Size: 56.5 MB (56480795 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-20211011` - linux; mips64le

```console
$ docker pull debian@sha256:4a0bb29a8e82741de4a47712c9975fbd4364fb1c6eac1fdf333ceccda4f8aaec
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.1 MB (54069055 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4dc26e5f1694179e8b84307074a831a2cb51187f382ad10b88386ca0ab337c62`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:09:56 GMT
ADD file:8f2e72c6d8bc47bf31947b2f916b90c94c62fd9a3fd11deef021971c079ff665 in / 
# Tue, 12 Oct 2021 01:09:57 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:fad81077a6466f0231de8750a5f41773b60638f37eeb5ec052d93dd340d095de`  
		Last Modified: Tue, 12 Oct 2021 01:17:59 GMT  
		Size: 54.1 MB (54069055 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-20211011` - linux; ppc64le

```console
$ docker pull debian@sha256:e1611ee0974084e3addfe03b5a2905cb3964d65156b176be4149be0227a6761e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **59.7 MB (59659978 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b770b8a29c1a84b3e0d7e9dba1c48a06b7f0ae524df43fec3f4844d2ca9a5aaf`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:24:06 GMT
ADD file:2208d7bf60cb752ee46dc1a2c107f0ffefb15b83324ef516b1b93f03c5d0c249 in / 
# Tue, 12 Oct 2021 01:24:11 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:3cb87628128232319961676d93044c80b66d2fba881080d5a34c710d8a09207d`  
		Last Modified: Tue, 12 Oct 2021 01:34:29 GMT  
		Size: 59.7 MB (59659978 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-20211011` - linux; s390x

```console
$ docker pull debian@sha256:862c7efde050551594c757f68b44e46100f62d49b3fbb679d72083f7921794d4
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.7 MB (53700141 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:fff27f662c407237cf9388d0ea2a608eb9558b6fbae278f0d41f4761d9f4f959`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:41:38 GMT
ADD file:c1434e9b848c2f01f3fbd798a3e6ea6f1806b144dbc3f1b0d5bb8e1f51339b7d in / 
# Tue, 12 Oct 2021 00:41:44 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:68fb91bf36b385ffeb27bd029bade1171ff1a60e865f85a09ee539cbdb35b1e7`  
		Last Modified: Tue, 12 Oct 2021 00:47:15 GMT  
		Size: 53.7 MB (53700141 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:bookworm-20211011-slim`

```console
$ docker pull debian@sha256:ae9bdb5571feb6badec4eaaff9e44e1e6d696ce67ee6a398284f511d859f3730
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:bookworm-20211011-slim` - linux; amd64

```console
$ docker pull debian@sha256:92d67639b39672a47c94e8e586d86116edb8d52870b0a37e896a80f304fc1d1a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **31.7 MB (31692052 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6927ab6f448b8c84f7554aae52d50c9a24cc5fc81292eb2de8a8cf3158479caa`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:20:19 GMT
ADD file:b19679c81e3d98d3cbd0b6ec72d2a362c09962e0ce831a2e38ac17ef87ddabd2 in / 
# Tue, 12 Oct 2021 01:20:19 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:545b369240c1b746a743d71bbe363b94bdb88045ff39f617813addb017a9db38`  
		Last Modified: Tue, 12 Oct 2021 01:25:20 GMT  
		Size: 31.7 MB (31692052 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-20211011-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:4c429af93819092d97ccd126171dcd6cbf797684d421265f86bc977a564bd371
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.2 MB (29211548 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9733ad9c58755ab9df6e8ee94ec2665f029d264fd8015f5b1ac0df91afbbae9f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:49:27 GMT
ADD file:1cc1654614b2d094a458e8a8551dd8176e62f3195bbb89459e45510fc11960de in / 
# Tue, 12 Oct 2021 00:49:28 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:9f46836bdbba00fc5266132714febc7f811c9a8359e7d244cc2c49f6107e8382`  
		Last Modified: Tue, 12 Oct 2021 01:04:31 GMT  
		Size: 29.2 MB (29211548 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-20211011-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:b5304ad3e82f2ab7071253d354e021ccaabe40b759d3038751e529408883b611
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **26.8 MB (26813217 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:86b6db36bd9dfd99fdf162784510d9f86d31a20918c7cef886d3e1f5a36396df`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:27:31 GMT
ADD file:c440250bfbd5321f6c02335a37021debd8b16c1cf17995cc724890c57bfee83e in / 
# Tue, 12 Oct 2021 01:27:31 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:50027ff2d1b31d50bb37b1d5e30f980285612ecaba26d55a99bc567ecfd51f2b`  
		Last Modified: Tue, 12 Oct 2021 01:43:02 GMT  
		Size: 26.8 MB (26813217 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-20211011-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:28f38a1292e397c8f95f93f8fe75e21416dbef17444e9d9ad780f425622d7f48
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.7 MB (30711467 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1a7cb47ea1a5c1bc4199d0fff286400dcdff083f52d892ff58a36c25d6211142`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:54 GMT
ADD file:40ef674c8f82aba05c38db6e5d27ff51c37bb7c7ac1198caa88fc063cb0e60b8 in / 
# Tue, 12 Oct 2021 01:40:54 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:00ac796bd8c532aef4ad77ab8175a39e0bca984f27287b38605b53b3f8190fd2`  
		Last Modified: Tue, 12 Oct 2021 01:47:22 GMT  
		Size: 30.7 MB (30711467 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-20211011-slim` - linux; 386

```console
$ docker pull debian@sha256:de0155830775f43a6753ca34c2ab24293902c52ed3df1966b026ce796daa8f99
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **32.7 MB (32726559 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:96f9f2758d836d3df24920a85b7b3c29fd57a0473eeb9ff38e04923715656ddf`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:39:16 GMT
ADD file:6d87e7e36b81c240e58aa7316772916c99b8423eacee000d2f6956dd9356f0e1 in / 
# Tue, 12 Oct 2021 01:39:17 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c91ec16b79b47c46caca08a0fa27f06df0e4f4c8c5d1e489dff2c0e4af8495e1`  
		Last Modified: Tue, 12 Oct 2021 01:46:43 GMT  
		Size: 32.7 MB (32726559 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-20211011-slim` - linux; mips64le

```console
$ docker pull debian@sha256:fd7fd4a53285c3aade29f5fb63fe4fe34ab8ad924b1328943204634aac690bd2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.3 MB (30313364 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:760bced86d5035543c1b4ec51a6df5e0c3ee4f3a3f23bdff74f70a6c8d19b5ba`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:10:22 GMT
ADD file:87aba135412a9bdf9f8da22d5a6eb90259bd6e54a71e7946443abca33b58867b in / 
# Tue, 12 Oct 2021 01:10:23 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:8e21180f99b22caf985c06275e081e2fcb5cfebaad7874eea3bec5af3f36f639`  
		Last Modified: Tue, 12 Oct 2021 01:18:36 GMT  
		Size: 30.3 MB (30313364 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-20211011-slim` - linux; ppc64le

```console
$ docker pull debian@sha256:e4ccc66ce1c20510bb0f3037745e765507832b91cc77fe8ec675fe4ca47585b8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **35.9 MB (35900678 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3f736f04688ed59050ee8d4848f17d26577a1cfee0d3739ced37f4abbaee3674`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:24:44 GMT
ADD file:5c750a1cf418050da8164b02e79d89893775c8cb82cba89ce6d2f820f4e43139 in / 
# Tue, 12 Oct 2021 01:24:49 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6281a0df95aaa56f35d9c470a0b63d5dcf2bab02587999175c26f131fc898534`  
		Last Modified: Tue, 12 Oct 2021 01:35:18 GMT  
		Size: 35.9 MB (35900678 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-20211011-slim` - linux; s390x

```console
$ docker pull debian@sha256:98a9cc4b4aeb8eb0d8f81f60155e1c2ed1a2c228678d2aca4047082f04b582c9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.9 MB (29947073 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:287ecd72602d334147536fbaa8c09a03503bd0d35ba2812d6e35b7774b7a8411`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:41:58 GMT
ADD file:fa2051ff320c558a9da82f6d05b5bc8144b1bbd925e103978a8493e9341ea7da in / 
# Tue, 12 Oct 2021 00:42:00 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:e44f3cf4e8b7f77baa7d2ac41c1a985b13c519dcfcb0667fb483f72860f50fca`  
		Last Modified: Tue, 12 Oct 2021 00:47:30 GMT  
		Size: 29.9 MB (29947073 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:bookworm-backports`

```console
$ docker pull debian@sha256:654d363d647706e4b6458ec598e100b76db11a1718516d7d4c84a8c2a48c04a2
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:bookworm-backports` - linux; amd64

```console
$ docker pull debian@sha256:150669b7d1b16b2ea6493ccab8a7cc9c38b9cd20fcfc61aa17d44cf7759713da
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.4 MB (55446265 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:eb1c3b8bb822441693d0ed182c36653582fc283cd297cb50af20a89a3ac67c7a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:20:05 GMT
ADD file:27dfce899c847022174d5caa95af127a948c63fcd5a23b6cbecc8167f4b2de21 in / 
# Tue, 12 Oct 2021 01:20:06 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:20:11 GMT
RUN echo 'deb http://deb.debian.org/debian bookworm-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:618a27eae568875e1902bc8979ae01edf1e0116fea44ca04d1d7c67d590cd708`  
		Last Modified: Tue, 12 Oct 2021 01:24:59 GMT  
		Size: 55.4 MB (55446041 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:104421c32b430b080cb319ebabcbaab6718a76638df220148a893aaaad3081d7`  
		Last Modified: Tue, 12 Oct 2021 01:25:09 GMT  
		Size: 224.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-backports` - linux; arm variant v5

```console
$ docker pull debian@sha256:9f6b720212d900beb03731e95d8be8dc3b7d247b060264a08683953118424f8e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.0 MB (52965168 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:af17d65cf2289dd37cb151341f138b4b1242b164e3b51895efe58fb4bfd0fddd`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:48:55 GMT
ADD file:ab43782dfcd6f5ad4a656cf43f0385f45ef4a3c26e23153f4898190cf9b704c5 in / 
# Tue, 12 Oct 2021 00:48:56 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 00:49:09 GMT
RUN echo 'deb http://deb.debian.org/debian bookworm-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:13544a9b0148e44e1e5fa2a9d5e9cd0ecec08898da21d7ebf79b6a6b34d6b67a`  
		Last Modified: Tue, 12 Oct 2021 01:03:53 GMT  
		Size: 53.0 MB (52964940 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:24e813a4832474dca02caadcb80883e590f995e930f1e4ca14ba99912abf6eb8`  
		Last Modified: Tue, 12 Oct 2021 01:04:04 GMT  
		Size: 228.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-backports` - linux; arm variant v7

```console
$ docker pull debian@sha256:c939e45a893dc72f8dfa304ee2259ad9e5c0fc13276f2cc34d1d57412d8ebe5b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.6 MB (50566464 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:c43f75ca1d2bb035b5d05458331ab44d0317e5066725f5bf57ca523cc8d8a272`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:26:56 GMT
ADD file:3b73f2accf5cf51a65b062fd02e1840ba8413191e44707e035ba6819a8eba1be in / 
# Tue, 12 Oct 2021 01:26:57 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:27:10 GMT
RUN echo 'deb http://deb.debian.org/debian bookworm-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:96084cd63f96d5395a465b5034a4392747af562af1269011d400540dc5b85a5d`  
		Last Modified: Tue, 12 Oct 2021 01:42:27 GMT  
		Size: 50.6 MB (50566236 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:8a4bb284d73302ba99570d9ec052fa58a187e4cd12c60bf0a47652931722f064`  
		Last Modified: Tue, 12 Oct 2021 01:42:39 GMT  
		Size: 228.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-backports` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:b0d643ffaa524d7b29fd3279d47c0deba661fe57499d97a2420abc6e055c0352
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.5 MB (54465395 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:720d134fac657c54c534ae83be567c3b4c640fe738ef99ef3cb270313a67f79d`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:41 GMT
ADD file:4f13e43ce08281cef17adbdc736c615c592b9a55db9dcea6106e9355805d8eb3 in / 
# Tue, 12 Oct 2021 01:40:42 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:40:47 GMT
RUN echo 'deb http://deb.debian.org/debian bookworm-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:2cbe7f3c2d37f6e753ada76cd01b0ecb8c8dc3a63791174a7bc8f7b2c95aa4eb`  
		Last Modified: Tue, 12 Oct 2021 01:46:59 GMT  
		Size: 54.5 MB (54465168 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:452ccd75a659ea27051705ee942662b01be42dd3e7df0a92d9dfcc08f1e14b8f`  
		Last Modified: Tue, 12 Oct 2021 01:47:10 GMT  
		Size: 227.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-backports` - linux; 386

```console
$ docker pull debian@sha256:4aa38ab2f152cf70dc581ae6cc6e8977614c8cbe59e4c078ed495e2614a70adc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **56.5 MB (56481022 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:84b112c3a46ed04798db57ddcbe8d16a20e9b19578efe3a2525f27ad1b245d6f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:39:00 GMT
ADD file:5c67fb048ea543a4bda0c18d5b1f13f9ed438f0c2da79f7710603bb8072f06e5 in / 
# Tue, 12 Oct 2021 01:39:01 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:39:07 GMT
RUN echo 'deb http://deb.debian.org/debian bookworm-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:5b6ea382495037522d99f596d06e6a07243f5c9a81406a71b1867d7b81c2d245`  
		Last Modified: Tue, 12 Oct 2021 01:46:18 GMT  
		Size: 56.5 MB (56480795 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:110b92c7383bdb6a8b5ebc6ddd880198e43bab191591320c17634b4ed06faa72`  
		Last Modified: Tue, 12 Oct 2021 01:46:29 GMT  
		Size: 227.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-backports` - linux; mips64le

```console
$ docker pull debian@sha256:4b701ae0899b5d93661e05b3db308e6d2c0af9329f6f4ad6b876d7f6ca143b7e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.1 MB (54069282 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f0b8df1494b6d86f40ac2ac8a29db6c1201232510920fb70d39f851564c48cd5`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:09:56 GMT
ADD file:8f2e72c6d8bc47bf31947b2f916b90c94c62fd9a3fd11deef021971c079ff665 in / 
# Tue, 12 Oct 2021 01:09:57 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:10:03 GMT
RUN echo 'deb http://deb.debian.org/debian bookworm-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:fad81077a6466f0231de8750a5f41773b60638f37eeb5ec052d93dd340d095de`  
		Last Modified: Tue, 12 Oct 2021 01:17:59 GMT  
		Size: 54.1 MB (54069055 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:ed867af3f7ee18bdef2fc2b049767bbb6bba4d8d53f61d12f9a244180c75f7ad`  
		Last Modified: Tue, 12 Oct 2021 01:18:08 GMT  
		Size: 227.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-backports` - linux; ppc64le

```console
$ docker pull debian@sha256:cf22c84ccd8efdf50dd221507871231768c0cd98ff25dde3773f04eb348d0c73
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **59.7 MB (59660206 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:718d2bbff060756f389eb0d469c78b30020375ed53c86d58f76e172f7e9338d1`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:24:06 GMT
ADD file:2208d7bf60cb752ee46dc1a2c107f0ffefb15b83324ef516b1b93f03c5d0c249 in / 
# Tue, 12 Oct 2021 01:24:11 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:24:32 GMT
RUN echo 'deb http://deb.debian.org/debian bookworm-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:3cb87628128232319961676d93044c80b66d2fba881080d5a34c710d8a09207d`  
		Last Modified: Tue, 12 Oct 2021 01:34:29 GMT  
		Size: 59.7 MB (59659978 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:4f8906c83d1be1e812c758a74046b16deb6646ed34f62abdf4756b9cb0017f04`  
		Last Modified: Tue, 12 Oct 2021 01:34:40 GMT  
		Size: 228.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-backports` - linux; s390x

```console
$ docker pull debian@sha256:e830b6567bc5813d206b5f906754ae35ce8f8bcc757c16d56f81542a45b93f10
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.7 MB (53700366 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:781cfc78360a9ae84f859c1c5c41584ad312d1a679601923bdd45740c3327c89`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:41:38 GMT
ADD file:c1434e9b848c2f01f3fbd798a3e6ea6f1806b144dbc3f1b0d5bb8e1f51339b7d in / 
# Tue, 12 Oct 2021 00:41:44 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 00:41:50 GMT
RUN echo 'deb http://deb.debian.org/debian bookworm-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:68fb91bf36b385ffeb27bd029bade1171ff1a60e865f85a09ee539cbdb35b1e7`  
		Last Modified: Tue, 12 Oct 2021 00:47:15 GMT  
		Size: 53.7 MB (53700141 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:8ecd56624e3942a458f6c84f3b49627bb4b95fe8d8421e4e0d83e3b1db44b5d4`  
		Last Modified: Tue, 12 Oct 2021 00:47:22 GMT  
		Size: 225.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:bookworm-slim`

```console
$ docker pull debian@sha256:ae9bdb5571feb6badec4eaaff9e44e1e6d696ce67ee6a398284f511d859f3730
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:bookworm-slim` - linux; amd64

```console
$ docker pull debian@sha256:92d67639b39672a47c94e8e586d86116edb8d52870b0a37e896a80f304fc1d1a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **31.7 MB (31692052 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6927ab6f448b8c84f7554aae52d50c9a24cc5fc81292eb2de8a8cf3158479caa`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:20:19 GMT
ADD file:b19679c81e3d98d3cbd0b6ec72d2a362c09962e0ce831a2e38ac17ef87ddabd2 in / 
# Tue, 12 Oct 2021 01:20:19 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:545b369240c1b746a743d71bbe363b94bdb88045ff39f617813addb017a9db38`  
		Last Modified: Tue, 12 Oct 2021 01:25:20 GMT  
		Size: 31.7 MB (31692052 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:4c429af93819092d97ccd126171dcd6cbf797684d421265f86bc977a564bd371
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.2 MB (29211548 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9733ad9c58755ab9df6e8ee94ec2665f029d264fd8015f5b1ac0df91afbbae9f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:49:27 GMT
ADD file:1cc1654614b2d094a458e8a8551dd8176e62f3195bbb89459e45510fc11960de in / 
# Tue, 12 Oct 2021 00:49:28 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:9f46836bdbba00fc5266132714febc7f811c9a8359e7d244cc2c49f6107e8382`  
		Last Modified: Tue, 12 Oct 2021 01:04:31 GMT  
		Size: 29.2 MB (29211548 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:b5304ad3e82f2ab7071253d354e021ccaabe40b759d3038751e529408883b611
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **26.8 MB (26813217 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:86b6db36bd9dfd99fdf162784510d9f86d31a20918c7cef886d3e1f5a36396df`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:27:31 GMT
ADD file:c440250bfbd5321f6c02335a37021debd8b16c1cf17995cc724890c57bfee83e in / 
# Tue, 12 Oct 2021 01:27:31 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:50027ff2d1b31d50bb37b1d5e30f980285612ecaba26d55a99bc567ecfd51f2b`  
		Last Modified: Tue, 12 Oct 2021 01:43:02 GMT  
		Size: 26.8 MB (26813217 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:28f38a1292e397c8f95f93f8fe75e21416dbef17444e9d9ad780f425622d7f48
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.7 MB (30711467 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1a7cb47ea1a5c1bc4199d0fff286400dcdff083f52d892ff58a36c25d6211142`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:54 GMT
ADD file:40ef674c8f82aba05c38db6e5d27ff51c37bb7c7ac1198caa88fc063cb0e60b8 in / 
# Tue, 12 Oct 2021 01:40:54 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:00ac796bd8c532aef4ad77ab8175a39e0bca984f27287b38605b53b3f8190fd2`  
		Last Modified: Tue, 12 Oct 2021 01:47:22 GMT  
		Size: 30.7 MB (30711467 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-slim` - linux; 386

```console
$ docker pull debian@sha256:de0155830775f43a6753ca34c2ab24293902c52ed3df1966b026ce796daa8f99
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **32.7 MB (32726559 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:96f9f2758d836d3df24920a85b7b3c29fd57a0473eeb9ff38e04923715656ddf`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:39:16 GMT
ADD file:6d87e7e36b81c240e58aa7316772916c99b8423eacee000d2f6956dd9356f0e1 in / 
# Tue, 12 Oct 2021 01:39:17 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c91ec16b79b47c46caca08a0fa27f06df0e4f4c8c5d1e489dff2c0e4af8495e1`  
		Last Modified: Tue, 12 Oct 2021 01:46:43 GMT  
		Size: 32.7 MB (32726559 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-slim` - linux; mips64le

```console
$ docker pull debian@sha256:fd7fd4a53285c3aade29f5fb63fe4fe34ab8ad924b1328943204634aac690bd2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.3 MB (30313364 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:760bced86d5035543c1b4ec51a6df5e0c3ee4f3a3f23bdff74f70a6c8d19b5ba`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:10:22 GMT
ADD file:87aba135412a9bdf9f8da22d5a6eb90259bd6e54a71e7946443abca33b58867b in / 
# Tue, 12 Oct 2021 01:10:23 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:8e21180f99b22caf985c06275e081e2fcb5cfebaad7874eea3bec5af3f36f639`  
		Last Modified: Tue, 12 Oct 2021 01:18:36 GMT  
		Size: 30.3 MB (30313364 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-slim` - linux; ppc64le

```console
$ docker pull debian@sha256:e4ccc66ce1c20510bb0f3037745e765507832b91cc77fe8ec675fe4ca47585b8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **35.9 MB (35900678 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3f736f04688ed59050ee8d4848f17d26577a1cfee0d3739ced37f4abbaee3674`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:24:44 GMT
ADD file:5c750a1cf418050da8164b02e79d89893775c8cb82cba89ce6d2f820f4e43139 in / 
# Tue, 12 Oct 2021 01:24:49 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6281a0df95aaa56f35d9c470a0b63d5dcf2bab02587999175c26f131fc898534`  
		Last Modified: Tue, 12 Oct 2021 01:35:18 GMT  
		Size: 35.9 MB (35900678 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bookworm-slim` - linux; s390x

```console
$ docker pull debian@sha256:98a9cc4b4aeb8eb0d8f81f60155e1c2ed1a2c228678d2aca4047082f04b582c9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.9 MB (29947073 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:287ecd72602d334147536fbaa8c09a03503bd0d35ba2812d6e35b7774b7a8411`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:41:58 GMT
ADD file:fa2051ff320c558a9da82f6d05b5bc8144b1bbd925e103978a8493e9341ea7da in / 
# Tue, 12 Oct 2021 00:42:00 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:e44f3cf4e8b7f77baa7d2ac41c1a985b13c519dcfcb0667fb483f72860f50fca`  
		Last Modified: Tue, 12 Oct 2021 00:47:30 GMT  
		Size: 29.9 MB (29947073 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:bullseye`

```console
$ docker pull debian@sha256:4d6ab716de467aad58e91b1b720f0badd7478847ec7a18f66027d0f8a329a43c
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:bullseye` - linux; amd64

```console
$ docker pull debian@sha256:826d8850098b954c3d67c2e45ea7a08fa681c128126ba01b35bd2689b1bb2e04
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.9 MB (54917520 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f776cfb21b5e06bb5b4883eb15c09ab928a411476b8275293c7f96d09b90f7f9`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:20:30 GMT
ADD file:aea313ae50ce6474a3df142b34d4dcba4e7e0186ea6fe55389cb2ea903b9ebbb in / 
# Tue, 12 Oct 2021 01:20:30 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:bb7d5a84853b217ac05783963f12b034243070c1c9c8d2e60ada47444f3cce04`  
		Last Modified: Tue, 12 Oct 2021 01:25:37 GMT  
		Size: 54.9 MB (54917520 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye` - linux; arm variant v5

```console
$ docker pull debian@sha256:4b164f1a9efee849da43191cf8316ac35e7ea1bc9c1b9c73f6c4dbcfa1296c8b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **52.5 MB (52452198 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ba60f6fec7f0faafeb0679c8e0a1df7c55eda14bed43d9e5f561d1fa50a0db76`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:49:54 GMT
ADD file:b9969604315238de271e1769a4b8dd85cdf0d33f9fac387e940d5195ac3030a1 in / 
# Tue, 12 Oct 2021 00:49:55 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:a97c782f2538791984ce12c859cada96e395e3950d4cb7d1238e08acd2eb74ec`  
		Last Modified: Tue, 12 Oct 2021 01:05:12 GMT  
		Size: 52.5 MB (52452198 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye` - linux; arm variant v7

```console
$ docker pull debian@sha256:580d9d9b46eb13e047b78c71a43d9f61521995def78d31586f6ab770c34d47cd
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.1 MB (50118673 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5aeb2ea51c3d45f85b7b225916cea627d677cad4371093aaea94f9a251e6cafb`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:27:59 GMT
ADD file:d9f40ca54fb79741d1e76c7bca9250f78453d0b39fb6c921337f3071a79a55a9 in / 
# Tue, 12 Oct 2021 01:28:00 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:88ce59c3081c4041f41eb407032db40c207cc2d806ba0eb12e170b697db91e52`  
		Last Modified: Tue, 12 Oct 2021 01:43:42 GMT  
		Size: 50.1 MB (50118673 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:21e79e173393a5c03e2dd910354affc1e6ea995aa3c88a18d40bae41760b3d4e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.6 MB (53603015 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:bf33d9a9dfb709a9a24218f6ea55bab3965d0029cdc2ce01214ad1a87040e530`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:04 GMT
ADD file:1529ae12e334fd992892d3fb97c103297cff7e0115b0475bec4c093939a2bff7 in / 
# Tue, 12 Oct 2021 01:41:04 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:1c47a423366578e5ce665d03788914bf0459485a627a27896fa9c5663ce55cdf`  
		Last Modified: Tue, 12 Oct 2021 01:47:41 GMT  
		Size: 53.6 MB (53603015 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye` - linux; 386

```console
$ docker pull debian@sha256:7448716003c940a5adbf4ddedf0486ecdfeb18fbcd8f13c700886ba0d9b82318
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.9 MB (55923419 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b76357513741b0e14fe9ca5c1aa46fe6f4c5e853ff0b0fc03ac27e798ca40c59`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:39:30 GMT
ADD file:e007d6eff02dbc696e1166db315b97a77ae8aa65f2e66ca8074765dde9c70a59 in / 
# Tue, 12 Oct 2021 01:39:31 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6911c8f66691266d86e2cafa67ca005d919f357436fac971c694d37625b5ba90`  
		Last Modified: Tue, 12 Oct 2021 01:47:06 GMT  
		Size: 55.9 MB (55923419 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye` - linux; mips64le

```console
$ docker pull debian@sha256:9d1807f24103e235abbc8df2d946332daad7f82f1a650b5abe0ffcd4af864aec
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53169836 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:18cdbedcbd6db4f83257f4056d6486eb20919f55f29ceb40d247c93b050903ec`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:10:49 GMT
ADD file:856952232716a2e2d5069387a06a537937a1dec1bb75bc9519c60d6ad226bddb in / 
# Tue, 12 Oct 2021 01:10:50 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:3d2cd78b9c44ce4eadbb0cc35b884bfd0dac51123f3c8c6645cb8dc9dc9c1512`  
		Last Modified: Tue, 12 Oct 2021 01:19:18 GMT  
		Size: 53.2 MB (53169836 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye` - linux; ppc64le

```console
$ docker pull debian@sha256:6e37f5053e41a23ff8c716c87bd1ba185e732979f0f4f26581842bafe5ae2790
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **58.8 MB (58808876 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ec0ee09301b7955b4289ddd8912742ea5c8473147ff2f55955cade3d9962c343`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:25:04 GMT
ADD file:127b21494e1c790bdbb4c277638b3e16b25fbbcd1e0cdb52e60a4f55a57cd0f2 in / 
# Tue, 12 Oct 2021 01:25:10 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c7e7ddcf3b3abfed2b7aab50c1b68bfdfc6fcffed2f4b93ebff4d6050b3d5e72`  
		Last Modified: Tue, 12 Oct 2021 01:36:12 GMT  
		Size: 58.8 MB (58808876 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye` - linux; s390x

```console
$ docker pull debian@sha256:853435582c5e3dd9695a0748116a6dc3bb09a6d8ead73bfc70d80a8aacc686f5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53192895 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:0ab94af22e7333d274f0aae62c0d3bcd833fa3f70ed1618bdb62eca5b57e7495`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:42:09 GMT
ADD file:a7a74f6be757ceb5e84c440e739019782df1a118264eef70aa49886a976c43f6 in / 
# Tue, 12 Oct 2021 00:42:12 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:081299fd984cd296112766e1cc55f44fffbf898b2900b01c0333962494a2dd80`  
		Last Modified: Tue, 12 Oct 2021 00:47:43 GMT  
		Size: 53.2 MB (53192895 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:bullseye-20211011`

```console
$ docker pull debian@sha256:4d6ab716de467aad58e91b1b720f0badd7478847ec7a18f66027d0f8a329a43c
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:bullseye-20211011` - linux; amd64

```console
$ docker pull debian@sha256:826d8850098b954c3d67c2e45ea7a08fa681c128126ba01b35bd2689b1bb2e04
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.9 MB (54917520 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f776cfb21b5e06bb5b4883eb15c09ab928a411476b8275293c7f96d09b90f7f9`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:20:30 GMT
ADD file:aea313ae50ce6474a3df142b34d4dcba4e7e0186ea6fe55389cb2ea903b9ebbb in / 
# Tue, 12 Oct 2021 01:20:30 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:bb7d5a84853b217ac05783963f12b034243070c1c9c8d2e60ada47444f3cce04`  
		Last Modified: Tue, 12 Oct 2021 01:25:37 GMT  
		Size: 54.9 MB (54917520 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-20211011` - linux; arm variant v5

```console
$ docker pull debian@sha256:4b164f1a9efee849da43191cf8316ac35e7ea1bc9c1b9c73f6c4dbcfa1296c8b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **52.5 MB (52452198 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ba60f6fec7f0faafeb0679c8e0a1df7c55eda14bed43d9e5f561d1fa50a0db76`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:49:54 GMT
ADD file:b9969604315238de271e1769a4b8dd85cdf0d33f9fac387e940d5195ac3030a1 in / 
# Tue, 12 Oct 2021 00:49:55 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:a97c782f2538791984ce12c859cada96e395e3950d4cb7d1238e08acd2eb74ec`  
		Last Modified: Tue, 12 Oct 2021 01:05:12 GMT  
		Size: 52.5 MB (52452198 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-20211011` - linux; arm variant v7

```console
$ docker pull debian@sha256:580d9d9b46eb13e047b78c71a43d9f61521995def78d31586f6ab770c34d47cd
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.1 MB (50118673 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5aeb2ea51c3d45f85b7b225916cea627d677cad4371093aaea94f9a251e6cafb`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:27:59 GMT
ADD file:d9f40ca54fb79741d1e76c7bca9250f78453d0b39fb6c921337f3071a79a55a9 in / 
# Tue, 12 Oct 2021 01:28:00 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:88ce59c3081c4041f41eb407032db40c207cc2d806ba0eb12e170b697db91e52`  
		Last Modified: Tue, 12 Oct 2021 01:43:42 GMT  
		Size: 50.1 MB (50118673 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-20211011` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:21e79e173393a5c03e2dd910354affc1e6ea995aa3c88a18d40bae41760b3d4e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.6 MB (53603015 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:bf33d9a9dfb709a9a24218f6ea55bab3965d0029cdc2ce01214ad1a87040e530`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:04 GMT
ADD file:1529ae12e334fd992892d3fb97c103297cff7e0115b0475bec4c093939a2bff7 in / 
# Tue, 12 Oct 2021 01:41:04 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:1c47a423366578e5ce665d03788914bf0459485a627a27896fa9c5663ce55cdf`  
		Last Modified: Tue, 12 Oct 2021 01:47:41 GMT  
		Size: 53.6 MB (53603015 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-20211011` - linux; 386

```console
$ docker pull debian@sha256:7448716003c940a5adbf4ddedf0486ecdfeb18fbcd8f13c700886ba0d9b82318
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.9 MB (55923419 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b76357513741b0e14fe9ca5c1aa46fe6f4c5e853ff0b0fc03ac27e798ca40c59`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:39:30 GMT
ADD file:e007d6eff02dbc696e1166db315b97a77ae8aa65f2e66ca8074765dde9c70a59 in / 
# Tue, 12 Oct 2021 01:39:31 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6911c8f66691266d86e2cafa67ca005d919f357436fac971c694d37625b5ba90`  
		Last Modified: Tue, 12 Oct 2021 01:47:06 GMT  
		Size: 55.9 MB (55923419 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-20211011` - linux; mips64le

```console
$ docker pull debian@sha256:9d1807f24103e235abbc8df2d946332daad7f82f1a650b5abe0ffcd4af864aec
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53169836 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:18cdbedcbd6db4f83257f4056d6486eb20919f55f29ceb40d247c93b050903ec`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:10:49 GMT
ADD file:856952232716a2e2d5069387a06a537937a1dec1bb75bc9519c60d6ad226bddb in / 
# Tue, 12 Oct 2021 01:10:50 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:3d2cd78b9c44ce4eadbb0cc35b884bfd0dac51123f3c8c6645cb8dc9dc9c1512`  
		Last Modified: Tue, 12 Oct 2021 01:19:18 GMT  
		Size: 53.2 MB (53169836 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-20211011` - linux; ppc64le

```console
$ docker pull debian@sha256:6e37f5053e41a23ff8c716c87bd1ba185e732979f0f4f26581842bafe5ae2790
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **58.8 MB (58808876 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ec0ee09301b7955b4289ddd8912742ea5c8473147ff2f55955cade3d9962c343`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:25:04 GMT
ADD file:127b21494e1c790bdbb4c277638b3e16b25fbbcd1e0cdb52e60a4f55a57cd0f2 in / 
# Tue, 12 Oct 2021 01:25:10 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c7e7ddcf3b3abfed2b7aab50c1b68bfdfc6fcffed2f4b93ebff4d6050b3d5e72`  
		Last Modified: Tue, 12 Oct 2021 01:36:12 GMT  
		Size: 58.8 MB (58808876 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-20211011` - linux; s390x

```console
$ docker pull debian@sha256:853435582c5e3dd9695a0748116a6dc3bb09a6d8ead73bfc70d80a8aacc686f5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53192895 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:0ab94af22e7333d274f0aae62c0d3bcd833fa3f70ed1618bdb62eca5b57e7495`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:42:09 GMT
ADD file:a7a74f6be757ceb5e84c440e739019782df1a118264eef70aa49886a976c43f6 in / 
# Tue, 12 Oct 2021 00:42:12 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:081299fd984cd296112766e1cc55f44fffbf898b2900b01c0333962494a2dd80`  
		Last Modified: Tue, 12 Oct 2021 00:47:43 GMT  
		Size: 53.2 MB (53192895 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:bullseye-20211011-slim`

```console
$ docker pull debian@sha256:dddc0f5f01db7ca3599fd8cf9821ffc4d09ec9d7d15e49019e73228ac1eee7f9
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:bullseye-20211011-slim` - linux; amd64

```console
$ docker pull debian@sha256:312218c8dae688bae4e9d12926704fa9af6f7307a6edb4f66e479702a9af5a0c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **31.4 MB (31357311 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:dd984c2cf05c58c61026b0bd2298b30aa87bca6f234db507396371137c891a6c`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:20:42 GMT
ADD file:16dc2c6d1932194edec28d730b004fd6deca3d0f0e1a07bc5b8b6e8a1662f7af in / 
# Tue, 12 Oct 2021 01:20:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:7d63c13d9b9b6ec5f05a2b07daadacaa9c610d01102a662ae9b1d082105f1ffa`  
		Last Modified: Tue, 12 Oct 2021 01:26:05 GMT  
		Size: 31.4 MB (31357311 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-20211011-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:6b8e1746e462cd13b1d954f55a6275a7eeed331a633cf0d62b17e5c6fe4983a2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **28.9 MB (28899715 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:83ed6052a5a4c461efabe92d02b3067de1d84e9f619dbb0f973eaf1bcc7872ac`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:50:30 GMT
ADD file:fb1afabdc1f93b4866b4c4d696f272b4888519cb5bf35eb7ed1a4c021d1a04c8 in / 
# Tue, 12 Oct 2021 00:50:30 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:5c9bab66abc5c53391435f6fd66e0ff3571295d568f45ac7d2dc480bcbfd56e8`  
		Last Modified: Tue, 12 Oct 2021 01:06:07 GMT  
		Size: 28.9 MB (28899715 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-20211011-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:c61311dc78459c6c94b87c3ae3ad070449e532d276459d395ee01645c2208676
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **26.6 MB (26561058 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b45f159c2abea86ab575dff875e962b1b64cabd5787d919253aab1db083a4d91`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:28:36 GMT
ADD file:89eac94007ac04f9168737686fa0a6c737f2c28fc9e5918d4d512924fe1973be in / 
# Tue, 12 Oct 2021 01:28:37 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:4e5300249f84466df4dc73ea0ce09938ca00c1718bb12619c4f26bd936162331`  
		Last Modified: Tue, 12 Oct 2021 01:44:28 GMT  
		Size: 26.6 MB (26561058 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-20211011-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:6b5a9ba2bf7d60c09ddee95638a8421a4ad182706052882f24f7069542891b88
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.0 MB (30043906 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:2b66020322207f385b8d57efb4be7222fa200473d803a7bcabf7c11c03f95839`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:18 GMT
ADD file:d84144ad575672cd6cc8a00c8e5a88ab0545348f040fc249ea5fcf8193b2bce6 in / 
# Tue, 12 Oct 2021 01:41:18 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:a9eb63951c1c2ee8efcc12b696928fee3741a2d4eae76f2da3e161a5d90548bf`  
		Last Modified: Tue, 12 Oct 2021 01:48:13 GMT  
		Size: 30.0 MB (30043906 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-20211011-slim` - linux; 386

```console
$ docker pull debian@sha256:5e70f265871e466baa10bf4ab07c62ca56b88d72d8504feacb466b2367e4a22a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **32.4 MB (32370344 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6f31b9786954c6a7ca8b1ed9c5da3397cef6ecea93626779c64675e2052b1d3f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:39:47 GMT
ADD file:42196ffa4c70af8d9f1e7b74edb3bb92d47296acf989adf615e8208230f8bd0c in / 
# Tue, 12 Oct 2021 01:39:47 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:87318d165b5c0fdf05c8ccf97d83084f56b4608075a3335b1a46c76295b82753`  
		Last Modified: Tue, 12 Oct 2021 01:47:39 GMT  
		Size: 32.4 MB (32370344 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-20211011-slim` - linux; mips64le

```console
$ docker pull debian@sha256:1fb2dc326cae06c54451f67ade73c85a23fe49a84de793146069a58c374cf833
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.6 MB (29618732 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:91f0d7e87aa154d1593aad58bebf939b9bb22590172455e65978a9b81bb858ee`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:11:15 GMT
ADD file:f90ca8957172106f42a9096c9a21082d51f3201aa78e6f64621a73117e2b7b6a in / 
# Tue, 12 Oct 2021 01:11:16 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:59ddca50ce05605e6234c1e7213c39cdedabc48ef11637e12156b7bd69afe161`  
		Last Modified: Tue, 12 Oct 2021 01:20:03 GMT  
		Size: 29.6 MB (29618732 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-20211011-slim` - linux; ppc64le

```console
$ docker pull debian@sha256:222db950b210833c502a932f306b728f6fd711035b2470facf689ed4286df69c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **35.3 MB (35258729 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8452d145c52c5b9d0ebed595d8909727e790af2b9b4a2d31c52918018c608fde`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:25:45 GMT
ADD file:d8794fadf16c9948c3574ba28740d08393eca57766340333ad91f9c2a33eb719 in / 
# Tue, 12 Oct 2021 01:25:53 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6c28c1405c677dd809094d06190225b38cee3d24f18da38d287ef5bcc67884d6`  
		Last Modified: Tue, 12 Oct 2021 01:37:00 GMT  
		Size: 35.3 MB (35258729 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-20211011-slim` - linux; s390x

```console
$ docker pull debian@sha256:6b06aa7d0d589aaeb2d76e1488bd1d1984a2f8524eef19e66592b80a66308489
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.6 MB (29641215 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:077e43ea2b6a64971755f52a382c17917c68ab24bb5eb5ee848be6ba18f4f675`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:42:27 GMT
ADD file:6038dd6db57fb05c3d39c02c3379667ccd2989e7667ff773a8020fe6a69a760c in / 
# Tue, 12 Oct 2021 00:42:29 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:ded751c48f72503973be01be2794cc039490f22b039b8ac106e9f17de4980742`  
		Last Modified: Tue, 12 Oct 2021 00:48:05 GMT  
		Size: 29.6 MB (29641215 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:bullseye-backports`

```console
$ docker pull debian@sha256:d87a25305470755325ffd4d07496a66ec94d18b4d7942ab2b60ac5c9f3cf4e5c
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:bullseye-backports` - linux; amd64

```console
$ docker pull debian@sha256:99c69d6fbd7bb7c92d7c8f429c004b21508727e72e1b7aaefedfd06bdff99020
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.9 MB (54917746 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:50ef70cf5583e9af3f63e6510a1e78339ad5c7ab03d4adb3bb88208876be9daf`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:20:30 GMT
ADD file:aea313ae50ce6474a3df142b34d4dcba4e7e0186ea6fe55389cb2ea903b9ebbb in / 
# Tue, 12 Oct 2021 01:20:30 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:20:34 GMT
RUN echo 'deb http://deb.debian.org/debian bullseye-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:bb7d5a84853b217ac05783963f12b034243070c1c9c8d2e60ada47444f3cce04`  
		Last Modified: Tue, 12 Oct 2021 01:25:37 GMT  
		Size: 54.9 MB (54917520 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:799356225d6dc165ffbfa67601d6c134bce1964c3c27653dd570ce8402fbcee1`  
		Last Modified: Tue, 12 Oct 2021 01:25:53 GMT  
		Size: 226.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-backports` - linux; arm variant v5

```console
$ docker pull debian@sha256:96d16fb911e1bdb4856c459a8344f9aac828f8f0b53368378aad6732ddd19e05
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **52.5 MB (52452426 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a5c79f29141f679338338bc53354a1416ca2cd20ca9a726ef8ee3814657bee44`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:49:54 GMT
ADD file:b9969604315238de271e1769a4b8dd85cdf0d33f9fac387e940d5195ac3030a1 in / 
# Tue, 12 Oct 2021 00:49:55 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 00:50:12 GMT
RUN echo 'deb http://deb.debian.org/debian bullseye-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:a97c782f2538791984ce12c859cada96e395e3950d4cb7d1238e08acd2eb74ec`  
		Last Modified: Tue, 12 Oct 2021 01:05:12 GMT  
		Size: 52.5 MB (52452198 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:f6a06514d4dff5ba89305dc9bb3d0e5da4e123193ba836b6e5b172d0c5859cfe`  
		Last Modified: Tue, 12 Oct 2021 01:05:40 GMT  
		Size: 228.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-backports` - linux; arm variant v7

```console
$ docker pull debian@sha256:013b564494f6acd7dcbe8360defaaa4a3a2fc62ffbb5cca6e979b82e0eabbd96
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.1 MB (50118902 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6c69850a1bb73676acb3ae05afe0ed632ac31bb8777e77357b0ea1111c0470f0`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:27:59 GMT
ADD file:d9f40ca54fb79741d1e76c7bca9250f78453d0b39fb6c921337f3071a79a55a9 in / 
# Tue, 12 Oct 2021 01:28:00 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:28:16 GMT
RUN echo 'deb http://deb.debian.org/debian bullseye-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:88ce59c3081c4041f41eb407032db40c207cc2d806ba0eb12e170b697db91e52`  
		Last Modified: Tue, 12 Oct 2021 01:43:42 GMT  
		Size: 50.1 MB (50118673 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:08190e7f5185f893be8d7f9b6e7f0ffda23089cf4a5d4ce7c4712c738b715a32`  
		Last Modified: Tue, 12 Oct 2021 01:44:05 GMT  
		Size: 229.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-backports` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:3fd3076ecab5e5c319f83b3ea8fa270523788a404ee5675e84f39341cf3ccc8d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.6 MB (53603242 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5e165b88145286cd15b37d929ac620ba0aaf7b74c7dc8cdfd0b02bb72894f9ec`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:04 GMT
ADD file:1529ae12e334fd992892d3fb97c103297cff7e0115b0475bec4c093939a2bff7 in / 
# Tue, 12 Oct 2021 01:41:04 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:41:11 GMT
RUN echo 'deb http://deb.debian.org/debian bullseye-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:1c47a423366578e5ce665d03788914bf0459485a627a27896fa9c5663ce55cdf`  
		Last Modified: Tue, 12 Oct 2021 01:47:41 GMT  
		Size: 53.6 MB (53603015 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:fe9d52455d85c315d0d0207631c92edec49bd2547a071a60682d32794c681b7b`  
		Last Modified: Tue, 12 Oct 2021 01:48:01 GMT  
		Size: 227.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-backports` - linux; 386

```console
$ docker pull debian@sha256:17a6f9d8fdd0ca7babd4e790864a43998954758bfe1f63448f52baf8fbf0e3e6
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.9 MB (55923644 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cc1253d103f5ee0fc52c69a557fa850bdb134d2cb4a0f55f3c5ddcd549aa2114`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:39:30 GMT
ADD file:e007d6eff02dbc696e1166db315b97a77ae8aa65f2e66ca8074765dde9c70a59 in / 
# Tue, 12 Oct 2021 01:39:31 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:39:38 GMT
RUN echo 'deb http://deb.debian.org/debian bullseye-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:6911c8f66691266d86e2cafa67ca005d919f357436fac971c694d37625b5ba90`  
		Last Modified: Tue, 12 Oct 2021 01:47:06 GMT  
		Size: 55.9 MB (55923419 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:2e386791194a3b2c5f5bbf14267f165ec4d32db20fefaf800786554d7d2124bc`  
		Last Modified: Tue, 12 Oct 2021 01:47:25 GMT  
		Size: 225.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-backports` - linux; mips64le

```console
$ docker pull debian@sha256:368464ec5c56f2b640a6d9944ae7e3e1abac9c9549ae214e272cdf6a68f06bd4
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53170065 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8d3792ce1079724323b0cbf97471cd2db55c5de9a93c65803615b93fa43c1284`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:10:49 GMT
ADD file:856952232716a2e2d5069387a06a537937a1dec1bb75bc9519c60d6ad226bddb in / 
# Tue, 12 Oct 2021 01:10:50 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:10:57 GMT
RUN echo 'deb http://deb.debian.org/debian bullseye-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:3d2cd78b9c44ce4eadbb0cc35b884bfd0dac51123f3c8c6645cb8dc9dc9c1512`  
		Last Modified: Tue, 12 Oct 2021 01:19:18 GMT  
		Size: 53.2 MB (53169836 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:e006cd97eb9073090f248bec077c5d09eb03d7f4042af018120745c0d000b843`  
		Last Modified: Tue, 12 Oct 2021 01:19:35 GMT  
		Size: 229.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-backports` - linux; ppc64le

```console
$ docker pull debian@sha256:40f8b8b2e3ea477ad834637661bbddd461a2f56b32c04a1b887ed0404e85f6a8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **58.8 MB (58809106 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:48a6877f5dd128c20503b72eb18b2926abe77cae1a58ce5df093a7814e83d067`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:25:04 GMT
ADD file:127b21494e1c790bdbb4c277638b3e16b25fbbcd1e0cdb52e60a4f55a57cd0f2 in / 
# Tue, 12 Oct 2021 01:25:10 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:25:26 GMT
RUN echo 'deb http://deb.debian.org/debian bullseye-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:c7e7ddcf3b3abfed2b7aab50c1b68bfdfc6fcffed2f4b93ebff4d6050b3d5e72`  
		Last Modified: Tue, 12 Oct 2021 01:36:12 GMT  
		Size: 58.8 MB (58808876 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:56fb001a0c62f3a0c70976e2acd34e874089886aa5b0f8ee10fe1e6e579ef15f`  
		Last Modified: Tue, 12 Oct 2021 01:36:31 GMT  
		Size: 230.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-backports` - linux; s390x

```console
$ docker pull debian@sha256:b197bc5c4de0dc464c4e04dc9afc53a974157b59b36c5eee0ed80b8994feb7e5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53193120 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:194a980937758b4f99a072dd0852f765701e9b001b176f5ed422ba5a9a89e918`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:42:09 GMT
ADD file:a7a74f6be757ceb5e84c440e739019782df1a118264eef70aa49886a976c43f6 in / 
# Tue, 12 Oct 2021 00:42:12 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 00:42:20 GMT
RUN echo 'deb http://deb.debian.org/debian bullseye-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:081299fd984cd296112766e1cc55f44fffbf898b2900b01c0333962494a2dd80`  
		Last Modified: Tue, 12 Oct 2021 00:47:43 GMT  
		Size: 53.2 MB (53192895 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:659e14461d58f8fb62b8e8f7631205eb1ff6eea842d61605a55232b8583d46f3`  
		Last Modified: Tue, 12 Oct 2021 00:47:56 GMT  
		Size: 225.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:bullseye-slim`

```console
$ docker pull debian@sha256:dddc0f5f01db7ca3599fd8cf9821ffc4d09ec9d7d15e49019e73228ac1eee7f9
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:bullseye-slim` - linux; amd64

```console
$ docker pull debian@sha256:312218c8dae688bae4e9d12926704fa9af6f7307a6edb4f66e479702a9af5a0c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **31.4 MB (31357311 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:dd984c2cf05c58c61026b0bd2298b30aa87bca6f234db507396371137c891a6c`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:20:42 GMT
ADD file:16dc2c6d1932194edec28d730b004fd6deca3d0f0e1a07bc5b8b6e8a1662f7af in / 
# Tue, 12 Oct 2021 01:20:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:7d63c13d9b9b6ec5f05a2b07daadacaa9c610d01102a662ae9b1d082105f1ffa`  
		Last Modified: Tue, 12 Oct 2021 01:26:05 GMT  
		Size: 31.4 MB (31357311 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:6b8e1746e462cd13b1d954f55a6275a7eeed331a633cf0d62b17e5c6fe4983a2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **28.9 MB (28899715 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:83ed6052a5a4c461efabe92d02b3067de1d84e9f619dbb0f973eaf1bcc7872ac`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:50:30 GMT
ADD file:fb1afabdc1f93b4866b4c4d696f272b4888519cb5bf35eb7ed1a4c021d1a04c8 in / 
# Tue, 12 Oct 2021 00:50:30 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:5c9bab66abc5c53391435f6fd66e0ff3571295d568f45ac7d2dc480bcbfd56e8`  
		Last Modified: Tue, 12 Oct 2021 01:06:07 GMT  
		Size: 28.9 MB (28899715 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:c61311dc78459c6c94b87c3ae3ad070449e532d276459d395ee01645c2208676
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **26.6 MB (26561058 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b45f159c2abea86ab575dff875e962b1b64cabd5787d919253aab1db083a4d91`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:28:36 GMT
ADD file:89eac94007ac04f9168737686fa0a6c737f2c28fc9e5918d4d512924fe1973be in / 
# Tue, 12 Oct 2021 01:28:37 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:4e5300249f84466df4dc73ea0ce09938ca00c1718bb12619c4f26bd936162331`  
		Last Modified: Tue, 12 Oct 2021 01:44:28 GMT  
		Size: 26.6 MB (26561058 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:6b5a9ba2bf7d60c09ddee95638a8421a4ad182706052882f24f7069542891b88
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.0 MB (30043906 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:2b66020322207f385b8d57efb4be7222fa200473d803a7bcabf7c11c03f95839`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:18 GMT
ADD file:d84144ad575672cd6cc8a00c8e5a88ab0545348f040fc249ea5fcf8193b2bce6 in / 
# Tue, 12 Oct 2021 01:41:18 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:a9eb63951c1c2ee8efcc12b696928fee3741a2d4eae76f2da3e161a5d90548bf`  
		Last Modified: Tue, 12 Oct 2021 01:48:13 GMT  
		Size: 30.0 MB (30043906 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-slim` - linux; 386

```console
$ docker pull debian@sha256:5e70f265871e466baa10bf4ab07c62ca56b88d72d8504feacb466b2367e4a22a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **32.4 MB (32370344 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6f31b9786954c6a7ca8b1ed9c5da3397cef6ecea93626779c64675e2052b1d3f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:39:47 GMT
ADD file:42196ffa4c70af8d9f1e7b74edb3bb92d47296acf989adf615e8208230f8bd0c in / 
# Tue, 12 Oct 2021 01:39:47 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:87318d165b5c0fdf05c8ccf97d83084f56b4608075a3335b1a46c76295b82753`  
		Last Modified: Tue, 12 Oct 2021 01:47:39 GMT  
		Size: 32.4 MB (32370344 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-slim` - linux; mips64le

```console
$ docker pull debian@sha256:1fb2dc326cae06c54451f67ade73c85a23fe49a84de793146069a58c374cf833
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.6 MB (29618732 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:91f0d7e87aa154d1593aad58bebf939b9bb22590172455e65978a9b81bb858ee`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:11:15 GMT
ADD file:f90ca8957172106f42a9096c9a21082d51f3201aa78e6f64621a73117e2b7b6a in / 
# Tue, 12 Oct 2021 01:11:16 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:59ddca50ce05605e6234c1e7213c39cdedabc48ef11637e12156b7bd69afe161`  
		Last Modified: Tue, 12 Oct 2021 01:20:03 GMT  
		Size: 29.6 MB (29618732 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-slim` - linux; ppc64le

```console
$ docker pull debian@sha256:222db950b210833c502a932f306b728f6fd711035b2470facf689ed4286df69c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **35.3 MB (35258729 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8452d145c52c5b9d0ebed595d8909727e790af2b9b4a2d31c52918018c608fde`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:25:45 GMT
ADD file:d8794fadf16c9948c3574ba28740d08393eca57766340333ad91f9c2a33eb719 in / 
# Tue, 12 Oct 2021 01:25:53 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6c28c1405c677dd809094d06190225b38cee3d24f18da38d287ef5bcc67884d6`  
		Last Modified: Tue, 12 Oct 2021 01:37:00 GMT  
		Size: 35.3 MB (35258729 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:bullseye-slim` - linux; s390x

```console
$ docker pull debian@sha256:6b06aa7d0d589aaeb2d76e1488bd1d1984a2f8524eef19e66592b80a66308489
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.6 MB (29641215 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:077e43ea2b6a64971755f52a382c17917c68ab24bb5eb5ee848be6ba18f4f675`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:42:27 GMT
ADD file:6038dd6db57fb05c3d39c02c3379667ccd2989e7667ff773a8020fe6a69a760c in / 
# Tue, 12 Oct 2021 00:42:29 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:ded751c48f72503973be01be2794cc039490f22b039b8ac106e9f17de4980742`  
		Last Modified: Tue, 12 Oct 2021 00:48:05 GMT  
		Size: 29.6 MB (29641215 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:buster`

```console
$ docker pull debian@sha256:f9182ead292f45165f4a851e5ff98ea0800e172ccedce7d17764ffaae5ed4d6e
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:buster` - linux; amd64

```console
$ docker pull debian@sha256:2f4b2c4b44ec6d9f4afc9578d8475f7272dc50783268dc672d19b0db6098b89d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.4 MB (50436692 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:2b6f409b1d24285cbae8f2c6baa6969741151bed561a9b47c57b7ff1ee33829a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:20:53 GMT
ADD file:98c256057b79b141aea9a806a4538cf6c3f340d7e3b0d6e8c363699333f3406b in / 
# Tue, 12 Oct 2021 01:20:53 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:07471e81507f7cf1100827f10c60c3c0422d1222430e34e527d97ec72b14a193`  
		Last Modified: Tue, 12 Oct 2021 01:26:26 GMT  
		Size: 50.4 MB (50436692 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster` - linux; arm variant v5

```console
$ docker pull debian@sha256:9e9fbcf7fb8d2709289dac41865ef04aa00d313e88f87671f03d543a178f2243
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **48.2 MB (48154085 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:59313f9a9838cbb93bbac0a7840af610a5f4157a02dd303f3d18a7ca58220dcd`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:50:58 GMT
ADD file:f9432d6da0faddeaa117c57c0c007cd56738ef01549baf71e5c59939b7e69b6c in / 
# Tue, 12 Oct 2021 00:50:59 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:ae00de77d9c27fe3ba9ea06833b273e4fc0aa128a3a61d3992632e6e3941a78c`  
		Last Modified: Tue, 12 Oct 2021 01:07:01 GMT  
		Size: 48.2 MB (48154085 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster` - linux; arm variant v7

```console
$ docker pull debian@sha256:502cc09268a941c89fd917694127bee670e25885303627116fe30039bc01115b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **45.9 MB (45917899 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:03a2b8db893e04a8f924f52abea53a5f813cbea9ce26d34ff99c50e78d5ecbf2`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:29:06 GMT
ADD file:b1857dc4fe4fbc4af220829cc6af7169c4555d3f63a3a304db6fa0146b1d5539 in / 
# Tue, 12 Oct 2021 01:29:08 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:139a906e9407e96c50669130c30fe8fba2681b14aa838a3e52f8753b566ef1c8`  
		Last Modified: Tue, 12 Oct 2021 01:45:13 GMT  
		Size: 45.9 MB (45917899 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:e10bf96e3d994bb35989db5acd0bc38e689fa7e88d44acaea92188a1cbb62efe
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.2 MB (49222756 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:714cbbc292e7a7c0d9607c589230e6069290bd286c1ffa66e74d3e03008de43e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:28 GMT
ADD file:aed1709ccba6a81b9726b228fad7b81bcf4c16bafe723981ad37076322d78986 in / 
# Tue, 12 Oct 2021 01:41:29 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2ff6d7a9e7d73e4a01b9417518d18c001728c45fa8109ed8f55aaa50e7981482`  
		Last Modified: Tue, 12 Oct 2021 01:48:38 GMT  
		Size: 49.2 MB (49222756 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster` - linux; 386

```console
$ docker pull debian@sha256:d607eb89ee478cf01e2e664fcfd15788c88096a12c356855766a461c0d247397
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **51.2 MB (51207606 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:292a353f30cfc06a30e9044c17083c5cb39c4194ee653e82ef6fe3dd91ae0ef6`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:01 GMT
ADD file:1461fa0362c70b5b7a677c57dd48633827fd9635a9cb136730aa7581cc523b46 in / 
# Tue, 12 Oct 2021 01:40:02 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:f4b233498baa64e956a5f70979351e206bd085bd547a3cf25c08b154348df726`  
		Last Modified: Tue, 12 Oct 2021 01:48:07 GMT  
		Size: 51.2 MB (51207606 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster` - linux; mips64le

```console
$ docker pull debian@sha256:c44701e5d78443bfaf970f5fad82b90cd44b438f855c790667d3037eb4cac7e6
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.1 MB (49079545 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e09b392b225ac4e6c8dba0e912d0a360db896a7426b4317f436e5fecef0a9876`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:11:41 GMT
ADD file:37326203d7d5c7764007ca3a26d61a2da861011dddf8992bc1a0b70ba659c0c1 in / 
# Tue, 12 Oct 2021 01:11:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:f086aaa10cbe2dfccc93401fe3cce55b2f5eed2ba1a2fe3e34a4501644f9c8fa`  
		Last Modified: Tue, 12 Oct 2021 01:20:49 GMT  
		Size: 49.1 MB (49079545 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster` - linux; ppc64le

```console
$ docker pull debian@sha256:96c855e6aac97b7fee0b8c94e6dd1279d763b5e0e6e33ce10984cf658e14608c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.2 MB (54183476 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ab49c44234888d80796a160c0c11bf72ba4efa6e3f68cdee24313275ac4121d2`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:26:10 GMT
ADD file:94a7157f0c578810dcc73fd2dbdcb4ce021626d9858288c970e007a590c71d44 in / 
# Tue, 12 Oct 2021 01:26:18 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:77e7cc3fe486cc9a5ddc4cee43979cbebb5e7c4f36b82ccaa61dbda5dd37dac8`  
		Last Modified: Tue, 12 Oct 2021 01:37:52 GMT  
		Size: 54.2 MB (54183476 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster` - linux; s390x

```console
$ docker pull debian@sha256:94950a0190738d1fca34b5038075f702536c73be4ce2f6ecc69b0123507856aa
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.0 MB (49004847 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3ba87fd7547978ad82059f9a3be11170d199a614928b3f1a0f7f6346eb292b6a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:42:39 GMT
ADD file:91e4bb81a5308737580259a9213b02933901431aa2ea23f3f4f59321a6ccc301 in / 
# Tue, 12 Oct 2021 00:42:41 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:9df790508568720a3b71c02b057e4a119d9d2e8ed003ccba18d600e1ea44fa8a`  
		Last Modified: Tue, 12 Oct 2021 00:48:22 GMT  
		Size: 49.0 MB (49004847 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:buster-20211011`

```console
$ docker pull debian@sha256:f9182ead292f45165f4a851e5ff98ea0800e172ccedce7d17764ffaae5ed4d6e
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:buster-20211011` - linux; amd64

```console
$ docker pull debian@sha256:2f4b2c4b44ec6d9f4afc9578d8475f7272dc50783268dc672d19b0db6098b89d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.4 MB (50436692 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:2b6f409b1d24285cbae8f2c6baa6969741151bed561a9b47c57b7ff1ee33829a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:20:53 GMT
ADD file:98c256057b79b141aea9a806a4538cf6c3f340d7e3b0d6e8c363699333f3406b in / 
# Tue, 12 Oct 2021 01:20:53 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:07471e81507f7cf1100827f10c60c3c0422d1222430e34e527d97ec72b14a193`  
		Last Modified: Tue, 12 Oct 2021 01:26:26 GMT  
		Size: 50.4 MB (50436692 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-20211011` - linux; arm variant v5

```console
$ docker pull debian@sha256:9e9fbcf7fb8d2709289dac41865ef04aa00d313e88f87671f03d543a178f2243
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **48.2 MB (48154085 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:59313f9a9838cbb93bbac0a7840af610a5f4157a02dd303f3d18a7ca58220dcd`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:50:58 GMT
ADD file:f9432d6da0faddeaa117c57c0c007cd56738ef01549baf71e5c59939b7e69b6c in / 
# Tue, 12 Oct 2021 00:50:59 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:ae00de77d9c27fe3ba9ea06833b273e4fc0aa128a3a61d3992632e6e3941a78c`  
		Last Modified: Tue, 12 Oct 2021 01:07:01 GMT  
		Size: 48.2 MB (48154085 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-20211011` - linux; arm variant v7

```console
$ docker pull debian@sha256:502cc09268a941c89fd917694127bee670e25885303627116fe30039bc01115b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **45.9 MB (45917899 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:03a2b8db893e04a8f924f52abea53a5f813cbea9ce26d34ff99c50e78d5ecbf2`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:29:06 GMT
ADD file:b1857dc4fe4fbc4af220829cc6af7169c4555d3f63a3a304db6fa0146b1d5539 in / 
# Tue, 12 Oct 2021 01:29:08 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:139a906e9407e96c50669130c30fe8fba2681b14aa838a3e52f8753b566ef1c8`  
		Last Modified: Tue, 12 Oct 2021 01:45:13 GMT  
		Size: 45.9 MB (45917899 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-20211011` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:e10bf96e3d994bb35989db5acd0bc38e689fa7e88d44acaea92188a1cbb62efe
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.2 MB (49222756 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:714cbbc292e7a7c0d9607c589230e6069290bd286c1ffa66e74d3e03008de43e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:28 GMT
ADD file:aed1709ccba6a81b9726b228fad7b81bcf4c16bafe723981ad37076322d78986 in / 
# Tue, 12 Oct 2021 01:41:29 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2ff6d7a9e7d73e4a01b9417518d18c001728c45fa8109ed8f55aaa50e7981482`  
		Last Modified: Tue, 12 Oct 2021 01:48:38 GMT  
		Size: 49.2 MB (49222756 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-20211011` - linux; 386

```console
$ docker pull debian@sha256:d607eb89ee478cf01e2e664fcfd15788c88096a12c356855766a461c0d247397
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **51.2 MB (51207606 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:292a353f30cfc06a30e9044c17083c5cb39c4194ee653e82ef6fe3dd91ae0ef6`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:01 GMT
ADD file:1461fa0362c70b5b7a677c57dd48633827fd9635a9cb136730aa7581cc523b46 in / 
# Tue, 12 Oct 2021 01:40:02 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:f4b233498baa64e956a5f70979351e206bd085bd547a3cf25c08b154348df726`  
		Last Modified: Tue, 12 Oct 2021 01:48:07 GMT  
		Size: 51.2 MB (51207606 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-20211011` - linux; mips64le

```console
$ docker pull debian@sha256:c44701e5d78443bfaf970f5fad82b90cd44b438f855c790667d3037eb4cac7e6
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.1 MB (49079545 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e09b392b225ac4e6c8dba0e912d0a360db896a7426b4317f436e5fecef0a9876`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:11:41 GMT
ADD file:37326203d7d5c7764007ca3a26d61a2da861011dddf8992bc1a0b70ba659c0c1 in / 
# Tue, 12 Oct 2021 01:11:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:f086aaa10cbe2dfccc93401fe3cce55b2f5eed2ba1a2fe3e34a4501644f9c8fa`  
		Last Modified: Tue, 12 Oct 2021 01:20:49 GMT  
		Size: 49.1 MB (49079545 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-20211011` - linux; ppc64le

```console
$ docker pull debian@sha256:96c855e6aac97b7fee0b8c94e6dd1279d763b5e0e6e33ce10984cf658e14608c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.2 MB (54183476 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ab49c44234888d80796a160c0c11bf72ba4efa6e3f68cdee24313275ac4121d2`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:26:10 GMT
ADD file:94a7157f0c578810dcc73fd2dbdcb4ce021626d9858288c970e007a590c71d44 in / 
# Tue, 12 Oct 2021 01:26:18 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:77e7cc3fe486cc9a5ddc4cee43979cbebb5e7c4f36b82ccaa61dbda5dd37dac8`  
		Last Modified: Tue, 12 Oct 2021 01:37:52 GMT  
		Size: 54.2 MB (54183476 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-20211011` - linux; s390x

```console
$ docker pull debian@sha256:94950a0190738d1fca34b5038075f702536c73be4ce2f6ecc69b0123507856aa
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.0 MB (49004847 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3ba87fd7547978ad82059f9a3be11170d199a614928b3f1a0f7f6346eb292b6a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:42:39 GMT
ADD file:91e4bb81a5308737580259a9213b02933901431aa2ea23f3f4f59321a6ccc301 in / 
# Tue, 12 Oct 2021 00:42:41 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:9df790508568720a3b71c02b057e4a119d9d2e8ed003ccba18d600e1ea44fa8a`  
		Last Modified: Tue, 12 Oct 2021 00:48:22 GMT  
		Size: 49.0 MB (49004847 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:buster-20211011-slim`

```console
$ docker pull debian@sha256:544c93597c784cf68dbe492ef35c00de7f4f6a990955c7144a40b20d86a3475f
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:buster-20211011-slim` - linux; amd64

```console
$ docker pull debian@sha256:f6ed7ce6e3264649e1d4f40585247c50e32faaf268984c5c5cbf0e67cf7f0ec7
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **27.1 MB (27139510 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8ae0cebc834a83dc033065409120379a215e64d4d717449a56f211c10c8f1c95`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:21:05 GMT
ADD file:910392427fdf089bc26b64d6dc450ff3d020c7c1a474d85b2f9298134d0007bd in / 
# Tue, 12 Oct 2021 01:21:05 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:b380bbd43752f83945df8b5d1074fef8dd044820e7d3aef33b655a2483e030c7`  
		Last Modified: Tue, 12 Oct 2021 01:26:51 GMT  
		Size: 27.1 MB (27139510 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-20211011-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:5cf40d31707d8371b9f7657b08fe5757f66b7541f0ba60a6884e3a228f3826e2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **24.9 MB (24872704 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f5327b97db05809d81919bb3751bf09f5ec0862d38c8f871f47d088bb4c13b8e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:51:32 GMT
ADD file:0d95eee31ffe3f4260263e4fac2a61123a4b98d2e7a67efcc7bfea338c54f41d in / 
# Tue, 12 Oct 2021 00:51:32 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:175b083b22fb061f7d1894fe4916a2967ce636d47376da526d90d57c8f7efc60`  
		Last Modified: Tue, 12 Oct 2021 01:07:45 GMT  
		Size: 24.9 MB (24872704 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-20211011-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:f865df940558a112dfac67eef50714dbd4bc2a82f6d302cd2c133649c2b4288e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **22.7 MB (22739698 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:c97f71b3175eb8ee139a3abbb2090e87dedf50b0199d0b2baa920a1e11293556`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:29:42 GMT
ADD file:fe1eaa1b1d760b3d250cb4ac331d26a261fc569f1694bd34e146f00874dc87e5 in / 
# Tue, 12 Oct 2021 01:29:43 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c0b5ba470cac8cfdae4633f5cddb3ba9350fe1d1b1507c4965a8224eb40e52c5`  
		Last Modified: Tue, 12 Oct 2021 01:45:52 GMT  
		Size: 22.7 MB (22739698 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-20211011-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:9f6e6f1f5a4665a552f46028808b28ab19788d28db470de6822febf710f47914
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **25.9 MB (25908479 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d5dbc8809dc0af0cdd5b012ad95108771f6957859cfe7e4ccf0695de3242db94`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:42 GMT
ADD file:f0d53027a7ba594477674127971aa477af3a2bc6bcddef6c0aa174953a5f2db0 in / 
# Tue, 12 Oct 2021 01:41:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:02b45931a436a68cadb742684148ed6aacbbf9aa21447b467c11bac97f92eb46`  
		Last Modified: Tue, 12 Oct 2021 01:49:07 GMT  
		Size: 25.9 MB (25908479 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-20211011-slim` - linux; 386

```console
$ docker pull debian@sha256:870b187824ecb90dbb968ff713e30ad950c42c6b2ddbae30559a48694b87fd77
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **27.8 MB (27791445 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d7232f6968596f6edce31c8b1a83faff4770cafddcf44639813b6ec911d827ee`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:17 GMT
ADD file:1b432d2306b487c59442b30c65ddabd08b45484c6ce09b0c25112c29f4a98f17 in / 
# Tue, 12 Oct 2021 01:40:17 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:860d012fe46ce39c3737ace24d10d8ad65ae9c78abde6891ca6e97b0d7f271dd`  
		Last Modified: Tue, 12 Oct 2021 01:48:37 GMT  
		Size: 27.8 MB (27791445 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-20211011-slim` - linux; mips64le

```console
$ docker pull debian@sha256:b642ad734ac5033873def85d8471bee90fc194a27105eb358e92f25c58c155d7
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **25.8 MB (25806535 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d6273f44d21d6b75bfcf57caf8935a94139bfd43f9fdcc44e4287d185150a495`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:12:07 GMT
ADD file:a467fbf2015350da455cad98b5f5f247d88749cf317062ca09c466c27d53f87b in / 
# Tue, 12 Oct 2021 01:12:07 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:4d8cdee9d607e77b6f8c2203cc9cc02b38004def2b7fe162538c1538da10c870`  
		Last Modified: Tue, 12 Oct 2021 01:21:36 GMT  
		Size: 25.8 MB (25806535 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-20211011-slim` - linux; ppc64le

```console
$ docker pull debian@sha256:ac88814d59305f903079944b7b647c813beceeca3e7ad7920604cd90b9dbd130
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.5 MB (30547197 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6e05e4d22ac1078c5c920a3d1b28da35d0e0c4770f9dc7fb505394229f218b43`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:26:46 GMT
ADD file:5526880c6f19a4b4231a8b2bd7cfc625d116764c4918eff6f0b55f8c1eb38e1a in / 
# Tue, 12 Oct 2021 01:26:50 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:d69973af32573c07c8789c0f9c144a5c0b822a1e85b59db3ce9f8ebc489ce85d`  
		Last Modified: Tue, 12 Oct 2021 01:38:40 GMT  
		Size: 30.5 MB (30547197 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-20211011-slim` - linux; s390x

```console
$ docker pull debian@sha256:007b10cc082c948b5a45f7298e8776c5b0df77331ab3e2bf00b7ed98d85f9d8e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **25.8 MB (25754252 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4340acf96cb3e2fec8d744a74977a94566b488869b7b467c9c1571a45cc40b95`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:42:56 GMT
ADD file:39da9acd4d06d69f3ca8d25ef5c097ae741972d6b15a6a057bc7487380157b61 in / 
# Tue, 12 Oct 2021 00:42:57 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:78a640c6bb4733e5156ce97c5ec773cf97b357c3a07244348384da5060788a61`  
		Last Modified: Tue, 12 Oct 2021 00:48:41 GMT  
		Size: 25.8 MB (25754252 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:buster-backports`

```console
$ docker pull debian@sha256:1d0610b0c516cdddf92373be116461528a1d984878da93d8bd949b70b1083b6f
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:buster-backports` - linux; amd64

```console
$ docker pull debian@sha256:260f275d50e9361423219f5af4a27fa33f4c851b73693e600ba37a0f26dc7984
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.4 MB (50436914 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8beffd5c6a5c4a4564bb69d9a16e9b7b59cc906229114bccf85c22c988c24e56`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:20:53 GMT
ADD file:98c256057b79b141aea9a806a4538cf6c3f340d7e3b0d6e8c363699333f3406b in / 
# Tue, 12 Oct 2021 01:20:53 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:20:57 GMT
RUN echo 'deb http://deb.debian.org/debian buster-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:07471e81507f7cf1100827f10c60c3c0422d1222430e34e527d97ec72b14a193`  
		Last Modified: Tue, 12 Oct 2021 01:26:26 GMT  
		Size: 50.4 MB (50436692 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:ba323914f950f87edf2a69b54067e19d48c5d384c9b48fdb6606c9fb1c50b2c3`  
		Last Modified: Tue, 12 Oct 2021 01:26:41 GMT  
		Size: 222.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-backports` - linux; arm variant v5

```console
$ docker pull debian@sha256:25b19f48f49e556b707f0d76017f83ff16fb6ca68645e33d112f5cff96be7673
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **48.2 MB (48154308 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:06bc18b4d7516528b337fa6bee7117b2b64fbc22ba04927dac76bd5f5e6a916d`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:50:58 GMT
ADD file:f9432d6da0faddeaa117c57c0c007cd56738ef01549baf71e5c59939b7e69b6c in / 
# Tue, 12 Oct 2021 00:50:59 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 00:51:14 GMT
RUN echo 'deb http://deb.debian.org/debian buster-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:ae00de77d9c27fe3ba9ea06833b273e4fc0aa128a3a61d3992632e6e3941a78c`  
		Last Modified: Tue, 12 Oct 2021 01:07:01 GMT  
		Size: 48.2 MB (48154085 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:663f423ff16601594ffc793510a0f613e3e31c1f522ba48ca97795cd48002054`  
		Last Modified: Tue, 12 Oct 2021 01:07:21 GMT  
		Size: 223.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-backports` - linux; arm variant v7

```console
$ docker pull debian@sha256:889b370d50bf8bb07262803819c7ef10ffcb106fc5b5f4671a4c8a1f4d636fd7
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **45.9 MB (45918124 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8f3189e6129ac2ef973ae48fda4307aed91fbf03ca2e34db3a12b28237173373`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:29:06 GMT
ADD file:b1857dc4fe4fbc4af220829cc6af7169c4555d3f63a3a304db6fa0146b1d5539 in / 
# Tue, 12 Oct 2021 01:29:08 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:29:23 GMT
RUN echo 'deb http://deb.debian.org/debian buster-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:139a906e9407e96c50669130c30fe8fba2681b14aa838a3e52f8753b566ef1c8`  
		Last Modified: Tue, 12 Oct 2021 01:45:13 GMT  
		Size: 45.9 MB (45917899 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:99650ff09159f62f16de9ca4bf86aa7bae46ad39890b0ad3534e99fbd1f9754a`  
		Last Modified: Tue, 12 Oct 2021 01:45:32 GMT  
		Size: 225.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-backports` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:1df89395c76d19313ef49af431708d8347ebf4560038af74b9512e402dba2b42
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.2 MB (49222981 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:90139762641543b67a17af2d42fe0e07f82f7bc49bfa12ee280583b123b6496d`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:28 GMT
ADD file:aed1709ccba6a81b9726b228fad7b81bcf4c16bafe723981ad37076322d78986 in / 
# Tue, 12 Oct 2021 01:41:29 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:41:35 GMT
RUN echo 'deb http://deb.debian.org/debian buster-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:2ff6d7a9e7d73e4a01b9417518d18c001728c45fa8109ed8f55aaa50e7981482`  
		Last Modified: Tue, 12 Oct 2021 01:48:38 GMT  
		Size: 49.2 MB (49222756 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:deb1f50ae6aa5c6810387c7ec745a6879b4c9ba6d8e2200b7117cbee481baad6`  
		Last Modified: Tue, 12 Oct 2021 01:48:55 GMT  
		Size: 225.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-backports` - linux; 386

```console
$ docker pull debian@sha256:5c552a9e449c1ef2665d3f696753c0ee661630ea4c87467030b17cb8760c00bb
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **51.2 MB (51207830 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8736b0513cfda3bd27a0183f1d604069df97cbe762233075c57c719e426bdf24`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:01 GMT
ADD file:1461fa0362c70b5b7a677c57dd48633827fd9635a9cb136730aa7581cc523b46 in / 
# Tue, 12 Oct 2021 01:40:02 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:40:08 GMT
RUN echo 'deb http://deb.debian.org/debian buster-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:f4b233498baa64e956a5f70979351e206bd085bd547a3cf25c08b154348df726`  
		Last Modified: Tue, 12 Oct 2021 01:48:07 GMT  
		Size: 51.2 MB (51207606 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:ce39e0c29dc71a85f4801928344cf2d38dc78e8292a8b9212232c9e3b4fc784b`  
		Last Modified: Tue, 12 Oct 2021 01:48:24 GMT  
		Size: 224.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-backports` - linux; mips64le

```console
$ docker pull debian@sha256:159da27aef6da222e62c36ae7a3c4b0114a93461360eb395d711eaae7766f7a2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.1 MB (49079769 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:28678b7935274c1fbfcb922a58e4ab3979b4603d32ad4657ceb558d0b30881ca`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:11:41 GMT
ADD file:37326203d7d5c7764007ca3a26d61a2da861011dddf8992bc1a0b70ba659c0c1 in / 
# Tue, 12 Oct 2021 01:11:42 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:11:49 GMT
RUN echo 'deb http://deb.debian.org/debian buster-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:f086aaa10cbe2dfccc93401fe3cce55b2f5eed2ba1a2fe3e34a4501644f9c8fa`  
		Last Modified: Tue, 12 Oct 2021 01:20:49 GMT  
		Size: 49.1 MB (49079545 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:cdbcc5d6852f0204a44b42c53799acc0960d93e8b905a4be4ff0878db7101a34`  
		Last Modified: Tue, 12 Oct 2021 01:21:07 GMT  
		Size: 224.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-backports` - linux; ppc64le

```console
$ docker pull debian@sha256:aa5d0067e39b42752123ae39fb55fd17e69a13ddce4dbe50e9b5beb3bd3dfcc8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.2 MB (54183701 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5c7a6bcfeab2836166f1ec5e0a8a34b2afa9c897adf94e10456b80f53ce42a4b`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:26:10 GMT
ADD file:94a7157f0c578810dcc73fd2dbdcb4ce021626d9858288c970e007a590c71d44 in / 
# Tue, 12 Oct 2021 01:26:18 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:26:34 GMT
RUN echo 'deb http://deb.debian.org/debian buster-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:77e7cc3fe486cc9a5ddc4cee43979cbebb5e7c4f36b82ccaa61dbda5dd37dac8`  
		Last Modified: Tue, 12 Oct 2021 01:37:52 GMT  
		Size: 54.2 MB (54183476 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:106b9c21fa3745aac5773021005af76fce1c69d766875988e2aef07b660b814d`  
		Last Modified: Tue, 12 Oct 2021 01:38:09 GMT  
		Size: 225.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-backports` - linux; s390x

```console
$ docker pull debian@sha256:c80ce30bb6f35fb54f41e085e04c040ed61461c819e3e1efc37abb9a7eb3032f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.0 MB (49005069 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:dbc2b4826ca9a3909fa5023a43d0e660b997461b2ed25e9170741ed622d59978`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:42:39 GMT
ADD file:91e4bb81a5308737580259a9213b02933901431aa2ea23f3f4f59321a6ccc301 in / 
# Tue, 12 Oct 2021 00:42:41 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 00:42:48 GMT
RUN echo 'deb http://deb.debian.org/debian buster-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:9df790508568720a3b71c02b057e4a119d9d2e8ed003ccba18d600e1ea44fa8a`  
		Last Modified: Tue, 12 Oct 2021 00:48:22 GMT  
		Size: 49.0 MB (49004847 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:34cf9091035162137105954527ad7088a181db3c9efb679b91bc6a8189807b01`  
		Last Modified: Tue, 12 Oct 2021 00:48:32 GMT  
		Size: 222.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:buster-slim`

```console
$ docker pull debian@sha256:544c93597c784cf68dbe492ef35c00de7f4f6a990955c7144a40b20d86a3475f
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:buster-slim` - linux; amd64

```console
$ docker pull debian@sha256:f6ed7ce6e3264649e1d4f40585247c50e32faaf268984c5c5cbf0e67cf7f0ec7
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **27.1 MB (27139510 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8ae0cebc834a83dc033065409120379a215e64d4d717449a56f211c10c8f1c95`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:21:05 GMT
ADD file:910392427fdf089bc26b64d6dc450ff3d020c7c1a474d85b2f9298134d0007bd in / 
# Tue, 12 Oct 2021 01:21:05 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:b380bbd43752f83945df8b5d1074fef8dd044820e7d3aef33b655a2483e030c7`  
		Last Modified: Tue, 12 Oct 2021 01:26:51 GMT  
		Size: 27.1 MB (27139510 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:5cf40d31707d8371b9f7657b08fe5757f66b7541f0ba60a6884e3a228f3826e2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **24.9 MB (24872704 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f5327b97db05809d81919bb3751bf09f5ec0862d38c8f871f47d088bb4c13b8e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:51:32 GMT
ADD file:0d95eee31ffe3f4260263e4fac2a61123a4b98d2e7a67efcc7bfea338c54f41d in / 
# Tue, 12 Oct 2021 00:51:32 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:175b083b22fb061f7d1894fe4916a2967ce636d47376da526d90d57c8f7efc60`  
		Last Modified: Tue, 12 Oct 2021 01:07:45 GMT  
		Size: 24.9 MB (24872704 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:f865df940558a112dfac67eef50714dbd4bc2a82f6d302cd2c133649c2b4288e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **22.7 MB (22739698 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:c97f71b3175eb8ee139a3abbb2090e87dedf50b0199d0b2baa920a1e11293556`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:29:42 GMT
ADD file:fe1eaa1b1d760b3d250cb4ac331d26a261fc569f1694bd34e146f00874dc87e5 in / 
# Tue, 12 Oct 2021 01:29:43 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c0b5ba470cac8cfdae4633f5cddb3ba9350fe1d1b1507c4965a8224eb40e52c5`  
		Last Modified: Tue, 12 Oct 2021 01:45:52 GMT  
		Size: 22.7 MB (22739698 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:9f6e6f1f5a4665a552f46028808b28ab19788d28db470de6822febf710f47914
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **25.9 MB (25908479 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d5dbc8809dc0af0cdd5b012ad95108771f6957859cfe7e4ccf0695de3242db94`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:42 GMT
ADD file:f0d53027a7ba594477674127971aa477af3a2bc6bcddef6c0aa174953a5f2db0 in / 
# Tue, 12 Oct 2021 01:41:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:02b45931a436a68cadb742684148ed6aacbbf9aa21447b467c11bac97f92eb46`  
		Last Modified: Tue, 12 Oct 2021 01:49:07 GMT  
		Size: 25.9 MB (25908479 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-slim` - linux; 386

```console
$ docker pull debian@sha256:870b187824ecb90dbb968ff713e30ad950c42c6b2ddbae30559a48694b87fd77
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **27.8 MB (27791445 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d7232f6968596f6edce31c8b1a83faff4770cafddcf44639813b6ec911d827ee`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:17 GMT
ADD file:1b432d2306b487c59442b30c65ddabd08b45484c6ce09b0c25112c29f4a98f17 in / 
# Tue, 12 Oct 2021 01:40:17 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:860d012fe46ce39c3737ace24d10d8ad65ae9c78abde6891ca6e97b0d7f271dd`  
		Last Modified: Tue, 12 Oct 2021 01:48:37 GMT  
		Size: 27.8 MB (27791445 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-slim` - linux; mips64le

```console
$ docker pull debian@sha256:b642ad734ac5033873def85d8471bee90fc194a27105eb358e92f25c58c155d7
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **25.8 MB (25806535 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d6273f44d21d6b75bfcf57caf8935a94139bfd43f9fdcc44e4287d185150a495`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:12:07 GMT
ADD file:a467fbf2015350da455cad98b5f5f247d88749cf317062ca09c466c27d53f87b in / 
# Tue, 12 Oct 2021 01:12:07 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:4d8cdee9d607e77b6f8c2203cc9cc02b38004def2b7fe162538c1538da10c870`  
		Last Modified: Tue, 12 Oct 2021 01:21:36 GMT  
		Size: 25.8 MB (25806535 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-slim` - linux; ppc64le

```console
$ docker pull debian@sha256:ac88814d59305f903079944b7b647c813beceeca3e7ad7920604cd90b9dbd130
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.5 MB (30547197 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6e05e4d22ac1078c5c920a3d1b28da35d0e0c4770f9dc7fb505394229f218b43`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:26:46 GMT
ADD file:5526880c6f19a4b4231a8b2bd7cfc625d116764c4918eff6f0b55f8c1eb38e1a in / 
# Tue, 12 Oct 2021 01:26:50 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:d69973af32573c07c8789c0f9c144a5c0b822a1e85b59db3ce9f8ebc489ce85d`  
		Last Modified: Tue, 12 Oct 2021 01:38:40 GMT  
		Size: 30.5 MB (30547197 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:buster-slim` - linux; s390x

```console
$ docker pull debian@sha256:007b10cc082c948b5a45f7298e8776c5b0df77331ab3e2bf00b7ed98d85f9d8e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **25.8 MB (25754252 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4340acf96cb3e2fec8d744a74977a94566b488869b7b467c9c1571a45cc40b95`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:42:56 GMT
ADD file:39da9acd4d06d69f3ca8d25ef5c097ae741972d6b15a6a057bc7487380157b61 in / 
# Tue, 12 Oct 2021 00:42:57 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:78a640c6bb4733e5156ce97c5ec773cf97b357c3a07244348384da5060788a61`  
		Last Modified: Tue, 12 Oct 2021 00:48:41 GMT  
		Size: 25.8 MB (25754252 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:experimental`

```console
$ docker pull debian@sha256:4f1a654e8faa6d79b2bddc387086689eb723e54790043d33f368529a42db8bd3
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 9
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; riscv64
	-	linux; s390x

### `debian:experimental` - linux; amd64

```console
$ docker pull debian@sha256:45b05b03d8dcb55a91f192f2b1ba939f557cba4855126df4c0a1913ed09f300c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.7 MB (55687710 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:41362f447867d0fc309d288a43d40dfdec369d00a051a47b87eb73da692ad37c`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:23:26 GMT
ADD file:768176ccdb1053418db077c8d4e7f86c7d238d14b4d6298282277ed9167f5103 in / 
# Tue, 12 Oct 2021 01:23:27 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:23:40 GMT
RUN echo 'deb http://deb.debian.org/debian experimental main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:68135aa259351f7843ab8a4668eac588b9c494bae3a339713817d0f4bd9e6572`  
		Last Modified: Tue, 12 Oct 2021 01:30:47 GMT  
		Size: 55.7 MB (55687490 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:6f78521d0a2b6454a78768f886ba8690e18795ceed1ce1d4552e364ef901c431`  
		Last Modified: Tue, 12 Oct 2021 01:31:11 GMT  
		Size: 220.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:experimental` - linux; arm variant v5

```console
$ docker pull debian@sha256:890e93ced7aa94f08f25318df45cb109ba1c56316f25f98cf303bf5956f350b7
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53186529 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:644ccdd631618cc306856129692f304c1244ad881fb4ba1b65904920cd8d9e9f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:57:38 GMT
ADD file:142ff5a3e7c083dbe37f25d418562ca69fb261ae8301ac6704d16389cac353ff in / 
# Tue, 12 Oct 2021 00:57:40 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 00:58:13 GMT
RUN echo 'deb http://deb.debian.org/debian experimental main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:6d15c84744a3fd9ea8041501d65ab1bd82ba58fb2d2a5efae10506df977440fb`  
		Last Modified: Tue, 12 Oct 2021 01:16:31 GMT  
		Size: 53.2 MB (53186307 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:2ed5e1c306d5761f0b0feb2e6678975fef169295197aeb91fd2fa392cfebc95c`  
		Last Modified: Tue, 12 Oct 2021 01:17:14 GMT  
		Size: 222.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:experimental` - linux; arm variant v7

```console
$ docker pull debian@sha256:2cb69e8a78c323c61599a58d5d5873bf5b8960a64cc325073c1043f7219b2885
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.8 MB (50797283 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5ca5708262eda55b6cad949d42b44fdfa4d911ea1948a27d77aa519fa9f3caf3`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:36:14 GMT
ADD file:be4b9a2b55c955b91060dc950fbb28c53858174da0e76833e50d3a818c01eec0 in / 
# Tue, 12 Oct 2021 01:36:15 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:36:50 GMT
RUN echo 'deb http://deb.debian.org/debian experimental main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:c5c2a3ca9a7b031872447f9db1cf1ff22cd0711b3d2db5d88595c25ed411d976`  
		Last Modified: Tue, 12 Oct 2021 01:53:58 GMT  
		Size: 50.8 MB (50797060 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:ecf20fff3bbe8438b10b6cb41c8084d34ecca24bedc6d9358a4ffba7924b2613`  
		Last Modified: Tue, 12 Oct 2021 01:54:36 GMT  
		Size: 223.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:experimental` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:9e39f2c9a51c1d6df1daa9a6e3dc0db2ddb8384de41ab5e331e618cb2254df8b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.7 MB (54703113 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1bb92dd3759aefbb612ce3141728641dd5672ee2fc75ce0445b12c1e6c27bec4`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:44:02 GMT
ADD file:894de0be09c3231a5faac7ccddbc80c3b60408789649645b6ee1f495fa9b9658 in / 
# Tue, 12 Oct 2021 01:44:02 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:44:16 GMT
RUN echo 'deb http://deb.debian.org/debian experimental main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:f7d44b65f406086d3f6c33e702e62ccb453a15e440f7a20e605c14d292b461e3`  
		Last Modified: Tue, 12 Oct 2021 01:53:38 GMT  
		Size: 54.7 MB (54702889 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:475af9fc2fe2608137d618cfead6cca5a53d40ea0eb4a3ea3f98f8162dfb0413`  
		Last Modified: Tue, 12 Oct 2021 01:54:03 GMT  
		Size: 224.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:experimental` - linux; 386

```console
$ docker pull debian@sha256:f61d1e3e2a66d096e99db36fd490cae635487f38f405649e981fef8501cca7ba
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **56.7 MB (56716365 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:dc94beb02f59f62ea35428287c0a095aa8b57d62120d5b1069dbbd5ca00fe2ee`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:17 GMT
ADD file:3509f6fbe74f127b3a47c4cb6323c6e8e8fb46e38fe742921d35fcd0ce76d276 in / 
# Tue, 12 Oct 2021 01:43:18 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:43:34 GMT
RUN echo 'deb http://deb.debian.org/debian experimental main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:1665cd0d0f387adf8fec7c5686c084e64edc02778c6f6244db411ddc55a1f4c1`  
		Last Modified: Tue, 12 Oct 2021 01:53:43 GMT  
		Size: 56.7 MB (56716144 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:001705f4d6e77993e3e9b15bfd6e40efcc270260e3c8d997df6b1fc2f8d95aaf`  
		Last Modified: Tue, 12 Oct 2021 01:54:11 GMT  
		Size: 221.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:experimental` - linux; mips64le

```console
$ docker pull debian@sha256:be3e2a66e000738453f854a5606740eaa43e25f12ead9e3af8e64b910aefb7d7
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.3 MB (54313680 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:7ea96ff5f92d9473503a6c5ed74d1ab0dc1cc09e625c894a4129ed5b1b08635a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:16:04 GMT
ADD file:2cc438bb6fb8a3a9b0818598818b7854a43d5393ac7e53bdf871b61d3357f09e in / 
# Tue, 12 Oct 2021 01:16:05 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:16:33 GMT
RUN echo 'deb http://deb.debian.org/debian experimental main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:4f27b1c33880effa793ac87419f57ce87673a42cd455a5eccb5b14568e55ce22`  
		Last Modified: Tue, 12 Oct 2021 01:27:38 GMT  
		Size: 54.3 MB (54313458 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:ffa954ac487aed0297c5d2daa45223a07a7dd7a36a69f238c6a12578b258a88c`  
		Last Modified: Tue, 12 Oct 2021 01:28:17 GMT  
		Size: 222.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:experimental` - linux; ppc64le

```console
$ docker pull debian@sha256:4e1c40d06c866035bcfc8f015b61e52ac9ebcdeb3dec44d94589ae27b9062b90
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **59.9 MB (59889424 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e685f3cca876eaf0c3ba43bc676f3ef498127a64057841ad168063e06217da01`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:30:57 GMT
ADD file:52b1e51a9aeb65e63eb0c1a42cef5a4bc15ead2428cb8ece49b10c4fe8464216 in / 
# Tue, 12 Oct 2021 01:31:02 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:31:33 GMT
RUN echo 'deb http://deb.debian.org/debian experimental main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:5339ee5d539a0754933340b57bd16312bc7f834914e59717e20c1b83eaa8072a`  
		Last Modified: Tue, 12 Oct 2021 01:44:12 GMT  
		Size: 59.9 MB (59889202 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:5c43ea5c91b12462cce1ef6b54ea15df8f50f27a259dd443a0c54ab8d8de75ce`  
		Last Modified: Tue, 12 Oct 2021 01:44:42 GMT  
		Size: 222.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:experimental` - linux; riscv64

```console
$ docker pull debian@sha256:6fa46d48943121066c5fe9142a297eba9e9a37a34251618fe938af616b8b487f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **51.5 MB (51516846 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:2cba3e98c3bcb33cc6d4e4fedf1501cd52fe2e856ec94081c67350535a2f24b5`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:17:56 GMT
ADD file:fa84e1dde40e56bc549344dc0d3f2bf2400e5009c42f96c262489c33aaa81455 in / 
# Tue, 12 Oct 2021 01:17:59 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:21:06 GMT
RUN echo 'deb http://deb.debian.org/debian-ports experimental main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:e70c24b27e8c6ebee7852a52b1fdd44e247e6ae2e2ce6e18bed96cc9408cff2d`  
		Last Modified: Tue, 12 Oct 2021 01:33:55 GMT  
		Size: 51.5 MB (51516616 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:4be7420055369178beaa1d7bdb785056a2cd5b3b1b420f78b3064101c302b572`  
		Last Modified: Tue, 12 Oct 2021 01:36:32 GMT  
		Size: 230.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:experimental` - linux; s390x

```console
$ docker pull debian@sha256:06c4e0c44afe02e648cdd5531d0280df7023967e12e41ee3272563fa22278ab6
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.9 MB (53940915 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a8619faf1bee4a51e2100d5f39b95cbdaf1adcbe14fa14ca91b1b00a83665d4e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:44:52 GMT
ADD file:8e72740308f6ae0d026697be186652e326c16f3f37e6ab6cc86de966dfa8cab4 in / 
# Tue, 12 Oct 2021 00:44:55 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 00:45:11 GMT
RUN echo 'deb http://deb.debian.org/debian experimental main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:7d3fc62e9c56880d86636919f36507d68aacc14f85a6c05acb3ecfcfedcafb8a`  
		Last Modified: Tue, 12 Oct 2021 00:50:54 GMT  
		Size: 53.9 MB (53940695 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:64db1ba033328d40f3acc0855295bc14ce0380c1aa79808f1ecbb1dcf9b4bb90`  
		Last Modified: Tue, 12 Oct 2021 00:51:13 GMT  
		Size: 220.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:experimental-20211011`

```console
$ docker pull debian@sha256:4f1a654e8faa6d79b2bddc387086689eb723e54790043d33f368529a42db8bd3
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 9
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; riscv64
	-	linux; s390x

### `debian:experimental-20211011` - linux; amd64

```console
$ docker pull debian@sha256:45b05b03d8dcb55a91f192f2b1ba939f557cba4855126df4c0a1913ed09f300c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.7 MB (55687710 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:41362f447867d0fc309d288a43d40dfdec369d00a051a47b87eb73da692ad37c`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:23:26 GMT
ADD file:768176ccdb1053418db077c8d4e7f86c7d238d14b4d6298282277ed9167f5103 in / 
# Tue, 12 Oct 2021 01:23:27 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:23:40 GMT
RUN echo 'deb http://deb.debian.org/debian experimental main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:68135aa259351f7843ab8a4668eac588b9c494bae3a339713817d0f4bd9e6572`  
		Last Modified: Tue, 12 Oct 2021 01:30:47 GMT  
		Size: 55.7 MB (55687490 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:6f78521d0a2b6454a78768f886ba8690e18795ceed1ce1d4552e364ef901c431`  
		Last Modified: Tue, 12 Oct 2021 01:31:11 GMT  
		Size: 220.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:experimental-20211011` - linux; arm variant v5

```console
$ docker pull debian@sha256:890e93ced7aa94f08f25318df45cb109ba1c56316f25f98cf303bf5956f350b7
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53186529 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:644ccdd631618cc306856129692f304c1244ad881fb4ba1b65904920cd8d9e9f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:57:38 GMT
ADD file:142ff5a3e7c083dbe37f25d418562ca69fb261ae8301ac6704d16389cac353ff in / 
# Tue, 12 Oct 2021 00:57:40 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 00:58:13 GMT
RUN echo 'deb http://deb.debian.org/debian experimental main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:6d15c84744a3fd9ea8041501d65ab1bd82ba58fb2d2a5efae10506df977440fb`  
		Last Modified: Tue, 12 Oct 2021 01:16:31 GMT  
		Size: 53.2 MB (53186307 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:2ed5e1c306d5761f0b0feb2e6678975fef169295197aeb91fd2fa392cfebc95c`  
		Last Modified: Tue, 12 Oct 2021 01:17:14 GMT  
		Size: 222.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:experimental-20211011` - linux; arm variant v7

```console
$ docker pull debian@sha256:2cb69e8a78c323c61599a58d5d5873bf5b8960a64cc325073c1043f7219b2885
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.8 MB (50797283 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5ca5708262eda55b6cad949d42b44fdfa4d911ea1948a27d77aa519fa9f3caf3`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:36:14 GMT
ADD file:be4b9a2b55c955b91060dc950fbb28c53858174da0e76833e50d3a818c01eec0 in / 
# Tue, 12 Oct 2021 01:36:15 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:36:50 GMT
RUN echo 'deb http://deb.debian.org/debian experimental main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:c5c2a3ca9a7b031872447f9db1cf1ff22cd0711b3d2db5d88595c25ed411d976`  
		Last Modified: Tue, 12 Oct 2021 01:53:58 GMT  
		Size: 50.8 MB (50797060 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:ecf20fff3bbe8438b10b6cb41c8084d34ecca24bedc6d9358a4ffba7924b2613`  
		Last Modified: Tue, 12 Oct 2021 01:54:36 GMT  
		Size: 223.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:experimental-20211011` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:9e39f2c9a51c1d6df1daa9a6e3dc0db2ddb8384de41ab5e331e618cb2254df8b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.7 MB (54703113 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1bb92dd3759aefbb612ce3141728641dd5672ee2fc75ce0445b12c1e6c27bec4`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:44:02 GMT
ADD file:894de0be09c3231a5faac7ccddbc80c3b60408789649645b6ee1f495fa9b9658 in / 
# Tue, 12 Oct 2021 01:44:02 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:44:16 GMT
RUN echo 'deb http://deb.debian.org/debian experimental main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:f7d44b65f406086d3f6c33e702e62ccb453a15e440f7a20e605c14d292b461e3`  
		Last Modified: Tue, 12 Oct 2021 01:53:38 GMT  
		Size: 54.7 MB (54702889 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:475af9fc2fe2608137d618cfead6cca5a53d40ea0eb4a3ea3f98f8162dfb0413`  
		Last Modified: Tue, 12 Oct 2021 01:54:03 GMT  
		Size: 224.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:experimental-20211011` - linux; 386

```console
$ docker pull debian@sha256:f61d1e3e2a66d096e99db36fd490cae635487f38f405649e981fef8501cca7ba
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **56.7 MB (56716365 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:dc94beb02f59f62ea35428287c0a095aa8b57d62120d5b1069dbbd5ca00fe2ee`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:17 GMT
ADD file:3509f6fbe74f127b3a47c4cb6323c6e8e8fb46e38fe742921d35fcd0ce76d276 in / 
# Tue, 12 Oct 2021 01:43:18 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:43:34 GMT
RUN echo 'deb http://deb.debian.org/debian experimental main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:1665cd0d0f387adf8fec7c5686c084e64edc02778c6f6244db411ddc55a1f4c1`  
		Last Modified: Tue, 12 Oct 2021 01:53:43 GMT  
		Size: 56.7 MB (56716144 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:001705f4d6e77993e3e9b15bfd6e40efcc270260e3c8d997df6b1fc2f8d95aaf`  
		Last Modified: Tue, 12 Oct 2021 01:54:11 GMT  
		Size: 221.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:experimental-20211011` - linux; mips64le

```console
$ docker pull debian@sha256:be3e2a66e000738453f854a5606740eaa43e25f12ead9e3af8e64b910aefb7d7
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.3 MB (54313680 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:7ea96ff5f92d9473503a6c5ed74d1ab0dc1cc09e625c894a4129ed5b1b08635a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:16:04 GMT
ADD file:2cc438bb6fb8a3a9b0818598818b7854a43d5393ac7e53bdf871b61d3357f09e in / 
# Tue, 12 Oct 2021 01:16:05 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:16:33 GMT
RUN echo 'deb http://deb.debian.org/debian experimental main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:4f27b1c33880effa793ac87419f57ce87673a42cd455a5eccb5b14568e55ce22`  
		Last Modified: Tue, 12 Oct 2021 01:27:38 GMT  
		Size: 54.3 MB (54313458 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:ffa954ac487aed0297c5d2daa45223a07a7dd7a36a69f238c6a12578b258a88c`  
		Last Modified: Tue, 12 Oct 2021 01:28:17 GMT  
		Size: 222.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:experimental-20211011` - linux; ppc64le

```console
$ docker pull debian@sha256:4e1c40d06c866035bcfc8f015b61e52ac9ebcdeb3dec44d94589ae27b9062b90
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **59.9 MB (59889424 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e685f3cca876eaf0c3ba43bc676f3ef498127a64057841ad168063e06217da01`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:30:57 GMT
ADD file:52b1e51a9aeb65e63eb0c1a42cef5a4bc15ead2428cb8ece49b10c4fe8464216 in / 
# Tue, 12 Oct 2021 01:31:02 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:31:33 GMT
RUN echo 'deb http://deb.debian.org/debian experimental main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:5339ee5d539a0754933340b57bd16312bc7f834914e59717e20c1b83eaa8072a`  
		Last Modified: Tue, 12 Oct 2021 01:44:12 GMT  
		Size: 59.9 MB (59889202 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:5c43ea5c91b12462cce1ef6b54ea15df8f50f27a259dd443a0c54ab8d8de75ce`  
		Last Modified: Tue, 12 Oct 2021 01:44:42 GMT  
		Size: 222.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:experimental-20211011` - linux; riscv64

```console
$ docker pull debian@sha256:6fa46d48943121066c5fe9142a297eba9e9a37a34251618fe938af616b8b487f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **51.5 MB (51516846 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:2cba3e98c3bcb33cc6d4e4fedf1501cd52fe2e856ec94081c67350535a2f24b5`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:17:56 GMT
ADD file:fa84e1dde40e56bc549344dc0d3f2bf2400e5009c42f96c262489c33aaa81455 in / 
# Tue, 12 Oct 2021 01:17:59 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:21:06 GMT
RUN echo 'deb http://deb.debian.org/debian-ports experimental main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:e70c24b27e8c6ebee7852a52b1fdd44e247e6ae2e2ce6e18bed96cc9408cff2d`  
		Last Modified: Tue, 12 Oct 2021 01:33:55 GMT  
		Size: 51.5 MB (51516616 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:4be7420055369178beaa1d7bdb785056a2cd5b3b1b420f78b3064101c302b572`  
		Last Modified: Tue, 12 Oct 2021 01:36:32 GMT  
		Size: 230.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:experimental-20211011` - linux; s390x

```console
$ docker pull debian@sha256:06c4e0c44afe02e648cdd5531d0280df7023967e12e41ee3272563fa22278ab6
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.9 MB (53940915 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a8619faf1bee4a51e2100d5f39b95cbdaf1adcbe14fa14ca91b1b00a83665d4e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:44:52 GMT
ADD file:8e72740308f6ae0d026697be186652e326c16f3f37e6ab6cc86de966dfa8cab4 in / 
# Tue, 12 Oct 2021 00:44:55 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 00:45:11 GMT
RUN echo 'deb http://deb.debian.org/debian experimental main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:7d3fc62e9c56880d86636919f36507d68aacc14f85a6c05acb3ecfcfedcafb8a`  
		Last Modified: Tue, 12 Oct 2021 00:50:54 GMT  
		Size: 53.9 MB (53940695 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:64db1ba033328d40f3acc0855295bc14ce0380c1aa79808f1ecbb1dcf9b4bb90`  
		Last Modified: Tue, 12 Oct 2021 00:51:13 GMT  
		Size: 220.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:latest`

```console
$ docker pull debian@sha256:4d6ab716de467aad58e91b1b720f0badd7478847ec7a18f66027d0f8a329a43c
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:latest` - linux; amd64

```console
$ docker pull debian@sha256:826d8850098b954c3d67c2e45ea7a08fa681c128126ba01b35bd2689b1bb2e04
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.9 MB (54917520 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f776cfb21b5e06bb5b4883eb15c09ab928a411476b8275293c7f96d09b90f7f9`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:20:30 GMT
ADD file:aea313ae50ce6474a3df142b34d4dcba4e7e0186ea6fe55389cb2ea903b9ebbb in / 
# Tue, 12 Oct 2021 01:20:30 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:bb7d5a84853b217ac05783963f12b034243070c1c9c8d2e60ada47444f3cce04`  
		Last Modified: Tue, 12 Oct 2021 01:25:37 GMT  
		Size: 54.9 MB (54917520 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:latest` - linux; arm variant v5

```console
$ docker pull debian@sha256:4b164f1a9efee849da43191cf8316ac35e7ea1bc9c1b9c73f6c4dbcfa1296c8b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **52.5 MB (52452198 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ba60f6fec7f0faafeb0679c8e0a1df7c55eda14bed43d9e5f561d1fa50a0db76`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:49:54 GMT
ADD file:b9969604315238de271e1769a4b8dd85cdf0d33f9fac387e940d5195ac3030a1 in / 
# Tue, 12 Oct 2021 00:49:55 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:a97c782f2538791984ce12c859cada96e395e3950d4cb7d1238e08acd2eb74ec`  
		Last Modified: Tue, 12 Oct 2021 01:05:12 GMT  
		Size: 52.5 MB (52452198 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:latest` - linux; arm variant v7

```console
$ docker pull debian@sha256:580d9d9b46eb13e047b78c71a43d9f61521995def78d31586f6ab770c34d47cd
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.1 MB (50118673 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5aeb2ea51c3d45f85b7b225916cea627d677cad4371093aaea94f9a251e6cafb`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:27:59 GMT
ADD file:d9f40ca54fb79741d1e76c7bca9250f78453d0b39fb6c921337f3071a79a55a9 in / 
# Tue, 12 Oct 2021 01:28:00 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:88ce59c3081c4041f41eb407032db40c207cc2d806ba0eb12e170b697db91e52`  
		Last Modified: Tue, 12 Oct 2021 01:43:42 GMT  
		Size: 50.1 MB (50118673 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:latest` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:21e79e173393a5c03e2dd910354affc1e6ea995aa3c88a18d40bae41760b3d4e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.6 MB (53603015 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:bf33d9a9dfb709a9a24218f6ea55bab3965d0029cdc2ce01214ad1a87040e530`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:04 GMT
ADD file:1529ae12e334fd992892d3fb97c103297cff7e0115b0475bec4c093939a2bff7 in / 
# Tue, 12 Oct 2021 01:41:04 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:1c47a423366578e5ce665d03788914bf0459485a627a27896fa9c5663ce55cdf`  
		Last Modified: Tue, 12 Oct 2021 01:47:41 GMT  
		Size: 53.6 MB (53603015 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:latest` - linux; 386

```console
$ docker pull debian@sha256:7448716003c940a5adbf4ddedf0486ecdfeb18fbcd8f13c700886ba0d9b82318
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.9 MB (55923419 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b76357513741b0e14fe9ca5c1aa46fe6f4c5e853ff0b0fc03ac27e798ca40c59`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:39:30 GMT
ADD file:e007d6eff02dbc696e1166db315b97a77ae8aa65f2e66ca8074765dde9c70a59 in / 
# Tue, 12 Oct 2021 01:39:31 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6911c8f66691266d86e2cafa67ca005d919f357436fac971c694d37625b5ba90`  
		Last Modified: Tue, 12 Oct 2021 01:47:06 GMT  
		Size: 55.9 MB (55923419 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:latest` - linux; mips64le

```console
$ docker pull debian@sha256:9d1807f24103e235abbc8df2d946332daad7f82f1a650b5abe0ffcd4af864aec
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53169836 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:18cdbedcbd6db4f83257f4056d6486eb20919f55f29ceb40d247c93b050903ec`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:10:49 GMT
ADD file:856952232716a2e2d5069387a06a537937a1dec1bb75bc9519c60d6ad226bddb in / 
# Tue, 12 Oct 2021 01:10:50 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:3d2cd78b9c44ce4eadbb0cc35b884bfd0dac51123f3c8c6645cb8dc9dc9c1512`  
		Last Modified: Tue, 12 Oct 2021 01:19:18 GMT  
		Size: 53.2 MB (53169836 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:latest` - linux; ppc64le

```console
$ docker pull debian@sha256:6e37f5053e41a23ff8c716c87bd1ba185e732979f0f4f26581842bafe5ae2790
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **58.8 MB (58808876 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ec0ee09301b7955b4289ddd8912742ea5c8473147ff2f55955cade3d9962c343`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:25:04 GMT
ADD file:127b21494e1c790bdbb4c277638b3e16b25fbbcd1e0cdb52e60a4f55a57cd0f2 in / 
# Tue, 12 Oct 2021 01:25:10 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c7e7ddcf3b3abfed2b7aab50c1b68bfdfc6fcffed2f4b93ebff4d6050b3d5e72`  
		Last Modified: Tue, 12 Oct 2021 01:36:12 GMT  
		Size: 58.8 MB (58808876 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:latest` - linux; s390x

```console
$ docker pull debian@sha256:853435582c5e3dd9695a0748116a6dc3bb09a6d8ead73bfc70d80a8aacc686f5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53192895 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:0ab94af22e7333d274f0aae62c0d3bcd833fa3f70ed1618bdb62eca5b57e7495`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:42:09 GMT
ADD file:a7a74f6be757ceb5e84c440e739019782df1a118264eef70aa49886a976c43f6 in / 
# Tue, 12 Oct 2021 00:42:12 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:081299fd984cd296112766e1cc55f44fffbf898b2900b01c0333962494a2dd80`  
		Last Modified: Tue, 12 Oct 2021 00:47:43 GMT  
		Size: 53.2 MB (53192895 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:oldoldstable`

```console
$ docker pull debian@sha256:c07e3fd22cde46953b2a1070bf64f7284f93d5f318bb37209d4560e92453ae30
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386

### `debian:oldoldstable` - linux; amd64

```console
$ docker pull debian@sha256:f00f6414af2edd4bad1b71932a57246d4e6757899d8cb44dd4269918fa6929e3
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **45.4 MB (45379650 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:389bf269f1c37e68845217ff046c6242037fa0ad2128ffe343c765db350c3608`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:21:15 GMT
ADD file:2dad8aa47b7c0abdcaccbf1c501793fd733f172a30177cc5a5906a7e8c94163d in / 
# Tue, 12 Oct 2021 01:21:15 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:a8b1a171ce712e7503b1f72abb7b9751bff88e01e4413539244c27b04b96af58`  
		Last Modified: Tue, 12 Oct 2021 01:27:11 GMT  
		Size: 45.4 MB (45379650 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable` - linux; arm variant v5

```console
$ docker pull debian@sha256:1aa499985f779e93df7634f379878fe5576ad3178529a00cd6ae38b4b4877b0a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **44.1 MB (44091934 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:061ddcb939a4b79e2dd4143ee731737459f9a4536fdd99bac41a0ba30a9b849d`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:51:59 GMT
ADD file:562359b7dd2e2aded9f637c664877f9d2a7e1a5ca24b5b0f87032c0c6f63ff05 in / 
# Tue, 12 Oct 2021 00:52:00 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:65b249c69eb3aa72e4d5fc5c9152f2138252bc14d2e8245381d9860d13f3e4fe`  
		Last Modified: Tue, 12 Oct 2021 01:08:32 GMT  
		Size: 44.1 MB (44091934 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable` - linux; arm variant v7

```console
$ docker pull debian@sha256:b7412ba79e69ab9e0ea3d0e1db866a274626cc4572d71f5d42758b5dc5004824
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **42.1 MB (42119365 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1f419b7c1027b9396d13d7390a62b30cbb6a60e33ce0f4427f9378648651e686`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:30:12 GMT
ADD file:5ea5714223f1698d41da52f17d7b6936e82f8e1f41f19e00b5e300b8f329d58b in / 
# Tue, 12 Oct 2021 01:30:13 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:ee5cec6429f90405fa16db25cfe4f7bf7d03099e0372790dc8f0a457d9d0a244`  
		Last Modified: Tue, 12 Oct 2021 01:46:47 GMT  
		Size: 42.1 MB (42119365 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:650a826b7c505f481cf2a727a2df43abaadf0888c7a8d8ea923b968147b29978
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **43.2 MB (43176728 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4ccb9b25c45fd91b23aa7b3f6af466bc9653807a9fb5d5aa7dca358630049503`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:52 GMT
ADD file:82529472573ae109efa01a1b422a0c016903bc71b937668f7c4ee238c356afe3 in / 
# Tue, 12 Oct 2021 01:41:53 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:1682a7bcf61dffda8aa42e61da61e3c02fbfdd938351384f3ff623d8d6465b64`  
		Last Modified: Tue, 12 Oct 2021 01:49:30 GMT  
		Size: 43.2 MB (43176728 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable` - linux; 386

```console
$ docker pull debian@sha256:87fe9e37f9f64ba508836c9a147faed99a465780fa80a20e9f3244120c83c3c3
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **46.1 MB (46097211 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e6f0b084c62b69ca5987572d23cdb6f4353fef1f59fce9405c1520a8e3737b75`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:30 GMT
ADD file:4fcadf4f144c0e7b2f9e6cd377b183799f017ea7ec585a314d6deaec3ca13a6c in / 
# Tue, 12 Oct 2021 01:40:31 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:5fcdd168800cca93cd361730f88c052b1ec6ddfced4c88f830d939fb2d6c8f3b`  
		Last Modified: Tue, 12 Oct 2021 01:49:04 GMT  
		Size: 46.1 MB (46097211 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:oldoldstable-20211011`

```console
$ docker pull debian@sha256:c07e3fd22cde46953b2a1070bf64f7284f93d5f318bb37209d4560e92453ae30
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386

### `debian:oldoldstable-20211011` - linux; amd64

```console
$ docker pull debian@sha256:f00f6414af2edd4bad1b71932a57246d4e6757899d8cb44dd4269918fa6929e3
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **45.4 MB (45379650 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:389bf269f1c37e68845217ff046c6242037fa0ad2128ffe343c765db350c3608`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:21:15 GMT
ADD file:2dad8aa47b7c0abdcaccbf1c501793fd733f172a30177cc5a5906a7e8c94163d in / 
# Tue, 12 Oct 2021 01:21:15 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:a8b1a171ce712e7503b1f72abb7b9751bff88e01e4413539244c27b04b96af58`  
		Last Modified: Tue, 12 Oct 2021 01:27:11 GMT  
		Size: 45.4 MB (45379650 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable-20211011` - linux; arm variant v5

```console
$ docker pull debian@sha256:1aa499985f779e93df7634f379878fe5576ad3178529a00cd6ae38b4b4877b0a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **44.1 MB (44091934 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:061ddcb939a4b79e2dd4143ee731737459f9a4536fdd99bac41a0ba30a9b849d`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:51:59 GMT
ADD file:562359b7dd2e2aded9f637c664877f9d2a7e1a5ca24b5b0f87032c0c6f63ff05 in / 
# Tue, 12 Oct 2021 00:52:00 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:65b249c69eb3aa72e4d5fc5c9152f2138252bc14d2e8245381d9860d13f3e4fe`  
		Last Modified: Tue, 12 Oct 2021 01:08:32 GMT  
		Size: 44.1 MB (44091934 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable-20211011` - linux; arm variant v7

```console
$ docker pull debian@sha256:b7412ba79e69ab9e0ea3d0e1db866a274626cc4572d71f5d42758b5dc5004824
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **42.1 MB (42119365 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1f419b7c1027b9396d13d7390a62b30cbb6a60e33ce0f4427f9378648651e686`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:30:12 GMT
ADD file:5ea5714223f1698d41da52f17d7b6936e82f8e1f41f19e00b5e300b8f329d58b in / 
# Tue, 12 Oct 2021 01:30:13 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:ee5cec6429f90405fa16db25cfe4f7bf7d03099e0372790dc8f0a457d9d0a244`  
		Last Modified: Tue, 12 Oct 2021 01:46:47 GMT  
		Size: 42.1 MB (42119365 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable-20211011` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:650a826b7c505f481cf2a727a2df43abaadf0888c7a8d8ea923b968147b29978
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **43.2 MB (43176728 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4ccb9b25c45fd91b23aa7b3f6af466bc9653807a9fb5d5aa7dca358630049503`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:52 GMT
ADD file:82529472573ae109efa01a1b422a0c016903bc71b937668f7c4ee238c356afe3 in / 
# Tue, 12 Oct 2021 01:41:53 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:1682a7bcf61dffda8aa42e61da61e3c02fbfdd938351384f3ff623d8d6465b64`  
		Last Modified: Tue, 12 Oct 2021 01:49:30 GMT  
		Size: 43.2 MB (43176728 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable-20211011` - linux; 386

```console
$ docker pull debian@sha256:87fe9e37f9f64ba508836c9a147faed99a465780fa80a20e9f3244120c83c3c3
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **46.1 MB (46097211 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e6f0b084c62b69ca5987572d23cdb6f4353fef1f59fce9405c1520a8e3737b75`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:30 GMT
ADD file:4fcadf4f144c0e7b2f9e6cd377b183799f017ea7ec585a314d6deaec3ca13a6c in / 
# Tue, 12 Oct 2021 01:40:31 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:5fcdd168800cca93cd361730f88c052b1ec6ddfced4c88f830d939fb2d6c8f3b`  
		Last Modified: Tue, 12 Oct 2021 01:49:04 GMT  
		Size: 46.1 MB (46097211 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:oldoldstable-20211011-slim`

```console
$ docker pull debian@sha256:26e0a9d420a9e06ab03cc7a69743fbfd888fc2af159f1837a0e94729d9d329e2
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386

### `debian:oldoldstable-20211011-slim` - linux; amd64

```console
$ docker pull debian@sha256:5d1e4df796f8ee15cbfd5e46932af78c0d351c6053e44c17c0c9f95acdc67f68
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **22.5 MB (22527518 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d852065264ac8a67bdb251ec797a82b7c9fac214c893ee302aced55997ef22ce`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:21:26 GMT
ADD file:6f0d65d34e99acc0a838582bb0c52c8b12eadb39798d231caf702a8f3a9f4de2 in / 
# Tue, 12 Oct 2021 01:21:26 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:1599ff3b20b129431eddbefc2dc08a6a0aa8ef68faf34d44eea8c9cfdf5eb921`  
		Last Modified: Tue, 12 Oct 2021 01:27:30 GMT  
		Size: 22.5 MB (22527518 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable-20211011-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:1c1201bdfd440e4c7f9ae2c594825f3f62e4f4485eae81b122f22424d745006f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **21.2 MB (21204338 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9138868e8021053dbf3833b593e719e5132d5539ea38441c9c7fabad87a2bbc3`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:52:30 GMT
ADD file:287e595e03955546f25eb897aa7553ee626ae6c6b64b8164d5d3b42297641edc in / 
# Tue, 12 Oct 2021 00:52:31 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:cee58a8ba33bca4d50453537ed50c73bed23501940fc2a62da2ac84e166732e7`  
		Last Modified: Tue, 12 Oct 2021 01:09:04 GMT  
		Size: 21.2 MB (21204338 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable-20211011-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:cba44244e91c243cd754845d3b910882c83f8c6324c8d4c4a3505840f5f8145d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **19.3 MB (19316443 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:825f907336d8205e7b0130eabb99133cde40dbaf9ca278d973318836dda78f46`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:30:44 GMT
ADD file:dfe14f5069e960939daf937d576bddf08c2dd9386730c01bc365ac610b035f71 in / 
# Tue, 12 Oct 2021 01:30:45 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:dc641641f7cb36d80035d98c2c9fe584419cdca7cf5ded32111b25f4945753fe`  
		Last Modified: Tue, 12 Oct 2021 01:47:18 GMT  
		Size: 19.3 MB (19316443 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable-20211011-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:07f91a5d79ab9d7d492307d76b77db6e241d5f830bd0d6323f7a03cf2add0608
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **20.4 MB (20389428 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e9e8adb4c56e2f66a90d67602595080738aeb7f949a6ed4881f2fd101fc69e5e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:04 GMT
ADD file:5d456d87f1a04e598f5e60aff2dfe22e2c5e218cc255dd5383e4a29659c30471 in / 
# Tue, 12 Oct 2021 01:42:05 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:51a573003c28d47d7b57a5fef8f423cf0a6d3c7f1f7fd8b538b4ae893fa860d4`  
		Last Modified: Tue, 12 Oct 2021 01:49:52 GMT  
		Size: 20.4 MB (20389428 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable-20211011-slim` - linux; 386

```console
$ docker pull debian@sha256:4f4f5898707a9955c71cb072fa46effc42556fb4829ed1f2ff71758df4d8213e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **23.2 MB (23156644 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e2078108904ce9e8978355229f5e5c709403d783d7cf4c34adbc74354722e644`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:45 GMT
ADD file:0c0934e7f8d30895fa74e47b73ad9064299cbf54de898ee430dbcde8b3a2d130 in / 
# Tue, 12 Oct 2021 01:40:45 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:0ff128fa75c485cac31b556b81d71c790608f3404c66504c40ac9940109223bb`  
		Last Modified: Tue, 12 Oct 2021 01:49:26 GMT  
		Size: 23.2 MB (23156644 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:oldoldstable-backports`

```console
$ docker pull debian@sha256:2bf0e1838199188582790c3db41c7a5c282546b209ffae0cc8a507f6ed6ac93d
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386

### `debian:oldoldstable-backports` - linux; amd64

```console
$ docker pull debian@sha256:d7bbc327d978d1fbdb680cace784a5af91f829381e2ceaa8b122fbb2d9a3404c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **45.4 MB (45379877 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9367c4a8363f79fac39d0630280e18ed848a3b149b404cc8402a2aed909efc43`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:21:15 GMT
ADD file:2dad8aa47b7c0abdcaccbf1c501793fd733f172a30177cc5a5906a7e8c94163d in / 
# Tue, 12 Oct 2021 01:21:15 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:21:18 GMT
RUN echo 'deb http://deb.debian.org/debian oldoldstable-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:a8b1a171ce712e7503b1f72abb7b9751bff88e01e4413539244c27b04b96af58`  
		Last Modified: Tue, 12 Oct 2021 01:27:11 GMT  
		Size: 45.4 MB (45379650 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:6d7255bd0b84533d575c620d180348c49ca21fc2e1a349eae841a239993cd4a4`  
		Last Modified: Tue, 12 Oct 2021 01:27:20 GMT  
		Size: 227.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable-backports` - linux; arm variant v5

```console
$ docker pull debian@sha256:44d7e3e3784429c105ccd6c64f9229c7c86840f2b150c303f9a5cf5ef7af7d9b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **44.1 MB (44092162 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:bf26e4cceede1e07c759dd5b9762e61798ff82db0e24d58e238d70d9a69b4d73`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:51:59 GMT
ADD file:562359b7dd2e2aded9f637c664877f9d2a7e1a5ca24b5b0f87032c0c6f63ff05 in / 
# Tue, 12 Oct 2021 00:52:00 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 00:52:13 GMT
RUN echo 'deb http://deb.debian.org/debian oldoldstable-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:65b249c69eb3aa72e4d5fc5c9152f2138252bc14d2e8245381d9860d13f3e4fe`  
		Last Modified: Tue, 12 Oct 2021 01:08:32 GMT  
		Size: 44.1 MB (44091934 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:796f25118818d6c99e7a3375e61f0f3d3c2d6db97af4b4dcce6bfa2cdfb526d2`  
		Last Modified: Tue, 12 Oct 2021 01:08:44 GMT  
		Size: 228.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable-backports` - linux; arm variant v7

```console
$ docker pull debian@sha256:634d7edda7c9c0538ba9fcbfb9a149befe77d39a48b86300f799ed0f33de10ab
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **42.1 MB (42119594 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:2c4c1426e5f97be9161dd9e3e3bed7079f9f7f4d41b6ca168573c42dd518a691`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:30:12 GMT
ADD file:5ea5714223f1698d41da52f17d7b6936e82f8e1f41f19e00b5e300b8f329d58b in / 
# Tue, 12 Oct 2021 01:30:13 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:30:25 GMT
RUN echo 'deb http://deb.debian.org/debian oldoldstable-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:ee5cec6429f90405fa16db25cfe4f7bf7d03099e0372790dc8f0a457d9d0a244`  
		Last Modified: Tue, 12 Oct 2021 01:46:47 GMT  
		Size: 42.1 MB (42119365 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:5eb86b790eca2e063d7cbaa9e5c9c1023e66ea32b0b7b5201679667260ae4ff9`  
		Last Modified: Tue, 12 Oct 2021 01:46:58 GMT  
		Size: 229.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable-backports` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:ca7a14702c0a5f89e06b3dc64904dd27a835fa2311945fad9dd2cb1f7ba87da9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **43.2 MB (43176955 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:bd1ead9ec72fa33e82cceabd5eff8953614a80ba297eb3d2b7f92985d37be182`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:52 GMT
ADD file:82529472573ae109efa01a1b422a0c016903bc71b937668f7c4ee238c356afe3 in / 
# Tue, 12 Oct 2021 01:41:53 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:41:58 GMT
RUN echo 'deb http://deb.debian.org/debian oldoldstable-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:1682a7bcf61dffda8aa42e61da61e3c02fbfdd938351384f3ff623d8d6465b64`  
		Last Modified: Tue, 12 Oct 2021 01:49:30 GMT  
		Size: 43.2 MB (43176728 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:b8ddaf2b98c1bd4bd5d39d4021a096879c08483dcbf1d16525b549f4b945b160`  
		Last Modified: Tue, 12 Oct 2021 01:49:41 GMT  
		Size: 227.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable-backports` - linux; 386

```console
$ docker pull debian@sha256:77345914a052130fc478b10e5258a752cab94ffc309abdb913d3f8eea59da565
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **46.1 MB (46097437 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:0810587cba56246e8d02c52acc2082f8aae1e2df5ae11f20bcbc8f039fdc949c`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:30 GMT
ADD file:4fcadf4f144c0e7b2f9e6cd377b183799f017ea7ec585a314d6deaec3ca13a6c in / 
# Tue, 12 Oct 2021 01:40:31 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:40:36 GMT
RUN echo 'deb http://deb.debian.org/debian oldoldstable-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:5fcdd168800cca93cd361730f88c052b1ec6ddfced4c88f830d939fb2d6c8f3b`  
		Last Modified: Tue, 12 Oct 2021 01:49:04 GMT  
		Size: 46.1 MB (46097211 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:1b0b98772c895bb9d7bf4ce6953ba50207c7d202b427517cf09efb4c78376c45`  
		Last Modified: Tue, 12 Oct 2021 01:49:15 GMT  
		Size: 226.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:oldoldstable-slim`

```console
$ docker pull debian@sha256:26e0a9d420a9e06ab03cc7a69743fbfd888fc2af159f1837a0e94729d9d329e2
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386

### `debian:oldoldstable-slim` - linux; amd64

```console
$ docker pull debian@sha256:5d1e4df796f8ee15cbfd5e46932af78c0d351c6053e44c17c0c9f95acdc67f68
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **22.5 MB (22527518 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d852065264ac8a67bdb251ec797a82b7c9fac214c893ee302aced55997ef22ce`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:21:26 GMT
ADD file:6f0d65d34e99acc0a838582bb0c52c8b12eadb39798d231caf702a8f3a9f4de2 in / 
# Tue, 12 Oct 2021 01:21:26 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:1599ff3b20b129431eddbefc2dc08a6a0aa8ef68faf34d44eea8c9cfdf5eb921`  
		Last Modified: Tue, 12 Oct 2021 01:27:30 GMT  
		Size: 22.5 MB (22527518 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:1c1201bdfd440e4c7f9ae2c594825f3f62e4f4485eae81b122f22424d745006f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **21.2 MB (21204338 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9138868e8021053dbf3833b593e719e5132d5539ea38441c9c7fabad87a2bbc3`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:52:30 GMT
ADD file:287e595e03955546f25eb897aa7553ee626ae6c6b64b8164d5d3b42297641edc in / 
# Tue, 12 Oct 2021 00:52:31 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:cee58a8ba33bca4d50453537ed50c73bed23501940fc2a62da2ac84e166732e7`  
		Last Modified: Tue, 12 Oct 2021 01:09:04 GMT  
		Size: 21.2 MB (21204338 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:cba44244e91c243cd754845d3b910882c83f8c6324c8d4c4a3505840f5f8145d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **19.3 MB (19316443 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:825f907336d8205e7b0130eabb99133cde40dbaf9ca278d973318836dda78f46`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:30:44 GMT
ADD file:dfe14f5069e960939daf937d576bddf08c2dd9386730c01bc365ac610b035f71 in / 
# Tue, 12 Oct 2021 01:30:45 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:dc641641f7cb36d80035d98c2c9fe584419cdca7cf5ded32111b25f4945753fe`  
		Last Modified: Tue, 12 Oct 2021 01:47:18 GMT  
		Size: 19.3 MB (19316443 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:07f91a5d79ab9d7d492307d76b77db6e241d5f830bd0d6323f7a03cf2add0608
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **20.4 MB (20389428 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e9e8adb4c56e2f66a90d67602595080738aeb7f949a6ed4881f2fd101fc69e5e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:04 GMT
ADD file:5d456d87f1a04e598f5e60aff2dfe22e2c5e218cc255dd5383e4a29659c30471 in / 
# Tue, 12 Oct 2021 01:42:05 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:51a573003c28d47d7b57a5fef8f423cf0a6d3c7f1f7fd8b538b4ae893fa860d4`  
		Last Modified: Tue, 12 Oct 2021 01:49:52 GMT  
		Size: 20.4 MB (20389428 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldoldstable-slim` - linux; 386

```console
$ docker pull debian@sha256:4f4f5898707a9955c71cb072fa46effc42556fb4829ed1f2ff71758df4d8213e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **23.2 MB (23156644 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e2078108904ce9e8978355229f5e5c709403d783d7cf4c34adbc74354722e644`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:45 GMT
ADD file:0c0934e7f8d30895fa74e47b73ad9064299cbf54de898ee430dbcde8b3a2d130 in / 
# Tue, 12 Oct 2021 01:40:45 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:0ff128fa75c485cac31b556b81d71c790608f3404c66504c40ac9940109223bb`  
		Last Modified: Tue, 12 Oct 2021 01:49:26 GMT  
		Size: 23.2 MB (23156644 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:oldstable`

```console
$ docker pull debian@sha256:843fdcc419eba12e43deff8b955d3b06f7a4296ddc8916b1fed048446c0356d2
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:oldstable` - linux; amd64

```console
$ docker pull debian@sha256:66b1a790407e21eab0f1c3cd8a5aacadcc2b70c0c865f87976cd6221eb64db2b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.4 MB (50436730 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:dc4441f19678c8bf67f5183b81e95491594f4154a51a84638eb939d799ec40b6`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:21:36 GMT
ADD file:1254fe58a557c9bd7a44702af996bc01a041007fa31c4bfd0674a7437f83e226 in / 
# Tue, 12 Oct 2021 01:21:36 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:486fdf79d93406106977e297ffa5f755ab39b223d63b7cf84a14c0ff8f62a9d2`  
		Last Modified: Tue, 12 Oct 2021 01:27:45 GMT  
		Size: 50.4 MB (50436730 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable` - linux; arm variant v5

```console
$ docker pull debian@sha256:173b47ac2d6db7999ce60877b7487600566ae307f2ab38d9953cdfd74590d27a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **48.2 MB (48154060 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:88ece4f04cfb212a3f78cac2c0cbd34357ebea607d6260d6c7c1daec40c25d44`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:52:56 GMT
ADD file:a042a1b4d39d508b4725145149caf6615d44e43427b4566e3839840e9dec939e in / 
# Tue, 12 Oct 2021 00:52:57 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:e187af41423780e5cc95f89de4b56a3c18a2c5636f1919be23b07449d54381a7`  
		Last Modified: Tue, 12 Oct 2021 01:09:44 GMT  
		Size: 48.2 MB (48154060 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable` - linux; arm variant v7

```console
$ docker pull debian@sha256:f81dd238be9034087264afc0dead1422c67347b03c4aa730e2e86337f6e18ce9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **45.9 MB (45917917 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:297db7d35883781b4abf5c1c520ad049e705249fba2d259ded490c9692c2a54b`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:31:12 GMT
ADD file:075763af19fd2f62d5199e9f9ece478e2602aee33c939ccd258f599d88469046 in / 
# Tue, 12 Oct 2021 01:31:13 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:e1e8a087bec2ff439032f91ace1191030ed684bf5807987913cb1d995565a032`  
		Last Modified: Tue, 12 Oct 2021 01:47:54 GMT  
		Size: 45.9 MB (45917917 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:ed3651fcf95b5fc5e1cba1b3de53e0501d2682a0978e0b800ec489ae6dd565aa
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.2 MB (49222718 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a6dcb8339dd72e9a03e5a41eb75f960e6bc1ab1e07146e53c2be2aa92e35892a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:14 GMT
ADD file:10280ad72673a7baf3b2455f9bc16ba1c50a1d0c5a8276d89e2a786b1c84af75 in / 
# Tue, 12 Oct 2021 01:42:14 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2d37bb723b5cb5b3b65952b63d77d16450a085709a53d71e1e3251bd609ea570`  
		Last Modified: Tue, 12 Oct 2021 01:50:11 GMT  
		Size: 49.2 MB (49222718 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable` - linux; 386

```console
$ docker pull debian@sha256:5cc593a0418586ee8d8063e1febdc5418e742e47fd3bbd36f4cfd90b83f7b0a1
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **51.2 MB (51207558 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:db65077c2e16181149a43424ec2c9edaf084b70bfcac9b5cb0ee4c54f136feb3`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:58 GMT
ADD file:53dbb1fd8cfb15946c2df51556975d015470c45ae4ca8033fba41c5f31356d52 in / 
# Tue, 12 Oct 2021 01:40:58 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:1f657a8783f7ec563f233d088f92dcd9bf9e3a39b1d449cb9fab27caed5aadb0`  
		Last Modified: Tue, 12 Oct 2021 01:49:47 GMT  
		Size: 51.2 MB (51207558 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable` - linux; mips64le

```console
$ docker pull debian@sha256:9178b403e2489c89d302a4275208d85f89916ff7170498745b738bb7dd4a7b74
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.1 MB (49079493 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:07c603e98ef5819b15fddd70fca5fa1093b1c81c3db58f07364d2bd5da5a2b1f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:12:34 GMT
ADD file:e8a4c8f46e0308d588aabdfa8cef54cbc9e873569fc4f06b1fb6942cb106c5b0 in / 
# Tue, 12 Oct 2021 01:12:35 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:771cec173f62208002de95d47a62059a81d70a4b0c86620fcc0f9dd6ff5deee2`  
		Last Modified: Tue, 12 Oct 2021 01:22:21 GMT  
		Size: 49.1 MB (49079493 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable` - linux; ppc64le

```console
$ docker pull debian@sha256:184f45d671a31ebc88fd5c7894fe8a38061f42d4cba5ffe1331af2034fb75d1b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.2 MB (54183530 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9b9071633d8c700d6759209257667ba09ac2910bdc81c32a65ce9891317fdacd`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:27:07 GMT
ADD file:1685b474962ea6fe3945c29bc49da37313319ebb22343899681a2e98b188f7a7 in / 
# Tue, 12 Oct 2021 01:27:12 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:f80c6b99f8408b2bde68df0b7885d92c0fcd5531033f895599a7d09df37fe5e2`  
		Last Modified: Tue, 12 Oct 2021 01:39:27 GMT  
		Size: 54.2 MB (54183530 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable` - linux; s390x

```console
$ docker pull debian@sha256:99e6214d374d8dd98b2d9feb31b61c8b1a7953a8d6034f3555d3138d9c78194f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.0 MB (49004893 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:c893c71789973beb7ae24997a12fd8adb6337a63a79205985622acb81fd21e9e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:43:08 GMT
ADD file:131966fcf68790a9e9a3c79c1be0f33ee52522f57e0d286fee2f2441daba4718 in / 
# Tue, 12 Oct 2021 00:43:10 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:e95a5b27ccd32e7e7d080f127e6d02afb66719bec59086cf85da5f76bf785fdf`  
		Last Modified: Tue, 12 Oct 2021 00:48:58 GMT  
		Size: 49.0 MB (49004893 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:oldstable-20211011`

```console
$ docker pull debian@sha256:843fdcc419eba12e43deff8b955d3b06f7a4296ddc8916b1fed048446c0356d2
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:oldstable-20211011` - linux; amd64

```console
$ docker pull debian@sha256:66b1a790407e21eab0f1c3cd8a5aacadcc2b70c0c865f87976cd6221eb64db2b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.4 MB (50436730 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:dc4441f19678c8bf67f5183b81e95491594f4154a51a84638eb939d799ec40b6`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:21:36 GMT
ADD file:1254fe58a557c9bd7a44702af996bc01a041007fa31c4bfd0674a7437f83e226 in / 
# Tue, 12 Oct 2021 01:21:36 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:486fdf79d93406106977e297ffa5f755ab39b223d63b7cf84a14c0ff8f62a9d2`  
		Last Modified: Tue, 12 Oct 2021 01:27:45 GMT  
		Size: 50.4 MB (50436730 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011` - linux; arm variant v5

```console
$ docker pull debian@sha256:173b47ac2d6db7999ce60877b7487600566ae307f2ab38d9953cdfd74590d27a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **48.2 MB (48154060 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:88ece4f04cfb212a3f78cac2c0cbd34357ebea607d6260d6c7c1daec40c25d44`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:52:56 GMT
ADD file:a042a1b4d39d508b4725145149caf6615d44e43427b4566e3839840e9dec939e in / 
# Tue, 12 Oct 2021 00:52:57 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:e187af41423780e5cc95f89de4b56a3c18a2c5636f1919be23b07449d54381a7`  
		Last Modified: Tue, 12 Oct 2021 01:09:44 GMT  
		Size: 48.2 MB (48154060 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011` - linux; arm variant v7

```console
$ docker pull debian@sha256:f81dd238be9034087264afc0dead1422c67347b03c4aa730e2e86337f6e18ce9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **45.9 MB (45917917 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:297db7d35883781b4abf5c1c520ad049e705249fba2d259ded490c9692c2a54b`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:31:12 GMT
ADD file:075763af19fd2f62d5199e9f9ece478e2602aee33c939ccd258f599d88469046 in / 
# Tue, 12 Oct 2021 01:31:13 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:e1e8a087bec2ff439032f91ace1191030ed684bf5807987913cb1d995565a032`  
		Last Modified: Tue, 12 Oct 2021 01:47:54 GMT  
		Size: 45.9 MB (45917917 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:ed3651fcf95b5fc5e1cba1b3de53e0501d2682a0978e0b800ec489ae6dd565aa
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.2 MB (49222718 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a6dcb8339dd72e9a03e5a41eb75f960e6bc1ab1e07146e53c2be2aa92e35892a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:14 GMT
ADD file:10280ad72673a7baf3b2455f9bc16ba1c50a1d0c5a8276d89e2a786b1c84af75 in / 
# Tue, 12 Oct 2021 01:42:14 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2d37bb723b5cb5b3b65952b63d77d16450a085709a53d71e1e3251bd609ea570`  
		Last Modified: Tue, 12 Oct 2021 01:50:11 GMT  
		Size: 49.2 MB (49222718 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011` - linux; 386

```console
$ docker pull debian@sha256:5cc593a0418586ee8d8063e1febdc5418e742e47fd3bbd36f4cfd90b83f7b0a1
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **51.2 MB (51207558 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:db65077c2e16181149a43424ec2c9edaf084b70bfcac9b5cb0ee4c54f136feb3`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:58 GMT
ADD file:53dbb1fd8cfb15946c2df51556975d015470c45ae4ca8033fba41c5f31356d52 in / 
# Tue, 12 Oct 2021 01:40:58 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:1f657a8783f7ec563f233d088f92dcd9bf9e3a39b1d449cb9fab27caed5aadb0`  
		Last Modified: Tue, 12 Oct 2021 01:49:47 GMT  
		Size: 51.2 MB (51207558 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011` - linux; mips64le

```console
$ docker pull debian@sha256:9178b403e2489c89d302a4275208d85f89916ff7170498745b738bb7dd4a7b74
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.1 MB (49079493 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:07c603e98ef5819b15fddd70fca5fa1093b1c81c3db58f07364d2bd5da5a2b1f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:12:34 GMT
ADD file:e8a4c8f46e0308d588aabdfa8cef54cbc9e873569fc4f06b1fb6942cb106c5b0 in / 
# Tue, 12 Oct 2021 01:12:35 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:771cec173f62208002de95d47a62059a81d70a4b0c86620fcc0f9dd6ff5deee2`  
		Last Modified: Tue, 12 Oct 2021 01:22:21 GMT  
		Size: 49.1 MB (49079493 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011` - linux; ppc64le

```console
$ docker pull debian@sha256:184f45d671a31ebc88fd5c7894fe8a38061f42d4cba5ffe1331af2034fb75d1b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.2 MB (54183530 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9b9071633d8c700d6759209257667ba09ac2910bdc81c32a65ce9891317fdacd`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:27:07 GMT
ADD file:1685b474962ea6fe3945c29bc49da37313319ebb22343899681a2e98b188f7a7 in / 
# Tue, 12 Oct 2021 01:27:12 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:f80c6b99f8408b2bde68df0b7885d92c0fcd5531033f895599a7d09df37fe5e2`  
		Last Modified: Tue, 12 Oct 2021 01:39:27 GMT  
		Size: 54.2 MB (54183530 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011` - linux; s390x

```console
$ docker pull debian@sha256:99e6214d374d8dd98b2d9feb31b61c8b1a7953a8d6034f3555d3138d9c78194f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.0 MB (49004893 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:c893c71789973beb7ae24997a12fd8adb6337a63a79205985622acb81fd21e9e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:43:08 GMT
ADD file:131966fcf68790a9e9a3c79c1be0f33ee52522f57e0d286fee2f2441daba4718 in / 
# Tue, 12 Oct 2021 00:43:10 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:e95a5b27ccd32e7e7d080f127e6d02afb66719bec59086cf85da5f76bf785fdf`  
		Last Modified: Tue, 12 Oct 2021 00:48:58 GMT  
		Size: 49.0 MB (49004893 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:oldstable-20211011-slim`

```console
$ docker pull debian@sha256:de685da52b9eda22b01498dfa7c62f5fe65b9d3eb52a7b4c77a23e33d8f3e6d9
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:oldstable-20211011-slim` - linux; amd64

```console
$ docker pull debian@sha256:39600b2f54313fb62f6109dadc36d62f1418c51dde871f7c97e9d6be9b9d8f59
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **27.1 MB (27139517 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:dfc5f8e95722e645dba909ea600a7ed02e899e4fb1c89513514ebe3b6c05ea2a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:21:48 GMT
ADD file:0087281285401f9572dc89dc688004e8d163d8a6d0f7d06d6b9e49182048d7f8 in / 
# Tue, 12 Oct 2021 01:21:48 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:fb1a97c1b19b06e11a10ec57653655b9eba0af39ba825e6bd53860c5d2004314`  
		Last Modified: Tue, 12 Oct 2021 01:28:05 GMT  
		Size: 27.1 MB (27139517 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:eca169327cf548858a07543d8e4267a37c30110f1abfbef21fac4bab1576cfb2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **24.9 MB (24872664 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f7e3c01c91b41008236f284ce976e0003653e2710454bfdd5e3851a01c8c979e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:53:27 GMT
ADD file:185c1c313855b2b561257e51db5d2f5391b483cc432f07e7514b60cd2286b22d in / 
# Tue, 12 Oct 2021 00:53:28 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:a6064fa7bf9cd05b7bc06c090d0c55c7d418fdf9d765054d4400566d4f0b0964`  
		Last Modified: Tue, 12 Oct 2021 01:10:18 GMT  
		Size: 24.9 MB (24872664 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:3459e7a1e45d8bfdebb9db507870ec2eab28569331636788ee970bf632ba3099
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **22.7 MB (22739684 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9b99ca577fedc45ac5ec1abfac64a8e85145d21ae23905c753c5927fc33edc96`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:31:44 GMT
ADD file:6518c6d487ce10fc0225f82202f45f76b27b8c2c730f9dc5d8684a2fe1abe052 in / 
# Tue, 12 Oct 2021 01:31:45 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6264a2b2178995c852d7382a07beb4b2180a91d897ea06ff0f85e8980b848ce2`  
		Last Modified: Tue, 12 Oct 2021 01:48:26 GMT  
		Size: 22.7 MB (22739684 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:4ab893492695e7b5d720f8c4e73a7e324c7031a197a10a9e6dc99bdc38311500
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **25.9 MB (25908531 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:38f9be39c6f8b4d85099dc2c53a77a435a09e3d341efc11bed012cb2754ff92a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:26 GMT
ADD file:d84316b679e908b43e684d1016a6717082e6f204251fa4073c9a2ef33fbf0f75 in / 
# Tue, 12 Oct 2021 01:42:27 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2868439da93c0a0bde3f7b871b0d731222d132234cbdb2c306bcee9b248f0230`  
		Last Modified: Tue, 12 Oct 2021 01:50:33 GMT  
		Size: 25.9 MB (25908531 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011-slim` - linux; 386

```console
$ docker pull debian@sha256:b3b09bba602445d783770407cf2f5cad522f30664cef4ad304fc6e9ec0d6b6dc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **27.8 MB (27791432 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a7be7b77864d8dfefa7cf5b430ed6b45eabbdd4d553e7fa373ec347ad2a9f115`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:13 GMT
ADD file:2dd30cffeab47a7a23ffe4360316e7c6dea896e3b1a2b09c2920ff6c365f95d1 in / 
# Tue, 12 Oct 2021 01:41:13 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:8a077bff6f39437082dd48e8f72a9f9e283de24e59f11abbd0ae1724e025858a`  
		Last Modified: Tue, 12 Oct 2021 01:50:12 GMT  
		Size: 27.8 MB (27791432 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011-slim` - linux; mips64le

```console
$ docker pull debian@sha256:e3cc551053ea193ecbf7dd470a4912fe80a56f2880681c95e77b56dda0241009
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **25.8 MB (25806507 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3daf9661d78d9210c3599942e82d0e8157ae3cae3de8169c0f5cb60a6c6ed018`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:13:00 GMT
ADD file:802804164a1553ad9a96fb3a18bd3b946bb8f13ff708ba1ee602833d5eeee223 in / 
# Tue, 12 Oct 2021 01:13:01 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:00d53dc1ef6d0acda8a927a222fcf676c7f6e815046421c750df808bec08249f`  
		Last Modified: Tue, 12 Oct 2021 01:22:55 GMT  
		Size: 25.8 MB (25806507 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011-slim` - linux; ppc64le

```console
$ docker pull debian@sha256:4e11cefcdb3b921eb7679001f0e265d31cc2c3cf1de1b293127177331ce79c23
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.5 MB (30547194 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a318b65276d8fee17e69fb358b56710039bfb33a95a46d644c8e3c847a7694c9`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:27:38 GMT
ADD file:f3be27a2f7c4b00219c330c9134f72512cb3c0e27748e8139f36ded07e864d69 in / 
# Tue, 12 Oct 2021 01:27:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:dc9eab1fc04bbc5273373b8b688f6153bb57283aabfa43a541656a5cbc07c382`  
		Last Modified: Tue, 12 Oct 2021 01:40:10 GMT  
		Size: 30.5 MB (30547194 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-20211011-slim` - linux; s390x

```console
$ docker pull debian@sha256:b38ac6dbb3e27812ca70b5a40a54dbad7c1fc95e97e8572315dc5bcf4e76fbd1
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **25.8 MB (25754237 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:7404699e67c5d79b5883165de65d63077045b31e72389b0a22bce55c83ecbfcc`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:43:24 GMT
ADD file:6ec6e214c53947bf14459791523b57a631aa100c905c746bdba061913e5b1347 in / 
# Tue, 12 Oct 2021 00:43:26 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:092d82f61df2a5e21e13d548ff1e09c69cb982e320d870d7b38ca7017cc46574`  
		Last Modified: Tue, 12 Oct 2021 00:49:14 GMT  
		Size: 25.8 MB (25754237 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:oldstable-backports`

```console
$ docker pull debian@sha256:5243ab1b76abe44ea045ad1eeee47de96619c5d6bda5c20e2395875204a904ee
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:oldstable-backports` - linux; amd64

```console
$ docker pull debian@sha256:6380533dd62ebe2bd320f8d6d1a5b6ebd6e3ea59003cb3d9dcf609098c048518
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.4 MB (50436958 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e8474337d037b33e18c195f959ced8c9164cceb76de317883f16d2cb7c2a0484`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:21:36 GMT
ADD file:1254fe58a557c9bd7a44702af996bc01a041007fa31c4bfd0674a7437f83e226 in / 
# Tue, 12 Oct 2021 01:21:36 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:21:40 GMT
RUN echo 'deb http://deb.debian.org/debian oldstable-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:486fdf79d93406106977e297ffa5f755ab39b223d63b7cf84a14c0ff8f62a9d2`  
		Last Modified: Tue, 12 Oct 2021 01:27:45 GMT  
		Size: 50.4 MB (50436730 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:904fffecb87b762376a2a541a5fe15d370f8f82caf2241e80794a1985da62648`  
		Last Modified: Tue, 12 Oct 2021 01:27:54 GMT  
		Size: 228.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-backports` - linux; arm variant v5

```console
$ docker pull debian@sha256:00d3089bc834c350def7f1141872cba227cbf981446fda9c80ab418654d4fad8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **48.2 MB (48154288 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:50056f0f79d54a29ab196b609f9ec149aa40b53af5f0308efc9bad6814cb0056`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:52:56 GMT
ADD file:a042a1b4d39d508b4725145149caf6615d44e43427b4566e3839840e9dec939e in / 
# Tue, 12 Oct 2021 00:52:57 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 00:53:10 GMT
RUN echo 'deb http://deb.debian.org/debian oldstable-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:e187af41423780e5cc95f89de4b56a3c18a2c5636f1919be23b07449d54381a7`  
		Last Modified: Tue, 12 Oct 2021 01:09:44 GMT  
		Size: 48.2 MB (48154060 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:f2fee3bcfa3710a132764abc7d51d4e4a2f8b1a1cb26b123dc2e5ec2b6a007ec`  
		Last Modified: Tue, 12 Oct 2021 01:09:55 GMT  
		Size: 228.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-backports` - linux; arm variant v7

```console
$ docker pull debian@sha256:acfe7fd2c4dc3e75c14f2e25d8f34bcb2f1f48ed1bbe34964ea092e4c915c404
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **45.9 MB (45918145 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4e0e3a0f95c2d2375a2a560ca0187eb7bbd573745444a1238cd8565f84992068`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:31:12 GMT
ADD file:075763af19fd2f62d5199e9f9ece478e2602aee33c939ccd258f599d88469046 in / 
# Tue, 12 Oct 2021 01:31:13 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:31:25 GMT
RUN echo 'deb http://deb.debian.org/debian oldstable-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:e1e8a087bec2ff439032f91ace1191030ed684bf5807987913cb1d995565a032`  
		Last Modified: Tue, 12 Oct 2021 01:47:54 GMT  
		Size: 45.9 MB (45917917 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:06d5fe54b7d2e0e4000dc91ff9e082e5b3328cfc66c7ea9cd92e2617e34e20cf`  
		Last Modified: Tue, 12 Oct 2021 01:48:06 GMT  
		Size: 228.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-backports` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:6d751ef6432e842e731d2eacfe3df4e190ef7aee2314d4d665d40e310cfcbd76
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.2 MB (49222943 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cc392951da1a1868cdc111888754a7b4d02fa92ab7e6bdc02fcfd91c78e92fe7`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:14 GMT
ADD file:10280ad72673a7baf3b2455f9bc16ba1c50a1d0c5a8276d89e2a786b1c84af75 in / 
# Tue, 12 Oct 2021 01:42:14 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:42:20 GMT
RUN echo 'deb http://deb.debian.org/debian oldstable-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:2d37bb723b5cb5b3b65952b63d77d16450a085709a53d71e1e3251bd609ea570`  
		Last Modified: Tue, 12 Oct 2021 01:50:11 GMT  
		Size: 49.2 MB (49222718 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:c51c2e0e1287f388532f7ccba9ef48bd1c75351aec0b98c30c58c88f5ed97c31`  
		Last Modified: Tue, 12 Oct 2021 01:50:22 GMT  
		Size: 225.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-backports` - linux; 386

```console
$ docker pull debian@sha256:4926935f134d09402c704ac5fc6228f498df692a4f98ea8ae3e51ed97d21f0b2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **51.2 MB (51207785 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:0c2e641a4b5992ed8ddd701cdc020c30d81df3e6f6ee22e3d7538efbb45d0f66`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:40:58 GMT
ADD file:53dbb1fd8cfb15946c2df51556975d015470c45ae4ca8033fba41c5f31356d52 in / 
# Tue, 12 Oct 2021 01:40:58 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:41:04 GMT
RUN echo 'deb http://deb.debian.org/debian oldstable-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:1f657a8783f7ec563f233d088f92dcd9bf9e3a39b1d449cb9fab27caed5aadb0`  
		Last Modified: Tue, 12 Oct 2021 01:49:47 GMT  
		Size: 51.2 MB (51207558 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:10d7f2e4b4fd74448d3c1e508cbb2de52ff827c88fed34c9cf7062ce5c0a4608`  
		Last Modified: Tue, 12 Oct 2021 01:49:59 GMT  
		Size: 227.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-backports` - linux; mips64le

```console
$ docker pull debian@sha256:f1f3a35fda76f13e289b4ce5fe0420f350827d934981c41f49d5a6e35e7b7b23
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.1 MB (49079720 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6bc6fb2b12468354066f6b9986e914740f594a46b96ef90f0ba58e96c2c8bb82`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:12:34 GMT
ADD file:e8a4c8f46e0308d588aabdfa8cef54cbc9e873569fc4f06b1fb6942cb106c5b0 in / 
# Tue, 12 Oct 2021 01:12:35 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:12:42 GMT
RUN echo 'deb http://deb.debian.org/debian oldstable-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:771cec173f62208002de95d47a62059a81d70a4b0c86620fcc0f9dd6ff5deee2`  
		Last Modified: Tue, 12 Oct 2021 01:22:21 GMT  
		Size: 49.1 MB (49079493 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:9cce9ca9d67188dd30df52aeac7c4928bff77b2efa948044fa744c0fc9e0d95d`  
		Last Modified: Tue, 12 Oct 2021 01:22:31 GMT  
		Size: 227.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-backports` - linux; ppc64le

```console
$ docker pull debian@sha256:2efa26897472def3e7607da3f947623eec0ec3ba6b9ad3a43b82eaa0da4eb843
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.2 MB (54183759 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1bafe1bf448cf088e395bce39fe004c0a47834712f17bb8a2d85fec632f88ecd`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:27:07 GMT
ADD file:1685b474962ea6fe3945c29bc49da37313319ebb22343899681a2e98b188f7a7 in / 
# Tue, 12 Oct 2021 01:27:12 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:27:25 GMT
RUN echo 'deb http://deb.debian.org/debian oldstable-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:f80c6b99f8408b2bde68df0b7885d92c0fcd5531033f895599a7d09df37fe5e2`  
		Last Modified: Tue, 12 Oct 2021 01:39:27 GMT  
		Size: 54.2 MB (54183530 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:07eb798c822c60d4194a983e9230877e5580353d3271e1a99c12cb4c64f47e60`  
		Last Modified: Tue, 12 Oct 2021 01:39:38 GMT  
		Size: 229.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-backports` - linux; s390x

```console
$ docker pull debian@sha256:f6596f7eace5f84a0ccf8ecc9aac007375cb65abff75de1ba93dfdcfc5c915a4
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **49.0 MB (49005119 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3515baf08a2e599eca5bb5135a95d75dd3812ac8ba139def5fae0f6f312b275e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:43:08 GMT
ADD file:131966fcf68790a9e9a3c79c1be0f33ee52522f57e0d286fee2f2441daba4718 in / 
# Tue, 12 Oct 2021 00:43:10 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 00:43:17 GMT
RUN echo 'deb http://deb.debian.org/debian oldstable-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:e95a5b27ccd32e7e7d080f127e6d02afb66719bec59086cf85da5f76bf785fdf`  
		Last Modified: Tue, 12 Oct 2021 00:48:58 GMT  
		Size: 49.0 MB (49004893 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:55a3b7df1003352420c9b201bfb5ac124c79af942a562b5e916337ce44ee0e4d`  
		Last Modified: Tue, 12 Oct 2021 00:49:05 GMT  
		Size: 226.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:oldstable-slim`

```console
$ docker pull debian@sha256:de685da52b9eda22b01498dfa7c62f5fe65b9d3eb52a7b4c77a23e33d8f3e6d9
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:oldstable-slim` - linux; amd64

```console
$ docker pull debian@sha256:39600b2f54313fb62f6109dadc36d62f1418c51dde871f7c97e9d6be9b9d8f59
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **27.1 MB (27139517 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:dfc5f8e95722e645dba909ea600a7ed02e899e4fb1c89513514ebe3b6c05ea2a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:21:48 GMT
ADD file:0087281285401f9572dc89dc688004e8d163d8a6d0f7d06d6b9e49182048d7f8 in / 
# Tue, 12 Oct 2021 01:21:48 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:fb1a97c1b19b06e11a10ec57653655b9eba0af39ba825e6bd53860c5d2004314`  
		Last Modified: Tue, 12 Oct 2021 01:28:05 GMT  
		Size: 27.1 MB (27139517 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:eca169327cf548858a07543d8e4267a37c30110f1abfbef21fac4bab1576cfb2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **24.9 MB (24872664 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f7e3c01c91b41008236f284ce976e0003653e2710454bfdd5e3851a01c8c979e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:53:27 GMT
ADD file:185c1c313855b2b561257e51db5d2f5391b483cc432f07e7514b60cd2286b22d in / 
# Tue, 12 Oct 2021 00:53:28 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:a6064fa7bf9cd05b7bc06c090d0c55c7d418fdf9d765054d4400566d4f0b0964`  
		Last Modified: Tue, 12 Oct 2021 01:10:18 GMT  
		Size: 24.9 MB (24872664 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:3459e7a1e45d8bfdebb9db507870ec2eab28569331636788ee970bf632ba3099
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **22.7 MB (22739684 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9b99ca577fedc45ac5ec1abfac64a8e85145d21ae23905c753c5927fc33edc96`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:31:44 GMT
ADD file:6518c6d487ce10fc0225f82202f45f76b27b8c2c730f9dc5d8684a2fe1abe052 in / 
# Tue, 12 Oct 2021 01:31:45 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6264a2b2178995c852d7382a07beb4b2180a91d897ea06ff0f85e8980b848ce2`  
		Last Modified: Tue, 12 Oct 2021 01:48:26 GMT  
		Size: 22.7 MB (22739684 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:4ab893492695e7b5d720f8c4e73a7e324c7031a197a10a9e6dc99bdc38311500
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **25.9 MB (25908531 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:38f9be39c6f8b4d85099dc2c53a77a435a09e3d341efc11bed012cb2754ff92a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:26 GMT
ADD file:d84316b679e908b43e684d1016a6717082e6f204251fa4073c9a2ef33fbf0f75 in / 
# Tue, 12 Oct 2021 01:42:27 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2868439da93c0a0bde3f7b871b0d731222d132234cbdb2c306bcee9b248f0230`  
		Last Modified: Tue, 12 Oct 2021 01:50:33 GMT  
		Size: 25.9 MB (25908531 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-slim` - linux; 386

```console
$ docker pull debian@sha256:b3b09bba602445d783770407cf2f5cad522f30664cef4ad304fc6e9ec0d6b6dc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **27.8 MB (27791432 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a7be7b77864d8dfefa7cf5b430ed6b45eabbdd4d553e7fa373ec347ad2a9f115`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:13 GMT
ADD file:2dd30cffeab47a7a23ffe4360316e7c6dea896e3b1a2b09c2920ff6c365f95d1 in / 
# Tue, 12 Oct 2021 01:41:13 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:8a077bff6f39437082dd48e8f72a9f9e283de24e59f11abbd0ae1724e025858a`  
		Last Modified: Tue, 12 Oct 2021 01:50:12 GMT  
		Size: 27.8 MB (27791432 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-slim` - linux; mips64le

```console
$ docker pull debian@sha256:e3cc551053ea193ecbf7dd470a4912fe80a56f2880681c95e77b56dda0241009
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **25.8 MB (25806507 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3daf9661d78d9210c3599942e82d0e8157ae3cae3de8169c0f5cb60a6c6ed018`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:13:00 GMT
ADD file:802804164a1553ad9a96fb3a18bd3b946bb8f13ff708ba1ee602833d5eeee223 in / 
# Tue, 12 Oct 2021 01:13:01 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:00d53dc1ef6d0acda8a927a222fcf676c7f6e815046421c750df808bec08249f`  
		Last Modified: Tue, 12 Oct 2021 01:22:55 GMT  
		Size: 25.8 MB (25806507 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-slim` - linux; ppc64le

```console
$ docker pull debian@sha256:4e11cefcdb3b921eb7679001f0e265d31cc2c3cf1de1b293127177331ce79c23
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.5 MB (30547194 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a318b65276d8fee17e69fb358b56710039bfb33a95a46d644c8e3c847a7694c9`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:27:38 GMT
ADD file:f3be27a2f7c4b00219c330c9134f72512cb3c0e27748e8139f36ded07e864d69 in / 
# Tue, 12 Oct 2021 01:27:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:dc9eab1fc04bbc5273373b8b688f6153bb57283aabfa43a541656a5cbc07c382`  
		Last Modified: Tue, 12 Oct 2021 01:40:10 GMT  
		Size: 30.5 MB (30547194 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:oldstable-slim` - linux; s390x

```console
$ docker pull debian@sha256:b38ac6dbb3e27812ca70b5a40a54dbad7c1fc95e97e8572315dc5bcf4e76fbd1
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **25.8 MB (25754237 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:7404699e67c5d79b5883165de65d63077045b31e72389b0a22bce55c83ecbfcc`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:43:24 GMT
ADD file:6ec6e214c53947bf14459791523b57a631aa100c905c746bdba061913e5b1347 in / 
# Tue, 12 Oct 2021 00:43:26 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:092d82f61df2a5e21e13d548ff1e09c69cb982e320d870d7b38ca7017cc46574`  
		Last Modified: Tue, 12 Oct 2021 00:49:14 GMT  
		Size: 25.8 MB (25754237 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:rc-buggy`

```console
$ docker pull debian@sha256:86dbc72e2ecf04776797974bd1b8c1bc842fc8940b2c769b8647671fea17a22c
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:rc-buggy` - linux; amd64

```console
$ docker pull debian@sha256:722413f4338fbd7f3c207f36253cc413329c494c41cecdfc3b61558957fbbd09
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.7 MB (55687696 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:89962650274438b586fb32a8bf22cbe35391e77b387c5fae95fb0f148172429b`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:21:58 GMT
ADD file:2d684471f088173801ff86eaf94c1d858b74220b84642d73b02bd1bfe0d54b60 in / 
# Tue, 12 Oct 2021 01:21:58 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:23:43 GMT
RUN echo 'deb http://deb.debian.org/debian rc-buggy main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:ba87283d9c312600aafb7f4149986884d524aae42e0701a90d2fc23c85e977b1`  
		Last Modified: Tue, 12 Oct 2021 01:28:21 GMT  
		Size: 55.7 MB (55687470 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:c26674154af9f7ba052f74e417b6322bb9f6e88a25c55d070d5a3c05cc3587b1`  
		Last Modified: Tue, 12 Oct 2021 01:31:20 GMT  
		Size: 226.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:rc-buggy` - linux; arm variant v5

```console
$ docker pull debian@sha256:7b6405b207f9d0e7cc57586feebe2ca61efcaf79ab2cc70db866170365157b9a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53186577 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:16ba8704ecfff8b7fac421cf93e23ee0ceaba7e4348758051819d0ea539030c5`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:53:53 GMT
ADD file:2918872df07d47b9a7862abf3b22f39600ab45587fcd02466bdefa775a3fc2dd in / 
# Tue, 12 Oct 2021 00:53:54 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 00:58:26 GMT
RUN echo 'deb http://deb.debian.org/debian rc-buggy main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:8f73c109c91bd2f18faab4d6862ba33d940956d6457aaaaeeda5e8f72281edcc`  
		Last Modified: Tue, 12 Oct 2021 01:11:00 GMT  
		Size: 53.2 MB (53186352 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:7bf674df9fccabd4879c5e06d5d0e93ce0977242d5abe4ae7811fbd707df2cbb`  
		Last Modified: Tue, 12 Oct 2021 01:17:25 GMT  
		Size: 225.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:rc-buggy` - linux; arm variant v7

```console
$ docker pull debian@sha256:8a13ae66758407abb43d05380da8fad9dc85c9f8da996ae19f0bee1ccc5370c2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.8 MB (50797295 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:144ae178316232a427863aecbccec0ba4392733300ca6fa591fb4c1dee73d715`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:32:13 GMT
ADD file:defbc06b1cc21db01a673f118d34d129435486fcb83814c5c987ff566c61360a in / 
# Tue, 12 Oct 2021 01:32:14 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:37:02 GMT
RUN echo 'deb http://deb.debian.org/debian rc-buggy main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:153dc8e6452cda134a8143d2ac887e7ebfa01c8568115d6b3172ca0008ef4838`  
		Last Modified: Tue, 12 Oct 2021 01:49:06 GMT  
		Size: 50.8 MB (50797066 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:c760bf384122a608ddf9bad0ff3fd4d156d88feb6ff4a57c7486af04b20b1def`  
		Last Modified: Tue, 12 Oct 2021 01:54:47 GMT  
		Size: 229.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:rc-buggy` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:c3560a23bfcd3e13cca0a86f250a88300f8155ebd4851ca8fe93cf3580a564fa
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.7 MB (54703123 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1102d8396514ec2783d970021bee45096e4ff166fee918f2fdcee6d8e77b156d`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:36 GMT
ADD file:830671e1709a0164ca60ad1012d3d0202e7e63c74bdbec7df54f308a4d4c8b11 in / 
# Tue, 12 Oct 2021 01:42:37 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:44:21 GMT
RUN echo 'deb http://deb.debian.org/debian rc-buggy main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:3ed37bc205e0aad196bfd6ba235a1a5fba10e02175afd192c816df6457d06c75`  
		Last Modified: Tue, 12 Oct 2021 01:50:51 GMT  
		Size: 54.7 MB (54702895 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:c96db8976b7344422b08e4e1070a5fdb6c26dde35253c22f707f92aecd437d20`  
		Last Modified: Tue, 12 Oct 2021 01:54:27 GMT  
		Size: 228.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:rc-buggy` - linux; 386

```console
$ docker pull debian@sha256:e16202c4c6bf73470ee2c337cb97f6c85e5831dabff86bb201926d1f55715211
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **56.7 MB (56716331 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:aa85f16c497abe3b358e4a5b2f347e154d6531d11acd83c88b58fdbded6f9e38`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:27 GMT
ADD file:9ab0c16547aac0a32292246b35737ca5260c5942d5e26ca1381be39c9e321ee5 in / 
# Tue, 12 Oct 2021 01:41:27 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:43:39 GMT
RUN echo 'deb http://deb.debian.org/debian rc-buggy main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:c36fe1aa4217cf0326227a0bdcadc16e85d2670bd6485518167903dfc6934c65`  
		Last Modified: Tue, 12 Oct 2021 01:50:33 GMT  
		Size: 56.7 MB (56716103 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:1170e260d363b1d2cdf209f09b5c8384055f728a512bb00e0f6a63b46b28df03`  
		Last Modified: Tue, 12 Oct 2021 01:54:22 GMT  
		Size: 228.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:rc-buggy` - linux; mips64le

```console
$ docker pull debian@sha256:c44e0fba235b23b0bcca787bb299f4d6592fc26b40875320de8878a32c67f07f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.3 MB (54313696 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:646adfe0abdf4cf97717d9f3bc2a761ff0da628fe34667dd3d4f08b919e5fbe5`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:13:27 GMT
ADD file:27e1d46380432bbd287bef3c049b6707a409fa2f49f07e76f4826c6ee57cfca9 in / 
# Tue, 12 Oct 2021 01:13:28 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:16:38 GMT
RUN echo 'deb http://deb.debian.org/debian rc-buggy main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:c7afe2e09744237d99a12c5f86f33ba55800e5acee482662abac2c64c64a7824`  
		Last Modified: Tue, 12 Oct 2021 01:23:41 GMT  
		Size: 54.3 MB (54313468 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:a771580da0d6914e7ca5bc4bc6f45b58872192457aff8bd17de8c5491e53bad9`  
		Last Modified: Tue, 12 Oct 2021 01:28:27 GMT  
		Size: 228.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:rc-buggy` - linux; ppc64le

```console
$ docker pull debian@sha256:8871ccce0b68b6b8e2f377f59816546c7757697c61fcea960b191e44ea775001
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **59.9 MB (59889396 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:16346dd33fc3f506b4950f71d8af23d5ebcffdd4a66f5776c8b9748d224f2f18`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:27:58 GMT
ADD file:975422ef5390ff94fc050a84a9840e39188158f1c74f7b6dfde93829787b7c8d in / 
# Tue, 12 Oct 2021 01:28:07 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:31:46 GMT
RUN echo 'deb http://deb.debian.org/debian rc-buggy main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:3794e49fdef56187e1a7ffe8a3c0d0dda8a5b5c159e370831000df5e5f638479`  
		Last Modified: Tue, 12 Oct 2021 01:41:02 GMT  
		Size: 59.9 MB (59889168 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:47f376a4c7a802fa7692086dc3096f4d65493c272843b8ac365023df781c43cd`  
		Last Modified: Tue, 12 Oct 2021 01:44:54 GMT  
		Size: 228.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:rc-buggy` - linux; s390x

```console
$ docker pull debian@sha256:5ffb0eb3ab03534c0bfbddd6627cbde5d521e9b3faa0dda11de1065cb8732809
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.9 MB (53940968 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1c756aa74b7bcf61f84a3f6be1fbb83ed79221862f8d4491fec7edf8d71b422e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:43:35 GMT
ADD file:d0d1a83115052a10810bdba27eb855359796f604b655de31492b0699abf1d546 in / 
# Tue, 12 Oct 2021 00:43:38 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 00:45:17 GMT
RUN echo 'deb http://deb.debian.org/debian rc-buggy main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:979a8d8677c5949943057dfc51bfa612e3d365d71df4150210296bec00f04cf2`  
		Last Modified: Tue, 12 Oct 2021 00:49:29 GMT  
		Size: 53.9 MB (53940740 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:432f1b4f0043528ebb69390269b5fe14396b57669ae96173a8a5c35110eaa8ad`  
		Last Modified: Tue, 12 Oct 2021 00:51:20 GMT  
		Size: 228.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:rc-buggy-20211011`

```console
$ docker pull debian@sha256:86dbc72e2ecf04776797974bd1b8c1bc842fc8940b2c769b8647671fea17a22c
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:rc-buggy-20211011` - linux; amd64

```console
$ docker pull debian@sha256:722413f4338fbd7f3c207f36253cc413329c494c41cecdfc3b61558957fbbd09
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.7 MB (55687696 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:89962650274438b586fb32a8bf22cbe35391e77b387c5fae95fb0f148172429b`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:21:58 GMT
ADD file:2d684471f088173801ff86eaf94c1d858b74220b84642d73b02bd1bfe0d54b60 in / 
# Tue, 12 Oct 2021 01:21:58 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:23:43 GMT
RUN echo 'deb http://deb.debian.org/debian rc-buggy main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:ba87283d9c312600aafb7f4149986884d524aae42e0701a90d2fc23c85e977b1`  
		Last Modified: Tue, 12 Oct 2021 01:28:21 GMT  
		Size: 55.7 MB (55687470 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:c26674154af9f7ba052f74e417b6322bb9f6e88a25c55d070d5a3c05cc3587b1`  
		Last Modified: Tue, 12 Oct 2021 01:31:20 GMT  
		Size: 226.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:rc-buggy-20211011` - linux; arm variant v5

```console
$ docker pull debian@sha256:7b6405b207f9d0e7cc57586feebe2ca61efcaf79ab2cc70db866170365157b9a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53186577 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:16ba8704ecfff8b7fac421cf93e23ee0ceaba7e4348758051819d0ea539030c5`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:53:53 GMT
ADD file:2918872df07d47b9a7862abf3b22f39600ab45587fcd02466bdefa775a3fc2dd in / 
# Tue, 12 Oct 2021 00:53:54 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 00:58:26 GMT
RUN echo 'deb http://deb.debian.org/debian rc-buggy main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:8f73c109c91bd2f18faab4d6862ba33d940956d6457aaaaeeda5e8f72281edcc`  
		Last Modified: Tue, 12 Oct 2021 01:11:00 GMT  
		Size: 53.2 MB (53186352 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:7bf674df9fccabd4879c5e06d5d0e93ce0977242d5abe4ae7811fbd707df2cbb`  
		Last Modified: Tue, 12 Oct 2021 01:17:25 GMT  
		Size: 225.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:rc-buggy-20211011` - linux; arm variant v7

```console
$ docker pull debian@sha256:8a13ae66758407abb43d05380da8fad9dc85c9f8da996ae19f0bee1ccc5370c2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.8 MB (50797295 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:144ae178316232a427863aecbccec0ba4392733300ca6fa591fb4c1dee73d715`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:32:13 GMT
ADD file:defbc06b1cc21db01a673f118d34d129435486fcb83814c5c987ff566c61360a in / 
# Tue, 12 Oct 2021 01:32:14 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:37:02 GMT
RUN echo 'deb http://deb.debian.org/debian rc-buggy main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:153dc8e6452cda134a8143d2ac887e7ebfa01c8568115d6b3172ca0008ef4838`  
		Last Modified: Tue, 12 Oct 2021 01:49:06 GMT  
		Size: 50.8 MB (50797066 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:c760bf384122a608ddf9bad0ff3fd4d156d88feb6ff4a57c7486af04b20b1def`  
		Last Modified: Tue, 12 Oct 2021 01:54:47 GMT  
		Size: 229.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:rc-buggy-20211011` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:c3560a23bfcd3e13cca0a86f250a88300f8155ebd4851ca8fe93cf3580a564fa
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.7 MB (54703123 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1102d8396514ec2783d970021bee45096e4ff166fee918f2fdcee6d8e77b156d`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:36 GMT
ADD file:830671e1709a0164ca60ad1012d3d0202e7e63c74bdbec7df54f308a4d4c8b11 in / 
# Tue, 12 Oct 2021 01:42:37 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:44:21 GMT
RUN echo 'deb http://deb.debian.org/debian rc-buggy main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:3ed37bc205e0aad196bfd6ba235a1a5fba10e02175afd192c816df6457d06c75`  
		Last Modified: Tue, 12 Oct 2021 01:50:51 GMT  
		Size: 54.7 MB (54702895 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:c96db8976b7344422b08e4e1070a5fdb6c26dde35253c22f707f92aecd437d20`  
		Last Modified: Tue, 12 Oct 2021 01:54:27 GMT  
		Size: 228.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:rc-buggy-20211011` - linux; 386

```console
$ docker pull debian@sha256:e16202c4c6bf73470ee2c337cb97f6c85e5831dabff86bb201926d1f55715211
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **56.7 MB (56716331 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:aa85f16c497abe3b358e4a5b2f347e154d6531d11acd83c88b58fdbded6f9e38`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:27 GMT
ADD file:9ab0c16547aac0a32292246b35737ca5260c5942d5e26ca1381be39c9e321ee5 in / 
# Tue, 12 Oct 2021 01:41:27 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:43:39 GMT
RUN echo 'deb http://deb.debian.org/debian rc-buggy main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:c36fe1aa4217cf0326227a0bdcadc16e85d2670bd6485518167903dfc6934c65`  
		Last Modified: Tue, 12 Oct 2021 01:50:33 GMT  
		Size: 56.7 MB (56716103 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:1170e260d363b1d2cdf209f09b5c8384055f728a512bb00e0f6a63b46b28df03`  
		Last Modified: Tue, 12 Oct 2021 01:54:22 GMT  
		Size: 228.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:rc-buggy-20211011` - linux; mips64le

```console
$ docker pull debian@sha256:c44e0fba235b23b0bcca787bb299f4d6592fc26b40875320de8878a32c67f07f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.3 MB (54313696 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:646adfe0abdf4cf97717d9f3bc2a761ff0da628fe34667dd3d4f08b919e5fbe5`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:13:27 GMT
ADD file:27e1d46380432bbd287bef3c049b6707a409fa2f49f07e76f4826c6ee57cfca9 in / 
# Tue, 12 Oct 2021 01:13:28 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:16:38 GMT
RUN echo 'deb http://deb.debian.org/debian rc-buggy main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:c7afe2e09744237d99a12c5f86f33ba55800e5acee482662abac2c64c64a7824`  
		Last Modified: Tue, 12 Oct 2021 01:23:41 GMT  
		Size: 54.3 MB (54313468 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:a771580da0d6914e7ca5bc4bc6f45b58872192457aff8bd17de8c5491e53bad9`  
		Last Modified: Tue, 12 Oct 2021 01:28:27 GMT  
		Size: 228.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:rc-buggy-20211011` - linux; ppc64le

```console
$ docker pull debian@sha256:8871ccce0b68b6b8e2f377f59816546c7757697c61fcea960b191e44ea775001
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **59.9 MB (59889396 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:16346dd33fc3f506b4950f71d8af23d5ebcffdd4a66f5776c8b9748d224f2f18`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:27:58 GMT
ADD file:975422ef5390ff94fc050a84a9840e39188158f1c74f7b6dfde93829787b7c8d in / 
# Tue, 12 Oct 2021 01:28:07 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:31:46 GMT
RUN echo 'deb http://deb.debian.org/debian rc-buggy main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:3794e49fdef56187e1a7ffe8a3c0d0dda8a5b5c159e370831000df5e5f638479`  
		Last Modified: Tue, 12 Oct 2021 01:41:02 GMT  
		Size: 59.9 MB (59889168 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:47f376a4c7a802fa7692086dc3096f4d65493c272843b8ac365023df781c43cd`  
		Last Modified: Tue, 12 Oct 2021 01:44:54 GMT  
		Size: 228.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:rc-buggy-20211011` - linux; s390x

```console
$ docker pull debian@sha256:5ffb0eb3ab03534c0bfbddd6627cbde5d521e9b3faa0dda11de1065cb8732809
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.9 MB (53940968 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:1c756aa74b7bcf61f84a3f6be1fbb83ed79221862f8d4491fec7edf8d71b422e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:43:35 GMT
ADD file:d0d1a83115052a10810bdba27eb855359796f604b655de31492b0699abf1d546 in / 
# Tue, 12 Oct 2021 00:43:38 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 00:45:17 GMT
RUN echo 'deb http://deb.debian.org/debian rc-buggy main' > /etc/apt/sources.list.d/experimental.list
```

-	Layers:
	-	`sha256:979a8d8677c5949943057dfc51bfa612e3d365d71df4150210296bec00f04cf2`  
		Last Modified: Tue, 12 Oct 2021 00:49:29 GMT  
		Size: 53.9 MB (53940740 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:432f1b4f0043528ebb69390269b5fe14396b57669ae96173a8a5c35110eaa8ad`  
		Last Modified: Tue, 12 Oct 2021 00:51:20 GMT  
		Size: 228.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:sid`

```console
$ docker pull debian@sha256:4e85ef0d6eac69c503ba9e4b4b1957c77468ecbe2c38e453ed0cc0ee0e709869
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 9
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; riscv64
	-	linux; s390x

### `debian:sid` - linux; amd64

```console
$ docker pull debian@sha256:d65918b2bcb6790c777771938ccae929a893b12fe8e3157004795c0c74969c5b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.7 MB (55687470 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cdd43765522e651a5208a53e039ad192b70dac64643d6202ae00c0c61c05191c`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:21:58 GMT
ADD file:2d684471f088173801ff86eaf94c1d858b74220b84642d73b02bd1bfe0d54b60 in / 
# Tue, 12 Oct 2021 01:21:58 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:ba87283d9c312600aafb7f4149986884d524aae42e0701a90d2fc23c85e977b1`  
		Last Modified: Tue, 12 Oct 2021 01:28:21 GMT  
		Size: 55.7 MB (55687470 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid` - linux; arm variant v5

```console
$ docker pull debian@sha256:9f3667c133e7d9ad5f1c437689087457c0ec33eefee613f0ebf1c441749ba08e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53186352 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:7179ffe402670d6eccfb8b78b0f2cc109035cd6f35187d9795edb81b9688d5b0`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:53:53 GMT
ADD file:2918872df07d47b9a7862abf3b22f39600ab45587fcd02466bdefa775a3fc2dd in / 
# Tue, 12 Oct 2021 00:53:54 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:8f73c109c91bd2f18faab4d6862ba33d940956d6457aaaaeeda5e8f72281edcc`  
		Last Modified: Tue, 12 Oct 2021 01:11:00 GMT  
		Size: 53.2 MB (53186352 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid` - linux; arm variant v7

```console
$ docker pull debian@sha256:7f07c18cbba49f9b8d72121f50f0e7e28a12e0e9a7b320a95890516cbccff830
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.8 MB (50797066 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6b6732764d17da5c6b322f69832b09240b9d6b3f2f5fcd9378560027e895aa29`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:32:13 GMT
ADD file:defbc06b1cc21db01a673f118d34d129435486fcb83814c5c987ff566c61360a in / 
# Tue, 12 Oct 2021 01:32:14 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:153dc8e6452cda134a8143d2ac887e7ebfa01c8568115d6b3172ca0008ef4838`  
		Last Modified: Tue, 12 Oct 2021 01:49:06 GMT  
		Size: 50.8 MB (50797066 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:3e7ed43c0ac67026555919d53f5c3f5be0719496acb23a6b3f9a37fb1ba90ab4
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.7 MB (54702895 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ec700318828577b1596c3328a8b89157631b2c85ffc8a8fdc9ad7128c9d281df`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:36 GMT
ADD file:830671e1709a0164ca60ad1012d3d0202e7e63c74bdbec7df54f308a4d4c8b11 in / 
# Tue, 12 Oct 2021 01:42:37 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:3ed37bc205e0aad196bfd6ba235a1a5fba10e02175afd192c816df6457d06c75`  
		Last Modified: Tue, 12 Oct 2021 01:50:51 GMT  
		Size: 54.7 MB (54702895 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid` - linux; 386

```console
$ docker pull debian@sha256:c736009eea3dc163b610a2ca778f35cfcf5952a8e7d3073cd7b5c9e000710231
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **56.7 MB (56716103 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cedbf171351223a2dc6f4104484770d2fc6b81b2c3e9ab15dd2a86820d901fac`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:27 GMT
ADD file:9ab0c16547aac0a32292246b35737ca5260c5942d5e26ca1381be39c9e321ee5 in / 
# Tue, 12 Oct 2021 01:41:27 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c36fe1aa4217cf0326227a0bdcadc16e85d2670bd6485518167903dfc6934c65`  
		Last Modified: Tue, 12 Oct 2021 01:50:33 GMT  
		Size: 56.7 MB (56716103 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid` - linux; mips64le

```console
$ docker pull debian@sha256:3052a2abff14664ebb15733f8d8068809bdfce0eb3b5e6c5a74da9369c5a5565
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.3 MB (54313468 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ecc78b5ee81d2e1cd95cbe5f646643ce3f49fc70cccd1df75080fcdfce354239`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:13:27 GMT
ADD file:27e1d46380432bbd287bef3c049b6707a409fa2f49f07e76f4826c6ee57cfca9 in / 
# Tue, 12 Oct 2021 01:13:28 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c7afe2e09744237d99a12c5f86f33ba55800e5acee482662abac2c64c64a7824`  
		Last Modified: Tue, 12 Oct 2021 01:23:41 GMT  
		Size: 54.3 MB (54313468 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid` - linux; ppc64le

```console
$ docker pull debian@sha256:1c583a08e96faf7d7a86a7ec39c475105142bb1504c8648deb10cf17fbce59c6
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **59.9 MB (59889168 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:c6f2d9026bc5232f5a623044f4c4177ec590277a4a52ce63a04d32582ffe8422`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:27:58 GMT
ADD file:975422ef5390ff94fc050a84a9840e39188158f1c74f7b6dfde93829787b7c8d in / 
# Tue, 12 Oct 2021 01:28:07 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:3794e49fdef56187e1a7ffe8a3c0d0dda8a5b5c159e370831000df5e5f638479`  
		Last Modified: Tue, 12 Oct 2021 01:41:02 GMT  
		Size: 59.9 MB (59889168 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid` - linux; riscv64

```console
$ docker pull debian@sha256:3b66eece44d970d4a5c2b199f263248ce43a709bc5600866b296dcf959b89b38
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **51.5 MB (51516608 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b7e86a02a6ca8b1048ca14491b9efc714383f9623ec2ba588f4fec8d804d8f13`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:14:22 GMT
ADD file:bd71d5680d1006cf090b0a295547cb405dc17469df91f3492267dbd5019f25c5 in / 
# Tue, 12 Oct 2021 01:14:25 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6f088a81a8bb9ab030ef8f6d37a5e280f5ad79eaf2817310f6498ffe43ca2dac`  
		Last Modified: Tue, 12 Oct 2021 01:30:29 GMT  
		Size: 51.5 MB (51516608 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid` - linux; s390x

```console
$ docker pull debian@sha256:fe9681d2dc796a7ddb9976bd1d3e9317d35a782c52e0ea5aa7a7dd5fe4dd63e5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.9 MB (53940740 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:217b6e3b937fd1fc843f464edf09d822d10ff4528c06e63a2dadfadd205eb33a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:43:35 GMT
ADD file:d0d1a83115052a10810bdba27eb855359796f604b655de31492b0699abf1d546 in / 
# Tue, 12 Oct 2021 00:43:38 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:979a8d8677c5949943057dfc51bfa612e3d365d71df4150210296bec00f04cf2`  
		Last Modified: Tue, 12 Oct 2021 00:49:29 GMT  
		Size: 53.9 MB (53940740 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:sid-20211011`

```console
$ docker pull debian@sha256:4e85ef0d6eac69c503ba9e4b4b1957c77468ecbe2c38e453ed0cc0ee0e709869
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 9
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; riscv64
	-	linux; s390x

### `debian:sid-20211011` - linux; amd64

```console
$ docker pull debian@sha256:d65918b2bcb6790c777771938ccae929a893b12fe8e3157004795c0c74969c5b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.7 MB (55687470 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cdd43765522e651a5208a53e039ad192b70dac64643d6202ae00c0c61c05191c`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:21:58 GMT
ADD file:2d684471f088173801ff86eaf94c1d858b74220b84642d73b02bd1bfe0d54b60 in / 
# Tue, 12 Oct 2021 01:21:58 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:ba87283d9c312600aafb7f4149986884d524aae42e0701a90d2fc23c85e977b1`  
		Last Modified: Tue, 12 Oct 2021 01:28:21 GMT  
		Size: 55.7 MB (55687470 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-20211011` - linux; arm variant v5

```console
$ docker pull debian@sha256:9f3667c133e7d9ad5f1c437689087457c0ec33eefee613f0ebf1c441749ba08e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53186352 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:7179ffe402670d6eccfb8b78b0f2cc109035cd6f35187d9795edb81b9688d5b0`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:53:53 GMT
ADD file:2918872df07d47b9a7862abf3b22f39600ab45587fcd02466bdefa775a3fc2dd in / 
# Tue, 12 Oct 2021 00:53:54 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:8f73c109c91bd2f18faab4d6862ba33d940956d6457aaaaeeda5e8f72281edcc`  
		Last Modified: Tue, 12 Oct 2021 01:11:00 GMT  
		Size: 53.2 MB (53186352 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-20211011` - linux; arm variant v7

```console
$ docker pull debian@sha256:7f07c18cbba49f9b8d72121f50f0e7e28a12e0e9a7b320a95890516cbccff830
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.8 MB (50797066 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6b6732764d17da5c6b322f69832b09240b9d6b3f2f5fcd9378560027e895aa29`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:32:13 GMT
ADD file:defbc06b1cc21db01a673f118d34d129435486fcb83814c5c987ff566c61360a in / 
# Tue, 12 Oct 2021 01:32:14 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:153dc8e6452cda134a8143d2ac887e7ebfa01c8568115d6b3172ca0008ef4838`  
		Last Modified: Tue, 12 Oct 2021 01:49:06 GMT  
		Size: 50.8 MB (50797066 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-20211011` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:3e7ed43c0ac67026555919d53f5c3f5be0719496acb23a6b3f9a37fb1ba90ab4
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.7 MB (54702895 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ec700318828577b1596c3328a8b89157631b2c85ffc8a8fdc9ad7128c9d281df`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:36 GMT
ADD file:830671e1709a0164ca60ad1012d3d0202e7e63c74bdbec7df54f308a4d4c8b11 in / 
# Tue, 12 Oct 2021 01:42:37 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:3ed37bc205e0aad196bfd6ba235a1a5fba10e02175afd192c816df6457d06c75`  
		Last Modified: Tue, 12 Oct 2021 01:50:51 GMT  
		Size: 54.7 MB (54702895 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-20211011` - linux; 386

```console
$ docker pull debian@sha256:c736009eea3dc163b610a2ca778f35cfcf5952a8e7d3073cd7b5c9e000710231
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **56.7 MB (56716103 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cedbf171351223a2dc6f4104484770d2fc6b81b2c3e9ab15dd2a86820d901fac`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:27 GMT
ADD file:9ab0c16547aac0a32292246b35737ca5260c5942d5e26ca1381be39c9e321ee5 in / 
# Tue, 12 Oct 2021 01:41:27 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c36fe1aa4217cf0326227a0bdcadc16e85d2670bd6485518167903dfc6934c65`  
		Last Modified: Tue, 12 Oct 2021 01:50:33 GMT  
		Size: 56.7 MB (56716103 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-20211011` - linux; mips64le

```console
$ docker pull debian@sha256:3052a2abff14664ebb15733f8d8068809bdfce0eb3b5e6c5a74da9369c5a5565
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.3 MB (54313468 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ecc78b5ee81d2e1cd95cbe5f646643ce3f49fc70cccd1df75080fcdfce354239`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:13:27 GMT
ADD file:27e1d46380432bbd287bef3c049b6707a409fa2f49f07e76f4826c6ee57cfca9 in / 
# Tue, 12 Oct 2021 01:13:28 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c7afe2e09744237d99a12c5f86f33ba55800e5acee482662abac2c64c64a7824`  
		Last Modified: Tue, 12 Oct 2021 01:23:41 GMT  
		Size: 54.3 MB (54313468 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-20211011` - linux; ppc64le

```console
$ docker pull debian@sha256:1c583a08e96faf7d7a86a7ec39c475105142bb1504c8648deb10cf17fbce59c6
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **59.9 MB (59889168 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:c6f2d9026bc5232f5a623044f4c4177ec590277a4a52ce63a04d32582ffe8422`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:27:58 GMT
ADD file:975422ef5390ff94fc050a84a9840e39188158f1c74f7b6dfde93829787b7c8d in / 
# Tue, 12 Oct 2021 01:28:07 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:3794e49fdef56187e1a7ffe8a3c0d0dda8a5b5c159e370831000df5e5f638479`  
		Last Modified: Tue, 12 Oct 2021 01:41:02 GMT  
		Size: 59.9 MB (59889168 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-20211011` - linux; riscv64

```console
$ docker pull debian@sha256:3b66eece44d970d4a5c2b199f263248ce43a709bc5600866b296dcf959b89b38
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **51.5 MB (51516608 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b7e86a02a6ca8b1048ca14491b9efc714383f9623ec2ba588f4fec8d804d8f13`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:14:22 GMT
ADD file:bd71d5680d1006cf090b0a295547cb405dc17469df91f3492267dbd5019f25c5 in / 
# Tue, 12 Oct 2021 01:14:25 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6f088a81a8bb9ab030ef8f6d37a5e280f5ad79eaf2817310f6498ffe43ca2dac`  
		Last Modified: Tue, 12 Oct 2021 01:30:29 GMT  
		Size: 51.5 MB (51516608 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-20211011` - linux; s390x

```console
$ docker pull debian@sha256:fe9681d2dc796a7ddb9976bd1d3e9317d35a782c52e0ea5aa7a7dd5fe4dd63e5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.9 MB (53940740 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:217b6e3b937fd1fc843f464edf09d822d10ff4528c06e63a2dadfadd205eb33a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:43:35 GMT
ADD file:d0d1a83115052a10810bdba27eb855359796f604b655de31492b0699abf1d546 in / 
# Tue, 12 Oct 2021 00:43:38 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:979a8d8677c5949943057dfc51bfa612e3d365d71df4150210296bec00f04cf2`  
		Last Modified: Tue, 12 Oct 2021 00:49:29 GMT  
		Size: 53.9 MB (53940740 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:sid-20211011-slim`

```console
$ docker pull debian@sha256:c8fafcd3e291857ebb5aaa15e86fbb5503edf73f5124cb6e02e30fd1b7e4cf12
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 9
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; riscv64
	-	linux; s390x

### `debian:sid-20211011-slim` - linux; amd64

```console
$ docker pull debian@sha256:025254bbfc82262031a971f63960d24ecf012eaf972e608b61756c631a34e6e1
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **31.7 MB (31731935 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9757525a4c83cdfed2a25cf6dd35a58ca447be791107d4424016205a37094286`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:22:08 GMT
ADD file:0f445d032ec45ac68f4ab9aec63d072544f2f037acfc934d6ad83c05e7a1c8f2 in / 
# Tue, 12 Oct 2021 01:22:08 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:fe4bf86b3222e855d7fbd5d536cbfd58a86d97b22ac9d818009064b8f0c9f70d`  
		Last Modified: Tue, 12 Oct 2021 01:28:35 GMT  
		Size: 31.7 MB (31731935 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-20211011-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:f943984a7e68c7a598c43182ac2100be6ab756828fe6d7b50d42ae4fea6c28bd
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.2 MB (29236158 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:abdb111ae5b6d0cadcf99ec5c556436f6e90ee20ffc4e18cd48bc36289230cdb`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:54:14 GMT
ADD file:0d62475df50475f297b93f15ebd9558d7d394da8162d787571a086573db86208 in / 
# Tue, 12 Oct 2021 00:54:15 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:267bc1ddf25b0a46bca086d5c858756088941d3e37d3b111768b17b9ef785f52`  
		Last Modified: Tue, 12 Oct 2021 01:11:31 GMT  
		Size: 29.2 MB (29236158 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-20211011-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:4056d12160b7dff77ff4b9fb55d961c338c316ae15958ff018299cb27c4d0016
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **26.8 MB (26844535 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:80b1e2ed697631a802b29456df07790656c3ab6ecf1e341926d19d5b88538210`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:32:37 GMT
ADD file:d9ca247784f1577677312109520a55abbf4398cf0945a671e4f6f06d138e457b in / 
# Tue, 12 Oct 2021 01:32:38 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:51a59c6e8056cfa5147497aa653cc2009174167aaf96d66180c3076399265071`  
		Last Modified: Tue, 12 Oct 2021 01:49:33 GMT  
		Size: 26.8 MB (26844535 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-20211011-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:c414ac6ff2f42868ab9d6a9b02d90690c598400306bb9bfe5e4a2a6461fcee8a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.7 MB (30748029 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:354990caee2d357d7107faff17cacd1d5f4fecb4bee72bf626e14a3b2e3667d2`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:44 GMT
ADD file:4fa890043902fb37f641132c1f05eb54a6426bc5acf42168b1325997d27630dd in / 
# Tue, 12 Oct 2021 01:42:45 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:72e9d9bbff420a8b05de6a0fca437727849fcd865412156e74a3ed3c2b31acc1`  
		Last Modified: Tue, 12 Oct 2021 01:51:06 GMT  
		Size: 30.7 MB (30748029 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-20211011-slim` - linux; 386

```console
$ docker pull debian@sha256:32b2f8d196a9f1747445653374060c6238a55b50c7214b8672b6b44a9b25a799
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **32.8 MB (32763456 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f74dcbe232aebc14a4ba21d9b45a955b2072340486ab485b9558d7279774b7bd`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:37 GMT
ADD file:aa104a082fed2b6420fcb08a1b08b7a35c9d1cba634e3870a61651e0c4cf2121 in / 
# Tue, 12 Oct 2021 01:41:38 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:080c5fa94c94c2285e3a5531cf014af5b9de902062387455579db76e75dc4e20`  
		Last Modified: Tue, 12 Oct 2021 01:50:51 GMT  
		Size: 32.8 MB (32763456 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-20211011-slim` - linux; mips64le

```console
$ docker pull debian@sha256:2941665bafbe82217b7b3d668a7ab38a63cda4893e1ae110f69bfd992c9e6e37
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.4 MB (30360295 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cd11b1196359a14617f6dd3ca909dca5163962510f3aba0cbfaefa4692b566ff`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:13:48 GMT
ADD file:714ca4b115daadc65209eb1d33c550e0471f06cb6c18d7a1fe6f696346fa9eaf in / 
# Tue, 12 Oct 2021 01:13:49 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:9f5541d536fc3aa4428caca822e0eef70cd3eb2f7b23664698d4885cc1314dff`  
		Last Modified: Tue, 12 Oct 2021 01:24:12 GMT  
		Size: 30.4 MB (30360295 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-20211011-slim` - linux; ppc64le

```console
$ docker pull debian@sha256:1db113adc6fa7fef63c252718f6b7d9524fcc696267215cc1c7797bb077a3853
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **35.9 MB (35935037 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a4c0bebab42996c2f005f3c5b4e3a30b11b7bf236da7876dc5658a42359de7b4`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:28:21 GMT
ADD file:e21a675ddadf39c3bb49dd0416d283aa3d9d566b255843cf2ca1cb7c4daf1f03 in / 
# Tue, 12 Oct 2021 01:28:26 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:7b0d2761efc65ea068376f8b2415c02777aed2d1e2dc740230380f851fd81948`  
		Last Modified: Tue, 12 Oct 2021 01:41:36 GMT  
		Size: 35.9 MB (35935037 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-20211011-slim` - linux; riscv64

```console
$ docker pull debian@sha256:b7858e5b27d186b682918e8011a529b0ebc618d704b3ef45d5235929bb5d751b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **27.6 MB (27563282 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b44b4afb292f9c55cca5cccda82c0a4feebb92f7f307daaf9e90f23b3d584c32`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:15:58 GMT
ADD file:f208b8fbff4c817dbc6db3f55963b389fe47f3c48e7f64b85471d9d125d7875f in / 
# Tue, 12 Oct 2021 01:15:59 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:4a0a5284a0463692c510b467e923e38595722dff8de4e8e5bc7aa2dae33c4337`  
		Last Modified: Tue, 12 Oct 2021 01:32:01 GMT  
		Size: 27.6 MB (27563282 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-20211011-slim` - linux; s390x

```console
$ docker pull debian@sha256:42a3541da7b01a4bce2c8b7822a7083b890d7a264b9cf95d86883e1bf1b2d022
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.0 MB (29984176 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:aa7d655906ea245de10ad3c9019a2d68a96f89951b43d8a1f1b9fc7b9bb46268`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:43:46 GMT
ADD file:6665e978a6ce5d437da63343803505d8c4635f430d97517512e64159e3b6ef7c in / 
# Tue, 12 Oct 2021 00:43:48 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c1a10e05e4a1ae95d84297c58d62e4ab685c5e256be0d8395d81ad943a8524db`  
		Last Modified: Tue, 12 Oct 2021 00:49:40 GMT  
		Size: 30.0 MB (29984176 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:sid-slim`

```console
$ docker pull debian@sha256:c8fafcd3e291857ebb5aaa15e86fbb5503edf73f5124cb6e02e30fd1b7e4cf12
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 9
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; riscv64
	-	linux; s390x

### `debian:sid-slim` - linux; amd64

```console
$ docker pull debian@sha256:025254bbfc82262031a971f63960d24ecf012eaf972e608b61756c631a34e6e1
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **31.7 MB (31731935 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9757525a4c83cdfed2a25cf6dd35a58ca447be791107d4424016205a37094286`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:22:08 GMT
ADD file:0f445d032ec45ac68f4ab9aec63d072544f2f037acfc934d6ad83c05e7a1c8f2 in / 
# Tue, 12 Oct 2021 01:22:08 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:fe4bf86b3222e855d7fbd5d536cbfd58a86d97b22ac9d818009064b8f0c9f70d`  
		Last Modified: Tue, 12 Oct 2021 01:28:35 GMT  
		Size: 31.7 MB (31731935 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:f943984a7e68c7a598c43182ac2100be6ab756828fe6d7b50d42ae4fea6c28bd
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.2 MB (29236158 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:abdb111ae5b6d0cadcf99ec5c556436f6e90ee20ffc4e18cd48bc36289230cdb`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:54:14 GMT
ADD file:0d62475df50475f297b93f15ebd9558d7d394da8162d787571a086573db86208 in / 
# Tue, 12 Oct 2021 00:54:15 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:267bc1ddf25b0a46bca086d5c858756088941d3e37d3b111768b17b9ef785f52`  
		Last Modified: Tue, 12 Oct 2021 01:11:31 GMT  
		Size: 29.2 MB (29236158 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:4056d12160b7dff77ff4b9fb55d961c338c316ae15958ff018299cb27c4d0016
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **26.8 MB (26844535 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:80b1e2ed697631a802b29456df07790656c3ab6ecf1e341926d19d5b88538210`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:32:37 GMT
ADD file:d9ca247784f1577677312109520a55abbf4398cf0945a671e4f6f06d138e457b in / 
# Tue, 12 Oct 2021 01:32:38 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:51a59c6e8056cfa5147497aa653cc2009174167aaf96d66180c3076399265071`  
		Last Modified: Tue, 12 Oct 2021 01:49:33 GMT  
		Size: 26.8 MB (26844535 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:c414ac6ff2f42868ab9d6a9b02d90690c598400306bb9bfe5e4a2a6461fcee8a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.7 MB (30748029 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:354990caee2d357d7107faff17cacd1d5f4fecb4bee72bf626e14a3b2e3667d2`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:44 GMT
ADD file:4fa890043902fb37f641132c1f05eb54a6426bc5acf42168b1325997d27630dd in / 
# Tue, 12 Oct 2021 01:42:45 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:72e9d9bbff420a8b05de6a0fca437727849fcd865412156e74a3ed3c2b31acc1`  
		Last Modified: Tue, 12 Oct 2021 01:51:06 GMT  
		Size: 30.7 MB (30748029 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-slim` - linux; 386

```console
$ docker pull debian@sha256:32b2f8d196a9f1747445653374060c6238a55b50c7214b8672b6b44a9b25a799
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **32.8 MB (32763456 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f74dcbe232aebc14a4ba21d9b45a955b2072340486ab485b9558d7279774b7bd`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:37 GMT
ADD file:aa104a082fed2b6420fcb08a1b08b7a35c9d1cba634e3870a61651e0c4cf2121 in / 
# Tue, 12 Oct 2021 01:41:38 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:080c5fa94c94c2285e3a5531cf014af5b9de902062387455579db76e75dc4e20`  
		Last Modified: Tue, 12 Oct 2021 01:50:51 GMT  
		Size: 32.8 MB (32763456 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-slim` - linux; mips64le

```console
$ docker pull debian@sha256:2941665bafbe82217b7b3d668a7ab38a63cda4893e1ae110f69bfd992c9e6e37
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.4 MB (30360295 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cd11b1196359a14617f6dd3ca909dca5163962510f3aba0cbfaefa4692b566ff`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:13:48 GMT
ADD file:714ca4b115daadc65209eb1d33c550e0471f06cb6c18d7a1fe6f696346fa9eaf in / 
# Tue, 12 Oct 2021 01:13:49 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:9f5541d536fc3aa4428caca822e0eef70cd3eb2f7b23664698d4885cc1314dff`  
		Last Modified: Tue, 12 Oct 2021 01:24:12 GMT  
		Size: 30.4 MB (30360295 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-slim` - linux; ppc64le

```console
$ docker pull debian@sha256:1db113adc6fa7fef63c252718f6b7d9524fcc696267215cc1c7797bb077a3853
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **35.9 MB (35935037 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a4c0bebab42996c2f005f3c5b4e3a30b11b7bf236da7876dc5658a42359de7b4`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:28:21 GMT
ADD file:e21a675ddadf39c3bb49dd0416d283aa3d9d566b255843cf2ca1cb7c4daf1f03 in / 
# Tue, 12 Oct 2021 01:28:26 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:7b0d2761efc65ea068376f8b2415c02777aed2d1e2dc740230380f851fd81948`  
		Last Modified: Tue, 12 Oct 2021 01:41:36 GMT  
		Size: 35.9 MB (35935037 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-slim` - linux; riscv64

```console
$ docker pull debian@sha256:b7858e5b27d186b682918e8011a529b0ebc618d704b3ef45d5235929bb5d751b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **27.6 MB (27563282 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b44b4afb292f9c55cca5cccda82c0a4feebb92f7f307daaf9e90f23b3d584c32`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:15:58 GMT
ADD file:f208b8fbff4c817dbc6db3f55963b389fe47f3c48e7f64b85471d9d125d7875f in / 
# Tue, 12 Oct 2021 01:15:59 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:4a0a5284a0463692c510b467e923e38595722dff8de4e8e5bc7aa2dae33c4337`  
		Last Modified: Tue, 12 Oct 2021 01:32:01 GMT  
		Size: 27.6 MB (27563282 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:sid-slim` - linux; s390x

```console
$ docker pull debian@sha256:42a3541da7b01a4bce2c8b7822a7083b890d7a264b9cf95d86883e1bf1b2d022
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.0 MB (29984176 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:aa7d655906ea245de10ad3c9019a2d68a96f89951b43d8a1f1b9fc7b9bb46268`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:43:46 GMT
ADD file:6665e978a6ce5d437da63343803505d8c4635f430d97517512e64159e3b6ef7c in / 
# Tue, 12 Oct 2021 00:43:48 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c1a10e05e4a1ae95d84297c58d62e4ab685c5e256be0d8395d81ad943a8524db`  
		Last Modified: Tue, 12 Oct 2021 00:49:40 GMT  
		Size: 30.0 MB (29984176 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:stable`

```console
$ docker pull debian@sha256:9de81807d160cd87c476ffe90215d85478d1d2e2c1d4973b54d993a34d3986de
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:stable` - linux; amd64

```console
$ docker pull debian@sha256:6a8bad8d20e1ca5ecbb7a314e51df6fca73fcce19af2778550671bdd1cbe7b43
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.9 MB (54917542 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5ef9fdf9b7e2c2341463a3c80366fe01c7c9e4c5942ecf612aeb0b5cb318c03a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:22:19 GMT
ADD file:5b20fa1d06a2543173c0349037f3cc0daf6865433bd0078294f56a1ad74eab99 in / 
# Tue, 12 Oct 2021 01:22:19 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:55cb80d281932941c15ecf1f22a0a0d3aeaf0cd54b119a2c866dc63f1a19d34b`  
		Last Modified: Tue, 12 Oct 2021 01:28:51 GMT  
		Size: 54.9 MB (54917542 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable` - linux; arm variant v5

```console
$ docker pull debian@sha256:f1a787f061d49cec1b933b3d657b38e4b79c311b8cfcf59073409af07fc99193
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **52.5 MB (52452247 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:54806557b8d01619c0f13258f4afebce0b74058d8ce5e1de4b46c4f194e52c0f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:54:40 GMT
ADD file:cf95c47a44c6429c9b78a21a77a25de617d7044ceac8f34076e0e4fda02a1d9a in / 
# Tue, 12 Oct 2021 00:54:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:cafc721d7e171ea8a3201aee9d49e7706edf5e50c628667efd8de159f25a2be5`  
		Last Modified: Tue, 12 Oct 2021 01:12:13 GMT  
		Size: 52.5 MB (52452247 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable` - linux; arm variant v7

```console
$ docker pull debian@sha256:485b88fde08409af5f105cf0bed2fb02ddd450d609412e1e4541b19cce050835
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.1 MB (50118660 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:c078eb0e6e57818dc23241a8945379944507c8f2411f3b3242b06cbc285cb85d`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:33:06 GMT
ADD file:49cf5edfc79e4e3ee46774cffd5252769c7920d2dc27fa3913bad6687620d64c in / 
# Tue, 12 Oct 2021 01:33:07 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:440069d8324183357e1261418d3a9a960b29f433ad2a633e70d73a72fef163a8`  
		Last Modified: Tue, 12 Oct 2021 01:50:11 GMT  
		Size: 50.1 MB (50118660 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:58e0daf62681d24cfcd64f5ab8f1a91e84b93332117393d510042e8634d31b69
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.6 MB (53602996 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:979017a0e0c87fa419e46d2a806d77d12367061a2803f7d16d19bf4e39e5aa52`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:54 GMT
ADD file:804ef9b2fc9c5b697d59a7a917482886a55807bbd36490892c70b3b629517d5b in / 
# Tue, 12 Oct 2021 01:42:54 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:ea317fc754dd760ba835f4b130dfe44ad680eb7b1fa5e5a46c133b48946f0dff`  
		Last Modified: Tue, 12 Oct 2021 01:51:25 GMT  
		Size: 53.6 MB (53602996 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable` - linux; 386

```console
$ docker pull debian@sha256:120621ece1c05c79de79dc03258b4258ec9eaab619cc7356e410eaca0cce7e9c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.9 MB (55923414 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:fba7987c39e47b72e5d8783fdfc47609726ae89bfa2ae54ef6b231d014d9651f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:51 GMT
ADD file:24d67f7b5641ca3ed5b97d965f8ce4f5f4c0387342825f51e77004fbd64dc739 in / 
# Tue, 12 Oct 2021 01:41:51 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6dc2525540e90f643ce691e74944f7fb377c97243f1d150666146756bba0ec27`  
		Last Modified: Tue, 12 Oct 2021 01:51:13 GMT  
		Size: 55.9 MB (55923414 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable` - linux; mips64le

```console
$ docker pull debian@sha256:d6f9c88fd7a0550bcc35cff245eef9cb6fed51ad3c79c949b3f800f5d6191459
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53169812 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a6b56c395f2d39368a76a931611989c4ff9d9212783a24d9f9944fe2697105f5`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:14:15 GMT
ADD file:0e49a2741eab1dd61adf85a42a5f7b1e7b8a801d17e5925ce71a962dd9436843 in / 
# Tue, 12 Oct 2021 01:14:16 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c643a33bbcb2443773c1a3a92228130d1cc6d19d74fe3ed68787c4024751ff66`  
		Last Modified: Tue, 12 Oct 2021 01:24:52 GMT  
		Size: 53.2 MB (53169812 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable` - linux; ppc64le

```console
$ docker pull debian@sha256:cbfd6decaf005166ebfbaba55c25c28a19b52b1f3fe019bd02c15877cbd1d93e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **58.8 MB (58808876 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:48fbf095386a152cddeea622bd2ec7bc98d434cac3b83ebba5552848d26f3d22`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:28:41 GMT
ADD file:50a64f2cbd267a73c297febc1aac2d0a0ff0966d105e37c199477a326aa29e76 in / 
# Tue, 12 Oct 2021 01:28:47 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:f2a063c91861196790fc45eaacac95fcbb278654c161c3397bc453aafb71bc96`  
		Last Modified: Tue, 12 Oct 2021 01:42:23 GMT  
		Size: 58.8 MB (58808876 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable` - linux; s390x

```console
$ docker pull debian@sha256:9920697169fc2f3464f2104978957748669190704e445613fea6707fd57075a5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53192883 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:dd8dad94e40fd66520452a5f35effb6d8aceb1b911e1ebf50cc103d5d8638d61`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:43:57 GMT
ADD file:edc453c86c89f128b79c20c971b4b63e822f652a01b62cba93042f5ab3a41c22 in / 
# Tue, 12 Oct 2021 00:44:00 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2f41532c0354f964b59239357dc1211cd22974215e0e805b74bb664ecd5eb1de`  
		Last Modified: Tue, 12 Oct 2021 00:49:54 GMT  
		Size: 53.2 MB (53192883 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:stable-20211011`

```console
$ docker pull debian@sha256:9de81807d160cd87c476ffe90215d85478d1d2e2c1d4973b54d993a34d3986de
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:stable-20211011` - linux; amd64

```console
$ docker pull debian@sha256:6a8bad8d20e1ca5ecbb7a314e51df6fca73fcce19af2778550671bdd1cbe7b43
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.9 MB (54917542 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5ef9fdf9b7e2c2341463a3c80366fe01c7c9e4c5942ecf612aeb0b5cb318c03a`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:22:19 GMT
ADD file:5b20fa1d06a2543173c0349037f3cc0daf6865433bd0078294f56a1ad74eab99 in / 
# Tue, 12 Oct 2021 01:22:19 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:55cb80d281932941c15ecf1f22a0a0d3aeaf0cd54b119a2c866dc63f1a19d34b`  
		Last Modified: Tue, 12 Oct 2021 01:28:51 GMT  
		Size: 54.9 MB (54917542 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-20211011` - linux; arm variant v5

```console
$ docker pull debian@sha256:f1a787f061d49cec1b933b3d657b38e4b79c311b8cfcf59073409af07fc99193
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **52.5 MB (52452247 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:54806557b8d01619c0f13258f4afebce0b74058d8ce5e1de4b46c4f194e52c0f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:54:40 GMT
ADD file:cf95c47a44c6429c9b78a21a77a25de617d7044ceac8f34076e0e4fda02a1d9a in / 
# Tue, 12 Oct 2021 00:54:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:cafc721d7e171ea8a3201aee9d49e7706edf5e50c628667efd8de159f25a2be5`  
		Last Modified: Tue, 12 Oct 2021 01:12:13 GMT  
		Size: 52.5 MB (52452247 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-20211011` - linux; arm variant v7

```console
$ docker pull debian@sha256:485b88fde08409af5f105cf0bed2fb02ddd450d609412e1e4541b19cce050835
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.1 MB (50118660 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:c078eb0e6e57818dc23241a8945379944507c8f2411f3b3242b06cbc285cb85d`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:33:06 GMT
ADD file:49cf5edfc79e4e3ee46774cffd5252769c7920d2dc27fa3913bad6687620d64c in / 
# Tue, 12 Oct 2021 01:33:07 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:440069d8324183357e1261418d3a9a960b29f433ad2a633e70d73a72fef163a8`  
		Last Modified: Tue, 12 Oct 2021 01:50:11 GMT  
		Size: 50.1 MB (50118660 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-20211011` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:58e0daf62681d24cfcd64f5ab8f1a91e84b93332117393d510042e8634d31b69
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.6 MB (53602996 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:979017a0e0c87fa419e46d2a806d77d12367061a2803f7d16d19bf4e39e5aa52`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:54 GMT
ADD file:804ef9b2fc9c5b697d59a7a917482886a55807bbd36490892c70b3b629517d5b in / 
# Tue, 12 Oct 2021 01:42:54 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:ea317fc754dd760ba835f4b130dfe44ad680eb7b1fa5e5a46c133b48946f0dff`  
		Last Modified: Tue, 12 Oct 2021 01:51:25 GMT  
		Size: 53.6 MB (53602996 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-20211011` - linux; 386

```console
$ docker pull debian@sha256:120621ece1c05c79de79dc03258b4258ec9eaab619cc7356e410eaca0cce7e9c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.9 MB (55923414 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:fba7987c39e47b72e5d8783fdfc47609726ae89bfa2ae54ef6b231d014d9651f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:51 GMT
ADD file:24d67f7b5641ca3ed5b97d965f8ce4f5f4c0387342825f51e77004fbd64dc739 in / 
# Tue, 12 Oct 2021 01:41:51 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6dc2525540e90f643ce691e74944f7fb377c97243f1d150666146756bba0ec27`  
		Last Modified: Tue, 12 Oct 2021 01:51:13 GMT  
		Size: 55.9 MB (55923414 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-20211011` - linux; mips64le

```console
$ docker pull debian@sha256:d6f9c88fd7a0550bcc35cff245eef9cb6fed51ad3c79c949b3f800f5d6191459
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53169812 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a6b56c395f2d39368a76a931611989c4ff9d9212783a24d9f9944fe2697105f5`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:14:15 GMT
ADD file:0e49a2741eab1dd61adf85a42a5f7b1e7b8a801d17e5925ce71a962dd9436843 in / 
# Tue, 12 Oct 2021 01:14:16 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c643a33bbcb2443773c1a3a92228130d1cc6d19d74fe3ed68787c4024751ff66`  
		Last Modified: Tue, 12 Oct 2021 01:24:52 GMT  
		Size: 53.2 MB (53169812 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-20211011` - linux; ppc64le

```console
$ docker pull debian@sha256:cbfd6decaf005166ebfbaba55c25c28a19b52b1f3fe019bd02c15877cbd1d93e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **58.8 MB (58808876 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:48fbf095386a152cddeea622bd2ec7bc98d434cac3b83ebba5552848d26f3d22`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:28:41 GMT
ADD file:50a64f2cbd267a73c297febc1aac2d0a0ff0966d105e37c199477a326aa29e76 in / 
# Tue, 12 Oct 2021 01:28:47 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:f2a063c91861196790fc45eaacac95fcbb278654c161c3397bc453aafb71bc96`  
		Last Modified: Tue, 12 Oct 2021 01:42:23 GMT  
		Size: 58.8 MB (58808876 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-20211011` - linux; s390x

```console
$ docker pull debian@sha256:9920697169fc2f3464f2104978957748669190704e445613fea6707fd57075a5
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53192883 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:dd8dad94e40fd66520452a5f35effb6d8aceb1b911e1ebf50cc103d5d8638d61`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:43:57 GMT
ADD file:edc453c86c89f128b79c20c971b4b63e822f652a01b62cba93042f5ab3a41c22 in / 
# Tue, 12 Oct 2021 00:44:00 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2f41532c0354f964b59239357dc1211cd22974215e0e805b74bb664ecd5eb1de`  
		Last Modified: Tue, 12 Oct 2021 00:49:54 GMT  
		Size: 53.2 MB (53192883 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:stable-20211011-slim`

```console
$ docker pull debian@sha256:6d6db11a446181868d088e4638f1392ea1489ce24a77d352d0d75d8e56307909
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:stable-20211011-slim` - linux; amd64

```console
$ docker pull debian@sha256:edb0a5915350ee6e2fedd8f9d0fe2e7f956f7a58f7f41b5e836e0bb7543e48a1
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **31.4 MB (31357347 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:11ed9f070876f7382b66e9ce1bcad0f5e40233f7f08a464bfde4477052500d0f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:22:31 GMT
ADD file:4f362433f6080825bee86a9f1c682bf4570f4057808bca78dd96338dca29c311 in / 
# Tue, 12 Oct 2021 01:22:31 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:adce49e07b069b00a50d0d61d9ee54a9b59b4b2cb77409ae75d28a1b976448db`  
		Last Modified: Tue, 12 Oct 2021 01:29:11 GMT  
		Size: 31.4 MB (31357347 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-20211011-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:b4350ec26fea1073d7498e82a410581c0b24e767c5d08a74e9cd7e1c1144a889
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **28.9 MB (28899694 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9c659a92c5d2e8754b8cd5879edbe847f5615b0cbfd13764b5bd3b8d02a1be51`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:55:12 GMT
ADD file:cbab9e3cfa5e8db683241e2f7954282bbbeba8f4c1c416d433e2f50ad37db705 in / 
# Tue, 12 Oct 2021 00:55:13 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:652ea08cf58402bb1137be61a410591890b542fa2fc82c6cb7bda2de1ae042b7`  
		Last Modified: Tue, 12 Oct 2021 01:12:51 GMT  
		Size: 28.9 MB (28899694 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-20211011-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:2ed3381dc520b76100dd6231c4d6cdbd76acec0d60a1532b9ec816f28ec17517
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **26.6 MB (26561055 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e48abc070ffd5f65a81e8c01f5901e0999abadebdfbde49ef50f959f4937d07d`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:33:39 GMT
ADD file:306bee95e9c083b8c53c224f1c95c4e0916066846898cc85fce2c4144a8f0cd6 in / 
# Tue, 12 Oct 2021 01:33:40 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:1a64e7c91f064d51d2fd3f2423bc75933ea608328a78574aad7a9c88c3875eb8`  
		Last Modified: Tue, 12 Oct 2021 01:50:44 GMT  
		Size: 26.6 MB (26561055 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-20211011-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:00bdd2cd7e6bba43444604b453d7895d5bd5fc52ec2a7b0afd2c390a9900b58b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.0 MB (30043947 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6b276e5b87dc34f09ad96d9eda412c63b5ceaa8d4333a3eeffe2633ed444236d`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:07 GMT
ADD file:a141cb3e243a0f0855631b8079045d039a85e867695553bc78439b4bf01c8616 in / 
# Tue, 12 Oct 2021 01:43:07 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:51da15902d5edd2c3ceaeb3d5051c3f5d0a1fb504bc463de8066e605a3f15cac`  
		Last Modified: Tue, 12 Oct 2021 01:51:48 GMT  
		Size: 30.0 MB (30043947 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-20211011-slim` - linux; 386

```console
$ docker pull debian@sha256:a10944bbb2cc079633dcbd85be26a633814cce8fe453e8130017d8f749873c70
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **32.4 MB (32370374 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:182cb73a54fc2611fdb8b3def1deb66c530eb51d7ab6f71d4ef23a40bf0773a6`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:07 GMT
ADD file:1142f2f5c40a76101ddf0224dad6d4f7524c7eedd06db2bc1912e9ea5eea4fed in / 
# Tue, 12 Oct 2021 01:42:07 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:1e6173dfff3ac1867162f6a4f7b2ce356942e122d4b6143ef90e6cb0fbd2bb52`  
		Last Modified: Tue, 12 Oct 2021 01:51:38 GMT  
		Size: 32.4 MB (32370374 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-20211011-slim` - linux; mips64le

```console
$ docker pull debian@sha256:a20313a0acb3000f827f9f8aaae7f3c33d246e130538a5b47225649b68e78ee9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.6 MB (29618729 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8cb8fea9bfeed77cf7ba0c072c921d0dfaf147a61b6e2a8b7e55221daeb76feb`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:14:42 GMT
ADD file:46e35382664a6e3726c33a3c7c76be2e48446e27eb64681767f23a9fd291bc85 in / 
# Tue, 12 Oct 2021 01:14:43 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6d182e03917a80138113af4d5d173014a12ee3690c9c029772d1a7bf22e15465`  
		Last Modified: Tue, 12 Oct 2021 01:25:34 GMT  
		Size: 29.6 MB (29618729 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-20211011-slim` - linux; ppc64le

```console
$ docker pull debian@sha256:8db5684ffe7ed1237db13c32675ab52e1fa17019cf44079725da72f2f4890330
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **35.3 MB (35258684 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e9771c04353c16a770e1e9a59ccf81f9d52934d37534ed284adedbc6c938ea60`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:29:20 GMT
ADD file:d79079de0552b16f985e2090473a38a7fb64d93f17af19025f98a0b8c630abf9 in / 
# Tue, 12 Oct 2021 01:29:24 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:0c92e5a5aea29b9e3de17e7228e4bca42228fcf6e447a558db18ce34a67eeb0c`  
		Last Modified: Tue, 12 Oct 2021 01:43:05 GMT  
		Size: 35.3 MB (35258684 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-20211011-slim` - linux; s390x

```console
$ docker pull debian@sha256:bdd6e283a992bbd2ecf2574d28a3e8a510be65bbea0c5e80aa49b410f2bae3fc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.6 MB (29641207 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4b4f4978f72e042065c09f6a961858efb6c75867f8ecb828c56356acc558108d`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:44:13 GMT
ADD file:477ffbcd999d81cc8e830ca9f8ae9991eb21cd3968dd5e50fcf817792bdb30c0 in / 
# Tue, 12 Oct 2021 00:44:15 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:e39be091b20ca683565fcae56fc2ba6848a4f4b424bb79407099ece6fae9b821`  
		Last Modified: Tue, 12 Oct 2021 00:50:11 GMT  
		Size: 29.6 MB (29641207 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:stable-backports`

```console
$ docker pull debian@sha256:188b9834d350fe721985e5d2b688d4f6501cf969e3abbd7debf743fa378df05b
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:stable-backports` - linux; amd64

```console
$ docker pull debian@sha256:3572883be4989594ccc7737690dcd8d6bba9f216624eb6b648df88e1b878e3e6
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.9 MB (54917768 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:23781c8e12e1ea8e99519ca57dccd36b12af9ad1b22a1e22c346ce72205454d1`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:22:19 GMT
ADD file:5b20fa1d06a2543173c0349037f3cc0daf6865433bd0078294f56a1ad74eab99 in / 
# Tue, 12 Oct 2021 01:22:19 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:22:23 GMT
RUN echo 'deb http://deb.debian.org/debian stable-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:55cb80d281932941c15ecf1f22a0a0d3aeaf0cd54b119a2c866dc63f1a19d34b`  
		Last Modified: Tue, 12 Oct 2021 01:28:51 GMT  
		Size: 54.9 MB (54917542 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:426cff2beeb71a8c45114968349c6bd5120ae688450ed98175ac73964c5ea2c8`  
		Last Modified: Tue, 12 Oct 2021 01:29:00 GMT  
		Size: 226.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-backports` - linux; arm variant v5

```console
$ docker pull debian@sha256:8411f1b98fab95365868dddedc49f257d8bd34f8cfd09de362fb07008e198874
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **52.5 MB (52452469 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:335d4735d05973b97cf063a64d828fead3f89a833b3259afd28f36f3265cebbb`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:54:40 GMT
ADD file:cf95c47a44c6429c9b78a21a77a25de617d7044ceac8f34076e0e4fda02a1d9a in / 
# Tue, 12 Oct 2021 00:54:42 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 00:54:54 GMT
RUN echo 'deb http://deb.debian.org/debian stable-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:cafc721d7e171ea8a3201aee9d49e7706edf5e50c628667efd8de159f25a2be5`  
		Last Modified: Tue, 12 Oct 2021 01:12:13 GMT  
		Size: 52.5 MB (52452247 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:d66dd91a34f1246a22d3d26701d91d8ee7141d8d53ee27146fe37c64e149e748`  
		Last Modified: Tue, 12 Oct 2021 01:12:26 GMT  
		Size: 222.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-backports` - linux; arm variant v7

```console
$ docker pull debian@sha256:af18ebd8fc6a3da1c9ade8a3f05be8ba7643840480ae0b255a6b6ab3503ad492
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.1 MB (50118886 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4633dda5ca89d26db462165084ad1299493a0d6912c7282d5c5f138690e8d105`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:33:06 GMT
ADD file:49cf5edfc79e4e3ee46774cffd5252769c7920d2dc27fa3913bad6687620d64c in / 
# Tue, 12 Oct 2021 01:33:07 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:33:19 GMT
RUN echo 'deb http://deb.debian.org/debian stable-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:440069d8324183357e1261418d3a9a960b29f433ad2a633e70d73a72fef163a8`  
		Last Modified: Tue, 12 Oct 2021 01:50:11 GMT  
		Size: 50.1 MB (50118660 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:db4c8e8a191ba872b26eed97b664e9be91b114a1cca80f1397f8836caa3d81de`  
		Last Modified: Tue, 12 Oct 2021 01:50:22 GMT  
		Size: 226.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-backports` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:0c19b0a2e053c6e4abaf059483c30fbbba7c2bc12198b2ce94d0cb24bf9d1c03
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.6 MB (53603219 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a5409ac2454f89fc067771e71d15bfd83fe7ba9802bf760f6888088b14aca980`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:54 GMT
ADD file:804ef9b2fc9c5b697d59a7a917482886a55807bbd36490892c70b3b629517d5b in / 
# Tue, 12 Oct 2021 01:42:54 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:42:59 GMT
RUN echo 'deb http://deb.debian.org/debian stable-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:ea317fc754dd760ba835f4b130dfe44ad680eb7b1fa5e5a46c133b48946f0dff`  
		Last Modified: Tue, 12 Oct 2021 01:51:25 GMT  
		Size: 53.6 MB (53602996 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:86d0027defdc8ae6475a265cbe6d65920cbdb99bcd1c4d3b5d6dcf55ebcd2799`  
		Last Modified: Tue, 12 Oct 2021 01:51:35 GMT  
		Size: 223.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-backports` - linux; 386

```console
$ docker pull debian@sha256:dd72ccd21610dc89eed62726fa0b0574b66c0f7f28ea62045f77822c786b6e38
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.9 MB (55923640 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:fa6478a10e09e58eb4f314f09b57f0c35acc1dc42911c28ee342d50a5a82fd73`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:51 GMT
ADD file:24d67f7b5641ca3ed5b97d965f8ce4f5f4c0387342825f51e77004fbd64dc739 in / 
# Tue, 12 Oct 2021 01:41:51 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:41:57 GMT
RUN echo 'deb http://deb.debian.org/debian stable-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:6dc2525540e90f643ce691e74944f7fb377c97243f1d150666146756bba0ec27`  
		Last Modified: Tue, 12 Oct 2021 01:51:13 GMT  
		Size: 55.9 MB (55923414 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:7fb9bc894e964fd63a277f3371167b7bb8f5dbb8679a12863e2afd665131f1b0`  
		Last Modified: Tue, 12 Oct 2021 01:51:24 GMT  
		Size: 226.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-backports` - linux; mips64le

```console
$ docker pull debian@sha256:e575729f9c56135dd9fa0ffcc393f8c6f84a57635a8ba5d3099c15c1062c159b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53170037 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b584bb19c57defea2c77e3b442be778bbdeaac41453d020adaa31521c4d03d83`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:14:15 GMT
ADD file:0e49a2741eab1dd61adf85a42a5f7b1e7b8a801d17e5925ce71a962dd9436843 in / 
# Tue, 12 Oct 2021 01:14:16 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:14:24 GMT
RUN echo 'deb http://deb.debian.org/debian stable-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:c643a33bbcb2443773c1a3a92228130d1cc6d19d74fe3ed68787c4024751ff66`  
		Last Modified: Tue, 12 Oct 2021 01:24:52 GMT  
		Size: 53.2 MB (53169812 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:1482b2dc356490d54837ff1a99d9b0fe904f804192af6cb7b49c315ea681d137`  
		Last Modified: Tue, 12 Oct 2021 01:25:07 GMT  
		Size: 225.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-backports` - linux; ppc64le

```console
$ docker pull debian@sha256:f9d1a613941ab711d46f651e2bdff5dfb03990319e2862f183b23d7f0be49d02
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **58.8 MB (58809102 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:bf2cc8c6a11ac4da588195f52aec612546ecebdf7035b94a4af27f1cd2a3a5d9`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:28:41 GMT
ADD file:50a64f2cbd267a73c297febc1aac2d0a0ff0966d105e37c199477a326aa29e76 in / 
# Tue, 12 Oct 2021 01:28:47 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:29:07 GMT
RUN echo 'deb http://deb.debian.org/debian stable-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:f2a063c91861196790fc45eaacac95fcbb278654c161c3397bc453aafb71bc96`  
		Last Modified: Tue, 12 Oct 2021 01:42:23 GMT  
		Size: 58.8 MB (58808876 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:57eddd7aa238ee81fabbae668fbfcc0a525fba6c00d486f0d622a00a6347a06a`  
		Last Modified: Tue, 12 Oct 2021 01:42:34 GMT  
		Size: 226.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-backports` - linux; s390x

```console
$ docker pull debian@sha256:b0970adb961d3c3c3ee3809bd8fc25e8939d8bde9b683ad3dbd4261df76fa550
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53193109 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ae8e2621d2336c802aedaefa842c5219cbf7ef7df1eee5e8664ded72053c7beb`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:43:57 GMT
ADD file:edc453c86c89f128b79c20c971b4b63e822f652a01b62cba93042f5ab3a41c22 in / 
# Tue, 12 Oct 2021 00:44:00 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 00:44:06 GMT
RUN echo 'deb http://deb.debian.org/debian stable-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:2f41532c0354f964b59239357dc1211cd22974215e0e805b74bb664ecd5eb1de`  
		Last Modified: Tue, 12 Oct 2021 00:49:54 GMT  
		Size: 53.2 MB (53192883 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:bfef84eea5d79a082c54dc169326ce1c393691cdedc56c0c304ce7fa1e6a96cc`  
		Last Modified: Tue, 12 Oct 2021 00:50:02 GMT  
		Size: 226.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:stable-slim`

```console
$ docker pull debian@sha256:6d6db11a446181868d088e4638f1392ea1489ce24a77d352d0d75d8e56307909
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:stable-slim` - linux; amd64

```console
$ docker pull debian@sha256:edb0a5915350ee6e2fedd8f9d0fe2e7f956f7a58f7f41b5e836e0bb7543e48a1
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **31.4 MB (31357347 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:11ed9f070876f7382b66e9ce1bcad0f5e40233f7f08a464bfde4477052500d0f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:22:31 GMT
ADD file:4f362433f6080825bee86a9f1c682bf4570f4057808bca78dd96338dca29c311 in / 
# Tue, 12 Oct 2021 01:22:31 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:adce49e07b069b00a50d0d61d9ee54a9b59b4b2cb77409ae75d28a1b976448db`  
		Last Modified: Tue, 12 Oct 2021 01:29:11 GMT  
		Size: 31.4 MB (31357347 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:b4350ec26fea1073d7498e82a410581c0b24e767c5d08a74e9cd7e1c1144a889
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **28.9 MB (28899694 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9c659a92c5d2e8754b8cd5879edbe847f5615b0cbfd13764b5bd3b8d02a1be51`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:55:12 GMT
ADD file:cbab9e3cfa5e8db683241e2f7954282bbbeba8f4c1c416d433e2f50ad37db705 in / 
# Tue, 12 Oct 2021 00:55:13 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:652ea08cf58402bb1137be61a410591890b542fa2fc82c6cb7bda2de1ae042b7`  
		Last Modified: Tue, 12 Oct 2021 01:12:51 GMT  
		Size: 28.9 MB (28899694 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:2ed3381dc520b76100dd6231c4d6cdbd76acec0d60a1532b9ec816f28ec17517
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **26.6 MB (26561055 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e48abc070ffd5f65a81e8c01f5901e0999abadebdfbde49ef50f959f4937d07d`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:33:39 GMT
ADD file:306bee95e9c083b8c53c224f1c95c4e0916066846898cc85fce2c4144a8f0cd6 in / 
# Tue, 12 Oct 2021 01:33:40 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:1a64e7c91f064d51d2fd3f2423bc75933ea608328a78574aad7a9c88c3875eb8`  
		Last Modified: Tue, 12 Oct 2021 01:50:44 GMT  
		Size: 26.6 MB (26561055 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:00bdd2cd7e6bba43444604b453d7895d5bd5fc52ec2a7b0afd2c390a9900b58b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.0 MB (30043947 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6b276e5b87dc34f09ad96d9eda412c63b5ceaa8d4333a3eeffe2633ed444236d`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:07 GMT
ADD file:a141cb3e243a0f0855631b8079045d039a85e867695553bc78439b4bf01c8616 in / 
# Tue, 12 Oct 2021 01:43:07 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:51da15902d5edd2c3ceaeb3d5051c3f5d0a1fb504bc463de8066e605a3f15cac`  
		Last Modified: Tue, 12 Oct 2021 01:51:48 GMT  
		Size: 30.0 MB (30043947 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-slim` - linux; 386

```console
$ docker pull debian@sha256:a10944bbb2cc079633dcbd85be26a633814cce8fe453e8130017d8f749873c70
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **32.4 MB (32370374 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:182cb73a54fc2611fdb8b3def1deb66c530eb51d7ab6f71d4ef23a40bf0773a6`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:07 GMT
ADD file:1142f2f5c40a76101ddf0224dad6d4f7524c7eedd06db2bc1912e9ea5eea4fed in / 
# Tue, 12 Oct 2021 01:42:07 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:1e6173dfff3ac1867162f6a4f7b2ce356942e122d4b6143ef90e6cb0fbd2bb52`  
		Last Modified: Tue, 12 Oct 2021 01:51:38 GMT  
		Size: 32.4 MB (32370374 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-slim` - linux; mips64le

```console
$ docker pull debian@sha256:a20313a0acb3000f827f9f8aaae7f3c33d246e130538a5b47225649b68e78ee9
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.6 MB (29618729 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8cb8fea9bfeed77cf7ba0c072c921d0dfaf147a61b6e2a8b7e55221daeb76feb`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:14:42 GMT
ADD file:46e35382664a6e3726c33a3c7c76be2e48446e27eb64681767f23a9fd291bc85 in / 
# Tue, 12 Oct 2021 01:14:43 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6d182e03917a80138113af4d5d173014a12ee3690c9c029772d1a7bf22e15465`  
		Last Modified: Tue, 12 Oct 2021 01:25:34 GMT  
		Size: 29.6 MB (29618729 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-slim` - linux; ppc64le

```console
$ docker pull debian@sha256:8db5684ffe7ed1237db13c32675ab52e1fa17019cf44079725da72f2f4890330
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **35.3 MB (35258684 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e9771c04353c16a770e1e9a59ccf81f9d52934d37534ed284adedbc6c938ea60`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:29:20 GMT
ADD file:d79079de0552b16f985e2090473a38a7fb64d93f17af19025f98a0b8c630abf9 in / 
# Tue, 12 Oct 2021 01:29:24 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:0c92e5a5aea29b9e3de17e7228e4bca42228fcf6e447a558db18ce34a67eeb0c`  
		Last Modified: Tue, 12 Oct 2021 01:43:05 GMT  
		Size: 35.3 MB (35258684 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stable-slim` - linux; s390x

```console
$ docker pull debian@sha256:bdd6e283a992bbd2ecf2574d28a3e8a510be65bbea0c5e80aa49b410f2bae3fc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.6 MB (29641207 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4b4f4978f72e042065c09f6a961858efb6c75867f8ecb828c56356acc558108d`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:44:13 GMT
ADD file:477ffbcd999d81cc8e830ca9f8ae9991eb21cd3968dd5e50fcf817792bdb30c0 in / 
# Tue, 12 Oct 2021 00:44:15 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:e39be091b20ca683565fcae56fc2ba6848a4f4b424bb79407099ece6fae9b821`  
		Last Modified: Tue, 12 Oct 2021 00:50:11 GMT  
		Size: 29.6 MB (29641207 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:stretch`

```console
$ docker pull debian@sha256:86269e614274db90a1d71dac258c74ed0a867a1d05f67dea6263b0f216ec7724
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386

### `debian:stretch` - linux; amd64

```console
$ docker pull debian@sha256:856ba93f897fb7d86ce4363461fe04b6997ce64a79e8198747e472464d58e0fa
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **45.4 MB (45379651 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5ddf6ebdcdb43d0b4b87d5c50996a322dbfd87aa2482febce521079422ba8b1f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:22:41 GMT
ADD file:084c8b3d38d578aa3910f3786b67b058962dbfdfd4a49d6e0201f2e91670873b in / 
# Tue, 12 Oct 2021 01:22:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2f0ef4316716c8b8925ba3665932f8bf2cef3b072d82de1aeb269d6b8b61b84c`  
		Last Modified: Tue, 12 Oct 2021 01:29:27 GMT  
		Size: 45.4 MB (45379651 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch` - linux; arm variant v5

```console
$ docker pull debian@sha256:80a932816893c940a3478402b574173be5fb7b1be76f35515fe8e1de61341f01
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **44.1 MB (44091932 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:634bcfa0a09aaabd232b426140bfbb01e8cd4fd43562b9698fbb7040133643fd`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:55:37 GMT
ADD file:eb65cc31f82e76c4ea5a8f21e4cad1372399517ee41a759740ff91d1d23b9e44 in / 
# Tue, 12 Oct 2021 00:55:38 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:d2f67fd5897d122073fc9ad347c1fb9faec31555c47b3ee6ff64ca2b553034ed`  
		Last Modified: Tue, 12 Oct 2021 01:13:31 GMT  
		Size: 44.1 MB (44091932 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch` - linux; arm variant v7

```console
$ docker pull debian@sha256:9434c91665ec2a24d73df1cea6956045563ee0d22fbf34946b8f1f3d69b865b8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **42.1 MB (42119423 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3c34ebc14ce655c7307d49d1b2be353ab021b23689a18bd48eb0e75b48b8b092`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:34:07 GMT
ADD file:eb5955ccd54a486767e85aeb1c03ffc6d8667b5476d0cf17b892ca407f1c8853 in / 
# Tue, 12 Oct 2021 01:34:08 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:8e9db3616ec4491d75e6c28254ff2376f24900797e7c4cc464f36a0c502b111f`  
		Last Modified: Tue, 12 Oct 2021 01:51:20 GMT  
		Size: 42.1 MB (42119423 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:fcfa90396a4e6d64fb2a721beefc43faa2e0b706b223581f6a1da33bc4d5cb76
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **43.2 MB (43176697 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:57945afea1d021f3fb89b42983c1ba68716bfc327205f96310c7088b02a848f3`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:16 GMT
ADD file:33c2886dff8a69edd550eb69499bf01658d58e2eff405d4b5d4cd5d5be535faa in / 
# Tue, 12 Oct 2021 01:43:16 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:cd2a530f50c2591d61de58d0e9cf724f6c5a2e1112456f38562e289732464c0c`  
		Last Modified: Tue, 12 Oct 2021 01:52:05 GMT  
		Size: 43.2 MB (43176697 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch` - linux; 386

```console
$ docker pull debian@sha256:8a71adf557086b1f0379142a24cbea502d9bc864b890f48819992b009118c481
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **46.1 MB (46097164 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:596fe040a7233ce2db4046c7bd5ebf3d528c3b7891acb28ee1a7720e7337e5e5`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:19 GMT
ADD file:5a6f08cc731f24357c3702231256fb05eb0a97733779b2ace406ba3c9bf64baa in / 
# Tue, 12 Oct 2021 01:42:19 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:d2ef4e90cca7406b4907a34e3571153fc56e3ecc9b2b5fdbb7787f22877cfec5`  
		Last Modified: Tue, 12 Oct 2021 01:51:58 GMT  
		Size: 46.1 MB (46097164 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:stretch-20211011`

```console
$ docker pull debian@sha256:86269e614274db90a1d71dac258c74ed0a867a1d05f67dea6263b0f216ec7724
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386

### `debian:stretch-20211011` - linux; amd64

```console
$ docker pull debian@sha256:856ba93f897fb7d86ce4363461fe04b6997ce64a79e8198747e472464d58e0fa
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **45.4 MB (45379651 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5ddf6ebdcdb43d0b4b87d5c50996a322dbfd87aa2482febce521079422ba8b1f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:22:41 GMT
ADD file:084c8b3d38d578aa3910f3786b67b058962dbfdfd4a49d6e0201f2e91670873b in / 
# Tue, 12 Oct 2021 01:22:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2f0ef4316716c8b8925ba3665932f8bf2cef3b072d82de1aeb269d6b8b61b84c`  
		Last Modified: Tue, 12 Oct 2021 01:29:27 GMT  
		Size: 45.4 MB (45379651 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch-20211011` - linux; arm variant v5

```console
$ docker pull debian@sha256:80a932816893c940a3478402b574173be5fb7b1be76f35515fe8e1de61341f01
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **44.1 MB (44091932 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:634bcfa0a09aaabd232b426140bfbb01e8cd4fd43562b9698fbb7040133643fd`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:55:37 GMT
ADD file:eb65cc31f82e76c4ea5a8f21e4cad1372399517ee41a759740ff91d1d23b9e44 in / 
# Tue, 12 Oct 2021 00:55:38 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:d2f67fd5897d122073fc9ad347c1fb9faec31555c47b3ee6ff64ca2b553034ed`  
		Last Modified: Tue, 12 Oct 2021 01:13:31 GMT  
		Size: 44.1 MB (44091932 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch-20211011` - linux; arm variant v7

```console
$ docker pull debian@sha256:9434c91665ec2a24d73df1cea6956045563ee0d22fbf34946b8f1f3d69b865b8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **42.1 MB (42119423 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3c34ebc14ce655c7307d49d1b2be353ab021b23689a18bd48eb0e75b48b8b092`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:34:07 GMT
ADD file:eb5955ccd54a486767e85aeb1c03ffc6d8667b5476d0cf17b892ca407f1c8853 in / 
# Tue, 12 Oct 2021 01:34:08 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:8e9db3616ec4491d75e6c28254ff2376f24900797e7c4cc464f36a0c502b111f`  
		Last Modified: Tue, 12 Oct 2021 01:51:20 GMT  
		Size: 42.1 MB (42119423 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch-20211011` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:fcfa90396a4e6d64fb2a721beefc43faa2e0b706b223581f6a1da33bc4d5cb76
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **43.2 MB (43176697 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:57945afea1d021f3fb89b42983c1ba68716bfc327205f96310c7088b02a848f3`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:16 GMT
ADD file:33c2886dff8a69edd550eb69499bf01658d58e2eff405d4b5d4cd5d5be535faa in / 
# Tue, 12 Oct 2021 01:43:16 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:cd2a530f50c2591d61de58d0e9cf724f6c5a2e1112456f38562e289732464c0c`  
		Last Modified: Tue, 12 Oct 2021 01:52:05 GMT  
		Size: 43.2 MB (43176697 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch-20211011` - linux; 386

```console
$ docker pull debian@sha256:8a71adf557086b1f0379142a24cbea502d9bc864b890f48819992b009118c481
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **46.1 MB (46097164 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:596fe040a7233ce2db4046c7bd5ebf3d528c3b7891acb28ee1a7720e7337e5e5`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:19 GMT
ADD file:5a6f08cc731f24357c3702231256fb05eb0a97733779b2ace406ba3c9bf64baa in / 
# Tue, 12 Oct 2021 01:42:19 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:d2ef4e90cca7406b4907a34e3571153fc56e3ecc9b2b5fdbb7787f22877cfec5`  
		Last Modified: Tue, 12 Oct 2021 01:51:58 GMT  
		Size: 46.1 MB (46097164 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:stretch-20211011-slim`

```console
$ docker pull debian@sha256:5bb84147ba0e80fe348af4bc2368148de74721444a2109a8625c98ea04f709f2
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386

### `debian:stretch-20211011-slim` - linux; amd64

```console
$ docker pull debian@sha256:51b5ea69ad41c56e2a79c874a7be9cfec4dfd3a3da3d30036625778f3074f8e8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **22.5 MB (22527572 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:22816b63493638811cfd75f71aa60e8801bbf8461e9c8c1b4974cb0487c9eaec`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:22:52 GMT
ADD file:1220579e31585dec45ca8e35874eb689018ed026a1f23b7906f791c0279671e0 in / 
# Tue, 12 Oct 2021 01:22:53 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:eec53b8a5053c739b5b685cb372b38eea3286ab6626532bad963291f76357c5f`  
		Last Modified: Tue, 12 Oct 2021 01:29:50 GMT  
		Size: 22.5 MB (22527572 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch-20211011-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:a9ecf4dbe81b23312dfcf0e748b5cd18f248d83ea8ce39930ef8c63182f03a70
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **21.2 MB (21204312 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ee231f2410bd399183d952d81cf4a7e4244adb8dbdfecc61796f1ca35e186e4b`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:56:09 GMT
ADD file:7d9fa668f5eefa7aa63b53766fa8274d9a23658bf6e4e07fbbbad36c489ad80b in / 
# Tue, 12 Oct 2021 00:56:10 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:79e3fe0e4dd5ba1fd349b975b6ae002bcf61c48327bc735bbaeb2964c857701b`  
		Last Modified: Tue, 12 Oct 2021 01:14:12 GMT  
		Size: 21.2 MB (21204312 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch-20211011-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:1963485172787c41bd94382890c4bfbd2d5f154ec526fe895edf392e26f7c2a8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **19.3 MB (19316474 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d3cc824b240e1bf2567d85b0d69a734e5e7242c8c1e35ae7b16b911c53cdd3d2`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:34:42 GMT
ADD file:9bfcfd0aaac802b902b0e842e040a6599c461c90b73579bcacc2fbdda7ec39cb in / 
# Tue, 12 Oct 2021 01:34:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:a1a3620b17011bd36d6f64dfcc8fd7c4cb3da78d19a59efb1b35afcadaf3f6a8`  
		Last Modified: Tue, 12 Oct 2021 01:51:59 GMT  
		Size: 19.3 MB (19316474 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch-20211011-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:fce43bd1a1fb7829efe2424037e93bf46a4b8551d0101998f1461d6a44065ec4
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **20.4 MB (20389450 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:94481b52b5464aa62213c5766e1c738caa9d5fedd95c9da1047847cb9284820b`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:29 GMT
ADD file:ebfc214a3da7b706f0759fd22fa991c905976d2c970b2d59d134753f7cbd5e06 in / 
# Tue, 12 Oct 2021 01:43:29 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:0b0a22641ad2aad782c696887c59bf05ec0b1e7aa1e07902ee51949bab802657`  
		Last Modified: Tue, 12 Oct 2021 01:52:33 GMT  
		Size: 20.4 MB (20389450 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch-20211011-slim` - linux; 386

```console
$ docker pull debian@sha256:3e6f082b2511516bb3f7f216d9611b837e1ce9891ef145c85cd5570b60170ed6
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **23.2 MB (23156692 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5b1775b4f91bd2929fc4e6ab2bde382fa03e1449eb5e29e0e3a032e677a7a882`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:34 GMT
ADD file:4420d4cd2022d6bad7f61fbfaf16f0759ca6c30424974c6076dc7d5c09910d66 in / 
# Tue, 12 Oct 2021 01:42:34 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:3d6d74524a87c2d3253aea0ad3bc52520036015f508d5b113e4c8c6627e92bc2`  
		Last Modified: Tue, 12 Oct 2021 01:52:28 GMT  
		Size: 23.2 MB (23156692 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:stretch-backports`

```console
$ docker pull debian@sha256:ec42feb6ca63049c200e6634444cd70242b9b8b4b5d059be93676503965bab7f
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386

### `debian:stretch-backports` - linux; amd64

```console
$ docker pull debian@sha256:c35a5937a4fa6d7a8d44c8f8b11740aa72e05dcfcbc9d544c4e0e5665c6a741c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **45.4 MB (45379876 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:c69f5f0f46ba80a182d0518d0a41e569da25b19dd8cdde6641a44d4e88e271d0`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:22:41 GMT
ADD file:084c8b3d38d578aa3910f3786b67b058962dbfdfd4a49d6e0201f2e91670873b in / 
# Tue, 12 Oct 2021 01:22:42 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:22:45 GMT
RUN echo 'deb http://deb.debian.org/debian stretch-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:2f0ef4316716c8b8925ba3665932f8bf2cef3b072d82de1aeb269d6b8b61b84c`  
		Last Modified: Tue, 12 Oct 2021 01:29:27 GMT  
		Size: 45.4 MB (45379651 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:1662ad9244d2f850a79beccfd1fdf3ff1bb3f42b8be81321be606afbc8c35d15`  
		Last Modified: Tue, 12 Oct 2021 01:29:41 GMT  
		Size: 225.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch-backports` - linux; arm variant v5

```console
$ docker pull debian@sha256:1d099ae172650b017f420eb12be11288219c4d564b2f47a717df8b6e045d900b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **44.1 MB (44092157 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d70ebe5186a5a6e64d9dbea6a6aa35b76a4a1ad09e01a1cf33ed5c7eacdf0db7`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:55:37 GMT
ADD file:eb65cc31f82e76c4ea5a8f21e4cad1372399517ee41a759740ff91d1d23b9e44 in / 
# Tue, 12 Oct 2021 00:55:38 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 00:55:53 GMT
RUN echo 'deb http://deb.debian.org/debian stretch-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:d2f67fd5897d122073fc9ad347c1fb9faec31555c47b3ee6ff64ca2b553034ed`  
		Last Modified: Tue, 12 Oct 2021 01:13:31 GMT  
		Size: 44.1 MB (44091932 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:2f43c270837fe20d1e49d63cf41a173e30c15fb5c4746779b9123416dbf09fef`  
		Last Modified: Tue, 12 Oct 2021 01:13:50 GMT  
		Size: 225.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch-backports` - linux; arm variant v7

```console
$ docker pull debian@sha256:686113d851c9d39fcb0db5c2a7dcb52aba5edbbfc7b75a4236ccc114def1c692
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **42.1 MB (42119648 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:31a5220b1a4c819d5fd592a824051615ae562537edcccb71692bce03d04015fb`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:34:07 GMT
ADD file:eb5955ccd54a486767e85aeb1c03ffc6d8667b5476d0cf17b892ca407f1c8853 in / 
# Tue, 12 Oct 2021 01:34:08 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:34:23 GMT
RUN echo 'deb http://deb.debian.org/debian stretch-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:8e9db3616ec4491d75e6c28254ff2376f24900797e7c4cc464f36a0c502b111f`  
		Last Modified: Tue, 12 Oct 2021 01:51:20 GMT  
		Size: 42.1 MB (42119423 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:6ebb32228f3f8b248d3f58e4e383e8be69cc128322f049c40a635ce5e5daf964`  
		Last Modified: Tue, 12 Oct 2021 01:51:40 GMT  
		Size: 225.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch-backports` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:1e101fcbcec8666a214cdc2209c5ac4debb03512e352f9ad9b8173fedebe2eeb
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **43.2 MB (43176920 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e22611f3a72b517527aba51e21ea8290353ac16ca953986a081584566177b108`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:16 GMT
ADD file:33c2886dff8a69edd550eb69499bf01658d58e2eff405d4b5d4cd5d5be535faa in / 
# Tue, 12 Oct 2021 01:43:16 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:43:22 GMT
RUN echo 'deb http://deb.debian.org/debian stretch-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:cd2a530f50c2591d61de58d0e9cf724f6c5a2e1112456f38562e289732464c0c`  
		Last Modified: Tue, 12 Oct 2021 01:52:05 GMT  
		Size: 43.2 MB (43176697 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:658cf5076d7bc6461714d5f9258cf0c5f5fc2c1b6cce9569b8c0d50234792789`  
		Last Modified: Tue, 12 Oct 2021 01:52:22 GMT  
		Size: 223.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch-backports` - linux; 386

```console
$ docker pull debian@sha256:44e38d9e14af0799348138b3247afcf774782ee6c3ad832b64ff564cdc9f72d7
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **46.1 MB (46097387 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:33807633aefce93cbfef044203994c10de859717557b4b1ed247eaf4b645a575`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:19 GMT
ADD file:5a6f08cc731f24357c3702231256fb05eb0a97733779b2ace406ba3c9bf64baa in / 
# Tue, 12 Oct 2021 01:42:19 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:42:26 GMT
RUN echo 'deb http://deb.debian.org/debian stretch-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:d2ef4e90cca7406b4907a34e3571153fc56e3ecc9b2b5fdbb7787f22877cfec5`  
		Last Modified: Tue, 12 Oct 2021 01:51:58 GMT  
		Size: 46.1 MB (46097164 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:f71bcbb0d3021d47e1af8a7d9a478e4045b72d0efc0109f3ddb4047dfda10869`  
		Last Modified: Tue, 12 Oct 2021 01:52:16 GMT  
		Size: 223.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:stretch-slim`

```console
$ docker pull debian@sha256:5bb84147ba0e80fe348af4bc2368148de74721444a2109a8625c98ea04f709f2
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 5
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386

### `debian:stretch-slim` - linux; amd64

```console
$ docker pull debian@sha256:51b5ea69ad41c56e2a79c874a7be9cfec4dfd3a3da3d30036625778f3074f8e8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **22.5 MB (22527572 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:22816b63493638811cfd75f71aa60e8801bbf8461e9c8c1b4974cb0487c9eaec`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:22:52 GMT
ADD file:1220579e31585dec45ca8e35874eb689018ed026a1f23b7906f791c0279671e0 in / 
# Tue, 12 Oct 2021 01:22:53 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:eec53b8a5053c739b5b685cb372b38eea3286ab6626532bad963291f76357c5f`  
		Last Modified: Tue, 12 Oct 2021 01:29:50 GMT  
		Size: 22.5 MB (22527572 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:a9ecf4dbe81b23312dfcf0e748b5cd18f248d83ea8ce39930ef8c63182f03a70
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **21.2 MB (21204312 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ee231f2410bd399183d952d81cf4a7e4244adb8dbdfecc61796f1ca35e186e4b`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:56:09 GMT
ADD file:7d9fa668f5eefa7aa63b53766fa8274d9a23658bf6e4e07fbbbad36c489ad80b in / 
# Tue, 12 Oct 2021 00:56:10 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:79e3fe0e4dd5ba1fd349b975b6ae002bcf61c48327bc735bbaeb2964c857701b`  
		Last Modified: Tue, 12 Oct 2021 01:14:12 GMT  
		Size: 21.2 MB (21204312 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:1963485172787c41bd94382890c4bfbd2d5f154ec526fe895edf392e26f7c2a8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **19.3 MB (19316474 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d3cc824b240e1bf2567d85b0d69a734e5e7242c8c1e35ae7b16b911c53cdd3d2`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:34:42 GMT
ADD file:9bfcfd0aaac802b902b0e842e040a6599c461c90b73579bcacc2fbdda7ec39cb in / 
# Tue, 12 Oct 2021 01:34:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:a1a3620b17011bd36d6f64dfcc8fd7c4cb3da78d19a59efb1b35afcadaf3f6a8`  
		Last Modified: Tue, 12 Oct 2021 01:51:59 GMT  
		Size: 19.3 MB (19316474 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:fce43bd1a1fb7829efe2424037e93bf46a4b8551d0101998f1461d6a44065ec4
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **20.4 MB (20389450 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:94481b52b5464aa62213c5766e1c738caa9d5fedd95c9da1047847cb9284820b`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:29 GMT
ADD file:ebfc214a3da7b706f0759fd22fa991c905976d2c970b2d59d134753f7cbd5e06 in / 
# Tue, 12 Oct 2021 01:43:29 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:0b0a22641ad2aad782c696887c59bf05ec0b1e7aa1e07902ee51949bab802657`  
		Last Modified: Tue, 12 Oct 2021 01:52:33 GMT  
		Size: 20.4 MB (20389450 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:stretch-slim` - linux; 386

```console
$ docker pull debian@sha256:3e6f082b2511516bb3f7f216d9611b837e1ce9891ef145c85cd5570b60170ed6
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **23.2 MB (23156692 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5b1775b4f91bd2929fc4e6ab2bde382fa03e1449eb5e29e0e3a032e677a7a882`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:34 GMT
ADD file:4420d4cd2022d6bad7f61fbfaf16f0759ca6c30424974c6076dc7d5c09910d66 in / 
# Tue, 12 Oct 2021 01:42:34 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:3d6d74524a87c2d3253aea0ad3bc52520036015f508d5b113e4c8c6627e92bc2`  
		Last Modified: Tue, 12 Oct 2021 01:52:28 GMT  
		Size: 23.2 MB (23156692 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:testing`

```console
$ docker pull debian@sha256:e3f8186278fd321c0b3460d6d423c1bc81557d09a8974036bf24de55b90d708f
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:testing` - linux; amd64

```console
$ docker pull debian@sha256:76025017aeddaa14bcfcc3f53f3432dc73a2c8977df9350d13d2cc28a591a96c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.4 MB (55445865 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4022f69526eecf543e97b259c48e368c0ac69f539f1f006ac2c9f69fecdc0530`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:23:03 GMT
ADD file:7a2d92b4684fdb24b1c954a390700dbb0a50ce8cc8774b959e562a3652fb0456 in / 
# Tue, 12 Oct 2021 01:23:03 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:91c31f9cd4fd949265f5532465cddce98935dcfa86015a5348b5f47c344d67e0`  
		Last Modified: Tue, 12 Oct 2021 01:30:11 GMT  
		Size: 55.4 MB (55445865 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing` - linux; arm variant v5

```console
$ docker pull debian@sha256:9d678ad4d51a5275fccddf0a00c6733bec3bbc732ec0bdbef5699f5b45e5146b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.0 MB (52964860 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:75bee44fbb8531b937b8fc6677c583c4dfe9dbe3cd74f918877122bd3e295f35`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:56:39 GMT
ADD file:a0ded502d0b8dfe5e897243e08b3778f3adc2f2f41c32568ca6681c428d2896c in / 
# Tue, 12 Oct 2021 00:56:40 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:10d62d8715d4ae164047d47200167e5ce725ffbc04256c778e0aba73de1fd27b`  
		Last Modified: Tue, 12 Oct 2021 01:15:02 GMT  
		Size: 53.0 MB (52964860 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing` - linux; arm variant v7

```console
$ docker pull debian@sha256:dda29e24c653c90ad3b5b4303590c97ac5ba98be2e19b217c58c95539418e0b2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.6 MB (50566085 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b551a927d2971e313f53905918a67117a4e269f9b04623f2b56c9ff128febe9e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:35:12 GMT
ADD file:4927232089e749d84b21863f4f2612c8a799f3039adcc70d971cbea78ed6a7af in / 
# Tue, 12 Oct 2021 01:35:13 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:bec27cf3f9ae45321e058c161bc06cb3640cc32d6d8cc5cd5119b254da7ecfca`  
		Last Modified: Tue, 12 Oct 2021 01:52:46 GMT  
		Size: 50.6 MB (50566085 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:0d97fee927f66da36b469718694d9c2384887faa39b4e510046ba2662763f09d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.5 MB (54465025 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:37726bb7e5af704bc1fabf9de18e72f40569e9d7fd364082fde69118ef50c863`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:39 GMT
ADD file:0d2781f09dc7fd32dad3f41e34a91046910847a56bf128bb53a7cad6c06c1f26 in / 
# Tue, 12 Oct 2021 01:43:40 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:0a75495c66c5fc986e1fa178fa94fe24ac603e1ee8a61ff4b344624e0b8b030e`  
		Last Modified: Tue, 12 Oct 2021 01:52:57 GMT  
		Size: 54.5 MB (54465025 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing` - linux; 386

```console
$ docker pull debian@sha256:47c810fa726452b53ce4e980d4ce1ecdf09c788439b41e2136f09e6157728c7d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **56.5 MB (56480852 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:85200d26145b3631fd03e8a3487999608f30534862eb4ddcb5599cf90b2bf0cd`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:48 GMT
ADD file:15469ad0ddbee66d623cee7627f9ae7a0e09baf6095d23599c5cb2ae7493abae in / 
# Tue, 12 Oct 2021 01:42:49 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:8508f9607417ad80810488921b0ada480e2ce5d8a1cb8f2303e5d0f3a1d2174e`  
		Last Modified: Tue, 12 Oct 2021 01:52:56 GMT  
		Size: 56.5 MB (56480852 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing` - linux; mips64le

```console
$ docker pull debian@sha256:3a8f5e0fbfd8d20e6c718d488d40d2b67bde613080d549f04ead60ccdc2f484c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.1 MB (54069071 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b705bc479a321116963aaaf14b8b38e2fd963bd8b430edea5d866fcd28c8efc1`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:15:09 GMT
ADD file:3d46a1b0da2f717a6fcd5a81011ddf94c751043fb103b98d488647a531fed650 in / 
# Tue, 12 Oct 2021 01:15:10 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:4216a1c7f16a580bb4ab02a3c570e7f8ccf383c93ed4aebda567c184fedb4fc6`  
		Last Modified: Tue, 12 Oct 2021 01:26:17 GMT  
		Size: 54.1 MB (54069071 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing` - linux; ppc64le

```console
$ docker pull debian@sha256:7f17ba887c708c29105ed16fb1142fc78e5bc1c2a7d529bed58ead7c83507768
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **59.7 MB (59659961 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:0f7a4e81d010e0501a02b9389b55eb09c038b2de245805d91479bd80d5c92ea2`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:29:42 GMT
ADD file:2920b1ef5c61978464fc969befdade3714d84884adee006fb93d4d89bb412093 in / 
# Tue, 12 Oct 2021 01:29:48 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:7695f94c27f2511cd3e23671d32882f166a2fc1b8124ec1f9d3f769e88536556`  
		Last Modified: Tue, 12 Oct 2021 01:43:25 GMT  
		Size: 59.7 MB (59659961 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing` - linux; s390x

```console
$ docker pull debian@sha256:a50a383979142b1b2425e3bb5861275308f801c038984641aa41da4e01551344
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.7 MB (53700056 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:48e41d87d85e9e7a6b313ccf471ba63bff3fe1ef999525c94f979d240c227428`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:44:24 GMT
ADD file:6bdd28da982bbaaa3e5fd43949b430a741f7441a3423aea45476b602884003fb in / 
# Tue, 12 Oct 2021 00:44:27 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:16455b9c3f308b090e540412543381f2981a94842cb89496c8f0d1636c7ad1da`  
		Last Modified: Tue, 12 Oct 2021 00:50:24 GMT  
		Size: 53.7 MB (53700056 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:testing-20211011`

```console
$ docker pull debian@sha256:e3f8186278fd321c0b3460d6d423c1bc81557d09a8974036bf24de55b90d708f
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:testing-20211011` - linux; amd64

```console
$ docker pull debian@sha256:76025017aeddaa14bcfcc3f53f3432dc73a2c8977df9350d13d2cc28a591a96c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.4 MB (55445865 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4022f69526eecf543e97b259c48e368c0ac69f539f1f006ac2c9f69fecdc0530`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:23:03 GMT
ADD file:7a2d92b4684fdb24b1c954a390700dbb0a50ce8cc8774b959e562a3652fb0456 in / 
# Tue, 12 Oct 2021 01:23:03 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:91c31f9cd4fd949265f5532465cddce98935dcfa86015a5348b5f47c344d67e0`  
		Last Modified: Tue, 12 Oct 2021 01:30:11 GMT  
		Size: 55.4 MB (55445865 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-20211011` - linux; arm variant v5

```console
$ docker pull debian@sha256:9d678ad4d51a5275fccddf0a00c6733bec3bbc732ec0bdbef5699f5b45e5146b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.0 MB (52964860 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:75bee44fbb8531b937b8fc6677c583c4dfe9dbe3cd74f918877122bd3e295f35`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:56:39 GMT
ADD file:a0ded502d0b8dfe5e897243e08b3778f3adc2f2f41c32568ca6681c428d2896c in / 
# Tue, 12 Oct 2021 00:56:40 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:10d62d8715d4ae164047d47200167e5ce725ffbc04256c778e0aba73de1fd27b`  
		Last Modified: Tue, 12 Oct 2021 01:15:02 GMT  
		Size: 53.0 MB (52964860 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-20211011` - linux; arm variant v7

```console
$ docker pull debian@sha256:dda29e24c653c90ad3b5b4303590c97ac5ba98be2e19b217c58c95539418e0b2
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.6 MB (50566085 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b551a927d2971e313f53905918a67117a4e269f9b04623f2b56c9ff128febe9e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:35:12 GMT
ADD file:4927232089e749d84b21863f4f2612c8a799f3039adcc70d971cbea78ed6a7af in / 
# Tue, 12 Oct 2021 01:35:13 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:bec27cf3f9ae45321e058c161bc06cb3640cc32d6d8cc5cd5119b254da7ecfca`  
		Last Modified: Tue, 12 Oct 2021 01:52:46 GMT  
		Size: 50.6 MB (50566085 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-20211011` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:0d97fee927f66da36b469718694d9c2384887faa39b4e510046ba2662763f09d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.5 MB (54465025 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:37726bb7e5af704bc1fabf9de18e72f40569e9d7fd364082fde69118ef50c863`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:39 GMT
ADD file:0d2781f09dc7fd32dad3f41e34a91046910847a56bf128bb53a7cad6c06c1f26 in / 
# Tue, 12 Oct 2021 01:43:40 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:0a75495c66c5fc986e1fa178fa94fe24ac603e1ee8a61ff4b344624e0b8b030e`  
		Last Modified: Tue, 12 Oct 2021 01:52:57 GMT  
		Size: 54.5 MB (54465025 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-20211011` - linux; 386

```console
$ docker pull debian@sha256:47c810fa726452b53ce4e980d4ce1ecdf09c788439b41e2136f09e6157728c7d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **56.5 MB (56480852 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:85200d26145b3631fd03e8a3487999608f30534862eb4ddcb5599cf90b2bf0cd`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:48 GMT
ADD file:15469ad0ddbee66d623cee7627f9ae7a0e09baf6095d23599c5cb2ae7493abae in / 
# Tue, 12 Oct 2021 01:42:49 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:8508f9607417ad80810488921b0ada480e2ce5d8a1cb8f2303e5d0f3a1d2174e`  
		Last Modified: Tue, 12 Oct 2021 01:52:56 GMT  
		Size: 56.5 MB (56480852 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-20211011` - linux; mips64le

```console
$ docker pull debian@sha256:3a8f5e0fbfd8d20e6c718d488d40d2b67bde613080d549f04ead60ccdc2f484c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.1 MB (54069071 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:b705bc479a321116963aaaf14b8b38e2fd963bd8b430edea5d866fcd28c8efc1`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:15:09 GMT
ADD file:3d46a1b0da2f717a6fcd5a81011ddf94c751043fb103b98d488647a531fed650 in / 
# Tue, 12 Oct 2021 01:15:10 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:4216a1c7f16a580bb4ab02a3c570e7f8ccf383c93ed4aebda567c184fedb4fc6`  
		Last Modified: Tue, 12 Oct 2021 01:26:17 GMT  
		Size: 54.1 MB (54069071 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-20211011` - linux; ppc64le

```console
$ docker pull debian@sha256:7f17ba887c708c29105ed16fb1142fc78e5bc1c2a7d529bed58ead7c83507768
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **59.7 MB (59659961 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:0f7a4e81d010e0501a02b9389b55eb09c038b2de245805d91479bd80d5c92ea2`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:29:42 GMT
ADD file:2920b1ef5c61978464fc969befdade3714d84884adee006fb93d4d89bb412093 in / 
# Tue, 12 Oct 2021 01:29:48 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:7695f94c27f2511cd3e23671d32882f166a2fc1b8124ec1f9d3f769e88536556`  
		Last Modified: Tue, 12 Oct 2021 01:43:25 GMT  
		Size: 59.7 MB (59659961 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-20211011` - linux; s390x

```console
$ docker pull debian@sha256:a50a383979142b1b2425e3bb5861275308f801c038984641aa41da4e01551344
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.7 MB (53700056 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:48e41d87d85e9e7a6b313ccf471ba63bff3fe1ef999525c94f979d240c227428`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:44:24 GMT
ADD file:6bdd28da982bbaaa3e5fd43949b430a741f7441a3423aea45476b602884003fb in / 
# Tue, 12 Oct 2021 00:44:27 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:16455b9c3f308b090e540412543381f2981a94842cb89496c8f0d1636c7ad1da`  
		Last Modified: Tue, 12 Oct 2021 00:50:24 GMT  
		Size: 53.7 MB (53700056 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:testing-20211011-slim`

```console
$ docker pull debian@sha256:c2d4cc4508746a3dcdb3238330d885b97a906c390352fc73144415663faaa967
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:testing-20211011-slim` - linux; amd64

```console
$ docker pull debian@sha256:381a7544474095e0bd9737d30b4327d285133cfe3b62e72a7d9b02f42001a29e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **31.7 MB (31692026 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e254ab466936be7fd30aad2dc653e1ffca975964521d4b6bb08ec0c4591f15dd`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:23:15 GMT
ADD file:f6b80ed3e804ddf5f782ae51a8bc04fa09bf52f65efb39cadb0857b781d08cca in / 
# Tue, 12 Oct 2021 01:23:16 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:e41a2e3d0b0a7e79e8b869b888c3cd874a287dbcb0195b0f58dbf77d330eed8f`  
		Last Modified: Tue, 12 Oct 2021 01:30:31 GMT  
		Size: 31.7 MB (31692026 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-20211011-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:9b6c35071bd5949a788d14f40b144f5bdbe2d4ac3c288dc91813ace08959f46f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.2 MB (29211602 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e4e729458c110bd7bc8247cdb7896c14a7f1858b04950bbb091ca76b122df943`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:57:12 GMT
ADD file:73fe64bf1b943046619e7c49d38fae01f1227cd1c4ecf479af518541a8fc4660 in / 
# Tue, 12 Oct 2021 00:57:13 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c2ad6bea8aeedf02a9bedf4bf8669b4b006f5ba7b01ec4080c655f359c7ea3a0`  
		Last Modified: Tue, 12 Oct 2021 01:15:41 GMT  
		Size: 29.2 MB (29211602 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-20211011-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:acea487aa87b458ee9a92873202de8843a9ea81bda72f3487b7a0ae1daf5863c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **26.8 MB (26813077 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:12950c4415d27eade99c05fd2e1c2d7b4c749bc5ff482230ff6e6d5f7c1bfd91`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:35:46 GMT
ADD file:1180a916a9a7bdc2cb4681f64bedd4b12596b91d59902e7e7a14436005b2912a in / 
# Tue, 12 Oct 2021 01:35:47 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:54ae0de37f782991c9999cb61b8b8aee1d3c5ad1abb13ae2ddc42269c5bb83de`  
		Last Modified: Tue, 12 Oct 2021 01:53:20 GMT  
		Size: 26.8 MB (26813077 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-20211011-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:505bc16b681a41a36df96c64aaa8193a0ce5ecbd6e383326adb3bd4d4b8d64c4
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.7 MB (30711434 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5fe2789664c10528575d52af305a871bd4b0eb6952651d7f7b96f0f41fd0305f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:52 GMT
ADD file:8259d7f861d8a2631d9f4acd401206fede662319db3e6898bd0627b5c8a80c62 in / 
# Tue, 12 Oct 2021 01:43:53 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2a967de72364dc4d247bcea2ffa3a7e32a4cb6747a8b71527b74ac7372e526fc`  
		Last Modified: Tue, 12 Oct 2021 01:53:19 GMT  
		Size: 30.7 MB (30711434 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-20211011-slim` - linux; 386

```console
$ docker pull debian@sha256:e27aba3e78139fda2484dabb945b26cad4762fc1ef92c33803ebaf1ff880428a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **32.7 MB (32726667 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:45ab4b5740a25d600f1d34bf99ecb10abc25be93708311fb451d738f1f448e6d`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:04 GMT
ADD file:d7185a52026638c9fa18bed919a0632cd93fa9ea97fd039d4b38a9dbeefd568a in / 
# Tue, 12 Oct 2021 01:43:04 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:ecce2aec504529229dcba1809cc5b268de6b494a1f67b17a591dac6acca40782`  
		Last Modified: Tue, 12 Oct 2021 01:53:22 GMT  
		Size: 32.7 MB (32726667 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-20211011-slim` - linux; mips64le

```console
$ docker pull debian@sha256:fae74ed361dd00626cf69cf15ad23c1586873d4038ddc173fd5c32603bb60902
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.3 MB (30313260 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:de3bc24fbd39908f83d30cbcb08bc963585fb47be21144ab86f17c24f3b5ce80`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:15:37 GMT
ADD file:b4260b6560d51c7575580c9e51965b6509ff23a7deeae32c9909adc859d39714 in / 
# Tue, 12 Oct 2021 01:15:38 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:d2bdfda2b58edd29fba5352889110e56882c383fa034dd5ce9bf197a0ca6f05a`  
		Last Modified: Tue, 12 Oct 2021 01:26:55 GMT  
		Size: 30.3 MB (30313260 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-20211011-slim` - linux; ppc64le

```console
$ docker pull debian@sha256:46c2b4ba2d4315cd5f632bd4725a580f0086932a48f9c0a97f54657667b4f7e1
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **35.9 MB (35900669 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3ba35589f777934d19d5bc639cea24ccac552b26beff6ad1a73a9bda6fc1adbf`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:30:38 GMT
ADD file:e2cddfa5844a7b6a1df121b58d7df96d27401bb7eaa57311dbf8f2d1fbefbdbc in / 
# Tue, 12 Oct 2021 01:30:41 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2463691f6014433729769cd612d2e4f913196d353b3e2b03669140872fa429a1`  
		Last Modified: Tue, 12 Oct 2021 01:43:51 GMT  
		Size: 35.9 MB (35900669 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-20211011-slim` - linux; s390x

```console
$ docker pull debian@sha256:2ff0a952d712916606876bef59e82c7a69481bf269c40e8b66974469cce0a9ca
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.9 MB (29947092 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:47200e6b0dfa4498f50d81360bcb8a590ed8a22b35dde1909161f98942e8093f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:44:41 GMT
ADD file:8a1f9a5b713b56f35ccf4b9cbec121b39d4657f61daff35687bcc7d5e057a772 in / 
# Tue, 12 Oct 2021 00:44:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:82c50934191a866dd0513b11f069a868b988df942169c6f1b2aa5fe440f96a9c`  
		Last Modified: Tue, 12 Oct 2021 00:50:41 GMT  
		Size: 29.9 MB (29947092 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:testing-backports`

```console
$ docker pull debian@sha256:e7cf5ce97e192f88cc88eb8e9b8012bb65c313f71ee8b131a3a703358645bf06
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:testing-backports` - linux; amd64

```console
$ docker pull debian@sha256:f071d8000941746b06552b7d86f62acb4aa0aa7be41f32b9cb6b23036e6c0c94
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.4 MB (55446091 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4bbf8ec476edf67f62e550d516cd4f7f1afd7d755f2c5c623c839f3522fc044e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:23:03 GMT
ADD file:7a2d92b4684fdb24b1c954a390700dbb0a50ce8cc8774b959e562a3652fb0456 in / 
# Tue, 12 Oct 2021 01:23:03 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:23:07 GMT
RUN echo 'deb http://deb.debian.org/debian testing-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:91c31f9cd4fd949265f5532465cddce98935dcfa86015a5348b5f47c344d67e0`  
		Last Modified: Tue, 12 Oct 2021 01:30:11 GMT  
		Size: 55.4 MB (55445865 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:e5824770af4ce48055efabe4fac2aab7542a6b709f6637eca8a1377ecc45d67e`  
		Last Modified: Tue, 12 Oct 2021 01:30:20 GMT  
		Size: 226.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-backports` - linux; arm variant v5

```console
$ docker pull debian@sha256:8620b8f705461640d885d7fd0664ec9e4c3b0869b72dba18e004d11d1002f72e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.0 MB (52965085 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:9bd5a09a176bdbab798d5c04d6877346c06222438d38fac0100bfe4b3a21af18`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:56:39 GMT
ADD file:a0ded502d0b8dfe5e897243e08b3778f3adc2f2f41c32568ca6681c428d2896c in / 
# Tue, 12 Oct 2021 00:56:40 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 00:56:53 GMT
RUN echo 'deb http://deb.debian.org/debian testing-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:10d62d8715d4ae164047d47200167e5ce725ffbc04256c778e0aba73de1fd27b`  
		Last Modified: Tue, 12 Oct 2021 01:15:02 GMT  
		Size: 53.0 MB (52964860 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:10f5e5caed528d1cd2839cb71052b6816e3eaf32635f29118816cf7ff52a6c34`  
		Last Modified: Tue, 12 Oct 2021 01:15:15 GMT  
		Size: 225.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-backports` - linux; arm variant v7

```console
$ docker pull debian@sha256:61d857d2b57d0100a9ab6b4dfd7bb920edca8704b79605786028ce80793b4f1a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.6 MB (50566311 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:c460cd1326c3c233bbf5ef02619564c276e72c79d854fd9221bff46fcfe1986e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:35:12 GMT
ADD file:4927232089e749d84b21863f4f2612c8a799f3039adcc70d971cbea78ed6a7af in / 
# Tue, 12 Oct 2021 01:35:13 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:35:26 GMT
RUN echo 'deb http://deb.debian.org/debian testing-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:bec27cf3f9ae45321e058c161bc06cb3640cc32d6d8cc5cd5119b254da7ecfca`  
		Last Modified: Tue, 12 Oct 2021 01:52:46 GMT  
		Size: 50.6 MB (50566085 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:b399cd77b8cd1e9b055352793d13269844b4917f8e1a8558bcf4c1dae69e3954`  
		Last Modified: Tue, 12 Oct 2021 01:52:57 GMT  
		Size: 226.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-backports` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:b2e544bbdea33141e4ef582c7b3538ed7c6303d2968f405a7eb386c438502eb4
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.5 MB (54465248 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:59ce844ced3881c1131de9fa4d83b8630208e2096e3cdba37222b594e2373685`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:39 GMT
ADD file:0d2781f09dc7fd32dad3f41e34a91046910847a56bf128bb53a7cad6c06c1f26 in / 
# Tue, 12 Oct 2021 01:43:40 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:43:45 GMT
RUN echo 'deb http://deb.debian.org/debian testing-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:0a75495c66c5fc986e1fa178fa94fe24ac603e1ee8a61ff4b344624e0b8b030e`  
		Last Modified: Tue, 12 Oct 2021 01:52:57 GMT  
		Size: 54.5 MB (54465025 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:18d61c7e6ffa68075e0b3f1eea821950b0b0f59ec58f9b1b2107daf1e12bfbbf`  
		Last Modified: Tue, 12 Oct 2021 01:53:08 GMT  
		Size: 223.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-backports` - linux; 386

```console
$ docker pull debian@sha256:d4d40853641218b9a8fdaf7aa897964b8aa8ba8bccb31aea88f461fe979394ce
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **56.5 MB (56481075 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:84a37d6c737feaebf9d2f5aba20c0466dc333f934d3b9d8b166ee3a842162c67`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:42:48 GMT
ADD file:15469ad0ddbee66d623cee7627f9ae7a0e09baf6095d23599c5cb2ae7493abae in / 
# Tue, 12 Oct 2021 01:42:49 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:42:55 GMT
RUN echo 'deb http://deb.debian.org/debian testing-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:8508f9607417ad80810488921b0ada480e2ce5d8a1cb8f2303e5d0f3a1d2174e`  
		Last Modified: Tue, 12 Oct 2021 01:52:56 GMT  
		Size: 56.5 MB (56480852 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:ec11d340291661cf3b9cc18cbf8a40719d23c3b8bef6b61460dc6a6b1c9cc97f`  
		Last Modified: Tue, 12 Oct 2021 01:53:08 GMT  
		Size: 223.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-backports` - linux; mips64le

```console
$ docker pull debian@sha256:357c32bcbf0ded68b7985e474081831f2fbe727f3b736cb004deace857db2152
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.1 MB (54069294 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f6813d2a9b792085a58c0b879b70bc565953adf3999934886a6de00cc22786ea`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:15:09 GMT
ADD file:3d46a1b0da2f717a6fcd5a81011ddf94c751043fb103b98d488647a531fed650 in / 
# Tue, 12 Oct 2021 01:15:10 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:15:18 GMT
RUN echo 'deb http://deb.debian.org/debian testing-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:4216a1c7f16a580bb4ab02a3c570e7f8ccf383c93ed4aebda567c184fedb4fc6`  
		Last Modified: Tue, 12 Oct 2021 01:26:17 GMT  
		Size: 54.1 MB (54069071 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:6959c86437187d476505fe0055d941cf0989208c989007161e291681b27a6a4c`  
		Last Modified: Tue, 12 Oct 2021 01:26:28 GMT  
		Size: 223.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-backports` - linux; ppc64le

```console
$ docker pull debian@sha256:e32c18612f213d84f9e18eac403fb57e5fe471b548de603dfb694a905bf47bd0
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **59.7 MB (59660185 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8f312451e2735904f799ad06290dc6af80d9cd9cd83118ba35e8fe8d779e13d2`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:29:42 GMT
ADD file:2920b1ef5c61978464fc969befdade3714d84884adee006fb93d4d89bb412093 in / 
# Tue, 12 Oct 2021 01:29:48 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 01:30:20 GMT
RUN echo 'deb http://deb.debian.org/debian testing-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:7695f94c27f2511cd3e23671d32882f166a2fc1b8124ec1f9d3f769e88536556`  
		Last Modified: Tue, 12 Oct 2021 01:43:25 GMT  
		Size: 59.7 MB (59659961 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:51001182f6335022e4749520f5736ebee1eef396f82d1d285774a66b31938bd1`  
		Last Modified: Tue, 12 Oct 2021 01:43:35 GMT  
		Size: 224.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-backports` - linux; s390x

```console
$ docker pull debian@sha256:bdc41768477b3bb9beb65aeac3ca8ffab0b164d3ac05943a47792c0fa6acb6d3
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.7 MB (53700280 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a5083110b8c0267d1775243cc8b7798d314f86be408b00cf1aa4b0c835c3415b`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:44:24 GMT
ADD file:6bdd28da982bbaaa3e5fd43949b430a741f7441a3423aea45476b602884003fb in / 
# Tue, 12 Oct 2021 00:44:27 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 00:44:33 GMT
RUN echo 'deb http://deb.debian.org/debian testing-backports main' > /etc/apt/sources.list.d/backports.list
```

-	Layers:
	-	`sha256:16455b9c3f308b090e540412543381f2981a94842cb89496c8f0d1636c7ad1da`  
		Last Modified: Tue, 12 Oct 2021 00:50:24 GMT  
		Size: 53.7 MB (53700056 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:ef97085952c5b25aa4e803de74f56e2b96bd6940d37b732a1e7691cc829c28e6`  
		Last Modified: Tue, 12 Oct 2021 00:50:32 GMT  
		Size: 224.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:testing-slim`

```console
$ docker pull debian@sha256:c2d4cc4508746a3dcdb3238330d885b97a906c390352fc73144415663faaa967
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 8
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; s390x

### `debian:testing-slim` - linux; amd64

```console
$ docker pull debian@sha256:381a7544474095e0bd9737d30b4327d285133cfe3b62e72a7d9b02f42001a29e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **31.7 MB (31692026 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e254ab466936be7fd30aad2dc653e1ffca975964521d4b6bb08ec0c4591f15dd`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:23:15 GMT
ADD file:f6b80ed3e804ddf5f782ae51a8bc04fa09bf52f65efb39cadb0857b781d08cca in / 
# Tue, 12 Oct 2021 01:23:16 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:e41a2e3d0b0a7e79e8b869b888c3cd874a287dbcb0195b0f58dbf77d330eed8f`  
		Last Modified: Tue, 12 Oct 2021 01:30:31 GMT  
		Size: 31.7 MB (31692026 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:9b6c35071bd5949a788d14f40b144f5bdbe2d4ac3c288dc91813ace08959f46f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.2 MB (29211602 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e4e729458c110bd7bc8247cdb7896c14a7f1858b04950bbb091ca76b122df943`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:57:12 GMT
ADD file:73fe64bf1b943046619e7c49d38fae01f1227cd1c4ecf479af518541a8fc4660 in / 
# Tue, 12 Oct 2021 00:57:13 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c2ad6bea8aeedf02a9bedf4bf8669b4b006f5ba7b01ec4080c655f359c7ea3a0`  
		Last Modified: Tue, 12 Oct 2021 01:15:41 GMT  
		Size: 29.2 MB (29211602 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:acea487aa87b458ee9a92873202de8843a9ea81bda72f3487b7a0ae1daf5863c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **26.8 MB (26813077 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:12950c4415d27eade99c05fd2e1c2d7b4c749bc5ff482230ff6e6d5f7c1bfd91`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:35:46 GMT
ADD file:1180a916a9a7bdc2cb4681f64bedd4b12596b91d59902e7e7a14436005b2912a in / 
# Tue, 12 Oct 2021 01:35:47 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:54ae0de37f782991c9999cb61b8b8aee1d3c5ad1abb13ae2ddc42269c5bb83de`  
		Last Modified: Tue, 12 Oct 2021 01:53:20 GMT  
		Size: 26.8 MB (26813077 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:505bc16b681a41a36df96c64aaa8193a0ce5ecbd6e383326adb3bd4d4b8d64c4
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.7 MB (30711434 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5fe2789664c10528575d52af305a871bd4b0eb6952651d7f7b96f0f41fd0305f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:52 GMT
ADD file:8259d7f861d8a2631d9f4acd401206fede662319db3e6898bd0627b5c8a80c62 in / 
# Tue, 12 Oct 2021 01:43:53 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2a967de72364dc4d247bcea2ffa3a7e32a4cb6747a8b71527b74ac7372e526fc`  
		Last Modified: Tue, 12 Oct 2021 01:53:19 GMT  
		Size: 30.7 MB (30711434 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-slim` - linux; 386

```console
$ docker pull debian@sha256:e27aba3e78139fda2484dabb945b26cad4762fc1ef92c33803ebaf1ff880428a
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **32.7 MB (32726667 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:45ab4b5740a25d600f1d34bf99ecb10abc25be93708311fb451d738f1f448e6d`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:04 GMT
ADD file:d7185a52026638c9fa18bed919a0632cd93fa9ea97fd039d4b38a9dbeefd568a in / 
# Tue, 12 Oct 2021 01:43:04 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:ecce2aec504529229dcba1809cc5b268de6b494a1f67b17a591dac6acca40782`  
		Last Modified: Tue, 12 Oct 2021 01:53:22 GMT  
		Size: 32.7 MB (32726667 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-slim` - linux; mips64le

```console
$ docker pull debian@sha256:fae74ed361dd00626cf69cf15ad23c1586873d4038ddc173fd5c32603bb60902
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.3 MB (30313260 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:de3bc24fbd39908f83d30cbcb08bc963585fb47be21144ab86f17c24f3b5ce80`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:15:37 GMT
ADD file:b4260b6560d51c7575580c9e51965b6509ff23a7deeae32c9909adc859d39714 in / 
# Tue, 12 Oct 2021 01:15:38 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:d2bdfda2b58edd29fba5352889110e56882c383fa034dd5ce9bf197a0ca6f05a`  
		Last Modified: Tue, 12 Oct 2021 01:26:55 GMT  
		Size: 30.3 MB (30313260 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-slim` - linux; ppc64le

```console
$ docker pull debian@sha256:46c2b4ba2d4315cd5f632bd4725a580f0086932a48f9c0a97f54657667b4f7e1
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **35.9 MB (35900669 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3ba35589f777934d19d5bc639cea24ccac552b26beff6ad1a73a9bda6fc1adbf`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:30:38 GMT
ADD file:e2cddfa5844a7b6a1df121b58d7df96d27401bb7eaa57311dbf8f2d1fbefbdbc in / 
# Tue, 12 Oct 2021 01:30:41 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2463691f6014433729769cd612d2e4f913196d353b3e2b03669140872fa429a1`  
		Last Modified: Tue, 12 Oct 2021 01:43:51 GMT  
		Size: 35.9 MB (35900669 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:testing-slim` - linux; s390x

```console
$ docker pull debian@sha256:2ff0a952d712916606876bef59e82c7a69481bf269c40e8b66974469cce0a9ca
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.9 MB (29947092 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:47200e6b0dfa4498f50d81360bcb8a590ed8a22b35dde1909161f98942e8093f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:44:41 GMT
ADD file:8a1f9a5b713b56f35ccf4b9cbec121b39d4657f61daff35687bcc7d5e057a772 in / 
# Tue, 12 Oct 2021 00:44:42 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:82c50934191a866dd0513b11f069a868b988df942169c6f1b2aa5fe440f96a9c`  
		Last Modified: Tue, 12 Oct 2021 00:50:41 GMT  
		Size: 29.9 MB (29947092 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:unstable`

```console
$ docker pull debian@sha256:911e49bbc76754c139b48cd2111d5ddec1338ae495cd624b7eb871fee24ee4fa
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 9
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; riscv64
	-	linux; s390x

### `debian:unstable` - linux; amd64

```console
$ docker pull debian@sha256:8433e61815f23a9fd11923709b74eac55e0b31952a3f372248f4b6d9da536716
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.7 MB (55687490 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:95a5ee88d43cc7bd4dbf637921c5e0527193b7d9bde8732986f04a1c3e68de85`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:23:26 GMT
ADD file:768176ccdb1053418db077c8d4e7f86c7d238d14b4d6298282277ed9167f5103 in / 
# Tue, 12 Oct 2021 01:23:27 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:68135aa259351f7843ab8a4668eac588b9c494bae3a339713817d0f4bd9e6572`  
		Last Modified: Tue, 12 Oct 2021 01:30:47 GMT  
		Size: 55.7 MB (55687490 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable` - linux; arm variant v5

```console
$ docker pull debian@sha256:7a63ba04b0026bfffb2842f8735914848c27ebded3c4ae18f2eaa0471c5d7b4b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53186307 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:63cf150c14c6cab9c7354db59d4f469bf883250c6336d79d218f36ee7c288810`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:57:38 GMT
ADD file:142ff5a3e7c083dbe37f25d418562ca69fb261ae8301ac6704d16389cac353ff in / 
# Tue, 12 Oct 2021 00:57:40 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6d15c84744a3fd9ea8041501d65ab1bd82ba58fb2d2a5efae10506df977440fb`  
		Last Modified: Tue, 12 Oct 2021 01:16:31 GMT  
		Size: 53.2 MB (53186307 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable` - linux; arm variant v7

```console
$ docker pull debian@sha256:58ba27af65ec4ebe418167c7e86e92f02c4cda9f00d1815c22f0638159aeb1ec
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.8 MB (50797060 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8a48f9648a6cb865b41d35ad12cf9482f2b3e9b32b99ccb1779a22bb8e69268e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:36:14 GMT
ADD file:be4b9a2b55c955b91060dc950fbb28c53858174da0e76833e50d3a818c01eec0 in / 
# Tue, 12 Oct 2021 01:36:15 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c5c2a3ca9a7b031872447f9db1cf1ff22cd0711b3d2db5d88595c25ed411d976`  
		Last Modified: Tue, 12 Oct 2021 01:53:58 GMT  
		Size: 50.8 MB (50797060 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:27d1fbaa3c41efb372bf56aeee62773e769a2c4053762d61637e14f8374b3b7d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.7 MB (54702889 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8eea3e5564dee6f4daa835f9600b64724d2fb6ff12ecb0df387ecbd9fb3d8274`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:44:02 GMT
ADD file:894de0be09c3231a5faac7ccddbc80c3b60408789649645b6ee1f495fa9b9658 in / 
# Tue, 12 Oct 2021 01:44:02 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:f7d44b65f406086d3f6c33e702e62ccb453a15e440f7a20e605c14d292b461e3`  
		Last Modified: Tue, 12 Oct 2021 01:53:38 GMT  
		Size: 54.7 MB (54702889 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable` - linux; 386

```console
$ docker pull debian@sha256:bc343c4a530fca5bc02a01afe34a457887c2181c07300a916b0dc59cf07d20bc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **56.7 MB (56716144 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ff2f64fca99c88e738b282653da3c64106af7073e3afdd8a1bf106acdda3d727`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:17 GMT
ADD file:3509f6fbe74f127b3a47c4cb6323c6e8e8fb46e38fe742921d35fcd0ce76d276 in / 
# Tue, 12 Oct 2021 01:43:18 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:1665cd0d0f387adf8fec7c5686c084e64edc02778c6f6244db411ddc55a1f4c1`  
		Last Modified: Tue, 12 Oct 2021 01:53:43 GMT  
		Size: 56.7 MB (56716144 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable` - linux; mips64le

```console
$ docker pull debian@sha256:c33597ce0a00d9ece2cf7ac9de46fa223b4984d09c49078286d76a8023f3e346
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.3 MB (54313458 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:988c9c93af6d5bf80a0c09f9febc14b93cb1bc9a3d6702d27bef179229f3e621`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:16:04 GMT
ADD file:2cc438bb6fb8a3a9b0818598818b7854a43d5393ac7e53bdf871b61d3357f09e in / 
# Tue, 12 Oct 2021 01:16:05 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:4f27b1c33880effa793ac87419f57ce87673a42cd455a5eccb5b14568e55ce22`  
		Last Modified: Tue, 12 Oct 2021 01:27:38 GMT  
		Size: 54.3 MB (54313458 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable` - linux; ppc64le

```console
$ docker pull debian@sha256:9223d6eca312fe2cf854702184cd57de1528bd7f09ffc49f6b039da5f1d3ab43
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **59.9 MB (59889202 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ba9c394f27ca08b92becd800aebba35ae8befe30da03cf7fcf6e71073dd5ee85`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:30:57 GMT
ADD file:52b1e51a9aeb65e63eb0c1a42cef5a4bc15ead2428cb8ece49b10c4fe8464216 in / 
# Tue, 12 Oct 2021 01:31:02 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:5339ee5d539a0754933340b57bd16312bc7f834914e59717e20c1b83eaa8072a`  
		Last Modified: Tue, 12 Oct 2021 01:44:12 GMT  
		Size: 59.9 MB (59889202 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable` - linux; riscv64

```console
$ docker pull debian@sha256:22fd02a51c09a8cd28b60aa0002da6684ebe636f3a6af9254b504b5f4ee60bfb
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **51.5 MB (51516616 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:7001c623dd7e7170ed515c5eb57e3050e9f8ce74245306e4a5ee85b1254ff3f0`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:17:56 GMT
ADD file:fa84e1dde40e56bc549344dc0d3f2bf2400e5009c42f96c262489c33aaa81455 in / 
# Tue, 12 Oct 2021 01:17:59 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:e70c24b27e8c6ebee7852a52b1fdd44e247e6ae2e2ce6e18bed96cc9408cff2d`  
		Last Modified: Tue, 12 Oct 2021 01:33:55 GMT  
		Size: 51.5 MB (51516616 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable` - linux; s390x

```console
$ docker pull debian@sha256:b462d85e8d3b3fa5a0a5cf7bd7c19c203f96b30ed03d07ce77902b248a59c8dd
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.9 MB (53940695 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:81653f9e5bcc1363f51436ac7f2f766cce507ad1ae71f2d09b6d162ebeb57e7c`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:44:52 GMT
ADD file:8e72740308f6ae0d026697be186652e326c16f3f37e6ab6cc86de966dfa8cab4 in / 
# Tue, 12 Oct 2021 00:44:55 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:7d3fc62e9c56880d86636919f36507d68aacc14f85a6c05acb3ecfcfedcafb8a`  
		Last Modified: Tue, 12 Oct 2021 00:50:54 GMT  
		Size: 53.9 MB (53940695 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:unstable-20211011`

```console
$ docker pull debian@sha256:911e49bbc76754c139b48cd2111d5ddec1338ae495cd624b7eb871fee24ee4fa
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 9
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; riscv64
	-	linux; s390x

### `debian:unstable-20211011` - linux; amd64

```console
$ docker pull debian@sha256:8433e61815f23a9fd11923709b74eac55e0b31952a3f372248f4b6d9da536716
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **55.7 MB (55687490 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:95a5ee88d43cc7bd4dbf637921c5e0527193b7d9bde8732986f04a1c3e68de85`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:23:26 GMT
ADD file:768176ccdb1053418db077c8d4e7f86c7d238d14b4d6298282277ed9167f5103 in / 
# Tue, 12 Oct 2021 01:23:27 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:68135aa259351f7843ab8a4668eac588b9c494bae3a339713817d0f4bd9e6572`  
		Last Modified: Tue, 12 Oct 2021 01:30:47 GMT  
		Size: 55.7 MB (55687490 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-20211011` - linux; arm variant v5

```console
$ docker pull debian@sha256:7a63ba04b0026bfffb2842f8735914848c27ebded3c4ae18f2eaa0471c5d7b4b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.2 MB (53186307 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:63cf150c14c6cab9c7354db59d4f469bf883250c6336d79d218f36ee7c288810`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:57:38 GMT
ADD file:142ff5a3e7c083dbe37f25d418562ca69fb261ae8301ac6704d16389cac353ff in / 
# Tue, 12 Oct 2021 00:57:40 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:6d15c84744a3fd9ea8041501d65ab1bd82ba58fb2d2a5efae10506df977440fb`  
		Last Modified: Tue, 12 Oct 2021 01:16:31 GMT  
		Size: 53.2 MB (53186307 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-20211011` - linux; arm variant v7

```console
$ docker pull debian@sha256:58ba27af65ec4ebe418167c7e86e92f02c4cda9f00d1815c22f0638159aeb1ec
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **50.8 MB (50797060 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8a48f9648a6cb865b41d35ad12cf9482f2b3e9b32b99ccb1779a22bb8e69268e`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:36:14 GMT
ADD file:be4b9a2b55c955b91060dc950fbb28c53858174da0e76833e50d3a818c01eec0 in / 
# Tue, 12 Oct 2021 01:36:15 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:c5c2a3ca9a7b031872447f9db1cf1ff22cd0711b3d2db5d88595c25ed411d976`  
		Last Modified: Tue, 12 Oct 2021 01:53:58 GMT  
		Size: 50.8 MB (50797060 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-20211011` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:27d1fbaa3c41efb372bf56aeee62773e769a2c4053762d61637e14f8374b3b7d
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.7 MB (54702889 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8eea3e5564dee6f4daa835f9600b64724d2fb6ff12ecb0df387ecbd9fb3d8274`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:44:02 GMT
ADD file:894de0be09c3231a5faac7ccddbc80c3b60408789649645b6ee1f495fa9b9658 in / 
# Tue, 12 Oct 2021 01:44:02 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:f7d44b65f406086d3f6c33e702e62ccb453a15e440f7a20e605c14d292b461e3`  
		Last Modified: Tue, 12 Oct 2021 01:53:38 GMT  
		Size: 54.7 MB (54702889 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-20211011` - linux; 386

```console
$ docker pull debian@sha256:bc343c4a530fca5bc02a01afe34a457887c2181c07300a916b0dc59cf07d20bc
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **56.7 MB (56716144 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ff2f64fca99c88e738b282653da3c64106af7073e3afdd8a1bf106acdda3d727`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:17 GMT
ADD file:3509f6fbe74f127b3a47c4cb6323c6e8e8fb46e38fe742921d35fcd0ce76d276 in / 
# Tue, 12 Oct 2021 01:43:18 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:1665cd0d0f387adf8fec7c5686c084e64edc02778c6f6244db411ddc55a1f4c1`  
		Last Modified: Tue, 12 Oct 2021 01:53:43 GMT  
		Size: 56.7 MB (56716144 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-20211011` - linux; mips64le

```console
$ docker pull debian@sha256:c33597ce0a00d9ece2cf7ac9de46fa223b4984d09c49078286d76a8023f3e346
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **54.3 MB (54313458 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:988c9c93af6d5bf80a0c09f9febc14b93cb1bc9a3d6702d27bef179229f3e621`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:16:04 GMT
ADD file:2cc438bb6fb8a3a9b0818598818b7854a43d5393ac7e53bdf871b61d3357f09e in / 
# Tue, 12 Oct 2021 01:16:05 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:4f27b1c33880effa793ac87419f57ce87673a42cd455a5eccb5b14568e55ce22`  
		Last Modified: Tue, 12 Oct 2021 01:27:38 GMT  
		Size: 54.3 MB (54313458 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-20211011` - linux; ppc64le

```console
$ docker pull debian@sha256:9223d6eca312fe2cf854702184cd57de1528bd7f09ffc49f6b039da5f1d3ab43
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **59.9 MB (59889202 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ba9c394f27ca08b92becd800aebba35ae8befe30da03cf7fcf6e71073dd5ee85`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:30:57 GMT
ADD file:52b1e51a9aeb65e63eb0c1a42cef5a4bc15ead2428cb8ece49b10c4fe8464216 in / 
# Tue, 12 Oct 2021 01:31:02 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:5339ee5d539a0754933340b57bd16312bc7f834914e59717e20c1b83eaa8072a`  
		Last Modified: Tue, 12 Oct 2021 01:44:12 GMT  
		Size: 59.9 MB (59889202 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-20211011` - linux; riscv64

```console
$ docker pull debian@sha256:22fd02a51c09a8cd28b60aa0002da6684ebe636f3a6af9254b504b5f4ee60bfb
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **51.5 MB (51516616 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:7001c623dd7e7170ed515c5eb57e3050e9f8ce74245306e4a5ee85b1254ff3f0`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:17:56 GMT
ADD file:fa84e1dde40e56bc549344dc0d3f2bf2400e5009c42f96c262489c33aaa81455 in / 
# Tue, 12 Oct 2021 01:17:59 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:e70c24b27e8c6ebee7852a52b1fdd44e247e6ae2e2ce6e18bed96cc9408cff2d`  
		Last Modified: Tue, 12 Oct 2021 01:33:55 GMT  
		Size: 51.5 MB (51516616 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-20211011` - linux; s390x

```console
$ docker pull debian@sha256:b462d85e8d3b3fa5a0a5cf7bd7c19c203f96b30ed03d07ce77902b248a59c8dd
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **53.9 MB (53940695 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:81653f9e5bcc1363f51436ac7f2f766cce507ad1ae71f2d09b6d162ebeb57e7c`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:44:52 GMT
ADD file:8e72740308f6ae0d026697be186652e326c16f3f37e6ab6cc86de966dfa8cab4 in / 
# Tue, 12 Oct 2021 00:44:55 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:7d3fc62e9c56880d86636919f36507d68aacc14f85a6c05acb3ecfcfedcafb8a`  
		Last Modified: Tue, 12 Oct 2021 00:50:54 GMT  
		Size: 53.9 MB (53940695 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:unstable-20211011-slim`

```console
$ docker pull debian@sha256:da81f85e40b587612c1b9366acdd9236c86dbcd93d1294295309c3cf8e386611
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 9
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; riscv64
	-	linux; s390x

### `debian:unstable-20211011-slim` - linux; amd64

```console
$ docker pull debian@sha256:8e09fb9dd5cc5aab6acfecce9a2eef3afb8d5514ecd1d4d2c9f18d5c4f8ca88b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **31.7 MB (31731974 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5218a74f4da567f6569712bf6b318713a19792e439d3503f9c1bef0490575e10`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:23:36 GMT
ADD file:017f678793e737c4eb26779b05dc1ff7542751e024938abaee2d3077c1822fa5 in / 
# Tue, 12 Oct 2021 01:23:36 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:f1c9427fc1e1e92049b6f5a6e7384a4507ae9ec93d75e72cd2530cccbd680310`  
		Last Modified: Tue, 12 Oct 2021 01:31:02 GMT  
		Size: 31.7 MB (31731974 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-20211011-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:2877f16f3620d7d2678e374a952de4e87ed2e4db3f37fd32fcb61b58b511b9bd
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.2 MB (29236155 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:07ce4fb8c7619d8fa5045731f280cb88a7f870681fc786414ffaaf7a42d059fd`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:57:59 GMT
ADD file:0a16c1d34be07dd282f59442792c1a3c13be01192aadb601d17937b5cfc7014a in / 
# Tue, 12 Oct 2021 00:58:01 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2667e161c1f445181b931350f0e137a01480e8b7aee7cd443260c58639223801`  
		Last Modified: Tue, 12 Oct 2021 01:17:03 GMT  
		Size: 29.2 MB (29236155 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-20211011-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:a16615d23bb3ae70642414f20f5a1f37166e6b8f4e89a9304ed4b0bbcbe037a0
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **26.8 MB (26844545 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:23e8689c159aa7100dcd0eaa27e838b42342bb048bbdb30a17abd725913dd2b0`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:36:36 GMT
ADD file:1aa4f2f1a9b196606788d4ef8971354311546ec87785e728eda6dd28dc8f0703 in / 
# Tue, 12 Oct 2021 01:36:37 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:af234ad4bc1a66f08150bb3e47a8655217a5881489b3d16455925299896aeb48`  
		Last Modified: Tue, 12 Oct 2021 01:54:25 GMT  
		Size: 26.8 MB (26844545 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-20211011-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:af707b5c78af806aaf2d1d6ec331c1b762b58cb93f95a3d8eaabe5d247dca739
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.7 MB (30748056 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f4a5cb9dbdc7c7eb2093d86aa39f75c8dd017a5d7400842f01613d6a907a2fed`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:44:10 GMT
ADD file:75c40743ce1d29b5bfad592cdd0e7bfc50a4294cd2fc2ec8a843f5a547afbfb9 in / 
# Tue, 12 Oct 2021 01:44:11 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:41402d960b89f43a20d3b0c4116e3623db2ae2dd0cf72e13625714e0dec92fe5`  
		Last Modified: Tue, 12 Oct 2021 01:53:53 GMT  
		Size: 30.7 MB (30748056 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-20211011-slim` - linux; 386

```console
$ docker pull debian@sha256:02867a14ce4a996d6f184ae4abef54c066546c567f664bf7728f1f196d6c10ba
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **32.8 MB (32763493 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:482f53a8414dd8375bd522122b3c96c56094ec2fb3f0c6f7baac24925f503c9c`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:28 GMT
ADD file:1f6c67031a134cc1488a3dd5b1afc48aa91f1fec0ef43163ec45e701d7fb5749 in / 
# Tue, 12 Oct 2021 01:43:28 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:8cc6f6761bfcf3ebcef675e4caf6fbec3b60204771c0bf05086986885db488be`  
		Last Modified: Tue, 12 Oct 2021 01:54:00 GMT  
		Size: 32.8 MB (32763493 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-20211011-slim` - linux; mips64le

```console
$ docker pull debian@sha256:38fdd1f36c850b19a74240cad66459dd0bf602a55165a1259ef90aaa33a5c9cf
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.4 MB (30360328 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:99b7fec1197a157131a21239056ab2a719be0e0ada47605224848da0043a588f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:16:25 GMT
ADD file:6993b092f03b469e7400686851bcfcebca618af437e056b1b666cef37b5b6153 in / 
# Tue, 12 Oct 2021 01:16:26 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:64a2ff80f178e33cf773999a407067f90dd9d3949b2dc79b39737ac25cf811c4`  
		Last Modified: Tue, 12 Oct 2021 01:28:08 GMT  
		Size: 30.4 MB (30360328 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-20211011-slim` - linux; ppc64le

```console
$ docker pull debian@sha256:e8381228ea8e6a2a603580be529c3cc8110b24d4d824d2adea3e77ba1a3ad802
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **35.9 MB (35935015 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d40481c899bc8e69850cc571fa027ae297f0d3fcdf5d02515f4ba43bbd5761af`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:31:16 GMT
ADD file:1e32ff7653c6a38b7fc55329c42957dd45ef1084a910cbd6219c735493d864b6 in / 
# Tue, 12 Oct 2021 01:31:22 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:49b65047f6921d4626cbf4019e684e789b9fee599da822a3305cb2cdccabb92c`  
		Last Modified: Tue, 12 Oct 2021 01:44:31 GMT  
		Size: 35.9 MB (35935015 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-20211011-slim` - linux; riscv64

```console
$ docker pull debian@sha256:105f953c74ec0c00abd27a3c2aa0bc61f2d37e16c3d99a707db829c9b74a03fe
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **27.6 MB (27563313 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8c08e4d8abe9c035035af444293cd7f20ffa362541bfa2b71b9def71e975174b`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:19:32 GMT
ADD file:d023bb4e594d57c08428052992601ba913854065f3d8481129223f986c2eabb8 in / 
# Tue, 12 Oct 2021 01:19:34 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:5dad889f11fd648e2e18c46f30ea00d750cf9c392dbabfcc4f72c2e09e3d0af0`  
		Last Modified: Tue, 12 Oct 2021 01:35:28 GMT  
		Size: 27.6 MB (27563313 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-20211011-slim` - linux; s390x

```console
$ docker pull debian@sha256:a989409c6363abb33a1a3d379073d3fddcf10d5e00cdd9b2f06c36f228f2aaa8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.0 MB (29984209 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:7787b1607637a684546f1ccc6a546b31c16edae7b06dda2232c4756f4bdba847`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:45:04 GMT
ADD file:9cf952a889ba9dc2c4e86a75aca595d35e0df14dec61963f64769d55df851e40 in / 
# Tue, 12 Oct 2021 00:45:05 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:0c3ff48a9ed28edd72e1e14389926020dc484e092e7db31244fc783a52859923`  
		Last Modified: Tue, 12 Oct 2021 00:51:07 GMT  
		Size: 30.0 MB (29984209 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `debian:unstable-slim`

```console
$ docker pull debian@sha256:da81f85e40b587612c1b9366acdd9236c86dbcd93d1294295309c3cf8e386611
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 9
	-	linux; amd64
	-	linux; arm variant v5
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; 386
	-	linux; mips64le
	-	linux; ppc64le
	-	linux; riscv64
	-	linux; s390x

### `debian:unstable-slim` - linux; amd64

```console
$ docker pull debian@sha256:8e09fb9dd5cc5aab6acfecce9a2eef3afb8d5514ecd1d4d2c9f18d5c4f8ca88b
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **31.7 MB (31731974 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5218a74f4da567f6569712bf6b318713a19792e439d3503f9c1bef0490575e10`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:23:36 GMT
ADD file:017f678793e737c4eb26779b05dc1ff7542751e024938abaee2d3077c1822fa5 in / 
# Tue, 12 Oct 2021 01:23:36 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:f1c9427fc1e1e92049b6f5a6e7384a4507ae9ec93d75e72cd2530cccbd680310`  
		Last Modified: Tue, 12 Oct 2021 01:31:02 GMT  
		Size: 31.7 MB (31731974 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-slim` - linux; arm variant v5

```console
$ docker pull debian@sha256:2877f16f3620d7d2678e374a952de4e87ed2e4db3f37fd32fcb61b58b511b9bd
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **29.2 MB (29236155 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:07ce4fb8c7619d8fa5045731f280cb88a7f870681fc786414ffaaf7a42d059fd`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:57:59 GMT
ADD file:0a16c1d34be07dd282f59442792c1a3c13be01192aadb601d17937b5cfc7014a in / 
# Tue, 12 Oct 2021 00:58:01 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:2667e161c1f445181b931350f0e137a01480e8b7aee7cd443260c58639223801`  
		Last Modified: Tue, 12 Oct 2021 01:17:03 GMT  
		Size: 29.2 MB (29236155 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-slim` - linux; arm variant v7

```console
$ docker pull debian@sha256:a16615d23bb3ae70642414f20f5a1f37166e6b8f4e89a9304ed4b0bbcbe037a0
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **26.8 MB (26844545 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:23e8689c159aa7100dcd0eaa27e838b42342bb048bbdb30a17abd725913dd2b0`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:36:36 GMT
ADD file:1aa4f2f1a9b196606788d4ef8971354311546ec87785e728eda6dd28dc8f0703 in / 
# Tue, 12 Oct 2021 01:36:37 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:af234ad4bc1a66f08150bb3e47a8655217a5881489b3d16455925299896aeb48`  
		Last Modified: Tue, 12 Oct 2021 01:54:25 GMT  
		Size: 26.8 MB (26844545 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-slim` - linux; arm64 variant v8

```console
$ docker pull debian@sha256:af707b5c78af806aaf2d1d6ec331c1b762b58cb93f95a3d8eaabe5d247dca739
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.7 MB (30748056 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:f4a5cb9dbdc7c7eb2093d86aa39f75c8dd017a5d7400842f01613d6a907a2fed`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:44:10 GMT
ADD file:75c40743ce1d29b5bfad592cdd0e7bfc50a4294cd2fc2ec8a843f5a547afbfb9 in / 
# Tue, 12 Oct 2021 01:44:11 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:41402d960b89f43a20d3b0c4116e3623db2ae2dd0cf72e13625714e0dec92fe5`  
		Last Modified: Tue, 12 Oct 2021 01:53:53 GMT  
		Size: 30.7 MB (30748056 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-slim` - linux; 386

```console
$ docker pull debian@sha256:02867a14ce4a996d6f184ae4abef54c066546c567f664bf7728f1f196d6c10ba
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **32.8 MB (32763493 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:482f53a8414dd8375bd522122b3c96c56094ec2fb3f0c6f7baac24925f503c9c`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:43:28 GMT
ADD file:1f6c67031a134cc1488a3dd5b1afc48aa91f1fec0ef43163ec45e701d7fb5749 in / 
# Tue, 12 Oct 2021 01:43:28 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:8cc6f6761bfcf3ebcef675e4caf6fbec3b60204771c0bf05086986885db488be`  
		Last Modified: Tue, 12 Oct 2021 01:54:00 GMT  
		Size: 32.8 MB (32763493 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-slim` - linux; mips64le

```console
$ docker pull debian@sha256:38fdd1f36c850b19a74240cad66459dd0bf602a55165a1259ef90aaa33a5c9cf
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.4 MB (30360328 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:99b7fec1197a157131a21239056ab2a719be0e0ada47605224848da0043a588f`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:16:25 GMT
ADD file:6993b092f03b469e7400686851bcfcebca618af437e056b1b666cef37b5b6153 in / 
# Tue, 12 Oct 2021 01:16:26 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:64a2ff80f178e33cf773999a407067f90dd9d3949b2dc79b39737ac25cf811c4`  
		Last Modified: Tue, 12 Oct 2021 01:28:08 GMT  
		Size: 30.4 MB (30360328 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-slim` - linux; ppc64le

```console
$ docker pull debian@sha256:e8381228ea8e6a2a603580be529c3cc8110b24d4d824d2adea3e77ba1a3ad802
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **35.9 MB (35935015 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d40481c899bc8e69850cc571fa027ae297f0d3fcdf5d02515f4ba43bbd5761af`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:31:16 GMT
ADD file:1e32ff7653c6a38b7fc55329c42957dd45ef1084a910cbd6219c735493d864b6 in / 
# Tue, 12 Oct 2021 01:31:22 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:49b65047f6921d4626cbf4019e684e789b9fee599da822a3305cb2cdccabb92c`  
		Last Modified: Tue, 12 Oct 2021 01:44:31 GMT  
		Size: 35.9 MB (35935015 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-slim` - linux; riscv64

```console
$ docker pull debian@sha256:105f953c74ec0c00abd27a3c2aa0bc61f2d37e16c3d99a707db829c9b74a03fe
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **27.6 MB (27563313 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:8c08e4d8abe9c035035af444293cd7f20ffa362541bfa2b71b9def71e975174b`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 01:19:32 GMT
ADD file:d023bb4e594d57c08428052992601ba913854065f3d8481129223f986c2eabb8 in / 
# Tue, 12 Oct 2021 01:19:34 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:5dad889f11fd648e2e18c46f30ea00d750cf9c392dbabfcc4f72c2e09e3d0af0`  
		Last Modified: Tue, 12 Oct 2021 01:35:28 GMT  
		Size: 27.6 MB (27563313 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `debian:unstable-slim` - linux; s390x

```console
$ docker pull debian@sha256:a989409c6363abb33a1a3d379073d3fddcf10d5e00cdd9b2f06c36f228f2aaa8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **30.0 MB (29984209 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:7787b1607637a684546f1ccc6a546b31c16edae7b06dda2232c4756f4bdba847`
-	Default Command: `["bash"]`

```dockerfile
# Tue, 12 Oct 2021 00:45:04 GMT
ADD file:9cf952a889ba9dc2c4e86a75aca595d35e0df14dec61963f64769d55df851e40 in / 
# Tue, 12 Oct 2021 00:45:05 GMT
CMD ["bash"]
```

-	Layers:
	-	`sha256:0c3ff48a9ed28edd72e1e14389926020dc484e092e7db31244fc783a52859923`  
		Last Modified: Tue, 12 Oct 2021 00:51:07 GMT  
		Size: 30.0 MB (29984209 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
