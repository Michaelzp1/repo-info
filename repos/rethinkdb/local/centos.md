# `rethinkdb:2.4.1-centos`

## Docker Metadata

- Image ID: `sha256:76992aa3ff22017132fc0d31a6c2cd470a5d8929b7cd7e71356d4421052abf03`
- Created: `2021-09-15T19:49:49.531540556Z`
- Virtual Size: ~ 294.82 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["rethinkdb","--bind","all"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `RETHINKDB_PACKAGE_VERSION=2.4.1`
- Labels:
  - `org.label-schema.build-date=20210915`
  - `org.label-schema.license=GPLv2`
  - `org.label-schema.name=CentOS Base Image`
  - `org.label-schema.schema-version=1.0`
  - `org.label-schema.vendor=CentOS`
