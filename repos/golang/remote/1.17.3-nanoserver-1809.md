## `golang:1.17.3-nanoserver-1809`

```console
$ docker pull golang@sha256:b2303d1e2e2c034e8c5ea2e074855b0ddbfb816e9252750f550ec76285bf399e
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	windows version 10.0.17763.2237; amd64

### `golang:1.17.3-nanoserver-1809` - windows version 10.0.17763.2237; amd64

```console
$ docker pull golang@sha256:473b364ae95d664bcaf43231a0dfb3d9936f8e65c442c323d13785134c3eb249
```

-	Docker Version: 20.10.8
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **247.8 MB (247847340 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:e8aca9e3dec3dafa837e6dc9195b6f5d607332b3260ed42f09a878c436e18310`
-	Default Command: `["c:\\windows\\system32\\cmd.exe"]`
-	`SHELL`: `["cmd","\/S","\/C"]`

```dockerfile
# Thu, 07 Oct 2021 08:01:14 GMT
RUN Apply image 1809-amd64
# Wed, 13 Oct 2021 12:53:34 GMT
SHELL [cmd /S /C]
# Wed, 13 Oct 2021 12:53:35 GMT
ENV GOPATH=C:\go
# Wed, 13 Oct 2021 12:53:36 GMT
USER ContainerAdministrator
# Wed, 13 Oct 2021 12:54:21 GMT
RUN setx /m PATH "%GOPATH%\bin;C:\Program Files\Go\bin;%PATH%"
# Wed, 13 Oct 2021 12:54:22 GMT
USER ContainerUser
# Thu, 04 Nov 2021 20:28:50 GMT
ENV GOLANG_VERSION=1.17.3
# Thu, 04 Nov 2021 20:31:05 GMT
COPY dir:109abd4b7de9c681888d02224b53efb3555fcce4cf01c933658c41331cc240cb in C:\Program Files\Go 
# Thu, 04 Nov 2021 20:31:49 GMT
RUN go version
# Thu, 04 Nov 2021 20:31:49 GMT
WORKDIR C:\go
```

-	Layers:
	-	`sha256:934e212983f208dc2bebc5de38259a6a62f1761868aacfee2cb3585a13b1e24b`  
		Size: 102.7 MB (102661372 bytes)  
		MIME: application/vnd.docker.image.rootfs.foreign.diff.tar.gzip
	-	`sha256:372f5dac265602794d3a4ddc20bd9845caf2acda0cec410704843c213a880da9`  
		Last Modified: Wed, 13 Oct 2021 13:29:46 GMT  
		Size: 1.2 KB (1168 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:183500fddc25e2dfe8ccd0711855ed60cf6ed8ea79a9a35d229e1404dcee495b`  
		Last Modified: Wed, 13 Oct 2021 13:29:45 GMT  
		Size: 1.1 KB (1131 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:0cd0fba843b7bfbab4c85a2bdddee6edca9a1abd62fd3a79f749ed662d3ce9cc`  
		Last Modified: Wed, 13 Oct 2021 13:29:46 GMT  
		Size: 1.1 KB (1139 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:97c71d616b79e5a5e1dc3c1bb13ae2c58cf3038ecc454f7114a77189d1958c02`  
		Last Modified: Wed, 13 Oct 2021 13:29:45 GMT  
		Size: 71.4 KB (71390 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:3d3f6872895200118f75543b31ccf5e8daa945cfaf05b3ab505bbe84f7f15a42`  
		Last Modified: Wed, 13 Oct 2021 13:29:43 GMT  
		Size: 1.1 KB (1117 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:6d54ac2eac012e3796270bb112fb83e47766fa12903f61c0a59a134e8b0e5312`  
		Last Modified: Thu, 04 Nov 2021 20:54:14 GMT  
		Size: 1.1 KB (1147 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:d67e6e5ad21fa3844df3850ef7ba55e281afad9bdf79d0fb26489edd594ec62f`  
		Last Modified: Thu, 04 Nov 2021 20:56:51 GMT  
		Size: 145.1 MB (145065441 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:e0daf805c056a62019475cbc922de2eba57c05fc333e83199f31f122dbbb29c4`  
		Last Modified: Thu, 04 Nov 2021 20:54:14 GMT  
		Size: 42.0 KB (42050 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:bf116623ee16f83036738ebfeba32cceebaa1a7fd9d365ec4948929de10bb2d9`  
		Last Modified: Thu, 04 Nov 2021 20:54:14 GMT  
		Size: 1.4 KB (1385 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
