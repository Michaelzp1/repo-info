## `golang:1.16.10-nanoserver-ltsc2022`

```console
$ docker pull golang@sha256:e8dc0c9b89cb89ca243bcc46d8219f27887b6c568c66e046e4aef76bdb01fa89
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	windows version 10.0.20348.288; amd64

### `golang:1.16.10-nanoserver-ltsc2022` - windows version 10.0.20348.288; amd64

```console
$ docker pull golang@sha256:a0d4b51a85b45be7d1e2b47112d7ea3b9d97256c0409dcf2aa658c62ba28ef01
```

-	Docker Version: 20.10.8
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **256.1 MB (256145752 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:fc5e75077401567779a49e08276dedbf53c1cf75de547cdd9400b8defe06e105`
-	Default Command: `["c:\\windows\\system32\\cmd.exe"]`
-	`SHELL`: `["cmd","\/S","\/C"]`

```dockerfile
# Thu, 07 Oct 2021 11:15:04 GMT
RUN Apply image ltsc2022-amd64
# Wed, 13 Oct 2021 12:48:50 GMT
SHELL [cmd /S /C]
# Wed, 13 Oct 2021 13:14:06 GMT
ENV GOPATH=C:\gopath
# Wed, 13 Oct 2021 13:14:07 GMT
USER ContainerAdministrator
# Wed, 13 Oct 2021 13:15:01 GMT
RUN setx /m PATH "%GOPATH%\bin;C:\go\bin;%PATH%"
# Wed, 13 Oct 2021 13:15:02 GMT
USER ContainerUser
# Thu, 04 Nov 2021 20:41:57 GMT
ENV GOLANG_VERSION=1.16.10
# Thu, 04 Nov 2021 20:44:03 GMT
COPY dir:7c36187e8b97805b62aa158319c234f9796386deca7028d36e4e4d0b0d9d6f4f in C:\go 
# Thu, 04 Nov 2021 20:44:53 GMT
RUN go version
# Thu, 04 Nov 2021 20:44:54 GMT
WORKDIR C:\gopath
```

-	Layers:
	-	`sha256:91284e7e8fd4bd7ebcfa98544a3e4f59639f38281225c81c34b6fe22e0dba4e5`  
		Size: 116.9 MB (116939483 bytes)  
		MIME: application/vnd.docker.image.rootfs.foreign.diff.tar.gzip
	-	`sha256:20e7750a023cb83652c7a9fb1fa59842126978ee34af47a4d3ed0508abf4b266`  
		Last Modified: Wed, 13 Oct 2021 13:28:53 GMT  
		Size: 1.2 KB (1182 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:2a481802b3b883baa1fc2eb4e39f0486435a014977f5e3f367fb206cccdc3651`  
		Last Modified: Wed, 13 Oct 2021 13:35:03 GMT  
		Size: 1.2 KB (1165 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:b7ccb1a946ed8e7a1cda0d5ee04620fe528947949e550ef501089ad1d368d9ee`  
		Last Modified: Wed, 13 Oct 2021 13:35:03 GMT  
		Size: 1.2 KB (1184 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:d310b02617c6a427ad026522be84c19698beabfc8ff9a589e8f11d5464b077ed`  
		Last Modified: Wed, 13 Oct 2021 13:35:03 GMT  
		Size: 85.1 KB (85061 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:6919951c215595f2cbdd02144dc1b678e23ecb3e13a5933cf3de064606a0cc14`  
		Last Modified: Wed, 13 Oct 2021 13:35:01 GMT  
		Size: 1.1 KB (1144 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:aa0a74e855b9da50d6e18826a80b8cdcd52e957447bfafd03e955e4cfd18f0b7`  
		Last Modified: Thu, 04 Nov 2021 21:01:15 GMT  
		Size: 1.1 KB (1133 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:bfb595fe48313a1b1b008f82d3e3668b7d6239ea7c3b6894f8f1a198bbd201ff`  
		Last Modified: Thu, 04 Nov 2021 21:01:44 GMT  
		Size: 139.1 MB (139062247 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:4eec80f8720356ba4eefb0470b5fb15e6ca8c4b0b2490ce24459237326a98159`  
		Last Modified: Thu, 04 Nov 2021 21:01:15 GMT  
		Size: 51.8 KB (51804 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:f34c4b3f78f7bc1999fa712fb87392a8b7ad2ddf22fed5e2cf6308744c0de95c`  
		Last Modified: Thu, 04 Nov 2021 21:01:15 GMT  
		Size: 1.3 KB (1349 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
