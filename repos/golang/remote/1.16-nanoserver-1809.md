## `golang:1.16-nanoserver-1809`

```console
$ docker pull golang@sha256:79bf791fe07fcb092abc38860c05742b6f4d97f6aded0b5201c8c17307e7e884
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	windows version 10.0.17763.2237; amd64

### `golang:1.16-nanoserver-1809` - windows version 10.0.17763.2237; amd64

```console
$ docker pull golang@sha256:b660465c432673b217bbeda3a433a46f79a513f89eab7724e1e24a21c989b45f
```

-	Docker Version: 20.10.8
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **241.9 MB (241887716 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:6f0172bab53beded6ace8bfd4158e65dda997d1d082a13cca5f8556e90851f15`
-	Default Command: `["c:\\windows\\system32\\cmd.exe"]`
-	`SHELL`: `["cmd","\/S","\/C"]`

```dockerfile
# Thu, 07 Oct 2021 08:01:14 GMT
RUN Apply image 1809-amd64
# Wed, 13 Oct 2021 12:53:34 GMT
SHELL [cmd /S /C]
# Wed, 13 Oct 2021 13:18:34 GMT
ENV GOPATH=C:\gopath
# Wed, 13 Oct 2021 13:18:35 GMT
USER ContainerAdministrator
# Wed, 13 Oct 2021 13:19:20 GMT
RUN setx /m PATH "%GOPATH%\bin;C:\go\bin;%PATH%"
# Wed, 13 Oct 2021 13:19:21 GMT
USER ContainerUser
# Thu, 04 Nov 2021 20:45:08 GMT
ENV GOLANG_VERSION=1.16.10
# Thu, 04 Nov 2021 20:47:05 GMT
COPY dir:7c36187e8b97805b62aa158319c234f9796386deca7028d36e4e4d0b0d9d6f4f in C:\go 
# Thu, 04 Nov 2021 20:47:47 GMT
RUN go version
# Thu, 04 Nov 2021 20:47:48 GMT
WORKDIR C:\gopath
```

-	Layers:
	-	`sha256:934e212983f208dc2bebc5de38259a6a62f1761868aacfee2cb3585a13b1e24b`  
		Size: 102.7 MB (102661372 bytes)  
		MIME: application/vnd.docker.image.rootfs.foreign.diff.tar.gzip
	-	`sha256:372f5dac265602794d3a4ddc20bd9845caf2acda0cec410704843c213a880da9`  
		Last Modified: Wed, 13 Oct 2021 13:29:46 GMT  
		Size: 1.2 KB (1168 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:e4693f04cfbbf4be1b0bc27ecb5b5dcedede16505c2e9ce2c68fc562e07d2da2`  
		Last Modified: Wed, 13 Oct 2021 13:37:35 GMT  
		Size: 1.2 KB (1167 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:89deba1445adbb16d40bbd41721b476eef20a17698d1aa87b66bc85b43edd509`  
		Last Modified: Wed, 13 Oct 2021 13:37:35 GMT  
		Size: 1.2 KB (1166 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:496c953186869b4416189c7cb67782911a6c383a1e51a285ac3ba57a6d08c752`  
		Last Modified: Wed, 13 Oct 2021 13:37:35 GMT  
		Size: 75.8 KB (75795 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:a9547e942abc349726cb7a251b94d0681b831ab6a170c0e2dbb46761d5998be0`  
		Last Modified: Wed, 13 Oct 2021 13:37:33 GMT  
		Size: 1.2 KB (1173 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:c306a9d980edda30b2ff17652e6150fcbb2f28cac40b862c1212a36e48cb1c84`  
		Last Modified: Thu, 04 Nov 2021 21:01:55 GMT  
		Size: 1.2 KB (1163 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:13528b539a7b23cbb37fa1883797cf8c5ff0a97d89e1c67897a4a4be052fb955`  
		Last Modified: Thu, 04 Nov 2021 21:02:22 GMT  
		Size: 139.1 MB (139061543 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:a6cf8f0523298a41ab5f6d1ca3486a7e9bd778f0eee7e68edda3af0ab72ea628`  
		Last Modified: Thu, 04 Nov 2021 21:01:55 GMT  
		Size: 81.8 KB (81784 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:9f81d62379d854cf93260d8e9f8aed2cc2af10fa58f0fc7173ea9c135c25b9ae`  
		Last Modified: Thu, 04 Nov 2021 21:01:55 GMT  
		Size: 1.4 KB (1385 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
