## `rakudo-star:alpine`

```console
$ docker pull rakudo-star@sha256:91c749be4bfb9aeda09997210eaf6a2a82bf1bf4f466ab29b42c432533713522
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 2
	-	linux; amd64
	-	linux; arm64 variant v8

### `rakudo-star:alpine` - linux; amd64

```console
$ docker pull rakudo-star@sha256:afa1287ffd0dd7a4f7e9eda4413a6db44fea0349267867eb4d045c53aa62d870
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **44.3 MB (44253891 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:fcc6b2308c66d4f98d46093f619d6c6ecef75713487ae0d609bb75c6af15589c`
-	Default Command: `["raku"]`

```dockerfile
# Tue, 31 Aug 2021 23:18:16 GMT
ADD file:ecdfb91a737d6c292265c1b77ffd3d82ba810dd43ea4ef79714b66b1da74a5aa in / 
# Tue, 31 Aug 2021 23:18:16 GMT
CMD ["/bin/sh"]
# Wed, 01 Sep 2021 05:31:21 GMT
RUN addgroup -S raku && adduser -S raku -G raku
# Wed, 01 Sep 2021 05:31:22 GMT
ARG rakudo_version=2021.04
# Wed, 01 Sep 2021 05:31:22 GMT
ENV rakudo_version=2021.04
# Wed, 01 Sep 2021 05:42:29 GMT
RUN buildDeps='         bash         gcc         gnupg         libc-dev         make         perl     '         url="https://rakudo.org/dl/star/rakudo-star-${rakudo_version}.tar.gz"     keyserver='ha.pool.sks-keyservers.net'     keyfp='B6F697742EFCAF5F23CE51D5031D65902E840821'     tmpdir="$(mktemp -d)"     && set -eux     && export GNUPGHOME="$tmpdir"     && apk add --no-cache --virtual .build-deps $buildDeps     && mkdir ${tmpdir}/rakudo         && wget ${url}.asc -O ${tmpdir}/rakudo.tar.gz.asc     && wget $url -O ${tmpdir}/rakudo.tar.gz     && gpg --batch --keyserver $keyserver --recv-keys $keyfp     && gpg --batch --verify ${tmpdir}/rakudo.tar.gz.asc ${tmpdir}/rakudo.tar.gz         && tar xzf ${tmpdir}/rakudo.tar.gz --strip-components=1 -C ${tmpdir}/rakudo     && (         cd ${tmpdir}/rakudo         && bash bin/rstar install -p /usr     )     && rm -rf $tmpdir     && apk del --no-network .build-deps
# Wed, 01 Sep 2021 05:42:30 GMT
ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/share/perl6/core/bin:/usr/share/perl6/site/bin:/usr/share/perl6/vendor/bin
# Wed, 01 Sep 2021 05:42:30 GMT
CMD ["raku"]
```

-	Layers:
	-	`sha256:4e9f2cdf438714c2c4533e28c6c41a89cc6c1b46cf77e54c488db30ca4f5b6f3`  
		Last Modified: Tue, 31 Aug 2021 23:18:55 GMT  
		Size: 2.8 MB (2814079 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:808c2518553c6a54b0365432153e8136c91eefdaea94c98566615a36b2b2df89`  
		Last Modified: Wed, 01 Sep 2021 05:42:44 GMT  
		Size: 1.3 KB (1256 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:2096ab665aeaeab23021868ff13652759683bde4c547dbc030f93790238987d3`  
		Last Modified: Wed, 01 Sep 2021 05:42:52 GMT  
		Size: 41.4 MB (41438556 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `rakudo-star:alpine` - linux; arm64 variant v8

```console
$ docker pull rakudo-star@sha256:c25f332674af3de7a8937b1f338ef237e7224a06b362c5ab5b9be59165d7dc4c
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **43.9 MB (43941189 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:cc6c059a8ec78031e860703d57870629573b3c36683d8b1b5e7e447db45e5929`
-	Default Command: `["raku"]`

```dockerfile
# Wed, 01 Sep 2021 02:50:45 GMT
ADD file:924de68748d5d710724ceb45b3bff9d38eedcad50d5744be4ce74f8f731a791f in / 
# Wed, 01 Sep 2021 02:50:45 GMT
CMD ["/bin/sh"]
# Tue, 12 Oct 2021 22:10:42 GMT
RUN addgroup -S raku && adduser -S raku -G raku
# Tue, 12 Oct 2021 22:10:43 GMT
ARG rakudo_version=2021.04
# Tue, 12 Oct 2021 22:10:44 GMT
ENV rakudo_version=2021.04
# Tue, 12 Oct 2021 22:21:44 GMT
RUN buildDeps='         bash         gcc         gnupg         libc-dev         make         perl     '         url="https://rakudo.org/dl/star/rakudo-star-${rakudo_version}.tar.gz"     keyserver='ha.pool.sks-keyservers.net'     keyfp='B6F697742EFCAF5F23CE51D5031D65902E840821'     tmpdir="$(mktemp -d)"     && set -eux     && export GNUPGHOME="$tmpdir"     && apk add --no-cache --virtual .build-deps $buildDeps     && mkdir ${tmpdir}/rakudo         && wget ${url}.asc -O ${tmpdir}/rakudo.tar.gz.asc     && wget $url -O ${tmpdir}/rakudo.tar.gz     && gpg --batch --keyserver $keyserver --recv-keys $keyfp     && gpg --batch --verify ${tmpdir}/rakudo.tar.gz.asc ${tmpdir}/rakudo.tar.gz         && tar xzf ${tmpdir}/rakudo.tar.gz --strip-components=1 -C ${tmpdir}/rakudo     && (         cd ${tmpdir}/rakudo         && bash bin/rstar install -p /usr     )     && rm -rf $tmpdir     && apk del --no-network .build-deps
# Tue, 12 Oct 2021 22:21:44 GMT
ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/share/perl6/core/bin:/usr/share/perl6/site/bin:/usr/share/perl6/vendor/bin
# Tue, 12 Oct 2021 22:21:45 GMT
CMD ["raku"]
```

-	Layers:
	-	`sha256:bbf911997326f5b56d515142e8dbdbe01d2f308276938ddbce3ab347584ed8ce`  
		Last Modified: Wed, 01 Sep 2021 02:51:37 GMT  
		Size: 2.7 MB (2713008 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:388c973c6a62746888ed56a11a4b75839cbf9808be0790bb83e04b1008bca2cb`  
		Last Modified: Tue, 12 Oct 2021 22:22:33 GMT  
		Size: 1.2 KB (1230 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:c91e76aa6befd7627b22b40163f5b57d33f99a9282fa6e0fc0ceadbb658a650a`  
		Last Modified: Tue, 12 Oct 2021 22:22:41 GMT  
		Size: 41.2 MB (41226951 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
