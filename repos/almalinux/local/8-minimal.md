# `almalinux:minimal`

## Docker Metadata

- Image ID: `sha256:0d6d00fb62f253cd8258ac74aa28e57415b47dac0ecf0e69117ee92318210b3a`
- Created: `2021-09-16T22:20:28.036352467Z`
- Virtual Size: ~ 102.44 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["/bin/bash"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
