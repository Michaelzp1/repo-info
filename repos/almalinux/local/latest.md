# `almalinux:latest`

## Docker Metadata

- Image ID: `sha256:18d68cc3378089aca428e2c52c39c20a00da4ded9483a4fab459d2b0fc87327f`
- Created: `2021-09-16T22:20:15.820418833Z`
- Virtual Size: ~ 210.04 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["/bin/bash"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
