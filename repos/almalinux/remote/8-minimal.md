## `almalinux:8-minimal`

```console
$ docker pull almalinux@sha256:e85b60707d7b1474d35ff4678d5bd64d1a70fb0bab17a9ead4d950d5c3ad9754
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 2
	-	linux; amd64
	-	linux; arm64 variant v8

### `almalinux:8-minimal` - linux; amd64

```console
$ docker pull almalinux@sha256:a0fb8a84f9b74d034e8ad09a37a484f8fb6f638ba8e5de1f1d33d50f13ca7c8e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **39.4 MB (39409694 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:0d6d00fb62f253cd8258ac74aa28e57415b47dac0ecf0e69117ee92318210b3a`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 16 Sep 2021 22:20:27 GMT
ADD file:77c97996fa61b9545119c1c435a45501ac2f2abb1d11b2e58a760472a1d440f6 in / 
# Thu, 16 Sep 2021 22:20:28 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:0fd6f21791398e15de2f2985065f9d707950a6812f5de1fcb70e0d19e4eaa775`  
		Last Modified: Thu, 16 Sep 2021 22:21:12 GMT  
		Size: 39.4 MB (39409694 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `almalinux:8-minimal` - linux; arm64 variant v8

```console
$ docker pull almalinux@sha256:23c2e3e5968ecde93fb4e2aabbecb6dd41e0fb4302aba44a79e4c4fd2b1d1635
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **39.1 MB (39120187 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:76b36b311c425bc1de257d5970f4287351fc57b540e5e5f3f2b55df97219f958`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 16 Sep 2021 22:39:37 GMT
ADD file:e1635a656103eadc2b9cb2bc78642faba94d0f44f326993f9d1e426fe2f0c8c4 in / 
# Thu, 16 Sep 2021 22:39:38 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:c78224c25b9f6250906a3e79a43538cc9ce9ad1e75c84eea4e35f990474dca2a`  
		Last Modified: Thu, 16 Sep 2021 22:40:32 GMT  
		Size: 39.1 MB (39120187 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
