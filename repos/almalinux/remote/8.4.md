## `almalinux:8.4`

```console
$ docker pull almalinux@sha256:b2e1ecb0bac82071625d81bad348cda7dc92b8a21b01a8cc0a6d489e4e0edd2f
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 2
	-	linux; amd64
	-	linux; arm64 variant v8

### `almalinux:8.4` - linux; amd64

```console
$ docker pull almalinux@sha256:73171963d158bda51d2bdf1e70a2cffef159fa391a608849f9ce34d259562625
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **77.0 MB (76951010 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:18d68cc3378089aca428e2c52c39c20a00da4ded9483a4fab459d2b0fc87327f`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 16 Sep 2021 22:20:15 GMT
ADD file:00c2aab0ab095bfeb660f384ecf2843852bc955e5b43b3563761c428edf2448a in / 
# Thu, 16 Sep 2021 22:20:15 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:5638f9e2aa61b544b54abe29657f3bf9a09fe0e4489e5af0d587807a90725fde`  
		Last Modified: Thu, 16 Sep 2021 22:20:53 GMT  
		Size: 77.0 MB (76951010 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `almalinux:8.4` - linux; arm64 variant v8

```console
$ docker pull almalinux@sha256:1408146154a68b1dfd8bd62a8490f76b65f9e9fc8ec72f869ccab88f19591db7
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **77.2 MB (77247636 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ed8fef7500f9ed30fcb1c8346bc51112d032f4e7f6fb9628a564b30cbd11f7ba`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 16 Sep 2021 22:39:26 GMT
ADD file:6c171abf25245f8496a13510bce7a4d394f5582113710d4e4fbfebe0b1e73738 in / 
# Thu, 16 Sep 2021 22:39:27 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:85b6b0c99d95d3b2f921a7c83033f808bb86b074e1feaf148086916d326a1445`  
		Last Modified: Thu, 16 Sep 2021 22:40:11 GMT  
		Size: 77.2 MB (77247636 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
