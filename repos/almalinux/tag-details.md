<!-- THIS FILE IS GENERATED VIA './update-remote.sh' -->

# Tags of `almalinux`

-	[`almalinux:8`](#almalinux8)
-	[`almalinux:8-minimal`](#almalinux8-minimal)
-	[`almalinux:8.4`](#almalinux84)
-	[`almalinux:8.4-minimal`](#almalinux84-minimal)
-	[`almalinux:latest`](#almalinuxlatest)
-	[`almalinux:minimal`](#almalinuxminimal)

## `almalinux:8`

```console
$ docker pull almalinux@sha256:b2e1ecb0bac82071625d81bad348cda7dc92b8a21b01a8cc0a6d489e4e0edd2f
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 2
	-	linux; amd64
	-	linux; arm64 variant v8

### `almalinux:8` - linux; amd64

```console
$ docker pull almalinux@sha256:73171963d158bda51d2bdf1e70a2cffef159fa391a608849f9ce34d259562625
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **77.0 MB (76951010 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:18d68cc3378089aca428e2c52c39c20a00da4ded9483a4fab459d2b0fc87327f`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 16 Sep 2021 22:20:15 GMT
ADD file:00c2aab0ab095bfeb660f384ecf2843852bc955e5b43b3563761c428edf2448a in / 
# Thu, 16 Sep 2021 22:20:15 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:5638f9e2aa61b544b54abe29657f3bf9a09fe0e4489e5af0d587807a90725fde`  
		Last Modified: Thu, 16 Sep 2021 22:20:53 GMT  
		Size: 77.0 MB (76951010 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `almalinux:8` - linux; arm64 variant v8

```console
$ docker pull almalinux@sha256:1408146154a68b1dfd8bd62a8490f76b65f9e9fc8ec72f869ccab88f19591db7
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **77.2 MB (77247636 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ed8fef7500f9ed30fcb1c8346bc51112d032f4e7f6fb9628a564b30cbd11f7ba`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 16 Sep 2021 22:39:26 GMT
ADD file:6c171abf25245f8496a13510bce7a4d394f5582113710d4e4fbfebe0b1e73738 in / 
# Thu, 16 Sep 2021 22:39:27 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:85b6b0c99d95d3b2f921a7c83033f808bb86b074e1feaf148086916d326a1445`  
		Last Modified: Thu, 16 Sep 2021 22:40:11 GMT  
		Size: 77.2 MB (77247636 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `almalinux:8-minimal`

```console
$ docker pull almalinux@sha256:e85b60707d7b1474d35ff4678d5bd64d1a70fb0bab17a9ead4d950d5c3ad9754
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 2
	-	linux; amd64
	-	linux; arm64 variant v8

### `almalinux:8-minimal` - linux; amd64

```console
$ docker pull almalinux@sha256:a0fb8a84f9b74d034e8ad09a37a484f8fb6f638ba8e5de1f1d33d50f13ca7c8e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **39.4 MB (39409694 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:0d6d00fb62f253cd8258ac74aa28e57415b47dac0ecf0e69117ee92318210b3a`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 16 Sep 2021 22:20:27 GMT
ADD file:77c97996fa61b9545119c1c435a45501ac2f2abb1d11b2e58a760472a1d440f6 in / 
# Thu, 16 Sep 2021 22:20:28 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:0fd6f21791398e15de2f2985065f9d707950a6812f5de1fcb70e0d19e4eaa775`  
		Last Modified: Thu, 16 Sep 2021 22:21:12 GMT  
		Size: 39.4 MB (39409694 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `almalinux:8-minimal` - linux; arm64 variant v8

```console
$ docker pull almalinux@sha256:23c2e3e5968ecde93fb4e2aabbecb6dd41e0fb4302aba44a79e4c4fd2b1d1635
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **39.1 MB (39120187 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:76b36b311c425bc1de257d5970f4287351fc57b540e5e5f3f2b55df97219f958`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 16 Sep 2021 22:39:37 GMT
ADD file:e1635a656103eadc2b9cb2bc78642faba94d0f44f326993f9d1e426fe2f0c8c4 in / 
# Thu, 16 Sep 2021 22:39:38 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:c78224c25b9f6250906a3e79a43538cc9ce9ad1e75c84eea4e35f990474dca2a`  
		Last Modified: Thu, 16 Sep 2021 22:40:32 GMT  
		Size: 39.1 MB (39120187 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `almalinux:8.4`

```console
$ docker pull almalinux@sha256:b2e1ecb0bac82071625d81bad348cda7dc92b8a21b01a8cc0a6d489e4e0edd2f
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 2
	-	linux; amd64
	-	linux; arm64 variant v8

### `almalinux:8.4` - linux; amd64

```console
$ docker pull almalinux@sha256:73171963d158bda51d2bdf1e70a2cffef159fa391a608849f9ce34d259562625
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **77.0 MB (76951010 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:18d68cc3378089aca428e2c52c39c20a00da4ded9483a4fab459d2b0fc87327f`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 16 Sep 2021 22:20:15 GMT
ADD file:00c2aab0ab095bfeb660f384ecf2843852bc955e5b43b3563761c428edf2448a in / 
# Thu, 16 Sep 2021 22:20:15 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:5638f9e2aa61b544b54abe29657f3bf9a09fe0e4489e5af0d587807a90725fde`  
		Last Modified: Thu, 16 Sep 2021 22:20:53 GMT  
		Size: 77.0 MB (76951010 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `almalinux:8.4` - linux; arm64 variant v8

```console
$ docker pull almalinux@sha256:1408146154a68b1dfd8bd62a8490f76b65f9e9fc8ec72f869ccab88f19591db7
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **77.2 MB (77247636 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ed8fef7500f9ed30fcb1c8346bc51112d032f4e7f6fb9628a564b30cbd11f7ba`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 16 Sep 2021 22:39:26 GMT
ADD file:6c171abf25245f8496a13510bce7a4d394f5582113710d4e4fbfebe0b1e73738 in / 
# Thu, 16 Sep 2021 22:39:27 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:85b6b0c99d95d3b2f921a7c83033f808bb86b074e1feaf148086916d326a1445`  
		Last Modified: Thu, 16 Sep 2021 22:40:11 GMT  
		Size: 77.2 MB (77247636 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `almalinux:8.4-minimal`

```console
$ docker pull almalinux@sha256:e85b60707d7b1474d35ff4678d5bd64d1a70fb0bab17a9ead4d950d5c3ad9754
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 2
	-	linux; amd64
	-	linux; arm64 variant v8

### `almalinux:8.4-minimal` - linux; amd64

```console
$ docker pull almalinux@sha256:a0fb8a84f9b74d034e8ad09a37a484f8fb6f638ba8e5de1f1d33d50f13ca7c8e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **39.4 MB (39409694 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:0d6d00fb62f253cd8258ac74aa28e57415b47dac0ecf0e69117ee92318210b3a`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 16 Sep 2021 22:20:27 GMT
ADD file:77c97996fa61b9545119c1c435a45501ac2f2abb1d11b2e58a760472a1d440f6 in / 
# Thu, 16 Sep 2021 22:20:28 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:0fd6f21791398e15de2f2985065f9d707950a6812f5de1fcb70e0d19e4eaa775`  
		Last Modified: Thu, 16 Sep 2021 22:21:12 GMT  
		Size: 39.4 MB (39409694 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `almalinux:8.4-minimal` - linux; arm64 variant v8

```console
$ docker pull almalinux@sha256:23c2e3e5968ecde93fb4e2aabbecb6dd41e0fb4302aba44a79e4c4fd2b1d1635
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **39.1 MB (39120187 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:76b36b311c425bc1de257d5970f4287351fc57b540e5e5f3f2b55df97219f958`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 16 Sep 2021 22:39:37 GMT
ADD file:e1635a656103eadc2b9cb2bc78642faba94d0f44f326993f9d1e426fe2f0c8c4 in / 
# Thu, 16 Sep 2021 22:39:38 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:c78224c25b9f6250906a3e79a43538cc9ce9ad1e75c84eea4e35f990474dca2a`  
		Last Modified: Thu, 16 Sep 2021 22:40:32 GMT  
		Size: 39.1 MB (39120187 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `almalinux:latest`

```console
$ docker pull almalinux@sha256:b2e1ecb0bac82071625d81bad348cda7dc92b8a21b01a8cc0a6d489e4e0edd2f
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 2
	-	linux; amd64
	-	linux; arm64 variant v8

### `almalinux:latest` - linux; amd64

```console
$ docker pull almalinux@sha256:73171963d158bda51d2bdf1e70a2cffef159fa391a608849f9ce34d259562625
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **77.0 MB (76951010 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:18d68cc3378089aca428e2c52c39c20a00da4ded9483a4fab459d2b0fc87327f`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 16 Sep 2021 22:20:15 GMT
ADD file:00c2aab0ab095bfeb660f384ecf2843852bc955e5b43b3563761c428edf2448a in / 
# Thu, 16 Sep 2021 22:20:15 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:5638f9e2aa61b544b54abe29657f3bf9a09fe0e4489e5af0d587807a90725fde`  
		Last Modified: Thu, 16 Sep 2021 22:20:53 GMT  
		Size: 77.0 MB (76951010 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `almalinux:latest` - linux; arm64 variant v8

```console
$ docker pull almalinux@sha256:1408146154a68b1dfd8bd62a8490f76b65f9e9fc8ec72f869ccab88f19591db7
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **77.2 MB (77247636 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:ed8fef7500f9ed30fcb1c8346bc51112d032f4e7f6fb9628a564b30cbd11f7ba`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 16 Sep 2021 22:39:26 GMT
ADD file:6c171abf25245f8496a13510bce7a4d394f5582113710d4e4fbfebe0b1e73738 in / 
# Thu, 16 Sep 2021 22:39:27 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:85b6b0c99d95d3b2f921a7c83033f808bb86b074e1feaf148086916d326a1445`  
		Last Modified: Thu, 16 Sep 2021 22:40:11 GMT  
		Size: 77.2 MB (77247636 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

## `almalinux:minimal`

```console
$ docker pull almalinux@sha256:e85b60707d7b1474d35ff4678d5bd64d1a70fb0bab17a9ead4d950d5c3ad9754
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 2
	-	linux; amd64
	-	linux; arm64 variant v8

### `almalinux:minimal` - linux; amd64

```console
$ docker pull almalinux@sha256:a0fb8a84f9b74d034e8ad09a37a484f8fb6f638ba8e5de1f1d33d50f13ca7c8e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **39.4 MB (39409694 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:0d6d00fb62f253cd8258ac74aa28e57415b47dac0ecf0e69117ee92318210b3a`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 16 Sep 2021 22:20:27 GMT
ADD file:77c97996fa61b9545119c1c435a45501ac2f2abb1d11b2e58a760472a1d440f6 in / 
# Thu, 16 Sep 2021 22:20:28 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:0fd6f21791398e15de2f2985065f9d707950a6812f5de1fcb70e0d19e4eaa775`  
		Last Modified: Thu, 16 Sep 2021 22:21:12 GMT  
		Size: 39.4 MB (39409694 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `almalinux:minimal` - linux; arm64 variant v8

```console
$ docker pull almalinux@sha256:23c2e3e5968ecde93fb4e2aabbecb6dd41e0fb4302aba44a79e4c4fd2b1d1635
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **39.1 MB (39120187 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:76b36b311c425bc1de257d5970f4287351fc57b540e5e5f3f2b55df97219f958`
-	Default Command: `["\/bin\/bash"]`

```dockerfile
# Thu, 16 Sep 2021 22:39:37 GMT
ADD file:e1635a656103eadc2b9cb2bc78642faba94d0f44f326993f9d1e426fe2f0c8c4 in / 
# Thu, 16 Sep 2021 22:39:38 GMT
CMD ["/bin/bash"]
```

-	Layers:
	-	`sha256:c78224c25b9f6250906a3e79a43538cc9ce9ad1e75c84eea4e35f990474dca2a`  
		Last Modified: Thu, 16 Sep 2021 22:40:32 GMT  
		Size: 39.1 MB (39120187 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
