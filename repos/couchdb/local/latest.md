# `couchdb:latest`

## Docker Metadata

- Image ID: `sha256:5c2fff885de6eb95e7060e056bff8a68415b76592d6a9a799dac45ba5989085b`
- Created: `2021-10-12T12:32:47.159224314Z`
- Virtual Size: ~ 185.19 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["tini","--","/docker-entrypoint.sh"]`
- Command: `["/opt/couchdb/bin/couchdb"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `GPG_COUCH_KEY=390EF70BB1EA12B2773962950EE62FB37A00258D`
  - `COUCHDB_VERSION=3.2.0`
- Labels:
  - `maintainer=CouchDB Developers dev@couchdb.apache.org`
