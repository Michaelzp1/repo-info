## `couchdb:latest`

```console
$ docker pull couchdb@sha256:6d9aea691cf15e0b185f8b0c245e3693cb1f367a502a24a5fb2d9a97ed45a076
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 2
	-	linux; amd64
	-	linux; arm64 variant v8

### `couchdb:latest` - linux; amd64

```console
$ docker pull couchdb@sha256:e150f08d4781b3d0ba2b2328896615436add41ebbc1ae8f46dbba0d5b50ef756
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **80.5 MB (80541108 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:5c2fff885de6eb95e7060e056bff8a68415b76592d6a9a799dac45ba5989085b`
-	Entrypoint: `["tini","--","\/docker-entrypoint.sh"]`
-	Default Command: `["\/opt\/couchdb\/bin\/couchdb"]`

```dockerfile
# Tue, 12 Oct 2021 01:21:05 GMT
ADD file:910392427fdf089bc26b64d6dc450ff3d020c7c1a474d85b2f9298134d0007bd in / 
# Tue, 12 Oct 2021 01:21:05 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 12:32:07 GMT
LABEL maintainer=CouchDB Developers dev@couchdb.apache.org
# Tue, 12 Oct 2021 12:32:08 GMT
RUN groupadd -g 5984 -r couchdb && useradd -u 5984 -d /opt/couchdb -g couchdb couchdb
# Tue, 12 Oct 2021 12:32:18 GMT
RUN set -ex;     apt-get update;     apt-get install -y --no-install-recommends         apt-transport-https         ca-certificates         dirmngr         gnupg      ;     rm -rf /var/lib/apt/lists/*
# Tue, 12 Oct 2021 12:32:22 GMT
RUN set -eux;     apt-get update;     apt-get install -y --no-install-recommends gosu tini;     rm -rf /var/lib/apt/lists/*;     gosu nobody true;     tini --version
# Tue, 12 Oct 2021 12:32:22 GMT
ENV GPG_COUCH_KEY=390EF70BB1EA12B2773962950EE62FB37A00258D
# Tue, 12 Oct 2021 12:32:29 GMT
RUN set -eux;     apt-get update;     apt-get install -y curl;     export GNUPGHOME="$(mktemp -d)";     curl -fL -o keys.asc https://couchdb.apache.org/repo/keys.asc;     gpg --batch --import keys.asc;     gpg --batch --export "${GPG_COUCH_KEY}" > /usr/share/keyrings/couchdb-archive-keyring.gpg;     command -v gpgconf && gpgconf --kill all || :;     rm -rf "$GNUPGHOME";     apt-key list;     apt purge -y --autoremove curl;     rm -rf /var/lib/apt/lists/*
# Tue, 12 Oct 2021 12:32:29 GMT
ENV COUCHDB_VERSION=3.2.0
# Tue, 12 Oct 2021 12:32:30 GMT
RUN . /etc/os-release;     echo "deb [signed-by=/usr/share/keyrings/couchdb-archive-keyring.gpg] https://apache.jfrog.io/artifactory/couchdb-deb/ ${VERSION_CODENAME} main" |         tee /etc/apt/sources.list.d/couchdb.list >/dev/null
# Tue, 12 Oct 2021 12:32:44 GMT
RUN set -eux;     apt-get update;         echo "couchdb couchdb/mode select none" | debconf-set-selections;     DEBIAN_FRONTEND=noninteractive apt-get install -y --allow-downgrades --allow-remove-essential --allow-change-held-packages             couchdb="$COUCHDB_VERSION"~buster     ;     rmdir /var/lib/couchdb /var/log/couchdb;     rm /opt/couchdb/data /opt/couchdb/var/log;     mkdir -p /opt/couchdb/data /opt/couchdb/var/log;     chown couchdb:couchdb /opt/couchdb/data /opt/couchdb/var/log;     chmod 777 /opt/couchdb/data /opt/couchdb/var/log;     rm /opt/couchdb/etc/default.d/10-filelog.ini;     find /opt/couchdb \! \( -user couchdb -group couchdb \) -exec chown -f couchdb:couchdb '{}' +;     find /opt/couchdb/etc -type d ! -perm 0755 -exec chmod -f 0755 '{}' +;     find /opt/couchdb/etc -type f ! -perm 0644 -exec chmod -f 0644 '{}' +;     chmod -f 0777 /opt/couchdb/etc/local.d;     rm -rf /var/lib/apt/lists/*;
# Tue, 12 Oct 2021 12:32:45 GMT
COPY --chown=couchdb:couchdbfile:459581cb8ff69dbc1cb246db7b488d5b6127e57fcbb0d0df1288722b5cd25111 in /opt/couchdb/etc/default.d/ 
# Tue, 12 Oct 2021 12:32:45 GMT
COPY --chown=couchdb:couchdbfile:f98e48e4254cb3ec4a766f3b9bd3260f16676a310eb0356ee9775c62edb3e8f3 in /opt/couchdb/etc/ 
# Tue, 12 Oct 2021 12:32:45 GMT
COPY file:5f96ca1bf2f6f650a65a16c93abec310412df7ca501bf32df2ac20f99b1a0742 in /usr/local/bin 
# Tue, 12 Oct 2021 12:32:46 GMT
RUN ln -s usr/local/bin/docker-entrypoint.sh /docker-entrypoint.sh # backwards compat
# Tue, 12 Oct 2021 12:32:46 GMT
ENTRYPOINT ["tini" "--" "/docker-entrypoint.sh"]
# Tue, 12 Oct 2021 12:32:46 GMT
VOLUME [/opt/couchdb/data]
# Tue, 12 Oct 2021 12:32:46 GMT
EXPOSE 4369 5984 9100
# Tue, 12 Oct 2021 12:32:47 GMT
CMD ["/opt/couchdb/bin/couchdb"]
```

-	Layers:
	-	`sha256:b380bbd43752f83945df8b5d1074fef8dd044820e7d3aef33b655a2483e030c7`  
		Last Modified: Tue, 12 Oct 2021 01:26:51 GMT  
		Size: 27.1 MB (27139510 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:f9a46377fe8ce67057386a4c7aa933b926be606ba3070c3342e42f63e68d052c`  
		Last Modified: Tue, 12 Oct 2021 12:34:00 GMT  
		Size: 3.4 KB (3412 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:9f4bf1b8a9bee200ee03e321a3d81949a87a559329ccffe9da773bd2d9d09fab`  
		Last Modified: Tue, 12 Oct 2021 12:34:00 GMT  
		Size: 6.7 MB (6691348 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:59081fa6a5d78e2274b9368f09dff77dc9e27812416dc235da1682d043b6129d`  
		Last Modified: Tue, 12 Oct 2021 12:33:58 GMT  
		Size: 1.3 MB (1258348 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:bc87af85d712b9d439d847c5575c30770dcd0910cd5aea578135b4a2d4e5f16e`  
		Last Modified: Tue, 12 Oct 2021 12:33:58 GMT  
		Size: 293.0 KB (292995 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:f937941c5ec804af4fe48f885c0f4a7cf4bb52686fa4f351789e0822a0a53010`  
		Last Modified: Tue, 12 Oct 2021 12:33:58 GMT  
		Size: 277.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:a9def6a2a7f7afb329b52aa79f6e7e364e4c0b1fc5f6456560d340092b1b11c1`  
		Last Modified: Tue, 12 Oct 2021 12:34:01 GMT  
		Size: 45.2 MB (45151893 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:c5ab67db1541e933f5d72714f7addf00e24526db1fb69a90ee4a23eb797acf32`  
		Last Modified: Tue, 12 Oct 2021 12:33:56 GMT  
		Size: 382.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:80723a7fbea8d1f758f276151c162b1bc34bcb58360f6056c219e4fab23328fc`  
		Last Modified: Tue, 12 Oct 2021 12:33:56 GMT  
		Size: 764.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:a57f490f20804c696113a543f5d12073356e77950f65b8da9b5e95546e7dd0a5`  
		Last Modified: Tue, 12 Oct 2021 12:33:55 GMT  
		Size: 2.1 KB (2058 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:ceefecb83d7b123a6fa5a0374e5bea18fba9de64bcedacdc4f36d1160d93c528`  
		Last Modified: Tue, 12 Oct 2021 12:33:56 GMT  
		Size: 121.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `couchdb:latest` - linux; arm64 variant v8

```console
$ docker pull couchdb@sha256:7b32c474698f734ff5c42be52ce150bc856f587316fe391b789278bbaf452cc3
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **75.6 MB (75645542 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3d439facf9dca9ce3276743e44eef177c831b96f9a58c4611aa95eccdb37280f`
-	Entrypoint: `["tini","--","\/docker-entrypoint.sh"]`
-	Default Command: `["\/opt\/couchdb\/bin\/couchdb"]`

```dockerfile
# Tue, 12 Oct 2021 01:41:42 GMT
ADD file:f0d53027a7ba594477674127971aa477af3a2bc6bcddef6c0aa174953a5f2db0 in / 
# Tue, 12 Oct 2021 01:41:42 GMT
CMD ["bash"]
# Tue, 12 Oct 2021 16:12:13 GMT
LABEL maintainer=CouchDB Developers dev@couchdb.apache.org
# Tue, 12 Oct 2021 16:12:14 GMT
RUN groupadd -g 5984 -r couchdb && useradd -u 5984 -d /opt/couchdb -g couchdb couchdb
# Tue, 12 Oct 2021 16:12:21 GMT
RUN set -ex;     apt-get update;     apt-get install -y --no-install-recommends         apt-transport-https         ca-certificates         dirmngr         gnupg      ;     rm -rf /var/lib/apt/lists/*
# Tue, 12 Oct 2021 16:12:25 GMT
RUN set -eux;     apt-get update;     apt-get install -y --no-install-recommends gosu tini;     rm -rf /var/lib/apt/lists/*;     gosu nobody true;     tini --version
# Tue, 12 Oct 2021 16:12:25 GMT
ENV GPG_COUCH_KEY=390EF70BB1EA12B2773962950EE62FB37A00258D
# Tue, 12 Oct 2021 16:12:32 GMT
RUN set -eux;     apt-get update;     apt-get install -y curl;     export GNUPGHOME="$(mktemp -d)";     curl -fL -o keys.asc https://couchdb.apache.org/repo/keys.asc;     gpg --batch --import keys.asc;     gpg --batch --export "${GPG_COUCH_KEY}" > /usr/share/keyrings/couchdb-archive-keyring.gpg;     command -v gpgconf && gpgconf --kill all || :;     rm -rf "$GNUPGHOME";     apt-key list;     apt purge -y --autoremove curl;     rm -rf /var/lib/apt/lists/*
# Tue, 12 Oct 2021 16:12:32 GMT
ENV COUCHDB_VERSION=3.2.0
# Tue, 12 Oct 2021 16:12:33 GMT
RUN . /etc/os-release;     echo "deb [signed-by=/usr/share/keyrings/couchdb-archive-keyring.gpg] https://apache.jfrog.io/artifactory/couchdb-deb/ ${VERSION_CODENAME} main" |         tee /etc/apt/sources.list.d/couchdb.list >/dev/null
# Tue, 12 Oct 2021 16:12:46 GMT
RUN set -eux;     apt-get update;         echo "couchdb couchdb/mode select none" | debconf-set-selections;     DEBIAN_FRONTEND=noninteractive apt-get install -y --allow-downgrades --allow-remove-essential --allow-change-held-packages             couchdb="$COUCHDB_VERSION"~buster     ;     rmdir /var/lib/couchdb /var/log/couchdb;     rm /opt/couchdb/data /opt/couchdb/var/log;     mkdir -p /opt/couchdb/data /opt/couchdb/var/log;     chown couchdb:couchdb /opt/couchdb/data /opt/couchdb/var/log;     chmod 777 /opt/couchdb/data /opt/couchdb/var/log;     rm /opt/couchdb/etc/default.d/10-filelog.ini;     find /opt/couchdb \! \( -user couchdb -group couchdb \) -exec chown -f couchdb:couchdb '{}' +;     find /opt/couchdb/etc -type d ! -perm 0755 -exec chmod -f 0755 '{}' +;     find /opt/couchdb/etc -type f ! -perm 0644 -exec chmod -f 0644 '{}' +;     chmod -f 0777 /opt/couchdb/etc/local.d;     rm -rf /var/lib/apt/lists/*;
# Tue, 12 Oct 2021 16:12:46 GMT
COPY --chown=couchdb:couchdbfile:459581cb8ff69dbc1cb246db7b488d5b6127e57fcbb0d0df1288722b5cd25111 in /opt/couchdb/etc/default.d/ 
# Tue, 12 Oct 2021 16:12:46 GMT
COPY --chown=couchdb:couchdbfile:f98e48e4254cb3ec4a766f3b9bd3260f16676a310eb0356ee9775c62edb3e8f3 in /opt/couchdb/etc/ 
# Tue, 12 Oct 2021 16:12:46 GMT
COPY file:5f96ca1bf2f6f650a65a16c93abec310412df7ca501bf32df2ac20f99b1a0742 in /usr/local/bin 
# Tue, 12 Oct 2021 16:12:47 GMT
RUN ln -s usr/local/bin/docker-entrypoint.sh /docker-entrypoint.sh # backwards compat
# Tue, 12 Oct 2021 16:12:47 GMT
ENTRYPOINT ["tini" "--" "/docker-entrypoint.sh"]
# Tue, 12 Oct 2021 16:12:47 GMT
VOLUME [/opt/couchdb/data]
# Tue, 12 Oct 2021 16:12:48 GMT
EXPOSE 4369 5984 9100
# Tue, 12 Oct 2021 16:12:48 GMT
CMD ["/opt/couchdb/bin/couchdb"]
```

-	Layers:
	-	`sha256:02b45931a436a68cadb742684148ed6aacbbf9aa21447b467c11bac97f92eb46`  
		Last Modified: Tue, 12 Oct 2021 01:49:07 GMT  
		Size: 25.9 MB (25908479 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:9c646791d9868995e805f149bec168431525d3c31b3d0c73124e5a37e4afa76f`  
		Last Modified: Tue, 12 Oct 2021 16:14:03 GMT  
		Size: 3.4 KB (3435 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:836357709ccd6570063bff28686a586c566ca7145e0adb10db6ca88966f4095e`  
		Last Modified: Tue, 12 Oct 2021 16:14:03 GMT  
		Size: 6.6 MB (6550798 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:b7164389db8f0224aead9715f7478a90fb8d0612a43377991018342353359def`  
		Last Modified: Tue, 12 Oct 2021 16:14:02 GMT  
		Size: 1.2 MB (1163463 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:4097cf55145d3e6941810229afc94a987195c0631cc4d532dd62be1d1db8cae4`  
		Last Modified: Tue, 12 Oct 2021 16:14:01 GMT  
		Size: 292.9 KB (292852 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:7f6cdff452cdb1cbe7c743d23568c7c2f36970b360d7719f5cdc94e6b556c99d`  
		Last Modified: Tue, 12 Oct 2021 16:14:01 GMT  
		Size: 278.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:2157f9eb628e6f0866a1288e2621b0f615e47020dba33d47a194c7ecdbc346cb`  
		Last Modified: Tue, 12 Oct 2021 16:14:04 GMT  
		Size: 41.7 MB (41722916 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:7f7ec62e0fb1cdc2de91fb8de6822acfa15e206691554d6134c7ff6d75f53c3e`  
		Last Modified: Tue, 12 Oct 2021 16:13:59 GMT  
		Size: 380.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:61af5e9d1ae116230517b15c801cb3dc7fe42e88a30222ef9b88b947327512d4`  
		Last Modified: Tue, 12 Oct 2021 16:13:59 GMT  
		Size: 765.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:48d296d0af30fbebf70e062435b833f7acdbbcdc6714124ccc0a43b9c6e1b8e9`  
		Last Modified: Tue, 12 Oct 2021 16:13:58 GMT  
		Size: 2.1 KB (2058 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:431e5a96bee9235b1db52994b56b5dd4ed5b22987684ea1e3c4fae3cc40d55b5`  
		Last Modified: Tue, 12 Oct 2021 16:13:59 GMT  
		Size: 118.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
