## `logstash:6.8.20`

```console
$ docker pull logstash@sha256:930ee70fea7e557e936afca04d5ccfa5f6500e2f4d0ae2223290792c11d76cd8
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `logstash:6.8.20` - linux; amd64

```console
$ docker pull logstash@sha256:940eaaaab88a1a345191a714a12c59a2a60b4ecad91ec6bb7512781653d37fe7
```

-	Docker Version: 20.10.9
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **387.6 MB (387618098 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:70ad8c8e0738d6b73b5a0ce2fcf7bd7587735f8154579a1b0fc0ef5001128426`
-	Entrypoint: `["\/usr\/local\/bin\/docker-entrypoint"]`

```dockerfile
# Wed, 15 Sep 2021 18:20:23 GMT
ADD file:b3ebbe8bd304723d43b7b44a6d990cd657b63d93d6a2a9293983a30bfc1dfa53 in / 
# Wed, 15 Sep 2021 18:20:23 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.name=CentOS Base Image org.label-schema.vendor=CentOS org.label-schema.license=GPLv2 org.label-schema.build-date=20201113 org.opencontainers.image.title=CentOS Base Image org.opencontainers.image.vendor=CentOS org.opencontainers.image.licenses=GPL-2.0-only org.opencontainers.image.created=2020-11-13 00:00:00+00:00
# Wed, 15 Sep 2021 18:20:23 GMT
CMD ["/bin/bash"]
# Thu, 07 Oct 2021 21:09:31 GMT
RUN yum update -y && yum install -y java-1.8.0-openjdk-devel-1.8.0.282.b08 java-1.8.0-openjdk-headless-1.8.0.282.b08 which &&     yum clean all
# Thu, 07 Oct 2021 21:09:34 GMT
RUN groupadd --gid 1000 logstash &&     adduser --uid 1000 --gid 1000       --home-dir /usr/share/logstash --no-create-home       logstash
# Thu, 07 Oct 2021 21:09:51 GMT
RUN curl -Lo - http://localhost:8000/logstash-6.8.20.tar.gz |     tar zxf - -C /usr/share &&     mv /usr/share/logstash-6.8.20 /usr/share/logstash &&     chown --recursive logstash:logstash /usr/share/logstash/ &&     chown -R logstash:root /usr/share/logstash &&     chmod -R g=u /usr/share/logstash &&     find /usr/share/logstash -type d -exec chmod g+s {} \; &&     ln -s /usr/share/logstash /opt/logstash
# Thu, 07 Oct 2021 21:09:53 GMT
WORKDIR /usr/share/logstash
# Thu, 07 Oct 2021 21:09:53 GMT
ENV ELASTIC_CONTAINER=true
# Thu, 07 Oct 2021 21:09:53 GMT
ENV PATH=/usr/share/logstash/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
# Thu, 07 Oct 2021 21:09:53 GMT
ADD file:1183410472ec370104718a08e1144081518db1d006a8cc82de824a34455ab3f3 in config/pipelines.yml 
# Thu, 07 Oct 2021 21:09:53 GMT
ADD file:83ab096464b764c812ae68c2872c05d48ee1620e6a1629948d52c13ac6dcfe11 in config/logstash.yml 
# Thu, 07 Oct 2021 21:09:53 GMT
ADD file:2ef21d4766eab3ac48ed3847c8b8d05554f1fd0b39061cba66c9ac93240087fa in config/ 
# Thu, 07 Oct 2021 21:09:54 GMT
ADD file:0cd9cc51daf5f37b2aa8aae8cf3570a3680e22852afb2803ccb87ddcd3369f52 in pipeline/logstash.conf 
# Thu, 07 Oct 2021 21:09:54 GMT
RUN chown --recursive logstash:root config/ pipeline/
# Thu, 07 Oct 2021 21:09:54 GMT
ENV LANG=en_US.UTF-8 LC_ALL=en_US.UTF-8
# Thu, 07 Oct 2021 21:09:54 GMT
ADD file:29dd60f159d64086c20a7a02f84a9314f44b2290304547233fb96744325b1245 in /usr/local/bin/ 
# Thu, 07 Oct 2021 21:09:55 GMT
RUN chmod 0755 /usr/local/bin/docker-entrypoint
# Thu, 07 Oct 2021 21:09:55 GMT
USER 1000
# Thu, 07 Oct 2021 21:09:55 GMT
ADD file:c571f1340b3840928052ac69357eca598068bd1a752b377b661e4a526205c25b in /usr/local/bin/ 
# Thu, 07 Oct 2021 21:09:55 GMT
EXPOSE 5044 9600
# Thu, 07 Oct 2021 21:09:55 GMT
LABEL org.label-schema.schema-version=1.0 org.label-schema.vendor=Elastic org.label-schema.name=logstash org.label-schema.version=6.8.20 org.label-schema.url=https://www.elastic.co/products/logstash org.label-schema.vcs-url=https://github.com/elastic/logstash license=Elastic License
# Thu, 07 Oct 2021 21:09:55 GMT
ENTRYPOINT ["/usr/local/bin/docker-entrypoint"]
```

-	Layers:
	-	`sha256:2d473b07cdd5f0912cd6f1a703352c82b512407db6b05b43f2553732b55df3bc`  
		Last Modified: Sat, 14 Nov 2020 00:21:39 GMT  
		Size: 76.1 MB (76097157 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:e6f845d959f7a000ccdd444e3f747f12b926b6fe6bcced60f5bfb44bfb51be85`  
		Last Modified: Thu, 14 Oct 2021 14:14:08 GMT  
		Size: 130.0 MB (130034901 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:c8e2d5941a02a9c25ee7d7c53a3406ec837b1c783ab857877f25a70a4980a9dd`  
		Last Modified: Thu, 14 Oct 2021 14:13:54 GMT  
		Size: 1.8 KB (1820 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:68a1b1ad4ae879f9a1ffe2f3f5d1b6519f45b7f06f991e02540f41b3e9a668de`  
		Last Modified: Thu, 14 Oct 2021 14:14:05 GMT  
		Size: 179.8 MB (179815573 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:e8894f89da33ccb66b6c5ba220c9db337703eaa6066fefc32518c6474ce035c1`  
		Last Modified: Thu, 14 Oct 2021 14:13:51 GMT  
		Size: 382.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:58ff9ace86c633a84e75f2da8c02e21ab9b2ee10dc6c388d33c8ff38afc623c2`  
		Last Modified: Thu, 14 Oct 2021 14:13:51 GMT  
		Size: 280.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:c31d2fb714ae3d471fa8ded079dd626f4fbdf279d3624b6b0b0d186ab79ac191`  
		Last Modified: Thu, 14 Oct 2021 14:13:51 GMT  
		Size: 447.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:37627b266c4172583d0111a0166531e70312e25f0f8cff517711236e79821f24`  
		Last Modified: Thu, 14 Oct 2021 14:13:49 GMT  
		Size: 307.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:704b9db70829fd31d7c64eb7c1f7e684db9cd22621e7f62c05e2abfa57587010`  
		Last Modified: Thu, 14 Oct 2021 14:13:48 GMT  
		Size: 2.7 KB (2680 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:8e3d0810a54972298734fbf0276a7bc3412aaa41af527490145bb8bec109cb7e`  
		Last Modified: Thu, 14 Oct 2021 14:13:48 GMT  
		Size: 498.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:8e3d0810a54972298734fbf0276a7bc3412aaa41af527490145bb8bec109cb7e`  
		Last Modified: Thu, 14 Oct 2021 14:13:48 GMT  
		Size: 498.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:6c6e711d4a1fda74a879de38afd3f1255071b6b751e7f4302ed1becd575092df`  
		Last Modified: Thu, 14 Oct 2021 14:13:48 GMT  
		Size: 1.7 MB (1663555 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
