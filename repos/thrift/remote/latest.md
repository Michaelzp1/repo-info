## `thrift:latest`

```console
$ docker pull thrift@sha256:c05261d526cdb880a07340d2b720afbde1a6da73e0dc994df2a0fb3129762ac3
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 1
	-	linux; amd64

### `thrift:latest` - linux; amd64

```console
$ docker pull thrift@sha256:e0a01e94dc4e65f65f5859ea9cb6dcebe047b71c5e4ee3180b7ebd15c6eb300e
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **43.7 MB (43741968 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4a3c6506b0935acabfd7b7e0661a975926d446495095cc6f5c2332397e6c73b0`
-	Default Command: `["thrift"]`

```dockerfile
# Fri, 01 Oct 2021 02:23:23 GMT
ADD file:0d82cd095966e8ee78b593cb47a352eec842edb7bd9d9468e8a70154522447d1 in / 
# Fri, 01 Oct 2021 02:23:24 GMT
CMD ["bash"]
# Fri, 01 Oct 2021 06:56:46 GMT
ENV THRIFT_VERSION=v0.12.0
# Fri, 01 Oct 2021 07:01:20 GMT
RUN buildDeps=" 		automake 		bison 		curl 		flex 		g++ 		libboost-dev 		libboost-filesystem-dev 		libboost-program-options-dev 		libboost-system-dev 		libboost-test-dev 		libevent-dev 		libssl-dev 		libtool 		make 		pkg-config 	"; 	apt-get update && apt-get install -y --no-install-recommends $buildDeps && rm -rf /var/lib/apt/lists/* 	&& curl -k -sSL "https://github.com/apache/thrift/archive/${THRIFT_VERSION}.tar.gz" -o thrift.tar.gz 	&& mkdir -p /usr/src/thrift 	&& tar zxf thrift.tar.gz -C /usr/src/thrift --strip-components=1 	&& rm thrift.tar.gz 	&& cd /usr/src/thrift 	&& ./bootstrap.sh 	&& ./configure --disable-libs 	&& make 	&& make install 	&& cd / 	&& rm -rf /usr/src/thrift 	&& apt-get purge -y --auto-remove $buildDeps 	&& rm -rf /var/cache/apt/* 	&& rm -rf /var/lib/apt/lists/* 	&& rm -rf /tmp/* 	&& rm -rf /var/tmp/*
# Fri, 01 Oct 2021 07:01:20 GMT
CMD ["thrift"]
```

-	Layers:
	-	`sha256:284055322776031bac33723839acb0db2d063a525ba4fa1fd268a831c7553b26`  
		Last Modified: Fri, 01 Oct 2021 02:25:02 GMT  
		Size: 26.7 MB (26705075 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:5cceb9b606db438ff089793b1562f2cd5365a63bdd5d29e07bee21271669bc90`  
		Last Modified: Fri, 01 Oct 2021 07:01:38 GMT  
		Size: 17.0 MB (17036893 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
