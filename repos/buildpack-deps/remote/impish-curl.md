## `buildpack-deps:impish-curl`

```console
$ docker pull buildpack-deps@sha256:ed9e4447b9ca9fb15efff55312c7025b4fed9eac970b18ceb863220113a5dce4
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms: 6
	-	linux; amd64
	-	linux; arm variant v7
	-	linux; arm64 variant v8
	-	linux; ppc64le
	-	linux; riscv64
	-	linux; s390x

### `buildpack-deps:impish-curl` - linux; amd64

```console
$ docker pull buildpack-deps@sha256:94e778a6cbd993df08839e17a34b60e613db75ae7ac321fd94cacab5e62c71fe
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **37.6 MB (37640287 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:04c2d1d7ecc298b4bb6231f2eea9f28b898c1509faaa2916bffe03ef97cf9ab7`
-	Default Command: `["bash"]`

```dockerfile
# Sat, 16 Oct 2021 00:37:58 GMT
ADD file:41d75787395224d025bcc0f7feca7c56fd4c1adea7cce667e2472fad282054fc in / 
# Sat, 16 Oct 2021 00:37:59 GMT
CMD ["bash"]
# Sat, 16 Oct 2021 01:48:18 GMT
RUN set -eux; 	apt-get update; 	apt-get install -y --no-install-recommends 		ca-certificates 		curl 		netbase 		wget 		tzdata 	; 	rm -rf /var/lib/apt/lists/*
# Sat, 16 Oct 2021 01:48:35 GMT
RUN set -ex; 	if ! command -v gpg > /dev/null; then 		apt-get update; 		apt-get install -y --no-install-recommends 			gnupg 			dirmngr 		; 		rm -rf /var/lib/apt/lists/*; 	fi
```

-	Layers:
	-	`sha256:3910f09893e52b84e55ef361892610a5a78e9df154e23da9bd7e317a4f3518d5`  
		Last Modified: Sat, 16 Oct 2021 00:38:57 GMT  
		Size: 30.4 MB (30391190 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:344f3e1ad647088394470484ad047b920daf1cfde567fb1b99b7526177300185`  
		Last Modified: Sat, 16 Oct 2021 01:55:21 GMT  
		Size: 3.7 MB (3697350 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:62e70db9e5b826e9fac9ce0f806fa2b5875a56ccc8ea33a2e7eeafab2210f485`  
		Last Modified: Sat, 16 Oct 2021 01:55:21 GMT  
		Size: 3.6 MB (3551747 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `buildpack-deps:impish-curl` - linux; arm variant v7

```console
$ docker pull buildpack-deps@sha256:320faf26b91dadf96e3584a78a08c05d0592dfd89e440b6f97644a25e5115d6f
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **34.1 MB (34107402 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d5ca790b6720183247a0a4cb2735fb3bf9773cba17b326e470a05ef7d20d5529`
-	Default Command: `["bash"]`

```dockerfile
# Sat, 16 Oct 2021 01:12:36 GMT
ADD file:200fc5e8340bed54c8a1f2d364d7eecb37212e03a3c7c5f8b9fe94a5689b34fc in / 
# Sat, 16 Oct 2021 01:12:37 GMT
CMD ["bash"]
# Sat, 16 Oct 2021 02:56:08 GMT
RUN set -eux; 	apt-get update; 	apt-get install -y --no-install-recommends 		ca-certificates 		curl 		netbase 		wget 		tzdata 	; 	rm -rf /var/lib/apt/lists/*
# Sat, 16 Oct 2021 02:56:21 GMT
RUN set -ex; 	if ! command -v gpg > /dev/null; then 		apt-get update; 		apt-get install -y --no-install-recommends 			gnupg 			dirmngr 		; 		rm -rf /var/lib/apt/lists/*; 	fi
```

-	Layers:
	-	`sha256:47d179bbc8c38e5833d8d05d251749ab4997c8e937e8913717f1124bdd9f3c75`  
		Last Modified: Sat, 16 Oct 2021 01:15:40 GMT  
		Size: 26.9 MB (26919116 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:08bb3c52ec36c639d0f08c768d92fdc4d7f3c666134b951d319f8898397c5a76`  
		Last Modified: Sat, 16 Oct 2021 03:07:20 GMT  
		Size: 3.4 MB (3445893 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:09246257384b997d403eae12b428a45f75bd853afa99e13b4ea5a6d3bda9c204`  
		Last Modified: Sat, 16 Oct 2021 03:07:19 GMT  
		Size: 3.7 MB (3742393 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `buildpack-deps:impish-curl` - linux; arm64 variant v8

```console
$ docker pull buildpack-deps@sha256:3cee4a7cc97bbeeead6035ad91791b2a75e236da312a67384f71ec157d9cf706
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **36.1 MB (36050448 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:2466a5093a1996bddc06370565b1bcff5ad7fb468d3f03a177905553413fa71d`
-	Default Command: `["bash"]`

```dockerfile
# Sat, 16 Oct 2021 01:48:00 GMT
ADD file:12f0ca9f9259de0f0a05250c3c108e96125ae9e13b868f0d2361add6c231bba4 in / 
# Sat, 16 Oct 2021 01:48:00 GMT
CMD ["bash"]
# Sat, 16 Oct 2021 03:09:40 GMT
RUN set -eux; 	apt-get update; 	apt-get install -y --no-install-recommends 		ca-certificates 		curl 		netbase 		wget 		tzdata 	; 	rm -rf /var/lib/apt/lists/*
# Sat, 16 Oct 2021 03:09:48 GMT
RUN set -ex; 	if ! command -v gpg > /dev/null; then 		apt-get update; 		apt-get install -y --no-install-recommends 			gnupg 			dirmngr 		; 		rm -rf /var/lib/apt/lists/*; 	fi
```

-	Layers:
	-	`sha256:87227608fa0c8780400c9e08e962fb29c6c6326768ea08e296ee1df1fd96e922`  
		Last Modified: Sat, 16 Oct 2021 01:49:59 GMT  
		Size: 29.0 MB (29042311 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:a6de1cb0e065d2da5a88c6e95dc74343d769c8445e54f1e4c58bd912b712fb66`  
		Last Modified: Sat, 16 Oct 2021 03:22:32 GMT  
		Size: 3.7 MB (3681570 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:747e87b1a38008b08e7eb642fc25e2d7bb6fe80f4bc024b256181095bc63e5ec`  
		Last Modified: Sat, 16 Oct 2021 03:22:32 GMT  
		Size: 3.3 MB (3326567 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `buildpack-deps:impish-curl` - linux; ppc64le

```console
$ docker pull buildpack-deps@sha256:4ac0688649cf19387d38e7b043f66225ec9f5309acad8674622821bc726b28c8
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **44.6 MB (44569136 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:81e1b983abbb1ffbb8530aa484a7d0b2e197359e1b0c09e942016ffdbfe2d748`
-	Default Command: `["bash"]`

```dockerfile
# Sat, 16 Oct 2021 00:37:08 GMT
ADD file:2882612be3480d0a26c565cfff723f6f7c913ecc1e3b982def388f5c29ad10e6 in / 
# Sat, 16 Oct 2021 00:37:12 GMT
CMD ["bash"]
# Sat, 16 Oct 2021 01:18:41 GMT
RUN set -eux; 	apt-get update; 	apt-get install -y --no-install-recommends 		ca-certificates 		curl 		netbase 		wget 		tzdata 	; 	rm -rf /var/lib/apt/lists/*
# Sat, 16 Oct 2021 01:19:04 GMT
RUN set -ex; 	if ! command -v gpg > /dev/null; then 		apt-get update; 		apt-get install -y --no-install-recommends 			gnupg 			dirmngr 		; 		rm -rf /var/lib/apt/lists/*; 	fi
```

-	Layers:
	-	`sha256:4cfd5cbebb9f6bac4f6c4e8b0ed1450129f245085d7131094975207c3091fb66`  
		Last Modified: Sat, 16 Oct 2021 00:39:02 GMT  
		Size: 36.2 MB (36177739 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:6c5e42bcc4ae69e168604f5e12739b0b1eb5c7801c5bbba53b80248a3522d3b2`  
		Last Modified: Sat, 16 Oct 2021 01:31:20 GMT  
		Size: 4.1 MB (4149852 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:1fbf4430060475cdfccdf811046cc128f1e962010f7015eb473cd56a44a181a8`  
		Last Modified: Sat, 16 Oct 2021 01:31:20 GMT  
		Size: 4.2 MB (4241545 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `buildpack-deps:impish-curl` - linux; riscv64

```console
$ docker pull buildpack-deps@sha256:79286edeff7a31bec8b2e2b654586ba284c8dbada8a7eac779484e0d00f8ecbd
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **34.5 MB (34505933 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:a31712f5300998cfb5a7da064b6738fd8ce992f469228720944c925fc306ae57`
-	Default Command: `["bash"]`

```dockerfile
# Sat, 16 Oct 2021 00:17:48 GMT
ADD file:8cf59b4b38ccab14d48f2740546784966d4b20690dc57f9018241a62adac259e in / 
# Sat, 16 Oct 2021 00:17:50 GMT
CMD ["bash"]
# Sat, 16 Oct 2021 01:02:36 GMT
RUN set -eux; 	apt-get update; 	apt-get install -y --no-install-recommends 		ca-certificates 		curl 		netbase 		wget 		tzdata 	; 	rm -rf /var/lib/apt/lists/*
# Sat, 16 Oct 2021 01:03:17 GMT
RUN set -ex; 	if ! command -v gpg > /dev/null; then 		apt-get update; 		apt-get install -y --no-install-recommends 			gnupg 			dirmngr 		; 		rm -rf /var/lib/apt/lists/*; 	fi
```

-	Layers:
	-	`sha256:19a9653dc9c1bf288e8c03d2027a0edd6609e7bc4c852b05e2ba35e947868701`  
		Last Modified: Sat, 16 Oct 2021 00:31:12 GMT  
		Size: 27.2 MB (27208455 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:4e415d53d305cea0edcb8861ea11f8c974a195af5e0bbab60d58e5e787c69b09`  
		Last Modified: Sat, 16 Oct 2021 01:31:29 GMT  
		Size: 3.5 MB (3494237 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:c502225d86b92683284985a96ecaca6eec34631b1832346308e6c7a4edb2378e`  
		Last Modified: Sat, 16 Oct 2021 01:31:28 GMT  
		Size: 3.8 MB (3803241 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `buildpack-deps:impish-curl` - linux; s390x

```console
$ docker pull buildpack-deps@sha256:f646bbd09edfae6986b960903e892636c074e1ba7ee28c315dd04faf19014eab
```

-	Docker Version: 20.10.7
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **38.3 MB (38262133 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:000fff3022b83720968529a823801427b71676efb3b37755cc42e7ec280291a9`
-	Default Command: `["bash"]`

```dockerfile
# Sat, 16 Oct 2021 00:42:02 GMT
ADD file:e3ac42c4b4650e7d57adf242fb9b7137898397121142c4fd47afb661024e0b00 in / 
# Sat, 16 Oct 2021 00:42:03 GMT
CMD ["bash"]
# Sat, 16 Oct 2021 01:11:17 GMT
RUN set -eux; 	apt-get update; 	apt-get install -y --no-install-recommends 		ca-certificates 		curl 		netbase 		wget 		tzdata 	; 	rm -rf /var/lib/apt/lists/*
# Sat, 16 Oct 2021 01:11:21 GMT
RUN set -ex; 	if ! command -v gpg > /dev/null; then 		apt-get update; 		apt-get install -y --no-install-recommends 			gnupg 			dirmngr 		; 		rm -rf /var/lib/apt/lists/*; 	fi
```

-	Layers:
	-	`sha256:7101024e2e65688584ff7a7aa5f503fe9e8165ebcc5fb924b1304bbdd0d4256e`  
		Last Modified: Sat, 16 Oct 2021 00:43:14 GMT  
		Size: 30.5 MB (30527974 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:d6c893f3d07db208a09670ca2a61f5af4e83488249bea3ec02f9de57eb0e6c1f`  
		Last Modified: Sat, 16 Oct 2021 01:15:57 GMT  
		Size: 3.8 MB (3771530 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:7a3328cbd7261ef1a0a50687cdd7d7c486ab672569fbd2f169f94fdb4d20618a`  
		Last Modified: Sat, 16 Oct 2021 01:15:57 GMT  
		Size: 4.0 MB (3962629 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
