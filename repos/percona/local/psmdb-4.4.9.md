# `percona:psmdb-4.4.9`

## Docker Metadata

- Image ID: `sha256:cc72685c6fa3c2f1529c6794b48513d514c77638b88d1027db7e76fc2f2d9e3f`
- Created: `2021-10-08T17:29:49.924405434Z`
- Virtual Size: ~ 535.10 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/entrypoint.sh"]`
- Command: `["mongod"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `PSMDB_VERSION=4.4.9-10`
  - `OS_VER=el8`
  - `FULL_PERCONA_VERSION=4.4.9-10.el8`
  - `K8S_TOOLS_VERSION=0.5.0`
  - `GOSU_VERSION=1.11`
- Labels:
  - `org.label-schema.build-date=20210915`
  - `org.label-schema.license=GPLv2`
  - `org.label-schema.name=CentOS Base Image`
  - `org.label-schema.schema-version=1.0`
  - `org.label-schema.vendor=CentOS`
  - `org.opencontainers.image.authors=info@percona.com`
