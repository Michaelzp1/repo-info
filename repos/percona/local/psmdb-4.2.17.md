# `percona:psmdb-4.2.17`

## Docker Metadata

- Image ID: `sha256:edaad76fee8fdbd6510a8f705956d7b3c3e5241775ffe009fafe7f68d1a70d96`
- Created: `2021-10-13T15:54:05.211603694Z`
- Virtual Size: ~ 484.47 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/entrypoint.sh"]`
- Command: `["mongod"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `PSMDB_VERSION=4.2.17-17`
  - `OS_VER=el8`
  - `FULL_PERCONA_VERSION=4.2.17-17.el8`
  - `K8S_TOOLS_VERSION=0.5.0`
  - `GOSU_VERSION=1.11`
- Labels:
  - `org.label-schema.build-date=20210915`
  - `org.label-schema.license=GPLv2`
  - `org.label-schema.name=CentOS Base Image`
  - `org.label-schema.schema-version=1.0`
  - `org.label-schema.vendor=CentOS`
  - `org.opencontainers.image.authors=info@percona.com`
