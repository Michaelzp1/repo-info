# `percona:psmdb-4.0.27`

## Docker Metadata

- Image ID: `sha256:0c3f99d0b34d3c7268838ba4d28baefdeb669193be773ec03fa910c4fa75c8ef`
- Created: `2021-09-30T18:34:49.372583293Z`
- Virtual Size: ~ 466.92 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/entrypoint.sh"]`
- Command: `["mongod"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `PSMDB_VERSION=4.0.27-22`
  - `OS_VER=el8`
  - `FULL_PERCONA_VERSION=4.0.27-22.el8`
  - `K8S_TOOLS_VERSION=0.5.0`
  - `GOSU_VERSION=1.11`
- Labels:
  - `org.label-schema.build-date=20210915`
  - `org.label-schema.license=GPLv2`
  - `org.label-schema.name=CentOS Base Image`
  - `org.label-schema.schema-version=1.0`
  - `org.label-schema.vendor=CentOS`
  - `org.opencontainers.image.authors=info@percona.com`
