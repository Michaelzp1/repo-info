# `percona:psmdb-5.0.3`

## Docker Metadata

- Image ID: `sha256:e5454d71e448a9c2a6bbb75476c5f4b5c291b8bf16978942d3bdd6d83b0f36ea`
- Created: `2021-10-15T23:34:58.586712613Z`
- Virtual Size: ~ 572.06 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/entrypoint.sh"]`
- Command: `["mongod"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `PSMDB_VERSION=5.0.3-2`
  - `OS_VER=el8`
  - `FULL_PERCONA_VERSION=5.0.3-2.el8`
  - `K8S_TOOLS_VERSION=0.5.0`
  - `GOSU_VERSION=1.11`
- Labels:
  - `org.label-schema.build-date=20210915`
  - `org.label-schema.license=GPLv2`
  - `org.label-schema.name=CentOS Base Image`
  - `org.label-schema.schema-version=1.0`
  - `org.label-schema.vendor=CentOS`
  - `org.opencontainers.image.authors=info@percona.com`
