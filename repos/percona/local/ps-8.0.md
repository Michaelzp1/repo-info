# `percona:8.0.26-16-centos`

## Docker Metadata

- Image ID: `sha256:4d8f7c11db188b2b59bcd6c8fe0fc110bca466f5f5db115a3318f3d66ccb0aec`
- Created: `2021-10-21T22:45:04.668288021Z`
- Virtual Size: ~ 858.25 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/docker-entrypoint.sh"]`
- Command: `["mysqld"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `PS_VERSION=8.0.26-16.1`
  - `OS_VER=el8`
  - `FULL_PERCONA_VERSION=8.0.26-16.1.el8`
- Labels:
  - `org.label-schema.build-date=20210915`
  - `org.label-schema.license=GPLv2`
  - `org.label-schema.name=CentOS Base Image`
  - `org.label-schema.schema-version=1.0`
  - `org.label-schema.vendor=CentOS`
  - `org.opencontainers.image.authors=info@percona.com`
