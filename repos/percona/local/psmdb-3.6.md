# `percona:psmdb-3.6.23`

## Docker Metadata

- Image ID: `sha256:6a1f241c8ca6a8cde6f6a8616807ddfa6730184fc0756d2e4f4237e92ef497cd`
- Created: `2021-09-15T19:03:16.167446574Z`
- Virtual Size: ~ 441.39 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/entrypoint.sh"]`
- Command: `["mongod"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `PSMDB_VERSION=3.6.23-13.0`
  - `OS_VER=el8`
  - `FULL_PERCONA_VERSION=3.6.23-13.0.el8`
  - `K8S_TOOLS_VERSION=0.5.0`
- Labels:
  - `org.label-schema.build-date=20210915`
  - `org.label-schema.license=GPLv2`
  - `org.label-schema.name=CentOS Base Image`
  - `org.label-schema.schema-version=1.0`
  - `org.label-schema.vendor=CentOS`
  - `org.opencontainers.image.authors=info@percona.com`
