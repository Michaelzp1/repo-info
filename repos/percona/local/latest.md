# `percona:5.7.35-centos`

## Docker Metadata

- Image ID: `sha256:14dacdf98c7ad66e08bb5015db42cb2ccef052c8f2d05c7ff5947ac762583c4d`
- Created: `2021-09-15T18:58:12.979609668Z`
- Virtual Size: ~ 657.58 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/docker-entrypoint.sh"]`
- Command: `["mysqld"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `PS_VERSION=5.7.35-38.1`
  - `OS_VER=el8`
  - `FULL_PERCONA_VERSION=5.7.35-38.1.el8`
- Labels:
  - `org.label-schema.build-date=20210915`
  - `org.label-schema.license=GPLv2`
  - `org.label-schema.name=CentOS Base Image`
  - `org.label-schema.schema-version=1.0`
  - `org.label-schema.vendor=CentOS`
  - `org.opencontainers.image.authors=info@percona.com`
